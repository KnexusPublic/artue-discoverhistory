 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: April 8, 2011
 ;;;; Project Name: Explanation
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file provides the functionality of explanation.

(in-package :artue)

;; Controls a useful debugging breakpoint triggered only when no consistent 
;; explanation is found.
(defvar *break-on-explanation-failure* nil)

;; Causes a break in the often anomalous but technically sound situation where 
;; a consistent explanation is found with no assumptions.
(defvar *break-on-no-assumptions* nil)

;; Causes a break after every explanation search completes.
(defvar *break-after-explanation* nil)

;; Debugging variables
(defvar *expgen* nil)
(defvar *exes* nil)
(defvar *fexes* nil)

(defvar *refinement-methods* nil)

(defvar *pregen-refinement-methods* nil)
(defvar *not-pregen-refinement-methods* nil)

(defvar *learning-refinement-methods* nil)

(defgeneric find-plausible-explanations (expgen exes &key &allow-other-keys))


;; Entry point to the explanation algorithm. Controls how deep the search goes
;; and how long iterations are allowed to take.
(defmethod find-plausible-explanations ((expgen discoverhistory-explanation-generator)
                                        exes 
                                        &key (keep-exes nil)
                                        &aux (found-exes nil) init-time)
    (when (or (null exes) (not (listp exes)))
        (error "Expected list of explanations as first argument to ~
                find-plausible-explanations.")
    )
    
    ;; Bypasses explanation search when existing explanations are ambiguously
    ;; consistent
    (when (member :ignore-ambiguous (get-inconsistency-selection-method expgen))
        (let (ambiguous-consistent-exes)
            (setq ambiguous-consistent-exes 
                (remove-if (embed-expgen #'get-unambiguous-inconsistencies expgen) exes)
            )
            (when ambiguous-consistent-exes 
                (return-from find-plausible-explanations ambiguous-consistent-exes)
            )
        )
    )
    (setf (get-completed-explanations expgen) nil)
    (when (not keep-exes)
        (setq exes (mapcar+ (embed-expgen #'reset-explanation expgen) exes :reuse nil))
        (dolist (ex exes)
            (setf (explanation-prohibits-var-symbols ex) t)
            (dolist (occ (get-events ex))
                (setf (occurrence-old occ) t)
            )
        )
    )
    ; (when (get-multi-agent expgen)
        ; (setq exes (apply #'append exes (mapcar+ #'add-recent-actions exes expgen)))
    ; )
    
  
    (setq *expgen* expgen)
    (setq *exes* exes)
    ;(break "About to perform discover history search.")
    #+SBCL(sb-ext::gc :full t)
    #+ALLEGRO(excl:gc t)
    #+ALLEGRO(sys:resize-areas)
    (setq found-exes nil)
    
    (setq init-time (get-internal-run-time))
    (if (get-sample-ambiguity expgen)
        ;then
        (dolist (ex exes)
            (let ((new-exes (do-search expgen (list ex))))
                (when (listp new-exes)
                    (push-all new-exes found-exes)
                )
            )
        )
        ;else
        (setq found-exes (do-search expgen exes))
    )
    (incf (get-total-search-time expgen) 
        (+ 0.0 
           (/ (- (get-internal-run-time) init-time) 
              internal-time-units-per-second
           )
        )
    )

    (setf (get-last-search-was-cutoff expgen) (or (null found-exes) (symbolp found-exes)))
    (when (and (get-last-search-was-cutoff expgen) (or *break-on-explanation-failure* *break-after-explanation*))
        (when (null found-exes)
            (break "find-plausible-explanations: Search cutoff due to depth.")
        )
        (when (eq found-exes :out-of-time)
            (break "find-plausible-explanations: Search ran out of time.")
        )
        (when (eq found-exes :exceeded-memory-bound)
            (break "find-plausible-explanations: Search ran out of space.")
        )
    )
    (when (and (not (get-last-search-was-cutoff expgen)) *break-after-explanation*)
        (setq *fexes* (remove-duplicates found-exes))
        (when (< (length *fexes*) (length found-exes))
            (break "duplicate explanations found")
            (setq found-exes *fexes*)
        )
        (dolist (ex found-exes)
            (format t "~%~%Explanation ~A:" (position ex found-exes))
            (print-one-per-line (sort (copy-list (get-events ex)) #'occurrence-earlier))
        )
        (break "find-plausible-explanations: Explanation successful.")
    )
  
    (when (symbolp found-exes) (setq found-exes nil))

    (when (and (get-sample-ambiguity expgen) found-exes)
        (format t "~%~%Sampling ambiguity.")
        (let ((sampler (new-reservoir-sampler *max-result-length*)))
            (sample-ambiguity sampler expgen found-exes)
            (when (zerop (utils::get-population-size sampler))
                (setq *fexes* found-exes)
                (break "Whuh...?")
            )
            (setq found-exes (get-results sampler))
            (format t "~%~%Population size: ~A Results: ~A" 
                (utils::get-population-size sampler) (length found-exes)
            )
        )
    )
    
    ;; This is for the function "get-last-search-depth".
    (if found-exes
        ;then
        (setf (get-cost-limit expgen) (apply #'max (mapcar #'(lambda (ex) (compute-cost expgen ex)) found-exes)))
        ;else
        (setf (get-cost-limit expgen) (get-max-cost expgen))
    )
    (if (and (not (get-sample-ambiguity expgen)) (eq 1 (get-explanations-requested expgen)) (cdr found-exes))
        ;then
        (list (car found-exes))
        ;else
        (reverse found-exes)
    )
)

(defun do-search (expgen exes)
    (node-search (mapcar+ #'set-inconsistency-estimate exes 0)
        (embed-expgen #'discover-history expgen)
        (embed-expgen #'compute-cost expgen)
        #'explanation-consistent
        :solution-count (get-explanations-requested expgen)
        :max-cost (if (and (get-max-cost expgen) (> (get-max-cost expgen) 1)) (get-max-cost expgen) 10000)
        :max-seconds (if (get-use-time-cutoff expgen) (get-cutoff-seconds expgen) nil)
        :search-method :best-first
        :redundant-fn #'remove-redundant-explanations
        :node-limit 30000
        :parameter-update-fn (get-search-parameter-update-fn expgen)
    )
)


(defun add-recent-actions (ex expgen &aux facts vsym pt (ret-exes nil) poss-occs)
    (setq pt (point-next expgen (occurrence-point (get-prior-observation expgen))))
    (setq facts (facts-at expgen ex pt))
    (setq facts (remove-from-fact-set (get-self-fact expgen) (copy-fact-set facts)))
    (setq vsym (new-var-sym 'person))
    (setf (var-symbol-neq vsym) (list (get-self expgen)))
    (setq facts (add-to-fact-set (make-fact :type 'self :value vsym) facts))
    (setq facts (remove-from-fact-set (get-self-fact expgen) (copy-fact-set facts)))
    (dolist (act-model (get-external-action-models expgen))
        (setq poss-occs
            (enumerate-occs expgen act-model facts pt
                 :assume-fn #'hidden-and-self-assume-fn
            )
        )
        ; (setq poss-occs
            ; (enumerate-partially-bound-occs expgen act-model (facts-at expgen ex pt) pt (list nil)
                 ; :assume-fn #'assume-anything
            ; )
        ; )
        (push-all (add-actions-to-ex ex expgen poss-occs) ret-exes)
    )
    ret-exes
)


(defun add-actions-to-ex (ex expgen poss-occs &aux (ret-exes nil) new-ex pt)
    (setq pt (point-next expgen (occurrence-point (get-prior-observation expgen))))
    (dolist (occ poss-occs)
        (setf (occurrence-is-action occ) t)
        (setf (occurrence-is-event occ) nil)
        (setf (occurrence-is-assumption occ) t)
        (setf (occurrence-point occ) pt)
        (when (not (member (get-self-fact expgen) (occurrence-pre occ) :test #'equal-fact))
            (setf (occurrence-pre occ)
                (remove 'self (occurrence-pre occ) :key #'fact-type)
            )
            (setq new-ex
                (find-extra-events expgen 
                    (add-event-to-explanation expgen occ ex)
                ) 
            )
            (setf (explanation-cost new-ex) 5)
            (setq new-ex (add-to-history new-ex 'add-recent-actions nil ex))
            (push new-ex ret-exes)
        )
    )
    ret-exes
)


(defun event-strict-supersetp (ev-list1 ev-list2)
    (and (subsetp ev-list2 ev-list1 :test #'occurrence-equal)
         (set-difference ev-list1 ev-list2 :test #'occurrence-equal)
    )
)

(defun event-supersetp (ev-list1 ev-list2)
    (subsetp ev-list2 ev-list1 :test #'occurrence-equal)
)


(defun reset-explanation (expgen ex &key (reuse nil) &aux copy-ex)
    (if reuse
        ;then
        (setq copy-ex ex)
        ;else
        (setq copy-ex (make-explanation))
    )
    
    (setf (explanation-prior-events copy-ex) (get-events ex))
    (setf (explanation-prior-vsym-counter copy-ex) (1- *next-symbol-id*))
    (setf (explanation-new-events copy-ex) nil)
    (setf (explanation-event-removals copy-ex) nil)
    (setf (explanation-cached-hidden-results copy-ex) nil)
    (setf (explanation-cost copy-ex) 0)
    (setf (explanation-depth copy-ex) 0)
    (setf (explanation-changes copy-ex) 0)
    (let ((persistent-vsyms (mapcar #'var-symbol-name (get-var-symbols (get-execution-history expgen))))
          (new-bindings nil))
        (dolist (binding (explanation-bindings ex))
            (when (member (car binding) persistent-vsyms)
                (push binding new-bindings)
            )
        )
        (setf (explanation-bindings copy-ex) new-bindings)
    )
        
    (setf (explanation-occurrence-precedence copy-ex) nil)
    (setf (explanation-cached-precedence-map copy-ex) (make-precedence-cache-map))
    (setf (explanation-consistent copy-ex) nil)
  
    (setf (explanation-valid-event-pt copy-ex) (explanation-valid-event-pt ex))

    (setf (explanation-valid-fact-cache-pt copy-ex) (explanation-valid-fact-cache-pt ex))
    (setf (explanation-cached-inconsistencies copy-ex) (explanation-cached-inconsistencies ex))
    (setf (explanation-cached-inconsistency-list copy-ex) (explanation-cached-inconsistency-list ex))
    (setf (explanation-cached-statics copy-ex) (explanation-cached-statics ex))
    (setf (explanation-cached-fact-occs copy-ex) (explanation-cached-fact-occs ex))
    (setf (explanation-cached-facts copy-ex) (explanation-cached-facts ex))
    (setf (explanation-cached-hidden-results copy-ex) (explanation-cached-hidden-results ex))
    (setf (explanation-abandoned-models copy-ex) (explanation-abandoned-models ex))
  
    (when (not reuse)
        (setf (explanation-valid-fact-cache-pt ex) nil)
        (setf (explanation-cached-inconsistencies ex) nil)
        (setf (explanation-cached-inconsistency-list ex) nil)
        (setf (explanation-cached-fact-occs ex) nil)
        (setf (explanation-cached-facts ex) nil)
        (setf (explanation-cached-hidden-results ex) nil)
    )
    
    copy-ex
)

(defun get-last-search-depth (expgen)
    (get-cost-limit expgen)
)

(defun try-learning (inc ex)
    (declare (ignore inc ex))
    t
)


;;This is the central DiscoverHistory function summarized in "What Just Happened?"
(defun discover-history (expgen ex &aux inc-set best-ex-set lesser-inc-set)
    ;; Prunes nodes
    (when (member (get-assumption-changes ex) 
              (get-completed-explanations expgen) 
              :key #'get-assumption-changes 
              :test #'assumption-change-supersetp
          )
        (return-from discover-history nil)
    )

    ;; This finds simultaneous events with contradictory post or pre-conditions. 
    ;; Eliminates events that violate condition 3 of a plausible explanation, 
    ;; in "What Just Happened?"
    (when (null ex)
;        (break "Contradictions exist.")
        (return-from discover-history nil)
    )
    
    ;; Computes inconsistencies to consider.
    (setq inc-set (find-inconsistencies expgen ex))
    (when (< (length inc-set) (floor (explanation-estimated-inconsistency-count ex) 2))
        (warn "Estimate should be less: ~A" (getf (first (explanation-history ex)) :refine-type))
    )

    (multiple-value-setq (inc-set lesser-inc-set) 
        (select-inconsistencies expgen ex inc-set)
    )
    
    ;; Finds child nodes to explore.
    (setq best-ex-set (find-best-child-explanation-set expgen ex inc-set))
    (when (and inc-set (null best-ex-set))
        (return-from discover-history nil)
    )
    
    ;; Make sure there are no unsolvable inconsistencies lying around.
    (let ((lesser-ex-set (find-best-child-explanation-set expgen ex lesser-inc-set :max-exes (if (null inc-set) 1 (min 2 (length best-ex-set))))))
        (when (null lesser-ex-set)
            (return-from discover-history nil)
        )
        (when (and (listp lesser-ex-set) (listp best-ex-set) (cdr best-ex-set) (null (cdr lesser-ex-set)))
            (setq best-ex-set lesser-ex-set)
        )
    )
    
    ;; Find extra events caused by the completed cause.
    (when (and (null inc-set) (not (listp best-ex-set)))
        ;(format t "~%Last added assumption: ~A" (explanation-last-added-assumption ex))
        (setq ex (find-extra-events expgen ex))
        (when (eq ex 'fail)
            (when *trace-discover-history*
                (format t "~%Failed to extrapolate extra events.")
            )
            (return-from discover-history nil)
        )
        (setf (explanation-last-added-assumption ex) nil)
        (setf (explanation-last-added-event ex) nil)
        (when (null (get-unambiguous-inconsistencies expgen ex))
            (when (eq (nth-value 1 (fix-contradictions expgen ex inc-set)) :modified)
                (when *trace-discover-history*
                    (format t "~%Found contradictory solution.")
                )
                (return-from discover-history nil)
            )
            (when *trace-discover-history*
                (format t "~%Found solution.")
            )
            (setf (explanation-consistent ex) t)
        )
        (return-from discover-history (list ex))
    )
    
    
    ;; Build next generation
    (setq best-ex-set (remove-redundant-refinements best-ex-set ex))
    (when (null best-ex-set)
        (setq best-ex-set (refine-inconsistency expgen ex (car inc-set)))
    )

    ;(when (= 1 (length best-ex-set)) 
        ;(decf (explanation-cost (first best-ex-set)))
    ;)
    
    (dolist (new-ex best-ex-set)
        ;(incf (explanation-cost new-ex))
        (when *trace-discover-history* 
            (output-refinement new-ex)
        )
    )
    
    (when (> (length best-ex-set) 15)
        ;(break "Lots of explanations at once.")
    )
    
    (remove nil 
        (mapcar+ 
            (embed-expgen #'fix-contradictions expgen) 
            (remove-if #'search-cycling best-ex-set) inc-set
        )
    )
)

(defun get-unambiguous-inconsistencies (expgen ex)
    (remove-if #'(lambda (inc) (inconsistency-remains-ambiguous inc expgen ex))
        (find-inconsistencies expgen ex)
    )
)

(defun find-best-child-explanation-set (expgen ex inc-set &key (max-exes most-positive-fixnum) &aux best-ex-set ex-set best-inc-cost)
    (when (null inc-set)
        (return-from find-best-child-explanation-set t)
    )
    (setq best-ex-set (refine-inconsistency expgen ex (car inc-set) :max-exes max-exes))
    (when (null best-ex-set)
        (return-from find-best-child-explanation-set nil)
    )
    (setq best-inc-cost (inconsistency-choice-cost expgen (car inc-set) ex best-ex-set))
    (dolist (inc (cdr inc-set))
        (setq max-exes (1+ (getf best-inc-cost :ex-count)))
        (setq ex-set (refine-inconsistency expgen ex inc :max-exes max-exes))
        (when (null ex-set)
            (when *trace-discover-history*
                (format t "~%Inconsistency ~A could not be resolved." (inconsistency-brief inc))
            )
            (return-from find-best-child-explanation-set nil)
        )
        (when (new-inconsistency-better (inconsistency-choice-cost expgen inc ex ex-set) best-inc-cost)
            (setq best-ex-set ex-set)
            (setq best-inc-cost (inconsistency-choice-cost expgen inc ex ex-set))
        )
    )
    best-ex-set
)

(defun basic-count (expgen ex ex-set)
    (count (compute-cost expgen ex) ex-set :key (embed-expgen #'compute-cost expgen) :test #'eql)
)

;(defun inconsistency-choice-cost (expgen ex ex-set &aux (tot-increase 0) parent-cost)
;    (when (null ex-set)
;        (return-from inconsistency-choice-cost 0)
;    )
;    (setq parent-cost (compute-cost expgen ex))
;    (dolist (new-ex ex-set)
;        (incf tot-increase (1+ (- (compute-cost expgen new-ex) parent-cost)))
;    )
;    (/ (* (length ex-set) (length ex-set))  tot-increase)
;)

; (defun inconsistency-choice-cost (expgen ex ex-set &aux (tot-increase 0) parent-cost)
    ; (when (null ex-set)
        ; (return-from inconsistency-choice-cost 0)
    ; )
    ; (setq parent-cost (compute-cost expgen ex))
    ; (dolist (new-ex ex-set)
        ; (incf tot-increase (- (compute-heuristic-cost expgen new-ex) parent-cost))
    ; )
    ; tot-increase
; )

(defun inconsistency-choice-cost (expgen inc ex ex-set)
    (declare (ignore expgen ex))
    (list 
     :ex-count (length ex-set)
     :disparity 
     (if (and (inconsistency-prior-occ inc) (inconsistency-next-occ inc)
              (point-later (occ-last-point (inconsistency-prior-occ inc)) (occ-first-point (inconsistency-next-occ inc)))
         )
         ;then
         (- (occ-last-point (inconsistency-prior-occ inc)) (occ-first-point (inconsistency-next-occ inc)))
         ;else
         nil
     )
    )
    ;(- 1000 (apply #'min (mapcar (embed-expgen #'compute-heuristic-cost *expgen*) ex-set)))
)

(defun new-inconsistency-better (new-inc-cost old-inc-cost)
  (cond 
   ((< (getf new-inc-cost :ex-count) (getf old-inc-cost :ex-count)) t)
   ((> (getf new-inc-cost :ex-count) (getf old-inc-cost :ex-count)) nil)
   ((or (null (getf new-inc-cost :disparity)) (null (getf old-inc-cost :disparity)) nil))
   ((< (getf new-inc-cost :disparity) (getf old-inc-cost :disparity)) t)
   (t nil)
  )
)
  

(defun unresolvable-inc (expgen inc)
    (and (null (inconsistency-prior-occ inc))
         (is-legal-assumption expgen (inconsistency-cnd inc))
         (variables-in-cnd (inconsistency-cnd inc))
    )
)

(defun select-inconsistencies (expgen ex orig-inc-set &aux remaining-incs inc-set)
    (let ((resolvable-incs (remove-if #'(lambda (inc) (unresolvable-inc expgen inc)) orig-inc-set)))
        (when (and orig-inc-set (null resolvable-incs))
            (return-from select-inconsistencies orig-inc-set)
        )
        (setq inc-set resolvable-incs)
    )
    (when (null (get-inconsistency-selection-method expgen))
        (return-from select-inconsistencies inc-set)
    )
    (setq remaining-incs inc-set)
    
    ;;Remove ambiguous inconsistencies
    (when (member :prefer-unambiguous (get-inconsistency-selection-method expgen))
        (setq remaining-incs
            (remove-if #'(lambda (inc) (inconsistency-remains-ambiguous inc expgen ex)) inc-set)
        )
        (when (null remaining-incs)
            (setq remaining-incs
                (remove-if #'(lambda (inc) (inconsistency-remains-ambiguous inc expgen ex)) orig-inc-set)
            )
            (when (and (null remaining-incs) (member :ignore-ambiguous (get-inconsistency-selection-method expgen)))
                (return-from select-inconsistencies (values nil inc-set))
            )
        )
    )
    
    ;; :prefer-recent heuristics
    (when (member :prefer-recent (get-inconsistency-selection-method expgen))
        ;; Cut inconsistencies down to those that reference the  
        ;; event/assumption most likely to cause others.
        (setq remaining-incs
            (find-most-recent-inconsistencies expgen ex remaining-incs)
        )
    )
    
    ;; :prefer-causative heuristics
    (when (member :prefer-causative (get-inconsistency-selection-method expgen))
        ;; Cut inconsistencies down to those that reference the  
        ;; event/assumption most likely to cause others.
        (setq remaining-incs
            (find-causative-inconsistencies expgen ex remaining-incs)
        )
    )
    
    ;; :prefer-earliest heuristics
    (when (member :prefer-earliest (get-inconsistency-selection-method expgen))
        ;; Cut inconsistencies down to those that reference the  
        ;; event/assumption most likely to cause others.
        (setq remaining-incs
            (find-earliest-inconsistencies expgen ex remaining-incs)
        )
    )
    
    (values remaining-incs (set-difference inc-set remaining-incs))
)

;;Attempts to remove inconsistencies that don't relate to the cause of the most recently posited event
(defun find-earliest-inconsistencies (expgen ex inc-set 
                                       &aux final-inc-set 
                                            new-inconsistent-occs 
                                            earliest-new-inconsistent-occs)
     (setq new-inconsistent-occs 
        (intersection
            (remove-duplicates
                (remove nil 
                    (append 
                        (mapcar #'inconsistency-prior-occ inc-set)
                        (mapcar #'inconsistency-next-occ inc-set)
                    )
                )
            )
            (explanation-new-events ex)
        )
    )

    (when (some #'occurrence-is-assumption new-inconsistent-occs)
        (setq earliest-new-inconsistent-occs
            (remove-if-not #'occurrence-is-assumption new-inconsistent-occs)
        )

        ;; when an assumption has been added and no inconsistencies reference 
        ;; an assumption, keep only inconsistencies that reference a variable 
        ;; in the last added assumption
        (when (null earliest-new-inconsistent-occs)
            (let ((var-syms
                        (remove-duplicates
                            (remove nil
                                (apply #'append
                                    (mapcar #'get-var-symbols
                                        earliest-new-inconsistent-occs
                                    )
                                )
                            )
                        )
                 ))
                (return-from find-earliest-inconsistencies 
                    (find-inconsistencies-relevant-to-variables inc-set var-syms)
                )
            )
        )
    )

    (when (null earliest-new-inconsistent-occs)
        (setq earliest-new-inconsistent-occs 
            (remove (apply #'min (get-last-point expgen) (mapcar #'occ-first-point new-inconsistent-occs))
                new-inconsistent-occs
                :key #'occ-first-point
                :test-not #'eql
            )
        )
    )
    
    (when (cdr earliest-new-inconsistent-occs)
        (setq earliest-new-inconsistent-occs 
            (remove (apply #'min (mapcar #'var-count earliest-new-inconsistent-occs))
                earliest-new-inconsistent-occs
                :key #'var-count
                :test-not #'eql
            )
        )
    )
    (setq final-inc-set nil)
    (dolist (inc inc-set)
        (when (member (inconsistency-prior-occ inc) earliest-new-inconsistent-occs)
            (push inc final-inc-set)
        )
        (when (member (inconsistency-next-occ inc) earliest-new-inconsistent-occs)
            (push inc final-inc-set)
        )
    )
    (when (null final-inc-set)
        (when new-inconsistent-occs
            (error "Can't be null.")
        )
        (setq final-inc-set inc-set)
    )
    final-inc-set
)

;;Attempts to remove inconsistencies that don't relate to the cause of the most recently posited event
(defun find-most-recent-inconsistencies (expgen ex inc-set 
                                         &aux final-inc-set most-recent-ev)
    (declare (ignore expgen))
    (setq most-recent-ev (explanation-last-added-assumption ex))
    (when (null most-recent-ev)
        (setq most-recent-ev (explanation-last-added-event ex))
    )
    (when (null most-recent-ev)
        (return-from find-most-recent-inconsistencies inc-set)
    )
    
    ;;First work on inconsistencies necessary to make something happen.
    (setq final-inc-set nil)
    (dolist (inc inc-set)
        (when (eq (inconsistency-next-occ inc) most-recent-ev)
            (push inc final-inc-set)
        )
        (when (eq (inconsistency-prior-occ inc) most-recent-ev)
            (push inc final-inc-set)
        )
    )
    (setq final-inc-set
        (union final-inc-set
            (find-inconsistencies-relevant-to-variables
                inc-set (get-var-symbols most-recent-ev)
            )
        )
    )

    final-inc-set
)

;;Attempts to remove inconsistencies that don't relate to the cause of the most recently posited event
(defun find-causative-inconsistencies (expgen ex inc-set 
                                         &aux final-inc-set most-recent-ev)
    (declare (ignore expgen))
    (setq most-recent-ev (explanation-last-added-assumption ex))
    (when (null most-recent-ev)
        (setq most-recent-ev (explanation-last-added-event ex))
    )
    (when (null most-recent-ev)
        (return-from find-causative-inconsistencies inc-set)
    )
    
    ;;First work on inconsistencies necessary to make something happen.
    (setq final-inc-set nil)
    (dolist (inc inc-set)
        (when (eq (inconsistency-next-occ inc) most-recent-ev)
            (push inc final-inc-set)
        )
    )
    (when (null final-inc-set)
        (setq final-inc-set 
            (find-inconsistencies-relevant-to-variables
                inc-set (get-var-symbols most-recent-ev)
            )
        )
    )

    final-inc-set
)

(defun find-inconsistencies-relevant-to-variables (inc-set vars)
    (remove-if-not
        #'(lambda (inc) 
            (intersection (get-var-symbols (inconsistency-cnd inc))
                vars
            )
          )
        inc-set
    )
)

(defun var-count (occ)
    (length (get-var-symbols occ))
)

(defun enumeration-cnd (cnd)
    (and (cnd-is-is cnd) (cnd-value cnd))
)

(defun inconsistency-remains-ambiguous (inc expgen ex &aux ret)
    (when (not (inconsistency-ambiguity inc))
        (return-from inconsistency-remains-ambiguous nil)
    )
    (setq ret (refine-by-binding-next-occ expgen ex inc :max-exes 2))
    (when (cdr ret)
        (return-from inconsistency-remains-ambiguous t)
    )
    (setq ret 
        (append ret
            (refine-by-binding-prior-occ expgen ex inc :max-exes (- 2 (length ret)))
        )
    )
    (return-from inconsistency-remains-ambiguous (cdr ret))
)
    

(defun inconsistency-brief (inc)
    (list  
        (inconsistency-prior-occ inc)
        '>
        (inconsistency-cnd inc)
        '>
        (inconsistency-next-occ inc)
    )
)

(defun output-refinement (ex &aux level lst)
    (setq level (explanation-depth ex))
    (terpri)
    (setq lst (list level (car (explanation-new-events ex))))
    (pprint-logical-block (*standard-output* lst)
        (pprint-indent :current level)
        (write level)
        (write-string ". ")
        (write (explanation-cost ex))
        (write-string ". ")
        (pprint-indent :current 0)
        (write (second lst))
    )
)

(defun choose-inconsistency-to-refine (ex inc-set)
    (declare (ignore ex))
    (first inc-set)
)

;; This method is called when no inconsistencies exist in an explanation, but 
;; more events may be implied. Assumes presence of assumptions, since all 
;; correct explanations must have such.
(defun find-extra-events (expgen ex &aux assumps new-ex new-evs rewind-pt all-events)
    (setq assumps (append (get-assumptions ex) (get-unstable-events expgen ex)))
    (when (and *break-on-no-assumptions* (null assumps) 
               (null (explanation-abandoned-models ex)) 
               (not (member 'event-cycle (explanation-event-removals ex) :key #'occurrence-type))
          )
        (break "An explanation with no inconsistencies should have assumptions or abandoned events.")
    )
    
    (setq rewind-pt
        (apply #'min (get-last-point expgen)
            (mapcar #'occ-first-point 
                (explanation-new-events ex)
            )
        )
    )
    ;(format t "~%Rewind point: ~A" rewind-pt)
    (setq new-ex (copy-explanation ex))
    (setq ex (copy-explanation ex))
    (trim-cache expgen new-ex rewind-pt)
    (when (> rewind-pt 10000)
        (error "Point out of range.")
    )
    (setq new-ex (extend-explanation expgen new-ex assumps))

    (when (eq new-ex 'fail)
        (return-from find-extra-events 'fail)
    )
    
    (setq rewind-pt (get-last-point expgen))

    ;;Old behavior: Add new events to existing explanation
    (setq new-evs (set-difference (get-events new-ex) (get-events ex) :test #'occurrence-equal))
    (setq all-events (get-events ex))
    (dolist (ev new-evs)
        (when (null (get-contradictory-events-in expgen ev all-events))
            (when (point-earlier (occ-first-point ev) rewind-pt)
                (setq rewind-pt (occ-first-point ev))
            )
            (when (not (member ev (explanation-event-removals ex) :test #'occurrence-equal))
                (setf (occurrence-old ev) t)
                (setq ex (add-event expgen ex ev))
            )
        )
    )

    ;;New events must be defeasible, to allow the appearance of new assumptions.
    ;(setf (explanation-new-events ex) (set-difference (explanation-new-events ex) new-evs))
    ;(setf (explanation-prior-events ex) (append new-evs (explanation-prior-events ex)))
    (setf (explanation-prior-vsym-counter ex) (1- *next-symbol-id*))
  
    
    ;;Check for satisfied intervals.
    (dolist (ev (explanation-new-events ex))
        (let ((final-ev 
                (find-if
                    #'(lambda (specific-ev) 
                        (occurrence-as-specific-as specific-ev ev)
                      )
                    (remove ev (get-events ex)) 
                )
             ))
            (when final-ev
                (when (point-earlier (occ-first-point ev) rewind-pt)
                    (setq rewind-pt (occ-first-point ev))
                )
                (when (point-earlier (occ-first-point final-ev) rewind-pt)
                    (setq rewind-pt (occ-first-point final-ev))
                )
                ;; Do the replace in occurrence-precedence.
                (setf (explanation-occurrence-precedence ex)
                    (subst final-ev ev
                        (explanation-occurrence-precedence ex)
                    )
                )
                ;(clrhash (explanation-cached-precedence-map ex))

                (setf (explanation-new-events ex)
                    (adjoin final-ev (remove final-ev (remove ev (explanation-new-events ex))))
                )
                (setf (explanation-prior-events ex)
                    (remove final-ev (explanation-prior-events ex))
                )
            )
        )
    )
    
    (let ((old-precedence (explanation-occurrence-precedence ex)))
        (setf (explanation-occurrence-precedence ex) nil)
        (clrhash (explanation-cached-precedence-map ex))
        (dolist (prec old-precedence)
            (when (and (member (first prec) (get-events ex)) (member (second prec) (get-events ex)))
                (setq ex (constrain-ordering expgen ex (first prec) (second prec) nil :allow-eq (third prec)))
                (when (null ex)
                    (return-from find-extra-events 'fail)
                )
            )
        )
    )
    
    ; (setf (explanation-new-events ex) (remove-duplicates (explanation-new-events ex) :test #'occurrence-equal))
    ; (setf (explanation-occurrence-precedence ex)
        ; (remove-if #'(lambda (prec) (occurrence-equal (first prec) (second prec)))
            ; (explanation-occurrence-precedence ex)
        ; )
    ; )
    
    (trim-cache expgen ex rewind-pt)
    ex
)

;; Returns events present in the explanation which are not currently implied 
;; by the explanation; includes all events with inconsistencies on their 
;; preconditions.
(defun get-unstable-events (expgen ex)
    (remove-duplicates
        (remove-if #'occurrence-is-assumption 
            (remove-if #'occurrence-is-observation 
                (mapcar #'inconsistency-next-occ (find-inconsistencies expgen ex))
            )
        )
    )
)

;; Returns t if an iteration has taken too long, or the search has passed the
;; current maximum cost
(defun should-give-up (expgen ex)
    (or (< 0 (get-cutoff-time expgen) (get-internal-run-time))
        (<= 0
            (get-cost-limit expgen)
            (compute-cost expgen ex)
        )
    )
)

(defun compute-cost (expgen ex)
    (case (get-cost-computation-method expgen)
        (:branch-based (explanation-cost ex))
        (:change-count (length (explanation-history ex)))
        (:heuristic-cost (estimate-cost expgen ex))
        (otherwise (error "Unrecognized method."))
    )
)
; (defun compute-heuristic-cost (expgen ex)
    ; (when (> (explanation-estimated-inconsistency-count ex) 1000)
        ; (error "Inconsistency estimate not computed.")
    ; )
    ; (+ (floor (explanation-estimated-inconsistency-count ex) 2) (compute-cost expgen ex) 1)
; )

; (defun compute-heuristic-cost (expgen ex)
    ; (when (> (explanation-estimated-inconsistency-count ex) 1000)
        ; (error "Inconsistency estimate not computed.")
    ; )
    ; (+ (floor (length (find-inconsistencies expgen ex)) 2) (compute-cost expgen ex) 1)
; )

(defun compute-heuristic-cost (expgen ex)
    (estimate-cost expgen ex)
)

(defun estimate-cost (expgen ex)                                                       
    (+  (length (explanation-history ex))
        (* 2 (explanation-cost ex))
        (apply #'max 0
            (mapcar+ #'calculate-event-interval-score (explanation-new-events ex) expgen)
        )
        (calculate-age-cost expgen ex) 
    )
)

(defun plausibility-cost (expgen ex)                                                       
    (+  (* 10 (count-if #'occurrence-is-assumption (explanation-new-events ex)))
        (calculate-age-cost expgen ex) 
    )
)


(defun estimate-remaining-steps (expgen ex)
    (declare (ignore expgen))
    (+  (length (get-var-symbols (remove-if #'occurrence-old (explanation-new-events ex))))
        (count-if #'occurrence-interval (explanation-new-events ex))
        (- (length (explanation-occurrence-precedence ex)))
    )
)

(defun calculate-event-interval-score (ev expgen)
    (if (occurrence-point ev)
        ;then 
        0
        ;else
        (calculate-interval-score (occurrence-interval ev) expgen)
    )
)

(defun calculate-interval-score (ival expgen &aux obs-count)
    (setq obs-count
        (count-if #'occurrence-is-observation
            (remove-if-not
                #'(lambda (occ)
                    (point-in-interval (occurrence-point occ) ival)
                  )
                (get-execution-history expgen)
            )
        )
    )
    (if (zerop obs-count) 
        0
        (log obs-count 2)
    )
)
(defun calculate-age-cost (expgen ex)
    (calculate-interval-score
        (make-interval
            :range-start 
               (apply #'min (point-next expgen (occurrence-point (get-prior-observation expgen)))
                   (mapcar #'occ-last-point
                       (remove-if #'occurrence-old
                           (explanation-new-events ex)
                       )
                   )
               )
           :range-end
               (occurrence-point (get-prior-observation expgen))
        )
        expgen
    )
)

(defun explanation-set-estimate (expgen ex-set &aux estimates)
    (setq estimates (mapcar #'(lambda (ex) (estimate-remaining-steps expgen ex)) ex-set))
    (+ (apply #'min 1000 estimates) (length estimates))
)

; (defun calculate-age-cost (expgen ex)
    ; (- (occurrence-point (get-prior-observation expgen))
       ; (apply #'min (occurrence-point (get-prior-observation expgen))
           ; (mapcar #'occ-last-point
               ; (remove-if #'occurrence-old
                   ; (explanation-new-events ex)
               ; )
           ; )
       ; )
    ; )
; )

(defun count-involved-events (expgen ex &aux evs)
    (setq evs nil)
    (dolist (inc (find-inconsistencies expgen ex))
        (pushnew (inconsistency-next-occ inc) evs)
        (pushnew (inconsistency-prior-occ inc) evs)
    )
    (length (remove-if #'occurrence-is-observation (remove nil evs)))
)

(defun estimate-inconsistent-events (expgen ex &aux last-incs leftover-incs cur-occ (inc-events nil) (troublesome-incs 0))
    (setq leftover-incs (find-inconsistencies expgen ex))
    (setq last-incs (cons t leftover-incs))
    (loop while (> (length last-incs) (length leftover-incs)) do
        (setq last-incs leftover-incs)
        (setq leftover-incs nil)
        (dolist (inc last-incs)
            (setq cur-occ (get-modifiable-occurrence ex inc inc-events))
            (cond 
                ((occurrence-p cur-occ) (pushnew cur-occ inc-events))
                ((eq cur-occ :two-answers) (push inc leftover-incs))
                ((eq cur-occ :no-answers) (incf troublesome-incs))
            )
        )
    )
    (values inc-events troublesome-incs)
)

(defun estimate-minimum-required-changes (expgen ex)
    (multiple-value-bind (occs leftovers) (estimate-inconsistent-events expgen ex)
        (+ leftovers (length occs))
    )
)

(defun get-modifiable-occurrence (ex inc inc-events &aux poss-events int-events)
    (setq poss-events (list (inconsistency-prior-occ inc) (inconsistency-next-occ inc)))
    (setq poss-events (remove-if #'occurrence-is-observation (remove nil poss-events)))
    (when (null poss-events)
        (return-from get-modifiable-occurrence :no-answers)
    )
    (when (null (cdr poss-events))
        (return-from get-modifiable-occurrence (only poss-events))
    )
    (setq int-events (intersection poss-events inc-events))
    (when int-events
        (return-from get-modifiable-occurrence (first int-events))
    )
    (setq int-events (intersection poss-events (explanation-new-events ex)))
    (cond
        ((null int-events) :unknown)
        ((cdr int-events) :two-answers)
        (t (only int-events))
    )
)    

(defun get-cooccurring-events-in (ev all-events)
    (when (not (occurrence-point ev))
        (return-from get-cooccurring-events-in nil)
    )
    (remove ev
        (remove (occurrence-point ev) all-events 
            :key #'occurrence-point
            :test-not #'eql
        )
    )
)

(defun get-contradictory-events-in (expgen ev all-events &aux co-evs contra-evs)
    (setq co-evs (get-cooccurring-events-in ev all-events))
    (when (occurrence-is-action ev)
        (return-from get-contradictory-events-in co-evs)
    )

    (setq contra-evs (find-contradictory-events ev co-evs #'occurrence-pre))
    (setq contra-evs 
        (remove-duplicates
            (append contra-evs 
                (find-contradictory-events ev co-evs #'occurrence-post)
            )
        )
    )
    ; (setq contra-evs 
        ; (append contra-evs 
            ; (find-contradictory-performed-actions ev co-evs)
        ; )
    ; )
    (dolist (cnd (remove-if #'get-var-symbols (occurrence-constraints ev)))
        (dolist (co-ev co-evs)
            (when (member nil (unify expgen cnd (occurrence-pre co-ev) nil :assume-fn #'assume-nothing))
                (push co-ev contra-evs)
            )
        )
    )
    
    (remove-duplicates contra-evs)
)     


;;This should remove events that have contradictory post- or pre-conditions with
;;new events. If new events contradict one another, will return fail. Otherwise,
;;returns an explanation without contradictory events.

;;Note: This is redundant with new add-event-to-explanation functionality.
(defun fix-contradictions (expgen ex old-incs &aux all-events contra-evs modified remaining-new-events ev)
    (setq modified nil)
    (setq all-events (get-events ex))
    (setq remaining-new-events (explanation-new-events ex))
    (loop while remaining-new-events do
        (setq ev (pop remaining-new-events))
        (setq contra-evs (get-contradictory-events-in expgen ev all-events))
        (dolist (contra-ev contra-evs)
            (setq ex (remove-event expgen ex contra-ev))
            (when (eq ex 'fail)
                (return-from fix-contradictions (values nil :modified))
            )
            (setq modified t)
            (setq remaining-new-events (remove contra-ev remaining-new-events))
            (setq all-events (remove contra-ev all-events))
            (when (member :inconsistency-count (get-heuristic-method expgen))
                (decf (explanation-estimated-inconsistency-count ex)
                    (count contra-ev old-incs :key #'inconsistency-prior-occ)
                )
                (decf (explanation-estimated-inconsistency-count ex)
                    (count contra-ev old-incs :key #'inconsistency-next-occ)
                )
            )
        )
    )
    (dolist (occ (get-execution-history expgen))
        (when (interpretation-causes-contradiction occ (explanation-bindings ex))
            (return-from fix-contradictions (values nil :modified))
        )
    )
    
    (check-for-problem ex)
     
    (values ex (if modified :modified :original))
)

(defun check-for-problem (ex)
    (when (set-difference 
            (append (mapcar #'first (explanation-occurrence-precedence ex)) 
                    (mapcar #'second (explanation-occurrence-precedence ex))
            )
            (get-events ex)
          )
        (break "Extra occurrence in map.")
    )
)

;;This returns all events that have contradictory facts in pre or post. 
;;Condition-fn should be #'occurrence-pre or #'occurrence-post to indicate
;;which applies
(defun find-contradictory-events (ev co-evs condition-fn &aux facts contra-evs)
    (setq contra-evs nil)
    (setq facts (funcall condition-fn ev))
    (dolist (co-ev co-evs)
        (when (intersection (funcall condition-fn co-ev) facts :test #'contradicts)
            ;;There is at least one contradictory literal
            (push co-ev contra-evs)
        )
    )
    contra-evs
)

;; Checks whether explanation bindings cause a contradiction within an occurrence.
(defun interpretation-causes-contradiction (occ blist)
    (and (var-symbols-in occ)
         (or (contradictions-in (mapcar+ #'bind-fact-if-necessary (occurrence-pre occ) blist))
             (contradictions-in (mapcar+ #'bind-fact-if-necessary (occurrence-post occ) blist))
         )
    )
)

;;This returns all events that have contradictory facts in pre or post. 
;;Condition-fn should be #'occurrence-pre or #'occurrence-post to indicate
;;which applies
(defun find-contradictory-performed-actions (ev co-evs &aux contra-evs)
    (when (not (occurrence-is-action ev))
        (return-from find-contradictory-performed-actions nil)
    )
    (setq contra-evs (remove (occurrence-performer ev) co-evs :key #'occurrence-performer :test-not #'eq))
    contra-evs
)

;; Returns the inconsistencies between events in an explanation and actions/
;; observations found in an explanation.
(defun find-inconsistencies (expgen ex &aux occs zero-occs pt facts fact-occs inc-set)
    (when (and (equalp (explanation-valid-fact-cache-pt ex) (get-last-point expgen))
               (equalp (explanation-valid-fact-cache-pt ex) (caar (explanation-cached-inconsistencies ex))))
        (return-from find-inconsistencies (explanation-cached-inconsistency-list ex))
    )
    (setq pt (caar (explanation-cached-inconsistencies ex)))
    (when (null pt) (setq pt (get-first-point expgen)))
    (setq inc-set (apply #'append (mapcar #'cdr (explanation-cached-inconsistencies ex))))
    (when (not (equalp pt (get-last-point expgen)))
        (setq occs (append (get-events ex) (get-execution-history expgen)))
        (setq zero-occs (remove-if-not #'(lambda (occ) (point-is-relevant-to expgen pt occ)) occs))
        (setq occs (remove pt occs :key #'occ-last-point :test-not #'point-earlier))
        (setq fact-occs (cdr (find pt (explanation-cached-fact-occs ex) :key #'car :test-not #'<)))
        (when (null fact-occs)
            (when (not (eql pt (get-first-point expgen)))
                (error "How did fact-occs get to be nil?")
            )
            (setq fact-occs (make-fact-occs))
        )
        (setq facts (cdr (find pt (explanation-cached-facts ex) :key #'car :test-not #'<)))
        (when (null facts)
            (when (not (eql pt (get-first-point expgen)))
                (error "How did facts get to be nil?")
            )
            (setq facts (make-fact-set))
        )
        (multiple-value-bind (first-facts first-fact-occs)
            (advance-facts expgen zero-occs facts fact-occs ex pt)
    
            (setq inc-set 
                (append
                    (find-inconsistencies-recursive-point
                        expgen
                        ex
                        occs
                        (point-next expgen (get-first-point expgen))
                        first-facts
                        first-fact-occs
                    )
                    inc-set
                )
            )
        )
    )
    (setf (explanation-cached-inconsistency-list ex) (remove-later-repeated-inconsistencies inc-set))
)

;; Returns the inconsistencies in an explanation subsequent to a specific 
;; occurrence point.
(defun find-inconsistencies-recursive-point (expgen ex occs pt past-facts fact-occs 
                                             &aux cur-occs inc-set (all-incs nil) last-cur-occs)
  (loop while pt do
    ;(when (null pt)
    ;    (return-from find-inconsistencies-recursive-point nil)
    ;)
    (when (null (explanation-valid-fact-cache-pt ex))
        (setf (explanation-valid-fact-cache-pt ex) 0)
    )
    
    (setq inc-set (assoc pt (explanation-cached-inconsistencies ex) :test #'eql))
    (if inc-set
        ;then
        (setq inc-set (cdr inc-set)
              past-facts nil
              fact-occs nil
        )
        ;else
        (setq inc-set 'fail)
    )
    (when (and pt (point-earlier (explanation-valid-fact-cache-pt ex) (or (caar (explanation-cached-inconsistencies ex)) 0)))
        (error "Finding incs out of order.")
    )
    (when (eq inc-set 'fail)
        (setq last-cur-occs (remove-if-not #'(lambda (occ) (point-is-relevant-to expgen (point-prior expgen pt) occ)) occs))
        (setq cur-occs (remove-if-not #'(lambda (occ) (point-is-relevant-to expgen pt occ)) occs))
        (setq inc-set nil)
        (when (or (neq (length last-cur-occs) (length cur-occs))
                  (notevery #'eq last-cur-occs cur-occs) 
                  (remove pt cur-occs :test-not #'point-equal :key #'occ-last-point)
              )
            (when (or (null past-facts) (null fact-occs))
                (setq past-facts (copy-fact-set (facts-at expgen ex (point-prior expgen pt))))
                (setq fact-occs (copy-fact-occs (fact-occs-at expgen ex (point-prior expgen pt))))
            )
            (when (or (null past-facts) (null fact-occs))
                (error "We need a real value now!")
            )
            (setq inc-set (make-inconsistencies expgen cur-occs past-facts fact-occs ex pt))
            (multiple-value-setq (past-facts fact-occs) 
                (advance-facts expgen cur-occs past-facts fact-occs ex pt)
            )
        )
        (setf (explanation-valid-fact-cache-pt ex) (max pt (explanation-valid-fact-cache-pt ex)))
    )
        
    (setq all-incs (append inc-set all-incs))
    (setq pt (point-next expgen pt))    
  )    
  all-incs      
)

;; Returns an in-order list of intervals which can happen directly after 
(defun find-following-intervals (ex pt &aux ret-ivals last-ivals)
    (setq ret-ivals nil)
    (setq last-ivals 
        (remove pt (explanation-intervals ex) 
                :key #'interval-range-start :test-not #'eq
        )
    )
    (loop while last-ivals do
        (push (car last-ivals) ret-ivals)
        (setq last-ivals 
            (append (cdr last-ivals)
                    (remove (car last-ivals) (explanation-intervals ex) 
                            :key #'interval-range-start :test-not #'eq
                    )
            )
        )
    )
    (reverse ret-ivals)
)

;; Returns the set of inconsistent facts found in a concurrent set of occurrences 
(defun make-inconsistencies (expgen occs past-facts fact-occs ex pt &aux inc-set impossible-occs)
    (when (and occs (eq *break-at-pt* pt))
        (break "Make-inconsistencies at pt ~A." *break-at-pt*)
    )
    (when (null occs)
        (push (list pt) (explanation-cached-inconsistencies ex))
        (return-from make-inconsistencies nil)
    )
    (let ((pair (assoc pt (explanation-cached-inconsistencies ex) :test #'eql)))
        (when pair
            (return-from make-inconsistencies (cdr pair))
        )
    )
    ;;First, find the list of occurrences which are still impossible given their
    ;; preceding state
    ;; MCM: Discovered this prevents binding of variables when occurrences can 
    ;; happen but still have variables. Therefore, removing. 3/22/2016
    ; (when (member :prefer-unambiguous (get-inconsistency-selection-method expgen))
        ; (setq impossible-occs (remove-if-not #'occurrence-is-observation occs))
        ; (dolist (occ (remove-if #'occurrence-is-observation occs))
            ; (when (is-impossible expgen occ :facts past-facts)
                ; (push occ impossible-occs)
            ; )
        ; )
        ; (setq occs impossible-occs)
    ; )
        
    (dolist (occ occs)
        (dolist (fact (occurrence-pre occ))
          (when (and (not (is-trivial-is-fact fact))
                     (not (and (certain expgen fact) (occurrence-is-observation occ)))
                )
            (when (and (occurrence-is-observation occ) (variables-in-fact fact))
                (setq fact (bind-fact fact (explanation-bindings ex)))
            )
            (let ((rel-fact-occs (find-relevant-fact-occs expgen ex (fact-type fact) (fact-args fact) past-facts occ fact-occs pt))
                  confirming-fact-occs
                  disconfirming-fact-occs
                 )
                (declare (ignore confirming-fact-occs))
                (multiple-value-setq (confirming-fact-occs disconfirming-fact-occs)
                    (split fact rel-fact-occs :key #'car :test #'equal-fact)
                )
                (when (and (null rel-fact-occs) (not (has-default-value expgen fact)))
                    (push 
                        (make-inconsistency 
                            :cnd (fact-to-cnd fact) 
                            :prior-occ nil
                            :next-occ occ
                            :ambiguity (unify expgen (fact-to-cnd fact) past-facts nil :assume-fn #'assume-nothing)
                        )
                        inc-set
                    )
                )
                (dolist (fact-occ-pair disconfirming-fact-occs)
                    (when (or
                            (and (explanation-prohibits-var-symbols ex) 
                                 (var-symbol-p (fact-value fact))
                                 (not (member fact rel-fact-occs :key #'car :test #'equal-fact))
                            )
                            (could-contradict fact (car fact-occ-pair))
                          )
                        (push 
                            (make-inconsistency 
                                :cnd (fact-to-cnd fact) 
                                :prior-occ (cdr fact-occ-pair)
                                :next-occ occ
                                :ambiguity (unify expgen (fact-to-cnd fact) past-facts nil :assume-fn #'assume-nothing)
                            )
                            inc-set
                        )
                    )
                )
            )
          )
        )
        (dolist (cnd (occurrence-constraints occ))
            (let ((rel-fact-occs (find-relevant-fact-occs expgen ex (cnd-type cnd) (cnd-args cnd) past-facts occ fact-occs pt))
                  (sat-blist (unify-from-fact-set expgen (negate-condition cnd) past-facts nil :assume-fn #'assume-nothing))
                 )
                (setq rel-fact-occs
                    (remove-if 
                        #'(lambda (fact-occ-pair) 
                            (set-difference (get-var-symbols (car fact-occ-pair)) (get-var-symbols cnd))
                          )
                        rel-fact-occs
                    )
                )
                (dolist (fact-occ-pair rel-fact-occs)
                    (when (unify expgen cnd (list (car fact-occ-pair)) nil :assume-fn #'assume-nothing) 
                        (push 
                            (make-inconsistency 
                                :cnd (negate-condition cnd) 
                                :prior-occ (cdr fact-occ-pair)
                                :next-occ occ
                                :ambiguity sat-blist
                            )
                            inc-set
                        )
                    )
                )
                ;; 6/15/16 Removed final clause of when that passes through conditions satisfied by default.
                (when (and (null rel-fact-occs) (not (member nil sat-blist))) ;(null (unify-from-fact-set expgen (negate-condition cnd) past-facts nil :assume-fn #'default-closed-world-assume-fn))))
                    (push 
                        (make-inconsistency 
                            :cnd (negate-condition cnd) 
                            :prior-occ nil
                            :next-occ occ
                            :ambiguity nil
                        )
                        inc-set
                    )
                )
            )
        )
        (when (occurrence-is-observation occ)
            (let ((ob-hash (new-fact-hash-table)))
                (dolist (fact (occurrence-pre occ))
                    (when (var-symbols-in fact)
                        (setq fact (bind-fact fact (explanation-bindings ex)))
                    )
                    (when (and (is-binary fact) (not (has-default-value expgen fact)))
                        (setf (gethash fact ob-hash) t)
                    )
                )
                (dolist (fact (get-checked-facts (get-last-envmodel expgen) past-facts))
                    (when (and (has-default expgen fact)
                               (not (has-default-value expgen fact))
                               ;(is-observable fact)
                               ;(not (is-internal fact))
                               (if (is-binary fact) 
                                   ;then
                                   (not (gethash fact ob-hash))
                                   ;else
                                   (not (member fact (occurrence-pre occ) :test #'same-map))
                               )
                          )
                        (dolist (prior-occ (get-most-recent-occs fact-occs fact))
                            (push 
                                (make-inconsistency 
                                    :cnd (fact-to-cnd (get-default-fact expgen fact)) 
                                    :prior-occ prior-occ
                                    :next-occ occ
                                    :ambiguity (unify expgen (fact-to-cnd fact) (occurrence-pre occ) nil :assume-fn #'assume-nothing)
                                )
                                inc-set
                            )
                        )
                    )
                )
            )
        )
        ; (when (and (null inc-set) (explanation-prohibits-var-symbols ex) (var-symbols-in (occurrence-signature occ)))
            ; ;;This special problem can occur when a function of two variables 
            ; ;;is constrained, but never used in a fact.
            ; (push
                ; (make-inconsistency 
                    ; :cnd (make-cnd :type 'var-symbol-exists 
                                   ; :args (get-var-symbols (occurrence-signature occ))
                                   ; :value t
                         ; )
                    ; :prior-occ occ
                    ; :next-occ occ
                ; )
                ; inc-set
            ; )
        ; )
    )
    (setq inc-set (remove-if #'(lambda (inc) (is-redundant-internal-inconsistency expgen ex inc pt)) inc-set))
    (push (cons pt inc-set) 
        (explanation-cached-inconsistencies ex)
    )
;    (when inc-set (break "inconsistencies discovered"))
    inc-set
)

(defun find-relevant-fact-occs (expgen ex f-type f-args fact-set new-occ fact-occs pt &aux rel-facts (rel-fact-occs nil) rel-occs)
    (setq rel-facts (find-all-same-map-in-facts f-type f-args fact-set))
    (when (and (null rel-facts) (not (is-hidden-type expgen f-type)))
        (return-from find-relevant-fact-occs 
            (list 
                (cons (make-fact :type f-type :args f-args :value (get-default-value-type expgen f-type))
                    (get-last-observation-before expgen pt)
                )
            )
        )
    )
    (dolist (fact rel-facts)
        (dolist (occ (remove nil (get-most-recent-occs fact-occs fact)))
            (push (cons fact occ) rel-fact-occs)
        )
    )
    (when (null rel-fact-occs)
        (return-from find-relevant-fact-occs (mapcar #'list rel-facts))
    )
    
    (setq rel-occs (remove-duplicates (mapcar #'cdr rel-fact-occs)))
    (dolist (occ rel-occs)
        (when (occurrence-must-precede ex occ new-occ)
            (dolist (between-occ (remove occ rel-occs))
                (when (and (occurrence-must-precede ex occ between-occ)
                           (occurrence-must-precede ex between-occ new-occ))
                    (setq rel-fact-occs
                        (remove occ rel-fact-occs :key #'cdr)
                    )
                )
            )
        )
    )
    rel-fact-occs
)
    
(defun is-redundant-internal-inconsistency (expgen ex inc pt)
    (or (and (occurrence-equal (inconsistency-prior-occ inc) (inconsistency-next-occ inc))
             (not (eq (cnd-type (inconsistency-cnd inc)) 'var-symbol-exists))
        )
        (and (occurrence-interval (inconsistency-next-occ inc))
             (or (null (inconsistency-prior-occ inc))
                 (not (eql (occ-first-point (inconsistency-prior-occ inc))
                           (point-prior expgen pt)
                      )
                 )
             )
             (point-earlier 
                 (interval-range-start 
                     (occurrence-interval (inconsistency-next-occ inc))
                 )
                 pt
             )
        )
        (and (inconsistency-prior-occ inc)
             (occurrence-interval (inconsistency-prior-occ inc))
             (occurrence-interval (inconsistency-next-occ inc))
             (occurrence-must-not-follow ex
                 (inconsistency-next-occ inc) (inconsistency-prior-occ inc)
             )
        )
    )
)

(defun is-trivial-is-fact (fact)
    (and (fact-is-is fact)
         (var-symbol-p (car (fact-args fact)))
         (eq (type-is-is (fact-type fact))
             (var-symbol-return-type (car (fact-args fact)))
         )
    )
)

(defun is-impossible (expgen occ &key (facts nil) &aux (ret nil))
    (dolist (vsym (get-var-symbols occ))
        (setf (var-symbol-temporary vsym) t)
    )
    (setq ret
        (null
            (remove-if #'binds-var-symbols-to-var-symbols
                (unify-set expgen 
                    (append (mapcar #'fact-to-cnd (remove-if #'fact-is-is (occurrence-pre occ)))
                            (mapcar #'negate-condition (occurrence-constraints occ))
                    )
                    facts nil 
                    :assume-fn #'default-closed-world-assume-fn
                    :binds-var-symbols nil
                )
            )
        )
    )
    (dolist (vsym (get-var-symbols occ))
        (setf (var-symbol-temporary vsym) nil)
    )
    ret
)

(defun remove-later-repeated-inconsistencies (inc-set &aux inc)
    (when (null (cdr inc-set))
        (return-from remove-later-repeated-inconsistencies inc-set)
    )
    (setq inc (car inc-set))
    (if (and (occurrence-interval (inconsistency-next-occ inc))
             (member inc (cdr inc-set) :test #'later-repeat-of)
        )
        ;then
        (remove-later-repeated-inconsistencies (cdr inc-set))
        ;else
        (cons inc (remove-later-repeated-inconsistencies (cdr inc-set)))
    )
)

(defun later-repeat-of (later-inc earlier-inc)
    (and (eq (inconsistency-next-occ later-inc) (inconsistency-next-occ earlier-inc))
         (inconsistency-prior-occ later-inc)
         (or (null (inconsistency-prior-occ earlier-inc))
             (point-earlier 
                 (occ-first-point (inconsistency-prior-occ earlier-inc))
                 (occ-first-point (inconsistency-prior-occ later-inc))
             )
         )
         (equal-cnd (inconsistency-cnd later-inc) (inconsistency-cnd earlier-inc))
    )
)
             

(defun get-last-observation-before (expgen pt &optional (hist-ptr (get-execution-history expgen)))
    (when (point-later pt (occurrence-point (car hist-ptr)))
        (let ((result (get-last-observation-before expgen pt (cdr hist-ptr))))
            (when result
                (return-from get-last-observation-before result)
            )
        )
        (when (occurrence-is-observation (car hist-ptr))
            (return-from get-last-observation-before (car hist-ptr))
        )
    )
    (return-from get-last-observation-before nil)
)

(defun refine-inconsistency (expgen ex inc &key (max-exes 1000) &aux ret-exes)
    (setq ret-exes nil)
    (dolist (refine-fun *refinement-methods*)
        (setq ret-exes (append ret-exes (funcall refine-fun expgen ex inc :max-exes (- max-exes (length ret-exes)))))
        (when (>= (length ret-exes) max-exes)
            (return-from refine-inconsistency ret-exes)
        )
    )
    ret-exes
)

;; first method for resolving an inconsistency; enumerates all possible event
;; occurrences and then isolates those which create the necessary fact at the
;; correct time
(defun refine-by-adding-occurrence (expgen ex inc &key (max-exes 1000)
                                    &aux history-ptr last-ob possible-evs new-ex)
    (declare (ignore max-exes))
    (when (variables-in-cnd (inconsistency-cnd inc))
        (return-from refine-by-adding-occurrence nil)
    )
    (setq history-ptr (get-execution-history expgen))
    (setq last-ob (inconsistency-prior-occ inc))
    (when (null last-ob)
        (setq last-ob (car (get-execution-history expgen)))
    )
    
    ;;In some situations, an established value can not be calculated correctly 
    ;;by the domain. This is an attempt to create an out for situations in 
    ;;which observed values differ but can not be predicted by the model. 
    (when (no-event-can-satisfy expgen (inconsistency-cnd inc))
        (when (and (null (cnd-inequality (inconsistency-cnd inc)))
                   (numberp (cnd-value (inconsistency-cnd inc)))
                   (or (occurrence-is-action last-ob)
                       (occurrence-is-observation last-ob)
                   )
                   (occurrence-is-observation (inconsistency-next-occ inc))
                   (eq (inconsistency-next-occ inc) 
                       (second (member (inconsistency-prior-occ inc) (get-execution-history expgen)))
                   )
                   (occurrence-point (inconsistency-next-occ inc))
                   (not (get-learning-allowed expgen))
               )
            (setq new-ex
                (add-event-to-explanation 
                    expgen
                    (make-occurrence 
                        :signature 
                            (list 'real-world-value 
                                (cnd-to-fact (inconsistency-cnd inc))
                            )
                        :point 
                            (point-prior expgen 
                                (occurrence-point 
                                    (inconsistency-next-occ inc)
                                )
                            )
                        :post (list (cnd-to-fact (inconsistency-cnd inc)))
                        :is-event t
                        :is-assumption t
                    )
                    ex
                )
            )
            (when (member :inconsistency-count (get-heuristic-method expgen))
                (setf (explanation-estimated-inconsistency-count new-ex)
                    (1- (length (find-inconsistencies expgen new-ex)))
                )
            )
            (return-from refine-by-adding-occurrence 
                (list (add-to-history new-ex 'refine-by-adding-occurrence inc ex))
            ) 
        )
        (return-from refine-by-adding-occurrence nil)
    )
                
    
    (when (occurrence-is-observation last-ob)
        (setq history-ptr (cdr (member last-ob (get-execution-history expgen))))
    )
    
    (when (not (occurrence-is-observation last-ob))
        (loop while (occurrence-earlier 
                        (car history-ptr) (inconsistency-prior-occ inc)) do
            (when (occurrence-is-observation (car history-ptr))
                (setq last-ob (car history-ptr))
            )
            (setq history-ptr (cdr history-ptr))
        )
    )
    
    (setq possible-evs (get-possible-events expgen last-ob))
    (when (inconsistency-prior-occ inc)
        (if (satisfies-condition expgen (inconsistency-cnd inc) (inconsistency-prior-occ inc))
            ;then (simulataneous events can't fix)
            (setq possible-evs 
                (remove (inconsistency-prior-occ inc) possible-evs 
                    :test-not #'occurrence-may-be-earlier
                )
            )
            ;else (simultaneous events can fix)
            (setq possible-evs 
                (remove (inconsistency-prior-occ inc) possible-evs 
                    :test #'occurrence-later
                )
            )
        )
    )
    
    (loop while (and history-ptr 
                    (occurrence-may-be-earlier 
                        (car history-ptr) 
                        (inconsistency-next-occ inc))) do
        (when (occurrence-is-observation (car history-ptr))
            (setq last-ob (car history-ptr))
            (push-all (get-possible-events expgen last-ob) possible-evs)
        )
        (setq history-ptr (cdr history-ptr))
    )
    
  
    (setq possible-evs 
        (remove (inconsistency-next-occ inc) possible-evs
            :test-not #'occurrence-may-be-later
        )
    )
    
    ;(break "Before adapt-explanation.")
    
    (mapcar+ #'add-to-history
        (mapcar+
            #'estimate-added-event-inconsistencies
            (apply #'append
                (cons nil
                    (mapcar+ #'adapt-explanation possible-evs expgen ex inc)
                )
            )
            expgen 
            (find-inconsistencies expgen ex)
        )
        'refine-by-adding-occurrence inc ex
    )
)

(defun estimate-added-event-inconsistencies (ex expgen old-incs &aux ev)
    (when (member :inconsistency-count (get-heuristic-method expgen))
        (setq ev (car (explanation-new-events ex)))
        (setf (explanation-estimated-inconsistency-count ex)
            (+ (length 
                    (union
                        (remove ev old-incs :key #'inconsistency-next-occ :test-not #'occurrence-may-be-later)
                        (remove ev 
                            (remove-if-not #'inconsistency-prior-occ old-incs)
                            :key #'inconsistency-prior-occ :test-not #'occurrence-may-be-earlier
                        )
                    )
               )
            )
        )
    )
    ex
)

(defun estimate-removed-event-inconsistencies (ex expgen occ old-incs)
    (when (member :inconsistency-count (get-heuristic-method expgen))
        (setf (explanation-estimated-inconsistency-count ex)
            (length
               (remove occ 
                   (remove occ 
                       old-incs 
                       :key #'inconsistency-prior-occ
                   )
                   :key #'inconsistency-next-occ
               )
            )
        )
    )
    ex
)


;; second method for resolving an inconsistency: removes an event from the 
;; explanation. If one of the ends of the inconsistency is an observation, an
;; action, or a new event, it will not generate a new explanation. Calling 
;; conditions should prevent occ from being removed already. 
(defun refine-by-removing-prior-occurrence (expgen ex inc &key (max-exes 1000) &aux occ ret-exes)
    (declare (ignore max-exes))
;    (when (variables-in-cnd (inconsistency-cnd inc))
;        (return-from refine-by-removing-prior-occurrence nil)
;    )
    (setq occ (inconsistency-prior-occ inc))
    (when (null occ)
        (return-from refine-by-removing-prior-occurrence nil)
    )
    (when (member occ (explanation-event-removals ex) :test #'occurrence-equal)
        (error "This should not be present in an inconsistency.")
    )
    (when (or (not (occurrence-old occ)) (occurrence-is-observation occ)
              (eq (occurrence-performer occ) (get-self expgen)))
        (return-from refine-by-removing-prior-occurrence nil)
    )
    (setq ret-exes
        (mapcar+ (embed-expgen #'remove-event-from-explanation expgen) 
            (if (occurrence-is-action occ)
                ;then
                (list :action)
                ;else
                (remove (inconsistency-cnd inc)
                    (append
                        (mapcar #'fact-to-cnd 
                            (remove-if-not (embed-expgen #'is-transient expgen) (occurrence-pre occ))
                        )
                        (mapcar #'negate-condition
                            (occurrence-constraints occ)
                        )
                    )
                    :test #'equal-cnd
                )
            )
            occ ex
        )
    )
    (when (and ret-exes (member :inconsistency-count (get-heuristic-method expgen))) 
        (setq ret-exes
            (mapcar+ #'estimate-removed-event-inconsistencies ret-exes
                expgen occ (find-inconsistencies expgen ex)
            )
        )
    )
    (setq ret-exes
        (mapcar+ #'add-to-history ret-exes
            'refine-by-removing-prior-occurrence inc ex
        )
    )
    (dolist (ex ret-exes)
        (incf (explanation-cost ex) 6)
    )
    ret-exes
)

;; When removing the occurrence afterward, either the fact in the occurrence 
;; should not be true at that time, or there is another (possibly complementary)
;; explanation why the fact is not true. Therefore, only a removal that 
;; specifies the inconsistent fact as the reason for removal is necessary.
(defun refine-by-removing-next-occurrence (expgen ex inc &key (max-exes 1000) &aux occ)
    (declare (ignore max-exes))
    (when (variables-in-cnd (inconsistency-cnd inc))
        (return-from refine-by-removing-next-occurrence nil)
    )
    (setq occ (inconsistency-next-occ inc))
    (when (null occ)
        (return-from refine-by-removing-next-occurrence nil)
    )
    (when (member occ (explanation-event-removals ex) :test #'occurrence-equal)
        (error "This should not be present in an inconsistency.")
    )
    (when (or (not (occurrence-old occ)) (occurrence-is-observation occ)
              (eq (occurrence-performer occ) (get-self expgen)))
        (return-from refine-by-removing-next-occurrence nil)
    )
    (list
        (add-to-history
            (estimate-removed-event-inconsistencies
                (remove-event-from-explanation 
                    expgen
                    (inconsistency-cnd inc)
                    occ ex
                )
                expgen occ
                (find-inconsistencies expgen ex)
            )
            'refine-by-removing-next-occurrence inc ex
        )
    )
)


;; third and final method for resolving an inconsistency: adds a special 
;; initial-value event at point0 to explore the hypothesis that a hidden
;; fact was true initially.
(defun refine-by-hypothesizing-initial-value (expgen ex inc &key (max-exes 1000) &aux cnd new-ex new-occ)
    (declare (ignore max-exes))
    (setq cnd (inconsistency-cnd inc))
    (when (eq (cnd-type cnd) '_failure)
        (let ((ret nil))
            (dolist (cnd (generate-assumable-conditions expgen))
                (push
                    (add-event
                        expgen
                        ex
                        (make-initial-assumption-occurrence expgen cnd)
                    )
                    ret
                )
            )
            (when (member :inconsistency-count (get-heuristic-method expgen))
                (setq ret
                    (mapcar+ #'set-inconsistency-estimate 
                        ret
                        (length 
                            (remove (inconsistency-cnd inc)
                                (find-inconsistencies expgen ex)
                                :key #'inconsistency-cnd
                                :test #'equal-cnd
                            )
                        )
                    )
                )
            )
            (return-from refine-by-hypothesizing-initial-value 
                (mapcar+ #'add-to-history ret
                    'refine-by-hypothesizing-initial-value inc ex
                )
            )
        )
    )
    (when (and (null (inconsistency-prior-occ inc)) 
             ;; make-inconsistencies should set this when no fact exists
             (is-legal-assumption expgen cnd)
             ;(not (variables-in-cnd cnd))
        )
        (setq new-occ (make-initial-assumption-occurrence expgen cnd))
        (setq new-ex (add-event expgen ex new-occ))
        (setf (explanation-last-added-event new-ex) nil)
        (setf (explanation-last-added-assumption new-ex) new-occ)
        (incf (explanation-cost new-ex) 5)
        (when (member :inconsistency-count (get-heuristic-method expgen))
            (setf (explanation-estimated-inconsistency-count new-ex)
                (length 
                    (remove (inconsistency-cnd inc)
                        (find-inconsistencies expgen ex)
                        :key #'inconsistency-cnd
                        :test #'equal-cnd
                    )
                )
            )
        )
        (return-from refine-by-hypothesizing-initial-value
            (list
                (add-to-history new-ex
                    'refine-by-hypothesizing-initial-value inc ex
                )
            )
        )
    )
    nil
)

(defun refine-by-calculation (expgen ex inc &key (max-exes 1000) &aux (blist nil) eqn)
    (declare (ignore max-exes))
    (dolist (var (get-var-symbols (inconsistency-cnd inc)))
        (setq eqn (sublis (explanation-bindings ex) (var-symbol-eqn var)))
        (when (and eqn (not (names-in eqn)))
            (handler-case 
                (push (cons var (eval eqn)) blist)
                (arithmetic-error () (return-from refine-by-calculation nil))
                (division-by-zero () (return-from refine-by-calculation nil))
                (error (err) 
                    (if (search "NaN" (format nil "~A" err))
                        ;then
                        (return-from refine-by-calculation nil)
                        ;else
                        (signal err)
                    )
                )
            )
        )
    )
    (when blist (error "Calculation should be impossible."))
    (if blist
        ;then
        (handler-case 
            (remove nil 
                (list 
                    (add-to-history
                        (estimate-bound-inconsistencies
                            (bind-explanation expgen ex inc blist)
                            expgen blist 
                            (find-inconsistencies expgen ex)
                        )
                        'refine-by-calculation
                        inc ex
                    )
                )
            )
            (error (err) 
                (if (search "NaN" (format nil "~A" err))
                    ;then
                    (return-from refine-by-calculation nil)
                    ;else
                    (signal err)
                )
            )
        )
        ;else
        nil
    )
)

(defun refine-by-moving-interval-start (expgen ex inc &key (max-exes 1000) &aux new-ex new-occ)
    (declare (ignore max-exes))
    (when (or (null (inconsistency-prior-occ inc))
              (not (occurrence-interval (inconsistency-prior-occ inc)))
              (occurrence-interval (inconsistency-next-occ inc))
              (point-earlier (occ-last-point (inconsistency-prior-occ inc))
                             (occ-last-point (inconsistency-next-occ inc))
              )
          )
        (return-from refine-by-moving-interval-start nil)
    )
    (multiple-value-setq (new-ex new-occ) 
        (replace-interval expgen ex (inconsistency-prior-occ inc) 
            (occ-last-point (inconsistency-next-occ inc))
            (occ-last-point (inconsistency-prior-occ inc))
        )
    )
    (when (member :inconsistency-count (get-heuristic-method expgen))
        (setf (explanation-estimated-inconsistency-count new-ex)
            (length
                (remove-shrunk-interval-inconsistencies
                    expgen new-occ (inconsistency-prior-occ inc) 
                    (find-inconsistencies expgen ex)
                )
            )
        )
    )
    (setq new-ex
        (add-to-history new-ex 'refine-by-moving-interval-start inc ex)
    )
    (list new-ex)
)

(defun refine-by-moving-interval-end (expgen ex inc &key (max-exes 1000) &aux new-ex new-occ)
    (declare (ignore max-exes))
    (when (or (null (inconsistency-prior-occ inc))
              (not (occurrence-interval (inconsistency-next-occ inc)))
              (occurrence-interval (inconsistency-prior-occ inc))
              (point-earlier (occ-first-point (inconsistency-prior-occ inc))
                             (occ-first-point (inconsistency-next-occ inc))
              )
          )
        (return-from refine-by-moving-interval-end nil)
    )
    (multiple-value-setq (new-ex new-occ) 
        (replace-interval expgen ex (inconsistency-next-occ inc) 
            (occ-first-point (inconsistency-next-occ inc))
            (occ-first-point (inconsistency-prior-occ inc))
        )
    )
    (when (member :inconsistency-count (get-heuristic-method expgen))
        (setf (explanation-estimated-inconsistency-count new-ex)
            (length
                (remove-shrunk-interval-inconsistencies
                    expgen new-occ (inconsistency-next-occ inc) 
                    (find-inconsistencies expgen ex)
                )
            )
        )
    )
    (setq new-ex
        (add-to-history new-ex 'refine-by-moving-interval-end inc ex)
    )
    (list new-ex)
)

(defun refine-by-binding-prior-occ (expgen ex inc &key (max-exes 1000)
                                        &aux blists new-ex 
                                             ret-exes old-incs facts)
    (declare (ignore max-exes))
    (when (refine-by-calculation expgen ex inc)
        (return-from refine-by-binding-prior-occ nil)
    )
    (when (null (inconsistency-prior-occ inc))
        (return-from refine-by-binding-prior-occ nil)
    )        
    (when (occurrence-is-observation (inconsistency-prior-occ inc))
        (return-from refine-by-binding-prior-occ nil)
    )
    ;;There are two possibilities: (1) an effect of the prior occ is variable 
    ;; and not observed later, which produces an inconsistency, or (2) an 
    ;; effect of the prior-occ is observed later with a specialized value, 
    ;; which is an inconsistency. In case 1, the arguments must have a variable,
    ;; and the effect is negated in the condition. In case 2, the value must be 
    ;; a variable, a specific value of which is in the condition.
    
    ;;If case 2 is met, the inconsistent condition should unify with an effect.
    (setq blists nil)
    (when (var-symbol-p (cnd-value (inconsistency-cnd inc)))
        (setq blists 
            (unify expgen
                (inconsistency-cnd inc)
                (occurrence-post (inconsistency-prior-occ inc))
                nil
                :assume-fn #'assume-nothing
            )
        )
    )
    
    (when blists
        (let (inc-pt alt-blists)
            (setq inc-pt
                (point-prior expgen 
                    (occ-first-point (inconsistency-next-occ inc))
                )
            )
            (when (and (inconsistency-prior-occ inc) 
                       (point-earlier inc-pt (occ-first-point (inconsistency-prior-occ inc)))
                  )
                (setq inc-pt (occ-first-point (inconsistency-prior-occ inc)))
            )
            (setq alt-blists
                (unify expgen
                    (inconsistency-cnd inc) 
                    (facts-at expgen ex inc-pt)
                    nil
                    :assume-fn #'assume-nothing
                )
            )
            (when (set-difference blists alt-blists :test #'equivalent-bindings)
            ;    (break "binding-prior-occ case 2 is useful!")
            )
        )
        (return-from refine-by-binding-prior-occ nil)
    )
        
    
    ;; If case 1 is met, case 2 will not produce results, and the next-occ must 
    ;; be an observation (in order to have a closed world assumption which shuts
    ;; out the effect). 
    (when (and (null blists) (occurrence-is-observation (inconsistency-next-occ inc)))
        (setq facts 
            (mapcar+ #'bind-fact-if-necessary 
                (remove (cnd-type (inconsistency-cnd inc)) 
                    (occurrence-pre (inconsistency-next-occ inc))
                    :key #'fact-type :test-not #'eq
                )
                (explanation-bindings ex)
            )
        )
        (setq blists 
            (unify expgen (negate-condition (inconsistency-cnd inc)) facts nil
                :assume-fn #'assume-nothing
            )
        )
    )
        
    
    (dolist (blist blists)
        (when (null blist)
            (error "Why is there an inconsistency if there's a trivial unification with the inconsistent condition?")
        )
        (setq new-ex (bind-explanation expgen ex inc blist))
        (when new-ex
            (when (null old-incs) 
                (setq old-incs (find-inconsistencies expgen ex))
            )
            
            (setq new-ex 
                (estimate-bound-inconsistencies
                    new-ex expgen blist old-incs
                )
            )
            (push new-ex ret-exes)
        )
    )
    (mapcar+ #'add-to-history ret-exes 'refine-by-binding-prior-occ inc ex)
)

;; This function works when an event that sets a needed fact occurs during the
;; interval of another event that overwrites that effect, such that the interval 
;; of the interval event can be shrunk, exposing the desired effect. 
(defun refine-by-uncovering-overwritten-effect (expgen ex inc &key (max-exes 1000)
                                                &aux new-ex old-incs ret-exes
                                                     new-incs evs
                                               )
    (setq evs
        (remove-if-not 
            #'(lambda (occ)
                (or (occ-overlaps-with (inconsistency-next-occ inc) occ)
                    (and (inconsistency-prior-occ inc)
                         (occ-overlaps-with (inconsistency-prior-occ inc) occ)
                    )
                )
              )
            (append (get-events ex) (get-execution-history expgen))
        )
    )
    (setq evs (remove (inconsistency-prior-occ inc) evs))
    (setq evs (remove (inconsistency-next-occ inc) evs))
    (setq evs (sort evs #'occurrence-earlier))

    (dolist (occ evs)
        (dolist (blist
                    (unify expgen 
                        (inconsistency-cnd inc) 
                        (occurrence-post occ)
                        nil
                        :assume-fn #'assume-nothing
                    )
                )
            (when 
              (unify expgen 
                  (inconsistency-cnd inc) 
                  (facts-at expgen ex (occ-first-point occ))
                  blist
                  :assume-fn #'assume-nothing
              )
                (setq new-ex ex)
                (setq new-incs old-incs)
                (when (inconsistency-prior-occ inc)
                    (multiple-value-setq (new-ex new-incs)
                        (constrain-ordering expgen ex 
                            (inconsistency-prior-occ inc)
                            occ
                            old-incs
                        )
                    )
                )
                (when new-ex
                    (multiple-value-setq (new-ex new-incs)
                        (constrain-ordering expgen new-ex 
                            occ
                            (inconsistency-next-occ inc)
                            new-incs
                        )
                    )
                )
                (when new-ex
                    (setq new-ex (bind-explanation expgen new-ex inc blist))
                )
                (when (eq ex new-ex)
                    (error "Inconsistency resolution leads to same explanation.")
                )
                (when new-ex 
                    (push (add-to-history new-ex 'refine-by-uncovering-overwritten-effect inc ex)
                        ret-exes
                    )
                )
                (when (>= (length ret-exes) max-exes)
                ;(when (and (< max-exes 5) (>= (length ret-exes) max-exes))
                    (return-from refine-by-uncovering-overwritten-effect ret-exes)
                )
            )
        )
    )
    ; (when (> (length ret-exes) 5)
        ; (let ((quartile (floor (length ret-exes) 4)))
            ; (setq ret-exes
                ; (list
                    ; (car (last ret-exes))
                    ; (nth (* 3 quartile) ret-exes)
                    ; (nth (* 2 quartile) ret-exes)
                    ; (nth quartile ret-exes)
                    ; (first ret-exes)
                ; )
            ; )
        ; )
    ; )
    ret-exes
)

(defun constrain-ordering (expgen ex earlier-occ later-occ incs &key (allow-eq nil) &aux new-ex new-occ)
    (setq earlier-occ (update-occurrence-reference earlier-occ ex))
    (setq later-occ (update-occurrence-reference later-occ ex))
    (when (point-earlier (occ-last-point earlier-occ) (occ-first-point later-occ))
        (return-from constrain-ordering ex)
    )
        
    (when (point-earlier (occ-last-point later-occ) (occ-first-point earlier-occ))
        (return-from constrain-ordering nil)
    )
    
    (when (and allow-eq (or (occurrence-is-action earlier-occ) (occurrence-is-action later-occ)))
        (setq allow-eq nil)
    )

    (when (and (occurrence-point earlier-occ) (occurrence-point later-occ))
        ;;i.e., same exact point
        (if allow-eq
            ;then
            (return-from constrain-ordering ex)
            ;else
            (return-from constrain-ordering nil)
        )
    )
    
    (if allow-eq
        ;then
        (when (occurrence-must-precede ex later-occ earlier-occ)
            ;;i.e., already constrained other way.
            (return-from constrain-ordering nil)
        )
        ;else
        (when (occurrence-must-not-follow ex later-occ earlier-occ)
            ;;i.e., already constrained other way.
            (return-from constrain-ordering nil)
        )
    )
    (when (occurrence-must-precede ex earlier-occ later-occ)
        ;;i.e., already constrained
        (return-from constrain-ordering ex)
    )

    ; (when (occurrence-point earlier-occ) ;and not (occurrence-point later-occ))
        ; ;; earlier occ during later's interval
        ; (return-from constrain-ordering
            ; (replace-interval
                ; expgen ex later-occ
                ; (point-next expgen (occurrence-point earlier-occ))
                ; (occ-last-point later-occ)
            ; )
        ; )
    ; )
; 
    ; (when (occurrence-point later-occ) ;and not (occurrence-point earlier-occ))
        ; ;; later occ during earlier's interval
        ; (return-from constrain-ordering
            ; (replace-interval
                ; expgen ex earlier-occ
                ; (occ-first-point earlier-occ) 
                ; (point-prior expgen (occurrence-point later-occ))
            ; )
        ; )
    ; )
    
    ;; Two overlapping intervals
    (setq new-ex ex)
    (let (first-allowed-later)
        (if allow-eq
            ;then
            (setq first-allowed-later (occ-first-point earlier-occ))
            ;else
            (setq first-allowed-later (point-next expgen (occ-first-point earlier-occ)))
        )
            
        (when (point-earlier (occ-first-point later-occ) first-allowed-later)
            (multiple-value-setq (new-ex new-occ) 
                (replace-interval
                    expgen new-ex later-occ
                    first-allowed-later
                    (occ-last-point later-occ)
                )
            )
            (when (null new-ex) (return-from constrain-ordering nil))
            (setq incs
                (remove-shrunk-interval-inconsistencies
                    expgen new-occ later-occ incs
                )
            )
            (setq later-occ new-occ)
            (setq earlier-occ (update-occurrence-reference earlier-occ new-ex))
        )
    )
    
    (let (last-allowed-earlier)
        (if allow-eq
            ;then
            (setq last-allowed-earlier (occ-last-point later-occ))
            ;else
            (setq last-allowed-earlier (point-prior expgen (occ-last-point later-occ)))
        )
        
        (when (point-earlier last-allowed-earlier (occ-last-point earlier-occ))
            (multiple-value-setq (new-ex new-occ) 
                (replace-interval
                    expgen new-ex earlier-occ
                    (occ-first-point earlier-occ) 
                    last-allowed-earlier
                )
            )
            (when (null new-ex) (return-from constrain-ordering nil))
            (setq incs
                (remove-shrunk-interval-inconsistencies
                    expgen new-occ earlier-occ incs
                )
            )
            (setq earlier-occ new-occ)
            (setq later-occ (update-occurrence-reference later-occ new-ex))
        )
    )
    
    (when (and (occurrence-interval earlier-occ) (occurrence-interval later-occ))
        (when (eq ex new-ex)
            (setq new-ex (copy-explanation ex))
        )
        (push (list earlier-occ later-occ allow-eq) (explanation-occurrence-precedence new-ex))
        ;(clrhash (explanation-cached-precedence-map new-ex))
        (trim-cache expgen new-ex (occ-first-point later-occ))
    )
    (when (member :inconsistency-count (get-heuristic-method expgen))
        (setf (explanation-estimated-inconsistency-count new-ex)
            (length incs)
        )
    )
    (values new-ex incs)
)

;; This returns an occurrence in the explanation that is equal to or more 
;; constrained than occ; useful when a lot of constraints are being generated
;; iteratively.
(defun update-occurrence-reference (occ ex)
    (if (or (member occ (get-events ex)) (occurrence-point occ))
        ;then
        occ
        ;else
        (find occ (get-events ex) :test #'occurrence-as-general-as) 
    )
)

;; This function works by finding a value in a prior state to bind the 
;; inconsistent condition to. Since the inconsistent condition, by 
;; definition, is not met in the prior state, this can only happen by 
;; changing a variable symbol to match something that exists in a prior 
;; state, or reducing the interval size, both of which change the next-occ
;; of the inconsistency. This function does not change the prior occ.
(defun refine-by-binding-next-occ (expgen ex inc &key (max-exes 1000)
                                        &aux blists new-ex 
                                             ret-exes old-incs inc-pt arg-syms
                                             prior-facts fact-occs)
    (when (refine-by-calculation expgen ex inc)
        (return-from refine-by-binding-next-occ nil)
    )

    ;(when (not (variables-in-cnd (inconsistency-cnd inc)))
        ;(return-from refine-by-binding-next-occ nil)
    ;)
    
    (setq inc-pt
        (point-prior expgen 
            (occ-first-point (inconsistency-next-occ inc))
        )
    )
    (when (and (inconsistency-prior-occ inc) 
               (point-earlier inc-pt (occ-first-point (inconsistency-prior-occ inc)))
          )
        (setq inc-pt (occ-first-point (inconsistency-prior-occ inc)))
    )
    
    (setq prior-facts (facts-at expgen ex inc-pt))
    (setq blists 
        (unify expgen
            (inconsistency-cnd inc)
            prior-facts
            nil
            :assume-fn #'assume-default-observables
            :binds-var-symbols t
        )
    )
    ; (setq blists 
        ; (unify-set expgen
            ; (mapcar #'fact-to-cnd (occurrence-pre (inconsistency-next-occ inc)))
            ; prior-facts
            ; nil
            ; :assume-fn #'default-closed-world-assume-fn
            ; :binds-var-symbols t
        ; )
    ; )
    
    (setq arg-syms (remove-if-not #'var-symbol-p (cnd-args (inconsistency-cnd inc))))
    (setq fact-occs (fact-occs-at expgen ex inc-pt))
    ;(setq blists (remove-if #'only-binds-var-symbols-to-var-symbols blists))
    (dolist (blist (remove nil blists))
        (setq new-ex ex)
        (when (and (inconsistency-prior-occ inc) (not (occurrence-is-observation (inconsistency-prior-occ inc))) (subsetp (mapcar #'car blist) arg-syms))
            (let* ((new-fact (car (bind-condition-to-fact expgen (inconsistency-cnd inc) blist prior-facts)))
                   (between-occ (car (get-most-recent-occs fact-occs new-fact))))
                (when (eq between-occ (inconsistency-prior-occ inc))
                    (setq new-ex nil)
                )
                (when new-ex
                    (setq new-ex 
                        (constrain-ordering expgen new-ex 
                            (inconsistency-prior-occ inc)
                            between-occ
                            nil
                        )
                    )
                )
                (when new-ex
                    (setq new-ex 
                        (constrain-ordering expgen new-ex 
                            between-occ
                            (inconsistency-next-occ inc)
                            nil
                        )
                    )
                )
            )
        )
        (when new-ex
            (setq new-ex (bind-explanation expgen new-ex inc blist))
        )
        
        (when (and new-ex (member :inconsistency-count (get-heuristic-method expgen)))
            (when (null old-incs) 
                (setq old-incs (find-inconsistencies expgen ex))
            )
            
            (setq new-ex 
                (estimate-bound-inconsistencies
                    new-ex expgen blist old-incs
                )
            )
        )
        (when new-ex
            (push new-ex ret-exes)
        )
        (when (>= (length ret-exes) max-exes)
            (return-from refine-by-binding-next-occ ret-exes)
        )
    )
    (mapcar+ #'add-to-history ret-exes 'refine-by-binding-next-occ inc ex)
)

;; This function works by finding a value in a prior state to bind the 
;; inconsistent condition to. Since the inconsistent condition, by 
;; definition, is not met in the prior state, this can only happen by 
;; changing a variable symbol to match something that exists in a prior 
;; state, or reducing the interval size, both of which change the next-occ
;; of the inconsistency. This function does not change the prior occ.
; (defun refine-by-uncovering-precondition (expgen ex inc 
                                          ; &aux blists pt new-ex last-blists 
                                               ; ret-exes old-incs new-occ)
    ; (when (refine-by-calculation expgen ex inc)
        ; (return-from refine-by-uncovering-precondition nil)
    ; )
; 
    ; (when (occurrence-point (inconsistency-next-occ inc))
        ; (return-from refine-by-uncovering-precondition nil)
    ; )
    ; (setq pt (point-next expgen (occ-first-point (inconsistency-next-occ inc))))
    ; (setq last-blists nil)
    ; (when (and (inconsistency-prior-occ inc)
               ; (not (point-earlier (occ-first-point (inconsistency-prior-occ inc)) pt))
          ; )
        ; (setq pt (point-next expgen (occ-first-point (inconsistency-prior-occ inc))))
        ; (when (point-earlier (occ-last-point (inconsistency-next-occ inc)) pt)
            ; (error "Malformed inconsistency ~A" inc)
        ; )
    ; )
    ; (loop while (and pt (not (point-earlier (occ-last-point (inconsistency-next-occ inc)) pt))) do
        ; (setq blists 
            ; (unify 
                ; (inconsistency-cnd inc) 
                ; (apply #'append 
                    ; (mapcar #'occurrence-post 
                        ; (remove (inconsistency-next-occ inc)
                            ; (remove-if-not 
                                ; #'(lambda (occ) (point-is-relevant-to expgen (point-prior expgen pt) occ))
                                ; (append (get-events ex) (get-execution-history expgen))
                            ; )
                        ; )
                    ; )
                ; )
                ; nil
                ; :assume-fn #'assume-nothing
            ; )
        ; )
        ; ;(setq blists (remove-if #'only-binds-var-symbols-to-var-symbols blists))
        ; (dolist (blist (set-difference blists last-blists :test #'equivalent-binding-set))
            ; ;; we remove the blists from before because they've already been 
            ; ;; accounted for.
            ; (setq new-occ nil)
            ; (multiple-value-setq (new-ex new-occ)
                ; (replace-interval
                    ; expgen ex (inconsistency-next-occ inc) pt 
                    ; (occ-last-point (inconsistency-next-occ inc))
                ; )
            ; )
            ; (when blist ;; i.e., the explanation must be bound.
                ; (setq new-ex (bind-explanation expgen new-ex inc blist))
            ; )
            ; 
            ; (when new-ex
                ; (when (null old-incs) 
                    ; (setq old-incs (find-inconsistencies expgen ex))
                ; )
                ; 
                ; (setq new-ex 
                    ; (estimate-bound-inconsistencies
                        ; new-ex
                        ; expgen blist
                        ; (remove-shrunk-interval-inconsistencies
                            ; expgen new-occ (inconsistency-next-occ inc) 
                            ; old-incs
                        ; )
                    ; )
                ; )
                ; (push new-ex ret-exes)
            ; )
        ; )
        ; 
        ; (setq pt (point-next expgen pt))
        ; (setq last-blists blists)
    ; )
    ; (mapcar+ #'add-to-history ret-exes 'refine-by-uncovering-precondition inc ex)
; )


;; This removes from an inconsistency list any inconsistency that might be 
;; fixed by delaying the start of the interval of a particular occurrence
(defun remove-shrunk-interval-inconsistencies (expgen new-occ changed-occ prior-incs)
    (declare (ignore expgen))
    (remove-if 
        #'(lambda (inc)
            (or
                (and (eq (inconsistency-prior-occ inc) changed-occ)
                    (or 
                        (point-earlier 
                            (occ-last-point (inconsistency-next-occ inc))
                            (occ-first-point new-occ)
                        )
                        (point-earlier 
                            (occ-last-point new-occ)
                            (occ-last-point changed-occ)
                        )
                    )
                )
                (and (eq (inconsistency-next-occ inc) changed-occ)
                    (or 
                        (and
                            (inconsistency-prior-occ inc)
                            (point-earlier 
                                (occ-last-point new-occ)
                                (occ-first-point (inconsistency-prior-occ inc))
                            )
                        )
                        (point-earlier 
                            (occ-first-point changed-occ)
                            (occ-first-point new-occ)
                        )
                    )
                )
            )
          )
        prior-incs
    )
)

(defun set-inconsistency-estimate (ex estimate)
    (setf (explanation-estimated-inconsistency-count ex)
        estimate
    )
    ex
)

(defun refine-by-reversing-order (expgen ex inc &key (max-exes 1000) &aux new-ex)
    (declare (ignore max-exes))
    (when (null (inconsistency-prior-occ inc))
        (return-from refine-by-reversing-order nil)
    )
    ;; Matt (12/8/2014) Theorizing that reversing order is a secondary concern 
    ;; to binding variable symbols, and that any problem can be resolved by 
    ;; binding variable symbols before restricting order.
    ;;(when (get-var-symbols (inconsistency-cnd inc))
    ;;    (return-from refine-by-reversing-order nil)
    ;;)
    (setq new-ex
        (constrain-ordering 
            expgen ex 
            (inconsistency-next-occ inc)
            (inconsistency-prior-occ inc)
            (find-inconsistencies expgen ex)
            :allow-eq t
        )
    )
    (if new-ex
        ;then
        (list (add-to-history new-ex 'refine-by-reversing-order inc ex))
        ;else
        nil
    )
)

(defun refine-by-unification (expgen ex inc &key (max-exes 1000) &aux blists all-the-facts (ex-list nil))
    (declare (ignore max-exes))
    (when (not (variables-in-cnd (inconsistency-cnd inc)))
        (return-from refine-by-unification nil)
    )
    (when (refine-by-calculation expgen ex inc)
        (return-from refine-by-unification nil)
    )
    (setq all-the-facts
        (append
            (apply #'append (mapcar #'occurrence-pre (get-events ex)))
            (apply #'append (mapcar #'occurrence-post (get-events ex)))
            (apply #'append (mapcar #'occurrence-pre (get-execution-history expgen)))
            (apply #'append (mapcar #'occurrence-post (get-execution-history expgen)))
        )
    )
    (setq all-the-facts (remove-if #'variables-in-fact all-the-facts))
    (when (and (not (var-symbols-in (cnd-args (inconsistency-cnd inc))))
               (not (eq 'fail (get-default-value-cnd expgen (inconsistency-cnd inc))))
          )
        (push 
            (make-fact 
                :type (cnd-type (inconsistency-cnd inc))
                :args (cnd-args (inconsistency-cnd inc))
                :value (get-default-value-cnd expgen (inconsistency-cnd inc))
            )
            all-the-facts
        )
    )
    (setq blists
        (remove nil
            (unify expgen (inconsistency-cnd inc) all-the-facts nil 
                :assume-fn #'assume-nothing
            )
        )
    )

    (let ((known-symbols nil))
        (setq known-symbols (get-var-symbols (inconsistency-cnd inc)))
        (setq known-symbols
            (append known-symbols
                (get-var-symbols (occurrence-signature (inconsistency-next-occ inc)))
            )
        )
        (when (inconsistency-prior-occ inc)
            (setq known-symbols
                (append known-symbols
                    (get-var-symbols (occurrence-signature (inconsistency-prior-occ inc)))
                )
            )
        )
        (setq known-symbols (remove-duplicates known-symbols))
        (dolist (sym known-symbols)
            (when (var-symbol->= sym)
                (push (list (cons sym (var-symbol->= sym))) blists)
            )
            (when (var-symbol-<= sym)
                (push (list (cons sym (var-symbol-<= sym))) blists)
            )
        )
    )
    
    (let ((old-incs (find-inconsistencies expgen ex)) new-ex) 
        (dolist (blist blists)
            (setq new-ex
                (estimate-bound-inconsistencies
                    (bind-explanation expgen ex inc blist)
                    expgen blist old-incs
                )
            )
            (incf (explanation-cost new-ex))
            (push new-ex ex-list)
        )
    )
    (mapcar+ #'add-to-history
        ex-list
        'refine-by-unification inc ex
    )
)

(defun estimate-bound-inconsistencies (ex expgen blist old-incs &aux inc-count)
    (when (not (member :inconsistency-count (get-heuristic-method expgen)))
        (return-from estimate-bound-inconsistencies ex)
    )
    (when (null ex)
        (return-from estimate-bound-inconsistencies nil)
    )
    (setq inc-count (length old-incs))
    (dolist (inc old-incs)
        (when (subsetp (get-var-symbols (inconsistency-cnd inc)) (mapcar #'car blist))
            (decf inc-count)
        )
    )
    (setf (explanation-estimated-inconsistency-count ex) inc-count)
    ex
)

;; This method creates a non-pre-enumerated occurrence that didn't happen, but 
;; could have.
(defun refine-by-hypothesizing-event-occurrence (expgen ex inc &key (max-exes 1000)
                                                 &aux new-ex inc-type static-facts 
                                                      (ex-set nil))
    ;(when (variables-in-cnd (inconsistency-cnd inc))
    ;    (return-from refine-by-hypothesizing-event-occurrence nil)
    ;)
    (when (null (get-intervening-event-points expgen inc))
        (return-from refine-by-hypothesizing-event-occurrence nil)
    )
    (when (and (cnd-inequality (inconsistency-cnd inc))
               (var-symbol-p (cnd-value (inconsistency-cnd inc)))
          )
        (return-from refine-by-hypothesizing-event-occurrence nil)
    )
    (when (and (inconsistency-prior-occ inc)
              (point-equal 
                (point-next expgen (occ-first-point (inconsistency-prior-occ inc)))
                (occ-last-point (inconsistency-next-occ inc))
              )
          )
        (return-from refine-by-hypothesizing-event-occurrence nil)
    )
        
    (setq inc-type (cnd-type (inconsistency-cnd inc)))
    (setq static-facts (get-static-facts expgen ex))
    
    (dolist (model (get-ev-models expgen))
        (let ((postconditions 
                    (remove inc-type (event-model-postconditions model) 
                        :key #'cnd-type :test-not #'eq
                    )
              )
              (inc-fact (bind-condition-to-fact expgen (inconsistency-cnd inc) nil nil))
              blists
              occs
             )
            (dolist (postcond postconditions)
                (setq blists (unify expgen postcond inc-fact nil :assume-fn #'assume-nothing))
                (setq occs
                    (enumerate-partially-bound-occs
                        expgen model static-facts nil blists 
                        :assume-fn #'assume-non-static
                    )
                )
                (when (> (length occs) 1)
                    (setq occs
                        (enumerate-partially-bound-occs
                            expgen model *empty-fact-set* nil blists 
                            :assume-fn #'assume-anything
                        )
                    )
                )
                (dolist (occ occs)
                    (when (unify expgen
                                 (inconsistency-cnd inc)
                                 (occurrence-post occ)
                                 nil
                                 :assume-fn #'default-closed-world-assume-fn
                          )
                        (if (cdr (get-intervening-event-points expgen inc))
                            ;then
                            (setf (occurrence-interval occ)
                                (make-interval 
                                    :range-start (apply #'min (get-intervening-event-points expgen inc))
                                    :range-end   (apply #'max (get-intervening-event-points expgen inc))
                                )
                            )
                            ;else
                            (setf (occurrence-point occ)
                                (only (get-intervening-event-points expgen inc))
                            )
                        )
                        (setq new-ex (add-event-to-explanation expgen occ ex))
                        (when new-ex
                            (when (inconsistency-prior-occ inc)
                                (setq new-ex 
                                    (constrain-ordering expgen new-ex (inconsistency-prior-occ inc) occ nil)
                                )
                            )
                        )
                        (when new-ex
                            (setq new-ex 
                                (constrain-ordering expgen new-ex occ (inconsistency-next-occ inc) nil)
                            )
                        )
                        (when new-ex
                            ; (incf (explanation-cost new-ex) 
                                ; (length (occurrence-constraints occ))
                            ; )
                            ; (dolist (fact (occurrence-pre occ))
                                ; (when (not (find-in-fact-set fact exp-facts))
                                    ; (incf (explanation-cost new-ex))
                                ; )
                            ; )
                            (setf (explanation-last-added-event new-ex) occ)
                            ;(setf (explanation-last-added-assumption new-ex) nil)
                            (incf (explanation-cost new-ex) 3)
                        )
                        (when new-ex
                            (dolist (blist (unify expgen (inconsistency-cnd inc) (occurrence-post occ) nil :assume-fn #'assume-nothing))
                                (when (not (null (remove-identities blist)))
                                    (setq new-ex (bind-explanation expgen new-ex inc blist))
                                )
                            )
                            (dolist (prior-ev (remove occ (explanation-new-events new-ex)))
                                (when (occurrences-unifiable occ prior-ev)
                                    (incf (explanation-cost new-ex) 10)
                                )
                            )
                            (push new-ex ex-set)
                        )
                        (when (>= (length ex-set) max-exes)
                            (return-from refine-by-hypothesizing-event-occurrence ex-set)
                        )
                    )
                )
            )
        )
    )
    (mapcar+ #'add-to-history
        (mapcar+ #'estimate-added-event-inconsistencies 
            (remove nil ex-set) expgen (find-inconsistencies expgen ex)
        )
        'refine-by-hypothesizing-event-occurrence inc ex
    )
)


;; This method creates a non-pre-enumerated occurrence that didn't happen, but 
;; could have.
(defun refine-by-hypothesizing-exogenous-action (expgen ex inc &key (max-exes 1000)
                                                 &aux new-ex (ex-set nil))
    (when (null (get-intervening-action-points expgen inc))
        (return-from refine-by-hypothesizing-exogenous-action nil)
    )
        
    (dolist (occ (find-hypothetical-actions expgen (inconsistency-cnd inc) ex))
        (if (cdr (get-intervening-action-points expgen inc))
            ;then
            (setf (occurrence-interval occ)
                (make-interval 
                    :range-start (apply #'min (get-intervening-action-points expgen inc))
                    :range-end   (apply #'max (get-intervening-action-points expgen inc))
                )
            )
            ;else
            (setf (occurrence-point occ)
                (only (get-intervening-action-points expgen inc)) 
            )
        )
        (setf (occurrence-is-action occ) t)
        (setf (occurrence-is-event occ) nil)
        (setf (occurrence-is-assumption occ) t)
        (setf (occurrence-pre occ)
            (remove 'self (occurrence-pre occ) :key #'fact-type)
        )
        (when (and (not (eq (occurrence-performer occ) (get-self expgen)))
                   (unify expgen 
                       (inconsistency-cnd inc)
                       (occurrence-post occ)
                       nil
                       :assume-fn #'default-closed-world-assume-fn
                   )
              )
            (setq new-ex (add-event-to-explanation expgen occ ex))
            (when new-ex
                (when (inconsistency-prior-occ inc)
                    (setq new-ex
                        (constrain-ordering expgen new-ex 
                            (inconsistency-prior-occ inc)
                            occ
                            nil
                        )
                    )
                )
            )
            (when new-ex
                (setq new-ex
                    (constrain-ordering expgen new-ex 
                        occ
                        (inconsistency-next-occ inc)
                        nil
                    )
                )
            )
            (when new-ex
                ;(setf (explanation-last-added-event new-ex) nil)
                (setf (explanation-last-added-assumption new-ex) occ)
                (incf (explanation-cost new-ex) 5)
                (push new-ex ex-set)
            )
            (when (>= (length ex-set) max-exes)
                (return-from refine-by-hypothesizing-exogenous-action ex-set)
            )
        )
    )
    (mapcar+ #'add-to-history
        (mapcar+ #'estimate-added-event-inconsistencies 
            ex-set expgen (find-inconsistencies expgen ex)
        )
        'refine-by-hypothesizing-exogenous-action inc ex
    )
)

(defun find-hypothetical-actions (expgen cnd ex &aux inc-type static-facts (new-occs nil))
    (let ((result (find-cached-hypothetical-actions expgen cnd)))
        (when (not (eq result :no-cache))
            ;(break "Checking cache.")
            (return-from find-hypothetical-actions result)
        )
    )
    (setq inc-type (cnd-type cnd))
    (setq static-facts (get-static-facts expgen ex))
    (dolist (model (get-action-models expgen))
        (let ((postcond 
                    (find inc-type (event-model-postconditions model) 
                        :key #'cnd-type
                    )
              )
              blists
              (occs nil)
             )
            (when postcond
                (setq blists (unify expgen postcond (bind-condition-to-fact expgen cnd nil nil) nil :assume-fn #'assume-nothing))
                (setq occs
                    (enumerate-partially-bound-occs
                        expgen model static-facts nil blists 
                        :assume-fn #'assume-non-static-and-self
                    )
                )
                (when (> (length occs) 3)
                    (setq occs
                        (enumerate-partially-bound-occs
                            expgen model *empty-fact-set* nil blists 
                            :assume-fn #'assume-anything
                        )
                    )
                )
                (setq new-occs (append occs new-occs))
            )
        )
    )
    (cache-hypothetical-actions expgen cnd new-occs)
    new-occs
)


(defun cache-hypothetical-actions (expgen cnd new-occs)
    (push (cons cnd (mapcar #'copy-occurrence new-occs)) (get-hypothetical-action-cache expgen))
)

(defun find-cached-hypothetical-actions (expgen cnd)
    (dolist (cache-pair (get-hypothetical-action-cache expgen))
        (let ((blist (cnd-equivalent (car cache-pair) cnd)))
            (when (not (eq blist 'fail))
                (when (eq blist t)
                    (setq blist nil)
                )
                (return-from find-cached-hypothetical-actions
                    (mapcar+ #'rebind-occurrence
                        (cdr cache-pair)
                        blist
                    )
                )
            )
        )
    )
    :no-cache
)

    
(defun enumerate-partially-bound-occs (expgen model facts pt blists &key (assume-fn #'assume-closed-world) &aux occs)
    (dolist (blist blists)
        (dolist (var-type-pair (model-var-types model))
            (when (not (member (car var-type-pair) blist :key #'car))
                (push (cons (car var-type-pair) (new-var-sym (second var-type-pair)))
                    blist
                )
            )
        )
        (setq occs 
            (append occs
                (enumerate-occs
                    expgen
                    model
                    facts
                    pt
                    :bindings blist
                    :assume-fn assume-fn
                    :open-precs t
                    :bind-var-symbols t
                )
            )
        )
    )
    occs
)    


(defun refine-by-hypothesizing-unknown-event (expgen ex inc &key (max-exes 1000) &aux ret occ)
    (declare (ignore max-exes))
    ;(when (variables-in-cnd (inconsistency-cnd inc))
    ;    (return-from refine-by-hypothesizing-unknown-event nil)
    ;)
    (when (null (get-intervening-event-points expgen inc))
        (return-from refine-by-hypothesizing-unknown-event nil)
    )
    (when (is-internal-cnd expgen (inconsistency-cnd inc))
        (return-from refine-by-hypothesizing-unknown-event nil)
    )
    (when (and (null (cnd-value (inconsistency-cnd inc))) (type-is-is (cnd-type (inconsistency-cnd inc))))
        (return-from refine-by-hypothesizing-unknown-event nil)
    )
    (when (is-static expgen (inconsistency-cnd inc))
        (return-from refine-by-hypothesizing-unknown-event nil)
    )
    (when (null (inconsistency-prior-occ inc))
        (return-from refine-by-hypothesizing-unknown-event nil)
    )
    (when (eq 'paradox (cnd-type (inconsistency-cnd inc)))
        (return-from refine-by-hypothesizing-unknown-event nil)
    )
    (when (cnd-inequality (inconsistency-cnd inc))
        ;;Currently, no way to learn that a constraint becomes satisfied.
        (return-from refine-by-hypothesizing-unknown-event nil)
    )
    (setq occ (make-unknown-event expgen ex inc))
    (setq ret (add-event-to-explanation expgen occ ex))
    (when (null ret)
        (return-from refine-by-hypothesizing-unknown-event nil)
    )
    (when ret
        (setq ret
            (constrain-ordering expgen ret
                (inconsistency-prior-occ inc)
                occ
                nil
            )
        )
    )
    (when ret
        (setq ret
            (constrain-ordering expgen ret
                occ
                (inconsistency-next-occ inc)
                nil
            )
        )
    )
    (when ret
        (setf (explanation-last-added-event ex) occ)
        ;(setf (explanation-last-added-assumption ex) nil)
        (incf (explanation-cost ret) 20)
        (setf (explanation-last-added-assumption ret) occ)
        (when (member :inconsistency-count (get-heuristic-method expgen))
            (setf (explanation-estimated-inconsistency-count ret)
                (1- (length (find-inconsistencies expgen ex)))
            )
        )
    )
    (if ret
        ;then
        (list
            (add-to-history ret
                'refine-by-hypothesizing-unknown-event inc ex
            )
        )
        ;else
        nil
    )
)

(defun make-unknown-event (expgen ex inc &aux pt)
    (setq pt (point-prior expgen (occ-first-point (inconsistency-next-occ inc))))
    (let* ((fact (cnd-to-fact (inconsistency-cnd inc)))
           prior-fact
           occ type-facts)
        (if (null (inconsistency-prior-occ inc))
            ;then
            (setq prior-fact (find-in-fact-set fact (facts-at expgen ex pt) :test #'same-map))
            ;else
            (setq prior-fact (find fact (occurrence-post (inconsistency-prior-occ inc)) :test #'same-map))
        )
                
        (when (null prior-fact)
            (setq prior-fact (get-default-fact expgen fact))
            (when (eq prior-fact 'fail)
                (error "Cannot continue. No default for: ~A." fact)
            )
        )
        
        (dolist (vsym (get-var-symbols fact))
            (push (make-fact :type (get-is-type (var-symbol-return-type vsym)) :args (list vsym) :value t) type-facts)
        )
        
        (setq occ
            (make-occurrence 
                :signature (list 'unknown-event inc)
                :pre (cons prior-fact type-facts) 
                :post (list fact)
                :is-event t
                :is-assumption t
            )
        )
        (if (cdr (get-intervening-event-points expgen inc))
            ;then
            (setf (occurrence-interval occ)
                (make-interval 
                    :range-start (apply #'min (get-intervening-event-points expgen inc))
                    :range-end   (apply #'max (get-intervening-event-points expgen inc))
                )
            )
            ;else
            (setf (occurrence-point occ)
                (only (get-intervening-event-points expgen inc))
            )
        )
        occ
    )
)


(defun refine-by-giving-up-event-in-cycle (expgen ex inc &key (max-exes 1000) &aux ret-set)
    (declare (ignore max-exes))
    (when (or (null (inconsistency-prior-occ inc))
              (not (eq 'event-cycle 
                       (occurrence-type (inconsistency-prior-occ inc))
                   )
              )
          )
        (return-from refine-by-giving-up-event-in-cycle nil)
    )
    (dolist (ev-type (mapcar #'first (rest (occurrence-signature (inconsistency-prior-occ inc)))))
        (when (member ev-type (get-learned-event-models expgen) :key #'model-type)
            (push-all (abandon-event expgen ex ev-type) ret-set)
        )
    )
    (mapcar+ #'add-to-history
        ret-set
        'refine-by-giving-up-event-in-cycle inc ex
    )
)


(defun refine-by-giving-up-learned-event (expgen ex inc &key (max-exes 1000) &aux ret)
    (declare (ignore max-exes))
    (when (or (null (inconsistency-prior-occ inc))
              (not (member (occurrence-type (inconsistency-prior-occ inc)) 
                           (mapcar #'model-type (get-learned-event-models expgen))
                   )
              )
          )
        (return-from refine-by-giving-up-learned-event nil)
    )
    (setq ret (abandon-event expgen ex (occurrence-type (inconsistency-prior-occ inc))))
    (add-to-history
        ret
        'refine-by-giving-up-learned-event inc ex
    )
)

(defun abandon-event (expgen ex em-type)
    ; (dolist (ev (remove em-type (get-events ex) :key #'occurrence-type :test-not #'eq))
        ; (setq ex (remove-event-from-explanation 'abandoning-event ev ex))
        ; (when (eq ex 'fail)
            ; (return-from abandon-event nil)
        ; )
    ; )
    ; (dolist (ev (remove 'event-cycle (get-events ex) :key #'occurrence-type :test-not #'eq))
        ; (when (assoc em-type (rest (occurrence-signature ev))) 
            ; (setq ex (remove-event-from-explanation 'cycle-resolved ev ex))
            ; (when (eq ex 'fail)
                ; (return-from abandon-event nil)
            ; )
        ; )
    ; )
    (setq ex 
        (make-explanation 
            :prior-events (get-assumptions ex)
            :abandoned-models (cons em-type (explanation-abandoned-models ex))
            :cost (+ 5 (explanation-cost ex))
        )
    )
    (extend-explanation expgen ex nil)
    (when (member :inconsistency-count (get-heuristic-method expgen))
        (setf (explanation-estimated-inconsistency-count ex) 
            (length (find-inconsistencies expgen ex))
        )
    )
    (list ex)
)

(defun get-unknown-events (ex)
    (remove-duplicates
        (remove 'unknown-event (get-events ex) 
            :key #'occurrence-type :test-not #'eq
        )
        :test #'equalp
    )
)


(defun get-intervening-action-points (expgen inc)
    (get-intervening-points expgen inc :test #'is-exogenous-action-pt)
)

(defun get-action-points-between (expgen range-start range-end)
    (get-points-between expgen range-start range-end :test #'is-exogenous-action-pt)
)

(defun get-intervening-event-points (expgen inc)
    (get-intervening-points expgen inc :test #'is-event-pt)
)


(defun get-event-points-between (expgen range-start range-end)
    (get-points-between expgen range-start range-end :test #'is-event-pt)
)

(defun get-intervening-points (expgen inc &key (test #'list) &aux first-pt)
    (if (null (inconsistency-prior-occ inc))
        ;then
        (setq first-pt (get-first-point expgen))
        ;else
        (setq first-pt (point-next expgen (occ-first-point (inconsistency-prior-occ inc))))
    )
    (get-points-between expgen first-pt (point-prior expgen (occ-last-point (inconsistency-next-occ inc)))
        :test test
    )
)

(defun get-intervening-point-count (expgen inc &aux first-pt)
    (if (null (inconsistency-prior-occ inc))
        ;then
        (setq first-pt (get-first-point expgen))
        ;else
        (setq first-pt (point-next expgen (occ-first-point (inconsistency-prior-occ inc))))
    )
    (- (point-prior expgen (occ-last-point (inconsistency-next-occ inc)))
       first-pt
    )
)

(defun sort-inconsistencies-by-point-count (expgen inc-list)
    (sort (copy-list inc-list) #'< :key (embed-expgen #'get-intervening-point-count expgen))
)

(defun get-points-between (expgen range-start range-end
                               &key (test #'identity)
                               &aux (ret-set nil) cur-pt)
    (setq cur-pt range-end)
    (loop while (and cur-pt (not (point-earlier cur-pt range-start))) do
        (when (funcall test expgen cur-pt)
            (push cur-pt ret-set)
        )
        (setq cur-pt (point-prior expgen cur-pt))
    )
    ret-set
)

(defun facts-at (expgen ex pt &aux ret)
    (when (and (explanation-valid-fact-cache-pt ex) (not (point-later pt (explanation-valid-fact-cache-pt ex))))
        (setq ret (cdr (assoc pt (explanation-cached-facts ex))))
        (loop while (and pt (null ret)) do
            (setq pt (point-prior expgen pt))
            (setq ret (cdr (assoc pt (explanation-cached-facts ex))))
        )
        (when (null ret)
            (error "Cached-facts not created.")
        )
    )
        
    (when (null ret)
        (find-inconsistencies expgen ex)
        (when (and (explanation-valid-fact-cache-pt ex) (point-later pt (explanation-valid-fact-cache-pt ex)))
            (error "Can't handle the truth.")
        )
        (when (and (null (explanation-valid-fact-cache-pt ex)) (not (eql 1 (get-last-point expgen))))
            (error "Bewildering.")
        )
        (when (eql (get-first-point expgen) (get-last-point expgen))
            (return-from facts-at (new-fact-set (occurrence-pre (get-last-observation expgen))))
        )
        ; (setq ret (cdr (assoc pt (explanation-cached-facts ex)))) ;; This line fails when there is a gap at pt.
        (setq ret (facts-at expgen ex pt))
    )
    ret
)

(defun get-static-facts (expgen ex)
    (when (explanation-cached-statics ex)
        (return-from get-static-facts (explanation-cached-statics ex))
    )
    (setf (explanation-cached-statics ex)
        (new-fact-set
            (remove 'self
                (remove-if-not (embed-expgen #'is-static-fact expgen) 
                    (get-facts (facts-at expgen ex 1))
                )
                :key #'fact-type
            )
        )
    )
    (explanation-cached-statics ex)
)

(defun fact-occs-at (expgen ex pt &aux ret)
    (when (and (explanation-valid-fact-cache-pt ex) (not (point-later pt (explanation-valid-fact-cache-pt ex))))
        (setq ret (cdr (assoc pt (explanation-cached-fact-occs ex))))
        (loop while (and pt (null ret)) do
            (setq pt (point-prior expgen pt))
            (setq ret (cdr (assoc pt (explanation-cached-fact-occs ex))))
        )
        (when (null ret)
            (error "Cached-fact-occs not created.")
        )
    )
        
    (when (null ret)
        (find-inconsistencies expgen ex)
        (when (point-later pt (explanation-valid-fact-cache-pt ex))
            (error "Can't handle the truth.")
        )
        (setq ret (fact-occs-at expgen ex pt))
    )
    ret
)

(defun generate-assumable-conditions (expgen &aux ret arg-types cnd blists)
    (when (get-hidden-facts expgen)
        (return-from generate-assumable-conditions
            (mapcar #'fact-to-cnd (get-hidden-facts expgen))
        )
    )
    (dolist (pred (remove nil (mapcar+ #'predicate-named (envmodel-hidden (get-last-envmodel expgen)) (get-predicates (get-last-envmodel expgen)))))
        (setq arg-types (predicate-arg-types pred))
        (setq cnd
            (make-cnd 
                :type (predicate-name pred) 
                :args (mapcar #'first (predicate-arg-types pred))
                :value t
            )
        )
        (when (is-legal-assumption expgen cnd)
            (when (predicate-value-type pred)
                (setf (cnd-value cnd) '?ret)
                (push (list '?ret (predicate-value-type pred)) 
                    arg-types
                )
            )
            (setq blists
                (unify-set
                    expgen
                    ;;cnd-set
                    (cons cnd (mapcar+ #'var-decl-to-cnd arg-types (get-last-envmodel expgen)))
                    ;;fact-set
                    (occurrence-pre (get-prior-observation expgen))
                    ;;binding-list
                    nil
                    :assume-fn #'assume-hidden-and-closed-world
                    :ground-all-vars t
                )
            )
            (setq blists 
                (remove 
                    (list (cons '?ret (get-default-value-cnd expgen cnd))) 
                    blists 
                    :test #'equal
                )
            )
            (setq ret 
                (append ret
                    (apply #'append
                        (mapcar 
                            #'(lambda (blist) 
                                (mapcar #'fact-to-cnd 
                                    (bind-condition-to-fact expgen cnd blist nil)
                                )
                            ) 
                            blists
                        )
                    )
                )
            )
        )
    )
    ret
)
        

(defun adapt-explanation (occ expgen ex inc &aux blists)
    (setq blists (unify expgen (inconsistency-cnd inc) (occurrence-post occ) nil :assume-fn #'assume-nothing))
    (when (null blists)
        (return-from adapt-explanation nil)
    )
    
    (setq ex (add-event-to-explanation expgen occ ex))
    (when (null ex)
        (return-from adapt-explanation nil)
    )
    (setf (explanation-last-added-event ex) occ)
    ;(setf (explanation-last-added-assumption ex) nil)
    (setq blists (mapcar #'remove-identities blists))
    (when (equal blists '(()))
        (return-from adapt-explanation (list ex))
    )
    (remove nil 
        (mapcar 
            #'(lambda (blist) (bind-explanation expgen ex inc blist)) 
            blists
        )
    )
)

(defun make-initial-assumption-occurrence (expgen cnd)
    (make-occurrence
        :signature 
            (list 
                'assume-initial-value 
                (cons (cnd-type cnd) (cnd-args cnd))
                (cnd-value cnd)
            )
        :point (get-first-point expgen)
        :pre (list (cnd-to-fact cnd))
        :is-assumption t
        :is-event t
    )
)

(defun get-possible-events (expgen ob)
    (gethash ob (get-possible-event-map expgen))
)

(defun consider-possible-events (expgen actions pt last-ob 
                                 &key (consider-actions nil) (ground-occurrences t)
                                 &aux state-facts duration init-time)
    (setq duration 0)
    (when (and actions (eq (car (occurrence-signature (car actions))) '!wait))
        (setq duration (second (occurrence-signature (car actions))))
    )
    (setq state-facts 
        (get-facts 
            (advance-facts expgen actions 
                (create-fact-set (get-last-hiddens expgen)) 
                nil nil pt
            )
        )
    )

    (when (and (null actions) (null consider-actions))
        (error "No actions to be taken.")
    )

    (setq init-time (get-internal-run-time))
    ;(break "before find-possible-events")
    (multiple-value-bind (possible-events possible-facts-after mutexes) 
        (find-possible-events
            expgen
            state-facts
            (apply #'append (mapcar #'occurrence-post actions))
            pt
            (get-last-mutexes expgen)
            duration
            :hypothesize-actions consider-actions
            :ground-occurrences ground-occurrences
        )
        (push-all possible-events 
            (gethash last-ob (get-possible-event-map expgen))
        )
        (setf (get-last-hiddens expgen) possible-facts-after)
        (setf (get-last-mutexes expgen) mutexes)
    )
    (incf (get-total-enumeration-time expgen) 
        (+ 0.0 
           (/ (- (get-internal-run-time) init-time) 
              internal-time-units-per-second
           )
        )
    )
    nil
)


(defun maintain-mutexes (expgen ob2)
    (setf (get-last-hiddens expgen)
        (append (occurrence-pre ob2)
            (remove-inconsistent-facts expgen 
                (get-last-hiddens expgen) 
                (occurrence-pre ob2)
                (get-last-mutexes expgen)
            )
        )
    )
    (setf (get-last-mutexes expgen)
        (remove-observable-mutexes expgen (get-last-mutexes expgen))
    )
)



(defun replace-interval (expgen ex occ range-start range-end &aux revised-occ new-ex allowed-points)
    ;;There seems to be a messy situation in which replacing the interval in later-occ
    ;; (above) results in replacing the interval in earlier-occ (already), so 
    ;; that earlier-occ references an occurrence no longer in the explanation.
    ;; If it must be constrained further, we're going to need an updated reference.
    
    (setq occ (update-occurrence-reference occ ex))
  
    (setq new-ex (copy-explanation ex))
  
    (if (member occ (explanation-prior-events ex))
        ;then
        (setf (explanation-prior-events new-ex)
            (remove occ (explanation-prior-events new-ex))
        )
        ;else
        (setf (explanation-new-events new-ex)
            (remove occ (explanation-new-events new-ex))
        )
    )
    (trim-cache expgen new-ex (occ-first-point occ))
    
    (setq revised-occ (copy-occurrence occ))
    (if (occurrence-is-action occ)
        ;then
        (setq allowed-points (get-action-points-between expgen range-start range-end))
        ;else
        (setq allowed-points (get-event-points-between expgen range-start range-end))
    )
    (when (null allowed-points)
        (return-from replace-interval nil)
    )
    (setq range-start (apply #'min allowed-points))
    (setq range-end (apply #'max allowed-points))
    
    (if (point-equal range-start range-end)
        ;then
        (setf (occurrence-point revised-occ) range-start
              (occurrence-interval revised-occ) nil
        )
        ;else
        (setf (occurrence-interval revised-occ)
            (make-interval 
                :range-start range-start
                :range-end range-end
            )
        )
    )
    
    (setq new-ex (add-event-to-explanation expgen revised-occ new-ex))
    (when (null new-ex)
        (return-from replace-interval nil)
    )
    (let ((following-cons (remove occ (explanation-occurrence-precedence new-ex) :key #'first :test-not #'eq)))
        (setf (explanation-occurrence-precedence new-ex) 
            (remove occ (explanation-occurrence-precedence new-ex) :key #'first)
        )
        ;(clrhash (explanation-cached-precedence-map new-ex))
        (dolist (f-con following-cons)
            (setq new-ex 
                (constrain-ordering expgen new-ex 
                    revised-occ (second f-con) 
                    nil :allow-eq (third f-con)
                )
            )
            (when (null new-ex)
                (return-from replace-interval nil)
            )
        )
    )
    
    (let ((preceding-cons (remove occ (explanation-occurrence-precedence new-ex) :key #'second :test-not #'eq)))
        (setf (explanation-occurrence-precedence new-ex) 
            (remove occ (explanation-occurrence-precedence new-ex) :key #'second)
        )
        ;(clrhash (explanation-cached-precedence-map new-ex))
        (dolist (p-con preceding-cons)
            (setq new-ex 
                (constrain-ordering expgen new-ex 
                    (first p-con) revised-occ 
                    nil :allow-eq (third p-con)
                )
            )
            (when (null new-ex)
                (return-from replace-interval nil)
            )
        )
    )
    (when (eq (explanation-last-added-event ex) occ)
        (setf (explanation-last-added-event new-ex) revised-occ)
    )
    (when (eq (explanation-last-added-assumption ex) occ)
        (setf (explanation-last-added-assumption new-ex) revised-occ)
    )
    
    
    (values new-ex (update-occurrence-reference revised-occ new-ex))
)

;; Adds an event, and enforces condition 3 for plausibility, that no facts
;; contradict one another before or after.
(defun add-event-to-explanation (expgen ev ex &aux simul-occs occ-removed)
    (when (member ev (explanation-event-removals ex) :test #'occurrence-equal)
        (return-from add-event-to-explanation nil)
    )
    (when (member (occurrence-type ev) (explanation-abandoned-models ex))
        (return-from add-event-to-explanation nil)
    )
    
    (if (occurrence-point ev)
        ;then
        (setq simul-occs (remove (occurrence-point ev) (get-events ex) :test-not #'eql))
        ;else
        (setq simul-occs nil)
    )
    ;(setq simul-occs (remove ev simul-occs :test #'occurrence-later))

    (dolist (occ simul-occs)
        (setq occ-removed nil)
        (when (or (intersection (occurrence-pre ev) (occurrence-pre occ) :test #'contradicts)
                  (intersection (occurrence-post ev) (occurrence-post occ) :test #'contradicts)
              )
            (setq ex (remove-event expgen ex occ))
            (when (eq ex 'fail)
                (return-from add-event-to-explanation nil)
            )
            (setq occ-removed t)
        )

        (loop for cnd in (occurrence-constraints ev) while (not occ-removed) do
            (when (unify expgen cnd (occurrence-pre occ) nil :assume-fn #'assume-nothing)
                (setq ex (remove-event expgen ex occ))
                (when (eq ex 'fail)
                    (return-from add-event-to-explanation nil)
                )
                (setq occ-removed t)
            )
        )
    )
    
    (setq ex (add-event expgen ex ev))
    (if (eq ex 'fail)
        ;then
        nil
        ;else
        ex
    )
)

(defun remove-event-from-explanation (expgen cnd occ ex &aux removal-occ)
    (setq ex (remove-event expgen ex occ))
    (when (eq ex 'fail)
        (return-from remove-event-from-explanation 'fail)
    )
    (when (cnd-p cnd)
        (setq removal-occ
            (make-occurrence 
                :signature (list 'removal cnd (occurrence-signature occ))
                :point (occurrence-point occ)
                :interval (occurrence-interval occ)
                :constraints (list cnd)
                :is-event t
                :old nil
            )
        )
        (push removal-occ (explanation-new-events ex))
        (setf (explanation-last-added-event ex) removal-occ)
        ;(setf (explanation-last-added-assumption ex) nil)
    )
    ex
)

(defun find-appropriate-sensing-actions (expgen fact-list known-facts &key (recursive nil))
    (let* ((f-type (fact-type (car fact-list)))
           (ev-models (cdr (assoc f-type (get-observation-events expgen))))
           (ret-actions nil)
           (trigger-action-name nil)
           (action-model nil)
           (cnds nil)
           (binding-sets nil))
        (dolist (ev-model ev-models)
            (setq cnds (remove f-type (event-model-conditions ev-model) :test-not #'eq :key #'cnd-type))
            (setq trigger-action-name 
                (get-trigger-action 
                    (cnd-type (car (event-model-conditions ev-model)))
                )
            )
            (setq action-model (find trigger-action-name (get-action-models expgen) :key #'event-model-type))
            (dolist (cnd cnds)
                (dolist (blist (unify expgen cnd fact-list nil :assume-fn #'default-closed-world-assume-fn))
                    (setq binding-sets
                        (unify-set expgen (remove cnd (cdr (event-model-conditions ev-model))) known-facts blist 
                            :assume-fn #'assume-nothing
                        )
                    )
                    (dolist (bindings binding-sets)
                        (let* 
                          ((trigger-fact 
                            (bind-condition-to-fact 
                                expgen
                                (car (event-model-conditions ev-model)) 
                                bindings 
                                known-facts
                            )
                           )
                           (blists2 
                             (unify expgen
                                 (car (last (event-model-postconditions action-model)))
                                 trigger-fact 
                                 nil
                                 :assume-fn #'default-closed-world-assume-fn
                             )
                           )
                           action-model-binding-sets
                          )
                          (dolist (blist2 blists2)
                              (setq action-model-binding-sets
                                (unify-set expgen
                                    (event-model-conditions action-model) 
                                    known-facts blist2
                                    :assume-fn #'default-closed-world-assume-fn
                                )
                              )
                              
                              (dolist (bindings2 action-model-binding-sets)
;                                  (break "Bindings found: ~A" bindings2) 
                                  (push 
                                      (cons trigger-action-name
                                         (mapcar #'cdr 
                                             (mapcar+ #'assoc 
                                                 (event-model-variables ev-model) 
                                                 bindings2
                                             )
                                         )
                                     )
                                     ret-actions
                                 )
                              )
                          )
                        )
                    )
                )
            )
        )
        (if (and (null ret-actions) (not recursive))
            ;then
            (find-appropriate-sensing-actions expgen fact-list (get-last-hiddens expgen) :recursive t)
            ;else
            ret-actions
        )
    )
)

(defun sample-ambiguity (sampler expgen exes &aux inc-set ex new-exes inc)
    (setq exes (copy-list exes))
    (loop while exes do
        (setq ex (pop exes))
        (setq inc-set (find-inconsistencies expgen ex))
        (when (null inc-set)
            (sample sampler ex)
        )
        (loop while inc-set do
            (setq inc (pop inc-set))
            (setq new-exes 
                (remove-if 
                    #'(lambda (ex-sub)
                        (var-symbols-in 
                            (mapcar #'cdr 
                                (set-difference 
                                    (explanation-bindings ex-sub)
                                    (explanation-bindings ex)
                                )
                            )
                        )
                      )
                    (refine-by-binding-next-occ expgen ex inc)
                )
            )
            (when new-exes
                (setq inc-set nil)
                (push-all new-exes exes)
            )
        )
    )
)

(defun search-cycling (ex)
    (or (cycle-has-formed 
            (list (first (explanation-history ex))) 
            (explanation-history ex) 
            :test #'equivalent-refinements
        )
        (cycle-has-formed 
            (ntop 2 (explanation-history ex)) 
            (explanation-history ex) 
            :test #'equivalent-refinements
        )
    )
)

