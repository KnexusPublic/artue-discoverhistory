 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: April 12, 2011
 ;;;; Project Name: Explanation
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file provides PDDL domain import functionality.
 
(in-package :artue)
 

(export '(neq increase decrease oneof unknown))

(defstruct envmodel
    (name 'unnamed)
    (file nil)
    (constants nil)
    (objects nil)
    (predicates nil)
    (hidden nil)
    (hidden-types nil)
    (static nil)
    (internal nil)
    (defaults nil)
    (functions nil)
    (actions nil)
    (methods nil)
    (events nil)
    (processes nil)
    (types nil)
    (supertypes nil)
    (open-types nil)
    (requirements nil)
    (constant-types nil)
    (integer-types nil)
    (real-types nil)
    (numeric-types nil)
    (unknown-object-likelihood nil)
    (assumption-likelihood nil)
)

(defstruct predicate
    (name 'unnamed)
    (arg-types nil)
    (value-type nil)
)

(defun create-model (domain &aux model)
    (setq model (apply #'make-envmodel (get-domain-arg-list domain)))
    (push '(int real) (envmodel-supertypes model))
    (setf (envmodel-numeric-types model) 
        (append (get-subtypes model 'int) 
                (get-subtypes model 'real) 
                (list 'int 'real)
        )
    )
    (setf (envmodel-integer-types model) (cons 'int (get-subtypes model 'int)))
    (setf (envmodel-real-types model) 
        (set-difference 
            (cons 'real (get-subtypes model 'real))
            (envmodel-integer-types model)
        )
    )
    (setf (envmodel-constant-types model) 
        (append (get-subtypes model 'boolean) 
                (envmodel-numeric-types model) 
                (list 'boolean)
        )
    )
    (dolist (pred (envmodel-predicates model))
        (when (not (member (car pred) (envmodel-defaults model)))
            (push (list (car pred) nil) (envmodel-defaults model))
        )
    )
    (push '_win (envmodel-hidden model))
    (push '(_win nil) (envmodel-defaults model))
    model
)

(defun read-pddl-domain (domain)
    (let ((domain-name (cadr (assoc 'domain domain))))
        (setf (get domain-name :pddls)
              (create-model domain)
        )
    )
)



(defun get-trigger-action (sym)
    (get sym 'trigger)
)


(defgeneric get-direct-supertypes (model subtype))
(defmethod get-direct-supertypes ((model envmodel) subtype)
    (mapcar #'second (remove subtype (envmodel-supertypes model) :key #'car :test-not #'eq))
)

(defgeneric is-constant-type (model type))
(defmethod is-constant-type ((model envmodel) type)
    (if (member type (envmodel-constant-types model))
        t
        nil
    )
)

(defgeneric is-numeric-type (model type))
(defmethod is-numeric-type ((model envmodel) type)
    (if (member type (envmodel-numeric-types model))
        t
        nil
    )
)

(defgeneric is-real-type (model type))
(defmethod is-real-type ((model envmodel) type)
    (if (member type (envmodel-real-types model))
        t
        nil
    )
)

(defgeneric is-integer-type (model type))
(defmethod is-integer-type ((model envmodel) type)
    (if (member type (envmodel-integer-types model))
        t
        nil
    )
)

(defgeneric get-supertypes (model subtype))
(defmethod get-supertypes ((model envmodel) subtype)
    (let
      ((new-supertypes (get-direct-supertypes model subtype))
       (supertype-list nil)
      )
        (loop while (not (null (set-difference new-supertypes supertype-list))) do
            (setq supertype-list (union supertype-list new-supertypes))
            (setq new-supertypes 
                (apply #'append
                    (mapcar #'(lambda (x) (get-direct-supertypes model x)) new-supertypes)
                )
            )
        )
        (cons :any supertype-list)
    )
)

(defmethod is-supertype ((model envmodel) ancestor descendant)
    (member ancestor (get-supertypes model descendant))
)

(defmethod is-assignable-from ((model envmodel) ancestor descendant)
    (or (eq ancestor descendant) (member ancestor (get-supertypes model descendant)))
)

(defgeneric get-subtypes (model supertype))
(defmethod get-subtypes ((model envmodel) supertype)
    (map-if #'(lambda (x) (when (is-supertype model supertype x) t)) (envmodel-types model))
)

(defun get-constants-of-type (envmodel type-name)
    (mapcar #'first
        (remove type-name
            (make-type-declarations (envmodel-constants envmodel))
            :key #'second :test-not #'eq
        )
    )
)

(defun get-domain-arg-list (domain)
   (let ((arg-list nil)
         (action-list nil)
         (event-list nil)
         (process-list nil)
         (method-list nil)
        )
       (loop for list-obj in domain do
           (cond 
              ((eq (car list-obj) :action)
                  (push (cdr list-obj) action-list)
              )
              ((eq (car list-obj) :event)
                  (push (cdr list-obj) event-list)
              )
              ((eq (car list-obj) :process)
                  (push (cdr list-obj) process-list)
              )
              ((eq (car list-obj) :method)
                  (push (cdr list-obj) method-list)
              )
              ((eq (car list-obj) :-)
                  ;;Problem, probably.
              )
              ((eq (car list-obj) 'domain)
                  (setq arg-list 
                       (cons :name (cons (cadr list-obj) arg-list))))
              ((eq (car list-obj) :operator)) ;operators are thrown out; only necessary for costs.
              (t
                  (setq arg-list
                       (append arg-list (list (car list-obj) (cdr list-obj)))))
           )
      )
      (append 
         arg-list 
         (list :actions action-list) 
         (list :methods method-list) 
         (list :events event-list)
         (list :processes process-list)
      )
   )
)

(defun pddl-model-name (pddl-obj)
    (car pddl-obj)
)

(defun pddl-model-performer (pddl-obj)
    (getf (cdr pddl-obj) :performer)
)

(defun pddl-model-parameters (pddl-obj)
    (getf (cdr pddl-obj) :parameters)
)

(defun pddl-model-effects (pddl-obj)
    (getf (cdr pddl-obj) :effect)
)

(defsetf pddl-model-effects (pddl-obj) (pddl-eff)
    `(setf (getf (cdr ,pddl-obj) :effect) ,pddl-eff)
)

(defun pddl-model-preconditions (pddl-obj)
    (getf (cdr pddl-obj) :precondition)
)

(defsetf pddl-model-preconditions (pddl-obj) (pddl-cond)
    `(setf (getf (cdr ,pddl-obj) :precondition) ,pddl-cond)
)

(defun pddl-model-initial (pddl-obj)
    (getf (cdr pddl-obj) :initial)
)

(defun pddl-model-likelihood (pddl-obj)
    (getf (cdr pddl-obj) :likelihood)
)

(defvar *belief-transitions* 0)

(defun create-event-from-uncertain-fact (uncertain-fact preds envmodel &aux invariance)
    (setq invariance (is-in-tree uncertain-fact 'invariant))
    (when invariance
        (setq uncertain-fact (subst 'or 'invariant uncertain-fact))
    )
    (build-event-model
        (list (combine-strings-symbols 'belief-transition- (incf *belief-transitions*))
            :initial t
            :parameters (get-param-list uncertain-fact preds)
            :precondition `(and ,(if invariance '(not (paradox)) '(initializing-beliefs)) (not ,uncertain-fact))
;            :precondition `(and (not ,uncertain-fact))
            :effect '(paradox)
        )
        preds
        envmodel
    )
)


(defvar *goal-count* 0)

(defun create-events-from-goal-success (expgen goal preds &key (condition-name nil) &aux win-fact)
    (when (null condition-name)
        (setq condition-name *goal-count*)
        (incf *goal-count*)
    )
    (setq win-fact 
        (make-fact :type '_win :args (list condition-name) :value t)
    )
    (values
        (build-event-model
            (list '_succeed
                :parameters (get-param-list goal preds)
                :precondition `(and ,goal (not (,(fact-type win-fact) ,condition-name)))
                :effect (list (fact-type win-fact) condition-name)
            )
            preds
            (get-last-envmodel expgen)
        )
        win-fact
    )
)

(defun get-pddl-param-list (formula preds)
    (let ((params (get-param-list formula preds)))
        (make-pddl-param-list (mapcar #'first params) params)
    )
)

(defun get-param-list (formula preds)
    (when (or (null formula) (not (listp formula)))
        (if (member formula '(or and not oneof))
            ;then
            (return-from get-param-list nil)
            ;else
            (error "predicate not found")
        )
    )
    (let ((cur-pred (predicate-named (car formula) preds)))
        (if (null cur-pred)
            ;then
            (apply #'append (mapcar+ #'get-param-list formula preds))
            ;else
            (make-param-list formula cur-pred) 
        )
    )
)

(defun make-param-list (formula pred)
    (remove-if-not
        #'is-variable
        (pairlis (cdr formula) (mapcar #'second (predicate-arg-types pred)))
        :key #'car
    )
)

(defun predicate-named (name preds)
    (find name preds :key #'predicate-name)
)

(defun build-event-model (pddl-ev preds envmodel &aux (ret nil) decls effect-lits cnds)
    (dolist (conj (cnf (pddl-model-preconditions pddl-ev)))
      (setq cnds (apply #'append (mapcar+ #'process-pddl-to-condition conj envmodel)))
      (when (not (contradictions-in-cnds cnds))
        (setq decls (pddl-model-parameters pddl-ev))
        (when (not (listp (first decls)))
            ;; Matt 7/30 : Not sure why this is different sometimes.
            (setq decls (make-type-declarations (pddl-model-parameters pddl-ev)))
        )
        (when (and (starts-with "INTERNAL_ACTION" (symbol-name (pddl-model-name pddl-ev)))
                (eq (first (pddl-model-parameters pddl-ev)) '?fact)
              )
            (return-from build-event-model
                (list
                    (make-event-model
                        :type (pddl-model-name pddl-ev)
                        :performer (pddl-model-performer pddl-ev)
                        :variables 
                            (remove-duplicates 
                                (append 
                                    (mapcar #'first decls) 
                                    (vars-in (remove 'forall conj :key #'first))
                                ) 
                                :from-end t
                            )
                        :likelihood (pddl-model-likelihood pddl-ev)
                        :arg-types decls
                        :conditions nil
                        :postconditions nil
                    )
                )
            )
        )
        (if (or (eq 'and (car (pddl-model-effects pddl-ev))) 
                (listp (car (pddl-model-effects pddl-ev)))
            )
            ;then
            (setq effect-lits (remove 'and (pddl-model-effects pddl-ev)))
            ;else
            (setq effect-lits (list (pddl-model-effects pddl-ev)))
        )
        (push
            (add-fact-types
                envmodel
                (make-event-model
                    :type (pddl-model-name pddl-ev)
                    :performer (pddl-model-performer pddl-ev)
                    :variables 
                        (remove-duplicates 
                            (append 
                                (mapcar #'first decls) 
                                (vars-in (remove 'forall conj :key #'first))
                            ) 
                            :from-end t
                        )
                    :arg-types decls
                    :conditions 
                        (apply #'append
                            cnds
                            (mapcar #'process-pddl-effect-to-preconditions effect-lits)
                        )
                    :postconditions 
                        (append
                            (apply #'append (mapcar+ #'process-pddl-cond-to-effect conj envmodel))
                            (mapcar #'process-pddl-effect-to-postcondition effect-lits)
                        )
                    :likelihood (pddl-model-likelihood pddl-ev)
                )
                preds
            )
            ret
        )
      )
    )
    ret
)

(defun build-process-model (pddl-proc preds envmodel &aux (ret nil) decls multiple-effects cnds)
    (dolist (conj (cnf (pddl-model-preconditions pddl-proc)))
      (setq cnds (apply #'append (mapcar+ #'process-pddl-to-condition conj envmodel)))
      (when (not (contradictions-in-cnds cnds))
        (setq decls (pddl-model-parameters pddl-proc))
        (when (not (listp (first decls)))
            ;; Matt 7/30 : Not sure why this is different sometimes.
            (setq decls (make-type-declarations (pddl-model-parameters pddl-proc)))
        )
        (setq multiple-effects 
            (or (eq 'and (car (pddl-model-effects pddl-proc))) 
                (listp (car (pddl-model-effects pddl-proc)))
            )
        )
        (push
            (add-fact-types
                envmodel
                (make-process-model
                    :type (pddl-model-name pddl-proc)
                    :variables 
                        (remove-duplicates 
                            (append 
                                (mapcar #'first decls) 
                                (vars-in conj)
                            ) 
                            :from-end t
                        )
                    :arg-types decls
                    :conditions cnds
                    :effects
                        (if multiple-effects
                            ;then
                            (mapcar #'process-pddl-function-to-effect 
                                (remove 'and (pddl-model-effects pddl-proc))
                            )
                            ;else
                            (list 
                                (process-pddl-function-to-effect 
                                    (pddl-model-effects pddl-proc)
                                )
                            )
                        )
                )
                preds
            )
            ret
        )
      )
    )
    ret
)

(defun process-pddl-function-to-effect (func &aux new-val update-func)
    (when (not (member (first func) '(increase decrease)))
        (error "func")
    )
    (setq update-func 
        (list
            (case (first func)
                (increase '+)
                (decrease '-)
                (otherwise (error "Unrecognized change function."))
            )
            (make-old-value-variable (second func))
            (third func)
        )
    )
    (when (not (member '?__dur (vars-in update-func)))
        (error "Explanation process cannot yet explain non-linear continuous functions.")
    )
    (setq new-val (make-current-value-symbol (make-fact :type (car func) :args (rest func))))
    (setf (var-symbol-update-function new-val) update-func)
        
    (make-cnd :type (first (second func)) :args (rest (second func)) :value new-val)
)

(defun get-variables-in-model (ev-model)
    (remove-duplicates
        (vars-in
            (apply #'append
                (cons (mapcar #'cnd-value (model-conditions ev-model))
                    (mapcar #'cnd-args (model-conditions ev-model))
                )
            )
        )
    )
)

(defun find-model-var-types (envmodel ev-model preds 
                             &aux candidate-types most-specific-type)
    (let* ((decls (model-arg-types ev-model))
           (extra-vars 
               (set-difference 
                   (get-variables-in-model ev-model) (mapcar #'first decls)
               )
           )
          )
        (dolist (var extra-vars)
            (setq candidate-types 
                (mapcar+ #'find-var-type-from-cnd 
                    (model-conditions ev-model) 
                    var
                    preds
                )
            )
            (setq candidate-types (remove nil (remove-duplicates candidate-types)))
            (when (null candidate-types)
                (error "No candidate types for variable ~A in model ~A." var (model-type ev-model))
            )
            (setq most-specific-type (first candidate-types))
            (dolist (var-type (rest candidate-types))
                (cond
                    ((is-supertype envmodel most-specific-type var-type)
                        (setq most-specific-type var-type)
                    )
                    ((not (is-supertype envmodel var-type most-specific-type))
                        (error "Multiple inconsistent candidate types for variable ~A in model ~A." var (model-type ev-model))
                    )
                )
            )
            (push (list var most-specific-type) decls)
        )
        decls
    )
)

(defun find-var-type-from-cnd (cnd var preds &aux rel-pred)
    (when (eq (cnd-type cnd) 'assign)
        (when (eq var (first (cnd-value cnd)))
            (return-from find-var-type-from-cnd 'real)
        )
        (return-from find-var-type-from-cnd nil)
    )
    (when (eq (cnd-type cnd) 'eval)
        (return-from find-var-type-from-cnd nil)
    )
    (when (eq (cnd-type cnd) 'create)
        (when (eq var (second (cnd-value cnd)))
            (return-from find-var-type-from-cnd (first (cnd-value cnd)))
        )
        (return-from find-var-type-from-cnd nil)
    )
    (when (eq (cnd-type cnd) 'forall)
        (return-from find-var-type-from-cnd nil)
    )
    (setq rel-pred (find (cnd-type cnd) preds :key #'predicate-name))
    (when (null rel-pred)
        (error "Predicate ~A was not defined."
            (cnd-type cnd)
        )
    )
    (cond 
        ((member var (cnd-args cnd))
            (second
                (nth (position var (cnd-args cnd)) 
                     (predicate-arg-types rel-pred)
                )
            )
        )
        ((eq var (cnd-value cnd))
            (predicate-value-type rel-pred)
        )
        (t nil)
    )
)


(defun get-is-type (var-type &aux type-name)
    (setq type-name
        (intern 
            (concatenate 'string 
                "IS-" 
                (symbol-name var-type)
            ) 
            :artue
        )
    )
    (setf (get type-name 'is) var-type)
    type-name
)

(defun fact-is-is (fact)
    (and (type-is-is (fact-type fact)) (fact-value fact))
)

(defun cnd-is-is (cnd)
    (type-is-is (cnd-type cnd))
)

(defun type-is-is (type-name)
    (get type-name 'is)
)

(defun var-decl-to-cnd (decl model &aux typ)
    (setq typ (second decl))
    (when (is-numeric-type model typ)
        (when (member typ (get-subtypes model 'int))
            (setq typ 'int)
        )
        (when (member typ (get-subtypes model 'real))
            (setq typ 'real)
        )
    )
    (make-cnd 
        :type (get-is-type typ)
        :args (list (first decl))
        :value t
    )
)

(defun make-is-facts (model decl &aux typ (ret nil))
    (setq typ (second decl))
    (when (is-numeric-type model typ)
        (when (member typ (get-subtypes model 'int))
            (setq typ 'int)
        )
        (when (member typ (get-subtypes model 'real))
            (setq typ 'real)
        )
    )
    (cons 
        (make-fact
            :type (get-is-type typ)
            :args (list (first decl))
            :value t
        )
        (mapcan 
            #'(lambda (supertype) (make-is-facts model (list (first decl) supertype)))
            (get-direct-supertypes model typ)
        )
    )
)

(defun add-fact-types (envmodel ev-model preds)
    (let ((var-types (find-model-var-types envmodel ev-model preds)))
        (setf (model-conditions ev-model)
            (append (model-conditions ev-model)
                    (mapcar 
                        #'(lambda (decl) (var-decl-to-cnd decl envmodel)) 
                        (remove-if 
                            #'(lambda (var-type) (created-in (car var-type) ev-model))
                            var-types
                        )
                    )
            )
        )
        (setf (model-var-types ev-model) var-types)
        (setf (model-variables ev-model) 
            (append (model-variables ev-model) (set-difference (mapcar #'car var-types) (model-variables ev-model)))
        )
    )
    ev-model
)

(defun created-in (var ev-model)
    (dolist (cnd (model-conditions ev-model))
        (when (and (eq (cnd-type cnd) 'create) (eq (second (cnd-value cnd)) var))
            (return-from created-in t)
        )
    )
    nil
)

(defun get-predicates (envmodel)
    (append 
        (list (make-predicate :name 'number :arg-types '((?value real)) :value-type 'real)) 
        (list (make-predicate :name 'real-var :arg-types '((?value real)) :value-type 'real)) 
        (list (make-predicate :name 'is-string :arg-types '((?value string)))) 
        (list (make-predicate :name 'is-real :arg-types '((?value real)))) 
        (list (make-predicate :name 'is-int :arg-types '((?value int)))) 
        (list (make-predicate :name '_win :arg-types '((?condition int)))) 
        (list (make-predicate :name 'adds-to :arg-types '((?_a real) (?_b real)) :value-type 'real)) 
        (get-standard-predicates envmodel) 
        (get-function-predicates envmodel)
    )
)

(defun create-action-models (envmodel)
    (apply #'append
        (build-event-model '(noop)
            (get-predicates envmodel)
            envmodel
        )
        (mapcar+ #'build-event-model 
            (envmodel-actions envmodel) 
            (get-predicates envmodel)
            envmodel
        )
    )
)

(defun create-event-models (envmodel)
    (apply #'append
        (mapcar+ #'build-event-model 
            (envmodel-events envmodel) 
            (get-predicates envmodel)
            envmodel
        )
    )
)

(defun get-standard-predicates (envmodel)
    (mapcar #'read-predicate (envmodel-predicates envmodel))
)

(defun read-predicate (lst)
    (make-predicate 
        :name (first lst) 
        :arg-types (make-type-declarations (rest lst))
    )
)

(defun get-function-predicates (envmodel &aux ptr cur-preds all-preds)
    (setq ptr (envmodel-functions envmodel))
    (setq cur-preds nil)
    (setq all-preds nil)
    (loop while ptr do
        (cond
            ((eq (car ptr) '-)
                (dolist (pred cur-preds)
                    (setf (predicate-value-type pred) (second ptr))
                )
                (push-all cur-preds all-preds)
                (setq cur-preds nil)
                (setq ptr (cddr ptr))
            )
            (t
                (push (read-predicate (car ptr)) cur-preds)
                (setq ptr (cdr ptr))
            )
        )
    )
    all-preds
)

;; We handle increases and decreases by including a condition for the initial
;; value, with a variable, an adds-to special condition, and an after-value
;; in the post-conditions. In all, two extra variables are added for each increase
;; or decrease.
(defun process-pddl-effect-to-preconditions (pddl-effect)
    (when (not (member (first pddl-effect) '(increase decrease)))
        (return-from process-pddl-effect-to-preconditions nil)
    )
    (when (or (not (eq (length pddl-effect) 3))
              (not (listp (second pddl-effect)))
          )
        (error "Change badly formed: ~A" pddl-effect)
    )
    (list
        (make-cnd
            :type (first (second pddl-effect))
            :args (rest (second pddl-effect))
            :value (make-variable (second pddl-effect) "BEFORE")
        )
        (if (eq (first pddl-effect) 'increase)
            ;then
            (make-cnd
                :type 'adds-to
                :args (list (third pddl-effect) (make-variable (second pddl-effect) "BEFORE"))
                :value (make-variable (second pddl-effect) "AFTER")
            )
            ;else (decrease)
            (make-cnd
                :type 'adds-to
                :args (list (third pddl-effect) (make-variable (second pddl-effect) "AFTER"))
                :value (make-variable (second pddl-effect) "BEFORE")
            )
        )
    )
)

(defun process-pddl-effect-to-postcondition (pddl-effect &aux new-cnd)
    (setq new-cnd
        (case (first pddl-effect)
            (set 
                (when (or (not (eq (length pddl-effect) 3))
                          (not (listp (second pddl-effect)))
                      )
                    (error "Change badly formed: ~A" pddl-effect)
                )
                (make-cnd
                    :type (first (second pddl-effect))
                    :args (rest (second pddl-effect))
                    :value (third pddl-effect)
                )
            )
            ((increase decrease) 
                (when (or (not (eq (length pddl-effect) 3))
                          (not (listp (second pddl-effect)))
                      )
                    (error "Change badly formed: ~A" pddl-effect)
                )
                (make-cnd
                    :type (first (second pddl-effect))
                    :args (rest (second pddl-effect))
                    :value (make-variable (second pddl-effect) "AFTER")
                )
            )
            (not 
                (when (or (not (eq (length pddl-effect) 2))
                          (not (listp (second pddl-effect)))
                      )
                    (error "Change badly formed: ~A" pddl-effect)
                )
                (make-cnd
                    :type (first (second pddl-effect))
                    :args (rest (second pddl-effect))
                    :value nil
                )
            )
            (t
                (when (some #'listp pddl-effect)
                    (error "Change badly formed: ~A" pddl-effect)
                )
                (make-cnd
                    :type (first pddl-effect)
                    :args (rest pddl-effect)
                    :value t
                )
            )
        )
    )
    new-cnd
)

(defun process-pddl-to-condition (pddl-cond envmodel)
  (setq pddl-cond (copy-list pddl-cond))
  (list
    (case (first pddl-cond)
        ((or and) (error "Should have gotten rid of these."))
        ((>= <= < > =)
            (when (not (eq (length pddl-cond) 3))
                (error "Inequality badly formed: ~A" pddl-cond)
            )
            (when (and (listp (second pddl-cond)) (listp (third pddl-cond)))
                (let ((new-var (gensym "?INEQ")))
                    (return-from process-pddl-to-condition
                        (append
                            (if (member (car (third pddl-cond)) (remove-if-not #'listp (envmodel-functions envmodel)) :key #'first)
                                ;then
                                (process-pddl-to-condition
                                    (list 'eq (third pddl-cond) new-var)
                                    envmodel
                                )
                                ;else
                                (process-pddl-to-condition
                                    (list 'assign new-var (third pddl-cond))
                                    envmodel
                                )
                            )
                            (process-pddl-to-condition
                                (list (first pddl-cond) (second pddl-cond) new-var)
                                envmodel
                            )
                        )
                    )
                )
            )
            (when (not (listp (second pddl-cond)))
                (cond 
                    ((listp (third pddl-cond)) 
                        (setq pddl-cond (cons (car pddl-cond) (reverse (cdr pddl-cond)))))
                    ((and (plan-var-p (second pddl-cond)) (numberp (third pddl-cond))) 
                        (return-from process-pddl-to-condition
                            (list
                                (switch-inequality
                                    (only 
                                        (process-pddl-to-condition
                                            (list 
                                                (car pddl-cond) 
                                                (list 'number (third pddl-cond)) 
                                                (second pddl-cond) 
                                            )
                                            envmodel
                                        )
                                    )
                                )
                            )
                        )
                    )
                    ((and (numberp (second pddl-cond)) (numberp (third pddl-cond)))
                        (error "Inequality not ambiguous: ~A" pddl-cond))
                    ((numberp (second pddl-cond)) 
                        (setf (second pddl-cond) (list 'number (second pddl-cond))))
                    ((plan-var-p (second pddl-cond)) 
                        (setf (second pddl-cond) (list 'real-var (second pddl-cond))))
                    (t (error "Arguments of inequality not understood: ~A" pddl-cond))
                )
            )
            (make-cnd
                :inequality (symbol-function (first pddl-cond))
                :type (first (second pddl-cond))
                :args (rest (second pddl-cond))
                :value (third pddl-cond)
            )
        )
        (not 
            (when (or (not (eq (length pddl-cond) 2))
                      (not (listp (second pddl-cond)))
                  )
                (error "Negation badly formed: ~A" pddl-cond)
            )
            (make-cnd
                :type (first (second pddl-cond))
                :args (rest (second pddl-cond))
                :value nil
            )
        )
        (eq 
            (when (or (not (eq (length pddl-cond) 3))
                      (not (listp (second pddl-cond)))
                  )
                (error "Inequality badly formed: ~A" pddl-cond)
            )
            (make-cnd
                :type (first (second pddl-cond))
                :args (rest (second pddl-cond))
                :value (third pddl-cond)
            )
        )
        (neq 
            (when (or (not (eq (length pddl-cond) 3))
                      (not (listp (second pddl-cond)))
                  )
                (error "Inequality badly formed: ~A" pddl-cond)
            )
            (make-cnd
                :inequality #'neq
                :type (first (second pddl-cond))
                :args (rest (second pddl-cond))
                :value (third pddl-cond)
            )
        )
        (assign
            (multiple-value-bind (cnds calc)
                (find-evaluation-sub-conditions (rest pddl-cond) envmodel)
                        
                (return-from process-pddl-to-condition
                    (append
                        cnds
                        (list
                            (make-cnd
                                :type 'assign
                                :args nil
                                :value calc
                            )
                        )
                    )
                )
            )
        )
        (create
            (return-from process-pddl-to-condition
                (list
                    (make-cnd
                        :type 'create
                        :args nil
                        :value (rest pddl-cond)
                    )
                    (make-cnd
                        :type (get-is-type (second pddl-cond))
                        :args (list (third pddl-cond))
                        :value nil
                    )
                )
            )
        )
        (is
                    (make-cnd
                        :type (get-is-type (second pddl-cond))
                        :args (list (third pddl-cond))
                        :value t
                    )
        )
        (eval
            (multiple-value-bind (cnds calc)
                (find-evaluation-sub-conditions pddl-cond envmodel)
                        
                (return-from process-pddl-to-condition
                    (append
                        cnds
                        (list
                            (make-cnd
                                :type 'eval
                                :args nil
                                :value calc
                            )
                        )
                    )
                )
            )
        )
        (forall 
            (cond
                ((eq 4 (length pddl-cond))
                    (make-cnd
                        :type 'forall 
                        :args nil
                        :value 
                            (list 'if 
                                (apply #'append
                                    (mapcar+ #'process-pddl-to-condition 
                                        (only (cnf (third pddl-cond)))
                                        envmodel
                                    )
                                )
                                (apply #'append
                                    (mapcar+ #'process-pddl-to-condition 
                                        (only (cnf (fourth pddl-cond)))
                                        envmodel
                                    )
                                )
                            )
                    )
                )
                ((eq 3 (length pddl-cond))
                    (make-cnd
                        :type 'forall
                        :args nil
                        :value (cnf (third pddl-cond))
                    )
                )
                (t (error "Did not expect forall of length ~A." (length pddl-cond)))
            )
        )
        (t
            (when (some #'listp pddl-cond)
                (error "Badly formed literal: ~A" pddl-cond)
            )
            (make-cnd
                :type (first pddl-cond)
                :args (rest pddl-cond)
                :value t
            )
        )
    )
  )
)

(defun process-pddl-cond-to-effect (pddl-cond envmodel)
    (if (eq (car pddl-cond) 'create)
        ;then
        (list
            (make-cnd 
                :type (get-is-type (second pddl-cond))
                :args (list (third pddl-cond))
                :value t
            )
        )
        ;else
        nil
    )
)

(defun find-evaluation-sub-conditions (calc envmodel)
    (let*
      ((func-names (mapcar #'predicate-name (get-function-predicates envmodel)))
       (func-calls 
           (remove-duplicates
               (find-in-tree 
                   calc
                   #'(lambda (lst) (and (listp lst) (member (car lst) func-names)))
               )
               :test #'equal
           )
       )
       (ret-cnds nil)
      )
        (dolist (func-call func-calls)
            (let ((new-sym (make-current-value-variable func-call)))
                (push
                    (make-cnd
                        :type (car func-call)
                        :args (rest func-call)
                        :value new-sym
                    )
                    ret-cnds
                )
                (setq calc (subst new-sym func-call calc :test #'equal))
            )
        )
        (values ret-cnds calc)
    )
)

(defun make-current-value-variable (func)
    (when (fact-p func)
        (setq func (cons (fact-type func) (fact-args func)))
    )
    (apply #'combine-strings-symbols 
        (append (list "?__VAL-" (first func))
            (apply #'append 
                (mapcar #'(lambda (arg) (list "-" arg)) (cdr func))
            )
        )
    )
)

(defun make-old-value-variable (func)
    (when (cnd-p func)
        (setq func (cons (cnd-type func) (cnd-args func)))
    )
    (when (fact-p func)
        (setq func (cons (fact-type func) (fact-args func)))
    )
    (apply #'combine-strings-symbols 
        (append (list "?__OLD-VAL-" (first func))
            (apply #'append 
                (mapcar #'(lambda (arg) (list "-" arg)) (cdr func))
            )
        )
    )
)

(defun all-pairs (lst)
    (if (cdr lst)
        ;then
        (append
            (mapcar+ #'list (rest lst) (first lst))
            (all-pairs (rest lst))
        )
        ;else
        nil
    )
)

(defun cnf (and-or-tree &optional (negative nil))
    (cond
        ((null and-or-tree) (list nil))
        ((eq (car and-or-tree) 'oneof)
            (let 
              ((expansion 
                (cons 'or
                    (mapcar 
                        #'(lambda (x) 
                            `(and ,x 
                                ,@(mapcar #'(lambda (subtree) (list 'not subtree))
                                    (remove x (cdr and-or-tree))
                                  )
                             )
                          )
                        (cdr and-or-tree)
                    )
                )
              ))
                (if negative (cnf (list 'not expansion)) (cnf expansion))
            )
        )
        ((eq (car and-or-tree) 'not)
            (cnf (second and-or-tree) (not negative))
        )
        ((eq (car and-or-tree) '<=)
            (if negative
                ;then
                (list (list (cons '> (cdr and-or-tree))))
                ;else
                (list (list and-or-tree))
            )
        )
        ((eq (car and-or-tree) '>=)
            (if negative
                ;then
                (list (list (cons '< (cdr and-or-tree))))
                ;else
                (list (list and-or-tree))
            )
        )
        ((eq (car and-or-tree) '<)
            (if negative
                ;then
                (list (list (cons '>= (cdr and-or-tree))))
                ;else
                (list (list and-or-tree))
            )
        )
        ((eq (car and-or-tree) '>)
            (if negative
                ;then
                (list (list (cons '<= (cdr and-or-tree))))
                ;else
                (list (list and-or-tree))
            )
        )
        ((eq (car and-or-tree) 'or)
            (if negative
                ;then
                (cross-product
                    (mapcar+ #'cnf
                       (cdr and-or-tree)
                       t
                    )
                )
                ;else
                (apply #'append
                   (mapcar #'cnf
                       (cdr and-or-tree)
                   )
                )
            )
        )
        ((eq (car and-or-tree) 'and)
            (if negative
                ;then
                (apply #'append
                   (mapcar+ #'cnf
                       (cdr and-or-tree)
                       t
                   )
                )
                ;else
                (cross-product
                    (mapcar #'cnf
                           (cdr and-or-tree)
                    )
                )
            )
        )
        ((eq (car and-or-tree) 'eval)
            (if negative
                ;then
                (list (list (list 'not and-or-tree)))
                ;else
                (list (list and-or-tree))
            )
        )
        ((eq (car and-or-tree) 'assign)
            (when negative
                (error "Can't negate an assign.")
            )
            (list (list and-or-tree))
        )
        ((eq (car and-or-tree) 'create)
            (when negative
                (error "Can't negate a create.")
            )
            (list (list and-or-tree))
        )
        ((eq (car and-or-tree) 'forall) ; becomes a normal fact
            (when negative
                (error "not handling this case yet.")
            )
            (list (list and-or-tree))
        )
        ((eq (car and-or-tree) 'eq) 
            (if negative
                ;then
                (list (list (cons 'neq (cdr and-or-tree))))
                ;else
                (list (list and-or-tree))
            )
        )
        ((eq (car and-or-tree) 'neq)
            (if negative
                ;then
                (list (list (cons 'eq (cdr and-or-tree))))
                ;else
                (list (list and-or-tree))
            )
        )
        ((all-atoms and-or-tree) 
            (if negative
                ;then
                (list (list (list 'not and-or-tree)))
                ;else
                (list (list and-or-tree))
            )
        )
        
        ((member (car and-or-tree) '(decrease increase)) 
            (when negative
                (error "Decreases and increases should not be negated.")
            )
            (list (list and-or-tree))
        )
        (t (error "Did not expect ~A" (car and-or-tree)))
    )
)

(defun dnf (and-or-tree &optional (negative nil))
    (cond
        ((null and-or-tree) (list nil))
        ((eq (car and-or-tree) 'oneof)
            (let 
              ((expansion 
                  (append
                    (list 'and (cons 'or (cdr and-or-tree)))
                    (apply #'append
                        (mapcar 
                            #'(lambda (x) 
                                (mapcar 
                                    #'(lambda (y) 
                                        `(or (not ,x) (not ,y))
                                      )
                                    (cdr (member x and-or-tree)) 
                                )
                              )
                            (cdr and-or-tree)
                        )
                    )
                )
              ))
                (break "~%expansion: ~A" expansion)
                (if negative (dnf (list 'not expansion)) (dnf expansion))
            )
        )
        ((eq (car and-or-tree) 'not)
            (dnf (second and-or-tree) (not negative))
        )
        ((eq (car and-or-tree) '<=)
            (if negative
                ;then
                (list (list (cons '> (cdr and-or-tree))))
                ;else
                (list (list and-or-tree))
            )
        )
        ((eq (car and-or-tree) '>=)
            (if negative
                ;then
                (list (list (cons '< (cdr and-or-tree))))
                ;else
                (list (list and-or-tree))
            )
        )
        ((eq (car and-or-tree) '<)
            (if negative
                ;then
                (list (list (cons '>= (cdr and-or-tree))))
                ;else
                (list (list and-or-tree))
            )
        )
        ((eq (car and-or-tree) '>)
            (if negative
                ;then
                (list (list (cons '<= (cdr and-or-tree))))
                ;else
                (list (list and-or-tree))
            )
        )
        ((eq (car and-or-tree) 'or)
            (if negative
                ;then
                (apply #'append
                   (mapcar+ #'dnf
                       (cdr and-or-tree)
                       t
                   )
                )
                ;else
                (cross-product
                    (mapcar #'dnf
                       (cdr and-or-tree)
                    )
                )
            )
        )
        ((eq (car and-or-tree) 'and)
            (if negative
                ;then
                (cross-product
                    (mapcar+ #'dnf
                       (cdr and-or-tree)
                       t
                    )
                )
                ;else
                (apply #'append
                   (mapcar #'dnf
                       (cdr and-or-tree)
                   )
                )
            )
        )
        ((eq (car and-or-tree) 'eval)
            (if negative
                ;then
                (list (list (list 'not and-or-tree)))
                ;else
                (list (list and-or-tree))
            )
        )
        ((eq (car and-or-tree) 'assign)
            (when negative
                (error "Can't negate an assign.")
            )
            (list (list and-or-tree))
        )
        ((eq (car and-or-tree) 'create)
            (when negative
                (error "Can't negate a create.")
            )
            (list (list and-or-tree))
        )
        ((eq (car and-or-tree) 'forall) ; becomes a normal fact
            (when negative
                (error "not handling this case yet.")
            )
            (list (list and-or-tree))
        )
        ((eq (car and-or-tree) 'eq) 
            (if negative
                ;then
                (list (list (cons 'neq (cdr and-or-tree))))
                ;else
                (list (list and-or-tree))
            )
        )
        ((eq (car and-or-tree) 'neq)
            (if negative
                ;then
                (list (list (cons 'eq (cdr and-or-tree))))
                ;else
                (list (list and-or-tree))
            )
        )
        ((all-atoms and-or-tree) 
            (if negative
                ;then
                (list (list (list 'not and-or-tree)))
                ;else
                (list (list and-or-tree))
            )
        )
        
        ((member (car and-or-tree) '(decrease increase)) 
            (when negative
                (error "Decreases and increases should not be negated.")
            )
            (list (list and-or-tree))
        )
        (t (error "Did not expect ~A" (car and-or-tree)))
    )
)

(defun all-atoms (atom-list)
    (and 
        (listp atom-list)
        (not (find-if-not #'atom atom-list))
    )
)

(defun is-non-constraining-cnd (cnd)
    (or (eq (cnd-type cnd) 'adds-to)
        (and (symbolp (cnd-value cnd)) (get (cnd-value cnd) 'non-constraining))
        (type-is-is (cnd-type cnd))
    )
)

(defun make-variable (pddl-ref differentiation-string &aux ret-string cur-char)
    (setq ret-string (format nil "?~A-~A" pddl-ref differentiation-string))
    (loop for i from 1 to (1- (length ret-string)) do
        (setq cur-char (char ret-string i))
        (when (not (alphanumericp cur-char))
            (setf (char ret-string i) #\-)
        )
    )
    (let ((var (intern ret-string)))
        (setf (get var 'non-constraining) t)
        var
    )
)


(defun add-default-facts (expgen prob-objs facts)
    (let ((defaults (get-all-defaults expgen prob-objs)))
        (append
            (set-difference defaults facts :test #'same-map)
            facts
        )
    )
)

(defun get-all-defaults (expgen prob-objs 
                         &aux eligible-preds pred pred-types cnds facts 
                              (ret nil) (envmodel (get-last-envmodel expgen))
                        )
    (setq facts (convert-problem-objects prob-objs envmodel))
    ;; Not interested in binary facts
    (setq eligible-preds (remove nil (get-predicates envmodel) :key #'predicate-value-type))
    
    (dolist (fact-type (mapcar #'first (envmodel-defaults envmodel)))
        (setq pred (find fact-type eligible-preds :key #'predicate-name))
        (when pred
            (setq pred-types (predicate-arg-types pred))
            (setq cnds
                (cons (make-cnd :type fact-type :args (mapcar #'first pred-types) :value '?_val)
                    (mapcar+ #'var-decl-to-cnd pred-types envmodel)
                )
            )
            (dolist (blist (unify-set envmodel cnds facts nil :assume-fn #'default-closed-world-assume-fn))
                (push (make-fact 
                        :type fact-type 
                        :args (sublis blist (mapcar #'first pred-types))
                        :value (cdr (assoc '?_val blist))
                      )
                    ret
                )
            )
        )
    )
    ret
)

