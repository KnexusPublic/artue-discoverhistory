 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: 2015
 ;;;; Project Name: Explanation
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file performs projection over an explanation to ensure that 
 ;;;; all implied events are present.


(in-package :artue)

(defvar *temp-occs* nil) ;For debugging only.
(defvar *temp-facts* nil) ;For debugging only.
(defvar *temp-fact-occs* nil) ;For debugging only.
(defvar *ne* nil) ;For debugging only.
(defvar *use-maxima* t)

;; Activates make-inconsistencies break point for particular explanation layer.
(defvar *break-at-pt* nil)

;; This function constructs a complete explanation given only the initial set of
;; zero-time events (initial assumptions).
(defun construct-explanation (expgen assumptions)
    (extend-explanation expgen (make-explanation) assumptions)
)

;; May be sped up by skipping points when nothing happens.
(defun extend-explanation (expgen new-exp assumptions
                           &aux history-ptr facts wait-for-occ new-occs fact-occs
                                pt recent-events awaited-event duration start-pt)
    ;(setq new-exp (copy-explanation new-exp))
    (setf (explanation-cached-hidden-results new-exp) nil)
    (when (or (explanation-new-events new-exp) (explanation-event-removals new-exp))
        (setq new-exp (reset-explanation expgen new-exp :reuse t))
    )

    (setq start-pt (explanation-valid-event-pt new-exp))
    (setq assumptions (append assumptions (get-assumptions new-exp)))
    (when (null start-pt)
        (let ((zero-events))
            (setq start-pt (get-first-point expgen))
            (trim-cache expgen new-exp start-pt)
            (setq zero-events (remove start-pt assumptions :key #'occurrence-point :test-not #'eq))
            (setf (explanation-prior-events new-exp) zero-events)
            (setf (explanation-new-events new-exp) nil)
            (multiple-value-setq (facts fact-occs) 
                (advance-facts expgen 
                    (cons (car (get-execution-history expgen)) zero-events) 
                    (make-fact-set) 
                    (make-fact-occs) 
                    new-exp
                    start-pt
                )
            )
        )
    )
    
    (when (null facts)
        (setq facts (copy-fact-set (facts-at expgen new-exp start-pt)))
        (setq fact-occs (make-fact-occs :prior (fact-occs-at expgen new-exp start-pt)))
    )

    (setf (explanation-prior-events new-exp)
        (union assumptions
            (remove start-pt (get-events new-exp) 
                :key #'occ-last-point :test #'point-earlier
            )
        )
    )

    (setq start-pt (point-next expgen start-pt))
    (trim-cache expgen new-exp start-pt)
    (when (null start-pt)
        (return-from extend-explanation (values new-exp (get-facts facts)))
    )
    
    ;; Assumptions list contains ongoing intervals for simplicity in finding 
    ;; non-triggered occs.
    (setq assumptions 
        (append assumptions
            (remove-if-not
                #'(lambda (occ) 
                    (and (occurrence-interval occ)
                         (point-earlier (occ-first-point occ) start-pt)
                         (not (point-later start-pt (occ-last-point occ)))
                    )
                  )
                (explanation-prior-events new-exp)
            )
        )
    )
    
    (setf (explanation-new-events new-exp) nil)
    
    (if (eq (occurrence-point (get-last-observation expgen)) t)
        ;then
        (setq history-ptr 
            (append 
                (remove start-pt (butlast (get-execution-history expgen)) :key #'occurrence-point :test #'point-later)
                (list (get-last-observation expgen))
            )
        )
        ;else
        (setq history-ptr (remove start-pt (get-execution-history expgen) :key #'occurrence-point :test #'point-later))
    )
    

    ;;Sets up loop invariant to start with an event 
    (when (and history-ptr (occurrence-is-observation (car history-ptr))
               (eq start-pt (occurrence-point (car history-ptr)))
          )
        (multiple-value-setq (facts fact-occs)
            (advance-facts expgen (list (car history-ptr)) facts fact-occs new-exp (occurrence-point (car history-ptr)))
        )
        (pop history-ptr)
        (when (null history-ptr)
            (return-from extend-explanation (values new-exp (get-facts facts)))
        )
        (setq start-pt (point-next expgen start-pt)) 
    )
    
    (when (is-action-pt expgen start-pt)
        ;; MCM: I'd like to try each occurrence being projected only at the start 
        ;; of its interval, during projection only. Therefore, I changed actions 
        ;; to occur then but have *not* tested it.
        (setq new-occs (remove-if-not #'(lambda (occ) (occurrence-should-project-at expgen occ start-pt)) assumptions))
        (when (notevery #'occurrence-is-action new-occs)
            (error "Non-action occurs at action occurrence point ~A in assumptions." start-pt)
        )
            
        (loop while (and history-ptr (eq (occurrence-point (car history-ptr)) start-pt)) do
            (push (pop history-ptr) new-occs)
            (when (not (occurrence-is-action (car new-occs)))
                (error "Expected action next in history.")
            )
        )
    
        (multiple-value-setq (facts fact-occs)
            (advance-facts expgen new-occs facts fact-occs new-exp start-pt)
        )
        (setq start-pt (point-next expgen start-pt)) 
    )
    (when (null start-pt)
        (return-from extend-explanation (values new-exp (get-facts facts)))
    )

    (setq pt (point-prior expgen start-pt))
    
    (setq recent-events nil)
    (setq duration 0)
    ;;Loop assumes that the next point will be an event point.
    (loop while history-ptr do

        ;; Sets markers for duration of waits.
        (when (and new-occs (eq (car (occurrence-signature (car new-occs))) '!wait-for))
            (setq awaited-event (second (occurrence-signature (car new-occs))))
            (setq wait-for-occ (car new-occs))
            (setq duration 100000)
        )
        (when (and new-occs (eq (car (occurrence-signature (car new-occs))) '!wait))
            (setq duration (second (occurrence-signature (car new-occs))))
        )
          
        (multiple-value-setq (new-exp new-occs pt)
            (extend-with-events expgen new-exp facts fact-occs assumptions pt)
        )
            
        (when (and new-occs (not (is-event-pt expgen pt)))
            (warn "Found events at a non-event point.")
            (return-from extend-explanation (values 'fail nil))
        )

        ;; If one of the most recent events is the event we wait for, both 
        ;; modify the event occurrence to indicate how much time actually 
        ;; passed and modify the duration to stop process checking.
        (when (find awaited-event new-occs :key #'occurrence-type)
            (modify-occurrence-signature wait-for-occ
                (list '!wait (- 100000 duration))
            )
            (setq duration 0)
        )

        (multiple-value-setq (new-occs recent-events facts fact-occs pt)
            (check-for-event-cycles expgen new-exp new-occs recent-events history-ptr facts fact-occs duration pt)
        )

        (when (eq pt t)
            ;;Handles corner case where event cycle in history precedes a phantom 
            ;;observation.
            (push-all new-occs (explanation-new-events new-exp))
            (return-from extend-explanation (values new-exp (get-facts facts)))
        )
        
        ;; Check for infinite loop; this gross check is intended as a catch all 
        ;; when the cycle check fails.
        (when (> pt 10000)
            (break "Never-ending explanation.")
        )
        
        ;;Check for events overlapping the observation (contradicts 
        ;; the enumerated events).
        (when (and (numberp (occurrence-point (car history-ptr)))
                   (> pt (occurrence-point (car history-ptr)))
              )
            (warn "Events continue into history points.")
            (return-from extend-explanation (values 'fail nil))
        )

        ;; Now that we've handled any cycles, integrate the predicted events 
        ;; with the explanation.
        (push-all (set-difference new-occs assumptions) 
            (explanation-new-events new-exp)
        )
        (multiple-value-setq (facts fact-occs)
            (advance-facts expgen 
                (append new-occs (get-ongoing-old-occs expgen new-exp pt)) 
                facts fact-occs new-exp pt
            )
        )

        (when (and (<= duration .00001) (null new-occs))
            (when (eq (occurrence-point (car history-ptr)) t)
                (multiple-value-setq (new-occs facts fact-occs history-ptr pt)
                    (extend-with-history expgen new-exp history-ptr assumptions facts fact-occs)
                )
                (return-from extend-explanation (values new-exp (get-facts facts)))
            )
            ;(format t "~%Jumping from ~A " pt)
            (setq pt 
                (point-prior expgen
                    (apply #'min
                        (occurrence-point (car history-ptr))
                        (mapcar #'occ-last-point (get-ongoing-occs expgen new-exp (point-next expgen pt)))
                    )
                )
            )
            ;(format t "to ~A.~%" pt)
            (finish-output)
            ;(break "jump")
        )
        
        ;; This handles the end of an instantaneous event chain, when no waiting 
        ;; is occurring (i.e., no processes must be predicted).
        (when (and (<= duration .00001) (null new-occs) ;; 6/10/16 Added null new-occs check because otherwise not end of event chain
                   (point-equal (point-next expgen pt) (occurrence-point (car history-ptr)))
              )
            (setq recent-events nil)
            (multiple-value-setq (new-occs facts fact-occs history-ptr pt)
                (extend-with-history expgen new-exp history-ptr assumptions facts fact-occs)
            )
            
        )

        ;; This handles the end of an instantaneous event chain, when waiting 
        ;; is occurring (i.e., process effects must be determined and time must 
        ;; be advanced to next event to occur).
        (when (and (null (remove-if #'occurrence-interval (remove 'event-cycle new-occs :key #'occurrence-type))) (> duration .00001))
            (setq recent-events nil)
            (multiple-value-setq (new-occs facts fact-occs duration pt)
                (extend-with-processes expgen new-exp facts fact-occs duration pt)
            )
        )
        (setf (explanation-valid-event-pt new-exp) pt)
        (setf (explanation-valid-fact-cache-pt new-exp) pt)
    )

    (values new-exp (get-facts facts))
)

(defun extend-with-events (expgen new-exp facts fact-occs assumptions pt &aux (new-occs nil) (cur-occs nil) (ongoing-occs nil))
    ;; Advances the time pointer.
    (if (null (point-next expgen pt))
        ;then
        (setq pt (next-point expgen :event))
        ;else
        (setq pt (point-next expgen pt))
    )
    
    ;; Generate next set of event occurrences: assumptions plus all events 
    ;; implied by current (believed) state.
    (setq cur-occs (remove-if-not #'(lambda (occ) (occurrence-should-project-at expgen occ pt)) assumptions))
    (loop for ev-model in (find-allowed-ev-models expgen new-exp) do
        (when (not (numberp (event-model-likelihood ev-model)))
            (push-all 
                (enumerate-occs expgen ev-model facts pt 
                    :assume-fn #'default-closed-world-variable-inequality-assume-fn
                    ;:ground-params t
                )
                new-occs
            )
        )
    )
    (dolist (occ new-occs)
        (setf (occurrence-old occ) t)
    )
    
    (setq ongoing-occs (get-ongoing-old-occs expgen new-exp pt))
    
    (dolist (ev ongoing-occs)
        (when (not (could-occur-if ev facts fact-occs pt))
            ;(break "Could-occur-if still useful.")
            (setq ongoing-occs (remove ev ongoing-occs))
            (multiple-value-setq (new-exp ev)
                (replace-interval expgen new-exp ev (interval-range-start (occurrence-interval ev)) (point-prior expgen pt))
            )
        )
    )
    

    (dolist (occ new-occs)
        (let ((prior-occ (find (occurrence-signature occ) ongoing-occs :key #'occurrence-signature :test #'equalp))
              ival
              )
            (when (null prior-occ)
                (push occ cur-occs)
            )
            (when prior-occ ;;(or (null prior-occ) (point-earlier (occ-last-point prior-occ) (occ-last-point occ)))
                (setq ival (get-limiting-interval expgen occ fact-occs pt))
                (cond 
                    ((null ival) (push occ cur-occs)) ;; Point occurrence, already finished
                    ((not (eq ival :fail))
                        (setf (occurrence-point occ) nil)
                        (setf (occurrence-interval occ) ival)
                        (push occ cur-occs)
                    )
                    (t 
                        ;; ival = :fail, so event can't happen at all
                    )
                )
            )
        )
    )
    ;(break "Checking events.")
    
    ;; For debugging; set *break-at-pt* to the point where you wish to 
    ;; start debugging, then call extend-explanation to see what happens.
    (when (eql *break-at-pt* pt)
        (setq *temp-facts* facts)
        (setq *temp-occs* cur-occs)
        (setq *temp-fact-occs* fact-occs)
        (setq *ne* new-exp)
        (break "At point ~A in construct-explanation." pt)
    )

    (values new-exp cur-occs pt)
)

(defun get-ongoing-occs (expgen ex pt)
    (remove-if-not #'(lambda (occ) (point-is-relevant-to expgen pt occ))
        (remove-if-not #'occurrence-interval (get-events ex))
    )
)

(defun get-ongoing-old-occs (expgen ex pt)
    (remove-if-not #'(lambda (occ) (point-is-relevant-to expgen pt occ))
        (remove-if-not #'occurrence-interval 
            (remove-if-not #'occurrence-old (get-events ex))
        )
    )
)

(defun get-limiting-interval (expgen ev fact-occs pt &aux causes end-pt)
    (setq causes (get-event-proximate-causes expgen ev fact-occs pt)) 
    (when (not (member (point-prior expgen pt) causes :key #'occ-first-point))
        ;; In this case, the proximate event did not just occur, so this 
        ;; shouldn't happen now at all. 
        (return-from get-limiting-interval :fail)
    )
    (setq end-pt 
        (apply #'max 
            (mapcar #'interval-range-end 
                (map-if #'occurrence-interval causes)
            )
        )
    )
    (if (and (point-next expgen end-pt) (is-event-pt expgen (point-next expgen end-pt)))
        ;then
        (setq end-pt (point-next expgen end-pt))
        ;else
        (loop while (not (is-event-pt expgen end-pt)) do
            (setq end-pt (point-prior expgen end-pt))
        )
    )
        
    (when (point-earlier pt end-pt) 
        (return-from get-limiting-interval
            (make-interval :range-start pt :range-end end-pt)
        )
    )
    nil
)

(defun get-event-proximate-causes (expgen ev fact-occs pt)
    (remove-if-not #'(lambda (occ) (point-is-relevant-to expgen (point-prior expgen pt) occ))
        (get-event-causes ev fact-occs)
    )
)
        
    
(defun get-event-causes (ev fact-occs)
    (remove nil 
        (apply #'append 
            (mapcar #'(lambda (fact) (get-most-recent-occs fact-occs fact))
                (occurrence-pre ev)
            )
        )
    )
)

(defun could-occur-if (occ fact-set fact-occs pt)
    (dolist (fact (occurrence-pre occ))
        (let ((contra-fact (find-in-fact-set fact fact-set :test #'contradicts)))
            (when (and contra-fact (not (find-in-fact-set fact fact-set :test #'equal-fact))
                       (remove nil (remove pt (get-most-recent-occs fact-occs contra-fact) :key #'occ-last-point :test-not #'point-later))
                  )
                (return-from could-occur-if nil)
            )
        )
    )
    t
)
        

(defun extend-with-history (expgen new-exp history-ptr assumptions facts fact-occs &aux cur-occs (pt nil))
    (setq cur-occs (list (pop history-ptr)))
    (when (and (occurrence-is-observation (car cur-occs)) (numberp (occurrence-point (car cur-occs))))
        (multiple-value-setq (facts fact-occs)
            (advance-facts expgen cur-occs facts fact-occs new-exp (occurrence-point (car cur-occs)))
        )
        (setq pt (point-next expgen (occurrence-point (car cur-occs))))
        (when (null pt)
            (when history-ptr
                (error "History left but no more points.")
            )
            (return-from extend-with-history (values cur-occs facts fact-occs history-ptr (occurrence-point (car cur-occs))))
        )
        (setq cur-occs nil)
        (loop while (and history-ptr (eql pt (occurrence-point (car history-ptr)))) do
            (push (pop history-ptr) cur-occs)
            (when (not (occurrence-is-action (car cur-occs)))
                (error "Expected action next in history.")
            )
        )
    )
    (when (null history-ptr)
        (setq pt 
            (apply #'min 100000
                (remove-if #'(lambda (assump-pt) (point-earlier assump-pt pt))
                    (mapcar #'occ-last-point assumptions)
                )
            )
        )
        
        (when (eql pt 100000)
            (setq pt (explanation-valid-fact-cache-pt new-exp))
        )

        (setq cur-occs nil)
    )
    (when (notevery #'occurrence-is-action cur-occs)
        (error "Expected only actions in list.")
    )
    (when cur-occs
        (setq pt (occurrence-point (car cur-occs)))
    )
    
    ;; MCM: Not sure if any occurrences have intervals in the history, but if 
    ;; so, this has not been completely tested.
    (push-all 
        (remove-if-not #'(lambda (occ) (occurrence-should-project-at expgen occ pt)) assumptions)
        cur-occs
    )
    (when cur-occs
        (when (notevery #'occurrence-is-action cur-occs)
            (error "Non-action occurs at action occurrence point ~A in assumptions (in loop)." pt)
        )
        (multiple-value-setq (facts fact-occs)
            (advance-facts expgen cur-occs facts fact-occs new-exp pt)
        )
    )
    
    (when (null pt) (error "How do we get through here without setting pt?"))
    
    (values cur-occs facts fact-occs history-ptr pt)
)

(defun extend-with-processes (expgen new-exp facts fact-occs duration pt &aux (cur-occs nil))
    (if (null (point-next expgen pt))
        ;then
        (setq pt (next-point expgen :process))
        ;else
        (setq pt (point-next expgen pt))
    )
    (multiple-value-bind (new-facts time-change orig-fact-values new-fact-values time-passes-occ)
            (advance-processes expgen facts duration pt)
        (declare (ignore new-facts orig-fact-values new-fact-values))
        (push time-passes-occ cur-occs)
        (push-all cur-occs (explanation-new-events new-exp))
        (multiple-value-setq (facts fact-occs)
            (advance-facts expgen cur-occs facts fact-occs new-exp pt)
        )
        ;(remove-all-from-fact-set orig-fact-values facts)
        ;(add-all-to-fact-set new-fact-values facts)
        (setq duration (- duration time-change))
    )
    
    (values cur-occs facts fact-occs duration pt)
)


(defun check-for-event-cycles (expgen new-exp cur-occs recent-events history-ptr facts fact-occs duration pt &aux repeated-occs earliest-pt)
    ;; The next bit is intended for handling cycles in event prediction.
    ;(push-all (remove-if #'occurrence-interval cur-occs) recent-events)
    (setq repeated-occs (cycle-occurring recent-events pt))
    (when (or (null cur-occs) (null repeated-occs))
        (return-from check-for-event-cycles (values cur-occs recent-events facts fact-occs pt))
    )
    (setq earliest-pt (occ-first-point (car (last repeated-occs))))
    (setq facts (copy-fact-set (facts-at expgen new-exp (point-prior expgen earliest-pt))))
    (setq fact-occs (copy-fact-occs (fact-occs-at expgen new-exp (point-prior expgen earliest-pt))))
    (trim-cache expgen new-exp earliest-pt)
    (setf (explanation-new-events new-exp) 
        (remove earliest-pt (explanation-new-events new-exp)
            :key #'occ-first-point
            :test-not #'point-later
        )
    )
    (push (make-event-cycle-event expgen repeated-occs earliest-pt) (explanation-new-events new-exp))
    (multiple-value-setq (facts fact-occs)
        (advance-facts expgen cur-occs facts fact-occs new-exp earliest-pt)
    )
    (when (< duration .00001)
        (setq pt (point-prior expgen (occurrence-point (car history-ptr))))
    )
    ; (when (not (get-learning-allowed expgen))
        ; ;(break "Event cycle found.")
    ; )
    (values nil recent-events facts fact-occs pt)
)

(defun make-event-cycle-event (expgen repeated-occs earliest-pt 
                               &aux (total-pre nil) (cur-pt earliest-pt) 
                                    cur-occs (total-post nil) (total-con nil)
                              )
    (loop while (not (point-later cur-pt (occ-first-point (car repeated-occs)))) do
        (setq cur-occs (remove cur-pt repeated-occs :key #'occ-first-point :test-not #'eql))
        (push-all 
            (set-difference
                (apply #'append (mapcar #'occurrence-pre cur-occs))
                total-post
                :test #'equal-fact
            )
            total-pre
        )
        (push-all
            (apply #'append (mapcar #'occurrence-post cur-occs))
            total-post
        )
        (push-all
            (apply #'append (mapcar #'occurrence-constraints cur-occs))
            total-con
        )
        (setq total-pre (remove-duplicates total-pre :test #'equal-fact))
        (setq total-post (remove-duplicates total-post :test #'equal-fact))
        (setq total-con (remove-duplicates total-con :test #'equal-cnd))
        (setq cur-pt (point-next expgen cur-pt))  
    )
    
    (make-occurrence
        :signature (cons 'event-cycle (mapcar #'occurrence-signature repeated-occs))
        :pre total-pre
        :post (list (make-fact :type 'paradox :args nil :value t))
        :constraints total-con
        :point earliest-pt
        :is-event t
    )
)    


(defun cycle-occurring (occ-hist pt &aux orig-pt safe-pt idx)
    (when (null occ-hist) (return-from cycle-occurring nil))
    (setq orig-pt (occ-first-point (car (last occ-hist))))
    (setq safe-pt (floor (+ pt orig-pt) 2))
    (loop while (> pt safe-pt) do
        (setq idx (position pt occ-hist :key #'occ-first-point :from-end t))
        (when (and idx (occurrence-cycle-has-formed (subseq occ-hist 0 (+ 1 idx)) occ-hist))
            (return-from cycle-occurring (subseq occ-hist (+ 1 idx) (* 2 (+ 1 idx))))
        )
        (decf pt)
    )
    nil
)

(defun occurrence-cycle-has-formed (new-occs occ-hist)
    (let ((idx (search new-occs occ-hist :test #'occurrence-is-repeat)) seq1 seq2)
        (when (null idx) 
            (return-from occurrence-cycle-has-formed nil)
        )
        (setq idx (+ idx (length new-occs)))
        (when (< (length occ-hist) (* idx 2))
            (return-from occurrence-cycle-has-formed nil)
        )
        ;; 7/15 (MCM): Only this bit is meaningfully different from the 
        ;; general cycle-has-formed in utils.
        (when (and (> (length occ-hist) (* idx 2))
                   (eql (occ-first-point (nth (* idx 2) occ-hist))
                        (occ-first-point (nth (1- (* idx 2)) occ-hist))
                   )
              )
            (return-from occurrence-cycle-has-formed nil)
        )
        (setq seq1 (subseq occ-hist 0 idx))
        (setq seq2 (subseq occ-hist idx (* idx 2)))
        (null (mismatch seq1 seq2 :test #'occurrence-is-repeat))
    )
)

;; Returns the set of facts after a set of simultaneous occurrences, provided
;; that a given set of facts were true before; also returns a mapping from a 
;; fact to the occurrence that last changed it
(defun advance-facts (expgen occs past-facts fact-occs ex pt &aux ret-facts ret-fact-occs)
    (when (null occs)
        (when (and ex (explanation-valid-fact-cache-pt ex))
            (setf (explanation-valid-fact-cache-pt ex) (max (explanation-valid-fact-cache-pt ex) pt))
            (when (point-earlier (explanation-valid-fact-cache-pt ex) (or (caar (explanation-cached-inconsistencies ex)) 0))
                (error "Out of order?")
            )
        )
        (return-from advance-facts (values past-facts fact-occs))
    )
    (when (equalp pt *break-at-pt*)
        (break "In advance-facts at ~A." pt)
    )
    (unless (null ex)
        (setq ret-facts (cdr (assoc pt (explanation-cached-facts ex) :test #'eql)))
        (unless (null ret-facts)
            (setq ret-facts (copy-fact-set ret-facts))
            (setq ret-fact-occs (make-fact-occs :prior (cdr (assoc pt (explanation-cached-fact-occs ex) :test #'eql))))
            (return-from advance-facts (values ret-facts ret-fact-occs))
        )
    )
    
    (setq fact-occs (make-fact-occs :prior fact-occs))

    (multiple-value-bind (new-facts new-fact-occs)
        ;; By not advancing facts to agree with the preconditions of events
        ;; and/or actions, we can ensure that inconsistencies are directly 
        ;; between observations and effects, which reduces the resolution steps
        ;; necessary during explanation.
        (if (some #'occurrence-is-observation occs)
            ;then
            (progn
                (remove-observables-from-fact-set past-facts 
                    (get-observable-types expgen) (get-hidden-facts expgen)
                )
                (advance-facts-phase expgen 
                    (remove-if #'occurrence-interval occs)
                    past-facts fact-occs ex #'occurrence-pre pt
                )
            )
            ;else
            (values past-facts fact-occs)
        )
        
        (multiple-value-setq (ret-facts ret-fact-occs)
            (advance-facts-phase expgen occs new-facts new-fact-occs ex #'occurrence-post pt)
        )
        (unless (null ex)
            (when (and (caar (explanation-cached-facts ex)) (point-earlier pt (caar (explanation-cached-facts ex))))
                (error "Bad insertion, can result when occurrences added to execution history out-of-order.")
            )
            (push (cons pt (copy-fact-set ret-facts)) 
                (explanation-cached-facts ex)
            )
            (push (cons pt ret-fact-occs) 
                (explanation-cached-fact-occs ex)
            )
            (when (not (numberp pt))
                (error "Bad value in final-pt.")
            )
            (setf (explanation-valid-fact-cache-pt ex) (max pt (or (explanation-valid-fact-cache-pt ex) 0)))
            (when (and pt (point-earlier (explanation-valid-fact-cache-pt ex) (or (caar (explanation-cached-inconsistencies ex)) 0)))
                (error "Out of order?")
            )
        )
    )
    (values ret-facts ret-fact-occs)
)

;; Breaks advance-facts down by the pre- and post-conditions.
(defun advance-facts-phase (expgen occs past-facts fact-occs ex phase-fn pt &aux facts)
    (when (not (fact-occs-p fact-occs))
        (error "Expected fact-occs object for first parameter to advance-facts-phase")
    )
    (when (not (typep past-facts 'fact-set))
        (error "Expected fact-set for second parameter to advance-facts-phase")
    )
;    (setq past-facts (copy-fact-set past-facts))
    (dolist (occ (sort-occs-by-precedence occs ex))
        (setq facts (funcall phase-fn occ))
        (when (occurrence-is-observation occ)
            (setq facts (mapcar+ #'bind-fact-if-necessary facts (explanation-bindings ex)))
            (setq facts
                (remove-if 
                    #'(lambda (fact) 
                        (and (variables-in-fact fact) 
                             (not (is-checked-type (get-last-envmodel expgen) (fact-type fact)))
                        )
                      )
                    facts
                )
            )
        )
        (dolist (fact facts)
            (let ((old-facts (find-all-in-fact-set fact past-facts :test #'same-map)) 
                  (old-fact nil) (non-preceding-setter nil))
                (cond
                    ((and (occurrence-point occ) (cdr old-facts))
                        (remove-all-from-fact-set old-facts past-facts)
                    )
                    ((occurrence-point occ) ;; 0 or 1 old-facts
                        (setq old-fact (car old-facts))
                    )
                    ((not (occurrence-interval occ))
                        (error "occurrence has neither point nor interval.")
                    )
                    ((equalp (occ-last-point occ) pt)
                        (dolist (former-fact old-facts)
                            (setq non-preceding-setter nil)
                            (dolist (former-occ (remove nil (get-most-recent-occs fact-occs former-fact)))
                                (setq non-preceding-setter
                                    (or non-preceding-setter
                                        (not (occurrence-must-precede ex former-occ occ))
                                    )
                                )
                            )
                            (when (not non-preceding-setter)
                                (remove-from-fact-set former-fact past-facts)
                            )
                        )
                        (setq old-fact (find fact old-facts :test #'equal-fact)) 
                    )
                    (t ;; during occurrence interval
                        (setq old-fact (find fact old-facts :test #'equal-fact))
                    )
                )

                (cond 
                    ((null old-fact)
                        (add-to-fact-set fact past-facts)
                    )
                    ((not (equalp (fact-value fact) (fact-value old-fact)))
                        (remove-from-fact-set old-fact past-facts)
                        (add-to-fact-set fact past-facts)
                    )
                )
            )
            (when (not (certain expgen fact))
                ;;certain facts are in every observation and don't cause inconsistencies.
                (set-most-recent-occ fact-occs fact occ)
            )
        )
    )
    (values past-facts (make-fact-occs :prior fact-occs))
)

(defun sort-occs-by-precedence (occs ex)
    (when (or (null ex) (null (explanation-occurrence-precedence ex)))
        (return-from sort-occs-by-precedence occs)
    )
    (append
        (remove-if #'occurrence-interval occs)
        (utils::partial-order-sort 
            (remove-if-not #'occurrence-interval occs)
            (explanation-occurrence-precedence ex)
        )
    )
)
        

(defun advance-processes (expgen facts duration pt
                          &aux changing-functions non-changing-facts 
                          orig-fact-values time-change new-func-value 
                          new-fact-values
                         )
;    (setq *facts* facts *duration* duration *pt* pt)
;    (break "Advance-processes")
    (setq changing-functions (get-changing-functions expgen facts))
    (setq orig-fact-values nil)
    (setq non-changing-facts nil)
    (dolist (fact (get-facts facts))
        (if (find fact changing-functions :test #'same-map)
            ;then
            (push fact orig-fact-values)
            ;else
            (push fact non-changing-facts)
        )
    )
;    (setq non-changing-facts (set-difference facts changing-functions :test #'same-map))
;    (setq orig-fact-values (set-difference facts non-changing-facts :test #'same-map))
    (let ((fset (make-fact-set)))
        (add-all-to-fact-set non-changing-facts fset)
        (add-all-to-fact-set changing-functions fset)
        
        (setq time-change 
            (apply #'min 
                (cons (+ duration 1)
                    (mapcar+ #'find-minimum-time
                        (get-ev-models expgen)
                        expgen
                        fset
                        changing-functions
                        duration
                    )
                )
            )
        )
    )
    (when (or (< time-change 0) (< duration (- time-change .000001)))
        (setq time-change duration)
    )
    (when (not (= time-change duration))
        (break "Doing something special.")
    )
    (dolist (func changing-functions)
        (setq new-func-value
            (eval
                (subst time-change '?__dur (var-symbol-update-function (fact-value func)))
            )
        )
        (push (contradictory-fact func new-func-value)
            new-fact-values
        )
    )
    (values
        (append non-changing-facts new-fact-values)
        time-change
        orig-fact-values
        new-fact-values
        (make-occurrence 
            :signature (list 'time-passes time-change)
            :post new-fact-values
            :point pt
            :is-event t
        )
    )
)

(defun create-continuous-time-assume-fn (orig-fact-values)
    #'(lambda (model cnd facts binding-list)
        (assume-hidden-and-closed-world model cnd facts binding-list
            :hidden-test
                #'(lambda (cnd)
                    (some 
                        #'(lambda (fact)
                            (cnd-maps-to-fact cnd fact binding-list)
                          )
                        orig-fact-values
                    )
                  )
        )
      )
)

(defun cnd-maps-to-fact (cnd fact binding-list)
    (and (eq (cnd-type cnd) (fact-type fact))
         (equal (sublis binding-list (cnd-args cnd)) (fact-args fact))
    )
)

(defun assume-default-observables (model cnd facts binding-list)
    (if (is-hidden model cnd)
        ;then
        nil
        ;else
        (assume-closed-world model cnd facts binding-list
            :default-fn #'get-default-value-cnd 
        )
    )
)

(defun default-closed-world-assume-fn (model cnd facts binding-list)
    (assume-closed-world model cnd facts binding-list
        :default-fn #'get-default-value-cnd 
    )
)


(defun default-and-self-assume-fn (model cnd facts binding-list)
    (assume-hidden-and-closed-world model cnd facts binding-list
        :default-fn #'get-default-value-cnd
        :hidden-test
            #'(lambda (model cnd-inner)
                (eq (cnd-type cnd-inner) 'self)
              )
    )
)

(defun assumable-and-self-assume-fn (model cnd facts binding-list)
    (assume-hidden-and-closed-world model cnd facts binding-list
        :default-fn #'get-default-value-cnd
        :hidden-test
            #'(lambda (model cnd-inner)
                (or (is-legal-assumption model cnd-inner) (eq (cnd-type cnd-inner) 'self))
              )
    )
)

(defun hidden-and-self-assume-fn (model cnd facts binding-list)
    (assume-hidden-and-closed-world model cnd facts binding-list
        :default-fn #'get-default-value-cnd
        :hidden-test
            #'(lambda (model cnd-inner)
                (or (is-hidden model cnd-inner) (eq (cnd-type cnd-inner) 'self))
              )
    )
)

(defun default-closed-world-variable-inequality-assume-fn (model cnd facts binding-list)
    (assume-variable-inequality-and-closed-world model cnd facts binding-list
        :default-fn #'get-default-value-cnd 
    )
)

(defun assume-variable-inequality-and-closed-world (model cnd facts binding-list &key (default-fn #'get-default-value-cnd) &aux args exp-value)
    (when (not (cnd-inequality cnd))
        (return-from assume-variable-inequality-and-closed-world
            (assume-closed-world model cnd facts binding-list
                :default-fn default-fn 
            )
        )
    )
    (when (subsetp 
              (remove-if-not #'is-variable-term (cnd-args cnd))
              (mapcar #'car binding-list)
          )
        (setq args (sublis binding-list (cnd-args cnd)))
        (setq exp-value (sublis binding-list (cnd-value cnd)))
        (let ((known-fact (find-same-map-in-facts (cnd-type cnd) args facts)))
            (when (and (cnd-inequality cnd) (or (and known-fact (var-symbol-p (fact-value known-fact))) (var-symbol-p exp-value)))
                (multiple-value-bind (blists policy) 
                        (default-unify-cnd model cnd facts binding-list #'assume-nothing t nil)
                    (when policy (error "Did not expect second return value of ~a." policy))
                    (when blists
                        (return-from assume-variable-inequality-and-closed-world (list binding-list))
                    )
                )
            )
            (return-from assume-variable-inequality-and-closed-world nil)
        )
    )
    (return-from assume-variable-inequality-and-closed-world
        (assume-closed-world model cnd facts binding-list
            :default-fn default-fn 
        )
    )
)

;;This adds the assumption that hidden facts are true to the closed world 
;;assumption.
(defun assume-hidden-and-closed-world (model cnd facts binding-list &key (hidden-test #'is-hidden) (default-fn #'get-default-value-cnd) &aux args fact)
    (when (not (funcall hidden-test model cnd))
        (return-from assume-hidden-and-closed-world (assume-closed-world model cnd facts binding-list :default-fn default-fn))
    )
    (when (subsetp 
              (remove-if-not #'is-variable (cons (cnd-value cnd) (cnd-args cnd)))
              (mapcar #'car binding-list)
          )
        (setq args (sublis binding-list (cnd-args cnd)))
        (setq fact (make-fact :type (cnd-type cnd) :args args :value (sublis binding-list (cnd-value cnd))))
        (when (and (listp facts)
                   (find fact facts :test #'same-map) 
              )
            (return-from assume-hidden-and-closed-world 
                (unify model cnd facts binding-list :assume-fn #'assume-nothing)
            )
        )
        (when (and (fact-set-p facts)
                   (find-in-fact-set fact facts :test #'same-map) 
              )
            (return-from assume-hidden-and-closed-world 
                (unify model cnd facts binding-list :assume-fn #'assume-nothing)
            )
        )
        (when (and (or (cnd-inequality cnd) (var-symbol-p (fact-value fact))) (var-symbols-in args))
            (return-from assume-hidden-and-closed-world 
                (cons binding-list
                    (unify model cnd facts binding-list :assume-fn #'assume-nothing)
                )
            )
        )
        (return-from assume-hidden-and-closed-world (list binding-list))
    )

    'defer
)

;;This adds the assumption that hidden facts are true to the closed world 
;;assumption.
(defun assume-hidden-as-variable (expgen cnd facts binding-list &key (hidden-test #'is-hidden) (default-fn #'get-default-value-cnd) &aux args fact free-vars ret)
    (when (not (funcall hidden-test expgen cnd))
        (setq ret (assume-closed-world expgen cnd facts binding-list :default-fn default-fn))
        (when (and (listp ret) (> (length ret) 3))
            (setq ret (generalize-bindings expgen cnd ret))
        )
        (return-from assume-hidden-as-variable ret)
    )
    (setq free-vars 
        (set-difference
            (remove-if-not #'is-variable (cons (cnd-value cnd) (cnd-args cnd)))
            (mapcar #'car binding-list)
        )
    )
    (when (null free-vars) 
        (setq args (sublis binding-list (cnd-args cnd)))
        (setq fact (make-fact :type (cnd-type cnd) :args args :value (sublis binding-list (cnd-value cnd))))
        (when (and (listp facts)
                   (find fact facts :test #'contradicts) 
                   (not (find fact facts :test #'equal-fact))
              )
            (return-from assume-hidden-as-variable 'fail)
        )
        (when (and (fact-set-p facts)
                   (find-in-fact-set fact facts :test #'contradicts) 
                   (not (find-in-fact-set fact facts :test #'equal-fact))
              )
            (return-from assume-hidden-as-variable 'fail)
        )
        (return-from assume-hidden-as-variable (list binding-list))
    )
    (dolist (var free-vars)
        (push (cons var (new-var-sym (get-cnd-var-type expgen cnd var))) binding-list)
    )
    (list binding-list)
)

(defun generalize-bindings (expgen cnd blists &aux ret-blist cur-blist blists-ptr cur-val)
    (dolist (var (mapcar #'car (first blists)))
        (setq cur-val (cdr (assoc var (car blists))))
        (setq blists-ptr (cdr blists))
        (loop while (and blists-ptr (not (var-symbol-p cur-val))) do
            (setq cur-blist (pop blists-ptr))
            (when (not (equalp cur-val (cdr (assoc var cur-blist))))
                (setq cur-val (new-var-sym (get-cnd-var-type expgen cnd var)))
            )
        )
        (setq ret-blist (append ret-blist (list (cons var cur-val))))
    )
    (return-from generalize-bindings (list ret-blist))
)

(defun assume-non-static-and-self (model cnd facts binding-list)
    (when (and (is-static model cnd) (not (eq (cnd-type cnd) 'self)))
        (return-from assume-non-static-and-self 
            (assume-closed-world model cnd facts binding-list
                :default-fn #'get-default-value-cnd
            )
        )
    )
    (assume-anything model cnd facts binding-list)
)


(defun assume-non-static (model cnd facts binding-list)
    (when (is-static model cnd)
        (return-from assume-non-static
            (assume-closed-world model cnd facts binding-list
                :default-fn #'get-default-value-cnd
            )
        )
    )
    (assume-anything model cnd facts binding-list)
)


(defun model-affected-by-changing-functions (ev-model changing-functions)
    (dolist (fact changing-functions)
        (when (find (fact-type fact) (model-conditions ev-model) :key #'cnd-type)
            (return-from model-affected-by-changing-functions t)
        )
    )
    nil
)

(defun find-minimum-time (ev-model expgen facts changing-functions max-duration &aux results)
    ;; Bypass irrelevant models here?
    (when (not (model-affected-by-changing-functions ev-model changing-functions))
        (return-from find-minimum-time max-duration)
    )
    (setq results 
        (unify-set expgen (event-model-conditions ev-model) facts nil
            ;:assume-fn #'assume-hidden-and-closed-world
            :assume-fn #'default-closed-world-assume-fn
            :return-deferred t
            :binds-var-symbols nil
        )
    )
    (setq results 
        (remove-if-not 
            #'(lambda (blist)
                (some #'is-update-function-variable (get-var-symbols blist))
              )
            results :key #'first
        )
    )
    ;(when results (break "Testing find-minimum-time"))
    (dolist (result results)
        (let ((blist (first result))
              (changing-conditions (second result))
              (new-duration 0)
             )
            (setq new-duration
                (find-minimum-duration 
                    blist changing-conditions  
                    changing-functions max-duration
                )
            )
            (when (< .000001 new-duration max-duration)
                (setq max-duration new-duration)
            )
        )
    )
    max-duration
)

(defun find-minimum-duration (blist changing-conditions changing-functions max-duration &aux min-root)
    (setq min-root max-duration)
    (dolist (fact changing-functions)
        (setq blist 
            (subst
                (var-symbol-update-function (fact-value fact))
                (fact-value fact)
                blist
            )
        )
    )
    (when *use-maxima*
        (shop2::ensure-maxima-loaded)
    )
    (dolist (cnd changing-conditions)
        (when (eq (cnd-type cnd) 'assign)
            (let ((value (sublis blist (second (cnd-value cnd)))))
                (if (vars-in value)
                    ;then
                    (push (cons (first (cnd-value cnd)) (second (cnd-value cnd))) blist)
                    ;else
                    (push (cons (first (cnd-value cnd)) (eval (second (cnd-value cnd)))) blist)
                )
            )
        )
    )
    (dolist (cnd changing-conditions)
        (let (eqn roots root)
            (cond 
                ((eq (cnd-type cnd) 'eval)
                    (setq eqn (sublis blist (second (cnd-value cnd)))))
                ((cnd-inequality cnd)
                    (break "What now?"))
                (t (error "Not sure how to handle changing condition ~A." cnd))
            )
            (when (var-symbols-in eqn)
                (return-from find-minimum-duration max-duration)
            )
            (when (not (equal (remove-duplicates (vars-in eqn)) '(?__dur)))
                (error "Only duration variable should be left now.")
            )
            (when *use-maxima*
                (setq roots 
                    (shop2::find-roots-lisp-expr 
                        (sublis '((> . =) (>= . =) (< . =) (<= . =)) eqn)
                        '?__dur
                    )
                )
            )
            (when (or (not *use-maxima*) (eq roots 'shop2::fail))
                (loop do
                    (setq root
                        (find-zero (shop2::make-function (cons '- (cdr eqn)))
                            0 min-root
                        )
                    )
                    until (or (null root) (check-root-validity root eqn))
                )
                (when (numberp root)
                    (setq min-root root)
                )
            )
            (when (and roots (listp roots))
                (setq roots (remove nil (mapcar+ #'check-root-validity roots eqn)))
                (setq min-root
                    (apply #'min (cons min-root (remove .000001 roots :test #'>=)))
                )
            )
        )
    )
    min-root
)

(defun check-root-validity (root eqn)
    (let (ctr-check)
        (setq ctr-check 0)
        (loop while (and (not (eval (subst root '?__dur eqn))) 
                         (<= ctr-check 100)) do
            (incf root .000001)
            (incf ctr-check)
        )
        (if (<= ctr-check 100)
            ;then
            root
            ;else 
            nil
        )
    )
)    

(defun is-update-function-variable (sym)
    (and (var-symbol-p sym) (var-symbol-update-function sym))
)
    
    

(defun get-changing-functions (expgen facts &aux blists ret-list)
    (dolist (proc (get-process-models expgen))
        (setq blists 
            (unify-set expgen
                (process-model-conditions proc) facts nil 
                :assume-fn #'default-closed-world-assume-fn
            )
        )
        (dolist (blist blists)
            (dolist (func (process-model-effects proc))
                (let ((new-fact
                        (make-fact 
                            :type (cnd-type func)
                            :args (sublis blist (cnd-args func))
                        )
                      )
                      (new-cnd (make-cnd :type (cnd-type func) :args (sublis blist (cnd-args func)) :value (make-old-value-variable func)))
                     )
                     (dolist (new-blist (unify expgen new-cnd facts blist :assume-fn #'default-closed-world-assume-fn))
                        (when (every #'var-symbol-update-function (get-var-symbols new-blist))
                            (setq new-fact
                                (contradictory-fact new-fact 
                                    (make-current-value-symbol new-fact)
                                )
                            )
                            (setf (var-symbol-update-function (fact-value new-fact))
                                (sublis new-blist (var-symbol-update-function (cnd-value func)))
                            )
                            (push 
                                new-fact
                                ret-list
                            )
                        )
                    )
                )
            )
        )
    )
    ret-list
)

(defun make-current-value-symbol (fact)
    (let ((var (make-var-symbol :return-type 'real)))
        (setf (var-symbol-id var)
            (apply #'combine-strings-symbols 
                (append (list "__VAL-" (fact-type fact))
                    (apply #'append 
                        (mapcar #'(lambda (arg) (list "-" arg)) (fact-args fact))
                    )
                )
            )
        )
        (setf (var-symbol-name var) (var-symbol-id var))
        var
    )
)

(defun find-allowed-ev-models (expgen ex)
    (remove-if 
        #'(lambda (em) 
            (member (model-type em) (explanation-abandoned-models ex))
          )
        (get-ev-models expgen)
    )
)  

