 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: April 20, 2011
 ;;;; Project Name: Explanation
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file creates planning graphs indicating what events are possible.
 
(in-package :artue)

(defvar *enumeration-function-names*
    '(  DECIDE-FACT-MUTEX ALL-MAY-NOT-OCCUR
        ALL-MAY-NOT-OCCUR-UNCACHED CONTRADICTS FIND-FACT-HASH MAKE-FACT SAME-MAP
        FACT-COULD-PERSIST-THROUGH CAUSE-IS-COMPATIBLE KNOWN-INCOMPATIBLE
        NO-CONSISTENT-CAUSE-EXISTS FACTS-ARE-MUTEX HASH-FACT-CAUSES
        FIND-POSSIBLE-EVENTS CONSIDER-POSSIBLE-EVENTS ENUMERATE-OCCS
        ADD-OCC-TO-LAYER OCC-POSSIBLE FIND-OCCURRENCE-MUTEXES
        ADD-FACT-TO-HASH MAKE-FULL-MUTEX-HASH SHARED-FACTS
     )
)
    

(defstruct layer
    (occurrence-back-links nil)
    (occurrence-mutexes nil)
    (fact-forward-links nil)
    (fact-back-links nil)
    (new-mutex-hash (new-fact-hash-table)) 
                               ;; this table holds mutexes that become true 
                               ;; starting with this layer
    (full-mutex-hash nil)      ;; this table holds mutexes that are true after 
                               ;; this layer, including mutexes that became true
                               ;; at prior layers
    (immediate-mutexes nil)    ;; the fact pairs cannot be true *and* new.
    (continuation-mutexes nil) ;; the fact pairs cannot be true *and* more 
                               ;; events occur
    (pt nil)
    (possible-facts nil)       ;; facts possible after occs in this layer
)

;; Point created when find-possible-events is called that identifies the earliest 
;; layer, where assumptions can be added for tracking.
(defvar *assumption-point* nil)
;; Occurrence created when find-possible-events is called that identifies a
;; theoretical occurrence that causes all assumptions made, useful for finding 
;; assumptions in certain data structures.
(defvar *assumption-occ* nil)
;; The following control two specific break points which are useful when 
;; debugging to examine state at a particular time. A value of t causes 
;; a break every cycle; a numeric value causes a break only at the specified
;; time point.
(defvar *break-before-enumeration* nil)
(defvar *break-before-find-mutex* nil)
;; Controls tracing of history consistency checking.
(defvar *trace-consistent-history-exists* nil)
;; Controls whether the time of mutex checking is recorded.
(defvar *timing* nil)
;; Controls a test that determines whether time passage is working.
(defvar *check-time-congruency* nil)

;; Temporary caches that speed up mutex checking; cleared every cycle in 
;; find-possible-events.
(defvar *incompatibility-true-cache* nil)
(defvar *incompatibility-false-cache* nil)

;;Temporary caches that speed up checks on necessary events.
(defvar *known-necessary-events* nil)
(defvar *known-unnecessary-events* nil)


(defvar *occs* nil) ;Only for debugging.
(defvar *layers* nil) ;Only for debugging.
(defvar *cur-layer* nil) ;Only for debugging.
(defvar *old-facts* nil) ;Only for debugging.
(defvar *old-fact-occs* nil) ;Only for debugging.
(defvar *known-facts* nil) ;Only for debugging.


#-SBCL
(defun hash-fact-causes (fact-causes-list)
    (+ (fact-hash-val (car fact-causes-list))
       (apply #'+ 
           (mapcar #'occurrence-hash (cdr fact-causes-list))
       )
    )
)

#+SBCL
(defun hash-fact-causes (fact-causes-list)
    (mod
        (+ (fact-hash-val (car fact-causes-list))
           (apply #'+ 
               (mapcar #'occurrence-hash (cdr fact-causes-list))
           )
        )
        most-positive-fixnum
    )
)

(defun equal-fact-causes (list1 list2)
    (and (eql (hash-fact-causes list1) (hash-fact-causes list2))
         (equal-fact (car list1) (car list2))
         (all-occs-shared (cdr list1) (cdr list2))
    )
)

#+SBCL(sb-ext::define-hash-table-test equal-fact-causes hash-fact-causes)
#-(or CLISP ABCL)(defvar *incompatibility-result* (make-hash-table :test 'equal-fact-causes :hash-function 'hash-fact-causes))
#+(or CLISP ABCL)(defvar *incompatibility-result* (make-hash-table :test 'equalp))


(defmacro time-if (do-time form)
    `(if ,do-time
        ;then
        (time ,form)
        ;else
        ,form
    )
)
 
(defun find-possible-events (expgen facts new-facts pt 
                             mutexes duration
                             &key (hypothesize-actions nil) (ground-occurrences t)
                             &aux cur-layer layers
                                  fact-occs cur-occs)
    (setq fact-occs 
        (mapcar #'(lambda (fact) (cons fact :original)) facts)
    )
    (setq cur-occs (eq duration 0))
    
    (setq layers 
        (initialize-layers expgen facts mutexes 
            (point-prior expgen pt)
        )
    )

    (when hypothesize-actions
        (multiple-value-setq (cur-layer cur-occs facts fact-occs new-facts)
            (make-action-layer expgen facts fact-occs layers pt
                :assume-fn
                    (if (get-hidden-facts expgen) 
                        ;then
                        #'default-closed-world-assume-fn
                        ;else
                        #'assume-hidden-as-variable
                    )
            )
        )
        (push (cons pt cur-layer) layers)
    )
    
    (multiple-value-setq (cur-occs facts new-facts fact-occs layers pt)
        (add-event-layers expgen cur-occs facts new-facts fact-occs layers duration pt
            :assume-fn
                (if (get-hidden-facts expgen) 
                    ;then
                    #'default-closed-world-assume-fn
                    ;else
                    #'assumable-and-self-assume-fn
                    ; #'assume-hidden-as-variable
                )
            :ground-occurrences ground-occurrences
        )
    )
  
    (values
        (mapcar #'car (apply #'append (mapcar #'layer-occurrence-back-links (mapcar #'cdr layers))))
        (combine-prior-fact-sets layers)
        (combine-prior-mutex-sets layers)
        layers
    )
    ; (values
        ; (mapcar #'car (apply #'append (mapcar #'layer-occurrence-back-links (mapcar #'cdr layers))))
        ; (get-possible-hidden-facts expgen facts fact-occs obs-facts layers)
        ; (get-hidden-mutexes expgen (layer-full-mutex-hash cur-layer))
        ; layers
    ; )
)

(defun make-diagnostic-layers (expgen pt duration
                               &key (ground-occurrences t)
                               &aux facts new-facts cur-layer layers
                                    fact-occs cur-occs)
    (setq cur-occs (eq duration 0))
    (setq new-facts nil)
    
    (multiple-value-setq (layers facts fact-occs) 
        (initialize-possible-worlds-layers expgen pt)
    )

    (incf pt)
    
    (multiple-value-setq (cur-layer cur-occs facts fact-occs new-facts)
         (make-action-layer expgen facts fact-occs layers pt 
             :annotation-fn #'annotate-with-possible-worlds
         )
    )
    (push (cons pt cur-layer) layers)
    
    (multiple-value-setq (cur-occs facts new-facts fact-occs layers pt)
        (add-event-layers 
            expgen cur-occs facts new-facts fact-occs layers 
            duration pt
            :ground-occurrences ground-occurrences
        )
    )
  
    
    (values
        (mapcar #'car (apply #'append (mapcar #'layer-occurrence-back-links (mapcar #'cdr layers))))
        facts
        (combine-prior-mutex-sets layers)
        layers
    )
    ; (values
        ; (mapcar #'car (apply #'append (mapcar #'layer-occurrence-back-links (mapcar #'cdr layers))))
        ; (get-possible-hidden-facts expgen facts fact-occs obs-facts layers)
        ; (get-hidden-mutexes expgen (layer-full-mutex-hash cur-layer))
        ; layers
    ; )
)



(defun add-event-layers (expgen cur-occs facts new-facts fact-occs layers duration pt
                         &key (assume-fn #'default-closed-world-assume-fn)
                              (ground-occurrences t)
                         &aux abandoned-facts cur-time cur-layer)
    (setq cur-time 0)
    (loop while (or cur-occs (< .00001 (- duration cur-time)))  do
        (when (null cur-occs)
            (setq pt (force-next-point expgen pt :process))
            (multiple-value-setq (cur-layer cur-time facts fact-occs new-facts)
                (make-process-layer expgen facts fact-occs layers duration cur-time pt)
            )
            (push (cons pt cur-layer) layers)
        )
        (setq pt (force-next-point expgen pt :event))
        
        (multiple-value-setq (cur-layer cur-occs facts fact-occs new-facts)
            (make-event-layer expgen facts fact-occs new-facts layers pt 
                :ground-occurrences ground-occurrences :assume-fn assume-fn
            )
        )
        (push (cons pt cur-layer) layers)
        
        (setq abandoned-facts 
            ;(append abandoned-facts
                (remove-if-not
                    #'(lambda (fact) (member *true-fact* (gethash fact (layer-full-mutex-hash cur-layer))))
                    facts
                )
            ;)
        )
          
        (setq facts (set-difference facts abandoned-facts :test #'equal-fact))
        
        (when (cycle-in-layers cur-occs layers)
            (setq cur-occs nil)
        )
    )
    ; (setq facts (append facts abandoned-facts))
    (values cur-occs facts new-facts fact-occs layers pt)
)

(defun initialize-layers (expgen facts mutexes pt &aux cur-layer layers)
    (setq *assumption-point* pt)
    (setq *assumption-occ* (make-occurrence :point *assumption-point*))
    (setq cur-layer 
        (make-layer 
            :pt *assumption-point*
            :fact-back-links 
                (mapcar #'list 
                    (remove-if-not 
                        #'(lambda (fact) (is-transient expgen fact))  
                        facts
                    )
                )
        )
    )
    (setf (layer-new-mutex-hash cur-layer) mutexes)
    (setq layers (list (cons *assumption-point* cur-layer)))
    (setf (layer-full-mutex-hash cur-layer) mutexes)
    layers
)

(defun remove-inconsistent-facts (expgen poss-facts obs-facts mutexes &aux new-known-trues all-known-trues)
    (dolist (fact poss-facts)
        (when (and (is-observable expgen fact) 
                   (eq (fact-value fact) t) 
                   (not (member fact obs-facts :test #'same-map))
              )
            (push (contradictory-fact fact) obs-facts)
        )
    )
    (setq poss-facts (remove-if #'(lambda (fact) (is-observable expgen fact)) poss-facts))
    (setq new-known-trues (cons *true-fact* obs-facts))
    (setq all-known-trues new-known-trues)
    (loop while new-known-trues do
        (setq poss-facts 
            (set-difference poss-facts new-known-trues 
                :test #'(lambda (f1 f2) 
                            (test-mutex f1 f2 mutexes)
                        )
            )
        )
    
        (setq new-known-trues
            (set-difference
                (set-difference 
                    poss-facts 
                    poss-facts 
                    :test #'could-contradict
                )
                all-known-trues
                :test #'equal-fact
            )
        )
        (push-all new-known-trues all-known-trues)
    )
    
    poss-facts
)

(defun remove-observable-mutexes (expgen table)
    (dolist (key (get-hash-keys table))
        (cond
            ((is-observable expgen key)
                (remhash key table)
            )
            (t
                (setf (gethash key table)
                    (remove-if (embed-expgen #'is-observable expgen)
                        (remove key (gethash key table) :test #'contradicts)
                    )
                )
            )
        )
    )
    table
)

(defun combine-prior-fact-sets (layers &aux master-hash master-facts)
    (setq master-facts (layer-possible-facts (cdar layers)))
    (dolist (layer (mapcar #'cdr (cdr layers)))
        (when (layer-continuation-mutexes layer)
            (setq master-facts (union master-facts (layer-possible-facts layer) :test #'equal-fact))
;            (break "Continuation mutexes causing union.")
        )
    )
    master-facts
)

(defun combine-prior-mutex-sets (layers &aux master-hash)
    (setq master-hash (layer-new-mutex-hash (cdar layers)))
    (dolist (layer (mapcar #'cdr (cdr layers)))
        (when (layer-continuation-mutexes layer)
            (setq master-hash (intersect-mutex-hashes master-hash (layer-new-mutex-hash layer)))
        )
    )
    (ensure-bidirectional master-hash)
    master-hash
)

(defun intersect-mutex-hashes (target-mutex-hash alt-mutex-hash)
    (maphash 
        #'(lambda (key value) 
            (setf (gethash key target-mutex-hash) 
                (intersection 
                    value
                    (gethash key alt-mutex-hash)
                    :test #'equal-fact
                )
            )
          )
        target-mutex-hash
    )
    target-mutex-hash
)
        

(defun remove-continuation-mutexes (table continuation-mutexes)
    (dolist (pair continuation-mutexes)
        (setf (gethash (car pair) table) (remove (cdr pair) (gethash (car pair) table))) 
        (setf (gethash (cdr pair) table) (remove (car pair) (gethash (cdr pair) table))) 
    )
    table
)

(defun force-next-point (expgen pt point-type)
    (if (null (point-next expgen pt))
        ;then
        (next-point expgen point-type)
        ;else
        (point-next expgen pt)
    )
)


(defun make-action-layer (expgen facts fact-occs layers pt
                          &key (assume-fn #'default-closed-world-assume-fn) 
                               (annotation-fn (arg-fn 2))
                          &aux cur-layer cur-occs known-facts)
    (setq cur-layer (make-layer :pt pt))
    (push (cons pt cur-layer) layers)
    
    (setq cur-occs 
        (enumerate-layer-actions 
            expgen (new-fact-set facts) 
            fact-occs cur-layer pt layers
            :annotation-fn annotation-fn
            :assume-fn assume-fn
        )
    )
    
    (multiple-value-bind (facts fact-occs new-facts old-facts old-fact-occs mutex-facts-new mutex-facts-old known-facts)
            (advance-layer-facts expgen cur-layer facts fact-occs layers)

        (build-action-mutexes expgen cur-layer layers old-facts old-fact-occs mutex-facts-new mutex-facts-old)
        (setf (layer-possible-facts cur-layer) facts)
        (values cur-layer cur-occs facts fact-occs new-facts) 
    )
)

;; This function is responsible for populating an event graph layer with all  
;; possible events, as well as the mutexes that are known afterward. Must also 
;; return resulting possible state space for ease of constructing next 
;; layer.
(defun make-event-layer (expgen facts fact-occs new-facts layers pt 
                         &key (ground-occurrences t) (assume-fn #'default-closed-world-assume-fn)
                         &aux cur-occs cur-layer)
    (setq cur-layer (make-layer :pt pt))
    (push (cons pt cur-layer) layers)
    
    (when (and *break-before-enumeration* 
            (or (not (numberp *break-before-enumeration*)) 
                (>= pt *break-before-enumeration*))) 
        (break "Finding events in enumeration at point ~A." pt)
    )

    (setq cur-occs 
        (enumerate-layer-events 
            expgen (new-fact-set facts) new-facts 
            fact-occs cur-layer pt layers 
            :ground-all-vars ground-occurrences
            :assume-fn assume-fn
        )
    )

    (when (and *break-before-enumeration* *trace-occurrence-consideration*) 
        (break "After enumeration.")
    )

    (find-occurrence-mutexes expgen cur-occs cur-layer layers fact-occs)
    (clrhash *incompatibility-result*)
    (setq *incompatibility-true-cache* nil)
    (setq *incompatibility-false-cache* nil)
    (setq *known-necessary-events* nil)
    (setq *known-unnecessary-events* nil)


    (multiple-value-bind (facts fact-occs new-facts old-facts old-fact-occs mutex-facts-new mutex-facts-old known-facts)
            (advance-layer-facts expgen cur-layer facts fact-occs layers)
        (when (and *break-before-find-mutex* 
                (or (not (numberp *break-before-find-mutex*)) 
                    (>= pt *break-before-find-mutex*))) 
            (setq *known-facts* known-facts)
            (setq *old-fact-occs* old-fact-occs)
            (setq *old-facts* old-facts)
            (setq *cur-layer* cur-layer)
            (setq *layers* layers)
            (setq *occs* (mapcar #'first (layer-occurrence-back-links cur-layer)))
            (break "Finding mutexes at point ~A." pt)
        )

        (build-layer-mutexes expgen cur-layer layers old-facts old-fact-occs mutex-facts-new mutex-facts-old known-facts)
        (setf (layer-possible-facts cur-layer) facts)
        (values cur-layer cur-occs facts fact-occs new-facts) 
    )
)

(defun build-layer-mutexes (expgen cur-layer layers old-facts old-fact-occs mutex-facts-new mutex-facts-old known-facts)
    (clear-mutexes cur-layer)
    (when *timing* (format t "~%Timing true mutex check.~%"))
    (time-if *timing*
        (dolist (fact1 (copy-list mutex-facts-old))
            (when (decide-fact-mutex 
                        expgen cur-layer fact1 *true-fact* old-fact-occs 
                        old-facts layers known-facts
                  )
                (setq mutex-facts-new (remove fact1 mutex-facts-new :test #'equal-fact))
                (setq mutex-facts-old (remove fact1 mutex-facts-old :test #'equal-fact))
            )
        )
    )
    (when *timing* (format t "~%Timing new mutex check.~%"))
    (time-if *timing*
        (dolist (fact1 mutex-facts-new)
            (dolist (fact2 (union mutex-facts-old (gethash fact1 (layer-full-mutex-hash (cdadr layers))) :test #'equal-fact))
                (decide-fact-mutex 
                    expgen cur-layer fact1 fact2 old-fact-occs old-facts 
                    layers known-facts
                )
            )
        )
    )
    (when *timing* (break "After second mutex decisions"))
    (setf (layer-full-mutex-hash cur-layer) 
        (make-full-mutex-hash layers mutex-facts-new
            :continuation (not (null mutex-facts-new))
        )
    )
)

(defun build-action-mutexes (expgen cur-layer layers old-facts old-fact-occs mutex-facts-new mutex-facts-old)
    (clear-mutexes cur-layer)
    (when *timing* (format t "~%Timing true mutex check.~%"))
    (time-if *timing*
        (dolist (fact1 (copy-list mutex-facts-old))
            (when (decide-action-mutex 
                        expgen
                        cur-layer fact1 *true-fact* old-fact-occs 
                        old-facts layers
                  )
                (break "Not sure this can happen.")
                (setq mutex-facts-new (remove fact1 mutex-facts-new :test #'equal-fact))
                (setq mutex-facts-old (remove fact1 mutex-facts-old :test #'equal-fact))
            )
        )
    )
    (when *timing* (format t "~%Timing new mutex check.~%"))
    (time-if *timing*
        (dolist (fact1 mutex-facts-new)
            (dolist (fact2 (union mutex-facts-old (gethash fact1 (layer-full-mutex-hash (cdadr layers))) :test #'equal-fact))
                (decide-action-mutex 
                    expgen
                    cur-layer fact1 fact2 old-fact-occs old-facts 
                    layers
                )
            )
        )
    )
    (when *timing* (break "After second mutex decisions"))
    (setf (layer-full-mutex-hash cur-layer) 
        (make-full-mutex-hash layers mutex-facts-new
            :continuation (not (null mutex-facts-new))
        )
    )
)


;; This function is responsible for both advancing the beliefs about the current 
;; state through the next layer, and creating the foundations necessary for 
;; calculating mutexes.
;; 
;; Inputs:
;; cur-layer: The most recent graph layer.
;; facts: Facts that may have been true after the prior graph layer.
;; fact-occs: Alist that maps facts to the occurrences that caused them.
;;
;; Outputs used for calculating next occurrence graph layer:
;; facts: These are the beliefs being passed on for construction of the next 
;;        layer of the occurrence graph
;; fact-occs: This alist maps a fact to the occurrence that caused it.
;; new-facts: All facts which may become true due to occurrences in this graph
;;            layer.
;;
;; Outputs used for mutex calculations:
;; old-facts: Describes facts which were or may have been true before the most 
;;            recent layer of occurrences. Note that occurrences in the most 
;;            recent layer may affect this, as they make assumptions that should
;;            be added. However, effects of occurrences in this layer are not 
;;            reflected in old-facts.
;; old-fact-occs: Maps old-facts to the occurrences that caused them.
;; mutex-facts-new: all facts which may have been set or contradicted by 
;;                  occurrences in the current layer.
;; mutex-facts-old: All facts in mutex-facts-new, as well as facts that may have 
;;                  affected the causation of occurrences in the current layer.
;; known-facts: All facts which were unambiguously true before the occurrences 
;;              in the current layer.
(defun advance-layer-facts (expgen cur-layer facts fact-occs layers
                            &aux (new-facts nil) (pre-facts nil) 
                                 (new-pre-facts nil) (new-contras nil) 
                                 (old-facts nil) (old-fact-occs nil)
                                 (supp-fact-occs nil) (mutex-facts-new nil))
    (dolist (occ (mapcar #'first (layer-occurrence-back-links cur-layer)))
        (dolist (fact (occurrence-pre occ))
            (pushnew fact pre-facts :test #'equal-fact)
            (when (not (member fact facts :test #'equal-fact))
                (push (cons fact *assumption-occ*) fact-occs)
                (pushnew fact facts :test #'equal-fact)
                (let ((alt-fact (default-alternative-fact expgen fact)))
                    (when alt-fact
                        (add-assumption alt-fact layers)
                        (push (cons alt-fact *assumption-occ*) fact-occs)
                        (pushnew alt-fact facts :test #'equal-fact)
                    )
                )
            )
        )

        (dolist (fact (occurrence-post occ))
            (when (not (member fact facts :test #'same-map))
                (let ((alt-fact (default-alternative-fact expgen fact)))
                    (when alt-fact
                        (add-assumption alt-fact layers)
                        (push (cons alt-fact *assumption-occ*) fact-occs)
                        (pushnew alt-fact facts :test #'equal-fact)
                    )
                )
            )
            (pushnew fact new-facts :test #'equal-fact)
            (setq supp-fact-occs (cons (cons fact occ) supp-fact-occs))
        )
    )
    
    (setq old-facts facts)
    (setq old-fact-occs fact-occs)
    
    (setq facts (union new-facts facts :test #'equal-fact))
    (setq fact-occs (append supp-fact-occs fact-occs))
    
    (setq pre-facts 
        (remove-if-not 
            #'(lambda (fact) (is-transient expgen fact))
            pre-facts
        )
    )
    
    (dolist (fact pre-facts)
        (setq new-pre-facts
            (union new-pre-facts
                (gethash fact (layer-full-mutex-hash (cdadr layers)))
                :test #'equal-fact
            )
        )
    )
    (dolist (fact facts)
        (when (find fact new-facts :test #'contradicts)
            (pushnew fact new-contras :test #'equal-fact)
        )
        (when (find fact pre-facts :test #'contradicts)
            (pushnew fact pre-facts :test #'equal-fact)
        )
    )
    (setq pre-facts (union pre-facts new-pre-facts :test #'equal-fact))
    
    (setq mutex-facts-new (union new-facts new-contras :test #'equal-fact))
    (values
        facts
        fact-occs
        new-facts
        old-facts
        old-fact-occs
        mutex-facts-new
        (union mutex-facts-new pre-facts :test #'equal-fact) ;;mutex-facts-old
        (remove-if #'variables-in-fact 
            (set-difference old-facts old-facts :test #'contradicts) ;;known-facts
        )
    )
)
    

(defun enumerate-layer-events (expgen fset new-facts fact-occs cur-layer pt layers 
                               &key (ground-all-vars t) (assume-fn #'default-closed-world-assume-fn)
                               &aux (cur-occs nil) (poss-occs nil))
    (dolist (ev-model (get-ev-models expgen))
        (setq poss-occs
            (enumerate-occs expgen ev-model fset pt 
                 :assume-fn assume-fn
                 :bind-var-symbols t
                 :ground-all-vars ground-all-vars
            )
        )
        (dolist (occ poss-occs)
            (when (occ-possible expgen occ new-facts fact-occs layers)
                (when *trace-occurrence-consideration*
                    (format t "~%Possible occurrence: ~A." occ)
                    (qp (occ-possible expgen occ new-facts fact-occs layers))
                )
                (push occ cur-occs)
                (add-occ-to-layer occ cur-layer layers fact-occs)
            )
        )
    )
    cur-occs
)

(defun enumerate-layer-actions (expgen fset fact-occs cur-layer pt layers 
                                &key (annotation-fn (arg-fn 2))
                                     (assume-fn #'default-closed-world-assume-fn)
                                &aux (cur-occs nil) self-fact poss-occs)
    (setq fset (remove-from-fact-set (get-self-fact expgen) (copy-fact-set fset)))
    (dolist (act-model (get-action-models expgen))
        (setq poss-occs
            (enumerate-occs expgen act-model fset pt 
                 :assume-fn assume-fn
                 :bind-var-symbols t
            )
        )
        (setq poss-occs (funcall annotation-fn expgen poss-occs pt))
        (dolist (occ poss-occs)
            (setf (occurrence-is-action occ) t)
            (setf (occurrence-is-event occ) nil)
            (setf (occurrence-is-assumption occ) t)
            (when (not (member (get-self-fact expgen) (occurrence-pre occ) :test #'equal-fact))
                (setf (occurrence-pre occ)
                    (remove 'self (occurrence-pre occ) :key #'fact-type)
                )
                (push occ cur-occs)
                (add-occ-to-layer occ cur-layer layers fact-occs)
            )
        )
    )
    cur-occs
) 

(defun annotate-with-possible-worlds (expgen acts pt)
    (dolist (act acts)
        (push 
            (make-fact 
                :type 'action-taken 
                :args (list pt) 
                :value (occurrence-signature act)
            )
            (occurrence-pre act)
        )
        (push 
            (make-fact 
                :type 'taken-action
                :args (occurrence-signature act)
                :value t
            )
            (occurrence-post act)
        )
    )
    acts
)

(defun initialize-possible-worlds-layers (expgen pt &aux consequences shared-facts unshared-facts facts worlds layer poss-world-fact)
    (setq consequences (mapcar+ #'get-predicted-state (get-plausible-explanations expgen) expgen))
    (setq shared-facts (reduce #'(lambda (facts1 facts2) (intersection facts1 facts2 :test #'equal-fact)) consequences))
    (setq consequences (mapcar+ #'set-difference consequences shared-facts :test #'equal-fact))
    (setq unshared-facts (reduce #'(lambda (facts1 facts2) (union facts1 facts2 :test #'same-map)) consequences :initial-value nil))
    (setq layer (make-layer :pt pt))
    (setq worlds 0)
    (dolist (conseq-set consequences)
        (setq poss-world-fact 
            (make-fact
                :type 'possible-world
                :args nil
                :value (incf worlds)
            )
        )
        (dolist (f (set-difference unshared-facts conseq-set :test #'equal-fact))
            (create-direct-mutex poss-world-fact f layer)
        )
    )
    (setq facts (append shared-facts unshared-facts))
    
    (setf (layer-fact-back-links layer) 
        (mapcar #'list 
            (remove-if-not 
                #'(lambda (fact) (is-transient expgen fact))  
                facts
            )
        )
    )
    (setf (layer-full-mutex-hash layer) (layer-new-mutex-hash layer))
    (values 
        (list (cons pt layer)) 
        facts
        (mapcar #'(lambda (fact) (cons fact :original)) facts)
    )
)

(defun get-predicted-state (explanation expgen)
    (nth-value 1 (extend-explanation expgen explanation nil))
)


(defun make-process-layer (expgen facts fact-occs layers duration cur-time pt 
                           &aux new-layer)
    (setq new-layer (make-layer :pt pt))
    (push (cons pt new-layer) layers)
    (multiple-value-bind (changed-facts time-change orig-fact-values 
                          new-fact-values time-passes-occ
                         )
            (advance-processes expgen (new-fact-set facts) duration pt)
        (declare (ignore orig-fact-values))
        (setq cur-time (+ cur-time time-change))
        (when *check-time-congruency*
            (let (poss-ex)
                (setf (get-execution-history expgen) 
                    (append (get-execution-history expgen) 
                        (list (make-occurrence 
                                :signature '(observation) 
                                :is-observation t :pre nil :point nil))))
                (setq poss-ex (construct-explanation expgen (get-assumptions (car (get-plausible-explanations expgen)))))
                (setf (get-execution-history expgen) (butlast (get-execution-history expgen))) 
                (when (not (member time-passes-occ (get-events poss-ex) :test #'occurrence-equal))
                    (break "Time-passes wrong.")
                )
            )
        )
        (add-occ-to-layer time-passes-occ new-layer layers fact-occs)
        (dolist (new-fact new-fact-values)
            (push (cons new-fact time-passes-occ) fact-occs)
        )
        (setq facts changed-facts)
        (setf (layer-full-mutex-hash new-layer) 
            (make-full-mutex-hash layers new-fact-values :continuation t)
        )
        (values new-layer cur-time facts fact-occs new-fact-values)
    )
)    

(defun cycle-in-layers (new-occs layers)
    (when (null new-occs)
        (return-from cycle-in-layers nil)
    )
    (occurrence-cycle-has-formed
        new-occs
        ;(nthcdr (length new-occs)
            (mapcar #'car 
                (apply #'append 
                    (mapcar #'layer-occurrence-back-links 
                        (mapcar #'cdr (cdr layers))
                    )
                )
            )
        ;)
    )
)


(defun get-hash-keys (hash &aux (keys nil))
    (maphash #'(lambda (key val) (declare (ignore val)) (push key keys)) hash)
    keys
)

(defun get-all-mutexes (table &aux ret-mutex-pairs)
    (setq ret-mutex-pairs nil)
    (dolist (key (get-hash-keys table))
        (dolist (val (gethash key table))
            (when (not (contradicts key val))
                (push (cons key val) ret-mutex-pairs)
            )
        )
    )
    ret-mutex-pairs
)

(defun equal-fact-pair (fact-cons1 fact-cons2)
    (and (equal-fact (car fact-cons1) (car fact-cons2))
         (equal-fact (cdr fact-cons1) (cdr fact-cons2))
    )
)

(defun mutex-facts-in (precs fact-occs layers &aux pre1)
    (push *true-fact* precs)
    (loop while precs do
        (setq pre1 (car precs))
        (dolist (pre2 (cdr precs))
            (when (facts-are-mutex pre1 pre2 fact-occs layers)
                (return-from mutex-facts-in t)
            )
        )
        (setq precs (cdr precs))
    )
    nil
)

(defun triggering-facts (envmodel occ available-triggers &aux actual-triggers)
    (dolist (possible-trigger (occurrence-pre occ))
        (dolist (available-trigger available-triggers)
            (when (unify envmodel (fact-to-cnd possible-trigger) (list available-trigger) nil :assume-fn #'assume-nothing)
                (push available-trigger actual-triggers)
            )
        )
    )
    (dolist (possible-relieved-constraint (mapcar #'negate-condition (occurrence-constraints occ)))
        (dolist (available-trigger available-triggers)
            (when (unify envmodel possible-relieved-constraint (list available-trigger) nil :assume-fn #'assume-nothing)
                (push available-trigger actual-triggers)
            )
        )
    )
    actual-triggers
)

(defun occ-possible (expgen occ new-facts fact-occs layers &aux new-new-facts)
;    (when (eq (car (occurrence-signature occ)) 'rover::compass-confused)
;        (break "Is occ possible?")
;    )
    (setq new-facts (triggering-facts (get-last-envmodel expgen) occ new-facts))
    (when (null new-facts)
        ;; Occurrence isn't possible if none of the conditions were just applied.
        (return-from occ-possible nil)
    )
    (when (mutex-facts-in (occurrence-pre occ) fact-occs layers)
        ;; Occurrence isn't possible if any conditions are mutex.
        (return-from occ-possible nil)
    )
    (let ((mutex-pairs (layer-immediate-mutexes (cdadr layers)))
          (new-fact-not-mutex t)
         )
        ;; Iterate over the new-facts to find the ones that could actually have
        ;; caused the event to occur.
        (dolist (new-fact new-facts)
            (setq new-fact-not-mutex t)
            (dolist (other-fact (occurrence-pre occ))
                (when (and new-fact-not-mutex 
                           (member (cons new-fact other-fact) mutex-pairs :test #'equal-fact-pair)
                      )
                    (setq new-fact-not-mutex nil)
                )
            )
            (when new-fact-not-mutex
                ;; Only new facts that are not immediate mutex to other conditions
                ;; could actually have caused the new event.
                (push new-fact new-new-facts)
            )
        )
    )
    (when (null new-new-facts)
        ;; Occurrence isn't possible if none of the conditions were just applied.
        (return-from occ-possible nil)
    )
;    (when *trace-occurrence-consideration*
;        (format t " Needs history.")
;    )
    (return-from occ-possible 
        (consistent-history-exists 
            expgen (occurrence-pre occ) new-new-facts fact-occs (cdr layers) nil
        )
    )
)


;; Notes on algorithm: consistent-history-exists is called recursively in two 
;; ways. One to continue proving that the current layer of occurrences is 
;; consistent, the other to move on to the next layer of occurrences. In each
;; invocation, either a new occurrence is added that must have occurred, or 
;; a new layer is begun. Moving to next layer requires 
;; (and (null new-facts) (not search-again)). The variable search-again 
;; indicates that a search to prove a fact is necessary. Note that this function
;; does not require that all occurrences in each layer have a proximate cause,
;; one is enough.
;;
;; facts: Facts that must be proven possible
;; new-facts: One of the new-facts must have occurred in the prior layer
;; fact-occs: Standard alist mapping from facts to occurrences that may have set
;;            those facts in the past.
;; layers: standard GraphPlan-esque data structure to maintain event history info
;; last-occs: The occurrences that have been added to the current layer so far
(defun consistent-history-exists (expgen facts new-facts fact-occs layers last-occs 
                                  &aux cur-occs next-new-facts ret next-occs 
                                       next-facts search-facts search-again)
    (when *trace-consistent-history-exists*
        (format t "~%~A Facts: ~A" (caar layers) facts)
        (format t "~%~A New Facts: ~A" (caar layers) new-facts)
        (format t "~%~A New Occurrence: ~A" (caar layers) (car last-occs))
    )
    (when (or (null (cdr layers)) (and (null new-facts) (null last-occs)))
;        (format t "~%Reached top layer.")
        (return-from consistent-history-exists '(()))
    )
    (setq cur-occs (mapcar #'first (layer-occurrence-back-links (cdar layers))))
    (if new-facts
        ;then
        (setq search-facts new-facts)
        ;else
        (setq search-facts facts)
    )
    
    ;; Null new-facts is true on the second iteration of a new occurrence layer,
    ;; after the next occurrence layer has been shown to be achievable.
    ;;
    ;; This block is responsible for determining what facts need to be found in
    ;; the current (newest) layer.
    (when (null new-facts)
        ;; Check for mutex occurrences among last-occs
        (dolist (old-occ (cdr last-occs))
            (when (occurrences-are-mutex old-occ (car last-occs) (cdar layers))
                (return-from consistent-history-exists nil)
            )
        )
        
        (setq next-new-facts (apply #'append (mapcar #'occurrence-pre last-occs)))
        (setq search-again nil)
        
        ;; Go over the facts that need to be proven. 
        (loop for fact in facts while (not search-again) do
            (let ((rel-occs (remove fact fact-occs :key #'car :test-not #'equal-fact)))
                (when (and ;;The fact has been set (assumed facts can be assumed later)
                           rel-occs
                           ;;The fact was not given when find-possible-events was called.
                           (not (eq (cdar (last rel-occs)) :original)) 
                           ;; The first occurrence that sets the fact occurs 
                           ;; during (or after?) the current layer
                           (not (occurrence-earlier (cdar (last rel-occs)) (car last-occs))) 
                      )
                    (setq search-facts (list fact))
                    (setq search-again t)
                )
            )
            (when (member fact next-new-facts :test #'contradicts)
                ;; Two contradictory preconditions cannot both be met.
                (setq search-facts (list fact))
                (setq search-again t)
            )
        )
        
        (when (not search-again)
            ;; Check for mutex facts among facts
            (let ((rem-facts (append facts next-new-facts)) fact1)
                (loop while (and rem-facts (not search-again)) do
                    (setq fact1 (car rem-facts))
                    (setq rem-facts (cdr rem-facts))
                    (dolist (fact2 rem-facts)
                        (when (facts-are-mutex fact1 fact2 fact-occs (cdr layers)) 
                            (setq search-again t)
                        )
                    )
                )
            )
        )
            
        
        ;; Some fact in new-facts is proven possible, no facts that cannot be 
        ;; proven later are left. Try moving back in time to prove next layer.
        (when (not search-again)
            ;; Check for new facts that can't be proven.
            (dolist (fact next-new-facts)
                (let ((rel-occs (remove fact fact-occs :key #'car :test-not #'equal-fact)))
                    (when (and ;; No prior reason to believe the fact
                               (null rel-occs) 
                               ;; The fact isn't hidden
                               (is-observable expgen fact) 
                               ;; The fact isn't true in a closed world
                               (not (null (fact-value fact)))
                          )
                        ;; Fact can't be proven.
                        (return-from consistent-history-exists nil)
                    )
                )
            )
    
            ;; Recurse to the next occurrence layer.
            (setq ret
               (consistent-history-exists 
                   expgen (append facts next-new-facts)
                   next-new-facts fact-occs (cdr layers) nil
               )
            )
            (when ret
                (return-from consistent-history-exists
                    (cons last-occs ret)
                )
            )
        )
    )
    (when *trace-consistent-history-exists*
        (format t "~%~A Search facts: ~A" (caar layers) search-facts)
    )
    (dolist (fact search-facts)
        ;; next-occs: The occs capable of satisfying the current-fact
        (setq next-occs
            (remove fact cur-occs :test-not #'satisfied-by)
        )
        ;; Branch over the possible occs to find ones that do not contradict
        ;; the known (necessary) future. Attempt to develop a consistent history
        ;; with each.
        (dolist (occ next-occs)
            (when *trace-consistent-history-exists*
                (format t "~%~A Branching fact: ~A" (caar layers) fact)
                (format t "~%~A Selected cause: ~A" (caar layers) (occurrence-signature occ))
            )
            ;; Remove the facts set by the current occ, which no longer need to 
            ;; be proven.
            (setq next-facts (set-difference facts (occurrence-post occ) :test #'equal-fact))
            ;; Test that the current occurrence does not contradict any unproven
            ;; facts from the future, which would make necessary occurrences 
            ;; impossible.
            (unless (intersection next-facts (occurrence-post occ) :test #'contradicts)
                (setq ret
                   (consistent-history-exists 
                       expgen next-facts nil fact-occs layers (cons occ last-occs)
                   )
                )
                (when ret
                    (return-from consistent-history-exists ret)
                )
            )
        )
    )
    (when *trace-consistent-history-exists*
        (format t "~%~A Failure in search for facts: ~A" (caar layers) search-facts)
    )
    nil
)

(defun satisfied-by (fact ev)
    (member fact (occurrence-post ev) :test #'equal-fact)
)

(defun contradicted-by (fact ev)
    (member fact (occurrence-post ev) :test #'contradicts)
)


(defun add-direct-mutex-list (mutexes layer)
    (dolist (mutex mutexes)
        (create-direct-mutex (car mutex) (cdr mutex) layer)
    )
)

;; adds a new mutex to the mutexes generated starting at this layer
(defun create-direct-mutex (fact1 fact2 layer)
    (let ((hlist1 (gethash fact1 (layer-new-mutex-hash layer))))
        (when (not (member fact2 hlist1 :test #'equal-fact))
            (setf (gethash fact1 (layer-new-mutex-hash layer)) 
                (cons fact2 hlist1)
            )
        )
    )
)

(defun clear-mutexes (layer)
    (clrhash (layer-new-mutex-hash layer))
    (setf (layer-continuation-mutexes layer) nil)
    (setf (layer-immediate-mutexes layer) nil)
)

;; Creates new layer's efficient full mutex hash table, using information from 
;; new layer's table of new known mutexes, as well as the prior layer's list of 
;; all known mutexes, and continuation mutexes that have not yet been used.
;;
;; layers should be a list of all planning graph layers, with the most recent 
;;        first.
;; new-facts should be a list of all facts established in the most recent layer;
;;           the complete mutexes of these facts are assumed to be present in 
;;           the newest layer's table of new known mutexes.
(defun make-full-mutex-hash (layers new-facts &key (continuation nil))
    ; (when (cdr layers)
        ; (setf (layer-continuation-mutexes (cdr (first layers)))
            ; (merge-continuation-mutexes
                ; (layer-continuation-mutexes (cdr (first layers)))
                ; (layer-continuation-mutexes (cdr (second layers)))
                ; new-facts
            ; )
        ; )
    ; )
    (add-mutexes-to-hash
        (new-fact-hash-table)
        (merge-old-facts-together
            (layer-new-mutex-hash (cdr (first layers)))
            (if (null (cdr layers))
                ;then
                (new-fact-hash-table)
                ;else
                (layer-new-mutex-hash (cdr (second layers)))
            )
            new-facts
            (if continuation
                ;then
                (layer-continuation-mutexes (cdr (first layers)))
                ;else
                nil
            )
        )
    )
)

#+SBCL(sb-ext::define-hash-table-test equal-fact hash-fact)
(defun new-fact-hash-table (&key (size :unknown))
    (if (eq size :unknown)
        ;then
        #-(or CLISP ABCL) (make-hash-table :test 'equal-fact :hash-function 'hash-fact)
        #+(or CLISP ABCL) (make-hash-table :test 'equalp)
        ;else
        #-(or CLISP ABCL) (make-hash-table :test 'equal-fact :hash-function 'hash-fact :size size)
        #+(or CLISP ABCL) (make-hash-table :test 'equalp :size size)
    )
)

(defun copy-fact-hash-table (old-hash)
    (let ((new-hash (new-fact-hash-table :size (hash-table-count old-hash))))
        (maphash 
            #'(lambda (key value) (setf (gethash key new-hash) value))
            old-hash
        )
        new-hash
    )
)

;; Responsible for pushing forward all former continuation mutexes that aren't 
;; overwritten by new info in the newest layer.
(defun merge-continuation-mutexes (new-continuation-mutexes old-continuation-mutexes new-facts)
    (dolist (mutex old-continuation-mutexes)
        (when (and (not (member (car mutex) new-facts :test #'equal-fact))
                   (not (member (cdr mutex) new-facts :test #'equal-fact)))
            (push mutex new-continuation-mutexes)
        )
    )
    new-continuation-mutexes
)

;; Adds the mutexes known from the prior layer into the mutex hash table on the 
;; new layer, skipping any mutexes that are overwritten at the current layer.
(defun merge-old-facts-together (new-mutex-hash prior-mutex-hash new-facts extra-mutex-list &aux merged-hash)
    (dolist (mapping (hash-to-alist prior-mutex-hash))
        (when (not (member (first mapping) new-facts :test #'same-map))
            (setf (gethash (first mapping) new-mutex-hash)
                (set-difference (rest mapping) new-facts :test #'same-map)
            )
        )
    )
    
    (setq merged-hash (copy-fact-hash-table new-mutex-hash))

    ;; Adding extra mutex pairs (for continuation)
    (dolist (mutex extra-mutex-list)
        (pushnew (cdr mutex) (gethash (car mutex) merged-hash) :test #'equal-fact)
    )

    ;;Ensure bidirectionality.
    (ensure-bidirectional merged-hash)
    merged-hash
)

(defun ensure-bidirectional (mutex-hash)
    (maphash 
        #'(lambda (fact mutex-facts)
            (dolist (mutex-fact mutex-facts)
                (pushnew fact (gethash mutex-fact mutex-hash) :test #'equal-fact)
            )
          )
        mutex-hash
    )
    mutex-hash
)


(defun add-mutexes-to-hash (full-table direct-table)
    (dolist (fact (mapcar #'car (hash-to-alist direct-table)))
        (when (eq :fail (gethash fact full-table :fail))
            (add-fact-to-hash fact full-table nil direct-table)
        )
    )
    (dolist (key (mapcar #'car (hash-to-alist full-table)))
        (when (null (gethash key full-table))
            (remhash key full-table)
        )
    )
    full-table
)


(defun add-fact-to-hash (fact mutex-hash facts-in-chain direct-table &aux mutex-facts)
    (when (null fact)
        (return-from add-fact-to-hash nil)
    )
;    (format t "~%~A. Add fact: ~A" (length facts-in-chain) fact)
    (let ((cur-hash (gethash fact mutex-hash))
          (cycle (cdr (member fact (reverse facts-in-chain) :test #'equal-fact)))
          (orig-fact fact))
        (when cycle 
            (when (and cur-hash (eq (car cur-hash) 'tautology))
                (setq orig-fact (second cur-hash))
            )
            (dolist (taut-fact cycle)
;                (when (eq (car (gethash taut-fact mutex-hash)) 'tautology)
;                    (break "Not sure what to do here.")
;                )
                (when (eq (car (gethash taut-fact mutex-hash)) 'tautologies)
                    (setq cycle (union cycle (cdr (gethash taut-fact mutex-hash))))
                )
            )
            (dolist (taut-fact cycle)
                (setf (gethash taut-fact mutex-hash) (list 'tautology orig-fact))
            )
            (setf (gethash orig-fact mutex-hash) 
                (cons 'tautologies 
                    (union cycle (cdr (gethash orig-fact mutex-hash)) :test #'equal-fact)
                )
            )
;            (format t "~%~A. Cycle: ~A Original fact: ~A Cycle: ~A"  (length facts-in-chain) fact orig-fact cycle)
            (return-from add-fact-to-hash nil)
        )
        (when (not (null cur-hash))
            (when (eq (car cur-hash) 'tautology)
                ;; The tautology is still being calculated; therefore the 
                ;; contribution of this node will be found elsewhere.
                (return-from add-fact-to-hash nil)
            )
;            (format t "~%~A. Known fact: ~A"  (length facts-in-chain) fact)
            (return-from add-fact-to-hash cur-hash)
        )
    )
    (setq mutex-facts (gethash fact direct-table))
    (when (is-binary fact)
        (when (some #'(lambda (ofact) (contradicts fact ofact)) mutex-facts)
            (break "There should be no contradictory facts in the direct-table.")
        )
        (setq mutex-facts (remove (contradictory-fact fact) mutex-facts :test #'equal-fact))
    )
    ; (let ((old-length (length mutex-facts)))
        ; (setq mutex-facts (remove-duplicates mutex-facts :test #'equal-fact))
        ; (when (< (length mutex-facts) old-length)
            ; (break "Duplicates in mutex facts list.")
        ; )
    ; )
    (setq mutex-facts
        (merge-sets
            (cons mutex-facts
                    (mapcar+ #'add-fact-to-hash
                        (mapcar #'contradictory-fact 
                            (remove-if-not #'is-binary mutex-facts)
                        )
                        mutex-hash (cons fact facts-in-chain) direct-table
                    )
            )
            :test #'equal-fact
        )
    )
    (when (or (member *true-fact* mutex-facts) 
              (member fact mutex-facts :test #'equal-fact)
          )
;        (break "Using true.")
        (setq mutex-facts (list *true-fact*))
    )
    (when (eq (car (gethash fact mutex-hash)) 'tautology)
;        (format t "~%~A. Tautology exit: ~A"  (length facts-in-chain) fact)
        (return-from add-fact-to-hash mutex-facts)
    )
    (when (eq (car (gethash fact mutex-hash)) 'tautologies)
        (dolist (taut-fact (cdr (gethash fact mutex-hash)))
            (setf (gethash taut-fact mutex-hash) mutex-facts)
        )
;        (when (find 'tautology (hash-to-alist mutex-hash) :key #'second)
;            (break "Tautology left over.")
;        )
;        (format t "~%~A. Tautology completion: ~A"  (length facts-in-chain) fact)
    )
    (setf (gethash fact mutex-hash) mutex-facts)
;    (format t "~%~A. Returning from fact: ~A Value: ~A"  (length facts-in-chain) fact mutex-facts)
    mutex-facts
)

(defun merge-sets (set-list &key (key #'identity) (test #'eq) &aux (ret-set nil) (new-keys nil) (old-keys nil))
    (setq old-keys (mapcar key (first set-list)))
    (setq ret-set (first set-list))
    (dolist (cur-set set-list)
        (dolist (cur-elem cur-set)
            (let ((cur-key (funcall key cur-elem)))
                (when (not (member cur-key old-keys :test test))
                    (push cur-key new-keys)
                    (push cur-elem ret-set)
                )
            )
            (setq old-keys (append new-keys old-keys) new-keys nil)
        )
    )
    ret-set
)
        

;; Checks two facts to determine whether they are mutually exclusive after the 
;; execution of events in the current layer. The value of "old-fact-occs" must
;; map facts to the most recent occurrence that set them *prior to* the current
;; layer. Similarly, old-facts must contain all facts that may have been set 
;; prior to the current layer. If the facts contradict, they are *not* mutually
;; exclusive. If the facts cannot be both true after the current layer 
;; completes, returns t and adds the fact mutexes to the current layer. 
;; Otherwise, returns nil.
(defun decide-fact-mutex (expgen cur-layer new-fact other-fact old-fact-occs old-facts
                          layers known-facts &aux new-causes other-causes new-existed 
                          other-existed all-causes (use-cache t))
    (declare (ignore use-cache))
    ;; If they contradict trivially, no need to include in mutexes.
    (when (contradicts new-fact other-fact)
        (return-from decide-fact-mutex nil)
    )
    
    ;; If they are caused by two separate occurrences at the same time that
    ;; can occur together, they are not mutex.
    (setq all-causes (mapcar #'first (layer-occurrence-back-links cur-layer)))
    (setq new-causes (cdr (assoc new-fact (layer-fact-back-links cur-layer) :test #'equal-fact)))
    (if (eq other-fact *true-fact*)
        ;then
        (setq other-causes all-causes)
        ;else
        (setq other-causes (cdr (assoc other-fact (layer-fact-back-links cur-layer) :test #'equal-fact)))
    )
    
    (when (causes-contradictory-fact other-fact new-fact cur-layer known-facts other-causes)
        (create-direct-mutex new-fact other-fact cur-layer)
        (return-from decide-fact-mutex t)
    )

    (dolist (other-cause other-causes)
        (dolist (new-cause new-causes)
            (when (not (occurrences-are-mutex new-cause other-cause cur-layer))
                (return-from decide-fact-mutex nil)
            )
        )
    )
    
    ;; If here, there is no compatible set of events. However, one or both might
    ;; have already existed. If so, we should consider the no-op occurrence for
    ;; pre-existing facts.
    (setq other-existed (was-assumable expgen other-fact old-facts))
    (when (and other-existed new-causes
               (fact-could-persist-through expgen other-fact new-causes old-facts old-fact-occs layers))
        (return-from decide-fact-mutex nil)
    )
    
    (setq new-existed (was-assumable expgen new-fact old-facts))
    (when (and new-existed other-causes
               (fact-could-persist-through expgen new-fact other-causes old-facts old-fact-occs layers))
        (when new-causes
            ;; The fact new-fact will be used to determine whether an occurrence
            ;; has newly become possible in the next layer, but the old fact 
            ;; doesn't persist when the new one is caused (see prior criterion).
            (push (cons new-fact other-fact) (layer-immediate-mutexes cur-layer))
        )
        (return-from decide-fact-mutex nil)
    )

    ;; If here, there is no way for one fact to be set by an occurrence while
    ;; the other remains. However, *both* could remain, *if* they aren't 
    ;; already mutex.
    (when (and new-existed other-existed
               (not (facts-are-mutex new-fact other-fact old-fact-occs layers))
               ;; MCM: Commented this out 11/14/17 to see if that improves speed in rovers domain.
               (all-facts-could-persist-through expgen (list new-fact other-fact) all-causes old-facts old-fact-occs layers 
                   :needs-compatible-cause nil 
                   :assume-fn #'default-closed-world-assume-fn
               )
          )
        (when new-causes
            ;; The fact new-fact will be used to determine whether an occurrence
            ;; has newly become possible in the next layer, but no occurrence 
            ;; involving both facts can have newly become possible.
            (push (cons new-fact other-fact) (layer-immediate-mutexes cur-layer))
        )
        (when (no-consistent-cause-exists expgen new-fact other-fact all-causes old-fact-occs layers 
                    :assume-fn #'default-closed-world-assume-fn
              )
            ;; We already know that neither fact can be caused while the other 
            ;; exists. This says that no occurrence can occur while the facts 
            ;; persist. Therefore, no further occurrences can be caused.
            (push (cons new-fact other-fact) (layer-continuation-mutexes cur-layer))
        )
        (return-from decide-fact-mutex nil)
    )
    
;    (break "before mutex creation")
    (create-direct-mutex new-fact other-fact cur-layer)
;    (break "6")
    t
)

;; Checks two facts to determine whether they are mutually exclusive after the 
;; execution of events in the current layer. The value of "old-fact-occs" must
;; map facts to the most recent occurrence that set them *prior to* the current
;; layer. Similarly, old-facts must contain all facts that may have been set 
;; prior to the current layer. If the facts contradict, they are *not* mutually
;; exclusive. If the facts cannot be both true after the current layer 
;; completes, returns t and adds the fact mutexes to the current layer. 
;; Otherwise, returns nil.
(defun decide-action-mutex (expgen cur-layer new-fact other-fact old-fact-occs old-facts
                            layers &aux new-causes other-causes new-existed 
                            other-existed all-causes)
    ;; If they contradict trivially, no need to include in mutexes.
    (when (contradicts new-fact other-fact)
        (return-from decide-action-mutex nil)
    )
    
    ;; If they are caused by two separate occurrences at the same time that
    ;; can occur together, they are not mutex.
    (setq all-causes (mapcar #'first (layer-occurrence-back-links cur-layer)))
    (setq new-causes (cdr (assoc new-fact (layer-fact-back-links cur-layer) :test #'equal-fact)))
    (if (eq other-fact *true-fact*)
        ;then
        (setq other-causes all-causes)
        ;else
        (setq other-causes (cdr (assoc other-fact (layer-fact-back-links cur-layer) :test #'equal-fact)))
    )
    
    ;; Any action that causes both means non-mutex.
    (when (intersection other-causes new-causes)
        (return-from decide-action-mutex nil)
    )
    
    ;; No action causes both. However, one or both might
    ;; have already existed. If so, we should consider the no-op occurrence for
    ;; pre-existing facts.
    (setq other-existed (was-assumable expgen other-fact old-facts))
    (when other-existed
        (dolist (new-cause new-causes)
            (when (cause-is-compatible expgen other-fact new-cause old-fact-occs layers)
                (return-from decide-action-mutex nil)
            )
        )
    )
    
    (setq new-existed (was-assumable expgen new-fact old-facts))
    (when new-existed
        (dolist (other-cause other-causes)
            (when (cause-is-compatible expgen new-fact other-cause old-fact-occs layers)
                (when new-causes
                    ;; The fact new-fact will be used to determine whether an occurrence
                    ;; has newly become possible in the next layer, but the old fact 
                    ;; doesn't persist when the new one is caused (see prior criterion).
                    (push (cons new-fact other-fact) (layer-immediate-mutexes cur-layer))
                )
                (return-from decide-action-mutex nil)
            )
        )
    )

    ;; If here, there is no way for one fact to be set by an occurrence while
    ;; the other remains. However, *both* could remain, *if* they aren't 
    ;; already mutex.
    (when (and new-existed other-existed
               (not (facts-are-mutex new-fact other-fact old-fact-occs layers))
          )
        (when new-causes
            ;; The fact new-fact will be used to determine whether an occurrence
            ;; has newly become possible in the next layer, but no occurrence 
            ;; involving both facts can have newly become possible.
            (push (cons new-fact other-fact) (layer-immediate-mutexes cur-layer))
        )
        (when (no-consistent-cause-exists expgen new-fact other-fact all-causes old-fact-occs layers 
                    :assume-fn #'default-closed-world-assume-fn
              )
            ;; We already know that neither fact can be caused while the other 
            ;; exists. This says that no occurrence can occur while the facts 
            ;; persist. Therefore, no further occurrences can be caused.
            (push (cons new-fact other-fact) (layer-continuation-mutexes cur-layer))
        )
        (return-from decide-action-mutex nil)
    )
    
    (create-direct-mutex new-fact other-fact cur-layer)
    t
)

;; Checks whether one fact, by itself, is sufficient to cause an event that 
;; prevents another fact from being true, and the causing fact could not become 
;; true independently.
;; 
;; i.e., at(cause-fact, t)      -> not(at(failed-fact, t+1)).
;; and   not(at(cause-fact, t)) -> not(at(cause-fact, t+1))
(defun causes-contradictory-fact (cause-fact failed-fact cur-layer known-facts other-causes &aux poss-evs)
    (when other-causes
        (return-from causes-contradictory-fact nil)
    )
    (setq poss-evs 
        (apply #'append 
            (mapcar #'cdr
                (remove failed-fact (layer-fact-back-links cur-layer) 
                    :test-not #'contradicts :key #'car
                )
            )
        )
    )
    (dolist (ev poss-evs)
        (let ((conditions (occurrence-pre ev)))
            (when (member cause-fact conditions :test #'equal-fact)
                (setq conditions (remove cause-fact conditions :test #'equal-fact))
                (when (subsetp conditions known-facts :test #'equal-fact)
                    (return-from causes-contradictory-fact t)
                )
            )
        )
    )
    nil
)

; (defun eliminate-contradictions (causes old-facts new-fact other-fact mutex-hash 
                                 ; &aux (changed-facts nil) (removed-facts nil) 
                                      ; (mutex-facts nil) (use-cache t) old-length)
    ; (setq mutex-facts
        ; (union
            ; (gethash new-fact   mutex-hash)
            ; (gethash other-fact mutex-hash)
            ; :test #'equal-fact
        ; )
    ; )
    ; (dolist (old-fact old-facts)
        ; (if (or (contradicts new-fact   old-fact)
                ; (contradicts other-fact old-fact)
                ; (member old-fact mutex-facts :test #'equal-fact)
            ; )
            ; ;then
            ; (push old-fact removed-facts)
            ; ;else
            ; (push old-fact changed-facts)
        ; )
    ; )
    ; (when removed-facts
        ; (setq old-facts changed-facts)
        ; (setq old-length (length causes))
        ; (setq causes 
            ; (remove removed-facts causes 
                ; :key #'occurrence-pre :test #'shared-facts
            ; )
        ; )
        ; (when (> old-length (length causes))
            ; (setq use-cache nil)
        ; )
    ; )
    ; (values causes old-facts use-cache)
; )

(defun shared-facts (fact-list1 fact-list2)
    (dolist (cur-fact fact-list1)
        (when (member cur-fact fact-list2 :test #'equal-fact)
            (return-from shared-facts t)
        )
    )
)

;; Returns t if some occurrence in causes can happen while fact "fact" holds, 
;; and that occurrence does not entail a contradiction of fact "fact".
;; Otherwise returns nil.
(defun compatible-cause-exists (expgen fact causes old-fact-occs layers &key (assume-fn #'assume-closed-world))
    (dolist (cause causes)
        (when (cause-is-compatible expgen fact cause old-fact-occs layers :assume-fn assume-fn)
            (return-from compatible-cause-exists t)
        )
    )
    nil
)

#-ABCL
(defun all-occs-shared (list1 list2)
    (when (not (eql (length list1) (length list2)))
        (format t "~%Unequal length in all-occs-shared.")
        (return-from all-occs-shared nil)
    )
    (let ((hashes1 (sort (copy-list list1) #'< :key #'occurrence-hash) )
          (hashes2 (sort (copy-list list2) #'< :key #'occurrence-hash) ))
        (loop for x in hashes1 for y in hashes2 do
            (when (not (occurrence-equal x y)) 
                ;(break "Returning nil from all-occs-shared")
                (return-from all-occs-shared nil)
            )
        )
    )
;    (format t "~%Returning t from all-occs-shared.")
    t
)

#+ABCL
(defun all-occs-shared (list1 list2)
    (null (set-exclusive-or list1 list2 :test #'occurrence-equal))
)

(defun set-known-incompatible (fact causes compatibility)
    (setf (gethash (cons fact causes) *incompatibility-result*) compatibility)
)

(defun known-incompatible (fact causes)
    (gethash (cons fact causes) *incompatibility-result*)
)

;; Return t if the fact "fact" could persist while at least one cause in "causes" occurs, and there
;; is some possible world in which each incompatible cause in "causes" does not occur.
; (defun fact-could-persist-through (expgen fact causes old-facts old-fact-occs layers
                                   ; &key (needs-compatible-cause t) (use-cache t)
                                        ; (assume-fn #'default-closed-world-assume-fn)
                                   ; &aux filtered-causes compatible-cause-exists)
    ; (when (eq fact *true-fact*)
        ; (when (null causes)
            ; (return-from fact-could-persist-through t)
        ; )
        ; (return-from fact-could-persist-through 
            ; (all-may-not-occur expgen causes old-facts use-cache)
        ; )
    ; )
    ; (when use-cache
        ; (case (known-incompatible fact causes)
            ; ((t) ;(format t "~%Known-incompatible helped (t). Cause size = ~A" (length causes)) 
                ; (return-from fact-could-persist-through t))
            ; ('f ;(format t "~%Known-incompatible helped (f). Cause size = ~A" (length causes))
                ; (when needs-compatible-cause
                    ; (return-from fact-could-persist-through nil)
                ; )
            ; )
        ; )
    ; )
    ; (setq filtered-causes causes)
    ; (setq compatible-cause-exists (not needs-compatible-cause))
    ; (dolist (cause causes)
        ; (when (cause-is-compatible expgen fact cause old-fact-occs layers :assume-fn assume-fn)
            ; (setq filtered-causes (remove cause filtered-causes))
            ; (setq compatible-cause-exists t)
        ; )
    ; )
    ; (when (null filtered-causes)
        ; (return-from fact-could-persist-through t)
    ; )
; ;    (when (cdr filtered-causes)
; ;        (format t "~%Checking fact ~A with ~A incompatible causes." fact (length filtered-causes))
; ;    )
; ;     (and compatible-cause-exists (all-may-not-occur filtered-causes old-facts))
    ; (if (and compatible-cause-exists (all-may-not-occur expgen filtered-causes old-facts use-cache))
        ; ;then
        ; (or (when (and needs-compatible-cause use-cache) (set-known-incompatible fact causes t)) t)
        ; ;else
        ; (and (when use-cache (set-known-incompatible fact causes 'f)) nil)
    ; )
; )


(defun fact-could-persist-through (expgen fact causes old-facts old-fact-occs layers
                                   &key (needs-compatible-cause t) (use-cache t)
                                        (assume-fn #'default-closed-world-assume-fn))
    (when (eq fact *true-fact*)
        (when (null causes)
            (return-from fact-could-persist-through t)
        )
        (return-from fact-could-persist-through 
            (all-may-not-occur expgen causes old-facts (layer-full-mutex-hash (cdadr layers)) use-cache)
        )
    )
    (all-facts-could-persist-through 
        expgen (list fact) causes old-facts old-fact-occs layers 
        :needs-compatible-cause needs-compatible-cause
        :use-cache use-cache
        :assume-fn assume-fn
    )
)


;; Return t if the facts supplied could all persist while at least one cause in "causes" occurs, and there
;; is some possible world in which each incompatible cause in "causes" does not occur.
(defun all-facts-could-persist-through (expgen facts causes old-facts old-fact-occs layers
                                   &key (needs-compatible-cause t) (use-cache t)
                                        (assume-fn #'default-closed-world-assume-fn)
                                   &aux filtered-causes compatible-cause-exists found-incompatibility)
    ;(setq use-cache (and use-cache (null (cdr (remove *true-fact* facts)))))
    (setq use-cache (and use-cache (null (cdr facts))))
    (when use-cache
        ;(case (known-incompatible (car (remove *true-fact* facts)) causes)
        (case (known-incompatible (car facts) causes)
            ((t) ;(format t "~%Known-incompatible helped (t). Cause size = ~A" (length causes)) 
                (return-from all-facts-could-persist-through t))
            ('f ;(format t "~%Known-incompatible helped (f). Cause size = ~A" (length causes))
                (when needs-compatible-cause
                    (return-from all-facts-could-persist-through nil)
                )
            )
        )
    )
    (setq filtered-causes nil)
    (setq compatible-cause-exists (not needs-compatible-cause))
    (dolist (cause causes)
        (setq found-incompatibility nil)
        (dolist (fact facts)
            (when (and (not found-incompatibility)
                       (not (cause-is-compatible expgen fact cause old-fact-occs layers :assume-fn assume-fn))
                  )
                (setq found-incompatibility t)
                (push cause filtered-causes)
            )
        )
        (setq compatible-cause-exists (or compatible-cause-exists (not found-incompatibility)))
    )
    (when (null filtered-causes)
        (return-from all-facts-could-persist-through t)
    )
;    (when (cdr filtered-causes)
;        (format t "~%Checking fact ~A with ~A incompatible causes." fact (length filtered-causes))
;    )
;     (and compatible-cause-exists (all-may-not-occur filtered-causes old-facts))
    (if (and compatible-cause-exists (all-may-not-occur expgen filtered-causes old-facts (layer-full-mutex-hash (cdadr layers)) t))
        ;then
        (or (when (and needs-compatible-cause use-cache) (set-known-incompatible (car facts) causes t)) t)
        ;else
        (and (when use-cache (set-known-incompatible (car facts) causes 'f)) nil)
    )

)



;; Returns t if in some world, no occurrence in causes occurs.
(defun all-may-not-occur (expgen causes old-facts mutex-hash &optional (use-cache t))
    (when (null causes) (error "Must call with one or more causes."))
    ;;When use-cache is nil, the set of possible worlds is a subset, so if in 
    ;;any world of the larger set one or more events must occur, the same is 
    ;;true in the smaller set.
    (dolist (false-list *incompatibility-false-cache*)
        (when (subsetp false-list causes)
;            (break "Using false cache.")
            (return-from all-may-not-occur nil)
        )
    )
    (when (> (length causes) 20)
        (return-from all-may-not-occur t)
    )
    (when (not use-cache)
        (return-from all-may-not-occur
            (all-may-not-occur-uncached expgen causes old-facts mutex-hash)
        )
    )
    (dolist (true-list *incompatibility-true-cache*)
        (when (subsetp causes true-list)
;            (break "Using true cache.")
            (return-from all-may-not-occur t)
        )
    )
    (if (all-may-not-occur-uncached expgen causes old-facts mutex-hash)
        ;then
        (progn
;            (format t "~%Changing true cache.")
            (setq *incompatibility-true-cache*
                (cons causes (remove causes *incompatibility-true-cache* :test #'supersetp))
            )
            t
        )
        ;else
        (progn
;            (format t "~%Changing false cache.")
            (setq *incompatibility-false-cache*
                (cons causes (remove causes *incompatibility-false-cache* :test #'subsetp))
            )
            nil
        )
    )
)

(defun supersetp (set1 set2)
    (subsetp set2 set1)
)

;; Returns t if in some world, no occurrence in causes occurs.
(defun all-may-not-occur-uncached (expgen causes old-facts mutex-hash &aux (tried nil))
;    (format t "~%First cause: ~A" (car causes)) 
    (when (null causes) (return-from all-may-not-occur-uncached t))
    (when (not (subsetp (occurrence-pre (car causes)) old-facts :test #'equal-fact))
        (return-from all-may-not-occur-uncached
            (all-may-not-occur-uncached expgen (cdr causes) old-facts mutex-hash)
        )
    )
    (dolist (fact old-facts)
        (when (member fact (occurrence-pre (car causes)) :test #'could-contradict)
            (when (all-may-not-occur-uncached expgen (cdr causes) (remove-possible-contradictions fact old-facts mutex-hash) mutex-hash)
                (return-from all-may-not-occur-uncached t)
            )
            (setq tried t)
        )
        (cond 
            (tried (setq tried nil))
            ((not tried) 
                (dolist (cnd (occurrence-constraints (car causes)))
                    (when (unify (get-last-envmodel expgen) cnd (list fact) nil :assume-fn #'assume-nothing)
                        (when (all-may-not-occur-uncached expgen (cdr causes) (remove-possible-contradictions fact old-facts mutex-hash) mutex-hash)
                            (return-from all-may-not-occur-uncached t)
                        )
                    )
                )
            )
        )
    )
;    (format t "~%No facts left: ~A" (car causes)) 
    nil
)

(defun event-must-occur (expgen cause old-facts)
    (when (member cause *known-necessary-events*)
        (return-from event-must-occur t)
    )
    (when (member cause *known-unnecessary-events*)
        (return-from event-must-occur nil)
    )
    (dolist (fact (occurrence-pre cause))
        (when (member fact old-facts :test #'could-contradict)
            (push cause *known-unnecessary-events*)
            (return-from event-must-occur nil)
        )
    )
    (push cause *known-necessary-events*)
    t
)

(defun remove-possible-contradictions (fact old-facts mutex-hash)
    (setq old-facts (remove fact old-facts :test #'could-contradict))
    (setq old-facts (set-difference old-facts (gethash fact mutex-hash) :test #'equal-fact))
    old-facts
)
    

;; Returns t if no occurrence in causes leaves both facts true. 
(defun no-consistent-cause-exists (expgen fact1 fact2 causes old-fact-occs layers 
                                   &key (assume-fn #'assume-closed-world))
    (dolist (cause causes)
        (when (and (cause-is-compatible expgen fact1 cause old-fact-occs layers :assume-fn assume-fn)
                   (cause-is-compatible expgen fact2 cause old-fact-occs layers :assume-fn assume-fn))
            (return-from no-consistent-cause-exists nil)
        )
    )
    t
)

(defun cause-is-compatible (expgen fact cause old-fact-occs layers &key (assume-fn #'default-closed-world-assume-fn))
    (dolist (pre (occurrence-pre cause))
        (when (facts-are-mutex fact pre old-fact-occs layers)
            (return-from cause-is-compatible nil)
        )
    )
    (dolist (constraint (occurrence-constraints cause))
        (when (fact-is-mutex-with-constraint expgen fact constraint layers)
            (return-from cause-is-compatible nil)
        )
    )
    (dolist (post (occurrence-post cause))
        (when (and (same-map fact post) 
                   (null (unify expgen (fact-to-cnd fact) (list post) nil :assume-fn assume-fn)))
            (return-from cause-is-compatible nil)
        )
    )
    t
)

;; Populates a hashtable recording the occurrence mutexes for a layer
(defun find-occurrence-mutexes (expgen occs cur-layer layers fact-occs &aux occ1 remaining-occs)
    (setf (layer-occurrence-mutexes cur-layer) (make-hash-table :test #'eq))
    (setq remaining-occs occs)
    (loop while remaining-occs do
        (setq occ1 (car remaining-occs))
        (setq remaining-occs (cdr remaining-occs))
        
        (dolist (occ2 remaining-occs)
            (when (decide-occ-mutex expgen occ1 occ2 cur-layer layers fact-occs)
                (push occ2 (gethash occ1 (layer-occurrence-mutexes cur-layer)))
                (push occ1 (gethash occ2 (layer-occurrence-mutexes cur-layer)))
            )
        )
    )
)

(defun occurrences-are-mutex (occ1 occ2 layer)
    (and (not (eq occ1 occ2))
         (or (occurrence-is-action occ1) ;; occurrence-is-action occ2 iff occurrence-is-action occ1
             (member occ2 (gethash occ1 (layer-occurrence-mutexes layer)))
         )
    )
)


;; Checks two occurrences from the same layer to determine whether they should
;; be mutually exclusive. If so, returns t and adds mutexes to layer. Otherwise,
;; returns nil. This requires that all mutexes for all prior layers be 
;; completely assigned in advance.
(defun decide-occ-mutex (expgen occ1 occ2 cur-layer layers fact-occs)
    (declare (ignore cur-layer))
    (dolist (pre1 (occurrence-pre occ1))
        (dolist (pre2 (occurrence-pre occ2))
            (when (facts-are-mutex pre1 pre2 fact-occs layers)
                (return-from decide-occ-mutex t)
            )
        )
        (dolist (con2 (occurrence-constraints occ2))
            (when (fact-is-mutex-with-constraint expgen pre1 con2 layers)
                (return-from decide-occ-mutex t)
            )
        )
    )
    (dolist (con1 (occurrence-constraints occ1))
        (dolist (pre2 (occurrence-pre occ2))
            (when (fact-is-mutex-with-constraint expgen pre2 con1 layers)
                (return-from decide-occ-mutex t)
            )
        )
    )
    nil
)


(defun facts-are-mutex (fact1 fact2 fact-occs layers)
    (declare (ignore fact-occs))
    (when (contradicts fact1 fact2)
        (return-from facts-are-mutex t)
    )
    (let ((table (layer-full-mutex-hash (cdar layers))))
        (when (null table)
            (when (null (cdr layers))
                (format t "~%Warning: No prior layer in facts-are-mutex.")
                (return-from facts-are-mutex nil)
            )
            (setq table (layer-full-mutex-hash (cdadr layers)))
        )
        (test-mutex fact1 fact2 table)
    )
)

(defun test-mutex (fact1 fact2 mutex-table)
    (member fact2 (gethash fact1 mutex-table) :test #'equal-fact)
)


(defun fact-is-mutex-with-constraint (expgen fact constraint layers)
    (declare (ignore layers))
    (when (unify expgen fact constraint nil :assume-fn #'assume-nothing)
        (return-from fact-is-mutex-with-constraint t)
    )
)

(defun add-occ-to-layer (occ layer point-layer-map fact-occs 
                         &aux prior-occ prior-layer back-links fact-link)
    (setq back-links nil)
    (dolist (fact (occurrence-pre occ))
        (setq prior-occ (cdr (assoc fact fact-occs :test #'equal-fact)))
        (cond
            ((eq prior-occ :original)
                (push :original back-links)
            )
            ((null prior-occ)
;                (break "Assuming.")
                (push (list fact *assumption-point*) back-links)
                (setq prior-layer (cdr (assoc *assumption-point* point-layer-map)))
                (push (list fact occ) (layer-fact-forward-links prior-layer))
                (push (cons fact *assumption-occ*) (cdr fact-occs))
            )
            (t
                (push (list fact (occurrence-point prior-occ)) back-links)
                (setq prior-layer (cdr (assoc (occurrence-point prior-occ) point-layer-map)))
                (push occ (cdr (assoc fact (layer-fact-forward-links prior-layer) :test #'equal-fact)))
            )
        )
    )
    (push (cons occ (reverse back-links)) (layer-occurrence-back-links layer))
    (dolist (fact (occurrence-post occ))
        (setq fact-link (assoc fact (layer-fact-back-links layer) :test #'equal-fact))
        (when (null fact-link)
            (setq fact-link (list fact))
            (push fact-link (layer-fact-back-links layer))
        )
        (push occ (cdr fact-link))
        (setq fact-link (assoc fact (layer-fact-forward-links layer) :test #'equal-fact))
        (when (null fact-link)
            (setq fact-link (list fact))
            (push fact-link (layer-fact-forward-links layer))
        )
    )
)

(defun eliminate-event (expgen contra-occ layers &aux back-links ret-contras forward-links)
    (setq ret-contras nil)
    (setq back-links (remove-occ-back-links contra-occ layers))
    
;    (break "Testing eliminate-event.")
    (dolist (link back-links)
        (when (and (listp link) (cdr link))
            (setq forward-links (get-fact-forward-links (first link) (second link) layers))
            (when forward-links ;;Because the forward-link might have been removed first
                (setf (cdr forward-links) (remove contra-occ (cdr forward-links)))
            )
        )
    )
    
    (setq back-links (remove :original back-links))
    (when (null back-links)
        (error "Can't remove event; it's based on original facts.")
    )
    (when (null (cdr back-links))
        (push-all
            (eliminate-fact 
                expgen
                (first (first back-links)) 
                (second (first back-links)) 
                layers
            )
            ret-contras
        )
        (push (first back-links) ret-contras)
    )
    
;    (break "Before back-links removed.")
    (dolist (fact (occurrence-post contra-occ))
        (setq back-links (get-fact-back-links fact (occurrence-point contra-occ) layers))
        (when back-links ;;because the fact might have already been removed
            (setf (cdr back-links) (remove contra-occ (cdr back-links)))
            (when (null (cdr back-links))
                (push-all
                    (eliminate-fact expgen fact (occurrence-point contra-occ) layers)
                    ret-contras
                )
            )
        )
    )
    ret-contras
)

(defun eliminate-fact (expgen contra-fact pt layers 
                        &aux forward-links back-links ret-contras old-pt)
    (setq ret-contras nil)
    
    (when (null pt)
        (error "All facts must be associated with a layer.")
    )
    
    (dolist (occ (remove-fact-back-links contra-fact pt layers))
        (push-all (eliminate-event expgen occ layers) ret-contras)
    )
;    (break "After removal.")
    
    (setq forward-links nil)
    (setq old-pt (point-prior expgen pt))
    ;; Iterate through prior layers to find a prior event that caused the same
    ;; fact
    (loop while (and old-pt (null forward-links)) do
        (setq forward-links (get-fact-forward-links contra-fact old-pt layers))
        (when (null forward-links)
            (setq old-pt (point-prior expgen old-pt))
        )
    )
    
    (when (null forward-links)
        ;; The fact was never caused before, so eliminate all events dependent
        ;; on it
        (dolist (occ (remove-fact-forward-links contra-fact pt layers))
            (push-all (eliminate-event expgen occ layers) ret-contras)
        )
    )
    
    (when forward-links
        ;; The fact was already caused, so base all events on the earlier layer
        (dolist (occ (remove-fact-forward-links contra-fact pt layers))
            (push occ (cdr forward-links))
            (setq back-links (get-occ-back-links occ layers))
            (setf (cdr (assoc contra-fact (cdr back-links) :test #'equal-fact)) (list old-pt))

            (let ((cause-links (remove :original (cdr back-links))))
                (setq cause-links 
                    (remove (point-prior expgen (occurrence-point occ)) cause-links
                        :key #'second
                        :test-not #'eq
                    )
                )
                (when (null cause-links)
                    ;; If no causes are from the prior occurrence point, this 
                    ;; event should have happened earlier.
                    
                    ;; Before we remove it, make sure that it doesn't remove an
                    ;; earlier fact recursively.
                    (push nil (cdr back-links)) 
                    
                    (push-all (eliminate-event expgen occ layers) ret-contras)
                )
            )
        )
    )
    
    ret-contras
)

(defun get-fact-forward-links (fact pt layers &aux layer)
    (setq layer (cdr (assoc pt layers)))
    (assoc fact (layer-fact-forward-links layer) :test #'equal-fact)
)

(defun get-fact-back-links (fact pt layers &aux layer)
    (setq layer (cdr (assoc pt layers)))
    (assoc fact (layer-fact-back-links layer) :test #'equal-fact)
)

(defun get-occ-back-links (occ layers &aux layer)
    (setq layer (cdr (assoc (occurrence-point occ) layers)))
    (assoc occ (layer-occurrence-back-links layer) :test #'occurrence-equal)
)

(defun remove-fact-forward-links (fact pt layers &aux layer links)
    (setq links (get-fact-forward-links fact pt layers))
    (setq layer (cdr (assoc pt layers)))
    (setf (layer-fact-forward-links layer) 
        (remove links (layer-fact-forward-links layer))
    )
    (cdr links)
)

(defun remove-fact-back-links (fact pt layers &aux layer links)
    (setq links (get-fact-back-links fact pt layers))
    (setq layer (cdr (assoc pt layers)))
    (setf (layer-fact-back-links layer) 
        (remove links (layer-fact-back-links layer))
    )
    (cdr links)
)

(defun remove-occ-back-links (occ layers &aux layer links)
    (setq links (get-occ-back-links occ layers))
    (setq layer (cdr (assoc (occurrence-point occ) layers)))
    (setf (layer-occurrence-back-links layer) 
        (remove links (layer-occurrence-back-links layer))
    )
    (cdr links)
)

(defun add-assumption (fact layers &aux prior-layer)
    (when (null (get-fact-forward-links fact *assumption-point* layers))
        (setq prior-layer (cdr (assoc *assumption-point* layers)))
        (push (list fact) (layer-fact-forward-links prior-layer))
    )
)

