 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: April 8, 2011
 ;;;; Project Name: Explanation
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file tests explanation functions

 
(when (not (find-package "ARTUE"))
    (load "utils/loader")
    (make-package "ARTUE" :use '("COMMON-LISP" "UTILS"))
)

(in-package :artue)

(load "full-explainer/new-event-explain")
(load "full-explainer/new-transition-manager")
(load "full-explainer/event-explain-model")
(load "full-explainer/event-unify")
(load "full-explainer/event-domain-model")
(load "full-explainer/event-enumeration")
(load "rover/RoversEventsCompassFunc.pddl")
 
(defun test-contradicts (fact1 fact2 ans)
    (format t "~%Contradict test: ~A ~A ~A"
        fact1
        fact2
        (if (contradicts fact1 fact2) "Opposite" "Safe")
    )
    (when (not (eq ans (contradicts fact1 fact2)))
        (error "Contradiction test failed.")
    )
)

(defun test-contradict-cases ()
    (test-contradicts
        (make-fact
            :type 'rover-moves
            :args '(rover1 north waypoint12)
            :value t
        )
        (make-fact
            :type 'rover-moves
            :args '(rover1 north waypoint12)
            :value t
        )
        nil
    )
    (test-contradicts
        (make-fact
            :type 'rover-moves
            :args '(rover1 north waypoint12)
            :value t
        )
        (make-fact
            :type 'rover-moves
            :args '(rover1 north waypoint13)
            :value t
        )
        nil
    )
    (test-contradicts
        (make-fact
            :type 'rover-moves
            :args '(rover1 north waypoint12)
            :value t
        )
        (make-fact
            :type 'rover-moves
            :args '(rover1 north waypoint12)
            :value nil
        )
        t
    )
    (test-contradicts
        (make-fact
            :type 'rover-moves
            :args '(rover1 north waypoint12)
            :value t
        )
        (make-fact
            :type 'rover-moves
            :args '(rover1 north waypoint13)
            :value nil
        )
        nil
    )
)

(defun test-find-contradictory-events (ev co-evs fn exp-events &aux ret-events)
    (setq ret-events (find-contradictory-events ev co-evs fn))
    (when (set-exclusive-or exp-events ret-events)
        (error "The set of contradictory events is incorrect!")
    )
)

(setq pt (make-point))
(setq f12t
    (make-fact
        :type 'rover-moves
        :args '(rover1 north waypoint12)
        :value t
    )
)
(setq f13t
    (make-fact
        :type 'rover-moves
        :args '(rover1 north waypoint13)
        :value t
    )
)
(setq f14t
    (make-fact
        :type 'rover-moves
        :args '(rover1 north waypoint14)
        :value t
    )
)
(setq f12f
    (make-fact
        :type 'rover-moves
        :args '(rover1 north waypoint12)
        :value nil
    )
)
(setq f13f
    (make-fact
        :type 'rover-moves
        :args '(rover1 north waypoint13)
        :value nil
    )
)
(setq f14f
    (make-fact
        :type 'rover-moves
        :args '(rover1 north waypoint14)
        :value nil
    )
)
(setq fother
    (make-fact
        :type 'storm-blows
        :args nil
        :value t
    )
)
(setq fotherf
    (make-fact
        :type 'storm-blows
        :args nil
        :value nil
    )
)

(setq ev1 
    (make-occurrence
        :signature '(ev1)
        :pre (list f12f)
        :post nil
        :is-event t
        :point pt
    )
)
(setq ev2 
    (make-occurrence
        :signature '(ev2)
        :pre (list f12t)
        :post nil
        :is-event t
        :point pt
    )
)
(setq ev3 
    (make-occurrence
        :signature '(ev3)
        :pre (list f12f f13t)
        :post (list fother)
        :is-event t
        :point pt
    )
)
(setq ev4 
    (make-occurrence
        :signature '(ev4)
        :pre (list f13f fother)
        :post (list f12f)
        :is-event t
        :point pt
    )
)
(setq ev5
    (make-occurrence
        :signature '(ev5)
        :pre (list f12t)
        :post (list fother f12t)
        :is-event t
        :point pt
    )
)
(setq ev6
    (make-occurrence
        :signature '(ev6)
        :pre (list fother)
        :post (list f13t)
        :is-event t
        :point pt
    )
)
(setq ev7
    (make-occurrence
        :signature '(ev7)
        :pre (list f12f)
        :post (list f12t)
        :is-event t
        :point pt
    )
)


(defun test-contradictory-event-cases (&aux pt)
    (test-find-contradictory-events ev1 (list ev2 ev3 ev4 ev5) #'occurrence-post 
        nil
    )
    (test-find-contradictory-events ev1 (list ev2 ev3 ev4 ev5) #'occurrence-pre 
        (list ev2 ev5)
    )
    (test-find-contradictory-events ev2 (list ev1 ev3 ev4 ev5) #'occurrence-pre
        (list ev1 ev3)
    )
    (test-find-contradictory-events ev3 (list ev2 ev1 ev4 ev5) #'occurrence-pre
        (list ev2 ev4 ev5)
    )
    (test-find-contradictory-events ev3 (list ev2 ev1 ev4 ev5) #'occurrence-post
        nil
    )
    (test-find-contradictory-events ev4 (list ev2 ev3 ev1 ev5) #'occurrence-post
        (list ev5)
    )
)

(defun test-same-explanation (correct-ex found-ex)
    (when (set-exclusive-or (explanation-event-removals correct-ex)
                            (explanation-event-removals found-ex)
                            :test #'occurrence-equal
          )
        (format t "~%Expected event removals: ~A~%Found event removals: ~A"
            (explanation-event-removals correct-ex)
            (explanation-event-removals found-ex)
        )
        (error "Event removals did not match.")
    )
    (when (set-exclusive-or (explanation-new-events correct-ex)
                            (explanation-new-events found-ex)
                            :test #'occurrence-equal
          )
        (format t "~%Expected new events: ~A~%Found new events: ~A"
            (explanation-new-events correct-ex)
            (explanation-new-events found-ex)
        )
        (error "New events did not match.")
    )
    (when (set-exclusive-or (explanation-prior-events correct-ex)
                            (explanation-prior-events found-ex)
                            :test #'occurrence-equal
          )
        (format t "~%Expected prior events: ~A~%Found prior events: ~A"
            (explanation-prior-events correct-ex)
            (explanation-prior-events found-ex)
        )
        (error "Prior events did not match.")
    )
)

(defun test-fix-contradictions (ex result &aux ret)
    (setq ret (fix-contradictions ex))
    (when (null ret)
        (when (not (null result))
            (error "Fixing should not have failed.")
        )
        (return-from test-fix-contradictions nil)
    )
    (when (null result)
        (error "Fixing should have failed.")
    )
    (test-same-explanation result ret)
)
    
(defun test-fix-contradiction-cases ()
    (test-fix-contradictions 
        (make-explanation
            :prior-events nil
            :new-events nil
            :event-removals nil
        )
        (make-explanation
            :prior-events nil
            :new-events nil
            :event-removals nil
        )
    )
    (test-fix-contradictions 
        (make-explanation
            :prior-events (list ev1 ev3)
            :new-events (list ev2)
            :event-removals nil
        )
        (make-explanation
            :prior-events (list ev1 ev3)
            :new-events (list ev2)
            :event-removals (list ev1 ev3)
        )
    )
    (test-fix-contradictions 
        (make-explanation
            :prior-events (list ev1)
            :new-events (list ev2 ev3)
            :event-removals nil
        )
        nil
    )
    (test-fix-contradictions 
        (make-explanation
            :prior-events (list ev2)
            :new-events (list ev4)
            :event-removals nil
        )
        (make-explanation
            :prior-events (list ev2)
            :new-events (list ev4)
            :event-removals nil
        )
    )
    (test-fix-contradictions 
        (make-explanation
            :prior-events (list ev2 ev6)
            :new-events (list ev5)
            :event-removals nil
        )
        (make-explanation
            :prior-events (list ev2 ev6)
            :new-events (list ev5)
            :event-removals nil
        )
    )
    (test-fix-contradictions 
        (make-explanation
            :prior-events (list ev2)
            :new-events (list ev1 ev3)
            :event-removals nil
        )
        (make-explanation
            :prior-events (list ev2)
            :new-events (list ev1 ev3)
            :event-removals (list ev2)
        )
    )
    (test-fix-contradictions 
        (make-explanation
            :prior-events nil
            :new-events (list ev4 ev5)
            :event-removals nil
        )
        nil
    )
    (test-fix-contradictions 
        (make-explanation
            :prior-events (list ev1 ev4)
            :new-events (list ev2 ev6)
            :event-removals nil
        )
        (make-explanation
            :prior-events (list ev1 ev4)
            :new-events (list ev2 ev6)
            :event-removals (list ev1)
        )
    )
)

(defun test-advance-facts (occs past-facts fact-occs exp-facts exp-fact-occs)
    (multiple-value-bind (ret-facts ret-fact-occs) (advance-facts occs past-facts fact-occs)
        (when (set-difference ret-facts exp-facts :test #'equal)
            (format t "Was not expecting these facts returned:")
            (print-one-per-line 
                (set-difference ret-facts exp-facts :test #'equal)
            )
            (error "Advance-facts returned unexpected facts.")
        )
        (when (set-difference exp-facts ret-facts :test #'equal)
            (format t "Expected the following facts returned:")
            (print-one-per-line 
                (set-difference exp-facts ret-facts :test #'equal)
            )
            (error "Advance-facts did not return expected facts.")
        )
        (let ((used-facts nil))
            (dolist (fact-occ exp-fact-occs)
                (let* ((fact (first fact-occ)) (exp-value (second fact-occ))
                       (ret-fact-occ (assoc fact ret-fact-occs :test #'same-map))
                       (ret-value (second ret-fact-occ))
                      )
                    (when (not (member fact used-facts :test #'same-map))
                        (push fact used-facts)
                        (when (not (equal exp-value ret-value))
                            (error "For value of ~A, expected ~A, found ~A." 
                                fact exp-value ret-value
                            )
                        )
                    )
                )
            )
        )
    )
)

(defun test-advance-cases ()
    (test-advance-facts (list ev1) nil nil (list f12f) (list (list f12f ev1)))
    (test-advance-facts 
        (list ev3) 
        nil 
        nil 
        (list fother f12f f13t) 
        (list (list fother ev3) (list f12f ev3) (list f13t ev3))
    )
    (test-advance-facts 
        (list ev7) 
        nil 
        nil 
        (list f12t) 
        (list (list f12t ev7))
    )
    (test-advance-facts 
        (list ev7) 
        (list f13t)
        (list (list f13t ev3))
        (list f12t f13t) 
        (list (list f12t ev7) (list f13t ev3))
    )
    (test-advance-facts 
        (list ev6) 
        (list f13t)
        (list (list f13t ev3))
        (list fother f13t) 
        (list (list fother ev6) (list f13t ev6))
    )
    (test-advance-facts 
        (list ev6) 
        (list f13f)
        (list (list f13f ev3))
        (list fother f13t) 
        (list (list fother ev6) (list f13t ev6))
    )
    (test-advance-facts 
        (list ev6 ev7) 
        (list f13t)
        (list (list f13t ev3))
        (list fother f13t f12t) 
        (list (list fother ev6) (list f13t ev6) (list f12t ev7))
    )
    (test-advance-facts 
        (list ev5 ev6 ev7) 
        (list f13t f12f)
        (list (list f13t ev3) (list f12f ev1))
        (list fother f13t f12t) 
        (list (list fother ev5) (list f13t ev6) (list f12t ev7))
    )
)

(defun test-make-inconsistencies (occs past-facts fact-occs exp-inc-set &aux ret-inc-set)
    (setq ret-inc-set (make-inconsistencies occs past-facts fact-occs))
    (when (set-difference exp-inc-set ret-inc-set :test #'equal-inconsistency)
        (format t "~%The following inconsistencies were expected but not returned:")
        (print-one-per-line (set-difference exp-inc-set ret-inc-set :test #'equal))
        (error "Expected inconsistencies not returned.")
    )
    (when (set-difference ret-inc-set exp-inc-set :test #'equal-inconsistency)
        (format t "~%The following inconsistencies were not expected:")
        (print-one-per-line (set-difference ret-inc-set exp-inc-set :test #'equal))
        (error "Returned inconsistencies not expected.")
    )
)

(defun test-make-inconsistencies-cases ()
    (test-make-inconsistencies
        (list ev1)
        (list f12f)
        nil
        nil
    )
    (test-make-inconsistencies
        (list ev1)
        (list f12t)
        (list (list f12t ev7))
        (list (make-inconsistency :cnd (fact-to-cnd f12f) :prior-occ ev7 :next-occ ev1))
    )
    (test-make-inconsistencies
        (list ev7)
        (list f12t)
        (list (list f12t ev7))
        (list (make-inconsistency :cnd (fact-to-cnd f12f) :prior-occ ev7 :next-occ ev7))
    )
    (test-make-inconsistencies
        (list ev1)
        nil
        nil
        nil
;        (list (make-inconsistency :cnd (fact-to-cnd f12f) :prior-occ nil :next-occ ev1))
    )
    (test-make-inconsistencies
        (list ev1 ev3)
        (list f12t)
        (list (list f12t ev7))
        (list (make-inconsistency :cnd (fact-to-cnd f12f) :prior-occ ev7 :next-occ ev1)
              (make-inconsistency :cnd (fact-to-cnd f13t) :prior-occ nil :next-occ ev3)
              (make-inconsistency :cnd (fact-to-cnd f12f) :prior-occ ev7 :next-occ ev3)
        )
    )
)

(defun test-find-following-intervals (ex pt exp-intervals)
    (setq ret-intervals (find-following-intervals ex pt))
    (when (not (equal ret-intervals exp-intervals))
        (format t "~%Unexpected intervals found following ~A:" pt)
        (print-one-per-line ret-intervals)
        (error "~%Unexpected intervals found following ~A." pt)
    )
)

(defvar *point-list* nil)

(setq *point0* (make-point))
(setq last-point *point0*)
(setq *point-list* nil)
(loop for i from 1 to 10 do 
    (setq new-point (make-point))
    (link-points last-point new-point)
    (push new-point *point-list*)
    (setq last-point new-point)
)
(setq *point-list* (reverse *point-list*))
(push *point0* *point-list*)

(setq o0 
    (make-occurrence 
        :signature '(o0)
        :pre (list f12t)
        :post nil
        :is-observation t
        :point *point0*
    )
)
(setq a1 
    (make-occurrence 
        :signature '(navigate east waypoint12 waypoint13)
        :pre (list f12t)
        :post nil
        :is-action t
        :point (nth 1 *point-list*)
    )
)
(setq e1 
    (make-occurrence 
        :signature '(rover-moves east waypoint12 waypoint13)
        :pre (list f12t)
        :post (list f12f f13t)
        :is-event t
        :point (nth 2 *point-list*)
    )
)
(setq o1 
    (make-occurrence 
        :signature '(o1)
        :pre (list f12f f13t)
        :post nil
        :is-observation t
        :point (nth 3 *point-list*)
    )
)
(setq a2
    (make-occurrence 
        :signature '(navigate east waypoint13 waypoint14)
        :pre (list f13t)
        :post nil
        :is-action t
        :point (nth 4 *point-list*)
    )
)
(setq e2 
    (make-occurrence 
        :signature '(rover-moves east waypoint13 waypoint14)
        :pre (list f13t)
        :post (list f13f f14t)
        :is-event t
        :point (nth 5 *point-list*)
    )
)
(setq o2 
    (make-occurrence 
        :signature '(o2)
        :pre (list f12f f13t)
        :post nil
        :is-observation t
        :point (nth 6 *point-list*)
    )
)
(setq ival1 (make-interval :range-start (nth 5 *point-list*) :range-end (nth 6 *point-list*)))
(setq e3 
    (make-occurrence 
        :signature '(storm-starts)
        :pre (list f14t fotherf)
        :post (list f14f f13t fother)
        :is-event t
        :interval ival1
    )
)
(setq ival2 (make-interval :range-start ival1 :range-end (nth 6 *point-list*)))
(setq e4 
    (make-occurrence 
        :signature '(storm-ends)
        :pre (list fother)
        :post (list fotherf)
        :is-event t
        :interval ival2
    )
)
(setq ival3 (make-interval :range-start ival1 :range-end (nth 6 *point-list*)))
(setq ival4 (make-interval :range-start ival2 :range-end (nth 6 *point-list*)))
(setq ival5 (make-interval :range-start ival3 :range-end (nth 6 *point-list*)))

(setq e5 
    (make-occurrence 
        :signature '(initial-value (storm-blows))
        :pre (list fotherf)
        :post nil
        :is-event t
        :point *point0*
    )
)

(setq e6 
    (make-occurrence 
        :signature '(storm-starts)
        :pre (list f12t fotherf)
        :post (list f14f f12t fother)
        :is-event t
        :interval ival1
    )
)

(setq *execution-history* (list o0 a1 o1 a2 o2))  

(setq ex1
    (make-explanation
        :prior-events (list e1 e2)
        :intervals    (list )
    )
)

(setq ex2
    (make-explanation
        :prior-events (list e1 e2 e3)
        :intervals    (list ival1)
    )
)

(setq ex2a
    (make-explanation
        :prior-events (list e1 e2 e4)
        :intervals    (list ival2)
    )
)

(setq ex3
    (make-explanation
        :prior-events (list e1 e2 e3 e4)
        :intervals    (list ival1 ival2)
    )
)

(setq ex4
    (make-explanation
        :prior-events (list e1 e2 e3 e4)
        :intervals    (list ival5 ival4 ival3 ival2 ival1)
    )
)

(setq ex5
    (make-explanation
        :prior-events (list e1 e2)
        :event-removals    (list e2)
    )
)

(setq ex6
    (make-explanation
        :prior-events (list e1 e2 e3 e4 e5)
        :intervals    (list ival1 ival2)
    )
)

(defun test-find-following-intervals-cases ()
    (test-find-following-intervals ex1 *point0* nil)
    (test-find-following-intervals ex2 *point0* nil)
    (test-find-following-intervals ex1 (nth 4 *point-list*) nil)
    (test-find-following-intervals ex2 (nth 4 *point-list*) nil)
    (test-find-following-intervals ex1 (nth 5 *point-list*) nil)
    (test-find-following-intervals ex2 (nth 5 *point-list*) (list ival1))
    (test-find-following-intervals ex3 (nth 5 *point-list*) (list ival1 ival2))
    (test-find-following-intervals ex1 (nth 6 *point-list*) nil)
    (test-find-following-intervals ex2 (nth 6 *point-list*) nil)
    (test-find-following-intervals ex4 (nth 5 *point-list*) (list ival1 ival3 ival2 ival5 ival4))
)

(defun test-find-inconsistencies (ex exp-inc-set &aux ret-inc-set)
    (setq ret-inc-set (find-inconsistencies ex))
    (let 
      ((unex (set-difference ret-inc-set exp-inc-set :test #'equal-inconsistency))
       (unfound (set-difference exp-inc-set ret-inc-set :test #'equal-inconsistency)))
        (when unex
            (format t "~%Found unexpected inconsistencies: ")
            (print-one-per-line unex)
        )
        (when unfound
            (format t "~%Expected inconsistencies not found: ")
            (print-one-per-line unfound)
        )
        (when (or unex unfound)
            (error "Find inconsistencies returned bad results.")
        )
    )
)

(defun test-find-inconsistencies-cases ()
    (setq *execution-history* (list o0 a1 o1 a2 o2))  
    (test-find-inconsistencies ex1 
        (list
            (make-inconsistency 
                :cnd (fact-to-cnd f13t)
                :prior-occ e2
                :next-occ o2
            )
        )
    )
    (test-find-inconsistencies ex2 
        nil
    )
    ; (test-find-inconsistencies ex2a
        ; (list
            ; (make-inconsistency 
                ; :cnd (fact-to-cnd f13t)
                ; :prior-occ e2
                ; :next-occ o2
            ; )
            ; (make-inconsistency 
                ; :cnd (fact-to-cnd fother)
                ; :fact fother
                ; :prior-occ nil
                ; :next-occ e4
            ; )
        ; )
    ; )
    (test-find-inconsistencies ex3 
        nil
    )
    (test-find-inconsistencies ex4 
        nil
    )
    (test-find-inconsistencies ex5 nil)
    (test-find-inconsistencies ex6 nil)

    (test-find-inconsistencies
        (make-explanation 
            :new-events 
                (list 
                    e1 
                    (make-occurrence 
                        :signature '(no-storm)
                        :pre (list fotherf)
                        :post nil
                        :is-event t
                        :point (nth 5 *point-list*)
                    )
                )
        )
        nil
    )
    
)

(defun test-bind-args (cnd fact binding-list exp-binding-list)
    (test-set-match 
        (bind-args cnd fact binding-list)
        exp-binding-list
        "bindings"
        (format nil "(bind-args ~A ~A ~A)" cnd fact binding-list)
    )
)

(defun test-set-match (ret-set exp-set items-type call-string &key (test #'equal))
    (when (or (not (listp ret-set)) (not (listp exp-set)))
        (when (not (equal ret-set exp-set))
            (error "Expected ~A to return ~A, found ~A." call-string exp-set ret-set)
        )
        (return-from test-set-match nil)
    )
    (let ((unexp (set-difference ret-set exp-set :test test))
          (unfound (set-difference exp-set ret-set :test test))
         )
        (when (or unexp unfound)
            (format t "~%Erroneous return from ~A." call-string)
        )
        (when unexp
            (format t "~%Found ~A that were unexpected: " items-type)
            (print-one-per-line unexp)
        )
        (when unfound
            (format t "~%Expected ~A that were not found: " items-type)
            (print-one-per-line unfound)
        )
        (when (or unexp unfound)
            (error "Erroneous return from ~A." call-string)
        )
    )
)

(defun test-bind-args-cases ()
    (test-bind-args
        (make-cnd 
            :type 'rover-moves
            :args '(?rov ?src ?dst ?dir)
            :value t
        )
        (make-fact
            :type 'rover-moves
            :args '(rover1 waypoint12 waypoint13 east)
            :value t
        )
        nil
        '((?rov . rover1) (?src . waypoint12) (?dst . waypoint13) (?dir . east))
    )
    (test-bind-args
        (make-cnd 
            :type 'rover-moves
            :args '(?rov ?src waypoint13 ?dir)
            :value t
        )
        (make-fact
            :type 'rover-moves
            :args '(rover1 waypoint12 waypoint13 east)
            :value t
        )
        nil
        '((?rov . rover1) (?src . waypoint12) (?dir . east))
    )
    (test-bind-args
        (make-cnd 
            :type 'rover-moves
            :args '(?rov ?src waypoint13 ?dir)
            :value t
        )
        (make-fact
            :type 'rover-moves
            :args '(rover1 waypoint12 waypoint13 east)
            :value t
        )
        '((?x . 1))
        '((?x . 1) (?rov . rover1) (?src . waypoint12) (?dir . east))
    )
    (test-bind-args
        (make-cnd 
            :type 'rover-moves
            :args '(?rov waypoint11 waypoint13 ?dir)
            :value t
        )
        (make-fact
            :type 'rover-moves
            :args '(rover1 waypoint12 waypoint13 east)
            :value t
        )
        nil
        'fail
    )
    (test-bind-args
        (make-cnd 
            :type 'rover-moves
            :args '(?rov ?src waypoint13 ?dir)
            :value t
        )
        (make-fact
            :type 'rover-moves
            :args '(rover1 waypoint12 waypoint13 east)
            :value t
        )
        '((?src . waypoint12))
        '((?rov . rover1) (?src . waypoint12) (?dir . east))
    )
    (test-bind-args
        (make-cnd 
            :type 'rover-moves
            :args '(?rov ?src waypoint13 ?dir)
            :value t
        )
        (make-fact
            :type 'rover-moves
            :args '(rover1 waypoint12 waypoint13 east)
            :value t
        )
        '((?src . waypoint11))
        'fail
    )
)

(defun test-bind-value (cnd fact binding-list exp-binding-list)
    (test-set-match 
        (bind-value cnd fact binding-list)
        exp-binding-list
        "bindings"
        (format nil "(bind-value ~A ~A ~A)" cnd fact binding-list)
    )
)

(defun test-bind-value-cases ()
    (test-bind-value
        (make-cnd 
            :type 'rover-moves
            :args '(?rov ?src ?dst ?dir)
            :value t
        )
        (make-fact
            :type 'rover-moves
            :args '(rover1 waypoint12 waypoint13 east)
            :value t
        )
        '((?rov . rover1) (?src . waypoint12) (?dst . waypoint13) (?dir . east))
        '((?rov . rover1) (?src . waypoint12) (?dst . waypoint13) (?dir . east))
    )
    (test-bind-value
        (make-cnd 
            :type 'rover-moves
            :args '(?rov ?src ?dst ?dir)
            :value t
        )
        (make-fact
            :type 'rover-moves
            :args '(rover1 waypoint12 waypoint13 east)
            :value t
        )
        nil
        nil
    )
    (test-bind-value
        (make-cnd 
            :type 'rover-moves
            :args '(?rov ?src ?dst ?dir)
            :value t
        )
        (make-fact
            :type 'rover-moves
            :args '(rover1 waypoint12 waypoint13 east)
            :value nil
        )
        '((?rov . rover1) (?src . waypoint12) (?dst . waypoint13) (?dir . east))
        'fail
    )
    (test-bind-value
        (make-cnd 
            :type 'rover-facing
            :args '(?rov)
            :value 'east
        )
        (make-fact
            :type 'rover-facing
            :args '(rover1)
            :value 'east
        )
        '((?rov . rover1))
        '((?rov . rover1))
    )
    (test-bind-value
        (make-cnd 
            :type 'rover-facing
            :args '(?rov)
            :value '?dir
        )
        (make-fact
            :type 'rover-facing
            :args '(rover1)
            :value 'east
        )
        '((?rov . rover1))
        '((?rov . rover1) (?dir . east))
    )
    (test-bind-value
        (make-cnd 
            :type 'rover-facing
            :args '(?rov)
            :value '?dir
        )
        (make-fact
            :type 'rover-facing
            :args '(rover1)
            :value 'east
        )
        '((?rov . rover1) (?dir . east))
        '((?rov . rover1) (?dir . east))
    )
    (test-bind-value
        (make-cnd 
            :type 'rover-facing
            :args '(?rov)
            :value '?dir
        )
        (make-fact
            :type 'rover-facing
            :args '(rover1)
            :value 'east
        )
        '((?rov . rover1) (?dir . west))
        'fail
    )
    (test-bind-value
        (make-cnd 
            :inequality #'neq
            :type 'rover-facing
            :args '(?rov)
            :value 'east
        )
        (make-fact
            :type 'rover-facing
            :args '(rover1)
            :value 'west
        )
        '((?rov . rover1))
        '((?rov . rover1))
    )
    (test-bind-value
        (make-cnd 
            :inequality #'neq
            :type 'rover-facing
            :args '(?rov)
            :value 'east
        )
        (make-fact
            :type 'rover-facing
            :args '(rover1)
            :value 'east
        )
        '((?rov . rover1))
        'fail
    )
    (test-bind-value
        (make-cnd 
            :inequality #'>
            :type 'energy
            :args '(rover1)
            :value 80
        )
        (make-fact
            :type 'energy
            :args '(rover1)
            :value 90
        )
        '((?rov . rover1))
        '((?rov . rover1))
    )
    (test-bind-value
        (make-cnd 
            :inequality #'>
            :type 'energy
            :args '(rover1)
            :value 80
        )
        (make-fact
            :type 'energy
            :args '(rover1)
            :value 70
        )
        '((?rov . rover1))
        'fail
    )
    (test-bind-value
        (make-cnd 
            :inequality #'>
            :type 'energy
            :args '(rover1)
            :value '?level
        )
        (make-fact
            :type 'energy
            :args '(rover1)
            :value '90
        )
        '((?rov . rover1) (?level . 80))
        '((?rov . rover1) (?level . 80))
    )
)

(defun test-unify (cnd fact-set binding-list exp-binding-lists &key (assume-fn #'assume-closed-world) &aux ret-binding-lists)
    (setq ret-binding-lists (unify cnd fact-set binding-list :assume-fn assume-fn))
    (when (eq exp-binding-lists 'fail)
        (if (eq ret-binding-lists 'fail)
            ;then
            (return-from test-unify nil)
            ;else
            (error "Expected fail, found bindings ~A." ret-binding-lists)
        )
    )
    (when (eq ret-binding-lists 'fail)
        (error "Expected bindings ~A, found fail." exp-binding-lists)
    )
        
    (when (not (eql (length exp-binding-lists) (length ret-binding-lists)))
        (error "Expected ~A bindings from unify, found ~A."
            (length exp-binding-lists) (length ret-binding-lists)
        )
    )
    (dolist (exp-blist exp-binding-lists)
        (test-set-match 
            (car ret-binding-lists)
            exp-blist
            "bindings"
            (format nil "(unify ~A ~A ~A)" cnd fact-set binding-list)
        )
        (setq ret-binding-lists (cdr ret-binding-lists))
    )
)

(defun test-unify-cases ()
    (test-unify
        (make-cnd 
            :type 'rover-moves
            :args '(?rov ?src ?dst ?dir)
            :value t
        )
        (list
            (make-fact :type 'rover-moves :args '(rover1 waypoint12 waypoint13 east) :value t)
        )
        nil
        '(((?rov . rover1) (?src . waypoint12) (?dst . waypoint13) (?dir . east)))
    )
    (test-unify
        (make-cnd 
            :type 'rover-moves
            :args '(?rov ?src ?dst ?dir)
            :value t
        )
        (list
            (make-fact :type 'rover-moves :args '(rover1 waypoint12 waypoint13 east) :value t)
        )
        '((?rov . rover1) (?src . waypoint12) (?dst . waypoint13) (?dir . east))
        '(((?rov . rover1) (?src . waypoint12) (?dst . waypoint13) (?dir . east)))
    )
    (test-unify
        (make-cnd 
            :type 'rover-moves
            :args '(?rov ?src ?dst ?dir)
            :value t
        )
        (list
            (make-fact :type 'rover-moves :args '(rover1 waypoint12 waypoint13 east) :value t)
            (make-fact :type 'rover-moves :args '(rover1 waypoint11 waypoint13 east) :value t)
        )
        '((?rov . rover1) (?src . waypoint12) (?dst . waypoint13) (?dir . east))
        '(((?rov . rover1) (?src . waypoint12) (?dst . waypoint13) (?dir . east)))
    )
    (test-unify
        (make-cnd 
            :type 'rover-moves
            :args '(?rov ?src ?dst ?dir)
            :value t
        )
        (list
            (make-fact :type 'rover-moves :args '(rover1 waypoint12 waypoint13 east) :value t)
            (make-fact :type 'rover-moves :args '(rover1 waypoint11 waypoint13 east) :value t)
        )
        '((?rov . rover1) (?dst . waypoint13) (?dir . east))
        '(((?rov . rover1) (?src . waypoint11) (?dst . waypoint13) (?dir . east))
          ((?rov . rover1) (?src . waypoint12) (?dst . waypoint13) (?dir . east)))
    )
    (test-unify
        (make-cnd 
            :type 'rover-moves
            :args '(?rov waypoint11 ?dst ?dir)
            :value t
        )
        (list
            (make-fact :type 'rover-moves :args '(rover1 waypoint12 waypoint13 east) :value t)
            (make-fact :type 'rover-moves :args '(rover1 waypoint11 waypoint13 east) :value t)
        )
        nil
        '(((?rov . rover1) (?dst . waypoint13) (?dir . east)))
    )
    (test-unify
        (make-cnd 
            :type 'rover-moves
            :args '(?rov ?src ?dst ?dir)
            :value t
        )
        (list
            (make-fact :type 'rover-moves :args '(rover1 waypoint12 waypoint13 east) :value t)
            (make-fact :type 'rover-moves :args '(rover1 waypoint11 waypoint13 east) :value t)
        )
        '((?src . waypoint12)) 
        '(((?rov . rover1) (?src . waypoint12) (?dst . waypoint13) (?dir . east)))
    )
    (test-unify
        (make-cnd 
            :type 'rover-moves
            :args '(?rov ?src ?dst ?dir)
            :value t
        )
        (list
            (make-fact :type 'rover-moves :args '(rover1 waypoint12 waypoint13 east) :value t)
            (make-fact :type 'rover-moves :args '(rover1 waypoint11 waypoint13 east) :value t)
            (make-fact :type 'rover-moves :args '(rover1 waypoint11 waypoint12 east) :value t)
        )
        '((?dst . waypoint13))
        '(((?rov . rover1) (?src . waypoint11) (?dst . waypoint13) (?dir . east))
          ((?rov . rover1) (?src . waypoint12) (?dst . waypoint13) (?dir . east)))
    )
    (test-unify
        (make-cnd 
            :type 'rover-moves
            :args '(?rov ?src ?dst ?dir)
            :value t
        )
        (list
            (make-fact :type 'rover-moves :args '(rover1 waypoint12 waypoint13 east) :value t)
            (make-fact :type 'rover-moves :args '(rover1 waypoint11 waypoint13 east) :value t)
            (make-fact :type 'rover-moves :args '(rover1 waypoint11 waypoint12 east) :value nil)
        )
        nil
        '(((?rov . rover1) (?src . waypoint11) (?dst . waypoint13) (?dir . east))
          ((?rov . rover1) (?src . waypoint12) (?dst . waypoint13) (?dir . east)))
    )
    (test-unify
        (make-cnd 
            :inequality #'<
            :type 'energy
            :args '(?rov ?t)
            :value 60
        )
        (list
            (make-fact :type 'energy :args '(rover1 1) :value 10)
            (make-fact :type 'energy :args '(rover2 2) :value 10)
            (make-fact :type 'energy :args '(rover3 3) :value 10)
            (make-fact :type 'rover-moves :args '(rover1 waypoint12 waypoint13 east) :value t)
            (make-fact :type 'rover-moves :args '(rover1 waypoint11 waypoint13 east) :value t)
            (make-fact :type 'rover-moves :args '(rover1 waypoint11 waypoint12 east) :value nil)
            (make-fact :type 'energy :args '(rover1 4) :value 20)
            (make-fact :type 'energy :args '(rover2 5) :value 20)
            (make-fact :type 'energy :args '(rover3 6) :value 20)
            (make-fact :type 'energy :args '(rover1 7) :value 90)
            (make-fact :type 'energy :args '(rover2 8) :value 90)
            (make-fact :type 'energy :args '(rover3 9) :value 90)
        )
        '((?rov . rover1))
        '(((?rov . rover1) (?t . 4))
          ((?rov . rover1) (?t . 1)))
    )
    (test-unify
        (make-cnd 
            :inequality #'<
            :type 'energy
            :args '(rover1 ?t)
            :value 60
        )
        (list
            (make-fact :type 'energy :args '(rover1 1) :value 10)
            (make-fact :type 'energy :args '(rover2 2) :value 10)
            (make-fact :type 'energy :args '(rover3 3) :value 10)
            (make-fact :type 'rover-moves :args '(rover1 waypoint12 waypoint13 east) :value t)
            (make-fact :type 'rover-moves :args '(rover1 waypoint11 waypoint13 east) :value t)
            (make-fact :type 'rover-moves :args '(rover1 waypoint11 waypoint12 east) :value nil)
            (make-fact :type 'energy :args '(rover1 4) :value 20)
            (make-fact :type 'energy :args '(rover2 5) :value 20)
            (make-fact :type 'energy :args '(rover3 6) :value 20)
            (make-fact :type 'energy :args '(rover1 7) :value 90)
            (make-fact :type 'energy :args '(rover2 8) :value 90)
            (make-fact :type 'energy :args '(rover3 9) :value 90)
        )
        nil
        '(((?t . 4))
          ((?t . 1)))
    )
    (test-unify
        (make-cnd 
            :inequality #'<
            :type 'energy
            :args '(?rov ?t)
            :value 60
        )
        (list
            (make-fact :type 'energy :args '(rover1 1) :value 10)
            (make-fact :type 'energy :args '(rover2 2) :value 10)
            (make-fact :type 'energy :args '(rover3 3) :value 10)
            (make-fact :type 'rover-moves :args '(rover1 waypoint12 waypoint13 east) :value t)
            (make-fact :type 'rover-moves :args '(rover1 waypoint11 waypoint13 east) :value t)
            (make-fact :type 'rover-moves :args '(rover1 waypoint11 waypoint12 east) :value nil)
            (make-fact :type 'energy :args '(rover1 4) :value 20)
            (make-fact :type 'energy :args '(rover2 5) :value 20)
            (make-fact :type 'energy :args '(rover3 6) :value 20)
            (make-fact :type 'energy :args '(rover1 7) :value 90)
            (make-fact :type 'energy :args '(rover2 8) :value 90)
            (make-fact :type 'energy :args '(rover3 9) :value 90)
        )
        nil
        '(((?rov . rover3) (?t . 6))
          ((?rov . rover2) (?t . 5))
          ((?rov . rover1) (?t . 4))
          ((?rov . rover3) (?t . 3))
          ((?rov . rover2) (?t . 2))
          ((?rov . rover1) (?t . 1)))
    )
    (test-unify
        (make-cnd 
            :inequality #'neq
            :type 'rover-facing
            :args '(?rov ?t)
            :value 'east
        )
        (list
            (make-fact :type 'rover-facing :args '(rover1 1) :value 'west)
            (make-fact :type 'rover-facing :args '(rover1 2) :value 'east)
            (make-fact :type 'rover-facing :args '(rover1 3) :value 'north)
            (make-fact :type 'rover-facing :args '(rover1 4) :value 'south)
            (make-fact :type 'rover-facing :args '(rover2 5) :value 'west)
            (make-fact :type 'rover-facing :args '(rover2 6) :value 'east)
            (make-fact :type 'rover-facing :args '(rover2 7) :value 'north)
            (make-fact :type 'rover-facing :args '(rover2 8) :value 'south)
            (make-fact :type 'energy :args '(rover1 7) :value 90)
        )
        '((?rov . rover1))
        '(((?rov . rover1) (?t . 4))
          ((?rov . rover1) (?t . 3))
          ((?rov . rover1) (?t . 1)))
    )
    (test-unify
        (make-cnd 
            :inequality #'neq
            :type 'rover-facing
            :args '(rover3 ?t)
            :value 'east
        )
        (list
            (make-fact :type 'rover-facing :args '(rover1 1) :value 'west)
            (make-fact :type 'rover-facing :args '(rover1 2) :value 'east)
            (make-fact :type 'rover-facing :args '(rover1 3) :value 'north)
            (make-fact :type 'rover-facing :args '(rover1 4) :value 'south)
            (make-fact :type 'rover-facing :args '(rover2 5) :value 'west)
            (make-fact :type 'rover-facing :args '(rover2 6) :value 'east)
            (make-fact :type 'rover-facing :args '(rover2 7) :value 'north)
            (make-fact :type 'rover-facing :args '(rover2 8) :value 'south)
            (make-fact :type 'energy :args '(rover1 7) :value 90)
        )
        nil
        nil
    )
    (test-unify
        (make-cnd 
            :inequality #'neq
            :type 'rover-facing
            :args '(rover2 ?t)
            :value 'east
        )
        (list
            (make-fact :type 'rover-facing :args '(rover1 1) :value 'west)
            (make-fact :type 'rover-facing :args '(rover1 2) :value 'east)
            (make-fact :type 'rover-facing :args '(rover1 3) :value 'north)
            (make-fact :type 'rover-facing :args '(rover1 4) :value 'south)
            (make-fact :type 'rover-facing :args '(rover2 5) :value 'west)
            (make-fact :type 'rover-facing :args '(rover2 6) :value 'east)
            (make-fact :type 'rover-facing :args '(rover2 7) :value 'north)
            (make-fact :type 'rover-facing :args '(rover2 8) :value 'south)
            (make-fact :type 'energy :args '(rover1 7) :value 90)
        )
        nil
        '(((?t . 8))
          ((?t . 7))
          ((?t . 5)))
    )
    (test-unify
        (make-cnd 
            :inequality #'neq
            :type 'rover-facing
            :args '(rover2 1)
            :value 'east
        )
        (list
            (make-fact :type 'rover-facing :args '(rover1 1) :value 'west)
            (make-fact :type 'rover-facing :args '(rover1 2) :value 'east)
            (make-fact :type 'rover-facing :args '(rover1 3) :value 'north)
            (make-fact :type 'rover-facing :args '(rover1 4) :value 'south)
            (make-fact :type 'rover-facing :args '(rover2 5) :value 'west)
            (make-fact :type 'rover-facing :args '(rover2 6) :value 'east)
            (make-fact :type 'rover-facing :args '(rover2 7) :value 'north)
            (make-fact :type 'rover-facing :args '(rover2 8) :value 'south)
            (make-fact :type 'energy :args '(rover1 7) :value 90)
        )
        nil
        nil
    )
    (test-unify
        (make-cnd 
            :inequality #'neq
            :type 'rover-facing
            :args '(?rov ?t)
            :value 'east
        )
        (list
            (make-fact :type 'rover-facing :args '(rover1 1) :value 'west)
            (make-fact :type 'rover-facing :args '(rover1 2) :value 'east)
            (make-fact :type 'rover-facing :args '(rover1 3) :value 'north)
            (make-fact :type 'rover-facing :args '(rover1 4) :value 'south)
            (make-fact :type 'rover-facing :args '(rover2 5) :value 'west)
            (make-fact :type 'rover-facing :args '(rover2 6) :value 'east)
            (make-fact :type 'rover-facing :args '(rover2 7) :value 'north)
            (make-fact :type 'rover-facing :args '(rover2 8) :value 'south)
            (make-fact :type 'energy :args '(rover1 7) :value 90)
        )
        '((?rov . rover2) (?t . 5))
        '(((?rov . rover2) (?t . 5)))
    )
    (test-unify
        (make-cnd 
            :inequality #'neq
            :type 'rover-facing
            :args '(?rov ?t)
            :value 'east
        )
        (list
            (make-fact :type 'rover-facing :args '(rover1 1) :value 'west)
            (make-fact :type 'rover-facing :args '(rover1 2) :value 'east)
            (make-fact :type 'rover-facing :args '(rover1 3) :value 'north)
            (make-fact :type 'rover-facing :args '(rover1 4) :value 'south)
            (make-fact :type 'rover-facing :args '(rover2 5) :value 'west)
            (make-fact :type 'rover-facing :args '(rover2 6) :value 'east)
            (make-fact :type 'rover-facing :args '(rover2 7) :value 'north)
            (make-fact :type 'rover-facing :args '(rover2 8) :value 'south)
            (make-fact :type 'energy :args '(rover1 7) :value 90)
        )
        '((?rov . rover3))
        nil
    )
    (test-unify
        (make-cnd 
            :inequality #'neq
            :type 'rover-facing
            :args '(?rov ?t)
            :value '?dir
        )
        (list
            (make-fact :type 'rover-facing :args '(rover1 2) :value 'west)
            (make-fact :type 'rover-facing :args '(rover1 2) :value 'east)
            (make-fact :type 'rover-facing :args '(rover1 2) :value 'north)
            (make-fact :type 'rover-facing :args '(rover1 2) :value 'south)
            (make-fact :type 'energy :args '(rover1 7) :value 90)
        )
        '((?rov . rover1) (?t . 2) (?dir . east))
        '(((?rov . rover1) (?t . 2) (?dir . east)))
    )
    (test-unify
        (make-cnd 
            :type 'rover-facing
            :args '(?rov ?t)
            :value 'east
        )
        (list
            (make-fact :type 'rover-facing :args '(rover1 1) :value 'west)
            (make-fact :type 'rover-facing :args '(rover1 2) :value 'east)
            (make-fact :type 'rover-facing :args '(rover1 3) :value 'north)
            (make-fact :type 'rover-facing :args '(rover1 4) :value 'south)
            (make-fact :type 'rover-facing :args '(rover2 5) :value 'west)
            (make-fact :type 'rover-facing :args '(rover2 6) :value 'east)
            (make-fact :type 'rover-facing :args '(rover2 7) :value 'north)
            (make-fact :type 'rover-facing :args '(rover2 8) :value 'south)
            (make-fact :type 'energy :args '(rover1 7) :value 90)
        )
        nil
        '(((?rov . rover2) (?t . 6))
          ((?rov . rover1) (?t . 2)))
    )
    (test-unify
        (make-cnd 
            :type 'rover-facing
            :args '(?rov)
            :value '?dir
        )
        (list
            (make-fact :type 'rover-facing :args '(rover1) :value 'east)
            (make-fact :type 'rover-facing :args '(rover2) :value 'west)
            (make-fact :type 'rover-moves :args '(rover1 waypoint11 waypoint12 east) :value t)
            (make-fact :type 'rover-moves :args '(rover2 waypoint12 waypoint11 north) :value t)
        )
        nil
        '(((?rov . rover2) (?dir . west))
          ((?rov . rover1) (?dir . east)))
    )
    (test-unify
        (make-cnd 
            :inequality #'neq
            :type 'rover-moves
            :args '(?rov ?src ?dst ?dir)
            :value t
        )
        (list
            (make-fact :type 'rover-moves :args '(rover1 waypoint12 waypoint13 east) :value t)
        )
        '((?rov . rover1) (?src . waypoint12) (?dst . waypoint13) (?dir . east))
        nil
    )
    (test-unify
        (make-cnd 
            :type 'rover-moves
            :args '(?rov ?src ?dst ?dir)
            :value nil
        )
        (list
            (make-fact :type 'rover-moves :args '(rover1 waypoint12 waypoint13 east) :value nil)
        )
        '((?rov . rover1) (?src . waypoint12) (?dst . waypoint13) (?dir . east))
        '(((?rov . rover1) (?src . waypoint12) (?dst . waypoint13) (?dir . east)))
    )
    (test-unify
        (make-cnd 
            :type 'rover-moves
            :args '(?rov ?src ?dst ?dir)
            :value nil
        )
        (list
            (make-fact :type 'rover-moves :args '(rover1 waypoint12 waypoint13 east) :value t)
        )
        '((?rov . rover1) (?src . waypoint12) (?dst . waypoint13) (?dir . east))
        nil
    )
    (test-unify
        (make-cnd 
            :type 'rover-moves
            :args '(?rov ?src ?dst ?dir)
            :value nil
        )
        (list
            (make-fact :type 'rover-moves :args '(rover1 waypoint11 waypoint13 east) :value nil)
        )
        '((?rov . rover1) (?src . waypoint12) (?dst . waypoint13) (?dir . east))
        '(((?rov . rover1) (?src . waypoint12) (?dst . waypoint13) (?dir . east)))
    )
    
    (setq *hidden-types* '(sandy))
    
    (test-unify
        (make-cnd 
            :type 'sandy
            :args '(?rov)
            :value t
        )
        (list
            (make-fact :type 'sandy :args '(rover1) :value nil)
            (make-fact :type 'sandy :args '(rover2) :value t)
        )
        '((?rov . rover3))
        '(((?rov . rover3)))
        :assume-fn #'assume-hidden-and-closed-world
    )

    (test-unify
        (make-cnd 
            :type 'sandy
            :args '(?rov)
            :value t
        )
        (list
            (make-fact :type 'sandy :args '(rover1) :value nil)
            (make-fact :type 'sandy :args '(rover2) :value t)
        )
        '((?rov . rover1))
        nil
        :assume-fn #'assume-hidden-and-closed-world
    )

    (test-unify
        (make-cnd 
            :type 'sandy
            :args '(?rov)
            :value t
        )
        (list
            (make-fact :type 'sandy :args '(rover1) :value nil)
            (make-fact :type 'sandy :args '(rover2) :value t)
        )
        '((?rov . rover2))
        '(((?rov . rover2)))
        :assume-fn #'assume-hidden-and-closed-world
    )
    
    (test-unify
        (make-cnd 
            :type 'sandy
            :args '(?rov)
            :value t
        )
        (list
            (make-fact :type 'sandy :args '(rover1) :value nil)
            (make-fact :type 'sandy :args '(rover2) :value t)
        )
        nil
        '(((?rov . rover2)))
        :assume-fn #'assume-hidden-and-closed-world
    )
    
    
    (test-unify
        (make-cnd 
            :type 'adds-to
            :args '(?a ?b)
            :value '?d
        )
        (list )
        '((?a . 1) (?b . 2) (?d . 3))
        '(((?a . 1) (?b . 2) (?d . 3)))
    )

    (test-unify
        (make-cnd 
            :type 'adds-to
            :args '(?a ?b)
            :value '?d
        )
        (list )
        '((?a . 1) (?b . 2))
        '(((?a . 1) (?b . 2) (?d . 3)))
    )
    
    (test-unify
        (make-cnd 
            :type 'adds-to
            :args '(?a ?b)
            :value '?d
        )
        (list )
        '((?a . 1) (?d . 3))
        '(((?a . 1) (?b . 2) (?d . 3)))
    )
    
    (test-unify
        (make-cnd 
            :type 'adds-to
            :args '(?a ?b)
            :value '?d
        )
        (list )
        '((?b . 2) (?d . 3))
        '(((?a . 1) (?b . 2) (?d . 3)))
    )
    (test-unify
        (make-cnd 
            :type 'adds-to
            :args '(1 ?b)
            :value '?d
        )
        (list )
        '((?b . 2) (?d . 3))
        '(((?b . 2) (?d . 3)))
    )

    (test-unify
        (make-cnd 
            :type 'adds-to
            :args '(?a 2)
            :value '?d
        )
        (list )
        '((?a . 1) (?d . 3))
        '(((?a . 1) (?d . 3)))
    )

    (test-unify
        (make-cnd 
            :type 'adds-to
            :args '(?a ?b)
            :value 3
        )
        (list )
        '((?a . 1) (?b . 2))
        '(((?a . 1) (?b . 2)))
    )

    (test-unify
        (make-cnd 
            :type 'adds-to
            :args '(1 ?b)
            :value '?d
        )
        (list )
        '((?d . 3))
        '(((?b . 2) (?d . 3)))
    )

    (test-unify
        (make-cnd 
            :type 'adds-to
            :args '(?a 2)
            :value '?d
        )
        (list )
        '((?d . 3))
        '(((?a . 1) (?d . 3)))
    )

    (test-unify
        (make-cnd 
            :type 'adds-to
            :args '(?a 2)
            :value '?d
        )
        (list )
        '((?a . 1))
        '(((?a . 1) (?d . 3)))
    )

    (test-unify
        (make-cnd 
            :type 'adds-to
            :args '(?a ?b)
            :value 3
        )
        (list )
        '((?a . 1))
        '(((?a . 1) (?b . 2)))
    )

    (test-unify
        (make-cnd 
            :type 'adds-to
            :args '(?a ?b)
            :value 3
        )
        (list )
        '((?b . 2))
        '(((?a . 1) (?b . 2)))
    )

    (test-unify
        (make-cnd 
            :type 'adds-to
            :args '(1 2)
            :value 3
        )
        (list )
        nil
        '(())
    )

    (test-unify
        (make-cnd 
            :type 'adds-to
            :args '(?a ?b)
            :value '?d
        )
        (list )
        '((?a . 1) (?b . 2) (?d . 4))
        'fail
    )


    (test-unify
        (make-cnd 
            :type 'adds-to
            :args '(1 ?b)
            :value '?d
        )
        (list )
        '((?b . 2) (?d . 4))
        'fail
    )

    (test-unify
        (make-cnd 
            :type 'adds-to
            :args '(?a ?b)
            :value '4
        )
        (list )
        '((?a . 1) (?b . 2))
        'fail
    )

    (test-unify
        (make-cnd 
            :type 'adds-to
            :args '(1 2)
            :value '4
        )
        (list )
        nil
        'fail
    )
)

(defun test-unify-set (cnd-set fact-set binding-list exp-binding-lists &aux ret-binding-lists)
    (setq ret-binding-lists (unify-set cnd-set fact-set binding-list))
    (when (not (eql (length exp-binding-lists) (length ret-binding-lists)))
        (error "Expected ~A bindings from unify-set, found ~A."
            (length exp-binding-lists) (length ret-binding-lists)
        )
    )
    (dolist (exp-blist exp-binding-lists)
        (test-set-match 
            (car ret-binding-lists)
            exp-blist
            "bindings"
            (format nil "(unify-set ~A ~A ~A)" cnd-set fact-set binding-list)
        )
        (setq ret-binding-lists (cdr ret-binding-lists))
    )
)


(defun test-unify-set-cases ()
    (test-unify-set
        (list
            (make-cnd 
                :type 'rover-facing
                :args '(?rov ?t)
                :value 'east
            )
        )
        (list
            (make-fact :type 'rover-facing :args '(rover1 1) :value 'west)
            (make-fact :type 'rover-facing :args '(rover1 2) :value 'east)
            (make-fact :type 'rover-facing :args '(rover1 3) :value 'north)
            (make-fact :type 'rover-facing :args '(rover1 4) :value 'south)
            (make-fact :type 'rover-facing :args '(rover2 5) :value 'west)
            (make-fact :type 'rover-facing :args '(rover2 6) :value 'east)
            (make-fact :type 'rover-facing :args '(rover2 7) :value 'north)
            (make-fact :type 'rover-facing :args '(rover2 8) :value 'south)
            (make-fact :type 'energy :args '(rover1 7) :value 90)
        )
        nil
        '(((?rov . rover2) (?t . 6))
          ((?rov . rover1) (?t . 2)))
    )
    (test-unify-set
        (list
            (make-cnd 
                :type 'rover-facing
                :args '(?rov)
                :value '?dir
            )
            (make-cnd 
                :type 'rover-moves
                :args '(?rov ?src ?dst ?dir)
                :value t
            )
        )
        (list
            (make-fact :type 'rover-facing :args '(rover1) :value 'east)
            (make-fact :type 'rover-facing :args '(rover2) :value 'west)
            (make-fact :type 'rover-moves :args '(rover1 waypoint11 waypoint12 east) :value t)
            (make-fact :type 'rover-moves :args '(rover2 waypoint12 waypoint11 north) :value t)
        )
        nil
        '(((?rov . rover1) (?dir . east) (?src . waypoint11) (?dst . waypoint12)))
    )
    (test-unify-set
        (list
            (make-cnd 
                :type 'rover-facing
                :args '(?rov)
                :value '?dir
            )
            (make-cnd 
                :type 'rover-moves
                :args '(?rov ?src ?dst ?dir)
                :value t
            )
        )
        (list
            (make-fact :type 'rover-facing :args '(rover1) :value 'east)
            (make-fact :type 'rover-facing :args '(rover2) :value 'west)
            (make-fact :type 'rover-facing :args '(rover3) :value 'north)
            (make-fact :type 'rover-moves :args '(rover1 waypoint11 waypoint12 east) :value t)
            (make-fact :type 'rover-moves :args '(rover2 waypoint12 waypoint11 north) :value t)
            (make-fact :type 'rover-moves :args '(rover3 waypoint15 waypoint11 north) :value t)
            (make-fact :type 'distance :args '(waypoint11 waypoint12) :value 1)
            (make-fact :type 'distance :args '(waypoint12 waypoint11) :value 1)
            (make-fact :type 'distance :args '(waypoint15 waypoint11) :value 4)
        )
        nil
        '(((?rov . rover3) (?dir . north) (?src . waypoint15) (?dst . waypoint11))
          ((?rov . rover1) (?dir . east) (?src . waypoint11) (?dst . waypoint12)))
    )
    (test-unify-set
        (list
            (make-cnd 
                :type 'rover-facing
                :args '(?rov)
                :value '?dir
            )
            (make-cnd 
                :type 'rover-moves
                :args '(?rov ?src ?dst ?dir)
                :value t
            )
            (make-cnd 
                :type 'can-traverse
                :args '(?src ?dst)
                :value nil
            )
        )
        (list
            (make-fact :type 'rover-facing :args '(rover1) :value 'east)
            (make-fact :type 'rover-facing :args '(rover2) :value 'west)
            (make-fact :type 'rover-facing :args '(rover3) :value 'north)
            (make-fact :type 'rover-moves :args '(rover1 waypoint11 waypoint12 east) :value t)
            (make-fact :type 'rover-moves :args '(rover2 waypoint12 waypoint11 north) :value t)
            (make-fact :type 'rover-moves :args '(rover3 waypoint15 waypoint11 north) :value t)
            (make-fact :type 'distance :args '(waypoint11 waypoint12) :value 1)
            (make-fact :type 'distance :args '(waypoint12 waypoint11) :value 1)
            (make-fact :type 'distance :args '(waypoint15 waypoint11) :value 4)
            (make-fact :type 'can-traverse :args '(waypoint11 waypoint12) :value t)
            (make-fact :type 'can-traverse :args '(waypoint12 waypoint11) :value t)
        )
        nil
        '(((?rov . rover3) (?dir . north) (?src . waypoint15) (?dst . waypoint11)))
    )
)    

(defun test-process-pddl-to-condition (pddl-cond exp-cnd)
    (compare-conditions pddl-cond exp-cnd (process-pddl-to-condition pddl-cond))
)

(defun test-process-pddl-to-condition-cases ()
    (test-process-pddl-to-condition
        '(>= (energy ?rover) 80)
        (make-cnd
            :inequality #'>=
            :type 'energy
            :args '(?rover)
            :value 80
        )
    )
    (test-process-pddl-to-condition
        '(= (energy ?rover) 80)
        (make-cnd
            :inequality #'=
            :type 'energy
            :args '(?rover)
            :value 80
        )
    )
    (test-process-pddl-to-condition
        '(visible ?src ?dest)
        (make-cnd
            :type 'visible
            :args '(?src ?dest)
            :value t
        )
    )
    (test-process-pddl-to-condition
        '(not (blocked ?r))
        (make-cnd
            :type 'blocked
            :args '(?r)
            :value nil
        )
    )
    (test-process-pddl-to-condition
        '(eq (compass-points ?r) ?newNorth)
        (make-cnd
            :type 'compass-points
            :args '(?r)
            :value '?newNorth
        )
    )
    (test-process-pddl-to-condition
        '(can_traverse ?r ?src NOWHERE ?realDir)
        (make-cnd
            :type 'can_traverse
            :args '(?r ?src NOWHERE ?realDir)
            :value t
        )
    )
    (test-process-pddl-to-condition
        '(neq (at ?r) ?w)
        (make-cnd
            :inequality #'neq
            :type 'at
            :args '(?r)
            :value '?w
        )
    )
)

(defun test-process-pddl-effect-to-postcondition (pddl-effect exp-cnd &aux ret-cnd)
    (compare-conditions pddl-effect exp-cnd (process-pddl-effect-to-postcondition pddl-effect))
)
    
(defun compare-conditions (pddl-effect exp-cnd ret-cnd)
    (when (not (eq (cnd-inequality exp-cnd) (cnd-inequality ret-cnd)))
        (error "Conditions do not match for ~A. Expected inequality ~A, found ~A."
            pddl-effect (cnd-inequality exp-cnd) (cnd-inequality ret-cnd)
        )
    )
    (when (not (eq (cnd-type exp-cnd) (cnd-type ret-cnd)))
        (error "Conditions do not match for ~A. Expected type ~A, found ~A."
            pddl-effect (cnd-type exp-cnd) (cnd-type ret-cnd)
        )
    )
    (when (not (equal (cnd-args exp-cnd) (cnd-args ret-cnd)))
        (error "Conditions do not match for ~A. Expected args ~A, found ~A."
            pddl-effect (cnd-args exp-cnd) (cnd-args ret-cnd)
        )
    )
    (when (not (eq (cnd-value exp-cnd) (cnd-value ret-cnd)))
        (error "Conditions do not match for ~A. Expected value ~A, found ~A."
            pddl-effect (cnd-value exp-cnd) (cnd-value ret-cnd)
        )
    )
)    

(defun test-process-pddl-effect-to-postcondition-cases ()
    (test-process-pddl-effect-to-postcondition
        '(set (at ?r) ?w)
        (make-cnd
            :type 'at
            :args '(?r)
            :value '?w
        )
    )
    (test-process-pddl-effect-to-postcondition
        '(increase (at ?r) ?w)
        (make-cnd
            :type 'at
            :args '(?r)
            :value '?-at--r--after
        )
    )
    (test-process-pddl-effect-to-postcondition
        '(decrease (at ?r) 1)
        (make-cnd
            :type 'at
            :args '(?r)
            :value '?-at--r--after
        )
    )
    (test-process-pddl-effect-to-postcondition
        '(rover-moves ?rover west ?src ?dest)
        (make-cnd
            :type 'rover-moves
            :args '(?rover west ?src ?dest)
            :value t
        )
    )
    (test-process-pddl-effect-to-postcondition
        '(not (rover-moves ?rover west ?src ?dest))
        (make-cnd
            :type 'rover-moves
            :args '(?rover west ?src ?dest)
            :value nil
        )
    )
)

(defun test-process-pddl-effect-to-preconditions (pddl-effect exp-cnds &aux ret-cnds)
    (setq ret-cnds (process-pddl-effect-to-preconditions pddl-effect))
    (when (null exp-cnds)
        (if ret-cnds
            ;then
            (error "Expected no preconditions from pddl effect ~A, but found ~A."
                pddl-effect ret-cnds
            )
            ;else
            (return-from test-process-pddl-effect-to-preconditions nil)
        )
    )
    (when (null ret-cnds)
        (error "Found no preconditions from pddl effect ~A."
            pddl-effect
        )
    )
    (compare-conditions pddl-effect (first exp-cnds) (first ret-cnds))
    (compare-conditions pddl-effect (second exp-cnds) (second ret-cnds))
)
    
(defun test-process-pddl-effect-to-preconditions-cases ()
    (test-process-pddl-effect-to-preconditions
        '(set (at ?r) ?w)
        nil
    )
    (test-process-pddl-effect-to-preconditions
        '(increase (at ?r) ?w)
        (list
            (make-cnd
                :type 'at
                :args '(?r)
                :value '?-at--r--before
            )
            (make-cnd
                :type 'adds-to
                :args '(?w ?-at--r--before)
                :value '?-at--r--after
            )
        )
    )
    (test-process-pddl-effect-to-preconditions
        '(decrease (at ?r) 1)
        (list
            (make-cnd
                :type 'at
                :args '(?r)
                :value '?-at--r--before
            )
            (make-cnd
                :type 'adds-to
                :args '(1 ?-at--r--after)
                :value '?-at--r--before
            )
        )
    )
    (test-process-pddl-effect-to-preconditions
        '(rover-moves ?rover west ?src ?dest)
        nil
    )
    (test-process-pddl-effect-to-preconditions
        '(not (rover-moves ?rover west ?src ?dest))
        nil
    )
)

(defun test-build-event-model ()
    (load "rover/RoversEventsCompassFunc.pddl")
    (dolist (pddl-ev (envmodel-events *last-envmodel*))
        (print (build-event-model pddl-ev))
    )
)

(defun test-bind-condition-to-fact (cnd blist exp-fact facts &aux ret-fact)
    (setq ret-fact (bind-condition-to-fact cnd blist facts))
    (when (null ret-fact)
        (if (null exp-fact)
            ;then
            (return-from test-bind-condition-to-fact nil)
            ;else
            (error "Expected return value of ~A, found nil." exp-fact)
        )
    )
    (when (null exp-fact)
        (error "Expected nil return value, found ~A." ret-fact)
    )
    (when (not (mapcar #'equal-fact exp-fact ret-fact))
        (error "Bad return value from (bind-condition-to-fact ~A ~A). Expected ~A, found ~A."
            cnd blist exp-fact ret-fact
        )
    )
)

(defun test-bind-condition-to-fact-cases ()
    (test-bind-condition-to-fact
        (make-cnd 
            :type 'rover-moves
            :args '(?rov ?src ?dst ?dir)
            :value t
        )
        '((?rov . rover1) (?src . waypoint12) (?dst . waypoint13) (?dir . east))
        (list (make-fact :type 'rover-moves :args '(rover1 waypoint12 waypoint13 east) :value t))
        nil
    )
    (test-bind-condition-to-fact
        (make-cnd 
            :type 'rover-moves
            :args '(?rov ?src ?dst ?dir)
            :value t
        )
        '((?rov . rover1) (?src . waypoint11) (?dst . waypoint13) (?dir . east))
        (list (make-fact :type 'rover-moves :args '(rover1 waypoint11 waypoint13 east) :value t))    
        nil
    )
    (test-bind-condition-to-fact
        (make-cnd 
            :type 'rover-moves
            :args '(?rov waypoint11 ?dst ?dir)
            :value t
        )
        '((?rov . rover1) (?dst . waypoint13) (?dir . east))
        (list (make-fact :type 'rover-moves :args '(rover1 waypoint11 waypoint13 east) :value t))
        nil
    )
    (test-bind-condition-to-fact
        (make-cnd 
            :inequality #'<
            :type 'energy
            :args '(?rov ?t)
            :value 60
        )
        '((?rov . rover1) (?t . 4))
        (list (make-fact :type 'energy :args '(rover1 4) :value 20))
        (list
            (make-fact :type 'energy :args '(rover1 1) :value 10)
            (make-fact :type 'energy :args '(rover2 2) :value 10)
            (make-fact :type 'energy :args '(rover3 3) :value 10)
            (make-fact :type 'rover-moves :args '(rover1 waypoint12 waypoint13 east) :value t)
            (make-fact :type 'rover-moves :args '(rover1 waypoint11 waypoint13 east) :value t)
            (make-fact :type 'rover-moves :args '(rover1 waypoint11 waypoint12 east) :value nil)
            (make-fact :type 'energy :args '(rover1 4) :value 20)
            (make-fact :type 'energy :args '(rover2 5) :value 20)
            (make-fact :type 'energy :args '(rover3 6) :value 20)
            (make-fact :type 'energy :args '(rover1 7) :value 90)
            (make-fact :type 'energy :args '(rover2 8) :value 90)
            (make-fact :type 'energy :args '(rover3 9) :value 90)
        )
    )
    (test-bind-condition-to-fact
        (make-cnd 
            :inequality #'<
            :type 'energy
            :args '(rover1 ?t)
            :value 60
        )
        '((?t . 1))
        (list (make-fact :type 'energy :args '(rover1 1) :value 10))
        (list
            (make-fact :type 'energy :args '(rover1 1) :value 10)
            (make-fact :type 'energy :args '(rover2 2) :value 10)
            (make-fact :type 'energy :args '(rover3 3) :value 10)
            (make-fact :type 'rover-moves :args '(rover1 waypoint12 waypoint13 east) :value t)
            (make-fact :type 'rover-moves :args '(rover1 waypoint11 waypoint13 east) :value t)
            (make-fact :type 'rover-moves :args '(rover1 waypoint11 waypoint12 east) :value nil)
            (make-fact :type 'energy :args '(rover1 4) :value 20)
            (make-fact :type 'energy :args '(rover2 5) :value 20)
            (make-fact :type 'energy :args '(rover3 6) :value 20)
            (make-fact :type 'energy :args '(rover1 7) :value 90)
            (make-fact :type 'energy :args '(rover2 8) :value 90)
            (make-fact :type 'energy :args '(rover3 9) :value 90)
        )
    )
    (test-bind-condition-to-fact
        (make-cnd 
            :inequality #'neq
            :type 'rover-facing
            :args '(?rov ?t)
            :value 'east
        )
        '((?rov . rover1) (?t . 3))
        (list (make-fact :type 'rover-facing :args '(rover1 3) :value 'north))
        (list
            (make-fact :type 'rover-facing :args '(rover1 1) :value 'west)
            (make-fact :type 'rover-facing :args '(rover1 2) :value 'east)
            (make-fact :type 'rover-facing :args '(rover1 3) :value 'north)
            (make-fact :type 'rover-facing :args '(rover1 4) :value 'south)
            (make-fact :type 'rover-facing :args '(rover2 5) :value 'west)
            (make-fact :type 'rover-facing :args '(rover2 6) :value 'east)
            (make-fact :type 'rover-facing :args '(rover2 7) :value 'north)
            (make-fact :type 'rover-facing :args '(rover2 8) :value 'south)
            (make-fact :type 'energy :args '(rover1 7) :value 90)
        )
    )
   (test-bind-condition-to-fact
        (make-cnd 
            :inequality #'neq
            :type 'rover-facing
            :args '(rover2 1)
            :value 'east
        )
        nil
        nil
        (list
            (make-fact :type 'rover-facing :args '(rover1 1) :value 'west)
            (make-fact :type 'rover-facing :args '(rover1 2) :value 'east)
            (make-fact :type 'rover-facing :args '(rover1 3) :value 'north)
            (make-fact :type 'rover-facing :args '(rover1 4) :value 'south)
            (make-fact :type 'rover-facing :args '(rover2 5) :value 'west)
            (make-fact :type 'rover-facing :args '(rover2 6) :value 'east)
            (make-fact :type 'rover-facing :args '(rover2 7) :value 'north)
            (make-fact :type 'rover-facing :args '(rover2 8) :value 'south)
            (make-fact :type 'energy :args '(rover1 7) :value 90)
        )
    )
   (test-bind-condition-to-fact
        (make-cnd 
            :inequality #'neq
            :type 'rover-facing
            :args '(?rov ?t)
            :value 'east
        )
        '((?rov . rover2) (?t . 5))
        (list (make-fact :type 'rover-facing :args '(rover2 5) :value 'west))
        (list
            (make-fact :type 'rover-facing :args '(rover1 1) :value 'west)
            (make-fact :type 'rover-facing :args '(rover1 2) :value 'east)
            (make-fact :type 'rover-facing :args '(rover1 3) :value 'north)
            (make-fact :type 'rover-facing :args '(rover1 4) :value 'south)
            (make-fact :type 'rover-facing :args '(rover2 5) :value 'west)
            (make-fact :type 'rover-facing :args '(rover2 6) :value 'east)
            (make-fact :type 'rover-facing :args '(rover2 7) :value 'north)
            (make-fact :type 'rover-facing :args '(rover2 8) :value 'south)
            (make-fact :type 'energy :args '(rover1 7) :value 90)
        )
    )
   (test-bind-condition-to-fact
        (make-cnd 
            :type 'rover-facing
            :args '(?rov)
            :value '?dir
        )
        '((?rov . rover2) (?dir . west))
        (list (make-fact :type 'rover-facing :args '(rover2) :value 'west))
        (list
            (make-fact :type 'rover-facing :args '(rover1) :value 'south)
            (make-fact :type 'rover-facing :args '(rover2) :value 'west)
            (make-fact :type 'energy :args '(rover1 7) :value 90)
        )
    )
   (test-bind-condition-to-fact
        (make-cnd 
            :type 'rover::at
            :inequality #'neq
            :args '(rover::?r)
            :value 'rover::?w
        )
        '((rover::?w . rover::waypoint5) (rover::?r . rover::rover1))
        (list (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint11))
        (list
            (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
            (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint11)
        )
    )
   (test-bind-condition-to-fact
        (make-cnd 
            :type 'rover::at
            :inequality #'neq
            :args '(rover::?r)
            :value 'rover::?w
        )
        '((rover::?w . rover::waypoint5) (rover::?r . rover::rover1))
        (list 
            (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint11)
            (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint3)
        )
        (list
            (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
            (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint11)
            (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint3)
        )
    )
)

(defun test-enumerate-occs (ev-model facts pt exp-occs &aux ret-occs)
    (setq ret-occs (enumerate-occs ev-model facts pt))
    (test-set-match ret-occs exp-occs "occurrences" "enumerate-occs" :test #'occurrence-equal)
)
    
(defun test-enumerate-occs-cases ()
    (test-enumerate-occs
        #S(EVENT-MODEL :TYPE ROVER::ROVER-GETS-SANDY
            :ARG-TYPES ((ROVER::?R ROVER::ROVER))
            :variables (rover::?r rover::?w)
            :CONDITIONS
            (#S(CND :INEQUALITY NIL :TYPE ROVER::AT :ARGS (ROVER::?R) :VALUE ROVER::?W)
             #S(CND :INEQUALITY NIL :TYPE ROVER::SANDY :ARGS (ROVER::?W) :VALUE T)
             #S(CND :INEQUALITY NIL :TYPE ROVER::SAND-COVERED :ARGS (ROVER::?R)
                :VALUE NIL))
            :POSTCONDITIONS
            (#S(CND :INEQUALITY NIL :TYPE ROVER::SAND-COVERED :ARGS (ROVER::?R)
                :VALUE T))) 
        (list
            (make-fact :type 'rover::at :args '(rover1) :value 'waypoint12)
            (make-fact :type 'rover::sandy :args '(waypoint12) :value t)
            (make-fact :type 'rover::sand-covered :args '(rover1) :value nil)
        )
        *point0*
        (list
            (make-occurrence 
                :is-event t
                :signature '(rover::rover-gets-sandy rover1 waypoint12)
                :pre
                    (list
                        (make-fact :type 'rover::at :args '(rover1) :value 'waypoint12)
                        (make-fact :type 'rover::sandy :args '(waypoint12) :value t)
                        (make-fact :type 'rover::sand-covered :args '(rover1) :value nil)
                    )
                :post
                    (list
                        (make-fact :type 'rover::sand-covered :args '(rover1) :value t)
                    )
                :point *point0*
            )
        )
    )
    (test-enumerate-occs
        #S(EVENT-MODEL :TYPE ROVER::ROVER-GETS-SANDY
            :ARG-TYPES ((ROVER::?R ROVER::ROVER))
            :variables (rover::?r rover::?w)
            :CONDITIONS
            (#S(CND :INEQUALITY NIL :TYPE ROVER::AT :ARGS (ROVER::?R) :VALUE ROVER::?W)
             #S(CND :INEQUALITY NIL :TYPE ROVER::SANDY :ARGS (ROVER::?W) :VALUE T)
             #S(CND :INEQUALITY NIL :TYPE ROVER::SAND-COVERED :ARGS (ROVER::?R)
                :VALUE NIL))
            :POSTCONDITIONS
            (#S(CND :INEQUALITY NIL :TYPE ROVER::SAND-COVERED :ARGS (ROVER::?R)
                :VALUE T))) 
        (list
            (make-fact :type 'rover::at :args '(rover1) :value 'waypoint12)
            (make-fact :type 'rover::sandy :args '(waypoint12) :value t)
            (make-fact :type 'rover::sand-covered :args '(rover1) :value nil)
            (make-fact :type 'rover-facing :args '(rover1 2) :value 'east)
            (make-fact :type 'rover-facing :args '(rover1 3) :value 'north)
            (make-fact :type 'rover-facing :args '(rover1 4) :value 'south)
            (make-fact :type 'rover::at :args '(rover2) :value 'waypoint12)
            (make-fact :type 'rover::sand-covered :args '(rover2) :value nil)
        )
        *point0*
        (list
            (make-occurrence 
                :is-event t
                :signature '(rover::rover-gets-sandy rover1 waypoint12)
                :pre
                    (list
                        (make-fact :type 'rover::at :args '(rover1) :value 'waypoint12)
                        (make-fact :type 'rover::sandy :args '(waypoint12) :value t)
                        (make-fact :type 'rover::sand-covered :args '(rover1) :value nil)
                    )
                :post
                    (list
                        (make-fact :type 'rover::sand-covered :args '(rover1) :value t)
                    )
                :point *point0*
            )
            (make-occurrence 
                :is-event t
                :signature '(rover::rover-gets-sandy rover2 waypoint12)
                :pre
                    (list
                        (make-fact :type 'rover::at :args '(rover2) :value 'waypoint12)
                        (make-fact :type 'rover::sandy :args '(waypoint12) :value t)
                        (make-fact :type 'rover::sand-covered :args '(rover2) :value nil)
                    )
                :post
                    (list
                        (make-fact :type 'rover::sand-covered :args '(rover2) :value t)
                    )
                :point *point0*
            )
        )
    )
    
    (test-enumerate-occs
         (make-EVENT-MODEL :TYPE 'ROVER::LOCATION-LEFT
            :ARG-TYPES '((ROVER::?R ROVER::ROVER))
            :variables '(rover::?r rover::?w)
            :CONDITIONS
            (list
             (make-CND :INEQUALITY NIL :TYPE 'ROVER::DETECTS-LOCATION
                :ARGS '(ROVER::?R ROVER::?W) :VALUE T)
             (make-CND
                :INEQUALITY #'neq
                :TYPE 'ROVER::AT :ARGS '(ROVER::?R) :VALUE 'ROVER::?W))
            :POSTCONDITIONS
            (list
              (make-CND :INEQUALITY NIL :TYPE 'ROVER::DETECTS-LOCATION
                :ARGS '(ROVER::?R ROVER::?W) :VALUE NIL))) 
        (list
            (make-fact :type 'rover::detects-location :args '(rover1 waypoint12) :value t)
            (make-fact :type 'rover::at :args '(rover1) :value 'waypoint13)
            (make-fact :type 'rover::detects-location :args '(rover2 waypoint15) :value t)
            (make-fact :type 'rover::at :args '(rover2) :value 'waypoint15)
        )
        *point0*
        (list
            (make-occurrence 
                :is-event t
                :signature '(rover::location-left rover1 waypoint12)
                :pre
                    (list
                        (make-fact :type 'rover::detects-location :args '(rover1 waypoint12) :value t)
                        (make-fact :type 'rover::at :args '(rover1) :value 'waypoint13)
                    )
                :post
                    (list
                        (make-fact :type 'rover::detects-location :args '(rover1 waypoint12) :value nil)
                    )
                :point *point0*
            )
        )
    )

    (test-enumerate-occs
         (make-EVENT-MODEL :TYPE 'ROVER::LOCATION-LEFT
            :ARG-TYPES '((ROVER::?R ROVER::ROVER))
            :variables '(rover::?r rover::?w)
            :CONDITIONS
            (list
             (make-CND :INEQUALITY NIL :TYPE 'ROVER::DETECTS-LOCATION
                :ARGS '(ROVER::?R ROVER::?W) :VALUE T)
             (make-CND
                :INEQUALITY #'neq
                :TYPE 'ROVER::AT :ARGS '(ROVER::?R) :VALUE 'ROVER::?W))
            :POSTCONDITIONS
            (list
              (make-CND :INEQUALITY NIL :TYPE 'ROVER::DETECTS-LOCATION
                :ARGS '(ROVER::?R ROVER::?W) :VALUE NIL))) 
        (list
            (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint5) :value t)
            (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
        )
        *point0*
        nil
    )
)

(defun test-construct-explanation (zero-events exp-exp &aux ret-exp)
    (setq ret-exp (construct-explanation zero-events))
    (test-set-match 
        (explanation-prior-events ret-exp) 
        (explanation-prior-events exp-exp)
        "prior event list" 
        "construct-explanation"
        :test #'occurrence-equal
    )
    (test-set-match 
        (explanation-new-events ret-exp) 
        (explanation-new-events exp-exp)
        "new events" 
        "construct-explanation"
        :test #'occurrence-equal
    )
    (test-set-match 
        (explanation-event-removals ret-exp) 
        (explanation-event-removals exp-exp)
        "event removals" 
        "construct-explanation"
        :test #'occurrence-equal
    )
    (test-set-match 
        (explanation-intervals ret-exp) 
        (explanation-intervals exp-exp)
        "intervals" 
        "construct-explanation"
    )
)

(defun test-construct-explanation-cases ()
    (setq *ev-models* 
        (apply #'append
            (mapcar #'build-event-model (envmodel-events *last-envmodel*))
        )
    )
    (setq *execution-history*
        (list
            (make-occurrence 
                :signature '(o0)
                :pre 
                    (list 
                        (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint11)
                        (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::energy :args '(rover::rover1) :value 100)
                        (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                        (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                        (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
                        (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
                        (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint11) :value t)
                    )
                :post nil
                :is-observation t
                :point *point0*
            )
            (make-occurrence 
                :signature '(rover::navigate-detected rover::rover1 rover::north)
                :pre
                    (list 
                        (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::energy :args '(rover::rover1) :value 100)
                        (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                    )
                :post
                    (list 
                        (make-fact :type 'rover::energy :args '(rover::rover1) :value 92)
                        (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value t)
                    )
                :is-action t
                :point (nth 1 *point-list*)
            )
            (make-occurrence 
                :signature '(o1)
                :pre
                    (list 
                        (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::energy :args '(rover::rover1) :value 92)
                        (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                        (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                        (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
                        (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
                        (make-fact :type 'rover::next-compass-point :args '(rover::north rover::east) :value t)
                        (make-fact :type 'rover::real-direction :args '(rover::east rover::north rover::east) :value t)
                        (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint5 rover::waypoint6 rover::east) :value t)
                        (make-fact :type 'rover::visible :args '(rover::waypoint5 rover::waypoint6) :value t)
                    )
                :post nil
                :is-observation t
                :point (nth 4 *point-list*)
            )
        )
    )
    
    (setq move-event1
        (make-occurrence 
            :signature '(rover::rover-moves rover::rover1 rover::north rover::waypoint11 rover::north rover::north rover::waypoint5)
            :pre
                (list 
                    (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value t)
                    (make-fact :type 'rover::blocked :args '(rover::rover1) :value nil)
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint11)
                    (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                    (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                    (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
                    (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
                )
            :post
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value nil)
                    (make-fact :type 'rover::compass-moved :args '(rover::rover1) :value nil)
                )
            :is-event t
            :point (nth 2 *point-list*)
        )
    )
    
    (setq detect-event1
        (make-occurrence 
            :signature '(rover::location-detected rover::rover1 rover::waypoint5)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint5) :value nil)
                    (make-fact :type 'rover::sandy :args '(rover::waypoint5) :value nil)
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value nil)
                )
            :post
                (list 
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint5) :value t)
                    (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                )
            :is-event t
            :point (nth 3 *point-list*)
        )
    )
    
    (setq leave-event1
        (make-occurrence 
            :signature '(rover::location-left rover::rover1 rover::waypoint11)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint11) :value t)
                )
            :post
                (list 
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint11) :value nil)
                )
            :is-event t
            :point (nth 3 *point-list*)
        )
    )
    
    (test-construct-explanation
        nil
        (make-explanation
            :new-events
                (list
                    move-event1
                    detect-event1
                    leave-event1
                )
        )
    )

    (setq init-sandy-event
        (make-occurrence 
            :signature '(assume-initial-value sandy waypoint5 t)
            :pre
                (list 
                    (make-fact :type 'rover::sandy :args '(rover::waypoint5) :value t)
                )
            :post nil
            :is-event t
            :point *point0*
        )
    )
    
    (setq gets-sandy-event
        (make-occurrence 
            :signature '(rover::rover-gets-sandy rover::rover1 rover::waypoint5)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::sandy :args '(rover::waypoint5) :value t)
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value nil)
                )
            :post
                (list 
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value t)
                )
            :is-event t
            :point (nth 3 *point-list*)
        )
    )
    
    (test-construct-explanation
        (list init-sandy-event)
        (make-explanation
            :prior-events (list init-sandy-event)
            :new-events
                (list
                    move-event1 
                    leave-event1
                    gets-sandy-event
                )
        )
    )
    
    (setq *execution-history*
        (append *execution-history*
            (list
                (make-occurrence 
                    :signature '(rover::navigate-detected rover::rover1 rover::east)
                    :pre
                        (list 
                            (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                            (make-fact :type 'rover::energy :args '(rover::rover1) :value 92)
                            (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                        )
                    :post
                        (list 
                            (make-fact :type 'rover::energy :args '(rover::rover1) :value 84)
                            (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::east) :value t)
                        )
                    :is-action t
                    :point (nth 5 *point-list*)
                )
                (make-occurrence 
                    :signature '(o2)
                    :pre 
                        (list 
                            (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                            (make-fact :type 'rover::energy :args '(rover::rover1) :value 100)
                            (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                            (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::east)
                            (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                            (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
                            (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
                            (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint6) :value t)
                        )
                    :post nil
                    :is-observation t
                    :point (nth 9 *point-list*)
                )
            )
        )
    )
    
    (setq init-windy-event
        (make-occurrence 
            :signature '(assume-initial-value windy waypoint6 t)
            :pre
                (list 
                    (make-fact :type 'rover::windy :args '(rover::waypoint6) :value t)
                )
            :post nil
            :is-event t
            :point *point0*
        )
    )
    
    (setq cleaned-event
        (make-occurrence 
            :signature '(rover::rover-cleaned rover::rover1 rover::waypoint6)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint6)
                    (make-fact :type 'rover::sandy :args '(rover::waypoint6) :value nil)
                    (make-fact :type 'rover::windy :args '(rover::waypoint6) :value t)
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value t)
                )
            :post
                (list 
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value nil)
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint6) :value t)
                    (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                )
            :is-event t
            :point (nth 7 *point-list*)
        )
    )
    
    (setq confused-event
        (make-occurrence 
            :signature '(rover::compass-confused rover::rover1 rover::waypoint6 rover::north rover::east)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint6)
                    (make-fact :type 'rover::windy :args '(rover::waypoint6) :value t)
                    (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                    (make-fact :type 'rover::next-compass-point :args '(rover::north rover::east) :value t)
                    (make-fact :type 'rover::compass-moved :args '(rover::rover1) :value nil)
                )
            :post
                (list 
                    (make-fact :type 'rover::compass-moved :args '(rover::rover1) :value t)
                    (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::east)
                )
            :is-event t
            :point (nth 7 *point-list*)
        )
    )
    
    (setq move-event2
        (make-occurrence 
            :signature '(rover::rover-moves rover::rover1 rover::east rover::waypoint5 rover::north rover::east rover::waypoint6)
            :pre
                (list 
                    (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::east) :value t)
                    (make-fact :type 'rover::blocked :args '(rover::rover1) :value nil)
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                    (make-fact :type 'rover::real-direction :args '(rover::east rover::north rover::east) :value t)
                    (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint5 rover::waypoint6 rover::east) :value t)
                    (make-fact :type 'rover::visible :args '(rover::waypoint5 rover::waypoint6) :value t)
                )
            :post
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint6)
                    (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::east) :value nil)
                    (make-fact :type 'rover::compass-moved :args '(rover::rover1) :value nil)
                )
            :is-event t
            :point (nth 6 *point-list*)
        )
    )
    
    (setq detect-event2
        (make-occurrence 
            :signature '(rover::location-detected rover::rover1 rover::waypoint6)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint6)
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint6) :value nil)
                    (make-fact :type 'rover::sandy :args '(rover::waypoint6) :value nil)
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value nil)
                )
            :post
                (list 
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint6) :value t)
                    (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                )
            :is-event t
            :point (nth 7 *point-list*)
        )
    )
    
    (setq leave-event2
        (make-occurrence 
            :signature '(rover::location-left rover::rover1 rover::waypoint5)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint6)
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint5) :value t)
                )
            :post
                (list 
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint5) :value nil)
                )
            :is-event t
            :point (nth 7 *point-list*)
        )
    )
    
    (test-construct-explanation
        nil
        (make-explanation
            :prior-events nil
            :new-events
                (list
                    move-event1 
                    leave-event1
                    detect-event1
                    move-event2
                    leave-event2
                    detect-event2
                )
        )
    )

    
    (test-construct-explanation
        (list init-sandy-event)
        (make-explanation
            :prior-events (list init-sandy-event)
            :new-events
                (list
                    move-event1 
                    leave-event1
                    gets-sandy-event
                    move-event2
                )
        )
    )
    
    (test-construct-explanation
        (list init-sandy-event init-windy-event)
        (make-explanation
            :prior-events (list init-sandy-event init-windy-event)
            :new-events
                (list
                    move-event1 
                    leave-event1
                    gets-sandy-event
                    move-event2
                    cleaned-event
                    confused-event
                )
        )
    )
    
    (test-construct-explanation
        (list init-windy-event)
        (make-explanation
            :prior-events (list init-windy-event)
            :new-events
                (list
                    move-event1 
                    detect-event1
                    leave-event1
                    move-event2
                    detect-event2
                    leave-event2
                    confused-event
                )
        )
    )
)

(defun test-add-occ-to-layer-cases (&aux (e1 (copy-occurrence e1)) (e2 (copy-occurrence e2)) (e3 (copy-occurrence e3)) (e6 (copy-occurrence e6)) )
    (setq *assumption-point* *point0*)
    (setq *assumption-occ* (make-occurrence :point *assumption-point*))    

    (setq assumption-layer (make-layer :pt *assumption-point*))
    (setq layer1 (make-layer :pt (nth 1 *point-list*)))
    (setq layer2 (make-layer :pt (nth 2 *point-list*)))
    (setq layer3 (make-layer :pt (nth 3 *point-list*)))
    (setq layer4 (make-layer :pt (nth 4 *point-list*)))
    (setq point-layer-map 
        (list
            (cons *assumption-point* assumption-layer)
            (cons (nth 1 *point-list*) layer1)
            (cons (nth 2 *point-list*) layer2)
            (cons (nth 3 *point-list*) layer3)
            (cons (nth 4 *point-list*) layer4)
        )
    )
    
    (setq fact-occs (list (cons f12t :original)))
    
    (setf (occurrence-point e1) (nth 1 *point-list*)) 
    (add-occ-to-layer e1 layer1 point-layer-map fact-occs)
    (test-set-match 
        (layer-occurrence-back-links layer1) 
        (list (list e1 :original))
        "occ back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-back-links layer1) 
        (list (list f12f e1) (list f13t e1))
        "fact back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-forward-links layer1) 
        (list (list f12f) (list f13t))
        "fact forward links"
        "add-occ-to-layer"
    )
    
    (push (cons f12f e1) fact-occs)
    (push (cons f13t e1) fact-occs)
    (setf (occurrence-point e2) (nth 2 *point-list*)) 
    (add-occ-to-layer e2 layer2 point-layer-map fact-occs)

    (test-set-match 
        (layer-occurrence-back-links layer2) 
        (list (list e2 (list f13t (nth 1 *point-list*))))
        "occ back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-back-links layer2) 
        (list (list f13f e2) (list f14t e2))
        "fact back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-forward-links layer2) 
        (list (list f13f) (list f14t))
        "fact forward links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-occurrence-back-links layer1) 
        (list (list e1 :original))
        "occ back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-back-links layer1) 
        (list (list f12f e1) (list f13t e1))
        "fact back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-forward-links layer1) 
        (list (list f12f) (list f13t e2))
        "fact forward links"
        "add-occ-to-layer"
    )
    
    (push (cons f14t e2) fact-occs)
    (push (cons f13f e2) fact-occs)
    (setf (occurrence-interval e3) nil) 
    (setf (occurrence-point e3) (nth 3 *point-list*)) 
    (add-occ-to-layer e3 layer3 point-layer-map fact-occs)
    
    
    (test-set-match 
        (layer-occurrence-back-links layer3) 
        (list (list e3 (list f14t (nth 2 *point-list*)) (list fotherf *assumption-point*)))
        "occ back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-back-links layer3) 
        (list (list f14f e3) (list f13t e3) (list fother e3))
        "fact back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-forward-links layer3) 
        (list (list f14f) (list f13t) (list fother))
        "fact forward links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-occurrence-back-links layer2) 
        (list (list e2 (list f13t (nth 1 *point-list*))))
        "occ back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-back-links layer2) 
        (list (list f13f e2) (list f14t e2))
        "fact back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-forward-links layer2) 
        (list (list f13f) (list f14t e3))
        "fact forward links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-occurrence-back-links layer1) 
        (list (list e1 :original))
        "occ back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-back-links layer1) 
        (list (list f12f e1) (list f13t e1))
        "fact back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-forward-links layer1) 
        (list (list f12f) (list f13t e2))
        "fact forward links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-occurrence-back-links assumption-layer) 
        nil
        "occ back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-back-links assumption-layer) 
        nil
        "fact back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-forward-links assumption-layer) 
        (list (list fotherf e3))
        "fact forward links"
        "add-occ-to-layer"
    )
    
    (setf (occurrence-interval e6) nil) 
    (setf (occurrence-point e6) (nth 3 *point-list*)) 
    (add-occ-to-layer e6 layer3 point-layer-map fact-occs)
    
    
    (test-set-match 
        (layer-occurrence-back-links layer3) 
        (list (list e3 (list f14t (nth 2 *point-list*)) (list fotherf *assumption-point*))
              (list e6 :original (list fotherf *assumption-point*))
        )
        "occ back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-back-links layer3) 
        (list (list f14f e6 e3) (list f13t e3) (list fother e6 e3) (list f12t e6))
        "fact back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-forward-links layer3) 
        (list (list f14f) (list f13t) (list fother) (list f12t))
        "fact forward links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-occurrence-back-links layer2) 
        (list (list e2 (list f13t (nth 1 *point-list*))))
        "occ back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-back-links layer2) 
        (list (list f13f e2) (list f14t e2))
        "fact back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-forward-links layer2) 
        (list (list f13f) (list f14t e3))
        "fact forward links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-occurrence-back-links assumption-layer) 
        nil
        "occ back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-back-links assumption-layer) 
        nil
        "fact back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-forward-links assumption-layer) 
        (list (list fotherf e6 e3))
        "fact forward links"
        "add-occ-to-layer"
    )
    
)

(defun test-eliminate-cases (&aux (e1 (copy-occurrence e1)) (e2 (copy-occurrence e2)) (e3 (copy-occurrence e3)) (e4 (copy-occurrence e4)) (e6 (copy-occurrence e6)) )
    (setq *assumption-point* *point0*)
    (setq *assumption-occ* (make-occurrence :point *assumption-point*))    

    (setq assumption-layer (make-layer :pt *assumption-point*))
    (setq layer1 (make-layer :pt (nth 1 *point-list*)))
    (setq layer2 (make-layer :pt (nth 2 *point-list*)))
    (setq layer3 (make-layer :pt (nth 3 *point-list*)))
    (setq layer4 (make-layer :pt (nth 4 *point-list*)))
    (setq point-layer-map 
        (list
            (cons *assumption-point* assumption-layer)
            (cons (nth 1 *point-list*) layer1)
            (cons (nth 2 *point-list*) layer2)
            (cons (nth 3 *point-list*) layer3)
            (cons (nth 4 *point-list*) layer4)
        )
    )
    
    
    (setq fact-occs (list (cons f12t :original)))
    
    (setf (occurrence-point e1) (nth 1 *point-list*)) 
    (add-occ-to-layer e1 layer1 point-layer-map fact-occs)
    
    (push (cons f12f e1) fact-occs)
    (push (cons f13t e1) fact-occs)
    (setf (occurrence-point e2) (nth 2 *point-list*)) 
    (add-occ-to-layer e2 layer2 point-layer-map fact-occs)
    
    (push (cons f14t e2) fact-occs)
    (push (cons f13f e2) fact-occs)
    (setf (occurrence-interval e3) nil) 
    (setf (occurrence-point e3) (nth 3 *point-list*)) 
    (add-occ-to-layer e3 layer3 point-layer-map fact-occs)
    
    (setf (occurrence-interval e6) nil) 
    (setf (occurrence-point e6) (nth 3 *point-list*)) 
    (add-occ-to-layer e6 layer3 point-layer-map fact-occs)
    
    (when (not (equal
                (eliminate-fact f13t (nth 3 *point-list*) point-layer-map)
                nil
          )    )
        (error "Bad return from eliminate-fact.")
    )
    
    (test-set-match 
        (layer-occurrence-back-links layer3) 
        (list (list e6 :original (list fotherf *assumption-point*)))
        "occ back links"
        "eliminate-fact"
    )
    (test-set-match 
        (layer-fact-back-links layer3) 
        (list (list f14f e6) (list fother e6) (list f12t e6))
        "fact back links"
        "eliminate-fact"
    )
    (test-set-match 
        (layer-fact-forward-links layer3) 
        (list (list f14f) (list fother) (list f12t))
        "fact forward links"
        "eliminate-fact"
    )
    (test-set-match 
        (layer-occurrence-back-links layer2) 
        (list (list e2 (list f13t (nth 1 *point-list*))))
        "occ back links"
        "eliminate-fact"
    )
    (test-set-match 
        (layer-fact-back-links layer2) 
        (list (list f13f e2) (list f14t e2))
        "fact back links"
        "eliminate-fact"
    )
    (test-set-match 
        (layer-fact-forward-links layer2) 
        (list (list f13f) (list f14t))
        "fact forward links"
        "eliminate-fact"
    )

    (test-set-match 
        (layer-occurrence-back-links layer1) 
        (list (list e1 :original))
        "occ back links"
        "eliminate-fact"
    )
    (test-set-match 
        (layer-fact-back-links layer1) 
        (list (list f12f e1) (list f13t e1))
        "fact back links"
        "eliminate-fact"
    )
    (test-set-match 
        (layer-fact-forward-links layer1) 
        (list (list f12f) (list f13t e2))
        "fact forward links"
        "eliminate-fact"
    )
    (test-set-match 
        (layer-occurrence-back-links assumption-layer) 
        nil
        "occ back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-back-links assumption-layer) 
        nil
        "fact back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-forward-links assumption-layer) 
        (list (list fotherf e6))
        "fact forward links"
        "add-occ-to-layer"
    )
    
    
    (setq assumption-layer (make-layer :pt *assumption-point*))
    (setq layer1 (make-layer :pt (nth 1 *point-list*)))
    (setq layer2 (make-layer :pt (nth 2 *point-list*)))
    (setq layer3 (make-layer :pt (nth 3 *point-list*)))
    (setq layer4 (make-layer :pt (nth 4 *point-list*)))
    (setq point-layer-map 
        (list
            (cons *assumption-point* assumption-layer)
            (cons (nth 1 *point-list*) layer1)
            (cons (nth 2 *point-list*) layer2)
            (cons (nth 3 *point-list*) layer3)
            (cons (nth 4 *point-list*) layer4)
        )
    )
    
    
    (setq fact-occs (list nil))
    
    (setf (occurrence-point e1) (nth 1 *point-list*)) 
    (add-occ-to-layer e1 layer1 point-layer-map fact-occs)
    
    (push (cons f12f e1) fact-occs)
    (push (cons f13t e1) fact-occs)
    (setf (occurrence-point e2) (nth 2 *point-list*)) 
    (add-occ-to-layer e2 layer2 point-layer-map fact-occs)
    
    (push (cons f14t e2) fact-occs)
    (push (cons f13f e2) fact-occs)
    (setf (occurrence-interval e3) nil) 
    (setf (occurrence-point e3) (nth 3 *point-list*)) 
    (add-occ-to-layer e3 layer3 point-layer-map fact-occs)
    
    (setf (occurrence-interval e6) nil) 
    (setf (occurrence-point e6) (nth 3 *point-list*)) 
    (add-occ-to-layer e6 layer3 point-layer-map fact-occs)
    
    (push (cons f14f e3) fact-occs)
    (push (cons f13t e3) fact-occs)
    (push (cons fother e3) fact-occs)
    (push (cons f14f e6) fact-occs)
    (push (cons f12t e6) fact-occs)
    (push (cons fother e6) fact-occs)
    (setf (occurrence-interval e4) nil) 
    (setf (occurrence-point e4) (nth 4 *point-list*)) 
    (add-occ-to-layer e4 layer4 point-layer-map fact-occs)

    (test-set-match 
        (layer-occurrence-back-links layer4) 
        (list (list e4 (list fother (nth 3 *point-list*))))
        "occ back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-back-links layer4) 
        (list (list fotherf e4))
        "fact back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-forward-links layer4) 
        (list (list fotherf))
        "fact forward links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-occurrence-back-links layer3) 
        (list (list e3 (list f14t (nth 2 *point-list*)) (list fotherf *assumption-point*))
              (list e6 (list f12t *assumption-point*) (list fotherf *assumption-point*))
        )
        "occ back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-back-links layer3) 
        (list (list f14f e6 e3) (list f13t e3) (list fother e6 e3) (list f12t e6))
        "fact back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-forward-links layer3) 
        (list (list f14f) (list f13t) (list fother e4) (list f12t))
        "fact forward links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-occurrence-back-links assumption-layer) 
        nil
        "occ back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-back-links assumption-layer) 
        nil
        "fact back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-forward-links assumption-layer) 
        (list (list fotherf e6 e3) (list f12t e6 e1))
        "fact forward links"
        "add-occ-to-layer"
    )
    
    
    (test-set-match 
        (eliminate-event e4 point-layer-map)
        (list 
            (list fother (nth 3 *point-list*))
        )
        "eliminated facts"
        "eliminate-event"
    )
    
    
    (test-set-match 
        (layer-occurrence-back-links layer4) 
        (list )
        "occ back links"
        "eliminate-fact"
    )
    (test-set-match 
        (layer-fact-back-links layer4) 
        (list )
        "fact back links"
        "eliminate-fact"
    )
    (test-set-match 
        (layer-fact-forward-links layer4) 
        (list )
        "fact forward links"
        "eliminate-fact"
    )
    (test-set-match 
        (layer-occurrence-back-links layer3) 
        (list)
        "occ back links"
        "eliminate-fact"
    )
    (test-set-match 
        (layer-fact-back-links layer3) 
        (list )
        "fact back links"
        "eliminate-fact"
    )
    (test-set-match 
        (layer-fact-forward-links layer3) 
        (list )
        "fact forward links"
        "eliminate-fact"
    )
    (test-set-match 
        (layer-occurrence-back-links layer2) 
        (list (list e2 (list f13t (nth 1 *point-list*))))
        "occ back links"
        "eliminate-fact"
    )
    (test-set-match 
        (layer-fact-back-links layer2) 
        (list (list f13f e2) (list f14t e2))
        "fact back links"
        "eliminate-fact"
    )
    (test-set-match 
        (layer-fact-forward-links layer2) 
        (list (list f13f) (list f14t))
        "fact forward links"
        "eliminate-fact"
    )

    (test-set-match 
        (layer-occurrence-back-links layer1) 
        (list (list e1 (list f12t *assumption-point*)))
        "occ back links"
        "eliminate-fact"
    )
    (test-set-match 
        (layer-fact-back-links layer1) 
        (list (list f12f e1) (list f13t e1))
        "fact back links"
        "eliminate-fact"
    )
    (test-set-match 
        (layer-fact-forward-links layer1) 
        (list (list f12f) (list f13t e2))
        "fact forward links"
        "eliminate-fact"
    )
    
    

    (setq assumption-layer (make-layer :pt *assumption-point*))
    (setq layer1 (make-layer :pt (nth 1 *point-list*)))
    (setq point-layer-map 
        (list
            (cons *assumption-point* assumption-layer)
            (cons (nth 1 *point-list*) layer1)
        )
    )
    
    
    (setq fact-occs (list (cons f12f :original)))
    
    (setq ea
        (make-occurrence
            :signature '(ea)
            :pre (list f13t f12f)
            :post nil
            :is-event t
            :point (nth 1 *point-list*)
        )
    )
    (setq eb
        (make-occurrence
            :signature '(eb)
            :pre (list f13t f14f)
            :post nil
            :is-event t
            :point (nth 1 *point-list*)
        )
    )
    (setq ec
        (make-occurrence
            :signature '(ec)
            :pre (list f12f f14f)
            :post nil
            :is-event t
            :point (nth 1 *point-list*)
        )
    )

    (add-occ-to-layer ea layer1 point-layer-map fact-occs)
    (add-occ-to-layer eb layer1 point-layer-map fact-occs)
    (add-occ-to-layer ec layer1 point-layer-map fact-occs)
    
    (test-set-match 
        (layer-occurrence-back-links layer1) 
        (list (list ea (list f13t *assumption-point*) :original)
              (list eb (list f13t *assumption-point*) (list f14f *assumption-point*))
              (list ec :original (list f14f *assumption-point*))
        )
        "occ back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-back-links layer1) 
        nil
        "fact back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-forward-links layer1) 
        nil
        "fact forward links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-occurrence-back-links assumption-layer) 
        nil
        "occ back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-back-links assumption-layer) 
        nil
        "fact back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-forward-links assumption-layer) 
        (list (list f13t eb ea) (list f14f ec eb))
        "fact forward links"
        "add-occ-to-layer"
    )

    
    
    (test-set-match 
        (eliminate-event ea point-layer-map)
        (list (list f13t *assumption-point*))
        "eliminated facts"
        "eliminate-event"
    )
    


    
    (test-set-match 
        (layer-occurrence-back-links layer1) 
        (list (list ec :original (list f14f *assumption-point*)))
        "occ back links"
        "eliminate-event"
    )
    (test-set-match 
        (layer-fact-back-links layer1) 
        nil
        "fact back links"
        "eliminate-event"
    )
    (test-set-match 
        (layer-fact-forward-links layer1) 
        nil
        "fact forward links"
        "eliminate-event"
    )
    (test-set-match 
        (layer-occurrence-back-links assumption-layer) 
        nil
        "occ back links"
        "eliminate-event"
    )
    (test-set-match 
        (layer-fact-back-links assumption-layer) 
        nil
        "fact back links"
        "eliminate-event"
    )
    (test-set-match 
        (layer-fact-forward-links assumption-layer) 
        (list (list f14f ec))
        "fact forward links"
        "eliminate-event"
    )
    
    
    (setq assumption-layer (make-layer :pt *assumption-point*))
    (setq layer1 (make-layer :pt (nth 1 *point-list*)))
    (setq layer2 (make-layer :pt (nth 2 *point-list*)))
    (setq point-layer-map 
        (list
            (cons *assumption-point* assumption-layer)
            (cons (nth 1 *point-list*) layer1)
            (cons (nth 2 *point-list*) layer2)
        )
    )
    
    
    (setq fact-occs (list nil))
    
    (setf (occurrence-interval e3) nil)
    (setf (occurrence-interval e4) nil)
    (setf (occurrence-interval e6) nil)
    (setf (occurrence-point e3) (nth 1 *point-list*))
    (setf (occurrence-point e4) (nth 1 *point-list*))
    (setf (occurrence-point e6) (nth 2 *point-list*))

    (add-occ-to-layer e3 layer1 point-layer-map fact-occs)
    (add-occ-to-layer e4 layer1 point-layer-map fact-occs)
    
    (push (cons f14t *assumption-occ*) fact-occs)
    (push (cons fotherf *assumption-occ*) fact-occs)
    (push (cons fother *assumption-occ*) fact-occs)
    (push (cons f14f e3) fact-occs)
    (push (cons f13t e3) fact-occs)
    (push (cons fother e3) fact-occs)
    (push (cons fotherf e4) fact-occs)
    (add-occ-to-layer e6 layer2 point-layer-map fact-occs)
    
    (test-set-match 
        (layer-occurrence-back-links layer2) 
        (list (list e6 (list f12t *assumption-point*) (list fotherf (nth 1 *point-list*))))
        "occ back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-back-links layer2) 
        (list (list f14f e6) (list f12t e6) (list fother e6))
        "fact back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-forward-links layer2) 
        (list (list f14f) (list f12t) (list fother))
        "fact forward links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-occurrence-back-links layer1) 
        (list (list e3 (list f14t *assumption-point*) (list fotherf *assumption-point*))
              (list e4 (list fother *assumption-point*))
        )
        "occ back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-back-links layer1) 
        (list (list f14f e3) (list f13t e3) (list fother e3) (list fotherf e4))
        "fact back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-forward-links layer1) 
        (list (list f14f) (list f13t) (list fother) (list fotherf e6))
        "fact forward links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-occurrence-back-links assumption-layer) 
        nil
        "occ back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-back-links assumption-layer) 
        nil
        "fact back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-forward-links assumption-layer) 
        (list (list f12t e6) (list fotherf e3) (list fother e4) (list f14t e3))
        "fact forward links"
        "add-occ-to-layer"
    )
    


    (test-set-match 
        (eliminate-event e4 point-layer-map)
        (list (list fother *assumption-point*))
        "eliminated facts"
        "eliminate-event"
    )
    


    (test-set-match 
        (layer-occurrence-back-links layer2) 
        (list )
        "occ back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-back-links layer2) 
        (list )
        "fact back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-forward-links layer2) 
        (list )
        "fact forward links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-occurrence-back-links layer1) 
        (list (list e3 (list f14t *assumption-point*) (list fotherf *assumption-point*)))
        "occ back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-back-links layer1) 
        (list (list f14f e3) (list f13t e3) (list fother e3))
        "fact back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-forward-links layer1) 
        (list (list f14f) (list f13t) (list fother))
        "fact forward links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-occurrence-back-links assumption-layer) 
        nil
        "occ back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-back-links assumption-layer) 
        nil
        "fact back links"
        "add-occ-to-layer"
    )
    (test-set-match 
        (layer-fact-forward-links assumption-layer) 
        (list (list f12t) (list fotherf e3) (list f14t e3))
        "fact forward links"
        "add-occ-to-layer"
    )
    
)

(defun test-find-possible-events (facts new-facts pt obs-facts exp-evs &aux ret-evs)
    (setq ret-evs (find-possible-events facts new-facts pt obs-facts nil))
    (test-set-match 
        ret-evs 
        exp-evs
        "possible events" 
        "find-possible-events"
        :test #'occurrence-equal
    )
)
(defun test-find-possible-events-cases ()
    (setq *ev-models* 
        (apply #'append
            (mapcar #'build-event-model (envmodel-events *last-envmodel*))
        )
    )
    (get-model-information)
    
    (setq *hidden-types* '(rover::sandy))
    
    (test-find-possible-events
        (list
            (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint11)
            (make-fact :type 'rover::available :args '(rover::rover1) :value t)
            (make-fact :type 'rover::energy :args '(rover::rover1) :value 100)
            (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
            (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
            (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
            (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
            (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
            (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint11) :value t)
            (make-fact :type 'rover::energy :args '(rover::rover1) :value 92)
            (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value t)
        )
        (list
            (make-fact :type 'rover::energy :args '(rover::rover1) :value 92)
            (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value t)
        )
        *point0*
        nil
        (list
            (make-occurrence
                :signature '(rover::rover-moves rover::rover1 rover::north rover::waypoint11 rover::north rover::north rover::waypoint5)
                :is-event t
                :point (nth 1 *point-list*)
                :pre
                    (list
                        (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value t)
                        (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint11)
                        (make-fact :type 'rover::blocked :args '(rover::rover1) :value nil)
                        (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                        (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                        (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
                        (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
                    )
                :post
                    (list
                        (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value nil)
                        (make-fact :type 'rover::compass-moved :args '(rover::rover1) :value nil)
                        (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    )
            )
            (make-occurrence
                :signature '(rover::location-left rover::rover1 rover::waypoint11)
                :is-event t
                :point (nth 2 *point-list*)
                :pre
                    (list
                        (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint11) :value t)
                        (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    )
                :post
                    (list
                        (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint11) :value nil)
                    )
            )
            (make-occurrence
                :signature '(rover::location-detected rover::rover1 rover::waypoint5)
                :is-event t
                :point (nth 2 *point-list*)
                :pre
                    (list
                        (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                        (make-fact :type 'rover::sandy :args '(rover::waypoint5) :value nil)
                        (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value nil)
                        (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint5) :value nil)
                    )
                :post
                    (list
                        (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint5) :value t)
                        (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                    )
            )
            (make-occurrence
                :signature '(rover::rover-gets-sandy rover::rover1 rover::waypoint5)
                :is-event t
                :point (nth 2 *point-list*)
                :pre
                    (list
                        (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                        (make-fact :type 'rover::sandy :args '(rover::waypoint5) :value t)
                        (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value nil)
                    )
                :post
                    (list
                        (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value t)
                    )
            )
        )
    )

    (setq *hidden-types* '(rover::at rover::sandy))

    (test-find-possible-events
        (list
            (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint12)
            (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint11)
            (make-fact :type 'rover::available :args '(rover::rover1) :value t)
            (make-fact :type 'rover::energy :args '(rover::rover1) :value 100)
            (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
            (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
            (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
            (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
            (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
            (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint12 rover::waypoint6 rover::north) :value t)
            (make-fact :type 'rover::visible :args '(rover::waypoint12 rover::waypoint6) :value t)
            (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint13 rover::waypoint7 rover::north) :value t)
            (make-fact :type 'rover::visible :args '(rover::waypoint13 rover::waypoint7) :value t)
            (make-fact :type 'rover::energy :args '(rover::rover1) :value 92)
            (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value t)
        )
        (list
            (make-fact :type 'rover::energy :args '(rover::rover1) :value 92)
            (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value t)
        )
        *point0*
        nil
        (list
            (make-occurrence
                :signature '(rover::rover-moves rover::rover1 rover::north rover::waypoint11 rover::north rover::north rover::waypoint5)
                :is-event t
                :point (nth 1 *point-list*)
                :pre
                    (list
                        (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value t)
                        (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint11)
                        (make-fact :type 'rover::blocked :args '(rover::rover1) :value nil)
                        (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                        (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                        (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
                        (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
                    )
                :post
                    (list
                        (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value nil)
                        (make-fact :type 'rover::compass-moved :args '(rover::rover1) :value nil)
                        (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    )
            )
            (make-occurrence
                :signature '(rover::rover-moves rover::rover1 rover::north rover::waypoint12 rover::north rover::north rover::waypoint6)
                :is-event t
                :point (nth 1 *point-list*)
                :pre
                    (list
                        (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value t)
                        (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint12)
                        (make-fact :type 'rover::blocked :args '(rover::rover1) :value nil)
                        (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                        (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                        (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint12 rover::waypoint6 rover::north) :value t)
                        (make-fact :type 'rover::visible :args '(rover::waypoint12 rover::waypoint6) :value t)
                    )
                :post
                    (list
                        (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value nil)
                        (make-fact :type 'rover::compass-moved :args '(rover::rover1) :value nil)
                        (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint6)
                    )
            )
            (make-occurrence
                :signature '(rover::location-detected rover::rover1 rover::waypoint5)
                :is-event t
                :point (nth 2 *point-list*)
                :pre
                    (list
                        (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                        (make-fact :type 'rover::sandy :args '(rover::waypoint5) :value nil)
                        (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value nil)
                        (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint5) :value nil)
                    )
                :post
                    (list
                        (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint5) :value t)
                        (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                    )
            )
            (make-occurrence
                :signature '(rover::location-detected rover::rover1 rover::waypoint6)
                :is-event t
                :point (nth 2 *point-list*)
                :pre
                    (list
                        (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint6)
                        (make-fact :type 'rover::sandy :args '(rover::waypoint6) :value nil)
                        (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value nil)
                        (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint6) :value nil)
                    )
                :post
                    (list
                        (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint6) :value t)
                        (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                    )
            )
            (make-occurrence
                :signature '(rover::rover-gets-sandy rover::rover1 rover::waypoint5)
                :is-event t
                :point (nth 2 *point-list*)
                :pre
                    (list
                        (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                        (make-fact :type 'rover::sandy :args '(rover::waypoint5) :value t)
                        (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value nil)
                    )
                :post
                    (list
                        (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value t)
                    )
            )
            (make-occurrence
                :signature '(rover::rover-gets-sandy rover::rover1 rover::waypoint6)
                :is-event t
                :point (nth 2 *point-list*)
                :pre
                    (list
                        (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint6)
                        (make-fact :type 'rover::sandy :args '(rover::waypoint6) :value t)
                        (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value nil)
                    )
                :post
                    (list
                        (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value t)
                    )
            )
        )
    )
            
)

(defun test-point-earlier-cases ()
    (loop for num1 from 0 to 10 do
        (loop for num2 from 0 to 10 do
            (setq ret (point-earlier (nth num1 *point-list*) (nth num2 *point-list*)))
            (setq expected-ret (< num1 num2))
            (when (not (eq expected-ret ret))
                (error "Point-earlier fails for ~A ~A.")
            )
        )
    )
)

(defun test-compatible-cause-exists (fact causes old-fact-occs layers expected-ret)
    (when (not (eq expected-ret (compatible-cause-exists fact causes old-fact-occs layers)))
        (error "Expected (compatible-cause-exists ~A ~A) to return ~A, found ~A."
            fact causes expected-ret (not expected-ret)
        )
    )
)


(defun test-compatible-cause-exists-cases ()
    (setq old-fact-occs 
        (list 
            (cons f12t ev1) 
            (cons f12f ev1) 
            (cons f13t ev1) 
            (cons f13f ev1) 
            (cons f12t ev1) 
            (cons fother ev1)
            (cons fotherf ev1)
        )
    )
    (setq layer1 (make-layer :pt pt))
    (setq pt2 (point-next pt))
    (setq layers (list (cons pt2 (make-layer :pt pt2)) (cons pt layer1)))
    (setf (layer-mutex-hash layer1) (make-full-mutex-hash (cdr layers) nil))
    (test-compatible-cause-exists f12f nil old-fact-occs layers nil)
    (test-compatible-cause-exists f12f (list ev2) old-fact-occs layers nil)
    (test-compatible-cause-exists f12f (list ev4) old-fact-occs layers t)
    (test-compatible-cause-exists f12f (list ev2 ev4) old-fact-occs layers t)
    (test-compatible-cause-exists f12f (list ev2 ev5) old-fact-occs layers nil)
    (test-compatible-cause-exists f13f (list ev3) old-fact-occs layers nil)
    (test-compatible-cause-exists f12t (list ev3) old-fact-occs layers nil)
    (test-compatible-cause-exists fother (list ev1 ev2 ev3 ev4 ev5 ev6 ev7) old-fact-occs layers t)
    (push (cons f12f f13f) (layer-fact-mutexes layer1))
    (push (cons f13f f12f) (layer-fact-mutexes layer1))
    (setf (layer-fact-forward-links layer1) (list (list f12f) (list f13f)))
    (setf (layer-fact-back-links layer1) (list (list f12f) (list f13f)))
    (setf (layer-mutex-hash layer1) (make-full-mutex-hash (cdr layers) nil))
    (test-compatible-cause-exists f12f (list ev2 ev4) old-fact-occs layers nil)
)

(defun test-facts-are-mutex (fact1 fact2 fact-occs layers expected-ret)
    (when (not (eq expected-ret (facts-are-mutex fact1 fact2 fact-occs layers)))
        (error "Expected (facts-are-mutex ~A ~A) to return ~A, found ~A."
            fact1 fact2 expected-ret (not expected-ret)
        )
    )
)

(defun test-facts-are-mutex-cases ()
    (setq fact-occs 
        (list 
            (cons f14f :original)
            (cons f12t ev1) 
            (cons f12f ev1) 
            (cons f13f ev1) 
            (cons f12t ev1) 
            (cons fother ev1)
        )
    )
    (setq layer1 (make-layer :pt pt))
    (setq layers (list (cons pt layer1)))
    (test-facts-are-mutex f12t f12f fact-occs layers t)
    (test-facts-are-mutex f12t f12t fact-occs layers nil)
    (test-facts-are-mutex f12t f13f fact-occs layers nil)
    (push (cons f12t f13f) (layer-fact-mutexes layer1))
    (push (cons f13f f12t) (layer-fact-mutexes layer1))
    (test-facts-are-mutex f12t f13f fact-occs layers t)
    (test-facts-are-mutex f13f f12t fact-occs layers t)
    (test-facts-are-mutex f12t f14f fact-occs layers nil)
    (test-facts-are-mutex f14f f13f fact-occs layers nil)
)

(defun test-decide-occ-mutex (occ1 cur-layer layers fact-occs occ2 expected-ret)
    (when (not (eq expected-ret (decide-occ-mutex occ1 cur-layer layers fact-occs occ2)))
        (error "Expected (decide-occ-mutex ~A ~A) to return ~A, found ~A."
            occ1 occ2 expected-ret (not expected-ret)
        )
    )
    (when expected-ret
        (when (not (member (cons occ1 occ2) (layer-occurrence-mutexes cur-layer) :test #'equal))
            (error "Mutex not properly added.")
        )
        (when (not (member (cons occ2 occ1) (layer-occurrence-mutexes cur-layer) :test #'equal))
            (error "Mutex not properly added.")
        )
    )
)

(defun test-decide-occ-mutex-cases ()
    (setq fact-occs 
        (list 
            (cons f12t ev1) 
            (cons f12f ev1) 
            (cons f13f ev1) 
            (cons f13t ev1) 
            (cons fother ev1)
        )
    )
    (setq layer1 (make-layer :pt pt))
    (setq pt2 (point-next pt))
    (setq layers (list (cons pt2 (make-layer :pt pt2)) (cons pt layer1)))
    (setf (layer-mutex-hash layer1) (make-full-mutex-hash (cdr layers) nil))
    (test-decide-occ-mutex ev1 layer1 layers fact-occs ev2 t)
    (test-decide-occ-mutex ev1 layer1 layers fact-occs ev3 nil)
    (test-decide-occ-mutex ev4 layer1 layers fact-occs ev3 t)
    (test-decide-occ-mutex ev4 layer1 layers fact-occs ev5 nil)
    (push (cons f12t f13f) (layer-fact-mutexes layer1))
    (push (cons f13f f12t) (layer-fact-mutexes layer1))
    (setf (layer-fact-forward-links layer1) (list (list f12f) (list f13f)))
    (setf (layer-fact-back-links layer1) (list (list f12f) (list f13f)))
    (setf (layer-mutex-hash layer1) (make-full-mutex-hash (cdr layers) nil))
    (test-decide-occ-mutex ev4 layer1 layers fact-occs ev5 t)
)

(defun test-decide-fact-mutex (cur-layer new-fact other-fact old-fact-occs old-facts layers expected-ret)
    (when (not (eq expected-ret (decide-fact-mutex cur-layer new-fact other-fact old-fact-occs old-facts layers)))
        (error "Expected (decide-fact-mutex ~A ~A) to return ~A, found ~A."
            new-fact other-fact expected-ret (not expected-ret)
        )
    )
    (when expected-ret
        (when (and (not (member (cons new-fact other-fact) (layer-fact-mutexes cur-layer) :test #'equal))
                   (not (member (cons new-fact other-fact) (layer-continuation-mutexes cur-layer) :test #'equal)))
            (error "Mutex not properly added.")
        )
    )
)

(defun test-decide-fact-mutex-cases ()
    (setq *assumption-point* *point0*)
    (setq *assumption-occ* (make-occurrence :point *assumption-point*))    

    (setq assumption-layer (make-layer :pt *assumption-point*))
    (setq layer1 (make-layer :pt (nth 1 *point-list*)))
    (setq layer2 (make-layer :pt (nth 2 *point-list*)))
    (setq point-layer-map 
        (list
            (cons (nth 2 *point-list*) layer2)
            (cons (nth 1 *point-list*) layer1)
            (cons *assumption-point* assumption-layer)
        )
    )
    
    (setq fact-occs (list (cons f12t :original)))
    
    (setq le0
        (make-occurrence 
            :signature '(le0)
            :pre (list fother)
            :post (list fotherf)
            :is-event t
            :point (nth 1 *point-list*)
        )
    )

    (setq le1
        (make-occurrence 
            :signature '(le1)
            :pre (list f12t fother)
            :post (list f12f f14t)
            :is-event t
            :point (nth 1 *point-list*)
        )
    )
    
    
    (setq le2
        (make-occurrence 
            :signature '(le2)
            :pre (list f14t fother)
            :post (list f14f f12t)
            :is-event t
            :point (nth 2 *point-list*)
        )
    )

    
    (setq le3
        (make-occurrence 
            :signature '(le3)
            :pre (list f14t fotherf)
            :post (list f14f)
            :is-event t
            :point (nth 2 *point-list*)
        )
    )
    
    
    (setq le4
        (make-occurrence 
            :signature '(le4)
            :pre (list f12t fotherf)
            :post (list f13t f12f)
            :is-event t
            :point (nth 2 *point-list*)
        )
    )

    (setq le5
        (make-occurrence 
            :signature '(le5)
            :pre (list fother)
            :post (list fotherf)
            :is-event t
            :point (nth 2 *point-list*)
        )
    )

    (setf (layer-mutex-hash layer1) (make-full-mutex-hash (cdr layers) nil))
    (setf (layer-mutex-hash layer2) (make-full-mutex-hash layers nil))

    (add-occ-to-layer le0 layer1 point-layer-map fact-occs)
    (add-occ-to-layer le1 layer1 point-layer-map fact-occs)
    
    (push (cons fotherf le0) fact-occs)
    (push (cons f12f le1) fact-occs)
    (push (cons f14t le1) fact-occs)
    
    (add-occ-to-layer le2 layer2 point-layer-map fact-occs)
    (add-occ-to-layer le3 layer2 point-layer-map fact-occs)
    (add-occ-to-layer le4 layer2 point-layer-map fact-occs)
    (add-occ-to-layer le5 layer2 point-layer-map fact-occs)
    
    (test-decide-occ-mutex le2 layer2 point-layer-map fact-occs le3 t)
    (test-decide-occ-mutex le2 layer2 point-layer-map fact-occs le4 t)
    (test-decide-occ-mutex le3 layer2 point-layer-map fact-occs le4 nil)
    (test-decide-occ-mutex le2 layer2 point-layer-map fact-occs le5 nil)
    (test-decide-occ-mutex le3 layer2 point-layer-map fact-occs le5 t)
    (test-decide-occ-mutex le4 layer2 point-layer-map fact-occs le5 t)

    ;; We use the old-fact-occs to call decide-fact-mutex.
    ; (push (cons f12t le2) fact-occs)
    ; (push (cons f14f le2) fact-occs)
    ; (push (cons f14f le3) fact-occs)
    ; (push (cons f12f le4) fact-occs)
    ; (push (cons f13t le4) fact-occs)
    ; (push (cons fotherf le5) fact-occs)
    
    (setq old-facts (list f12t fother fotherf f12f f14t))
    

    (test-decide-fact-mutex layer2 fother fotherf fact-occs old-facts point-layer-map nil)
    (test-decide-fact-mutex layer2 f12t fotherf fact-occs old-facts point-layer-map nil)
    (test-decide-fact-mutex layer2 f14f f12f fact-occs old-facts point-layer-map nil)
    (test-decide-fact-mutex layer2 f14f f13t fact-occs old-facts point-layer-map nil)
    
    (test-decide-fact-mutex layer2 f13t fother fact-occs old-facts point-layer-map t)
    (test-decide-fact-mutex layer2 fother f13t fact-occs old-facts point-layer-map t)
    
    (test-decide-fact-mutex layer2 f14f fother fact-occs old-facts point-layer-map nil)
    (test-decide-fact-mutex layer2 fother f14f fact-occs old-facts point-layer-map nil)
    
    (test-decide-fact-mutex layer2 f14f fotherf fact-occs old-facts point-layer-map nil)
    (test-decide-fact-mutex layer2 fotherf f14f fact-occs old-facts point-layer-map nil)

    (test-decide-fact-mutex layer2 f13t f12f fact-occs old-facts point-layer-map nil)
    (test-decide-fact-mutex layer2 f12f f13t fact-occs old-facts point-layer-map nil)
    
    (test-decide-fact-mutex layer2 f12f fother fact-occs old-facts point-layer-map t)
    (test-decide-fact-mutex layer2 fother f12f fact-occs old-facts point-layer-map t)

    (push (cons f12f fother) (layer-fact-mutexes layer1))
    (push (cons fother f12f) (layer-fact-mutexes layer1))

    (setf (layer-mutex-hash layer1) (make-full-mutex-hash (cdr layers) nil))
    (setf (layer-mutex-hash layer2) (make-full-mutex-hash layers nil))
    
    (test-decide-fact-mutex layer2 f12f fother fact-occs old-facts point-layer-map t)
    (test-decide-fact-mutex layer2 fother f12f fact-occs old-facts point-layer-map t)
)

(defun test-mutex-facts-in (precs mutex-pairs layers expected-ret)
    (when (not (eq (mutex-facts-in precs mutex-pairs layers) expected-ret))
        (error "Expected (mutex-facts-in ~A ~A ~A) to return ~A, found ~A."
            precs mutex-pairs layers expected-ret (not expected-ret)
        )
    )
)

(defun test-mutex-facts-in-cases ()
    (setq layer1 (make-layer :pt pt))
    (setq ea 
        (make-occurrence 
            :signature '(rover-moves east waypoint13 waypoint14)
            :pre (list f13t)
            :post (list f13f f14t)
            :is-event t
            :point pt
        )
    )
    (setq eb 
        (make-occurrence 
            :signature '(rover-moves east waypoint12 waypoint13)
            :pre (list f13f)
            :post (list f13t f12f)
            :is-event t
            :point pt
        )
    )
    (push (cons f13f f12f) (layer-fact-mutexes layer1))
    (push (cons f12f f13f) (layer-fact-mutexes layer1))
    (push (cons f12f f14t) (layer-fact-mutexes layer1))
    (push (cons f14t f12f) (layer-fact-mutexes layer1))
    (push (cons f14t f13t) (layer-fact-mutexes layer1))
    (push (cons f13t f14t) (layer-fact-mutexes layer1))

    (setq pt2 (point-next pt))
    (setq layers (list (cons pt2 (make-layer :pt pt2)) (cons pt layer1)))
    (setq back-links (list (cons f13f ea) (cons f13t eb) (cons f14t ea) (cons f12f eb)))
    (setq others (list (list f14f) (list f12t)))
    (setf (layer-fact-back-links layer1) (append back-links others))
    (setf (layer-fact-forward-links layer1) (append back-links others))
    (setf (layer-mutex-hash layer1) (make-full-mutex-hash (cdr layers) nil))
    
    
    ; (test-mutex-facts-in 
        ; (list f12f f13f)
        ; nil
        ; layers
        ; nil
    ; )
    (test-mutex-facts-in 
        (list f12f f13f)
        back-links
        layers
        t
    )
    (test-mutex-facts-in 
        (list f13f f13t)
        back-links
        layers
        t
    )

    (test-mutex-facts-in 
        (list f12f f13t f14t)
        back-links
        layers
        t
    )

    (test-mutex-facts-in 
        (list f12f f13t f14f)
        back-links
        layers
        nil
    )
)

(defun test-refine-by-adding-occurrence (ex inc exp-exps &aux ret-exps)
    (setq ret-exps (refine-by-adding-occurrence ex inc))
    (test-set-match 
        ret-exps 
        exp-exps
        "explanations" 
        "refine-by-adding-occurrence"
        :test #'explanation-equal
    )
)

(defun test-refine-by-removing-occurrence (ex occ exp-exps &aux ret-exps)
    (setq ret-exps (refine-by-removing-occurrence ex occ))
    (test-set-match 
        ret-exps 
        exp-exps
        "explanations" 
        "refine-by-removing-occurrence"
        :test #'explanation-equal
    )
)

(defun test-refine-by-hypothesizing-initial-value (ex occ exp-exps &aux ret-exps)
    (setq ret-exps (refine-by-hypothesizing-initial-value ex occ))
    (test-set-match 
        ret-exps 
        exp-exps
        "explanations" 
        "refine-by-hypothesizing-initial-value"
        :test #'explanation-equal
    )
)


(defun test-refine-by-adding-occurrence-cases ()
   (setq *hidden-types* '(rover::sandy rover::windy))
   (setq *ev-models* 
        (apply #'append
            (mapcar #'build-event-model (envmodel-events *last-envmodel*))
        )
    )
    (setq *execution-history*
        (list
            (make-occurrence 
                :signature '(o0)
                :pre 
                    (list 
                        (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint11)
                        (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::energy :args '(rover::rover1) :value 100)
                        (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                        (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                        (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
                        (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
                        (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint11) :value t)
                    )
                :post nil
                :is-observation t
                :point *point0*
            )
            (make-occurrence 
                :signature '(rover::navigate-detected rover::rover1 rover::north)
                :pre
                    (list 
                        (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::energy :args '(rover::rover1) :value 100)
                        (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                    )
                :post
                    (list 
                        (make-fact :type 'rover::energy :args '(rover::rover1) :value 92)
                        (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value t)
                    )
                :is-action t
                :point (nth 1 *point-list*)
            )
            (make-occurrence 
                :signature '(o1)
                :pre
                    (list 
                        (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                        (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::energy :args '(rover::rover1) :value 92)
                        (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                        (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                        (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
                        (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
                        (make-fact :type 'rover::next-compass-point :args '(rover::north rover::east) :value t)
                        (make-fact :type 'rover::real-direction :args '(rover::east rover::north rover::east) :value t)
                        (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint5 rover::waypoint6 rover::east) :value t)
                        (make-fact :type 'rover::visible :args '(rover::waypoint5 rover::waypoint6) :value t)
                        (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value t)
                    )
                :post nil
                :is-observation t
                :point (nth 4 *point-list*)
            )
        )
    )
    
    (setq move-event1
        (make-occurrence 
            :signature '(rover::rover-moves rover::rover1 rover::north rover::waypoint11 rover::north rover::north rover::waypoint5)
            :pre
                (list 
                    (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value t)
                    (make-fact :type 'rover::blocked :args '(rover::rover1) :value nil)
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint11)
                    (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                    (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                    (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
                    (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
                )
            :post
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value nil)
                    (make-fact :type 'rover::compass-moved :args '(rover::rover1) :value nil)
                )
            :is-event t
            :point (nth 2 *point-list*)
        )
    )
    
    (setq detect-event1
        (make-occurrence 
            :signature '(rover::location-detected rover::rover1 rover::waypoint5)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint5) :value nil)
                    (make-fact :type 'rover::sandy :args '(rover::waypoint5) :value nil)
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value nil)
                )
            :post
                (list 
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint5) :value t)
                    (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                )
            :is-event t
            :point (nth 3 *point-list*)
        )
    )
    
    (setq leave-event1
        (make-occurrence 
            :signature '(rover::location-left rover::rover1 rover::waypoint11)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint11) :value t)
                )
            :post
                (list 
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint11) :value nil)
                )
            :is-event t
            :point (nth 3 *point-list*)
        )
    )
    

    (setq init-sandy-event
        (make-occurrence 
            :signature '(assume-initial-value sandy waypoint5 t)
            :pre
                (list 
                    (make-fact :type 'rover::sandy :args '(rover::waypoint5) :value t)
                )
            :post nil
            :is-event t
            :point *point0*
        )
    )
    
    (setq gets-sandy-event
        (make-occurrence 
            :signature '(rover::rover-gets-sandy rover::rover1 rover::waypoint5)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::sandy :args '(rover::waypoint5) :value t)
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value nil)
                )
            :post
                (list 
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value t)
                )
            :is-event t
            :point (nth 3 *point-list*)
        )
    )
    
    (consider-possible-events (first *execution-history*) (third *execution-history*))
    
    (test-refine-by-adding-occurrence
        (make-explanation
            :prior-events
                (list
                    move-event1
                    detect-event1
                    leave-event1
                )
        )
        (make-inconsistency
            :cnd (make-cnd :type 'rover::sand-covered :args '(rover::rover1) :value t)
            :prior-occ (car *execution-history*) 
            :next-occ (car (last *execution-history*))
        )
        (list
            (make-explanation
                :prior-events
                    (list
                        move-event1
                        detect-event1
                        leave-event1
                    )
                :new-events
                    (list
                        gets-sandy-event
                    )
                :event-removals
                    (list
                        detect-event1
                    )
            )
        )
    )

    (setq *execution-history*
        (append *execution-history*
            (list
                (make-occurrence 
                    :signature '(rover::navigate-detected rover::rover1 rover::east)
                    :pre
                        (list 
                            (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                            (make-fact :type 'rover::energy :args '(rover::rover1) :value 92)
                            (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                        )
                    :post
                        (list 
                            (make-fact :type 'rover::energy :args '(rover::rover1) :value 84)
                            (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::east) :value t)
                        )
                    :is-action t
                    :point (nth 5 *point-list*)
                )
                (make-occurrence 
                    :signature '(o2)
                    :pre 
                        (list 
                            (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                            (make-fact :type 'rover::energy :args '(rover::rover1) :value 100)
                            (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                            (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::east)
                            (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                            (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
                            (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
                            (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint6) :value t)
                        )
                    :post nil
                    :is-observation t
                    :point (nth 9 *point-list*)
                )
            )
        )
    )
    
    (setq init-windy-event
        (make-occurrence 
            :signature '(assume-initial-value windy waypoint6 t)
            :pre
                (list 
                    (make-fact :type 'rover::windy :args '(rover::waypoint6) :value t)
                )
            :post nil
            :is-event t
            :point *point0*
        )
    )
    
    (setq cleaned-event
        (make-occurrence 
            :signature '(rover::rover-cleaned rover::rover1 rover::waypoint6)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint6)
                    (make-fact :type 'rover::sandy :args '(rover::waypoint6) :value nil)
                    (make-fact :type 'rover::windy :args '(rover::waypoint6) :value t)
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value t)
                )
            :post
                (list 
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value nil)
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint6) :value t)
                    (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                )
            :is-event t
            :point (nth 7 *point-list*)
        )
    )
    
    (setq confused-event
        (make-occurrence 
            :signature '(rover::compass-confused rover::rover1 rover::waypoint6 rover::north rover::east)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint6)
                    (make-fact :type 'rover::windy :args '(rover::waypoint6) :value t)
                    (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                    (make-fact :type 'rover::next-compass-point :args '(rover::north rover::east) :value t)
                    (make-fact :type 'rover::compass-moved :args '(rover::rover1) :value nil)
                )
            :post
                (list 
                    (make-fact :type 'rover::compass-moved :args '(rover::rover1) :value t)
                    (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::east)
                )
            :is-event t
            :point (nth 7 *point-list*)
        )
    )
    
    (setq move-event2
        (make-occurrence 
            :signature '(rover::rover-moves rover::rover1 rover::east rover::waypoint5 rover::north rover::east rover::waypoint6)
            :pre
                (list 
                    (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::east) :value t)
                    (make-fact :type 'rover::blocked :args '(rover::rover1) :value nil)
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                    (make-fact :type 'rover::real-direction :args '(rover::east rover::north rover::east) :value t)
                    (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint5 rover::waypoint6 rover::east) :value t)
                    (make-fact :type 'rover::visible :args '(rover::waypoint5 rover::waypoint6) :value t)
                )
            :post
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint6)
                    (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::east) :value nil)
                    (make-fact :type 'rover::compass-moved :args '(rover::rover1) :value nil)
                )
            :is-event t
            :point (nth 6 *point-list*)
        )
    )
    
    (setq detect-event2
        (make-occurrence 
            :signature '(rover::location-detected rover::rover1 rover::waypoint6)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint6)
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint6) :value nil)
                    (make-fact :type 'rover::sandy :args '(rover::waypoint6) :value nil)
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value nil)
                )
            :post
                (list 
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint6) :value t)
                    (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                )
            :is-event t
            :point (nth 7 *point-list*)
        )
    )
    
    (setq leave-event2
        (make-occurrence 
            :signature '(rover::location-left rover::rover1 rover::waypoint5)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint6)
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint5) :value t)
                )
            :post
                (list 
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint5) :value nil)
                )
            :is-event t
            :point (nth 7 *point-list*)
        )
    )
    
    ; (test-construct-explanation
        ; nil
        ; (make-explanation
            ; :prior-events nil
            ; :new-events
                ; (list
                    ; move-event1 
                    ; leave-event1
                    ; detect-event1
                    ; move-event2
                    ; leave-event2
                    ; detect-event2
                ; )
        ; )
    ; )
; 
    ; 
    ; (test-construct-explanation
        ; (list init-sandy-event)
        ; (make-explanation
            ; :prior-events (list init-sandy-event)
            ; :new-events
                ; (list
                    ; move-event1 
                    ; leave-event1
                    ; gets-sandy-event
                    ; move-event2
                ; )
        ; )
    ; )
    ; 
    ; (test-construct-explanation
        ; (list init-sandy-event init-windy-event)
        ; (make-explanation
            ; :prior-events (list init-sandy-event init-windy-event)
            ; :new-events
                ; (list
                    ; move-event1 
                    ; leave-event1
                    ; gets-sandy-event
                    ; move-event2
                    ; cleaned-event
                    ; confused-event
                ; )
        ; )
    ; )
    (consider-possible-events (third *execution-history*) (fifth *execution-history*))

    (test-refine-by-adding-occurrence
        (make-explanation
            :new-events
                (list
                    move-event1
                    leave-event1
                    gets-sandy-event
                    move-event2
                    leave-event2
                )
        )
        (make-inconsistency
            :cnd (make-cnd :type 'rover::detects-location :args '(rover::rover1 rover::waypoint6) :value t)
            :prior-occ (third *execution-history*) 
            :next-occ (fifth *execution-history*)
        )
        (list
            (make-explanation
                :new-events
                    (list
                        move-event1
                        leave-event1
                        gets-sandy-event
                        move-event2
                        leave-event2
                        cleaned-event
                    )
            )
        )
    )
    (test-refine-by-adding-occurrence
        (make-explanation
            :new-events
                (list
                    move-event1
                    leave-event1
                    gets-sandy-event
                    move-event2
                    leave-event2
                    cleaned-event
                )
        )
        (make-inconsistency
            :cnd (make-cnd :type 'rover::compass-points :args '(rover::rover1) :value 'rover::east)
            :prior-occ (third *execution-history*)
            :next-occ (fifth *execution-history*)
        )
        (list
            (make-explanation
                :new-events
                    (list
                        move-event1
                        leave-event1
                        gets-sandy-event
                        move-event2
                        leave-event2
                        cleaned-event
                        confused-event
                    )
            )
        )
    )
)

(defun test-refine-by-removing-occurrence-cases ()
   (setq *hidden-types* '(rover::sandy rover::windy))
   (setq *ev-models* 
        (apply #'append
            (mapcar #'build-event-model (envmodel-events *last-envmodel*))
        )
    )
    (setq *execution-history*
        (list
            (make-occurrence 
                :signature '(o0)
                :pre 
                    (list 
                        (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint11)
                        (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::energy :args '(rover::rover1) :value 100)
                        (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                        (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                        (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
                        (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
                        (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint11) :value t)
                    )
                :post nil
                :is-observation t
                :point *point0*
            )
            (make-occurrence 
                :signature '(rover::navigate-detected rover::rover1 rover::north)
                :pre
                    (list 
                        (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::energy :args '(rover::rover1) :value 100)
                        (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                    )
                :post
                    (list 
                        (make-fact :type 'rover::energy :args '(rover::rover1) :value 92)
                        (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value t)
                    )
                :is-action t
                :point (nth 1 *point-list*)
            )
            (make-occurrence 
                :signature '(o1)
                :pre
                    (list 
                        (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                        (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::energy :args '(rover::rover1) :value 92)
                        (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                        (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                        (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
                        (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
                        (make-fact :type 'rover::next-compass-point :args '(rover::north rover::east) :value t)
                        (make-fact :type 'rover::real-direction :args '(rover::east rover::north rover::east) :value t)
                        (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint5 rover::waypoint6 rover::east) :value t)
                        (make-fact :type 'rover::visible :args '(rover::waypoint5 rover::waypoint6) :value t)
                        (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value t)
                    )
                :post nil
                :is-observation t
                :point (nth 4 *point-list*)
            )
        )
    )
    
    (setq move-event1
        (make-occurrence 
            :signature '(rover::rover-moves rover::rover1 rover::north rover::waypoint11 rover::north rover::north rover::waypoint5)
            :pre
                (list 
                    (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value t)
                    (make-fact :type 'rover::blocked :args '(rover::rover1) :value nil)
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint11)
                    (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                    (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                    (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
                    (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
                )
            :post
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value nil)
                    (make-fact :type 'rover::compass-moved :args '(rover::rover1) :value nil)
                )
            :is-event t
            :point (nth 2 *point-list*)
        )
    )
    
    (setq detect-event1
        (make-occurrence 
            :signature '(rover::location-detected rover::rover1 rover::waypoint5)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint5) :value nil)
                    (make-fact :type 'rover::sandy :args '(rover::waypoint5) :value nil)
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value nil)
                )
            :post
                (list 
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint5) :value t)
                    (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                )
            :is-event t
            :point (nth 3 *point-list*)
        )
    )
    
    (setq leave-event1
        (make-occurrence 
            :signature '(rover::location-left rover::rover1 rover::waypoint11)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint11) :value t)
                )
            :post
                (list 
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint11) :value nil)
                )
            :is-event t
            :point (nth 3 *point-list*)
        )
    )
    

    (setq init-sandy-event
        (make-occurrence 
            :signature '(assume-initial-value sandy waypoint5 t)
            :pre
                (list 
                    (make-fact :type 'rover::sandy :args '(rover::waypoint5) :value t)
                )
            :post nil
            :is-event t
            :point *point0*
        )
    )
    
    (setq gets-sandy-event
        (make-occurrence 
            :signature '(rover::rover-gets-sandy rover::rover1 rover::waypoint5)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::sandy :args '(rover::waypoint5) :value t)
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value nil)
                )
            :post
                (list 
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value t)
                )
            :is-event t
            :point (nth 3 *point-list*)
        )
    )
    
    (test-refine-by-removing-occurrence
        (make-explanation
            :new-events
                (list
                    move-event1
                    detect-event1
                    leave-event1
                )
        )
        leave-event1
        nil
    )
    (test-refine-by-removing-occurrence
        (make-explanation
            :prior-events
                (list
                    move-event1
                    detect-event1
                    leave-event1
                )
        )
        leave-event1
        (list
            (make-explanation
                :prior-events
                    (list
                        move-event1
                        detect-event1
                        leave-event1
                    )
                :event-removals
                    (list
                        leave-event1
                    )
            )
        )
    )
)

(defun test-refine-by-hypothesizing-initial-value-cases ()
   (setq *hidden-types* '(rover::sandy rover::windy))
   (setq *ev-models* 
        (apply #'append
            (mapcar #'build-event-model (envmodel-events *last-envmodel*))
        )
    )
    (setq *execution-history*
        (list
            (make-occurrence 
                :signature '(o0)
                :pre 
                    (list 
                        (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint11)
                        (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::energy :args '(rover::rover1) :value 100)
                        (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                        (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                        (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
                        (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
                        (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint11) :value t)
                    )
                :post nil
                :is-observation t
                :point *point0*
            )
            (make-occurrence 
                :signature '(rover::navigate-detected rover::rover1 rover::north)
                :pre
                    (list 
                        (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::energy :args '(rover::rover1) :value 100)
                        (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                    )
                :post
                    (list 
                        (make-fact :type 'rover::energy :args '(rover::rover1) :value 92)
                        (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value t)
                    )
                :is-action t
                :point (nth 1 *point-list*)
            )
            (make-occurrence 
                :signature '(o1)
                :pre
                    (list 
                        (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                        (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::energy :args '(rover::rover1) :value 92)
                        (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                        (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                        (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
                        (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
                        (make-fact :type 'rover::next-compass-point :args '(rover::north rover::east) :value t)
                        (make-fact :type 'rover::real-direction :args '(rover::east rover::north rover::east) :value t)
                        (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint5 rover::waypoint6 rover::east) :value t)
                        (make-fact :type 'rover::visible :args '(rover::waypoint5 rover::waypoint6) :value t)
                        (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value t)
                    )
                :post nil
                :is-observation t
                :point (nth 4 *point-list*)
            )
        )
    )
    
    (setq move-event1
        (make-occurrence 
            :signature '(rover::rover-moves rover::rover1 rover::north rover::waypoint11 rover::north rover::north rover::waypoint5)
            :pre
                (list 
                    (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value t)
                    (make-fact :type 'rover::blocked :args '(rover::rover1) :value nil)
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint11)
                    (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                    (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                    (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
                    (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
                )
            :post
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value nil)
                    (make-fact :type 'rover::compass-moved :args '(rover::rover1) :value nil)
                )
            :is-event t
            :point (nth 2 *point-list*)
        )
    )
    
    (setq detect-event1
        (make-occurrence 
            :signature '(rover::location-detected rover::rover1 rover::waypoint5)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint5) :value nil)
                    (make-fact :type 'rover::sandy :args '(rover::waypoint5) :value nil)
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value nil)
                )
            :post
                (list 
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint5) :value t)
                    (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                )
            :is-event t
            :point (nth 3 *point-list*)
        )
    )
    
    (setq leave-event1
        (make-occurrence 
            :signature '(rover::location-left rover::rover1 rover::waypoint11)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint11) :value t)
                )
            :post
                (list 
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint11) :value nil)
                )
            :is-event t
            :point (nth 3 *point-list*)
        )
    )
    

    (setq init-sandy-event
        (make-occurrence 
            :signature '(assume-initial-value sandy waypoint5 t)
            :pre
                (list 
                    (make-fact :type 'rover::sandy :args '(rover::waypoint5) :value t)
                )
            :post nil
            :is-event t
            :point *point0*
        )
    )
    
    (setq gets-sandy-event
        (make-occurrence 
            :signature '(rover::rover-gets-sandy rover::rover1 rover::waypoint5)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::sandy :args '(rover::waypoint5) :value t)
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value nil)
                )
            :post
                (list 
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value t)
                )
            :is-event t
            :point (nth 3 *point-list*)
        )
    )
    
    (setq init-sandy-event
        (make-occurrence 
            :signature 
                '(assume-initial-value (rover::sandy rover::waypoint5) t)
            :pre
                (list 
                    (make-fact :type 'rover::sandy :args '(rover::waypoint5) :value t)
                )
            :post nil
            :is-event t
            :point *point0*
        )
    )

    (test-refine-by-hypothesizing-initial-value
        (make-explanation
            :prior-events
                (list
                    move-event1
                    detect-event1
                    gets-sandy-event
                )
        )
        (make-inconsistency
            :cnd (make-cnd :type 'rover::sandy :args '(rover::waypoint5) :value t)
            :prior-occ nil 
            :next-occ gets-sandy-event
        )
        (list
            (make-explanation
                :prior-events
                    (list
                        move-event1
                        detect-event1
                        gets-sandy-event
                    )
                :new-events
                    (list
                        init-sandy-event
                    )
            )
        )
    )
)

(defun test-find-extra-events (ex exp-ex &aux ret)
    (setq ret-ex (find-extra-events ex))
    (when (eq ret-ex 'fail)
        (when (not (eq exp-ex 'fail))
            (error "Find-extra-events failed unexpectedly.")
        )
        (return-from test-find-extra-events nil)
    )
    (when (eq exp-ex 'fail)
        (format t "~&Returned Explanation: ~%~A" ret-ex) 
        (error "Expected a failure, but explanation was returned.")
    )
    (test-same-explanation exp-ex ret-ex)
)
    

(defun test-find-extra-events-cases ()
   (setq *hidden-types* '(rover::sandy rover::windy))
   (setq *ev-models* 
        (apply #'append
            (mapcar #'build-event-model (envmodel-events *last-envmodel*))
        )
    )
    (setq *execution-history*
        (list
            (make-occurrence 
                :signature '(o0)
                :pre 
                    (list 
                        (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint11)
                        (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::energy :args '(rover::rover1) :value 100)
                        (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                        (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                        (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
                        (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
                        (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint11) :value t)
                    )
                :post nil
                :is-observation t
                :point *point0*
            )
            (make-occurrence 
                :signature '(rover::navigate-detected rover::rover1 rover::north)
                :pre
                    (list 
                        (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::energy :args '(rover::rover1) :value 100)
                        (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                    )
                :post
                    (list 
                        (make-fact :type 'rover::energy :args '(rover::rover1) :value 92)
                        (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value t)
                    )
                :is-action t
                :point (nth 1 *point-list*)
            )
            (make-occurrence 
                :signature '(o1)
                :pre
                    (list 
                        (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                        (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::energy :args '(rover::rover1) :value 92)
                        (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                        (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                        (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
                        (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
                        (make-fact :type 'rover::next-compass-point :args '(rover::north rover::east) :value t)
                        (make-fact :type 'rover::real-direction :args '(rover::east rover::north rover::east) :value t)
                        (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint5 rover::waypoint6 rover::east) :value t)
                        (make-fact :type 'rover::visible :args '(rover::waypoint5 rover::waypoint6) :value t)
                        (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value t)
                    )
                :post nil
                :is-observation t
                :point (nth 4 *point-list*)
            )
        )
    )
    
    (setq move-event1
        (make-occurrence 
            :signature '(rover::rover-moves rover::rover1 rover::north rover::waypoint11 rover::north rover::north rover::waypoint5)
            :pre
                (list 
                    (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value t)
                    (make-fact :type 'rover::blocked :args '(rover::rover1) :value nil)
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint11)
                    (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                    (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                    (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
                    (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
                )
            :post
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value nil)
                    (make-fact :type 'rover::compass-moved :args '(rover::rover1) :value nil)
                )
            :is-event t
            :point (nth 2 *point-list*)
        )
    )
    
    (setq detect-event1
        (make-occurrence 
            :signature '(rover::location-detected rover::rover1 rover::waypoint5)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint5) :value nil)
                    (make-fact :type 'rover::sandy :args '(rover::waypoint5) :value nil)
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value nil)
                )
            :post
                (list 
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint5) :value t)
                    (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                )
            :is-event t
            :point (nth 3 *point-list*)
        )
    )
    
    (setq leave-event1
        (make-occurrence 
            :signature '(rover::location-left rover::rover1 rover::waypoint11)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint11) :value t)
                )
            :post
                (list 
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint11) :value nil)
                )
            :is-event t
            :point (nth 3 *point-list*)
        )
    )
    

    (setq init-sandy-event
        (make-occurrence 
            :signature '(assume-initial-value sandy waypoint5 t)
            :pre
                (list 
                    (make-fact :type 'rover::sandy :args '(rover::waypoint5) :value t)
                )
            :post nil
            :is-event t
            :point *point0*
        )
    )
    
    (setq gets-sandy-event
        (make-occurrence 
            :signature '(rover::rover-gets-sandy rover::rover1 rover::waypoint5)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::sandy :args '(rover::waypoint5) :value t)
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value nil)
                )
            :post
                (list 
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value t)
                )
            :is-event t
            :point (nth 3 *point-list*)
        )
    )
    
    (setq *execution-history*
        (append *execution-history*
            (list
                (make-occurrence 
                    :signature '(rover::navigate-detected rover::rover1 rover::east)
                    :pre
                        (list 
                            (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                            (make-fact :type 'rover::energy :args '(rover::rover1) :value 92)
                            (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                        )
                    :post
                        (list 
                            (make-fact :type 'rover::energy :args '(rover::rover1) :value 84)
                            (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::east) :value t)
                        )
                    :is-action t
                    :point (nth 5 *point-list*)
                )
                (make-occurrence 
                    :signature '(o2)
                    :pre 
                        (list 
                            (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                            (make-fact :type 'rover::energy :args '(rover::rover1) :value 100)
                            (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                            (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::east)
                            (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                            (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
                            (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
                            (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint6) :value t)
                        )
                    :post nil
                    :is-observation t
                    :point (nth 9 *point-list*)
                )
            )
        )
    )
    
    (setq init-windy-event
        (make-occurrence 
            :signature '(assume-initial-value windy waypoint6 t)
            :pre
                (list 
                    (make-fact :type 'rover::windy :args '(rover::waypoint6) :value t)
                )
            :post nil
            :is-event t
            :point *point0*
        )
    )
    
    (setq cleaned-event
        (make-occurrence 
            :signature '(rover::rover-cleaned rover::rover1 rover::waypoint6)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint6)
                    (make-fact :type 'rover::sandy :args '(rover::waypoint6) :value nil)
                    (make-fact :type 'rover::windy :args '(rover::waypoint6) :value t)
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value t)
                )
            :post
                (list 
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value nil)
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint6) :value t)
                    (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                )
            :is-event t
            :point (nth 7 *point-list*)
        )
    )
    
    (setq confused-event
        (make-occurrence 
            :signature '(rover::compass-confused rover::rover1 rover::waypoint6 rover::north rover::east)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint6)
                    (make-fact :type 'rover::windy :args '(rover::waypoint6) :value t)
                    (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                    (make-fact :type 'rover::next-compass-point :args '(rover::north rover::east) :value t)
                    (make-fact :type 'rover::compass-moved :args '(rover::rover1) :value nil)
                )
            :post
                (list 
                    (make-fact :type 'rover::compass-moved :args '(rover::rover1) :value t)
                    (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::east)
                )
            :is-event t
            :point (nth 7 *point-list*)
        )
    )
    
    (setq move-event2
        (make-occurrence 
            :signature '(rover::rover-moves rover::rover1 rover::east rover::waypoint5 rover::north rover::east rover::waypoint6)
            :pre
                (list 
                    (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::east) :value t)
                    (make-fact :type 'rover::blocked :args '(rover::rover1) :value nil)
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                    (make-fact :type 'rover::real-direction :args '(rover::east rover::north rover::east) :value t)
                    (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint5 rover::waypoint6 rover::east) :value t)
                    (make-fact :type 'rover::visible :args '(rover::waypoint5 rover::waypoint6) :value t)
                )
            :post
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint6)
                    (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::east) :value nil)
                    (make-fact :type 'rover::compass-moved :args '(rover::rover1) :value nil)
                )
            :is-event t
            :point (nth 6 *point-list*)
        )
    )
    
    (setq detect-event2
        (make-occurrence 
            :signature '(rover::location-detected rover::rover1 rover::waypoint6)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint6)
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint6) :value nil)
                    (make-fact :type 'rover::sandy :args '(rover::waypoint6) :value nil)
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value nil)
                )
            :post
                (list 
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint6) :value t)
                    (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                )
            :is-event t
            :point (nth 7 *point-list*)
        )
    )
    
    (setq leave-event2
        (make-occurrence 
            :signature '(rover::location-left rover::rover1 rover::waypoint5)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint6)
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint5) :value t)
                )
            :post
                (list 
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint5) :value nil)
                )
            :is-event t
            :point (nth 7 *point-list*)
        )
    )
    
    (test-find-extra-events
        (make-explanation
            :new-events
                (list
                    init-sandy-event
                    move-event1 
                    leave-event1
                    move-event2
                )
        )
        (make-explanation
            :new-events
                (list
                    init-sandy-event
                    move-event1 
                    leave-event1
                    gets-sandy-event
                    move-event2
                )
        )
    )
    
    (test-find-extra-events
        (make-explanation
            :new-events
                (list
                    init-sandy-event
                    init-windy-event
                    move-event1 
                    leave-event1
                    gets-sandy-event
                    move-event2
                    cleaned-event
                )
        )
        (make-explanation
            :new-events
                (list
                    init-sandy-event
                    init-windy-event
                    move-event1 
                    leave-event1
                    gets-sandy-event
                    move-event2
                    cleaned-event
                    confused-event
                )
        )
    )

    (test-find-extra-events
        (make-explanation
            :prior-events
                (list
                    init-sandy-event
                    move-event1 
                    leave-event1
                    detect-event1
                    gets-sandy-event
                    move-event2
                )
            :new-events
                (list
                    init-windy-event
                    cleaned-event
                )
            :event-removals
                (list
                    detect-event1
                )
        )
        (make-explanation
            :prior-events
                (list
                    init-sandy-event
                    move-event1 
                    leave-event1
                    detect-event1
                    gets-sandy-event
                    move-event2
                )
            :new-events
                (list
                    init-windy-event
                    cleaned-event
                    confused-event
                )
            :event-removals
                (list
                    detect-event1
                )
        )
    )
    (test-find-extra-events
        (make-explanation
            :prior-events
                (list
                    init-sandy-event
                    move-event1 
                    leave-event1
                    detect-event1
                    gets-sandy-event
                    move-event2
                    confused-event
                )
            :new-events
                (list
                    init-windy-event
                    cleaned-event
                )
            :event-removals
                (list
                    detect-event1
                    confused-event
                )
        )
        'fail
    )
)

(defun test-discover-history (ex exp-exps &aux ret-exps)
    (setq ret-exps (discover-history ex))
    (test-set-match 
        ret-exps 
        exp-exps
        "explanations" 
        "discover-history"
        :test #'explanation-equal
    )
)


(defun test-discover-history-cases ()
   (setq *hidden-types* '(rover::sandy rover::windy))
   (setq *ev-models* 
        (apply #'append
            (mapcar #'build-event-model (envmodel-events *last-envmodel*))
        )
    )
   
    (setq ob0 
        (make-occurrence 
            :signature '(o0)
            :pre 
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint11)
                    (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                    (make-fact :type 'rover::energy :args '(rover::rover1) :value 100)
                    (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                    (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                    (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                    (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
                    (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint11) :value t)
                    (make-fact :type 'rover::next-compass-point :args '(rover::north rover::east) :value t)
                    (make-fact :type 'rover::real-direction :args '(rover::east rover::north rover::east) :value t)
                    (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint5 rover::waypoint6 rover::east) :value t)
                    (make-fact :type 'rover::visible :args '(rover::waypoint5 rover::waypoint6) :value t)
                )
            :post nil
            :is-observation t
            :point *point0*
        )
    )
    
    (setq ac1 
        (make-occurrence 
            :signature '(rover::navigate-detected rover::rover1 rover::north)
            :pre
                (list 
                    (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                    (make-fact :type 'rover::energy :args '(rover::rover1) :value 100)
                    (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                )
            :post
                (list 
                    (make-fact :type 'rover::energy :args '(rover::rover1) :value 92)
                    (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value t)
                )
            :is-action t
            :point (nth 1 *point-list*)
        )
    )
    
    (setq ob1
        (make-occurrence 
            :signature '(o1)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                    (make-fact :type 'rover::energy :args '(rover::rover1) :value 92)
;                    (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                    (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                    (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                    (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
                    (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
                    (make-fact :type 'rover::next-compass-point :args '(rover::north rover::east) :value t)
                    (make-fact :type 'rover::real-direction :args '(rover::east rover::north rover::east) :value t)
                    (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint5 rover::waypoint6 rover::east) :value t)
                    (make-fact :type 'rover::visible :args '(rover::waypoint5 rover::waypoint6) :value t)
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value t)
                )
            :post nil
            :is-observation t
            :point (nth 4 *point-list*)
        )
    )
        
    
    (setq *execution-history* (list ob0 ac1 ob1))
    
    (setq move-event1
        (make-occurrence 
            :signature '(rover::rover-moves rover::rover1 rover::north rover::waypoint11 rover::north rover::north rover::waypoint5)
            :pre
                (list 
                    (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value t)
                    (make-fact :type 'rover::blocked :args '(rover::rover1) :value nil)
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint11)
                    (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                    (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                    (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
                    (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
                )
            :post
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::north) :value nil)
                    (make-fact :type 'rover::compass-moved :args '(rover::rover1) :value nil)
                )
            :is-event t
            :point (nth 2 *point-list*)
        )
    )
    
    (setq detect-event1
        (make-occurrence 
            :signature '(rover::location-detected rover::rover1 rover::waypoint5)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint5) :value nil)
                    (make-fact :type 'rover::sandy :args '(rover::waypoint5) :value nil)
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value nil)
                )
            :post
                (list 
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint5) :value t)
                    (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                )
            :is-event t
            :point (nth 3 *point-list*)
        )
    )
    
    (setq leave-event1
        (make-occurrence 
            :signature '(rover::location-left rover::rover1 rover::waypoint11)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint11) :value t)
                )
            :post
                (list 
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint11) :value nil)
                )
            :is-event t
            :point (nth 3 *point-list*)
        )
    )
    

    (setq init-sandy-event
        (make-occurrence 
            :signature '(assume-initial-value (rover::sandy rover::waypoint5) t)
            :pre
                (list 
                    (make-fact :type 'rover::sandy :args '(rover::waypoint5) :value t)
                )
            :post nil
            :is-event t
            :point *point0*
        )
    )
    
    (setq gets-sandy-event
        (make-occurrence 
            :signature '(rover::rover-gets-sandy rover::rover1 rover::waypoint5)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::sandy :args '(rover::waypoint5) :value t)
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value nil)
                )
            :post
                (list 
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value t)
                )
            :is-event t
            :point (nth 3 *point-list*)
        )
    )
    
    (test-discover-history 
        (make-explanation
            :new-events
                (list
                    init-sandy-event
                    move-event1
                    leave-event1
                    gets-sandy-event
                )
        )
        (list
            (make-explanation
                :new-events
                    (list
                        init-sandy-event
                        move-event1
                        leave-event1
                        gets-sandy-event
                    )
            )
        )
    )
    
    (test-discover-history 
        (make-explanation
            :new-events
                (list
                    move-event1
                    leave-event1
                    gets-sandy-event
                )
        )
        (list
            (make-explanation
                :new-events
                    (list
                        init-sandy-event
                        move-event1
                        leave-event1
                        gets-sandy-event
                    )
            )
        )
    )
    (test-discover-history 
        (make-explanation
            :prior-events
                (list
                    move-event1
                    leave-event1
                    detect-event1
                )
        )
        (list
            (make-explanation
                :prior-events
                    (list
                        move-event1
                        leave-event1
                        detect-event1
                    )
                :new-events
                    (list 
                        init-sandy-event
                        gets-sandy-event
                    )
                :event-removals
                    (list
                        detect-event1
                    )
            )
        )
    )
    (test-discover-history 
        (make-explanation)
        (list
            (make-explanation
                :new-events
                    (list
                        init-sandy-event
                        move-event1
                        leave-event1
                        gets-sandy-event
                    )
            )
        )
    )
    
    
    
    (setq *execution-history*
        (append *execution-history*
            (list
                (make-occurrence 
                    :signature '(rover::navigate-detected rover::rover1 rover::east)
                    :pre
                        (list 
                            (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                            (make-fact :type 'rover::energy :args '(rover::rover1) :value 92)
                            (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                        )
                    :post
                        (list 
                            (make-fact :type 'rover::energy :args '(rover::rover1) :value 84)
                            (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::east) :value t)
                        )
                    :is-action t
                    :point (nth 5 *point-list*)
                )
                (make-occurrence 
                    :signature '(o2)
                    :pre 
                        (list 
                            (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                            (make-fact :type 'rover::energy :args '(rover::rover1) :value 84)
                            (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                            (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::east)
                            (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                            (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
                            (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
                            (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint6) :value t)
                        )
                    :post nil
                    :is-observation t
                    :point (nth 9 *point-list*)
                )
            )
        )
    )
    
    (setq init-windy-event
        (make-occurrence 
            :signature '(assume-initial-value (rover::windy rover::waypoint6) t)
            :pre
                (list 
                    (make-fact :type 'rover::windy :args '(rover::waypoint6) :value t)
                )
            :post nil
            :is-event t
            :point *point0*
        )
    )
    
    (setq cleaned-event
        (make-occurrence 
            :signature '(rover::rover-cleaned rover::rover1 rover::waypoint6)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint6)
                    (make-fact :type 'rover::sandy :args '(rover::waypoint6) :value nil)
                    (make-fact :type 'rover::windy :args '(rover::waypoint6) :value t)
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value t)
                )
            :post
                (list 
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value nil)
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint6) :value t)
                    (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                )
            :is-event t
            :point (nth 7 *point-list*)
        )
    )
    
    (setq confused-event
        (make-occurrence 
            :signature '(rover::compass-confused rover::rover1 rover::waypoint6 rover::north rover::east)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint6)
                    (make-fact :type 'rover::windy :args '(rover::waypoint6) :value t)
                    (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                    (make-fact :type 'rover::next-compass-point :args '(rover::north rover::east) :value t)
                    (make-fact :type 'rover::compass-moved :args '(rover::rover1) :value nil)
                )
            :post
                (list 
                    (make-fact :type 'rover::compass-moved :args '(rover::rover1) :value t)
                    (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::east)
                )
            :is-event t
            :point (nth 7 *point-list*)
        )
    )
    
    (setq move-event2
        (make-occurrence 
            :signature '(rover::rover-moves rover::rover1 rover::east rover::waypoint5 rover::north rover::east rover::waypoint6)
            :pre
                (list 
                    (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::east) :value t)
                    (make-fact :type 'rover::blocked :args '(rover::rover1) :value nil)
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint5)
                    (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                    (make-fact :type 'rover::real-direction :args '(rover::east rover::north rover::east) :value t)
                    (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint5 rover::waypoint6 rover::east) :value t)
                    (make-fact :type 'rover::visible :args '(rover::waypoint5 rover::waypoint6) :value t)
                )
            :post
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint6)
                    (make-fact :type 'rover::attempting-move :args '(rover::rover1 rover::east) :value nil)
                    (make-fact :type 'rover::compass-moved :args '(rover::rover1) :value nil)
                )
            :is-event t
            :point (nth 6 *point-list*)
        )
    )
    
    (setq detect-event2
        (make-occurrence 
            :signature '(rover::location-detected rover::rover1 rover::waypoint6)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint6)
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint6) :value nil)
                    (make-fact :type 'rover::sandy :args '(rover::waypoint6) :value nil)
                    (make-fact :type 'rover::sand-covered :args '(rover::rover1) :value nil)
                )
            :post
                (list 
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint6) :value t)
                    (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                )
            :is-event t
            :point (nth 7 *point-list*)
        )
    )
    
    (setq leave-event2
        (make-occurrence 
            :signature '(rover::location-left rover::rover1 rover::waypoint5)
            :pre
                (list 
                    (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint6)
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint5) :value t)
                )
            :post
                (list 
                    (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint5) :value nil)
                )
            :is-event t
            :point (nth 7 *point-list*)
        )
    )
    
    (test-discover-history 
        (make-explanation
            :prior-events
                (list
                    init-sandy-event
                    move-event1
                    leave-event1
                    gets-sandy-event
                    move-event2
                )
        )
        (list
            (make-explanation
                :prior-events
                    (list
                        init-sandy-event
                        move-event1
                        leave-event1
                        gets-sandy-event
                        move-event2
                    )
                :new-events
                    (list 
                        init-windy-event
                        cleaned-event
                        confused-event
                    )
            )
        )
    )

    (test-discover-history 
        (make-explanation
            :prior-events
                (list
                    init-sandy-event
                    move-event1
                    leave-event1
                    gets-sandy-event
                )
        )
        (list
            (make-explanation
                :prior-events
                    (list
                        init-sandy-event
                        move-event1
                        leave-event1
                        gets-sandy-event
                    )
                :new-events
                    (list 
                        move-event2
                        init-windy-event
                        cleaned-event
                        confused-event
                    )
            )
        )
    )

    (test-discover-history 
        (make-explanation)
        (list
            (make-explanation
                :prior-events
                    (list
                    )
                :new-events
                    (list 
                        init-sandy-event
                        move-event1
                        leave-event1
                        gets-sandy-event
                        move-event2
                        init-windy-event
                        cleaned-event
                        confused-event
                    )
            )
        )
    )
    
)

(defun test-convert-pddl-action (action pt exp-occs &aux ret-occs)
    (setq ret-occs (convert-pddl-action action pt))
    (test-set-match 
        ret-occs exp-occs 
        "actions" "convert-pddl-action" 
        :test #'occurrence-equal
    )
)
    
(defun test-convert-pddl-action-cases ()
     (setq *action-models* 
         (apply #'append
             (mapcar #'build-event-model (envmodel-actions *last-envmodel*))
         )
     )
     (setq *execution-history*
         (list
            (make-occurrence 
                :signature '(o0)
                :pre 
                    (list 
                        (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint11)
                        (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::energy :args '(rover::rover1) :value 100)
                        (make-fact :type 'rover::recharges :args '(rover::rover1) :value 1)
;                        (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                        (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                        (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
                        (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
;                        (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint11) :value t)
                    )
                :post nil
                :is-observation t
                :point *point0*
            )
        )
    )
    
    (test-convert-pddl-action
        '(rover::navigate rover::rover1 rover::north)
        (nth 1 *point-list*)
        (list
            (make-occurrence
                :is-action t
                :point (nth 1 *point-list*)
                :signature '(rover::navigate rover::rover1 rover::north)
                :pre 
                    (list
                        (make-fact
                            :type 'rover::available
                            :args '(rover::rover1)
                            :value t
                        )
                        (make-fact
                            :type 'rover::energy
                            :args '(rover::rover1)
                            :value 100
                        )
                        (make-fact
                            :type 'rover::known-location
                            :args '(rover::rover1)
                            :value nil
                        )
                    )
                :post 
                    (list
                        (make-fact
                            :type 'rover::attempting-move 
                            :args '(rover::rover1 rover::north)
                            :value t
                        )
                        (make-fact
                            :type 'rover::energy 
                            :args '(rover::rover1)
                            :value 92
                        )
                    )
            )
        )
    )
    
     (setq *execution-history*
         (list
            (make-occurrence 
                :signature '(o0)
                :pre 
                    (list 
                        (make-fact :type 'rover::at :args '(rover::rover1) :value 'rover::waypoint11)
                        (make-fact :type 'rover::in_sun :args '(rover::waypoint11) :value t)
                        (make-fact :type 'rover::available :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::energy :args '(rover::rover1) :value 50)
                        (make-fact :type 'rover::recharges :args '(rover::rover1) :value 1)
;                        (make-fact :type 'rover::known-location :args '(rover::rover1) :value t)
                        (make-fact :type 'rover::compass-points :args '(rover::rover1) :value 'rover::north)
                        (make-fact :type 'rover::real-direction :args '(rover::north rover::north rover::north) :value t)
                        (make-fact :type 'rover::can_traverse :args '(rover::rover1 rover::waypoint11 rover::waypoint5 rover::north) :value t)
                        (make-fact :type 'rover::visible :args '(rover::waypoint11 rover::waypoint5) :value t)
;                        (make-fact :type 'rover::detects-location :args '(rover::rover1 rover::waypoint11) :value t)
                    )
                :post nil
                :is-observation t
                :point *point0*
            )
        )
    )
    

     (test-convert-pddl-action
        '(rover::recharge rover::rover1 rover::waypoint11)
        (nth 1 *point-list*)
        (list
            (make-occurrence
                :is-action t
                :point (nth 1 *point-list*)
                :signature '(rover::recharge rover::rover1 rover::waypoint11)
                :pre 
                    (list
                        (make-fact
                            :type 'rover::at
                            :args '(rover::rover1)
                            :value 'rover::waypoint11
                        )
                        (make-fact
                            :type 'rover::in_sun
                            :args '(rover::waypoint11)
                            :value t
                        )
                        (make-fact
                            :type 'rover::energy
                            :args '(rover::rover1)
                            :value 50
                        )
                        (make-fact 
                            :type 'rover::recharges
                            :args nil
                            :value 1
                        )
                    )
                :post 
                    (list
                        (make-fact
                            :type 'rover::energy
                            :args '(rover::rover1)
                            :value 100
                        )
                        (make-fact 
                            :type 'rover::recharges
                            :args nil
                            :value 2
                        )
                    )
            )
        )
    )
    
)

(defun get-hash-keys (hash &aux (keys nil))
    (maphash #'(lambda (key val) (push key keys)) hash)
    keys
)
    

(defun test-add-fact-to-hash (fact ret-hash facts-in-chain mutex-pairs exp-hash)
    (add-fact-to-hash fact ret-hash facts-in-chain mutex-pairs)
    (let* ((same t)
           (exp-keys (get-hash-keys exp-hash))
           (ret-keys (get-hash-keys ret-hash))
           (unexp-keys (set-difference ret-keys exp-keys :test #'equal-fact))
           (unfound-keys (set-difference exp-keys ret-keys :test #'equal-fact))
         )
        (when unfound-keys
            (format t "~%Keys missing from return value of add-fact-to-hash:")
            (qp unfound-keys)
            (setq same nil)
        )
        (when unexp-keys
            (format t "~%Unexpected keys found in return value of add-fact-to-hash:")
            (qp unexp-keys)
            (setq same nil)
        )
        (dolist (key exp-keys)
            (let ((exp-vals (gethash key exp-hash))
                  (ret-vals (gethash key ret-hash))
                  unexp-vals unfound-vals
                  (cycle-present nil)
                 )
                (if (equal exp-vals '(true))
                    ;then
                    (progn 
                        (setq cycle-present t)
                        (when (not (equal ret-vals '(true)))
                            (format t "~%Expected cycle on ~A not found." key)
                            (setq same nil)
                        )
                    )
                    ;else
                    (when (equal ret-vals '(true))
                        (format t "~%Found unexpected cycle on ~A." key)
                        (setq cycle-present t)
                        (setq same nil)
                    )
                )
                (when (not cycle-present)
                    (setq unexp-vals (set-difference ret-vals exp-vals :test #'equal-fact)
                          unfound-vals (set-difference exp-vals ret-vals :test #'equal-fact))
                    
                    (when (not (member key unfound-keys))
                        (when unfound-vals
                            (format t "~%Values missing from ~A list: ~A" key unfound-vals)
                            (setq same nil)
                        )
                        (when unexp-vals
                            (format t "~%Unexpected values found in ~A list: ~A" key unexp-vals)
                            (setq same nil)
                        )
                    )
                )
            )
        )
        (when (not same)
            (error "Hash map returned from add-fact-to-hash does not match expectations.")
        )
    )
)

(defun make-hash-from-lists (lists &key (test #'equal))
    (let ((table (make-hash-table :test test)))
        (dolist (lst lists)
            (setf (gethash (car lst) table) (cdr lst))       
        )
        table
    )
)

(defun test-add-fact-to-hash-cases ()
    (setq atrue (make-fact :type 'a :args nil :value t))
    (setq afalse (make-fact :type 'a :args nil :value nil))
    (setq btrue (make-fact :type 'b :args nil :value t))
    (setq bfalse (make-fact :type 'b :args nil :value nil))
    (setq ctrue (make-fact :type 'c :args nil :value t))
    (setq cfalse (make-fact :type 'c :args nil :value nil))
    (setq dtrue (make-fact :type 'd :args nil :value t))
    (setq dfalse (make-fact :type 'd :args nil :value nil))
    (setq etrue (make-fact :type 'e :args nil :value t))
    (setq efalse (make-fact :type 'e :args nil :value nil))
    (setq ftrue (make-fact :type 'f :args nil :value t))
    (setq ffalse (make-fact :type 'f :args nil :value nil))
    (setq g1 (make-fact :type 'g :args nil :value 1))
    (setq g2 (make-fact :type 'g :args nil :value 2))
    (setq gnil (make-fact :type 'g :args nil :value nil))
    (test-add-fact-to-hash
        atrue
        (make-hash-table :test #'equalp)
        nil
        (list
            (cons atrue btrue)
        )
        (make-hash-from-lists
            (list 
                (list atrue btrue)
                (list bfalse)
            )
            :test #'equalp
        )
    )
    (test-add-fact-to-hash
        atrue
        (make-hash-table :test #'equalp)
        nil
        (list
            (cons atrue btrue)
            (cons bfalse ctrue)
        )
        (make-hash-from-lists
            (list 
                (list atrue btrue ctrue)
                (list bfalse ctrue)
                (list cfalse)
            )
            :test #'equalp
        )
    )
    (test-add-fact-to-hash
        btrue
        (make-hash-table :test #'equalp)
        nil
        (list
            (cons atrue btrue)
            (cons bfalse ctrue)
        )
        (make-hash-from-lists
            (list 
                (list btrue atrue)
                (list afalse)
            )
            :test #'equalp
        )
    )
    (test-add-fact-to-hash
        atrue
        (make-hash-table :test #'equalp)
        nil
        (list
            (cons atrue btrue)
            (cons bfalse atrue)
        )
        (make-hash-from-lists
            (list 
                (list atrue 'true)
                (list bfalse atrue)
                (list afalse)
                (list btrue atrue)
            )
            :test #'equalp
        )
    )
    (test-add-fact-to-hash
        atrue
        (make-hash-table :test #'equalp)
        nil
        (list
            (cons bfalse ftrue)
            (cons ctrue efalse)
            (cons btrue cfalse)
            (cons atrue btrue)
            (cons bfalse ctrue)
            (cons ftrue ctrue)
        )
        (make-hash-from-lists
            (list 
                (list atrue btrue ctrue ftrue)
                (list bfalse ctrue ftrue btrue)
                (list cfalse btrue)
                (list ffalse)
            )
            :test #'equalp
        )
    )
    (test-add-fact-to-hash
        atrue
        (make-hash-table :test #'equalp)
        nil
        (list
            (cons bfalse ftrue)
            (cons ctrue efalse)
            (cons atrue btrue)
            (cons bfalse ctrue)
            (cons ftrue ctrue)
        )
        (make-hash-from-lists
            (list 
                (list atrue btrue ctrue ftrue)
                (list bfalse ctrue ftrue)
                (list ffalse)
                (list cfalse)
            )
            :test #'equalp
        )
    )
    (test-add-fact-to-hash
        atrue
        (make-hash-table :test #'equalp)
        nil
        (list
            (cons atrue g1)
            (cons g2 btrue)
            (cons gnil ctrue)
        )
        (make-hash-from-lists
            (list 
                (list atrue g1)
            )
            :test #'equalp
        )
    )
)

(defun test-suite ()
    (test-contradict-cases)
    (test-contradictory-event-cases)
    (test-fix-contradiction-cases)
    (test-advance-cases)
    (test-make-inconsistencies-cases)
    (test-find-following-intervals-cases)
    (test-find-inconsistencies-cases)
    (test-bind-args-cases)
    (test-bind-value-cases)
    (test-unify-cases)
    (test-unify-set-cases)
    (test-process-pddl-to-condition-cases)
    (test-process-pddl-effect-to-preconditions-cases)
    (test-process-pddl-effect-to-postcondition-cases)
    (test-bind-condition-to-fact-cases)
    (test-enumerate-occs-cases)
    (test-construct-explanation-cases)
    (test-add-occ-to-layer-cases)
    (test-eliminate-cases)
    (test-find-possible-events-cases)
    (test-point-earlier-cases)
    (test-compatible-cause-exists-cases)
    (test-decide-occ-mutex-cases)
    (test-decide-fact-mutex-cases)
    (test-mutex-facts-in-cases)
    (test-refine-by-adding-occurrence-cases)
    (test-refine-by-removing-occurrence-cases)
    (test-refine-by-hypothesizing-initial-value-cases)
    (test-find-extra-events-cases)
    (test-discover-history-cases)
)
