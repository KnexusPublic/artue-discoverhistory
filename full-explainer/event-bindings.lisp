 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: April 8, 2011
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file manipulates bindings and conditions to create appropriate
 ;;;; facts.
 
(in-package :artue)

;; Given a condition (from an event model) and a binding list, creates a fact
;; that should be true when the condition holds. If more than one fact is 
;; possible, creates a list of all matching facts.
(defun bind-condition-to-fact (expgen cnd blist facts &aux ret-fact val (ret-facts :fail))
    (setq ret-fact
        (make-fact
            :type (cnd-type cnd)
            :args (mapcar+ #'map-binding (cnd-args cnd) blist)
        )
    )
    (setq val (map-binding (cnd-value cnd) blist))
    (when (member (cnd-type cnd) '(number)) ;;Only needed in unify
        (return-from bind-condition-to-fact (values (list ret-fact) blist))
    )
    (when (and (cnd-inequality cnd) (not (var-symbol-p val)))
        (let ((safe-facts nil))
            (when (listp facts)
                (setq ret-facts (remove ret-fact facts :test-not #'same-map))
            )
            (when (fact-set-p facts)
                (setq ret-facts (find-all-in-fact-set ret-fact facts :test #'same-map))
            )
            (when (null ret-facts)
                (let ((default (get-default-value-cnd expgen cnd))) 
                    (if (or (is-hidden expgen cnd) (eq default 'fail))
                        ;then
                        (return-from bind-condition-to-fact
                            (values (list (bind-var-symbol-inequality expgen cnd val ret-fact)) blist)
                        )
                        ;else
                        (return-from bind-condition-to-fact
                            (values (list (contradictory-fact ret-fact default)) blist)
                        )
                    )
                )
            )
            (when (eq ret-facts :fail)
                (error "Facts field in bind-condition-to-fact is strange: ~A" facts)
            )
            (dolist (new-fact ret-facts)
                (when (var-symbol-p (fact-value new-fact))
                    (multiple-value-bind (nvar nblist)
                        (restrict-inequality 
                            (cnd-inequality cnd) 
                            val
                            (fact-value new-fact) 
                            blist
                        )
                      
                        (when nvar
                            (push
                                (make-fact 
                                    :type (fact-type ret-fact)
                                    :args (fact-args ret-fact)
                                    :value nvar 
                                )
                                safe-facts
                            )
                            (setq blist nblist)
                        )
                    )
                )
                (when (and (not (var-symbol-p (fact-value new-fact)))
                           (check-inequality (cnd-inequality cnd) new-fact val)
                      )
                    (push new-fact safe-facts)
                )
            )
            (return-from bind-condition-to-fact (values (remove nil safe-facts) blist))
        )
    )
    (when (and (cnd-inequality cnd) (var-symbol-p val) (not (is-hidden expgen cnd)))
        (when (listp facts)
            (setq ret-facts (remove ret-fact facts :test-not #'same-map))
        )
        (when (fact-set-p facts)
            (setq ret-facts (find-all-in-fact-set ret-fact facts :test #'same-map))
        )
        ;; MCM This is unused ca. 10/22, not sure what it was meant for.
        ; (when (and (null ret-facts) (has-default-value expgen (cnd-to-fact cnd)))
            ; (setq ret-facts 
                ; (list 
                    ; (make-fact 
                        ; :type (cnd-type cnd) 
                        ; :args (cnd-args cnd)
                        ; :value (get-default-value-cnd expgen cnd)
                    ; )
                ; )
            ; )
            ; ;(error "Why does a non-hidden fact not have a value?")
        ; )
        ;(when (cdr ret-facts)
        ;    (error "Why multiple values for a non-hidden fact?")
        ;)
        (let (new-val (new-facts nil))
            (dolist (ret-fact ret-facts)
                (cond
                    ((var-symbol-p (fact-value ret-fact))
                        (setq new-val
                            (var-symbol-intersection val (fact-value ret-fact))
                        )
                        (push (contradictory-fact ret-fact new-val) new-facts)
                    )
                    ((restrict-inequality 
                        (cnd-inequality (reverse-inequality cnd)) 
                        (fact-value ret-fact) 
                        val blist
                     )
                        (push ret-fact new-facts)
                    )
                )
            )
            (return-from bind-condition-to-fact (values new-facts blist))
        )
    )
    ;; MCM (7/13) Not sure why function didn't already handle this case, but 
    ;; added fallthrough so that an inequality condition never gets handled 
    ;; like an equality.
    (when (cnd-inequality cnd)
        (multiple-value-bind (new-val new-blist)
                (restrict-inequality 
                    (cnd-inequality cnd)
                    val 
                    (new-var-sym 'real)
                    blist
                )
            (return-from bind-condition-to-fact 
                (values (list (contradictory-fact ret-fact new-val)) new-blist)
            )
        )
    )
    (values (list (contradictory-fact ret-fact val)) blist)
)


(defun bind-inconsistency (inc blist)
    (make-inconsistency
        :cnd (bind-cnd (inconsistency-cnd inc) blist)
        :prior-occ (if (inconsistency-prior-occ inc) (bind-occurrence (inconsistency-prior-occ inc) blist) nil)
        :next-occ (bind-occurrence (inconsistency-next-occ inc) blist)
    )
)

(defun bind-fact (fact blist)
    (when (and (eq (fact-type fact) 'unbound-var) 
               (member (var-symbol-name (car (fact-args fact))) blist :key #'car)
          )
        (return-from bind-fact nil)
    )
    
    (make-fact 
        :type (fact-type fact)
        :args (mapcar+ #'var-name-sub (fact-args fact) blist)
        :value (var-name-sub (fact-value fact) blist)
    )
)

(defun bind-cnd (cnd blist)
    (make-cnd 
        :inequality (cnd-inequality cnd)
        :type (cnd-type cnd)
        :args (mapcar+ #'var-name-sub (cnd-args cnd) blist)
        :value (var-name-sub (cnd-value cnd) blist)
    )
)

(defun var-name-eqn-sub (eqn blist)
    (if (listp eqn)
        ;then
        (mapcar+ #'var-name-eqn-sub eqn blist)
        ;else
        (var-name-sub eqn blist)
    )
)

(defun var-name-sub (var blist &aux val-pair)
    (when (listp var)
        (return-from var-name-sub (mapcar+ #'var-name-sub var blist))
    )
    (when (var-symbol-p var)
        (setq val-pair (assoc (var-symbol-name var) blist))
        (when val-pair
            (return-from var-name-sub (cdr val-pair))
        )
    )
    var
)

(defun bind-var-symbol-inequality (expgen cnd val ret-fact &aux (type-name 'int))
    (when (not (numberp val)) 
        (setq type-name
            (predicate-value-type 
                (predicate-named 
                    (cnd-type cnd) 
                    (get-predicates (get-last-envmodel expgen))
                )
            )
        )
    )
    (contradictory-fact ret-fact
        (restrict-inequality 
            (cnd-inequality cnd) 
            val 
            (new-var-sym type-name) nil
        )
    )        
)

(defun bind-condition-to-fact-set (expgen cnd blist fact-set &aux flist)
    (setq flist (get-type-facts (cnd-type cnd) fact-set))
    (bind-condition-to-fact expgen cnd blist flist)
)

(defun map-binding (sym blist)
    (cond
        ((var-symbol-p sym) 
            (if (assoc sym blist) 
                ;then 
                (cdr (assoc sym blist))
                ;else
                sym
            )
        )
        ((is-variable sym)
            (cdr (assoc sym blist))
        )
        (t sym)
    )
)

(defun combine-bindings (blist1 blist2)
    (dolist (pair blist1)
        (when (not (equal (assoc (car pair) blist2) pair))
            (return-from combine-bindings 'fail)
        )
    )
    (union blist1 blist2 :test #'equal)
)

(defun bind-fact-if-necessary (fact blist)
    ;(format t "New bind-occ")
    (if (variables-in-fact fact)
        ;then
        (bind-fact fact blist)
        ;else
        fact
    )
)

(defun bind-cnd-if-necessary (fact blist)
    ;(format t "New bind-occ")
    (if (variables-in-cnd fact)
        ;then
        (bind-cnd fact blist)
        ;else
        fact
    )
)



(defun bind-occ-if-necessary (occ vsyms blist)
    ;(format t "New bind-occ")
    (if (intersection-exists (get-var-symbols occ) vsyms)
        ;then
        (bind-occurrence occ blist)
        ;else
        occ
    )
)

(defun bind-inconsistency-if-necessary (obj blist)
    (if (intersection-exists (mapcar #'var-symbol-name (get-var-symbols obj))
                             (mapcar #'car blist)
        )
        ;then
        (bind-inconsistency obj blist)
        ;else
        obj
    )
)

(defun bind-anything (obj blist)
    (cond
        ((listp obj) (mapcar+ #'bind-anything obj blist))
        ((var-symbol-p obj) (var-name-sub obj blist))
        ((occurrence-p obj) (bind-occurrence obj blist))
        ((fact-p obj) (bind-fact-if-necessary obj blist))
        ((cnd-p obj) (bind-cnd-if-necessary obj blist))
        ((inconsistency-p obj) (bind-inconsistency-if-necessary obj blist))
        ((symbolp obj) obj)
        ((numberp obj) obj)
        ((stringp obj) obj)
        (t (error "Can't bind this object."))
    )
)

    


(defun bind-occurrence (occ blist &aux new-occ)
    (setq new-occ
        (make-occurrence
            :signature (mapcar+ #'bind-anything (occurrence-signature occ) blist)
            :point (occurrence-point occ)
            :performer (var-name-sub (occurrence-performer occ) blist)
            :interval (occurrence-interval occ)
            :pre (remove nil (mapcar+ #'bind-fact (occurrence-pre occ) blist))
            :post (remove nil (mapcar+ #'bind-fact (occurrence-post occ) blist))
            :constraints (mapcar+ #'bind-cnd (occurrence-constraints occ) blist)
            :is-action (occurrence-is-action occ)
            :is-event (occurrence-is-event occ)
            :is-observation (occurrence-is-observation occ)
            :is-assumption (occurrence-is-assumption occ)
        )
    )
)

(defun rebind-occurrence (occ blist &aux names name-blist new-sym)
    (setq blist (substitute-names blist))
    (setq name-blist (pairlis (mapcar #'car blist) (mapcar #'var-symbol-name (mapcar #'cdr blist))))
    (setq names (mapcar #'car blist))
    (dolist (vsym (get-var-symbols occ))
        (when (not (member (var-symbol-name vsym) names))
            (let ((new-sym (dupe-var-sym vsym)))
                (setf (var-symbol-temporary new-sym) nil)
                (push (cons (var-symbol-name vsym) new-sym) blist)
                (push (cons (var-symbol-name vsym) (var-symbol-name new-sym)) name-blist)
            )
        )
    )
    (setq occ (bind-occurrence occ blist))
    (dolist (vsym (get-var-symbols occ))
        (when (var-symbol-eqn vsym)
            (setf (var-symbol-eqn vsym) (sublis name-blist (var-symbol-eqn vsym)))
        )
    ) 
    occ
)

(defun bind-explanation (expgen ex inc blist)
    ;(format t "~&Blist:  ~A" blist)
    (setq blist (remove-identities blist))
    (when (null blist)
        (return-from bind-explanation ex)
    )
    (dolist (pair blist)
        (when (not (allowed-value (car pair) (cdr pair)))
            (return-from bind-explanation nil)
        )
        (when (var-symbol-p (cdr pair))
            (setf (var-symbol-temporary (cdr pair)) nil)
        )
    )
    
    (let ((new-ex (copy-explanation ex)) (changed-pt nil)  
          (bound-occs nil) (vsyms nil))
        
        (setq blist (update-equations ex blist))
        (setq vsyms (mapcar #'car blist))
        ; (when (not (subsetp vsyms all-var-syms))
            ; (error "Where did extra var-symbols come from?")
        ; )
                
        (dolist (occ (append (get-events ex) (get-execution-history expgen)))
            (when (intersection-exists (get-var-symbols occ) vsyms)
                (let ((new-changed-pt (point-prior expgen (occ-first-point occ))))
                    (when (null new-changed-pt) (setq new-changed-pt (get-first-point expgen)))
                    (when (or (null changed-pt) (point-earlier new-changed-pt changed-pt))
                        (setq changed-pt new-changed-pt)
                    )
                )
                (when (not (member occ (get-execution-history expgen)))
                    (push occ bound-occs)
                )
            )
        )
        (let* ((unbound-prior-events 
                 (remove-if-not
                    #'(lambda (occ) (intersection-exists (get-var-symbols occ) vsyms))
                    (explanation-prior-events ex)
                 )
               )
               (bound-prior-events 
                   (mapcar+ #'bind-occurrence 
                       unbound-prior-events
                       (substitute-names blist)
                   ) 
               )
               event-subs
              )
            (setf (explanation-new-events new-ex) 
                (append
                    (mapcar+ #'bind-occ-if-necessary
                        (explanation-new-events new-ex)
                        vsyms
                        (substitute-names blist)
                    )
                    bound-prior-events
                )
            )
            (setq event-subs
                (pairlis
                    (append (explanation-new-events ex) unbound-prior-events)
                    (explanation-new-events new-ex)
                )
            )
            (setf (explanation-occurrence-precedence new-ex)
                (sublis event-subs
                    (explanation-occurrence-precedence new-ex)
                )
            )
            ;(clrhash (explanation-cached-precedence-map new-ex))
            (setf (explanation-last-added-event new-ex)
                (sublis event-subs
                    (explanation-last-added-event new-ex)
                )
            )
            (setf (explanation-last-added-assumption new-ex)
                (sublis event-subs
                    (explanation-last-added-assumption new-ex)
                )
            )
            (setf (explanation-prior-events new-ex) 
                (set-difference 
                    (explanation-prior-events new-ex) 
                    unbound-prior-events
                )
            )
            (when (null (explanation-last-added-assumption new-ex))
                (let (new-assumptions)
                    (setq new-assumptions 
                        (set-difference 
                            (remove-if-not #'occurrence-is-assumption (explanation-new-events new-ex))
                            (remove-if-not #'occurrence-is-assumption (explanation-new-events ex))
                        )
                    )
                    (when new-assumptions
                        (setf (explanation-last-added-assumption new-ex)
                            (car new-assumptions)
                        )
                    )
                )
            )
        )
        
        (setf (explanation-bindings new-ex)
            (append (explanation-bindings ex) (substitute-names blist))
        )
        (incf (explanation-changes new-ex))
        (dolist (ev (explanation-new-events new-ex))
            (when (eq (occurrence-performer ev) (get-self expgen))
                (return-from bind-explanation nil)
            )
            (when (member ev (explanation-event-removals new-ex) :test #'occurrence-equal)
                (return-from bind-explanation nil)
            )
        )
        ;(let ((remaining-events (explanation-new-events new-ex))
        ;      (most-specific-set nil)
        ;      cur-ev
        ;     )
        ;    (loop while remaining-events do
        ;        (setq cur-ev (pop remaining-events))
        ;        (when (notany
        ;                #'(lambda (specific-ev) 
        ;                    (occurrence-as-specific-as specific-ev cur-ev)
        ;                  )
        ;                (append remaining-events most-specific-set)
        ;              )
        ;            (push cur-ev most-specific-set)
        ;        )
        ;    )
        ;    (setf (explanation-new-events new-ex) most-specific-set)
        ;)

        (dolist (ev (explanation-new-events new-ex))
            (when (or (null changed-pt) (point-earlier (occ-first-point ev) changed-pt))
                (setq changed-pt (occ-first-point ev))
            )
        )
        
        (when changed-pt
            (trim-cache expgen new-ex changed-pt)
        )
        
        ;; Old code was intended to recompute inconsistencies only, in order to 
        ;; save time when changes are made that do not affect downstream facts. 
        ;; Unfortunately, fact-occs must still be recomputed, or new 
        ;; inconsistencies may refer to old, partially unbound occurrences. 
        ;; Therefore, this code is probably a dead-end.
        ; (let (inc-pt new-cache)
            ; (setq new-cache (copy-alist (explanation-cached-inconsistencies ex)))
            ; (dolist (occ bound-occs)
                ; (loop while (not (point-earlier (occ-last-point (inconsistency-next-occ inc)) inc-pt)) do
                    ; (when (or (null changed-pt) (point-earlier inc-pt changed-pt))
                        ; (setf (cdr (assoc inc-pt new-cache))
                            ; (make-inconsistencies 
                                ; expgen
                                ; (remove-if-not 
                                    ; #'(lambda (occ) (point-is-relevant-to inc-pt occ)) 
                                    ; (get-events new-ex)
                                ; )
                                ; (facts-at expgen new-ex (point-prior expgen inc-pt))
                                ; (fact-occs-at expgen new-ex (point-prior expgen inc-pt))
                                ; (make-explanation) ;;Empty explanation used to ignore caches
                                ; inc-pt
                            ; )
                        ; )
                    ; )
                    ; (setq inc-pt (point-next expgen inc-pt))
                ; )
            ; )
            ; (setf (explanation-cached-inconsistencies new-ex) new-cache)
        ; )
        new-ex
    )
)

(defun update-equations (ex blist &aux old-eqn-syms next-old-eqn-syms new-nsyms (new-vsym-found t))
    (setq next-old-eqn-syms 
        (set-difference 
            (remove-if-not #'var-symbol-eqn (get-var-symbols (get-events ex)))
            (mapcar #'car blist)
        )
    )
    (setq new-nsyms (mapcar #'var-symbol-name (mapcar #'car blist)))
    (loop while new-vsym-found do
        (setq new-vsym-found nil)
        (setq old-eqn-syms next-old-eqn-syms)
        (dolist (vsym old-eqn-syms)
            (let ((sym-overlap (intersection new-nsyms (get-num-symbols-in-equations (var-symbol-eqn vsym)))))
                (when sym-overlap
                    (dolist (temp-eqn (sublis (append (explanation-bindings ex) (substitute-names blist)) (var-symbol-eqn vsym)))
                        (when (not (var-or-num-symbols-in temp-eqn))
                            ;then
                            (progn
                                (setq new-vsym-found t)
                                (push (cons vsym (eval temp-eqn)) blist)
                                (setq next-old-eqn-syms (remove vsym next-old-eqn-syms))
                            )
                            ; ;else
                            ; (dolist (nsym sym-overlap)
                                ; (pushnew (cons nsym (cdr (assoc nsym (substitute-names blist)))) (equation-relevant-bindings ex))
                            ; )
                        )
                    )
                )
            )
        )
    )
    blist
)

;; This check looks for specious bindings that only say that one var-symbol 
;; could be bound to another of the same type, without adding any conditions.
;; If both var-symbols have the same name, then the var-symbol has been changed 
;; to include additional conditions, which is a meaningful binding.
(defun only-binds-var-symbols-to-var-symbols (blist)
    (and blist
        (every
            #'(lambda (pair) 
                (and (var-symbol-p (car pair)) (var-symbol-p (cdr pair))
                     (not (eq (var-symbol-name (car pair))
                              (var-symbol-name (cdr pair))
                          )
                     )
                )
              )
            blist
        )
    )
)

;; This check determines whether a binding list includes var-symbol to 
;; var-symbol mappings, which sometimes indicate a false unification.
(defun binds-var-symbols-to-var-symbols (blist)
    (some
        #'(lambda (pair) 
            (and (var-symbol-p (car pair)) (var-symbol-p (cdr pair)))
          )
        blist
    )
)

