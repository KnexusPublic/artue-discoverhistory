 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: April 8, 2011
 ;;;; Project Name: Explanation for Planning
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file provides the data backend for performing event-based 
 ;;;; explanation.
 
(when (not (find-package :artue))
    (setq *explainer-version* 2)
    (load "rover/rovertest")
)
 
(in-package :artue)

(defun find-action-consequences (action-model facts &aux names bindings action-occ ob1 ob2 evs start-facts)
    (setq names (mapcar #'first (event-model-arg-types action-model)))
    (initialize-points 0)
    (setq bindings 
        (mapcar #'(lambda (name) 
                    (cons name (gensym (subseq (symbol-name name) 1)))
                  )
            names
        )
    )
    (setq ob1
        (make-occurrence 
            :signature '(observation)
            :is-observation t 
            :point (next-point expgen)
        )
    )
    (setq action-occ
        (car (enumerate-occs expgen action-model nil (next-point expgen) :bindings bindings :assume-fn #'cnd-p))
    )
    (setf (occurrence-pre ob1) (append facts (occurrence-pre action-occ)))
    (setf (occurrence-is-action action-occ) t)
    (setf (occurrence-is-event action-occ) nil)
    (setq ob2
        (make-occurrence 
            :signature '(observation)
            :is-observation t 
            :point nil
        )
    )
    (setf (get-execution-history expgen) 
        (list 
            ob1
            action-occ
            ob2
        )
    )
    
    (setf (get-hidden-types expgen) (mapcar #'predicate-name (get-predicates *last-envmodel*)))
    (setf (get-variable-events expgen) t)
    (consider-possible-events expgen ob1 ob2)
    (setf (get-variable-events expgen) nil)
    
    (setq evs (gethash ob1 *possible-event-map*))
    (setq start-facts 
        (append
            (set-difference 
                (occurrence-pre action-occ) 
                (occurrence-post action-occ) 
                :test #'contradicts
            )
            (occurrence-post action-occ) 
        )
    )
    (mapcar+ #'convert-to-when
        (find-conditionals 
            start-facts 
            nil
            evs 
            3
        )
        start-facts
        bindings
    )
)

    

(defun bind-to-var (sym)
    (cons sym 
        (gentemp (concatenate 'string "?" (symbol-name (get-return-type sym)))
            (symbol-package sym)
        )
    )
)

(defun convert-to-when (pddl-condition start-facts bindings &aux output-condition free-vars)
    (setq precs
        (mapcar #'convert-discover-fact
            (remove-if #'type-is-is
                (set-difference (car pddl-condition) start-facts :test #'equal-fact)
                :key #'fact-type
            )
        )
    )
    (setq effs
        (mapcar #'convert-discover-fact
            (set-difference (second pddl-condition) (append (car pddl-condition) start-facts) :test #'equal-fact)
        )
    )
    (setq free-vars (find-in-tree (list precs effs) #'var-symbol-p))

    (setq precs 
        (append precs 
            (remove nil (mapcar #'(lambda (x) (var-symbol-adds-to x)) free-vars))
        )
    )
    
    (setq bindings
        (append
            (mapcar #'(lambda (binding) (cons (cdr binding) (car binding))) bindings)
            (mapcar #'convert-var free-vars)
        )
    )
;    (break "In convert-to-when")
        
    (setq output-condition 
        (cons 'when (sublis bindings (list (cons 'and precs) (cons 'and effs))))
    )
    (setq free-vars
        (mapcar #'bind-to-var
            (remove-duplicates 
                (find-in-tree output-condition #'get-return-type)
            )
        )
    )
    (if free-vars
        ;then
        (list 'forall 
            (make-pddl-param-list 
                (mapcar #'cdr free-vars) 
                (mapcar #'(lambda (x) (list (cdr x) (get-return-type (car x)))) free-vars)
            )
            (sublis free-vars output-condition)
        )
        ;else
        output-condition
    )
)

(defun convert-var (free-var)
    (cons free-var 
        (gentemp 
            (concatenate 'string 
                "?"
                (symbol-name (get-return-type free-var))
            )
            :artue
        )
    )
)

#|
(defun find-conditionals (cur-truth evs)
    (mapcar+ #'find-conditional evs cur-truth evs)
)

(defun find-conditional (ev cur-truth evs &aux prior-evs)
    (setq prior-evs (remove ev evs :test-not #'occurrence-later))
    (when (null prior-evs)
        (list (set-difference (occurrence-pre ev) cur-truth)
              (set-difference (occurrence-post ev) cur-truth)
        )
    )
)
|#
(defun find-conditionals (conditions effects evs pt)
    (find-conditionals-current
        conditions
        effects
        nil
        nil
        (remove pt evs :key #'occurrence-point :test-not #'eq)
        (remove pt evs :key #'occurrence-point :test #'eq)
        pt
    )
)

(defun find-conditionals-current (conditions effects known-evs known-not-evs cur-evs remaining-evs pt)
    (if (null cur-evs)
        ;then
        (find-conditionals-next
            conditions
            effects
            known-evs
            known-not-evs
            remaining-evs 
            (1+ pt)
        )
        ;else
        (append
            (if (check-compatible-known 
                    (car cur-evs) known-evs known-not-evs conditions effects
                )
                ;then
                (find-conditionals-current 
                    conditions
                    effects
                    (cons (car cur-evs) known-evs)
                    known-not-evs
                    (cdr cur-evs)
                    remaining-evs
                    pt
                )
                ;else
                nil
            )
            (if (check-compatible-not-known 
                    (car cur-evs) known-evs known-not-evs conditions effects
                )
                ;then
                (find-conditionals-current 
                    conditions
                    effects
                    known-evs
                    (cons (car cur-evs) known-not-evs)
                    (cdr cur-evs)
                    remaining-evs
                    pt
                )
                ;else
                nil
            )
        )
    )
)

(defun find-conditionals-next (conditions effects known-evs known-not-evs
                               remaining-evs pt &aux cur-truth new-conditions
                               new-effects)
;    (break "begin find-conditionals-next")
    (setq cur-truth 
        (append effects (set-difference conditions effects :test #'contradicts))
    )
    (setq new-conditions (union-fact-sets (mapcar #'occurrence-pre known-evs)))
    (setq new-effects 
        (append
            (union-fact-sets (mapcar #'occurrence-post known-evs))
            (mapcar #'(lambda (x) (make-fact :type 'occurs :args (occurrence-signature x) :value t)) known-evs)
        )
    )
        
    (setq conditions
        (append
            conditions
            (set-difference new-conditions cur-truth :test #'equal-fact)
        )
    )
    (setq effects
        (append 
            (set-difference effects new-effects :test #'contradicts)
            new-effects
        )
    )
    (if (null remaining-evs)
        ;then
        (list (list conditions effects))
        ;else
        (find-conditionals conditions effects remaining-evs pt)
    )
)

(defun check-compatible-known (ev known-evs known-not-evs effects conditions)
    (setq cur-truth (append effects (set-difference conditions effects :test #'contradicts)))
    (when (or (find-contradictory-events ev known-evs #'occurrence-pre)
              (find-contradictory-events ev known-evs #'occurrence-post)
          )
        (return-from check-compatible-known nil)
    )
    (when (intersection (occurrence-pre ev) cur-truth :test #'contradicts)
        (return-from check-compatible-known nil)
    )
    (setq new-conditions (union-fact-sets (mapcar #'occurrence-pre known-evs)))
    (setq new-effects (union-fact-sets (mapcar #'occurrence-post known-evs)))
    (dolist (ev known-not-evs)
        (when (subsetp (occurrence-pre ev) (append cur-truth new-conditions) :test #'equal-fact)
            (return-from check-compatible-known nil)
        )
    )
    t
)    

(defun check-compatible-not-known (ev known-evs known-not-evs effects conditions)
    (setq cur-truth (append effects (set-difference conditions effects :test #'contradicts)))
    (setq new-conditions (union-fact-sets (mapcar #'occurrence-pre known-evs)))
    (setq new-effects (union-fact-sets (mapcar #'occurrence-post known-evs)))
    (dolist (ev known-not-evs)
        (when (subsetp (occurrence-pre ev) (append cur-truth new-conditions) :test #'equal-fact)
            (return-from check-compatible-not-known nil)
        )
    )
    t
)

(defun make-background-facts (prob &aux objs facts)
;    (setq objs (make-type-declarations (pddl-problem-objects prob))) 
    (setq facts (convert-problem-objects (pddl-problem-objects prob)))
;    (dolist (pred (get-function-predicates *last-envmodel*))
;        (setq facts (append (get-func-facts pred objs) facts))
;    )
    (append facts 
        (remove-if #'(lambda (fact) (is-transient expgen fact))  
            (mapcar #'convert-problem-fact (pddl-problem-init prob))
        )
    )
)

(defun get-func-facts (pred objs &aux facts)
    (setq facts nil)
    (dolist (vlist (get-var-lists (predicate-arg-types pred) objs nil))
        (push 
            (make-fact 
                :type (predicate-name pred) 
                :args vlist 
                :value (new-var-sym)
            )
            facts
        )
    )
    facts
)

(defun get-var-lists (arg-types objs args &aux cur-objs vlists)
    (when (null arg-types)
        (return-from get-var-lists (list (reverse args)))
    )
    (setq cur-objs (remove (cadar arg-types) objs :key #'second :test-not #'eq))
    (setq vlists nil)
    (dolist (obj cur-objs)
        (setq vlists (append (get-var-lists (cdr arg-types) objs (cons (car obj) args)) vlists))
    )
    vlists
)

(defun get-background-facts (&aux (facts nil) (sym nil))
    (dolist (typ (envmodel-types *last-envmodel*))
        ;;If needed, correct to use new var-syms.
;        (setq sym (gentemp (symbol-name typ) (symbol-package typ)))
;        (set-return-type sym typ)
        (push 
            (make-fact
                :type (get-is-type typ)
                :args
                    (list sym)
                :value t
            )
            facts
        )
    )
    facts
)

(defun build-possibilities (action-model facts &aux ob1 ob2 action-occ)
    (setq names (mapcar #'first (event-model-arg-types action-model)))
    (initialize-points 20)
    (setq bindings 
        (mapcar #'(lambda (name) 
                    (cons name (gensym (subseq (symbol-name name) 1)))
                  )
            names
        )
    )
    (setq ob1
        (make-occurrence 
            :signature '(observation)
            :is-observation t 
            :point 1
        )
    )
    (setq ob2
        (make-occurrence 
            :signature '(observation)
            :is-observation t 
            :point 10
        )
    )
    (setq action-occ
        (car (enumerate-occs expgen action-model nil 11 :bindings bindings :assume-fn #'cnd-p))
    )
    (setf (occurrence-pre ob1) (remove-if-not #'fact-is-is (occurrence-pre action-occ)))
    (setf (occurrence-pre ob2) (append facts (remove-if #'fact-is-is (occurrence-pre action-occ))))
    (setf (occurrence-is-action action-occ) t)
    (setf (occurrence-is-event action-occ) nil)
    (setf (get-execution-history expgen) 
        (list 
            ob1
            (make-occurrence
                :signature '(dummy-action)
                :is-action t
                :point 2
            )
            ob2
            action-occ
            (make-occurrence 
                :signature '(observation)
                :is-observation t 
                :point 20
            )
        )
    )
    (setq start-facts 
        (append
            (set-difference 
                (occurrence-pre action-occ) 
                (occurrence-post action-occ) 
                :test #'contradicts
            )
            (occurrence-post action-occ) 
        )
    )
    (setq start-facts 
        (append
            (set-difference 
                (append
                    (occurrence-pre ob1) 
                    (occurrence-pre ob2) 
                )
                (occurrence-post action-occ) 
                :test #'contradicts
            )
            (occurrence-post action-occ) 
        )
    )
    (setf (get-variable-events expgen) t)
    (mapcar+ #'convert-to-when
        (determine-next-occs 
            nil  ;; no assumptions
            start-facts
            (occurrence-post action-occ) 
            (remove nil (mapcar #'contradictory-fact (occurrence-post action-occ))) 
            nil  ;; no occs
            12
        )
        start-facts
        bindings
    )
)

(defun determine-next-occs (assumptions facts new-facts last-facts occs pt &aux blist new-occs new-rules)
    (setf (occurrence-pre (car *execution-history*)) (append assumptions (remove-if-not #'fact-is-is facts)))
    (let* ((new-ex (construct-explanation nil))
           (real-evs
               (remove 10
                   (remove pt (explanation-new-events new-ex) 
                       :key #'occurrence-point :test #'<=
                   )
                   :key #'occurrence-point :test #'>
               )
           )
          )
        (when (null (set-exclusive-or real-evs occs :test #'occurrence-equal))
            (break "Occs couldn't have happened.")
            (return-from determine-next-occs nil)
        )
    )
    
    (setq new-occs nil)
    (setq new-rules nil)
    (dolist (ev-model *ev-models*)
        (dolist (new-fact new-facts)
            (dolist (ev-cnd (remove-if #'is-non-constraining-cnd (event-model-conditions ev-model)))
                (setq blist (unify ev-cnd (list new-fact) nil :assume-fn #'assume-nothing))
                (when (and blist (not (eq blist 'fail)))
                    (setq blist
                        (list 
                            (append
                                (mapcar 
                                    #'(lambda (var-type) 
                                        (cons (first var-type) 
                                            (new-var-sym (second var-type))
                                        )
                                      )
                                    (set-difference
                                        (find-event-model-var-types ev-model (get-predicates *last-envmodel*))
                                        (first blist)
                                        :key #'car
                                    )
                                )
                                (first blist)
                            )
                        )
                    )
                    (break "event = ~A blist = ~A" (event-model-type ev-model) blist)
                    (push-all 
                        (enumerate-occs expgen ev-model facts pt 
                            :bindings (car blist) 
                            :assume-fn #'(lambda (cnd) (not (type-is-is (cnd-type cnd))))
                        )
                        new-occs
                    )
                )
            )
        )
    )
    (format t "~&~%New-occs: ")
    (qp new-occs)
    (break "Testing.")
    
    (when (null new-occs)
        (return-from determine-next-occs (list (make-effect assumptions facts)))
    )
    
    (dolist (occ-part (all-occ-partitions new-occs))
        (when (verify-partition-validity occ-part facts)
            (let ((new-assumps
                    (remove-duplicates
                        (set-difference 
                            (apply #'append (mapcar #'occurrence-pre (car occ-part)))
                            facts
                            :test #'equal-fact
                        )
                        :test #'equal-fact
                    )
                  )
                  (new-new-facts
                    (remove-duplicates
                        (set-difference 
                            (apply #'append (mapcar #'occurrence-post (car occ-part)))
                            facts
                            :test #'equal-fact
                        )
                        :test #'equal-fact
                    )
                  )
                 )
                (when (null (point-next expgen pt))
                    (next-point expgen)
                )
                (push-all
                    (determine-next-occs
                        (append assumptions new-assumps)
                        (append 
                            (set-difference 
                                (remove-duplicates
                                    (append facts new-assumps)
                                    :test #'equal-fact
                                )
                                new-new-facts 
                                :test #'contradicts
                            )
                            new-new-facts
                        )
                        new-new-facts
                        facts
                        (append occs (car occ-part))
                        (point-next expgen pt)
                    )
                    new-rules
                )
            )
        )
    )
    new-rules
)

(defun make-effect (cause effect &aux ret)
    (setq ret (convert-to-when (list cause effect) nil nil))
    (format t "~%Rule: ~A" ret)
    (list cause effect)
)

(defun all-occ-partitions (a-set &optional (part1 nil) (part2 nil))
    (when (null a-set)
        (return-from all-occ-partitions (list (list part1 part2)))
    )
    (append
        (if (not (intersection (occurrence-pre (car a-set))
                               (apply #'append (mapcar #'occurrence-pre part1))
                               :test #'contradicts
                 )
            )
            ;then
            (all-occ-partitions
                (cdr a-set)
                (cons (car a-set) part1)
                part2
            )
            ;else 
            nil
        )
        (all-occ-partitions
            (cdr a-set)
            part1
            (cons (car a-set) part2)
        )
    )
)

(defun verify-partition-validity (occ-part facts)
    (setq facts 
        (append facts (apply #'append (mapcar #'occurrence-pre (first occ-part))))
    )
    (dolist (occ (second occ-part))
        (when (subsetp (occurrence-pre occ) facts :test #'equal-fact)
            (return-from verify-partition-validity nil)
        )
    )
    t
)

(defun check-possible-history ()
    (setq ob2
        (make-occurrence 
            :signature '(observation)
            :is-observation t 
            :point nil
        )
    )
    (setf (get-execution-history expgen) (append *execution-history* (list ob2)))
)

(defun test-conversion-with-rover (&optional (action-name nil))
    (load "rover/RoversEventsCompassHTN-orig-AAAI.htn")
    (load "rover/genProbsProb30/pfile-gen1.lisp")
    (reset-explainer)
    (get-model-information)
    (when (null action-name)
        (setq action-name 'rover::recharge)
    )
    (setf (get-am expgen) (find action-name *action-models* :key #'event-model-type))
;    (find-action-consequences *am* (get-background-facts))
;    (find-action-consequences *am* nil)
    (build-possibilities *am* nil)
)
