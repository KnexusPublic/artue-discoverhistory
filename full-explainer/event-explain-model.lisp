 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: April 8, 2011
 ;;;; Project Name: Explanation
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file provides the data backend for performing event-based 
 ;;;; explanation.

(in-package :artue)


;; Explanation invariants:
;; - Cached data is always maintained as correct for points up to "final-pt".
;; - Events never have occurrence points at which any observation or action 
;;   occurs in the relevant execution history.
;; 
;; A possible series of events compatible with an execution history
(defstruct (explanation (:print-function print-readably-explanation)) 
    (prior-events nil)      ; list of events that were assumed to occur before
                            ; the current hypothesis search
    (new-events nil)        ; list of new events found during this hypothesis
                            ; search
    (event-removals nil)    ; a list of the events from prior-event-list which
                            ; did not occur according to this explanation
    (intervals nil)         ; a set of intervals used to constrain the time at
                            ; which events in this explanation occur
    (cost 0)                ; The cost of forming the explanation
    (depth 0)               ; The number of refinements made to explanation
    (changes 0)             ; The number of changes made to explanation
    (cached-facts nil)      ; a list of facts that hold at various time points.
    (cached-fact-occs nil)  ; a list of facts that hold at various time points.
    (cached-inconsistencies nil)  ; an alist mapping time points to 
                                  ; inconsistency lists
    (cached-inconsistency-list nil)  ; a list of all inconsistencies found in 
                                     ; the last call to find-inconsistencies
    (cached-hidden-results nil)  ; a list of hidden facts that hold after all
                                 ; events in the explanation are executed
    (cached-statics nil)    ; a fact set containing all static facts that are 
                            ; true in the initial state
    (abandoned-models nil)  ; a list of event names which are banned from this 
                            ; explanation (ie., those event models are disbelieved)
    (bindings nil)
;    (equation-relevant-bindings nil) ; bindings relevant to calculating values of
;                                     ; unknown variables
    (consistent nil)
    (valid-fact-cache-pt nil) ; Indicates the last point at which cached facts
                              ; are known to still be valid.
    (valid-event-pt nil)    ; Indicates the last point at which event predictions
                            ; are known to be valid.
    (estimated-inconsistency-count 10000)
    (prohibits-var-symbols t) ; flag indicates whether var symbols are 
                              ; prohibited. Used to narrow down var-symbols 
                              ; until it stops being useful
    (history nil)             ; gives a trace of what happened in the 
                              ; construction of this explanation.
    (occurrence-precedence nil) ; pairs of occurrences that must remain ordered.
    (cached-precedence-map (make-precedence-cache-map))
    (last-added-event nil)
    (last-added-assumption nil)
    (prior-vsym-counter 0)
)




(utils::defun-print-readably explanation 
    ("PRIOR-EVENTS" "NEW-EVENTS" "EVENT-REMOVALS" "COST" "DEPTH" "CHANGES" "BINDINGS")
)

(defvar *occurrence-counter* 100000)

;; An action, event, or observation
(defstruct (occurrence (:print-function print-occurrence)) 
    (name (make-num-sym :num (incf *occurrence-counter*)))     ; unique name of the occurrence
    (performer nil)         ; agent who made this happen
    (signature nil)         ; signature of an event or action
    (point nil)             ; point at which an occurrence happened; nil if 
                            ; exact point is unknown
    (interval nil)          ; interval during which occurrence happened; nil if
                            ; exact point is known
    (pre nil)               ; facts which must have been true before this occurrence
    (constraints nil)       ; A list of cnd objects which must not be true when
                            ; the occurrence happens
    (post nil)              ; facts which must have been true after this occurrence
    (is-action nil)         ; t iff this occurrence is an action
    (is-event nil)          ; t iff this occurrence is an event
    (is-observation nil)    ; t iff this occurrence is an observation
    (is-assumption nil)     ; t iff this occurrence is an assumption
    (hash-val nil)   
    (possible-histories nil)
    (var-syms :unknown)
    (old nil) ; nil iff this occurrence is new to this search, t otherwise
)

(utils::defun-print-readably occurrence 
    ("NAME" "SIGNATURE" "POINT" "INTERVAL" "PRE" "CONSTRAINTS" "POST" "IS-ACTION" 
     "IS-EVENT" "IS-OBSERVATION" "IS-ASSUMPTION")
)

(defstruct model
    (type nil)              ; Human-readable name
    (variables nil)         ; an ordering of the variables used
    (arg-types nil)         ; Types of objects in event, used to ensure every
                            ; possible event occurs 
    (var-types nil)         ; Stored types of all variables referenced by an 
                            ; event
    (conditions nil)        ; A set of conditions which together cause an 
                            ; event to occur
)

(defstruct (process-model (:include model))
    (effects nil)           ; A set of functions over time that govern variable 
                            ; values while the process occurs
)

(defstruct (event-model (:include model))
    (postconditions nil)    ; A set of conditions that should hold after an
                            ; event takes place
    (performer nil)         ; Agent that performed the action. Should be atom 
                            ; (variable or constant)
    (likelihood nil)        ; likelihood that an event will occur; null value 
                            ; means deterministic, other value means 
                            ; non-deterministic
)

;; Time at which an occurrence happens
;(defstruct (point (:print-function print-point) (:constructor construct-point))
;    (num -1)     ; unique name identifying this point
;)

(defun make-point (expgen point-type &key (num -1))
    (when (< num 0)
        (setq num (incf (get-last-point expgen)))
    )
    (setf (aref (get-point-types expgen) num) point-type)
    num
)

(defun point-count (expgen)
    (get-last-point expgen)
)

(defun initialize-points (expgen point-count)
    (setf (get-last-point expgen) point-count)
)

(defun point-prior (expgen pt)
    (if (> pt 1)
        ;then
        (1- pt)
        ;else
        nil
    )
)

(defun point-next (expgen pt)
    (if (< pt (get-last-point expgen))
        ;then
        (1+ pt)
        ;else
        nil
    )
)

(defvar *interval-counter* 200000)


;; Time range during which a possible occurrence may have occurred
(defstruct (interval (:print-function print-interval)) 
    (name (make-num-sym :num (incf *interval-counter*)))     ; unique name identifying this interval
    (range-start nil)       ; earliest point at which the occurrence was possible
    (range-end nil)         ; latest point at which the occurrence was possible
)

(utils::defun-print-readably interval ("NAME" "RANGE-START" "RANGE-END"))

(defstruct (inconsistency (:print-function print-inconsistency))
    (cnd nil)               ; Condition required at time of next-occ to resolve inconsistency
    (prior-occ nil)         ; Most recent relevant occurrence when fact is false (o)
    (next-occ nil)          ; Following relevant occurrence when fact is true (o')
    (ambiguity nil)
)

(utils::defun-print-readably inconsistency ("CND" "PRIOR-OCC" "NEXT-OCC"))

(defun print-inconsistency (inc str k)
    (if *print-readably*
        ;then
        (print-readably-interval inc str k)
        ;else
        (pprint-logical-block 
            (str nil 
             :prefix "#S("
             :suffix ")"
            )
            (if (inconsistency-ambiguity inc)
                ;then
                (write 'ambiguity :stream str)
                ;else
                (write 'inconsistency :stream str)
            )
            (pprint-newline :linear str)
            (write :cnd :stream str)
            (write-char #\space str)
            (write (inconsistency-cnd inc) :stream str)
            (pprint-newline :linear str)
            (write :prior :stream str)
            (write-char #\space str)
            (write (inconsistency-prior-occ inc) :stream str)
            (pprint-newline :linear str)
            (write :next :stream str)
            (write-char #\space str)
            (write (inconsistency-next-occ inc) :stream str)
        )
    )
)

;(defun print-point (pt s k)
;    (declare (ignore k))
;    (if *print-readably*
;        ;then
;        (print-readably-point pt s k)
;        ;else
;        (format s "P~A" (point-num pt))
;    )
;)

(defun print-interval (i s k)
    (if *print-readably*
        ;then
        (print-readably-interval i s k)
        ;else
        (format s "(interval :name ~A :start ~A :end ~A)" (interval-name i)
            (if (typep (interval-range-start i) 'interval) 
                ;then
                (interval-name (interval-range-start i))
                ;else
                (interval-range-start i)
            )
            (if (typep (interval-range-end i) 'interval) 
                ;then
                (interval-name (interval-range-end i))
                ;else
                (interval-range-end i)
            )
        )
    )
)

(defun print-occurrence (occ s k)
    (cond
        (*print-readably*
            (print-readably-occurrence occ s k)
        )
        ((occurrence-is-observation occ)
            (format s "(observation :time ~A)" (occurrence-point occ))
        )
        (t
            (format s "~A" 
                (append (occurrence-signature occ) 
                        '(:time)
                        (if (occurrence-point occ) (list (occurrence-point occ)) nil)
                        (if (occurrence-interval occ) (list (occurrence-interval occ)) nil)
                )
            )
        )
    )
)


(defun explanation-is-trivial (expgen ex)
    (null (remove (get-first-point expgen) (get-events ex) :key #'occurrence-point))
)

;;Returns an explanation with an event removed
(defun remove-event (expgen ex ev)
    (when (not (occurrence-old ev))
        (return-from remove-event 'fail)
    )
    (setq ex (copy-explanation ex))
    (trim-cache expgen ex (occ-first-point ev))
    (incf (explanation-changes ex) 1)
    (setf (explanation-prior-events ex) (remove ev (explanation-prior-events ex)))
    (setf (explanation-new-events ex) (remove ev (explanation-new-events ex)))
    (setf (explanation-occurrence-precedence ex) (remove ev (explanation-occurrence-precedence ex) :key #'first))
    (setf (explanation-occurrence-precedence ex) (remove ev (explanation-occurrence-precedence ex) :key #'second))
    (push ev (explanation-event-removals ex))
    ex
)

;;Returns an explanation with an event added
(defun add-event (expgen ex ev)
    (when (or (member ev (explanation-prior-events ex)) (member ev (explanation-new-events ex)))
        (return-from add-event 'fail)
    )
    (setq ex (copy-explanation ex))
    (trim-cache expgen ex (occ-first-point ev))
    (incf (explanation-changes ex) 1)
    (push ev (explanation-new-events ex))
    ex
)

;; Important note: the point at which the cache is trimmed will no longer 
;; exist in the cache afterwards. Therefore, when trim-cache returns, the final
;; cached point of the explanation shall be *earlier than* the argument pt. 
(defun trim-cache (expgen ex pt &aux old-map)
    (when (null (explanation-valid-fact-cache-pt ex))
        (return-from trim-cache nil)
    )
    (when (null pt)
        (setf (explanation-cached-statics ex) nil)
        (setf (explanation-cached-facts ex) nil)
        (setf (explanation-cached-fact-occs ex) nil)
        (setf (explanation-cached-inconsistencies ex) nil)
        (setf (explanation-cached-hidden-results ex) nil)
        (setf (explanation-cached-precedence-map ex) (make-precedence-cache-map))
        (setf (explanation-valid-fact-cache-pt ex) nil)
        (return-from trim-cache t)
    )
    (setf (explanation-cached-precedence-map ex) (make-precedence-cache-map))
    (when (point-earlier (explanation-valid-fact-cache-pt ex) pt)
        (return-from trim-cache nil)
    )
    (setf (explanation-cached-facts ex)
        (member pt
            (explanation-cached-facts ex)
            :key #'car
            :test #'point-later
        )
    )
    (setf (explanation-cached-fact-occs ex)
        (member pt
            (explanation-cached-fact-occs ex)
            :key #'car
            :test #'point-later
        )
    )

    (when (and (explanation-cached-facts ex) 
               (null (assoc pt (explanation-cached-facts ex))))
        (setq pt (point-prior expgen pt))
        (loop while (null (assoc pt (explanation-cached-facts ex))) do
            (setq pt (point-prior expgen pt))
        )
    )

    (if (null (point-prior expgen pt))
        ;then
        (setf (explanation-cached-inconsistencies ex) nil)
        ;else
        (setf (explanation-cached-inconsistencies ex)
            (member (point-prior expgen pt)
                (explanation-cached-inconsistencies ex)
                :key #'car
                :test #'point-later
            )
        )
    )

    (setf (explanation-cached-hidden-results ex) nil)
    (when (not (numberp pt))
        (error "Bad value in final-pt.")
    )
    (setf (explanation-valid-fact-cache-pt ex) pt)
    (when (and pt (point-earlier (explanation-valid-fact-cache-pt ex) (or (caar (explanation-cached-inconsistencies ex)) 0)))
        (error "Out of order?")
    )
    (setf (explanation-valid-event-pt ex) pt)
    
    (setq old-map (explanation-cached-precedence-map ex))
    (setf (explanation-cached-precedence-map ex) (make-precedence-cache-map))

;    (maphash 
;        #'(lambda (key val) 
;            (when (and (point-earlier (occ-last-point (car key)) pt)
;                       (point-earlier (occ-last-point (cdr key)) pt)
;                  )
;                (set-precedence ex (car key) (cdr key) val)
;            )
;          )
;        old-map
;    )
    
    (when (null (explanation-cached-facts ex))
        (setf (explanation-cached-statics ex) nil)
        (setf (explanation-cached-inconsistencies ex) nil)
        (setf (explanation-valid-fact-cache-pt ex) nil)
    )
    t
)

(defun add-to-history (ex refine-fn-name inc parent-ex)
    (push 
        (append 
            (list :refine-type refine-fn-name :inc inc)
            (find-refinements ex parent-ex)
        )
        (explanation-history ex)
    )
    ex
)

(defun discard-old-cache (ex)
    (when (null (explanation-valid-fact-cache-pt ex))
        (return-from discard-old-cache nil)
    )
    (setf (explanation-cached-facts ex)
        (remove (- (explanation-valid-fact-cache-pt ex) 20)
            (explanation-cached-facts ex)
            :key #'car
            :test #'point-later
        )
    )
    (setf (explanation-cached-fact-occs ex)
        (remove (- (explanation-valid-fact-cache-pt ex) 20)
            (explanation-cached-fact-occs ex)
            :key #'car
            :test #'point-later
        )
    )
)

;; Returns the set of events that have occurred according to an explanation
(defun get-events (ex)
    ;(set-difference
        (append 
            (explanation-new-events ex) 
            (explanation-prior-events ex)
        )
        ;(explanation-event-removals ex)
    ;)
)


(defun equal-inconsistency (inc1 inc2)
    (and (equal-cnd (inconsistency-cnd inc1) (inconsistency-cnd inc2))
         (eq (inconsistency-prior-occ inc1) (inconsistency-prior-occ inc2))
         (eq (inconsistency-next-occ inc1) (inconsistency-next-occ inc2))
    )
)

; (defun link-points (pt1 pt2)
    ; (setf (point-next expgen pt1) pt2)
    ; (setf (point-prior expgen pt2) pt1)
; )

(defun occurrence-equal (occ1 occ2)
    (or (eq occ1 occ2)
        (and occ1 occ2  ;;if both are nil, handled by first case
             (eq (occurrence-is-observation occ1) (occurrence-is-observation occ2))
             (eq (occurrence-is-action occ1) (occurrence-is-action occ2))
             (eq (occurrence-is-event occ1) (occurrence-is-event occ2))
             (eq (occurrence-is-assumption occ1) (occurrence-is-assumption occ2))
             (eql (occurrence-point occ1) (occurrence-point occ2))
             (interval-equal (occurrence-interval occ1) (occurrence-interval occ2))
             (val-close-enough (occurrence-signature occ1) (occurrence-signature occ2))
             (null (set-exclusive-or (occurrence-pre occ1) (occurrence-pre occ2) :test #'equal-fact))
             (null (set-exclusive-or (occurrence-constraints occ1) (occurrence-constraints occ2) :test #'equal-cnd))
             (null (set-exclusive-or (occurrence-post occ1) (occurrence-post occ2) :test #'equal-fact))
        )
    )
)

(defun occurrence-is-repeat (occ1 occ2)
    (and (eq (occurrence-is-observation occ1) (occurrence-is-observation occ2))
         (eq (occurrence-is-action occ1) (occurrence-is-action occ2))
         (eq (occurrence-is-event occ1) (occurrence-is-event occ2))
         (= (occ-interval-length occ1) (occ-interval-length occ2))
         (every #'equal-arg (occurrence-signature occ1) (occurrence-signature occ2))
         (null (set-exclusive-or (occurrence-pre occ1) (occurrence-pre occ2) :test #'repeat-fact))
         (null (set-exclusive-or (occurrence-constraints occ1) (occurrence-constraints occ2) :test #'equal-cnd))
         (null (set-exclusive-or (occurrence-post occ1) (occurrence-post occ2) :test #'repeat-fact))
    )
)

(defun occ-first-point (occ)
    (if (occurrence-interval occ)
        ;then
        (interval-range-start (occurrence-interval occ))
        ;else
        (occurrence-point occ)
    )
)

(defun occ-last-point (occ)
    (if (occurrence-interval occ)
        ;then
        (interval-range-end (occurrence-interval occ))
        ;else
        (occurrence-point occ)
    )
)

(defun occ-interval-length (occ)
    (- (occ-last-point occ) (occ-first-point occ))
)

(defun point-is-relevant-to (expgen pt occ)
    (let ((ival (occurrence-interval occ)))
        (if (null ival)
            ;then
            (eql (occurrence-point occ) pt)
            ;else
            (and (not (point-earlier pt (interval-range-start ival)))
                 (not (point-earlier (interval-range-end ival) pt))
            )
        )
    )
)

(defun occurrence-could-happen-at (expgen occ pt)
    (let ((ival (occurrence-interval occ)))
        (if (null ival)
            ;then
            (eql (occurrence-point occ) pt)
            ;else
            (and (not (point-earlier pt (interval-range-start ival)))
                 (not (point-earlier (interval-range-end ival) pt))
                 (if (occurrence-is-action occ)
                     ;then
                     (is-action-pt expgen pt)
                     ;else
                     (is-event-pt expgen pt)
                 )
            )
        )
    )
)

(defun occurrence-should-project-at (expgen occ pt)
    (eql pt (occ-first-point occ))
)

(defun point-is-during (pt occ)
    (and (point-earlier pt (occ-last-point occ))
         (not (point-earlier pt (occ-first-point occ)))
    )
)
(defun is-obs-pt (expgen pt &aux occ)
    (eq (aref (get-point-types expgen) pt) :observation)
    ;(setq occ (find pt (get-execution-history expgen) :key #'occurrence-point :test #'point-equal))
    ;(and occ (occurrence-is-observation occ))
)

(defun is-exogenous-action-pt (expgen pt &aux occ)
    (eq (aref (get-point-types expgen) pt) :exogenous-action)
    ;(setq occ (find pt (get-execution-history expgen) :key #'occurrence-point :test #'point-equal))
    ;(and (null occ) (is-obs-pt expgen (point-prior expgen pt)))
)

(defun is-action-pt (expgen pt &aux occ)
    (member (aref (get-point-types expgen) pt) '(:action :exogenous-action))
    ;(is-obs-pt expgen (point-prior expgen pt))
)

(defun is-event-pt (expgen pt)
    (eq (aref (get-point-types expgen) pt) :event)
    ;(not (member pt (get-execution-history expgen) :key #'occurrence-point :test #'point-equal))
)

(defun extend-occ-interval (occ pt)
    (setf (occurrence-interval occ)
        (make-interval 
            :range-start (occ-first-point occ) 
            :range-end pt
        )
    )
    (setf (occurrence-point occ) pt)
)

(defun interval-equal (int1 int2)
    (when (null int1) (return-from interval-equal (null int2)))
    (when (null int2) (return-from interval-equal nil))
    (and (eq (interval-range-start int1) (interval-range-start int2))
         (eq (interval-range-end int1) (interval-range-end int2))
    )
)

(defun point-in-interval (pt ival)
    (and (not (point-later (interval-range-start ival) pt))
         (not (point-later pt (interval-range-end ival)))
    )
)

(defun remove-less-general-occs (occs &aux (ret-occs nil) (occ nil))
    (loop while occs do
        (setq occ (car occs))
        (setq occs (cdr occs))
        (let ((rel-occs (remove (occurrence-type occ) ret-occs :key #'occurrence-type :test-not #'eq))
              known-occ
              more-general-found
             )
            ;(format t "~&~%Next occ: ~A" occ)
            ;(format t "~&~%Rel-occs:")
            ;(qp rel-occs)
            (setq more-general-found nil)
            (loop while rel-occs do
                (setq known-occ (car rel-occs))
                (setq rel-occs (cdr rel-occs))
                (cond
                    ((occurrence-as-general-as known-occ occ)
                        (setq rel-occs nil)
                        (setq more-general-found t)
                    )
                    ((occurrence-as-general-as occ known-occ)
                        (setq ret-occs (remove known-occ ret-occs))
                        (setq rel-occs nil)
                        (setq more-general-found nil)
                    )
                )
            )
            (when (not more-general-found)
                (push occ ret-occs)
            )
            ;(format t "~&~%Ret-occs:")
            ;(qp ret-occs)
        )
    )
    ret-occs
)
                    

(defun occurrence-as-general-as (occ1 occ2)
    (and (eq (occurrence-is-observation occ1) (occurrence-is-observation occ2))
         (eq (occurrence-is-action occ1) (occurrence-is-action occ2))
         (eq (occurrence-is-event occ1) (occurrence-is-event occ2))
         (eq (car (occurrence-signature occ1)) (car (occurrence-signature occ2)))
         (every #'arg-as-general-as (cdr (occurrence-signature occ1)) (cdr (occurrence-signature occ2)))
    )
)

(defun occurrences-unifiable (occ1 occ2)
    (and (eq (occurrence-is-observation occ1) (occurrence-is-observation occ2))
         (eq (occurrence-is-action occ1) (occurrence-is-action occ2))
         (eq (occurrence-is-event occ1) (occurrence-is-event occ2))
         (eq (car (occurrence-signature occ1)) (car (occurrence-signature occ2)))
         (every #'args-unifiable (cdr (occurrence-signature occ1)) (cdr (occurrence-signature occ2)))
         (not (point-earlier (occ-last-point occ1) (occ-first-point occ2)))
         (not (point-earlier (occ-last-point occ2) (occ-first-point occ1)))
    )
)

(defun arg-as-general-as (arg1 arg2)
    (cond
        ((equal arg1 arg2) t)
        ((and (var-symbol-p arg1) (var-symbol-p arg2))
            (var-sym-as-general-as arg1 arg2)
        )
        ((and (var-symbol-p arg1) (allowed-value arg1 arg2))
            t
        )
        (t nil)
    )
)

(defun args-unifiable (arg1 arg2)
    (cond
        ((equal arg1 arg2) t)
        ((and (var-symbol-p arg1) (var-symbol-p arg2))
            (var-sym-as-general-as arg1 arg2)
        )
        ((and (var-symbol-p arg1) (allowed-value arg1 arg2))
            t
        )
        ((and (var-symbol-p arg2) (allowed-value arg2 arg1))
            t
        )
        (t nil)
    )
)

(defun equal-occ-pair (occ-pair1 occ-pair2)
    (and (occurrence-equal (car occ-pair1) (car occ-pair2))
         (occurrence-equal (cdr occ-pair1) (cdr occ-pair2)))
)

(defun explanation-equal (ex1 ex2)
    (and (or (eq (explanation-prior-events ex1) (explanation-prior-events ex2))
             (null (set-exclusive-or  (explanation-prior-events ex1) (explanation-prior-events ex2) :test #'occurrence-equal))
         )
         (or (eq (explanation-new-events ex1) (explanation-new-events ex2))
             (null (set-exclusive-or  (explanation-new-events ex1) (explanation-new-events ex2) :test #'occurrence-equal))
         )
         (or (eq (explanation-event-removals ex1) (explanation-event-removals ex2))
             (null (set-exclusive-or  (explanation-event-removals ex1) (explanation-event-removals ex2) :test #'occurrence-equal))
         )
         (null (set-exclusive-or  (explanation-intervals ex1) (explanation-intervals ex2) :test #'interval-equal))
    )
)

(defun remove-redundant-explanations (exes &aux found-exes)
    (setq exes 
        (sort (copy-list exes) #'< 
            :key #'(lambda (ex) (length (get-assumptions ex)))
        )
    )
    (setq found-exes nil)
    (dolist (ex exes)
        (when (not (member (get-assumption-changes ex) 
                      found-exes 
                      :key #'get-assumption-changes 
                      :test #'assumption-change-supersetp
              )    )
            (push ex found-exes)
        )
    )
    found-exes
)

(defun remove-redundant-refinements (exes parent-ex &aux (ret-exes nil) (known-refines nil))
    (setq exes 
        (sort (copy-list exes) #'< 
            :key #'explanation-cost
        )
    )
    (dolist (child-ex exes)
        (let ((cur-refine (find-refinements child-ex parent-ex)))
            (when (not (member cur-refine known-refines :test #'equivalent-refinements))
                (push child-ex ret-exes)
                (push cur-refine known-refines)
            )
        )
    )
    ret-exes
)

(defun find-refinements (child-ex parent-ex)
    (list 
        :added-events
        (set-difference 
            (explanation-new-events child-ex) 
            (explanation-new-events parent-ex)
        )
        :replaced-events
        (remove-if #'occurrence-old
            (set-difference 
                (explanation-new-events parent-ex)
                (explanation-new-events child-ex) 
            )
        )
        :removed-events
        (set-difference 
            (explanation-event-removals child-ex) 
            (explanation-event-removals parent-ex)
        )
        :added-bindings
        (set-difference 
            (explanation-bindings child-ex) 
            (explanation-bindings parent-ex)
        )
        :replaced-bindings
        (set-difference 
            (explanation-bindings parent-ex)
            (explanation-bindings child-ex) 
        )
    )
)

(defun equivalent-refinements (refine1 refine2)
    (and
        (set-equal (getf refine1 :replaced-events)
                   (getf refine2 :replaced-events)
        )
        (set-equal (getf refine1 :replaced-bindings)
                   (getf refine2 :replaced-bindings)
        )
        (set-equal (getf refine1 :removed-events)
                   (getf refine2 :removed-events)
        )
        (set-equal (getf refine1 :added-bindings)
                   (getf refine2 :added-bindings)
                   :test #'equivalent-binding
        )
        (set-equal (getf refine1 :added-events)
                   (getf refine2 :added-events)
                   :test #'equivalent-occurrence
        )
    )
)

(defun equivalent-refinements-lax (refine1 refine2)
    (and
        (set-equal (getf refine1 :replaced-events)
                   (getf refine2 :replaced-events)
                   :test #'equivalent-occurrence
        )
        (set-equal (getf refine1 :added-events)
                   (getf refine2 :added-events)
                   :test #'equivalent-occurrence
        )
        (set-equal (getf refine1 :removed-events)
                   (getf refine2 :removed-events)
                   :test #'equivalent-occurrence
        )
;        (every #'equivalent-binding-lax
;            (getf refine1 :added-bindings)
;            (getf refine2 :added-bindings)
;        )
;        (every #'equivalent-binding-lax
;            (getf refine1 :replaced-bindings)
;            (getf refine2 :replaced-bindings)
;        )
    )
)

(defun equivalent-binding-set (blist1 blist2)
    (every #'equivalent-binding blist1 blist2)
)

(defun equivalent-binding (binding1 binding2)
    (and (eq (car binding1) (car binding2))
         (equal (cdr binding1) (cdr binding2))
    )
)
             
(defun equivalent-binding-lax (binding1 binding2)
     (value-as-specific-as (cdr binding1) (cdr binding2))
)
             

(defun equivalent-occurrence (occ1 occ2 &aux blist)
    (setq blist (unify-occurrences occ1 occ2))
    (when (eq blist :fail)
        (return-from equivalent-occurrence nil)
    )
    (setq occ1 (bind-occurrence occ1 blist))
    (and (occurrence-as-specific-as occ1 occ2)
         (occurrence-as-specific-as occ2 occ1)
    )
)

(defun unify-occurrences (occ1 occ2)
    (when (and (null occ1) (null occ2))
        (return-from unify-occurrences nil)
    )
    (when (or (null occ1) (null occ2))
        (return-from unify-occurrences :fail)
    )
    (when (not (every 
                    #'(lambda (arg1 arg2) 
                        (or (eql arg1 arg2) 
                            (and (var-symbol-p arg1) (var-symbol-p arg2))
                        )
                      ) 
                    (occurrence-signature occ1) 
                    (occurrence-signature occ2)
               )
          )
        (return-from unify-occurrences :fail)
    )
    (mapcan
        #'(lambda (arg1 arg2)
            (if (eq arg1 arg2)
                ;then
                nil
                ;else
                `((,(var-symbol-name arg1) . ,arg2))
            )
          )
        (occurrence-signature occ1) 
        (occurrence-signature occ2)
    )
)
    

(defun set-equal (set1 set2 &key (test #'eq))
    (and (null (set-difference set1 set2 :test test))
         (null (set-difference set2 set1 :test test))
    )
)

(defun occurrence-repeat (occ1 occ2)
    (and (eq (occurrence-is-observation occ1) (occurrence-is-observation occ2))
         (eq (occurrence-is-action occ1) (occurrence-is-action occ2))
         (eq (occurrence-is-event occ1) (occurrence-is-event occ2))
         (equal (occurrence-signature occ1) (occurrence-signature occ2))
         (null (set-exclusive-or (occurrence-pre occ1) (occurrence-pre occ2) :test #'equal-fact))
         (null (set-exclusive-or (occurrence-constraints occ1) (occurrence-constraints occ2) :test #'equal-cnd))
         (null (set-exclusive-or (occurrence-post occ1) (occurrence-post occ2) :test #'equal-fact))
    )
)

(defun equivalent-inconsistency (inc1 inc2 &aux blist1 blist2)
    (setq blist1
        (unify-occurrences 
            (inconsistency-prior-occ inc1) 
            (inconsistency-prior-occ inc2)
        )
    )
    (when (eq blist1 :fail)
        (return-from equivalent-inconsistency nil)
    )
    (setq blist2
        (unify-occurrences 
            (inconsistency-next-occ inc1) 
            (inconsistency-next-occ inc2)
        )
    )
    (when (eq blist2 :fail)
        (return-from equivalent-inconsistency nil)
    )
    (setq inc1 (bind-inconsistency inc1 (append blist1 blist2)))
    (and (inconsistency-as-specific-as inc1 inc2)
         (inconsistency-as-specific-as inc2 inc1)
    )
)



(defun inconsistency-as-specific-as (specific-inc general-inc)
    (and (cnd-as-specific-as (inconsistency-cnd specific-inc)
                             (inconsistency-cnd general-inc)
         )
         (or (and (null (inconsistency-prior-occ specific-inc))
                  (null (inconsistency-prior-occ general-inc))
             )
             (and (inconsistency-prior-occ specific-inc)
                  (inconsistency-prior-occ general-inc)
                  (occurrence-as-specific-as (inconsistency-prior-occ specific-inc) 
                                             (inconsistency-prior-occ general-inc)
                  )
             )
         )
         (occurrence-as-specific-as (inconsistency-next-occ specific-inc) 
                                    (inconsistency-next-occ general-inc)
         )
    )
)

(defun occurrence-as-specific-as (specific-occ general-occ)
    (and (eq (occurrence-is-observation specific-occ) (occurrence-is-observation general-occ))
         (eq (occurrence-is-action specific-occ) (occurrence-is-action general-occ))
         (eq (occurrence-is-event specific-occ) (occurrence-is-event general-occ))
         (equal (occurrence-type specific-occ) (occurrence-type general-occ))
         (not (point-later (occ-first-point general-occ) (occ-first-point specific-occ)))
         (not (point-later (occ-last-point specific-occ) (occ-last-point general-occ)))
         (eql (length (occurrence-post specific-occ)) (length (occurrence-post general-occ)))
         (eql (+ (length (occurrence-pre specific-occ)) 
                 (length (occurrence-constraints specific-occ))
              )
              (+ (length (occurrence-pre general-occ)) 
                 (length (occurrence-constraints general-occ))
              )
         )
         (conditions-as-specific-as 
             (mapcar #'fact-to-cnd (occurrence-post specific-occ))
             (mapcar #'fact-to-cnd (occurrence-post general-occ))
         )
         (conditions-as-specific-as 
             (append 
                 (mapcar #'negate-condition (occurrence-constraints specific-occ))
                 (mapcar #'fact-to-cnd (occurrence-pre specific-occ))
             )
             (append 
                 (mapcar #'negate-condition (occurrence-constraints general-occ))
                 (mapcar #'fact-to-cnd (occurrence-pre general-occ))
             )
         )
    )
)

(defun conditions-as-specific-as (specific-cnd-set general-cnd-set &aux more-general-cnds)
    (when (and (null specific-cnd-set) (null general-cnd-set))
        (return-from conditions-as-specific-as t)
    )
    (setq more-general-cnds
        (remove-if-not 
            #'(lambda (general-cnd) 
                (cnd-as-specific-as (car specific-cnd-set) general-cnd)
              )
            general-cnd-set
        )
    )
    (dolist (general-cnd more-general-cnds)
        (when (conditions-as-specific-as 
                (cdr specific-cnd-set)
                (remove general-cnd general-cnd-set)
              )
            (return-from conditions-as-specific-as t)
        )
    )
    nil
)

(defun cnd-as-specific-as (specific-cnd general-cnd &aux ineq-pair)
    (when (not (eq (cnd-type specific-cnd) (cnd-type general-cnd)))
        (return-from cnd-as-specific-as nil)
    )
    (dolist (arg-pair (pairlis (cnd-args specific-cnd) (cnd-args general-cnd)))
        (when (not (value-as-specific-as (car arg-pair) (cdr arg-pair)))
            (return-from cnd-as-specific-as nil)
        )
    )
    (when (cnd-inequality specific-cnd)
        (when (not (cnd-inequality general-cnd))
            (return-from cnd-as-specific-as nil)
        )
        (setq ineq-pair
            (cons (nth-value 2 
                    (function-lambda-expression (cnd-inequality specific-cnd))
                  )
                  (nth-value 2 
                    (function-lambda-expression (cnd-inequality general-cnd))
                  )
            )
        )
        (cond
            ((member ineq-pair '((> . >=) (> . >) (>= . >=)) :test #'equalp) 
                (return-from cnd-as-specific-as
                    (fudge-inequality #'>=
                        (get-lower-bound (cnd-value specific-cnd)) 
                        (get-lower-bound (cnd-value general-cnd))
                    )
                )
            )
            ((member ineq-pair '((< . <=) (< . <) (<= . <=)) :test #'equalp) 
                (return-from cnd-as-specific-as
                    (fudge-inequality #'<=
                        (get-upper-bound (cnd-value specific-cnd)) 
                        (get-upper-bound (cnd-value general-cnd))
                    )
                )
            )
            ((member ineq-pair '((>= . >)) :test #'equalp) 
                (return-from cnd-as-specific-as
                    (fudge-inequality #'>
                       (get-lower-bound (cnd-value specific-cnd)) 
                       (get-lower-bound (cnd-value general-cnd))
                    )
                )
            )
            ((member ineq-pair '((<= . <)) :test #'equalp) 
                (return-from cnd-as-specific-as
                    (fudge-inequality #'<
                       (get-upper-bound (cnd-value specific-cnd)) 
                       (get-upper-bound (cnd-value general-cnd))
                    )
                )
            )
            ((member ineq-pair '((< . neq) (<= . neq)) :test #'equalp) 
                (return-from cnd-as-specific-as
                    (fudge-inequality #'<
                       (get-upper-bound (cnd-value specific-cnd)) 
                       (get-lower-bound (cnd-value general-cnd))
                    )
                )
            )
            ((member ineq-pair '((> . neq) (>= . neq)) :test #'equalp) 
                (return-from cnd-as-specific-as
                    (fudge-inequality #'>
                       (get-lower-bound (cnd-value specific-cnd)) 
                       (get-upper-bound (cnd-value general-cnd))
                    )
                )
            )
            ((member ineq-pair '((neq . neq)) :test #'equalp) 
                (when (var-symbol-p (cnd-value specific-cnd))
                    (return-from cnd-as-specific-as
                        (and (var-symbol-p (cnd-value general-cnd))
                             (var-symbol-as-specific-as 
                                 (cnd-value general-cnd)
                                 (cnd-value specific-cnd)
                             )
                        )
                    )
                )
                (return-from cnd-as-specific-as
                    (eql (cnd-value specific-cnd) (cnd-value general-cnd))
                )
            )
            (t (return-from cnd-as-specific-as nil))
        )
    )
    (when (cnd-inequality general-cnd)
        ;; NOT inequality in specific-cnd
        (case (nth-value 2 (function-lambda-expression (cnd-inequality general-cnd)))
            ((> >=)
                (return-from cnd-as-specific-as
                    (fudge-inequality (cnd-inequality general-cnd)
                        (get-lower-bound (cnd-value specific-cnd))
                        (get-lower-bound (cnd-value general-cnd))
                    )
                )
            )
            ((< <=)
                (return-from cnd-as-specific-as
                    (fudge-inequality (cnd-inequality general-cnd)
                        (get-upper-bound (cnd-value specific-cnd))
                        (get-upper-bound (cnd-value general-cnd))
                    )
                )
            )
            (neq 
                (when (var-symbol-p (cnd-value general-cnd))
                    ;; not equal to a variable effectively gives no info,
                    ;; anything is as specific as that.
                    (return-from cnd-as-specific-as t)
                )
                (when (var-symbol-p (cnd-value specific-cnd))
                    (return-from cnd-as-specific-as 
                        (not (allowed-value (cnd-value specific-cnd) 
                                            (cnd-value general-cnd)
                             )
                        )
                    )
                )
                (return-from cnd-as-specific-as 
                    (not (equalp (cnd-value specific-cnd) (cnd-value general-cnd)))
                )
            )
            (t (error "Unexpected inequality ~A in cnd-as-specific-as!" (cnd-inequality general-cnd)))
        )
    )
    ;;If get to here, comparing equalities
    (value-as-specific-as (cnd-value specific-cnd) (cnd-value general-cnd))
)

(defun cnd-equivalent (cnd1 cnd2 &aux name-blist blist)
    (when (not (eq (cnd-type cnd1) (cnd-type cnd2)))
        (return-from cnd-equivalent 'fail)
    )
    (when (not (eq (cnd-inequality cnd1) (cnd-inequality cnd2)))
        (return-from cnd-equivalent 'fail)
    )
    (setq blist 
        (find-var-symbol-bindings
            (cons (cnd-value cnd1) (cnd-args cnd1))
            (cons (cnd-value cnd2) (cnd-args cnd2))
        )
    )
    (when (eq blist 'fail)
        (return-from cnd-equivalent 'fail)
    )
    (setq name-blist 
        (mapcar 
            #'(lambda (pair) (cons (var-symbol-name (car pair)) (var-symbol-name (cdr pair))))
            blist
        )
    )
            
    (if
        (every 
            #'(lambda (pair) (var-symbol-equivalent (car pair) (cdr pair) :blist name-blist))
            blist
        )
        ;then
        blist
        ;else
        'fail
    )
)

(defun find-var-symbol-bindings (lst1 lst2 &aux blist)
    (when (and (null lst1) (null lst2))
        (return-from find-var-symbol-bindings nil)
    )
    (when (or (null lst1) (null lst2))
        (return-from find-var-symbol-bindings 'fail)
    )
    (when (and (not (var-symbol-p (car lst1))) (not (eq (car lst1) (car lst2))))
        (return-from find-var-symbol-bindings 'fail)
    )
    (when (and (var-symbol-p (car lst1)) (not (var-symbol-p (car lst2))))
        (return-from find-var-symbol-bindings 'fail)
    )
    (setq blist (find-var-symbol-bindings (cdr lst1) (cdr lst2)))
    (when (eq blist 'fail)
        (return-from find-var-symbol-bindings 'fail)
    )
    (when (var-symbol-p (car lst1)) ; and (var-symbol-p (car lst2))
        (return-from find-var-symbol-bindings 
            (cons (cons (car lst1) (car lst2)) blist)
        )
    )
    (return-from find-var-symbol-bindings blist)
)

(defun pair-eq (pair)
    (equalp (car pair) (cdr pair))
)


(defun get-lower-bound (value)
    (when (numberp value)
        (return-from get-lower-bound value)
    )
    (when (not (var-symbol-p value))
        (error "Unacceptable value in get-lower-bound.")
    )
    (when (if-number (var-symbol-> value))
        (when (if-number (var-symbol->= value))
            (error "Unacceptable dual bound in get-lower-bound.")
        )
        (return-from get-lower-bound (+ (car (var-symbol-> value)) double-float-epsilon))
    )
    (if (not (if-number (var-symbol->= value)))
        ;then
        most-negative-double-float
        ;else
        (car (var-symbol->= value))
    )
)

(defun get-upper-bound (value)
    (when (numberp value)
        (return-from get-upper-bound value)
    )
    (when (not (var-symbol-p value))
        (error "Unacceptable value in get-upper-bound.")
    )
    (when (if-number (var-symbol-< value))
        (when (if-number (var-symbol-<= value))
            (error "Unacceptable dual bound in get-upper-bound.")
        )
        (return-from get-upper-bound (- (car (var-symbol-< value)) double-float-epsilon))
    )
    (if (not (if-number (var-symbol-<= value)))
        ;then
        most-positive-double-float
        ;else
        (car (var-symbol-<= value))
    )
)
                
(defun value-as-specific-as (specific-value general-value)
    (cond
        ((val-close-enough specific-value general-value) t)
        ((and (var-symbol-p specific-value) (var-symbol-p general-value))
            (var-symbol-as-specific-as specific-value general-value)
        )
        ((var-symbol-p general-value)
            (allowed-value general-value specific-value)
        )
        (t nil)
    )
)

(defun var-symbol-as-specific-as (specific-vsym general-vsym)
    (and (>= (get-lower-bound specific-vsym) (get-lower-bound general-vsym)) 
         (<= (get-upper-bound specific-vsym) (get-upper-bound general-vsym))
         (subsetp (var-symbol-neq general-vsym) (var-symbol-neq specific-vsym))
         (or (null (var-symbol-rooted general-vsym))
             (eq (var-symbol-rooted general-vsym) (var-symbol-rooted specific-vsym))
         )
    )
)

(defun get-assumptions (ex)
    (remove-if-not #'occurrence-is-assumption (get-events ex))
)

(defun get-assumption-changes (ex)
    (list 
        (remove-if-not #'occurrence-is-assumption (explanation-new-events ex))
        (remove-if-not #'occurrence-is-assumption (explanation-event-removals ex))
    )
)

(defun assumption-change-supersetp (changes1 changes2)
    (and (event-supersetp (car  changes1) (car  changes2))
         (event-supersetp (cadr changes1) (cadr changes2))
    )
)

(defun point-equal (pt1 pt2)
    (= pt1 pt2)
)

(defun point-earlier (pt1 pt2)
    ; (when (null (point-prior expgen pt2))
        ; (return-from point-earlier nil)
    ; )
    ; (when (eq (point-prior expgen pt2) pt1)
        ; (return-from point-earlier t)
    ; )
    ; (point-earlier pt1 (point-prior expgen pt2))
    (< pt1 pt2)
)

(defun point-later (pt1 pt2)
    (point-earlier pt2 pt1)
)

(defun occurrence-type (occ)
    (car (occurrence-signature occ))
)

(defun occurrence-earlier (occ1 occ2)
    (point-earlier (occ-last-point occ1) (occ-first-point occ2))
)

(defun occurrence-starts-earlier (occ1 occ2)
    (point-earlier (occ-first-point occ1) (occ-first-point occ2))
)

(defun occurrence-may-be-earlier (occ1 occ2)
    (point-earlier (occ-first-point occ1) (occ-last-point occ2))
)

(defun occurrence-later (occ1 occ2)
    (occurrence-earlier occ2 occ1)
)

(defun occurrence-may-be-later (occ1 occ2)
    (occurrence-may-be-earlier occ2 occ1)
)

(defun occ-overlaps-with (occ1 occ2)
    (when (occurrence-point occ1) (return-from occ-overlaps-with nil))
    (let ((fp1 (occ-first-point occ1))
          (fp2 (occ-first-point occ2))
          (lp1 (occ-last-point occ1))
          (lp2 (occ-last-point occ2))
         )
      
        (or (and (not (point-later fp1 fp2))
                 (not (point-later fp2 lp1))
            )
            (and (not (point-later fp1 lp2))
                 (not (point-later lp2 lp1))
            )
        )
    )
)

(defun occurrence-must-precede (ex earlier-occ later-occ &key (depth 0) (prior-occs nil) &aux following-cons)
    (when (occurrence-earlier earlier-occ later-occ)
        (return-from occurrence-must-precede t)
    )
    (when (member earlier-occ prior-occs)
        (return-from occurrence-must-precede nil)
    )
    (case (get-precedence ex earlier-occ later-occ)
        ((t) (return-from occurrence-must-precede t))
        ((nil) (return-from occurrence-must-precede nil))
        (:unknown ) ; do nothing
        (otherwise (error "Unexpected value in map."))
    )
    (when (> depth (length (explanation-occurrence-precedence ex)))
        (error "Infinite recursion.")
    )
    (setq following-cons
        (remove earlier-occ 
            (explanation-occurrence-precedence ex)
            :key #'first
            :test-not #'occurrence-equal
        )
    )
    (when (member later-occ following-cons :key #'second)
        (let ((result (notevery #'third (remove later-occ following-cons :key #'second :test-not #'occurrence-equal))))
            (when result
                (set-precedence ex earlier-occ later-occ result)
                (return-from occurrence-must-precede result)
            )
        )
    )

    (dolist (f-con following-cons)
        (cond
            ((and (third f-con) 
                  (occurrence-must-precede ex (second f-con) later-occ 
                      :depth (1+ depth) 
                      :prior-occs (cons earlier-occ prior-occs)
                  )
             )
                (set-precedence ex earlier-occ later-occ t)
                (return-from occurrence-must-precede t)
            )
            ((and (not (third f-con)) (occurrence-must-not-follow ex (second f-con) later-occ :depth (1+ depth)))
                (set-precedence ex earlier-occ later-occ t)
                (return-from occurrence-must-precede t)
            )
        )
    )
    (set-precedence ex earlier-occ later-occ nil)
    nil
)

(defun get-precedence (ex earlier-occ later-occ)
    (gethash (cons earlier-occ later-occ) (explanation-cached-precedence-map ex) :unknown)
)

(defun set-precedence (ex earlier-occ later-occ val)
    (setf (gethash (cons earlier-occ later-occ) (explanation-cached-precedence-map ex)) val)
)


(defsetf get-precedence set-precedence)


(defun occurrence-must-not-follow (ex earlier-occ later-occ &key (depth 0) (prior-occs nil) &aux following-cons)
    (when (member earlier-occ prior-occs)
        (return-from occurrence-must-not-follow nil)
    )
    (when (> depth (length (explanation-occurrence-precedence ex)))
        (error "Infinite recursion.")
    )
    (setq following-cons
        (remove earlier-occ 
            (explanation-occurrence-precedence ex)
            :key #'first
            :test-not #'occurrence-equal
        )
    )
    (when (member later-occ following-cons :key #'second)
        (return-from occurrence-must-not-follow t) 
    )
    (dolist (f-con following-cons)
        (cond
            ; ((eq (second f-con) later-occ)
                ; (return-from occurrence-must-not-follow t)
            ; )
            ((occurrence-must-not-follow ex (second f-con) later-occ 
                 :depth (1+ depth)
                 :prior-occs (cons earlier-occ prior-occs)
             )
                (return-from occurrence-must-not-follow t)
            )
        )
    )
    nil
)

(defun modify-occurrence-signature (occ new-sig)
    (setf (occurrence-signature occ) new-sig)
    (setf (occurrence-hash-val occ) nil)
)

#-SBCL
(defun find-occurrence-hash (occ)
    (sxhash (occurrence-signature occ))
)    

#+SBCL
(defun find-occurrence-hash (occ)
    (sbcl-hash-list (occurrence-signature occ))
)    

(defun sbcl-hash-list (lst &aux hash-value)
    (setq hash-value (sxhash (car lst)))
    (setq hash-value (hash-manip hash-value))
    (dolist (item (cdr lst))
        (setq hash-value (fixnum-left-shift-4 hash-value))
        (incf hash-value (hash-arg item))
        (setq hash-value (hash-manip hash-value))
    )
    hash-value
)
    
    

(defun occurrence-hash (occ)
    (when (null (occurrence-hash-val occ))
        (setf (occurrence-hash-val occ)
            (find-occurrence-hash occ)
        )
    )
    (occurrence-hash-val occ)
)

;; Combines remove-duplicates and any. If any pair of distinct objects in the
;; list satisfy test (which must be commutative), returns t without checking
;; any further.
(defun any-duplicates (lst &key (test #'eq) &aux head)
    (loop while lst do
        (setq head (pop lst))
        (dolist (tail-item lst)
            (when (funcall test head tail-item)
                (return-from any-duplicates t)
            )
        )
    )
    nil
)


(defun make-precedence-cache-map ()
#-(or CLISP ABCL) (make-hash-table :test 'equal-occ-pair :hash-function 'hash-occ-pair)
#+(or CLISP ABCL) (make-hash-table :test 'equalp)
)

#-SBCL
(defun hash-occ-pair (occ-pair) 
    (+ (occurrence-hash (car occ-pair))
       (occurrence-hash (cdr occ-pair))
    )
)

#+SBCL
(defun hash-occ-pair (occ-pair)
    (mod
        (+ (occurrence-hash (car occ-pair))
           (occurrence-hash (cdr occ-pair))
        )
        most-positive-fixnum
    )
)

#+SBCL(sb-ext::define-hash-table-test equal-occ-pair hash-occ-pair)

