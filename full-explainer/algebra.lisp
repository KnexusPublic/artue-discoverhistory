 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: April 8, 2011
 ;;;; Project Name: Explanation for Planning
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file provides the capability to manipulate mathematical lisp
 ;;;; equations.
 
(in-package :artue)

(defun solve-for (var lhs rhs &aux new-rhs new-lhs)
    (when (eq rhs var)
        (return-from solve-for lhs)
    )
    (when (not (listp rhs))
        (error "No ~A on right-hand side in solve-for: ~A = ~A" var lhs rhs)
    )
    (when (and (eq '+ (car rhs)) (member var (flatten rhs)))
        (when (null (cdr rhs))
            (error "Right-hand-side is empty in solve-for: ~A ~A = ~A" var lhs rhs) 
        )
        (when (null (cddr rhs))
            (return-from solve-for (solve-for var lhs (second rhs)))
        )
        (setq new-lhs nil)
        (setq new-rhs nil)
        (dolist (addend (cdr rhs))
            (if (member var (flatten addend))
                ;then
                (if (null new-rhs)
                    ;then
                    (setq new-rhs addend)
                    ;else
                    (error "solve-for can't handle: ~A ~A ~A" var lhs rhs)
                )
                ;else
                (push addend new-lhs)
            )
        )
        (when (null new-rhs)
            (error "Lost the var: ~A ~A ~A" var lhs rhs)
        )
        (setq new-lhs (cons '- (cons lhs new-lhs)))
        (return-from solve-for (solve-for var new-lhs new-rhs))
    )
    (when (and (eq '* (car rhs)) (member var (flatten rhs)))
        (setq new-lhs nil)
        (setq new-rhs nil)
        (dolist (addend (cdr rhs))
            (if (member var (flatten addend))
                ;then
                (if (null new-rhs)
                    ;then
                    (setq new-rhs addend)
                    ;else
                    (error "solve-for can't handle: ~A ~A ~A" var lhs rhs)
                )
                ;else
                (push addend new-lhs)
            )
        )
        (when (null new-rhs)
            (error "Lost the var: ~A ~A ~A" var lhs rhs)
        )
        (setq new-lhs (cons '/ (cons lhs new-lhs)))
        (return-from solve-for (solve-for var new-lhs new-rhs))
    )
    (when (eq '- (car rhs))
        (when (null (cdr rhs))
            (error "Right-hand-side is empty in solve-for:~A ~A ~A" var lhs rhs) 
        )
        (when (null (cddr rhs))
            (return-from solve-for (solve-for var (cons '- lhs) (second rhs)))
        )
        (when (member var (flatten (second rhs)))
            (when (member var (flatten (cddr rhs)))
                (error "solve-for can't handle: ~A ~A ~A" var lhs rhs)
            )
            (setq new-lhs (cons '+ (cons lhs (cddr rhs))))
            (setq new-rhs (second rhs))
            (return-from solve-for (solve-for var new-lhs new-rhs))
        )
        (when (member var (flatten (cddr rhs)))
            (setq new-lhs (list '- (second rhs) lhs))
            (setq new-rhs (cons '+ (cddr rhs)))
            (return-from solve-for (solve-for var new-lhs new-rhs))
        )
    )
    (when (eq '/ (car rhs))
        (when (null (cdr rhs))
            (error "Right-hand-side is empty in solve-for:~A ~A ~A" var lhs rhs) 
        )
        (when (null (cddr rhs))
            (error "solve-for can't handle: ~A ~A ~A" var lhs rhs)
        )
        (when (member var (flatten (second rhs)))
            (when (member var (flatten (cddr rhs)))
                (error "solve-for can't handle: ~A ~A ~A" var lhs rhs)
            )
            (setq new-lhs (cons '* (cons lhs (cddr rhs))))
            (setq new-rhs (second rhs))
            (return-from solve-for (solve-for var new-lhs new-rhs))
        )
        (when (member var (flatten (cddr rhs)))
            (setq new-lhs (list '/ (second rhs) lhs))
            (setq new-rhs (cons '* (cddr rhs)))
            (return-from solve-for (solve-for var new-lhs new-rhs))
        )
    )
    (error "solve-for not yet implemented")
    ; (if (and (eq var '?__dur) (equal rhs '(+ ?__old-val ?__dur)))
        ; ;then
        ; (list '- lhs '?__old-val)
        ; ;else
        ; (error "solve-for not yet implemented")
    ; )
)

