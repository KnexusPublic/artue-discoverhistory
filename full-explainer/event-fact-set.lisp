 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: July 1, 2013
 ;;;; Project Name: Explanation
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file provides the fact-set class and associated functions for fast 
 ;;;; retrieval of facts.

(in-package :artue)

(defstruct (fact-set (:copier copy-fact-set-useless)) 
    (fact-hash (make-hash-table :size 20))
)

(defvar *empty-fact-set* (make-fact-set))

(defun add-all-to-fact-set (facts fact-set)
    (dolist (fact facts)
        (add-to-fact-set fact fact-set)
    )
)

(defun new-fact-set (facts &aux fset)
    (setq fset (make-fact-set))
    (add-all-to-fact-set facts fset)
    fset
)


(defun find-fact-list (fact fact-set create-if-missing &aux fhash)
    (find-fact-list-separated (fact-type fact) (fact-args fact) fact-set create-if-missing)
)

(defun find-fact-list-separated (f-type f-args fact-set create-if-missing &aux fhash)
    (setq fhash (gethash f-type (fact-set-fact-hash fact-set)))
    (when (null fhash)
        (when (not create-if-missing)
            (return-from find-fact-list-separated nil)
        )
        #+SBCL (setq fhash (make-hash-table :test 'eq-safe-num :size 20))
        #+ALLEGRO (setq fhash (make-hash-table :test 'eq-safe-num :hash-function 'hash-safe-num :size 20))
        #-(or SBCL ALLEGRO) (setq fhash (make-hash-table :test 'equalp :size 20))
        (setf (gethash :all fhash) (list :all))
        (setf (gethash :copies fhash) 1) 
        (setf (gethash f-type (fact-set-fact-hash fact-set)) 
            fhash
        )
    )
    (if (or (numberp (car f-args)) (symbolp (car f-args)) (var-symbol-p (car f-args)))
        ;then
        (values (gethash (car f-args) fhash) fhash)
        ;else
        (values (cdr (gethash :all fhash)) fhash)
    )
)

(defun remove-observables-from-fact-set (fact-set observable-types hidden-facts &aux fhash)
    (when hidden-facts
        (remove-all-from-fact-set hidden-facts fact-set)
    )
    (dolist (ob-type observable-types)
        (setq fhash (gethash ob-type (fact-set-fact-hash fact-set)))
        (when fhash
            (decf (gethash :copies fhash))
        )
        (remhash ob-type (fact-set-fact-hash fact-set))
        ; (let ((fhash (gethash ob-type (fact-set-fact-hash fact-set) :fail)))
            ; (when (not (eq :fail fhash))
                ; (clrhash fhash)
            ; )
        ; )
    )
)

(defun add-to-fact-set (fact fact-set &aux flist fhash all-list)
    (multiple-value-setq (flist fhash) (find-fact-list fact fact-set t))
    (cond
        ((< 1 (gethash :copies fhash))
            (setq fhash (copy-fact-hash fhash))
            (setf (gethash (fact-type fact) (fact-set-fact-hash fact-set)) fhash)
            (setf (gethash (car (fact-args fact)) fhash) (cons fact (copy-list flist)))
        )
        ((null flist) 
            (setf (gethash (car (fact-args fact)) fhash) 
                (list fact)
            )
        )
        (t 
            (setf (cdr flist) (cons fact (cdr flist)))
        )
    )
    (setq all-list (gethash :all fhash :fail))
    (when (listp all-list)
        (setf (cdr all-list) (cons fact (cdr all-list)))
    )
    fact-set
)

(defun remove-all-from-fact-set (facts fact-set)
    (dolist (fact facts)
        (remove-from-fact-set fact fact-set)
    )
)    

(defun remove-from-fact-set (fact fact-set &aux flist fhash)
    (multiple-value-setq (flist fhash) (find-fact-list fact fact-set nil))
    (when flist 
        (let ((new-flist (remove fact flist :test #'equal-fact))
              (copies (gethash :copies fhash)))
            (when (< (length new-flist) (length flist))
                (when (> copies 1)
                    (setq fhash (copy-fact-hash fhash))
                    (setf (gethash (fact-type fact) (fact-set-fact-hash fact-set)) fhash)
                    (setq new-flist (copy-list new-flist))
                )
                (setf (gethash (car (fact-args fact)) fhash) new-flist) 
                (remhash :all fhash)
            )
        )
    )
    fact-set
)

(defun find-in-fact-set (fact fact-set &key (test #'equal-fact))
    (find fact (find-fact-list fact fact-set nil) :test test)
)

(defun find-all-in-fact-set (fact fact-set &key (test #'equal-fact) &aux flist)
    (setq flist (find-fact-list fact fact-set nil))
    (remove fact flist :test-not test)
)

(defun find-same-map-in-facts (f-type f-args facts)
    (when (listp facts)
        (setq facts (remove f-type facts :key #'fact-type :test-not #'eq))
    )
    (when (fact-set-p facts)
        (setq facts (find-fact-list-separated f-type f-args facts nil))
    )
    (dolist (fact facts)
        (when (equalp f-args (fact-args fact))
            (return-from find-same-map-in-facts fact)
        )
    )
)

(defun find-all-same-map-in-facts (f-type f-args facts)
    (when (fact-set-p facts)
        (setq facts (find-fact-list-separated f-type f-args facts nil))
    )
    (setq facts (remove f-type facts :key #'fact-type :test-not #'eq))
    (remove f-args facts :key #'fact-args :test-not #'equalp)
    ;(dolist (fact facts)
    ;    (when (not (equalp f-args (fact-args fact)))
    ;        (setq facts (remove fact facts))
    ;    )
    ;)
    ;facts
)

(defun find-unifying-facts (expgen cnd fact-set blist &key (assume-fn #'assume-nothing))
    (remove-if-not
        #'(lambda (fact) (unify expgen cnd (list fact) blist :binds-var-symbols nil :assume-fn assume-fn))
        (get-facts-in-hash (gethash (cnd-type cnd) (fact-set-fact-hash fact-set)))
    )
)

(defun create-fact-set (facts &aux fact-set)
    (setq fact-set (make-fact-set))
    (add-all-to-fact-set facts fact-set)
    fact-set
)

(defun copy-fact-hash (fact-hash &aux new-hash)
#-ALLEGRO (setq new-hash (make-hash-table :size (hash-table-count fact-hash)
                                          :test (hash-table-test fact-hash)))
#+ALLEGRO (setq new-hash (make-hash-table :size (hash-table-count fact-hash)
                                          :test (hash-table-test fact-hash)
                                          :hash-function 'hash-safe-num))
    (maphash 
        #'(lambda (key val) 
            (when (listp val)
                (setf (gethash key new-hash)
                    (copy-list val)
                )
            )
          )
        fact-hash
    )
    (decf (gethash :copies fact-hash))
    (setf (gethash :copies new-hash) 1)
    new-hash
)

(defun copy-fact-set (fact-set &aux new-fset)
    (setq new-fset 
        (make-fact-set 
            :fact-hash
                (make-hash-table 
                    :size (hash-table-count (fact-set-fact-hash fact-set))
                    :test (hash-table-test (fact-set-fact-hash fact-set))
                )
        )
    )
    (maphash 
        #'(lambda (key val) 
            (setf (gethash key (fact-set-fact-hash new-fset)) val)
            (incf (gethash :copies val))
          )
        (fact-set-fact-hash fact-set)
    )
    new-fset
)

(defun get-facts-in-hash (fact-hash &aux (facts nil))
    (when (null fact-hash)
        (return-from get-facts-in-hash nil)
    )
    (let ((all-list (gethash :all fact-hash :fail)))
        (when (listp all-list)
            (return-from get-facts-in-hash (cdr all-list))
        )
    )
    (remhash :all fact-hash)
    (maphash 
        #'(lambda (key val) 
            (declare (ignore key))
            (when (listp val)
                (setq facts (append facts val))
            )
          )
        fact-hash
    )
    (setf (gethash :all fact-hash) (cons :all facts))
    facts
)

(defun get-facts (fact-set &aux (facts nil))
    (maphash 
        #'(lambda (key val) 
            (declare (ignore key))
            (setq facts (append facts (get-facts-in-hash val)))
          )
        (fact-set-fact-hash fact-set)
    )
    facts
)

(defun get-checked-facts (model fact-set &aux (facts nil))
    (maphash 
        #'(lambda (key val) 
            (when (is-checked-type model key)
                (setq facts (append facts (get-facts-in-hash val)))
            )
          )
        (fact-set-fact-hash fact-set)
    )
    facts
)

(defun is-checked-type (envmodel fact-type)
    (and (not (type-is-is fact-type))
         (not (member fact-type (envmodel-static envmodel)))
         (not (member fact-type (envmodel-internal envmodel)))
         (not (member fact-type (envmodel-hidden envmodel)))
    )
)
         

(defun get-type-facts (type-sym fact-set)
    (get-facts-in-hash (gethash type-sym (fact-set-fact-hash fact-set)))
)


(defun cross-product (fact-set-lists &optional (facts-so-far nil))
   (let 
    ((conjunct-sets nil))
      (if fact-set-lists
         (loop for fact-set in (car fact-set-lists) do
            (setq conjunct-sets
               (append 
                  conjunct-sets
                  (cross-product 
                     (cdr fact-set-lists) 
                     (append facts-so-far fact-set)
                  )
               )
            )
         )
         ;else
         (push facts-so-far conjunct-sets)
     )
     conjunct-sets
   )
)



(defstruct (fact-occs (:constructor make-fact-occs-1)) 
    (prior nil)
    (new nil)
    (requests 0)
    (total-depth 0)
)

(defun make-fact-occs (&key (prior nil))
    (cond
        ((null prior) (make-fact-occs-1))
        ((null (fact-occs-new prior)) (make-fact-occs-1 :prior (fact-occs-prior prior)))
        (t (make-fact-occs-1 :prior prior))
    )
)

(defun get-most-recent-occs (fact-occs fact &aux (ret nil) (depth 0))
    (when (fact-occs-new fact-occs)
        (setq ret (gethash fact (fact-occs-new fact-occs)))
    )
    (when (and (null ret) (null (fact-occs-prior fact-occs)))
        (setq ret (list nil))
    )
    (when (null ret)
        (multiple-value-setq (ret depth) 
            (get-most-recent-occs (fact-occs-prior fact-occs) fact)
        )
        (setq depth (1+ depth))
    )
    (incf (fact-occs-requests fact-occs))
    (incf (fact-occs-total-depth fact-occs) depth)
    (values ret depth)
)

(defun set-most-recent-occ (fact-occs fact occ)
    (when (null (fact-occs-new fact-occs))
        (setf (fact-occs-new fact-occs) (new-fact-hash-table))
    )
    (setf (gethash fact (fact-occs-new fact-occs)) (cons occ (gethash fact (fact-occs-new fact-occs))))
)




