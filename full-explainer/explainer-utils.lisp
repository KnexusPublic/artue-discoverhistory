 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: April 12, 2011
 ;;;; Project Name: Explanation
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file provides utility functions used by higher level explanation 
 ;;;; functions.
 
(in-package :artue)
 
(defun get-prior-observation (expgen)
    (find-if #'occurrence-is-observation (butlast (get-execution-history expgen)) :from-end t)
)

(defun get-last-observation (expgen)
    (find-if #'occurrence-is-observation (get-execution-history expgen) :from-end t)
)

(defun get-first-observation (expgen)
    (car (get-execution-history expgen))
)

(defun is-observable (expgen fact)
    (if (null (get-sense-pred-names (get-domain expgen)))
        ;then
        (not (is-hidden expgen fact))
        ;else
        (member (fact-type fact) (get-sense-pred-names (get-domain expgen)))
    )
)

(defun is-transient (expgen fact)
    (if (fact-p fact)
        ;then
        (member (fact-type fact) (get-transient-types expgen))
        ;else
        (cond
            ((listp (car fact)) (member (caar fact) (get-transient-types expgen)))
            ((eq (car fact) 'not) (member (caadr fact) (get-transient-types expgen)))
            (t (member (car fact) (get-transient-types expgen)))
        )
    )
)

(defun is-hidden (expgen fact)
    (cond
      ((fact-p fact)
        (if (get-hidden-facts expgen)
            ;then
            (member fact (get-hidden-facts expgen) :test #'equal-fact)
            ;else
            (member (fact-type fact) (envmodel-hidden (get-last-envmodel expgen)))
        )
      )
      ((cnd-p fact)
          (member (cnd-type fact) (envmodel-hidden (get-last-envmodel expgen)))
      )
      (t
        (cond
            ((listp (car fact)) (member (caar fact) (envmodel-hidden (get-last-envmodel expgen))))
            ((eq (car fact) 'not) (member (caadr fact) (envmodel-hidden (get-last-envmodel expgen))))
            (t (member (car fact) (envmodel-hidden (get-last-envmodel expgen))))
        )
      )
    )
)

(defun is-hidden-type (expgen type-sym)
    (member type-sym (envmodel-hidden (get-last-envmodel expgen)))
)

(defun non-physical-fact (fact)
    (member (fact-type fact) '(eval assign create is-string is-real is-int adds-to number real-var))
)

(defun get-default-fact (expgen fact &aux default-value)
    (setq default-value (get-default-value-fact expgen fact))
    (when (eq default-value 'fail)
        (return-from get-default-fact 'fail)
    )
    (make-fact :type (fact-type fact) :args (fact-args fact) :value default-value)
)

(defun has-default-value (expgen fact)
    (val-close-enough (fact-value fact) (get-default-value-fact expgen fact))
)

(defun is-internal (expgen fact)
    (member (fact-type fact) (envmodel-internal (get-last-envmodel expgen)))
)
    
(defun is-internal-cnd (expgen cnd)
    (member (cnd-type cnd) (envmodel-internal (get-last-envmodel expgen)))
)
    
(defun is-static (expgen cnd)
    (member (cnd-type cnd) (envmodel-static (get-last-envmodel expgen)))
)
    
(defun is-static-fact (expgen fact)
    (member (fact-type fact) (envmodel-static (get-last-envmodel expgen)))
)
    
(defun is-positive (cnd)
    (or (cnd-inequality cnd) (cnd-value cnd))
) 

(defun is-removal-occ (occ)
    (eq 'removal (car (occurrence-signature occ)))
)

(defun is-time-passes-occ (occ)
    (eq 'time-passes (car (occurrence-signature occ)))
)

(defun default-alternative-fact (expgen fact)
    (when (is-binary fact) 
        (if (and (not (is-hidden expgen fact)) (null (fact-value fact)))
            ;then
            (return-from default-alternative-fact nil)
            ;else
            (return-from default-alternative-fact (contradictory-fact fact))
        )
    )
    (let ((default (get-default-fact expgen fact)))
        (if (or (eq default 'fail) (equal-fact fact default))
            ;then
            nil
            ;else
            default
        )
    )
)

(defun is-legal-assumption (expgen cnd)
    (when (or (cnd-inequality cnd)
              (not (member (cnd-type cnd) (envmodel-hidden (get-last-envmodel expgen)))))
        (return-from is-legal-assumption nil)
    )
    (when (eq 'none (second (assoc (cnd-type cnd) (envmodel-assumption-likelihood (get-last-envmodel expgen)))))
        (return-from is-legal-assumption nil)
    )
    (when (not (is-legal-unknown-object-type expgen (cnd-is-is cnd)))
        (return-from is-legal-assumption nil)
    )
    ; (dolist (arg (cnd-args cnd))
        ; (when (and (var-symbol-p arg) (not (is-legal-unknown-object expgen arg)))
            ; (return-from is-legal-assumption nil)
        ; )
    ; )
    ; (when (and (var-symbol-p (cnd-value cnd)) (not (is-legal-unknown-object expgen (cnd-value cnd))))
        ; (return-from is-legal-assumption nil)
    ; )
    t
)

(defun is-legal-unknown-object-type (expgen var-type)
    (not 
        (eq 'none 
            (second
                (assoc 
                    var-type 
                    (envmodel-unknown-object-likelihood (get-last-envmodel expgen))
                )
            )
        )
    )
)

(defmethod is-assignable-from ((expgen discoverhistory-explanation-generator) ancestor descendant)
    (is-assignable-from (get-last-envmodel expgen) ancestor descendant)
)

(defmethod is-supertype ((expgen discoverhistory-explanation-generator) ancestor descendant)
    (is-supertype (get-last-envmodel expgen) ancestor descendant)
)

(defun is-assumable (expgen fact)
    (or (null (fact-value fact))
        (and (is-hidden expgen fact) (is-legal-assumption expgen (fact-to-cnd fact)))
        (eq *true-fact* fact)
    )
)

(defun was-assumable (expgen fact old-facts)
    (or (member fact old-facts :test #'equal-fact)
        (and (is-assumable expgen fact) 
             (not (member fact old-facts :test #'same-map))
        )
    )
)


(defun explanation-assumptions (ex)
    (mapcar #'first 
        (mapcar #'occurrence-pre 
            (remove-if-not #'is-assumption-occ (get-events ex))
        )
    )
)

(defun is-assumption-occ (occ)
    (occurrence-is-assumption occ)
)

(defun is-special-occ (occ)
    (or (is-assumption-occ occ) (is-removal-occ occ))
)

(defun has-default (expgen fact)
    (or (is-binary fact) 
        (assoc (fact-type fact) (envmodel-defaults (get-last-envmodel expgen)))
    )
)

(defun is-default (expgen fact &aux pair)
    (setq pair (assoc (fact-type fact) (envmodel-defaults (get-last-envmodel expgen))))
    (when (and (null pair) (is-binary fact))
        (return-from is-default (null (fact-value fact)))
    )
    (if (null pair)
        ;then
        (return-from is-default nil)
        ;else
        (return-from is-default (eq (fact-value fact) (second pair)))
    )
)

(defgeneric get-default-value-cnd (model obj))

(defmethod get-default-value-cnd ((envmodel envmodel) obj &aux ret)
     (setq ret (assoc (cnd-type obj) (envmodel-defaults envmodel)))
     (if (null ret)
         ;then
         'fail
         ;else
         (second ret)
     )
)

(defmethod get-default-value-cnd ((expgen discoverhistory-explanation-generator) obj)
    (gethash (cnd-type obj) (get-default-map expgen) 'fail)
)

(defun get-default-value-fact (expgen obj)
    (gethash (fact-type obj) (get-default-map expgen) 'fail)
)

(defmethod get-default-value-type ((expgen discoverhistory-explanation-generator) type-sym)
    (gethash type-sym (get-default-map expgen) 'fail)
)

(defun no-event-can-satisfy (expgen cnd)
    (or (and (cnd-value cnd) (not (member (cnd-type cnd) (get-causables expgen)) ))
        (and (null (cnd-value cnd)) (not (member (cnd-type cnd) (get-removables expgen)) ))
    )
)

(defun names-in (eqn)
    (dolist (sym (cdr eqn))
        (cond
            ((listp sym) (when (names-in sym) (return-from names-in t)))
            ((not (numberp sym)) (return-from names-in t))
        )
    )
)

(defun next-point (expgen point-type)
    (make-point expgen point-type)
)

(defun embed-expgen (fn expgen)
    #'(lambda (&rest args) (apply fn expgen args))
)

(defun rerun-explanation-history (expgen ex orig-ex &key (hist nil) &aux inc-set cur-inc cur-exes ret-ex)
    (when (null hist)
        (setq hist (explanation-history ex))
    )
    (setq ret-ex orig-ex)
    (dolist (tuple (reverse hist))
        (setq inc-set (find-inconsistencies expgen ret-ex))
        ;(setq inc-set (select-inconsistencies expgen ret-ex (find-inconsistencies expgen ret-ex)))
        (when (null inc-set)
            (setq ret-ex (find-extra-events expgen ret-ex))
            (setf (explanation-last-added-assumption ret-ex) nil)
            (setf (explanation-last-added-event ret-ex) nil)
            (setq inc-set (find-inconsistencies expgen ret-ex))
            ;(setq inc-set (select-inconsistencies expgen ret-ex (find-inconsistencies expgen ret-ex)))
        )
        (setq cur-inc 
            (find (getf tuple :inc) inc-set
                :test #'equivalent-inconsistency
            )
        )
        (when (null cur-inc)
            (error "inconsistency did not recur!")
        )
        (setq cur-exes (funcall (getf tuple :refine-type) expgen ret-ex cur-inc))
        (setq cur-exes
            (remove-if-not 
                #'(lambda (ex) 
                    (equivalent-refinements-lax tuple 
                        (first (explanation-history ex))
                    )
                  )
                cur-exes
            )
        )
        (when (null cur-exes)
            (error "no refinements for past inconsistency!")
        )
        (when (cdr cur-exes)
            (format t "~%Multiple explanations refine explanation:~%~A~%Existing refinement:~%~A"
                ret-ex
                tuple
            )
            
            (print-one-per-line
                (mapcar #'car (mapcar #'explanation-history cur-exes))
            )
            (finish-output)
            (let ((user-input nil))
                (loop while (or (not (integerp user-input)) (null (nth user-input cur-exes))) do
                    (format t "~%Please select by number. > ")
                    (setq user-input (read *terminal-io*))
                )
                (setq ret-ex (nth user-input cur-exes))
            )
        )
        (when (null (cdr cur-exes))
            (setq ret-ex (car cur-exes))
        )
    )
    ret-ex
)

(defun get-explanation-refinements (new-ex prior-ex)
    (list
        (list :additions (set-difference (explanation-new-events new-ex) (explanation-new-events prior-ex)))
        (list :removals (set-difference (explanation-event-removals new-ex) (explanation-event-removals prior-ex)))
        (list :bindings (set-difference (explanation-bindings new-ex) (explanation-bindings prior-ex)))
    )
)

(defun get-num-symbols-in-equations (eqns)
    (remove-duplicates-speedy 
        (remove-if-not #'num-sym-p (flatten eqns))
    )
)



(defun get-var-symbols (tree)
    (remove-duplicates-speedy (get-var-symbols1 tree))
)

(defun get-var-symbols1 (tree)
    (when (var-symbol-p tree)
        (return-from get-var-symbols1
            (list tree)
        )
    )
    (when (consp tree)
        (return-from get-var-symbols1
            (append (get-var-symbols1 (car tree)) 
                    (get-var-symbols1 (cdr tree))
            )
        )
    )
    
    (when (cnd-p tree)
        (return-from get-var-symbols1
            (append (get-var-symbols1 (cnd-args tree)) 
                    (get-var-symbols1 (cnd-value tree))
            )
        )
    )
    (when (fact-p tree)
        (return-from get-var-symbols1
            (append (get-var-symbols1 (fact-args tree)) 
                    (get-var-symbols1 (fact-value tree))
            )
        )
    )
    (when (occurrence-p tree)
        (when (eq (occurrence-var-syms tree) :unknown)
            (setf (occurrence-var-syms tree)
                (remove-duplicates 
                    (append
                        (get-var-symbols1 (occurrence-constraints tree)) 
                        (get-var-symbols1 (occurrence-pre tree)) 
                        (get-var-symbols1 (occurrence-post tree)) 
                    )
                )
            )
        )
        (return-from get-var-symbols1 (occurrence-var-syms tree))
    )
    (when (inconsistency-p tree)
        (return-from get-var-symbols1
            (append (get-var-symbols1 (inconsistency-next-occ tree)) 
                    (get-var-symbols1 (inconsistency-prior-occ tree))
                    (get-var-symbols1 (inconsistency-cnd tree))
            )
        )
    )
    (remove-if-not #'var-symbol-p (flatten tree))
)

(defun get-variable-terms (tree)
    (remove-duplicates-speedy (get-variable-terms1 tree))
)

(defun get-variable-terms1 (tree)
    (when (is-variable-term tree)
        (return-from get-variable-terms1
            (list tree)
        )
    )
    (when (consp tree)
        (return-from get-variable-terms1
            (append (get-variable-terms1 (car tree)) 
                    (get-variable-terms1 (cdr tree))
            )
        )
    )
    
    (when (cnd-p tree)
        (return-from get-variable-terms1
            (append (get-variable-terms1 (cnd-args tree)) 
                    (get-variable-terms1 (cnd-value tree))
            )
        )
    )
    (when (fact-p tree)
        (return-from get-variable-terms1
            (append (get-variable-terms1 (fact-args tree)) 
                    (get-variable-terms1 (fact-value tree))
            )
        )
    )
    (when (occurrence-p tree)
        (return-from get-variable-terms1
            (append
                (get-variable-terms1 (occurrence-constraints tree)) 
                (get-variable-terms1 (occurrence-pre tree)) 
                (get-variable-terms1 (occurrence-post tree)) 
            )
        )
    )
    (when (inconsistency-p tree)
        (return-from get-variable-terms1
            (append (get-variable-terms1 (inconsistency-next-occ tree)) 
                    (get-variable-terms1 (inconsistency-prior-occ tree))
                    (get-variable-terms1 (inconsistency-cnd tree))
            )
        )
    )
    (remove-if-not #'is-variable-term (flatten tree))
)


(defun get-cnd-var-type (expgen cnd var)
    (find-var-type-from-cnd cnd var (get-predicates (get-last-envmodel expgen)))
)

(defun intersection-exists (lst1 lst2)
    (dolist (elema lst1)
        (dolist (elemb lst2)
            (when (eq elema elemb)
                (return-from intersection-exists t)
            )
        )
    )
    nil
)


(defun remove-duplicates-speedy (lst)
    (if (null lst)
        ;then
        (return-from remove-duplicates-speedy nil)
        ;else
        (return-from remove-duplicates-speedy (remove-duplicates-speedy1 (car lst) (cdr lst)))
    )
)

(defun remove-duplicates-speedy1 (elem lst)
    (loop while lst do
        (when (not (eq (car lst) elem))
            (return-from remove-duplicates-speedy1 (remove-duplicates-speedy2 elem (car lst) (cdr lst)))
        )
        (setq lst (cdr lst))
    )
    (list elem)
)

(defun remove-duplicates-speedy2 (elem1 elem2 lst)
    (loop while lst do
        (when (and (not (eq (car lst) elem1)) (not (eq (car lst) elem2)))
            (return-from remove-duplicates-speedy2 (remove-duplicates-speedy3 elem1 elem2 (car lst) (cdr lst)))
        )
        (setq lst (cdr lst))
    )
    (list elem1 elem2)
)


(defun remove-duplicates-speedy3 (elem1 elem2 elem3 lst)
    (loop while lst do
        (when (and (not (eq (car lst) elem1)) (not (eq (car lst) elem2)) (not (eq (car lst) elem3)))
            (return-from remove-duplicates-speedy3 (remove-duplicates-speedy4 elem1 elem2 elem3 (car lst) (cdr lst)))
        )
        (setq lst (cdr lst))
    )
    (list elem1 elem2 elem3)
)


(defun remove-duplicates-speedy4 (elem1 elem2 elem3 elem4 lst &aux new-lst)
    (setq new-lst (list elem1 elem2 elem3 elem4))
    (dolist (new-elem lst)
        (when (not (member new-elem new-lst))
            (push new-elem new-lst)
        )
    )
    new-lst
)

(defun find-unresolvable-inconsistencies (expgen ex &aux (ret-incs nil))
    (dolist (inc (find-inconsistencies expgen ex))
        (when (null (refine-inconsistency expgen ex inc))
            (push inc ret-incs)
        )
    )
    ret-incs
)

(defun display-history (expgen ex)
    (print-one-per-line 
        (sort 
            (copy-list 
                (append 
                    (get-execution-history expgen) 
                    (get-events ex)
                )
            )
            #'occurrence-earlier
        )
    )
)

(defun find-prior-occ-point-in (expgen pt evs)
    (when (find pt evs :key #'occurrence-point) 
        (return-from find-prior-occ-point-in (point-prior expgen pt))
    )
    (return-from find-prior-occ-point-in
        (find-prior-occ-point-in (point-prior expgen pt) evs)
    )
)
