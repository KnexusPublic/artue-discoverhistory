 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: April 8, 2018
 ;;;; Project Name: Explanation
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file provides unification of events with states.

(in-package :artue)
 
(defvar *trace-unify* nil)
(defvar *variable-events* nil)

(defgeneric is-assignable-from (model ancestor descendant))
(defgeneric is-supertype (model ancestor descendant))

(defun satisfies-condition (model cnd occ)
    (unify model cnd (occurrence-post occ) nil :assume-fn #'assume-nothing)
)

(defun unify-set (model cnd-set fact-set binding-list 
                  &key (return-deferred nil) (assume-fn #'assume-closed-world)
                       (binds-var-symbols nil) (ground-all-vars nil) 
                  &aux (current-id *next-symbol-id*) (result-blists nil)
                       (revised-result-blists nil) (new-blist-pairs nil))
    (setq result-blists
        (unify-set1 model cnd-set fact-set binding-list 0
            :return-deferred return-deferred
            :assume-fn assume-fn
            :binds-var-symbols binds-var-symbols
            :ground-all-vars ground-all-vars
        )
    )
    
    (dolist (blist-pair result-blists)
        (when (var-symbols-in (mapcar #'car (car blist-pair)))
            (when *trace-unify* (format t "~%~%Retesting blist ~A." (car blist-pair)))
            (setq new-blist-pairs
                (unify-set1 model 
                    (remove 'assign cnd-set :key #'cnd-type) 
                    fact-set (car blist-pair) 0
                    :return-deferred return-deferred
                    :assume-fn assume-fn
                    :binds-var-symbols binds-var-symbols
                    :ground-all-vars ground-all-vars
                )
            )
            (when (null new-blist-pairs)
                (setq blist-pair nil)
            )
        )
        (when blist-pair 
            (setf (car blist-pair)
                (remove-new-var-sym-bindings
                    (car blist-pair)
                    current-id
                )
            )
            (dolist (vsym (get-var-symbols (car blist-pair)))
                (when (var-symbol-temporary vsym)
                    (if binds-var-symbols
                        ;then
                        (setf (var-symbol-temporary vsym) nil)
                        ;else
                        (if (find-if 
                                #'(lambda (cnd) 
                                    (and (eq (cnd-type cnd) 'create)
                                         (member (cons (second (cnd-value cnd)) vsym)
                                             (car blist-pair) :test #'equalp
                                         )
                                    )
                                  )
                                cnd-set
                            )
                            ;then
                            (setf (var-symbol-temporary vsym) nil)
                            ;else
                            (setq blist-pair nil)
                        )
                    )
                )
            )
            (when blist-pair
                (push blist-pair revised-result-blists)
            )
        )
    )

    (if return-deferred
        ;then
        revised-result-blists
        ;else
        (mapcar #'first
            revised-result-blists
        )
    )
)

(defun remove-new-var-sym-bindings (blist latest-id &aux ret)
    (setq ret
        (remove-if 
            #'(lambda (bind-target) 
                (and (var-symbol-p bind-target)
                     (var-symbol-temporary bind-target)
                     ; (integerp (var-symbol-id bind-target))
                     ; (> (var-symbol-id bind-target) latest-id)
                )
              )
            blist 
            :key #'car
        )
    )
    ret
)

(defun unify-set1 (model cnd-set fact-set binding-list defer-count 
                   &key (return-deferred nil) (assume-fn #'assume-closed-world)
                        (binds-var-symbols nil) (ground-all-vars nil)
                   &aux ret-blists temp-fact-set)
    (when (null cnd-set)
        (return-from unify-set1 (list (list binding-list nil)))
    )
    
    (when (> defer-count (length cnd-set))
        (if return-deferred
            ;then
            (return-from unify-set1 (list (list binding-list cnd-set)))
            ;else
            (return-from unify-set1 nil)
        )
    )
    
    (multiple-value-bind (blist-set inst)
            (unify model (car cnd-set) fact-set binding-list 
                :assume-fn assume-fn
                :binds-var-symbols binds-var-symbols
                :ground-all-vars ground-all-vars
            )
        
        (when (eq inst 'defer)
            (return-from unify-set1
                (unify-set1 
                    model
                    (append (cdr cnd-set) (list (car cnd-set))) 
                    fact-set binding-list 
                    (+ 1 defer-count)
                    :assume-fn assume-fn
                    :return-deferred return-deferred
                    :binds-var-symbols binds-var-symbols
                    :ground-all-vars ground-all-vars
                )
            )
        )
        
;        (setq blist-set (remove-duplicates blist-set :test #'equivalent-bindings))
        
;        (when *trace-unify* (break "before type-is-is"))
        ; (when (and (null *hidden-facts*)
                   ; (type-is-is (cnd-type (car cnd-set)))
                   ; (every assume-fn 
                       ; (remove-if #'type-is-is (cdr cnd-set) :key #'cnd-type)
                   ; )
                   ; (not (member (car (cnd-args (car cnd-set))) 
                                ; (mapcar #'car binding-list)
                        ; )
                   ; )
              ; )
            ; (when *variable-events* (setq blist-set nil))
            ; (setq blist-set 
                ; (cons (cons (cons (car (cnd-args (car cnd-set))) 
                                  ; (new-var-sym (type-is-is (cnd-type (car cnd-set))))
                            ; )
                            ; binding-list
                      ; )
                      ; blist-set
                ; )
            ; )
        ; )
        (setq ret-blists nil)
        (dolist (blist blist-set)
            (when (or binds-var-symbols (binds-no-var-symbols blist))
                (setq temp-fact-set fact-set)
                (when (and (eq inst 'assume) (member (cnd-type (car cnd-set)) (cdr cnd-set) :key #'cnd-type))
                    (setq temp-fact-set (copy-fact-set fact-set))
                    (multiple-value-bind (new-facts new-blist)
                            (bind-condition-to-fact model (car cnd-set) blist fact-set)
                        (add-all-to-fact-set new-facts temp-fact-set)
                        (setq blist new-blist)
                    )
                )
                (setq ret-blists 
                    (append ret-blists 
                        (unify-set1 model (cdr cnd-set) temp-fact-set blist 
                            0
                            :assume-fn assume-fn
                            :return-deferred return-deferred
                            :binds-var-symbols binds-var-symbols
                            :ground-all-vars ground-all-vars
                        )
                    )
                )
            )
        )
    )
    ret-blists
)
        
(defun binds-no-var-symbols (blist)
    (notany #'(lambda (binding) (and (var-symbol-p (car binding)) (not (var-symbol-temporary (car binding))))) blist)
)

        

(defun equivalent-bindings (blist1 blist2)
    (when (and (null blist1) (null blist2))
        (return-from equivalent-bindings t)
    )
    (when (or (null blist1) (null blist2))
        (return-from equivalent-bindings nil)
    )
    (let* ((var1 (caar blist1))
           (val1 (cdar blist1))
           (val2 (cdr (assoc var1 blist2))))
        (cond 
            ((eq val1 val2) (equivalent-bindings (cdr blist1) (remove var1 blist2 :key #'car)))
            ((and (var-symbol-p val1) (var-symbol-p val2) (var-symbol-equivalent val1 val2))
                (equivalent-bindings (cdr blist1) (remove var1 blist2 :key #'car))
            )
            (t nil)
        )
    )
)
    
(defun unify (model cnd fact-set binding-list &key (assume-fn #'assume-closed-world) (binds-var-symbols t) (ground-all-vars nil))
    (when *trace-unify*
        (format t "~%Unify: ~A ~A" cnd binding-list)
    )
    (case (cnd-type cnd)
        (adds-to (unify-cnd-adds-to model cnd fact-set binding-list))
        (number (unify-cnd-number model cnd fact-set binding-list))
        (real-var (unify-cnd-real-var model cnd fact-set binding-list assume-fn ground-all-vars))
        (create (unify-cnd-create model cnd fact-set binding-list))
        (assign (unify-cnd-assign model cnd fact-set binding-list))
        (eval (unify-cnd-eval model cnd fact-set binding-list))
        (forall (unify-cnd-forall model cnd fact-set binding-list assume-fn binds-var-symbols))
        (otherwise (default-unify-cnd model cnd fact-set binding-list assume-fn binds-var-symbols ground-all-vars))
    )
)


(defun unify-cnd-adds-to (model cnd fact-set binding-list)
    (declare (ignore fact-set model))
    (when (null binding-list)
        (when *trace-unify*
            (format t "~%Unify1: <== fail")
        )
        (return-from unify-cnd-adds-to nil)
    )
    (setq binding-list (unify-adds-to cnd binding-list))
    (when *trace-unify*
        (format t "~%Unify2: <== ~A" binding-list)
    )
    (cond ((eq binding-list 'fail) nil)
          ((eq binding-list 'defer) (values nil 'defer))
          (t (list binding-list))
    )
)

(defun unify-cnd-number (model cnd fact-set binding-list)
    (declare (ignore fact-set model))
    (setq binding-list 
        (bind-value 
            cnd 
            (make-fact 
                :type 'number 
                :args (cnd-args cnd) 
                :value (first (cnd-args cnd))
            ) 
            binding-list
            nil
        )
    )
    (when *trace-unify*
        (format t "~%Unify-num: <== ~A" binding-list)
    )
    (cond ((eq binding-list 'fail) nil)
          ((eq binding-list 'defer) (values nil 'defer))
          (t (list binding-list))
    )
)

(defun unify-cnd-real-var (model cnd fact-set binding-list assume-fn ground-all-vars
                           &aux val ret-bindings)
    (when (and fact-set 
               (listp fact-set)
               (null (cdr fact-set))
               (eq (fact-type (car fact-set)) 'real-var)
          )
        (return-from unify-cnd-real-var (default-unify-cnd model cnd fact-set binding-list assume-fn nil ground-all-vars))
    )
    (setq val (cdr (assoc (first (cnd-args cnd)) binding-list)))
    
    (when (and (null val) (null (cnd-inequality cnd)))
        (setq val (cdr (assoc (cnd-value cnd) binding-list)))
    )
    (when (or (null val) 
              (and (cnd-inequality cnd) 
                   (null (assoc (cnd-value cnd) binding-list))
              )
          )
        (when *trace-unify*
            (format t "~%Unify-real-var: <== defer")
        )
        (return-from unify-cnd-real-var (values nil 'defer))
    )
    (setq ret-bindings
        (unify model cnd 
            (list 
                (make-fact 
                    :type 'real-var 
                    :args (list val) 
                    :value val
                ) 
            )
            binding-list
            :assume-fn assume-fn   
        )
    )
    (when *trace-unify*
        (format t "~%Unify-real-var: <== ~A" ret-bindings)
    )
    
    (if (eq ret-bindings 'fail) nil ret-bindings)
)

(defun unify-cnd-create (model cnd fact-set binding-list &aux fn-call val)
    (declare (ignore fact-set model))
    ;; creates can't be directly replicated in though, because they create 
    ;; new objects. This is therefore bypassed.
    ; (setq fn-call (third (cnd-value cnd)))
    ; (when (subsetp (remove-if-not #'is-variable-term fn-call) (mapcar #'car binding-list))
        ; (setq fn-call (sublis binding-list fn-call))
        ; (when (var-symbols-in fn-call)
            ; (setq val (new-var-sym (first (cnd-value cnd))))
            ; (setf (var-symbol-eqn val) fn-call)
        ; )
        ; (when (null val)
            ; (setq val (eval fn-call))
        ; )
        ; (setq binding-list (cons (cons (second (cnd-value cnd)) val) binding-list))
        ; (when *trace-unify*
            ; (format t "~%Unify-create: <== ~A" binding-list)
        ; )
        ; (return-from unify-cnd-create 
            ; (list binding-list)
        ; )
    ; )
    ; (when *trace-unify*
        ; (format t "~%Unify-create: <== defer")
    ; )
    (setq val (new-var-sym (first (cnd-value cnd))))
    (setf (var-symbol-rooted val) (var-symbol-name val))
    (setq binding-list (cons (cons (second (cnd-value cnd)) val) binding-list))
    (when *trace-unify*
        (format t "~%Unify-create: <== ~A" binding-list)
    )
    (return-from unify-cnd-create 
        (list binding-list)
    )
    ;(values nil 'defer)
)


(defun unify-cnd-assign (model cnd fact-set binding-list)
    (declare (ignore fact-set model))
    (setq binding-list (unify-assign cnd binding-list))
    (when *trace-unify*
        (format t "~%Unify4: <== ~A" binding-list)
    )
    (cond ((eq binding-list 'fail) nil)
          ((eq binding-list 'defer) (values nil 'defer))
          (t (list binding-list))
    )
)


(defun unify-cnd-eval (model cnd fact-set binding-list)
    (declare (ignore fact-set model))
    (when (null binding-list)
        (when *trace-unify*
            (format t "~%Unify3a: <== fail")
        )
        (return-from unify-cnd-eval 'fail)
    )
    (setq binding-list (unify-assign cnd binding-list))
    (when *trace-unify*
        (format t "~%Unify4a: <== ~A" binding-list)
    )
    (cond ((eq binding-list 'fail) nil)
          ((eq binding-list 'defer) (values nil 'defer))
          (t (list binding-list))
    )
)

(defun unify-cnd-forall (model cnd fact-set binding-list assume-fn binds-var-symbols)
    (unify-forall model cnd fact-set binding-list :assume-fn assume-fn :binds-var-symbols binds-var-symbols)
)
    

(defun unify-is (model cnd binding-list ground-all-vars &aux var)
    (setq var (cdr (assoc (car (cnd-args cnd)) binding-list)))
    (when (and (null var) (is-variable-term (car (cnd-args cnd))))
        (when ground-all-vars (return-from unify-is :no-behavior)) 
        (let ((new-vsym (new-var-sym (type-is-is (cnd-type cnd)))))
            (when *trace-unify*
                (format t "~%Unify6c: <== ~A" 
                    (list
                        (cons (cons (car (cnd-args cnd)) new-vsym)
                            binding-list
                        )
                    )
                )
            )
            (return-from unify-is
                (list
                    (cons (cons (car (cnd-args cnd)) new-vsym)
                        binding-list
                    )
                )
            )
        )
    )
    (when (and (null var) (var-symbol-p (car (cnd-args cnd))) (null (cnd-value cnd)))
        (setq var (car (cnd-args cnd)))
    )
    (when (and var (var-symbol-p var))
        (when (and (not (eq (cnd-type cnd) 'is-int)) (not (eq (cnd-type cnd) 'is-real)) 
                   (or ground-all-vars (null (cnd-value cnd)))
              )
            (return-from unify-is :no-behavior)
        )
        (when (is-assignable-from model (type-is-is (cnd-type cnd)) (var-symbol-return-type var))
            (when *trace-unify*
                (format t "~%Unify5b: <== ~A" (list binding-list))
            )
            (return-from unify-is (list binding-list))
        )
        (when (is-assignable-from model (var-symbol-return-type var) (type-is-is (cnd-type cnd)))
            (return-from unify-is :no-behavior)
        )
        (when *trace-unify*
            (format t "~%Unify5c: <== ~A" nil)
        )
        (return-from unify-is nil)
    )
    (when (and (eq (cnd-type cnd) 'is-string) (stringp var))
        (when *trace-unify*
            (format t "~%Unify6a: <== ~A" (list binding-list))
        )
        (return-from unify-is (list binding-list))
    )
    (when (and (eq (cnd-type cnd) 'is-real) (numberp var))
        (when *trace-unify*
            (format t "~%Unify6a: <== ~A" (list binding-list))
        )
        (return-from unify-is (list binding-list))
    )
    (when (and (eq (cnd-type cnd) 'is-int) (integerp var))
        (when *trace-unify*
            (format t "~%Unify6b: <== ~A" (list binding-list))
        )
        (return-from unify-is (list binding-list))
    )
    (when (and (eq (cnd-type cnd) 'is-int) (realp var) (= (truncate var) var))
        (setq binding-list
            (cons (cons (car (cnd-args cnd)) (truncate var))
                (remove (car (cnd-args cnd)) binding-list :key #'car)
            )
        )
        (when *trace-unify*
            (format t "~%Unify6b (real): <== ~A" (list binding-list))
        )
        (return-from unify-is (list binding-list))
    )
    (when (and (or (eq (cnd-type cnd) 'is-int) (eq (cnd-type cnd) 'is-real)) (var-symbol-p var))
        (break "Wasn't expecting this.")
        (cond
            ((null (cnd-value cnd))
                (when *trace-unify*
                    (format t "~%Unify6e: <== ~A" nil)
                )
                (return-from unify-is nil)
            )
            (t
                (when *trace-unify*
                    (format t "~%Unify6f: <== ~A" (list binding-list))
                )
                (return-from unify-is (list binding-list))
            )
        )
    )
    :no-behavior
)    

(defun default-unify-cnd (model cnd fact-set binding-list 
                          assume-fn binds-var-symbols ground-all-vars
                          &aux ret-bindings new-bindings)
    (when (and (type-is-is (cnd-type cnd)) (not (eq assume-fn #'assume-nothing)))
        (setq ret-bindings (unify-is model cnd binding-list ground-all-vars))
        (when (not (eq ret-bindings :no-behavior))
            (return-from default-unify-cnd ret-bindings)
        )
    )
        
    (let ((assume-bindings (funcall assume-fn model cnd fact-set binding-list)))
        (case assume-bindings 
            (fail 
                ;;Happens when a hidden fact is contradicted.
                (when *trace-unify*
                    (format t "~%Unify7: <== nil")
                )
                (return-from default-unify-cnd nil)
            )
            (defer
                ;;Happens when a fact is not ground (enough).
                (when (not (cnd-is-is cnd)) 
                    ;; Is conditions can't be deferred; they are the final line.
                    ;; While any is fact can be assumed, if they are deferred 
                    ;; then alternatives will never be suggested.
                    (when *trace-unify*
                        (format t "~%Unify8: <== defer")
                    )
                    (return-from default-unify-cnd (values nil 'defer))
                )
            )
            ((nil)
                ;;No assumption behavior; move on.
            )
            (otherwise
                (when *trace-unify*
                    (format t "~%Unify9: <== ~A" assume-bindings)
                )
                (return-from default-unify-cnd (values assume-bindings 'assume))
            )
        )
    )
            
    (setq ret-bindings nil)
    (setq fact-set
        (if (fact-set-p fact-set)
            ;then
            (get-type-facts (cnd-type cnd) fact-set)
            ;else
            (remove (cnd-type cnd) fact-set :key #'fact-type :test-not #'eq)
        )
    )
    
;    (when (> (length fact-set) 10)
;        (setq *prob* (list cnd binding-list fact-set))
;        (format t "~%Cnd: ~a~%Blist: ~a~%Facts:~%" cnd binding-list)
;        (print-one-per-line fact-set)
;        (break "Lots of facts in unify.")
;    )

    (dolist (fact fact-set)
        (setq new-bindings (bind-value cnd fact binding-list binds-var-symbols))
        (when (eq new-bindings 'defer)
            (when *trace-unify*
                (format t "~%Unify10: <== defer")
            )
            (return-from default-unify-cnd (values nil 'defer))
        )
        (when (not (eq new-bindings 'fail)) 
            (when (equalp (cnd-args cnd) (fact-args fact))
                ;; Any other unification must be overridden by an exact match.
                (when 
                        (intersection (get-var-symbols (cnd-args cnd)) 
                                      (get-var-symbols 
                                          (mapcar #'car 
                                              (remove-if #'var-symbol-p 
                                                  new-bindings :key #'cdr
                                              )
                                          )
                                      )
                        )
                    (when *trace-unify*
                        (format t "~%Unify13a: <== ~A" nil)
                    )
                    (return-from default-unify-cnd nil)
                )
                (when *trace-unify*
                    (format t "~%Unify13b: <== ~A" (list new-bindings))
                )
                (return-from default-unify-cnd (list new-bindings))
            )
            (setq new-bindings (bind-args cnd fact new-bindings binds-var-symbols))
            (when (and (not (eq new-bindings 'fail)) 
                       (every #'is-variable (mapcar #'car new-bindings)))
                (push new-bindings ret-bindings)
            )
        )
        ;; Shortcut says that if the most trivial case works, we should return 
        ;; just that.
        (when (and (eq new-bindings binding-list) (null (get-var-symbols (cnd-args cnd))))
            (when *trace-unify*
                (format t "~%Unify12: <== ~A" (list binding-list))
            )
            (return-from default-unify-cnd (list binding-list))
        )
    )

    (when *trace-unify*
        (format t "~%Unify11: <== ~A" ret-bindings)
    )

    (if (cdr ret-bindings)
        ;then
        (remove-duplicates ret-bindings :test #'equal)
        ;else
        ret-bindings
    )
)


(defun unify-adds-to (cnd binding-list &aux bound-value bound-args)
    (when (and (not (is-variable (cnd-value cnd))) (not (numberp (cnd-value cnd))))
        (error "The value of an adds-to fact must be a variable or numeric constant.")
    )
    (when (not (null (remove-if #'numberp (remove-if #'is-variable (cnd-args cnd)))))
        (error "Only numeric constants and variables are allowed as adds-to arguments.")
    )
    (if (is-variable (cnd-value cnd))
        ;then
        (setq bound-value (cdr (assoc (cnd-value cnd) binding-list)))
        ;else
        (setq bound-value (cnd-value cnd))
    )
    (setq bound-args
        (append
            (mapcar #'cdr (mapcar+ #'assoc (remove-if #'numberp (cnd-args cnd)) binding-list))
            (remove-if-not #'numberp (cnd-args cnd))
        )
    )

    (when (null bound-value)
        (when (find nil bound-args)
            (error "When the value of adds-to is variable, all args must be bound.")
        )
        (when (notevery #'numberp bound-args)
            (when (every #'var-symbol-fn-p (remove-if #'numberp bound-args))
                (let ((new-sym (new-var-sym)))
                    (setf (var-symbol-adds-to new-sym) `(adds-to ,@bound-args ,new-sym))
                    (return-from unify-adds-to 
                        (cons (cons (cnd-value cnd) new-sym) binding-list)
                    )
                )
            )
            (return-from unify-adds-to 'defer)
            ;(error "Each argument in an adds-to fact must be bound to a number.")
        )
        ;; here, all args are bound to numbers, and value is unbound.
        (return-from unify-adds-to 
            (cons (cons (cnd-value cnd) (apply #'+ bound-args)) binding-list)
        )
    )
    (when (< 1 (count nil bound-args))
        (return-from unify-adds-to 'defer)
        ;(error "At most 1 argument to adds-to may be unbound.")
    )
    (when (and (not (numberp bound-value)) (not (var-symbol-p bound-value)))
        (error "The value of adds-to can not be bound to a non-numeric value.")
    )
    (let* ((pos (position nil bound-args))
           var
          )
        (when pos
            (setq var (nth pos (remove-if #'numberp (cnd-args cnd))))
            (when (not (is-variable var))
                (error "This should be prevented by conditions earlier in the method.")
            )
            (when (notevery #'numberp (remove nil bound-args))
                (setq bound-args (remove nil bound-args))
                (when (every #'var-symbol-fn-p (remove-if #'numberp bound-args))
                    (let ((new-sym (new-var-sym)))
                        (setf (var-symbol-adds-to new-sym) `(adds-to ,new-sym ,@bound-args (cnd-value cnd)))
                        (return-from unify-adds-to 
                            (cons (cons var new-sym) binding-list)
                        )
                    )
                )
                (return-from unify-adds-to 'defer)
                ;(error "Each argument in an adds-to fact must be bound to a number.")
            )
            (when (var-symbol-p bound-value)
                (let ((new-sym (new-var-sym)))
                    (setf (var-symbol-adds-to new-sym) `(adds-to ,new-sym ,@bound-args (cnd-value cnd)))
                    (return-from unify-adds-to 
                        (cons (cons var new-sym) binding-list)
                    )
                )
            )
            
            ;; One variable is unbound, value is bound 
            (return-from unify-adds-to 
                (cons 
                    (cons var
                        (- bound-value (apply #'+ (remove nil bound-args)))
                    )
                    binding-list
                )
            )
        )
    )
    
    (when (or (var-symbol-p bound-value) (some #'var-symbol-p bound-args))
        (let (eqn blist vsym) 
            (setq eqn (cons '+ bound-args))
            (setq blist (remove-if-not #'var-symbol-p bound-args))
            (setq blist (mapcar #'cons blist (mapcar #'var-symbol-name blist)))
            (setq eqn (sublis blist eqn))
            (when (var-symbol-p bound-value)
                (multiple-value-setq (bound-value binding-list)
                    (add-var-symbol-eqn bound-value binding-list eqn)
                )
            )
            (dolist (pair blist)
                (multiple-value-setq (vsym binding-list)
                    (add-var-symbol-eqn (car pair) binding-list 
                        (solve-for (cdr pair) 
                            (if (var-symbol-p bound-value)
                                ;then
                                (var-symbol-name bound-value)
                                ;else
                                bound-value
                            )
                            eqn
                        )
                    )
                )
            )
            (return-from unify-adds-to binding-list)
        )
    )
        
    
    ;; All variables and the value are bound.
    (if (eq (apply #'+ bound-args) bound-value)
        ;then
        binding-list
        ;else
        'fail
    )
)

;; This is a kludge to get around an SBCL bug. (It doesn't like lambdas in an every.)
(defun var-symbol-fn-p (obj)
    (var-symbol-p obj)
)

(defun unify-forall (model cnd fact-set binding-list &key (assume-fn #'assume-nothing) (binds-var-symbols t))
    (cond 
        ((eq 'if (first (cnd-value cnd)))
            (dolist (blist (unify-set model (second (cnd-value cnd)) fact-set binding-list :assume-fn assume-fn :binds-var-symbols binds-var-symbols))
                (when (null (unify-set model (third (cnd-value cnd)) fact-set blist :assume-fn assume-fn :binds-var-symbols binds-var-symbols))
                    (return-from unify-forall nil)
                )
            )
            (list binding-list)
        )
        (t
            (dolist (cnd-set (cnd-value cnd))
                (when (unify-set model cnd-set fact-set binding-list :assume-fn assume-fn :binds-var-symbols binds-var-symbols)
                    (return-from unify-forall binding-list)
                )
            )
        )
    )
)

(defun unify-assign (cnd binding-list &aux eqn (result nil) (existing-var nil))
    (when (set-difference (vars-in (second (cnd-value cnd))) (mapcar #'car binding-list))
        (return-from unify-assign 'defer)
    )
    (setq existing-var (cdr (assoc (first (cnd-value cnd)) binding-list)))
    (when existing-var
        (setq binding-list (remove (first (cnd-value cnd)) binding-list :key #'car))
    )
    (setq eqn (sublis binding-list (second (cnd-value cnd))))

    ;;Check that second assign to same variable agrees.
    (when (and (not (eq 'eval (first (cnd-value cnd)))) 
               (not (null existing-var))
               (not (var-symbol-p existing-var))
               (null (var-symbols-in eqn))
          )
        (if (equalp existing-var (eval eqn))
            ;then
            (return-from unify-assign binding-list)
            ;else
            (return-from unify-assign 'fail)
        )
        ;(error "Target of assign already bound.")
    )
    (when (var-symbols-in eqn)
        (when (eq 'eval (first (cnd-value cnd)))
            (return-from unify-assign 'defer)
        )
        (let ((var-name-pairs
                    (mapcar #'cons 
                        (get-var-symbols eqn) 
                        (mapcar #'(lambda (v) (var-symbol-name v)) (get-var-symbols eqn))
                    )
              )
              result-name new-eqn
             )
            (setq eqn (sublis var-name-pairs eqn))
            (cond
                ;; Prior binding was var-symbol.
                ((var-symbol-p existing-var) 
                    (multiple-value-setq (result binding-list) 
                        (add-var-symbol-eqn existing-var binding-list eqn)
                    )
                    (setq result-name (var-symbol-name result))
                )
                ;; Prior binding was constant.
                ((and existing-var (not (plan-var-p existing-var)))
                    (setq result existing-var)
                    (setq result-name result)
                )
                ;; Prior binding was variable or non-existent.
                (t 
                    (setq result (new-var-sym 'real))
                    (add-var-symbol-eqn result nil eqn)
                    (setq result-name (var-symbol-name result))
                )
            )
            (dolist (pair var-name-pairs)
                (setq new-eqn (solve-for (cdr pair) result-name eqn))
                (if (null (num-symbols-in new-eqn))
                    ;then
                    (setq binding-list 
                        (subst (eval new-eqn) (car pair) binding-list)
                    )
                    ;else
                    (setq binding-list 
                        (nth-value 1
                            (add-var-symbol-eqn (car pair) binding-list new-eqn)
                        )
                    )
                )
            )
        )
    )
    (when (null result)
        (setq result (eval eqn))
    )
    (when (eq 'eval (first (cnd-value cnd)))
        (if (null result)
            ;then
            (return-from unify-assign 'fail)
            ;else
            (return-from unify-assign binding-list)
        )
    )
    (cons 
        (cons (first (cnd-value cnd)) 
            result
        )
        binding-list
    )
)


;; This encodes the closed world assumption; if the condition is that a 
;; literal does not exist, then in the absence of positive evidence, we assume
;; a match. This is extended for default values of functions, which should be 
;; assumed in the absence of a given function value.
(defun assume-closed-world (model cnd facts binding-list &key (default-fn #'null) &aux default args fact new-bindings exp-value)
    (setq default (funcall default-fn model cnd))
    (when (eq default 'fail)
        (return-from assume-closed-world nil)
    )
    ;; Matt (1/18/13) I have found it useful to allow inequalities, because 
    ;; the inequality can rule out a value or range of values that do not 
    ;; include the default. This may only be useful now because defaults are 
    ;; available.
;    (when (cnd-inequality cnd)
;        (return-from assume-closed-world nil)
;    )
    (when (and (not (is-variable (cnd-value cnd))) 
               (not (eq (cnd-value cnd) default))
               (null (cnd-inequality cnd))
          )
        (return-from assume-closed-world nil)
    )
    
    (when (subsetp 
              (remove-if-not #'is-variable-term (cnd-args cnd))
              (mapcar #'car binding-list)
          )
        (setq args (sublis binding-list (cnd-args cnd)))
        (when (find-same-map-in-facts (cnd-type cnd) args facts)
            (return-from assume-closed-world nil)
        )
        (setq fact (make-fact :type (cnd-type cnd) :args args :value default))
        (setq exp-value (sublis binding-list (cnd-value cnd)))
        (cond 
            ((not (var-symbols-in args))
                (if (eq (eq exp-value default) (null (cnd-inequality cnd)))
                    ;then
                    (setq new-bindings (unify model cnd (list fact) binding-list :assume-fn #'assume-nothing))
                    ;else
                    (setq new-bindings (unify-with-extra-fact model cnd fact facts binding-list :assume-fn #'assume-nothing))
                )
            )
            ;((and (eq exp-value default) (null (cnd-inequality cnd)))
            ;    (setq new-bindings (list binding-list))
            ;)
            ((and (eq exp-value default) (cnd-inequality cnd))
                (setq new-bindings nil)
            )
            ((is-variable exp-value)
                ; (when (not (var-symbol-p exp-value))
                    ; (let ((new-sym (new-var-sym 'obj)))
                        ; (setq binding-list 
                            ; (cons 
                                ; (cons exp-value new-sym) 
                                ; (subst new-sym exp-value binding-list)
                            ; )
                        ; )
                        ; (setq exp-value new-sym)
                    ; )
                ; )
                ; (setq fact (make-fact :type (cnd-type cnd) :args args :value exp-value))
                ; (when (unify cnd (list fact) binding-list :assume-fn #'assume-nothing)
                    ; (setq new-bindings (list binding-list))
                ; )
                (setq new-bindings (unify-with-extra-fact model cnd fact facts binding-list :assume-fn #'assume-nothing))
            )
            ((cnd-inequality cnd)
                (setq new-bindings (unify-with-extra-fact model cnd fact facts binding-list :assume-fn #'assume-nothing))
            )                
            (t ;; inequality condition with var symbols and a non-default constant value
                (setq new-bindings (unify-with-extra-fact model cnd fact facts binding-list :assume-fn #'assume-nothing))
            )
        )
        (return-from assume-closed-world new-bindings)
    )

    ;; This says that in the absence of full bindings, we will only assume
    ;; over all possible bindings if no answers are present.
    ; 'defer
    (setq new-bindings (default-unify-cnd model cnd facts binding-list #'assume-nothing t nil))
    (if (null new-bindings)
        ;then
        'defer
        ;else
        new-bindings
    )
)

(defun unify-with-extra-fact (model cnd extra-fact facts binding-list 
                               &key (assume-fn #'assume-nothing)
                               &aux ret-bindings)
    (when (fact-set-p facts)
        ; (add-to-fact-set extra-fact facts)
        ; (setq ret-bindings (unify cnd facts binding-list :assume-fn assume-fn))
        ; (remove-from-fact-set extra-fact facts)
        (setq ret-bindings 
            (append
                (unify model cnd (list extra-fact) binding-list :assume-fn assume-fn)
                (unify model cnd facts binding-list :assume-fn assume-fn)
            )
        )
        (return-from unify-with-extra-fact ret-bindings)
    )
    (unify model cnd (cons extra-fact facts) binding-list :assume-fn assume-fn)
)

(defun assume-nothing (model cnd facts binding-list)
    (declare (ignore model cnd facts binding-list))
    nil
)


(defun assume-anything (model cnd facts binding-list)
    (declare (ignore model))
    (if (subsetp 
            (remove-if-not #'is-variable (cons (cnd-value cnd) (cnd-args cnd)))
            (mapcar #'car binding-list)
        )
        ; then
        (if (find-same-map-in-facts (cnd-type cnd) (sublis binding-list (cnd-args cnd)) facts)
            ;then
            nil
            ;else
            (list binding-list)
        )
        ;else
        'defer
    )
)

(defun bind-value (cnd fact binding-list binds-var-symbols &aux val fvalue)
    (setq val (cnd-value cnd))
    (setq fvalue (fact-value fact))

    (when (is-variable val) 
        (let ((new-val (assoc val binding-list)))
            (when (and (null new-val) (not (var-symbol-p val)))
                ;; Here iff condition value is variable term not in binding-list
                (when (cnd-inequality cnd) 
                    (return-from bind-value 'defer)
                )
                (return-from bind-value 
                    (cons (cons val fvalue) binding-list)
                )
            )
            (when new-val
                (setq val (cdr new-val))
            )
        )
    )
    (when (cnd-inequality cnd)
        ;; binding is a var sym, condition is inequality
        (when (var-symbol-p val)
            (multiple-value-bind (new-var blist)
                    (restrict-inequality 
                        (cnd-inequality (switch-inequality cnd))
                        fvalue
                        val
                        binding-list
                    )
                (if (null new-var)
                    ;then
                    (return-from bind-value 'fail)
                    ;else
                    (if (eq new-var val)
                        ;then
                        (return-from bind-value blist)
                        ;else
                        (return-from bind-value (cons (cons val new-var) (subst new-var val blist)))
                    )
                )
            )
        )
        (when (var-symbol-p fvalue) 
            (when (and (not (numberp val)) (not (eq (cnd-inequality cnd) #'neq)))
                (error "Cannot compare symbols with ~A" (cnd-inequality cnd))
            )
            (multiple-value-bind (new-var blist)
                    (restrict-inequality 
                        (cnd-inequality cnd);(reverse-inequality cnd))
                        val
                        fvalue
                        binding-list
                    )
                (cond
                    ((null new-var)
                        (return-from bind-value 'fail))
                    ((eq new-var fvalue)
                        (return-from bind-value blist))
                    ((and (not binds-var-symbols) (var-symbol-p fvalue) (not (var-symbol-temporary fvalue)))
                        (return-from bind-value 'fail))
                    (t
                        (return-from bind-value (cons (cons fvalue new-var) blist)))
                )
            )
        )
        (if (check-inequality (cnd-inequality cnd) fact val)
            ;then
            (return-from bind-value binding-list)
            ;else
            (return-from bind-value 'fail)
        )
    )
    (when (eq val fvalue)
        (return-from bind-value binding-list)
    )
    (when (var-symbol-p fvalue)
        (when (var-symbol-p val)
            (let ((combined-var (var-symbol-intersection fvalue val)))
                (when (and combined-var (not (eq combined-var fvalue)))
                    (when (and (not binds-var-symbols) (not (var-symbol-temporary fvalue)))
                        (return-from bind-value 'fail)
                    )
                    (setq binding-list (cons (cons fvalue combined-var) binding-list))
                )
                        
                (if combined-var
                    ;then
                    (if (eq combined-var val)
                        ;then
                        (return-from bind-value binding-list)
                        ;else
                        (return-from bind-value 
                            (cons (cons val combined-var) 
                                (subst combined-var val binding-list)
                            )
                        )
                    )
                    ;else
                    (return-from bind-value 'fail)
                )
            )
        )
        (if (and (or binds-var-symbols (var-symbol-temporary fvalue)) (allowed-value fvalue val))
            ;then
            (return-from bind-value (cons (cons fvalue val) binding-list))
            ;else
            (return-from bind-value 'fail)
        )
    )
    
    (when (var-symbol-p val)
        (when (and (not binds-var-symbols) (not (var-symbol-temporary val)))
            (return-from bind-value 'fail)
        )
        (if (allowed-value val fvalue)
            ;then
            (return-from bind-value (cons (cons val fvalue) (subst fvalue val binding-list)))
            ;else
            (return-from bind-value 'fail)
        )
    )

    ;; Dealt with all strange conditions.
    (if (val-close-enough fvalue val)
        ;then
        binding-list
        ;else
        'fail
    )
)

(defun fudge-inequality (fn val1 val2 &optional (fudge-depth .0001))
    (cond
        ((funcall fn val1 val2) 
            (or (eq fn #'<=)
                (eq fn #'>=)
                (not (within val1 val2 fudge-depth))
            )
        )
        ((or (eq fn #'<=) (eq fn #'>=))
            (within val1 val2 fudge-depth)
        )
        (t nil)
    )
)

(defun check-inequality (fn fact1 val2 &optional (fudge-depth .0001))
    (cond
        ((and (fact-is-real fact1) (not (numberp (fact-value fact1))))
           ;; Hiding an egregious error here, needs fixing.
           nil
        )
        ((fact-is-real fact1) 
            (fudge-inequality fn (fact-value fact1) val2 fudge-depth)
        )
        (t (funcall fn (fact-value fact1) val2))
    )
)

;; Old function not consistent with "within".
; (defun check-inequality (fn fact1 val2 fudge-depth &aux fudge-add)
    ; (setq fudge-add
        ; (case (nth-value 2 (function-lambda-expression fn))
            ; ((> >=) -1)
            ; ((< <=) 1)
            ; (t 0)
        ; )
    ; )
    ; (if (fact-is-real fact1)
        ; ;then
        ; (funcall fn 
            ; (truncate (* (fact-value fact1) fudge-depth) 1)
            ; (+ (truncate (* val2 fudge-depth) 1) fudge-add)
        ; )
        ; ;else
        ; (funcall fn (fact-value fact1) val2)
    ; )
; )


(defun bind-args (cnd fact binding-list binds-var-symbols &aux val arg-list farg-is-sym carg-is-sym)
    (setq arg-list (fact-args fact))
    (dolist (cnd-arg (cnd-args cnd))
        (setq farg-is-sym (var-symbol-p (car arg-list)))
        (cond 
            ((and (is-variable cnd-arg)) 
                (setq val (assoc cnd-arg binding-list))
                
                (when val
                    (setq carg-is-sym (var-symbol-p (cdr val)))
                    (cond
                        ;;binding is var sym, fact is standard
                        ((and carg-is-sym (not farg-is-sym))
                            (when (not (allowed-value (cdr val) (car arg-list)))
                                (return-from bind-args 'fail)
                            )
                            ; otherwise, keep the var sym, which is correct
                            (setq binding-list
                                (cons (cons cnd-arg (car arg-list))
                                    (cons (cons (cdr val) (car arg-list))
                                        (remove cnd-arg binding-list :key #'car)
                                    )
                                )
                            )
                        )
                        ((eq (cdr val) (car arg-list))
                            ;; Implies both are var-symbols, and no binding is 
                            ;; necessary.
                        )
                        ((and (not binds-var-symbols) (var-symbol-p (car arg-list)) (not (var-symbol-temporary (car arg-list))))
                            (return-from bind-args 'fail)
                        )
                        ;;fact has var-sym, binding does not
                        ((and (not carg-is-sym) farg-is-sym)
                            (when (not (allowed-value (car arg-list) (cdr val)))
                                (return-from bind-args 'fail)
                            )
                            ; only works if the var-sym happens to be that 
                            ; specific value
                            (push
                                (cons (car arg-list) (cdr val))
                                binding-list
                            )
                        )
                        ;;both fact and binding have var syms
                        ((and carg-is-sym farg-is-sym)
                            ; works at the intersection
                            (let ((new-var (var-symbol-intersection (car arg-list) (cdr val)))
                                  new-bindings
                                 )
                                (setq new-bindings
                                    (remove-if #'(lambda (binding) (eq (car binding) (cdr binding)))
                                        (list
                                            (cons (car arg-list) new-var)
                                            (cons (cdr val) new-var)
                                        )
                                    )
                                )
                                (setq binding-list
                                    (append
                                        new-bindings
                                        (sublis new-bindings binding-list)
                                    )
                                )
                            )
                        )
                        ;;neither fact nor binding is var sym, and they don't match 
                        ((not (equalp (cdr val) (car arg-list)))
                            (return-from bind-args 'fail)
                        )
                    )
                )
                
                ;;no existing binding 
                (when (and (null val) (not (eq cnd-arg (car arg-list))))
                    (push (cons cnd-arg (car arg-list)) binding-list)
                )
            )
            ;; constant constraint, constant fact
            ((not farg-is-sym) ;;(not (is-variable cnd-arg))
                (when (not (eq-safe-num (car arg-list) cnd-arg))
                    (return-from bind-args 'fail)
                )
            )
            ;; constant constraint, variable fact
            (t
                (when (eq cnd-arg (cdr (assoc (car arg-list) binding-list)))
                    (return-from bind-args binding-list)
                )
                (when (and (not binds-var-symbols) (var-symbol-p (car arg-list)) (not (var-symbol-temporary (car arg-list))))
                    (return-from bind-args 'fail)
                )
                
                ; works only if var-symbol happens to match the condition
                (if (allowed-value (car arg-list) cnd-arg)
                    ;then
                    (setq binding-list
                        (cons 
                            (cons (car arg-list) cnd-arg) 
                            (subst cnd-arg (car arg-list) binding-list)
                        )
                    )
                    ;else
                    (return-from bind-args 'fail)
                )
            )
        )
        (setq arg-list (cdr arg-list))
    )
    binding-list
)

(defun eq-safe-num (a b)
    (cond
        ((stringp a) (and (stringp b) (string-equal a b)))
        ((not (numberp a)) (eq a b))
        ((not (numberp b)) nil)
        (t (= a b))
    )
)

(defun hash-safe-num (a)
    (cond 
        ((numberp a) (sxhash (coerce a 'double-float)))
        ((var-symbol-p a) (sxhash (var-symbol-id a)))
        (t (sxhash a))
    )
)

#+SBCL (sb-ext:define-hash-table-test eq-safe-num hash-safe-num)

(defun substitute-names (blist)
    (let ((var-name-pairs
                (mapcar #'cons 
                    (get-var-symbols blist) 
                    (mapcar #'(lambda (v) (var-symbol-name v)) (get-var-symbols blist))
                )
         ))
        (pairlis (sublis var-name-pairs (mapcar #'car blist)) (mapcar #'cdr blist))
    )
)

(defun remove-identities (blist)
    (remove-if #'(lambda (pair) (eq (car pair) (cdr pair))) blist)
)

(defun unify-from-fact-set (model cnd fact-set bindings &key (assume-fn #'assume-closed-world) &aux flist)
    (setq flist (get-facts-in-hash (gethash (cnd-type cnd) (fact-set-fact-hash fact-set))))
    (unify model cnd flist bindings :assume-fn assume-fn)
)

