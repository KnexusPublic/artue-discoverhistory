 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: 2013
 ;;;; Project Name: Explanation
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file is responsible for generating sets of ground or partially
 ;;;; ground occurrences that match an occurrence model in a given state.

(in-package :artue)

;; A special debugging variable. List of event names to break on in 
;; enumerate-occs.
(defvar *broken-events* nil)

;; Finds and returns all events that must happen in response to the input state.
(defun enumerate-occs (expgen ev-model facts pt 
                        &key (assume-fn #'assume-closed-world)
                             (bindings nil)
                             (open-precs nil)
                             (bind-var-symbols nil)
                             (ground-all-vars nil)
                             (ground-params nil)
                        &aux new-occs blists)
    (when (member (event-model-type ev-model) *broken-events*)
        (break "Enumerating ~A." (event-model-type ev-model))
    )
    (when ground-params
        (setq blists
            (unify-set expgen
                (remove-if-not 
                    #'(lambda (cnd) 
                        (member (car (cnd-args cnd)) 
                                (mapcar #'car (model-arg-types ev-model))
                        )
                      ) 
                    (remove-if-not #'cnd-is-is (model-conditions ev-model))
                )
                facts bindings
                :assume-fn #'assume-nothing :binds-var-symbols nil
                :ground-all-vars t
            )
        )
        (setq new-occs nil)
        (dolist (blist blists)
            (push-all 
                (enumerate-occs expgen ev-model facts pt 
                    :bindings blist :assume-fn assume-fn :open-precs open-precs
                    :bind-var-symbols bind-var-symbols :ground-all-vars ground-all-vars
                    :ground-params nil
                )
                new-occs
            )
        )
        (return-from enumerate-occs new-occs)
    )
    (setq blists (unify-set expgen (event-model-conditions ev-model) facts bindings 
                            :assume-fn assume-fn :binds-var-symbols bind-var-symbols
                            :ground-all-vars ground-all-vars
                 )
          new-occs nil
    )
    (dolist (blist blists)
        (when (or bind-var-symbols (binds-no-var-symbols blist))
            (push-all 
                (create-occurrences expgen ev-model blist facts pt :assume-fn assume-fn :open-precs open-precs) 
                new-occs
            )
        )
    )
    new-occs
)

(defun create-occurrences (expgen ev-model blist facts pt &key (open-precs nil) (assume-fn #'assume-closed-world) &aux ret-occs constraints)
    (setq ret-occs nil)
    (setq blist (mapcar #'simplify-var-syms blist))
    (dolist (pre-facts (find-satisfying-fact-sets 
                            expgen
                            (event-model-conditions ev-model) blist facts
                            :open-precs open-precs
                            :assume-fn assume-fn
                       )
            )
        (setq constraints (second pre-facts)
              pre-facts (first pre-facts)
        )
        ;; Duplicates and adds-to conditions are added to event models when 
        ;; "decrease" or "increase" is present in the effects. These conditions
        ;; are necessary to support unification, but they do not correspond to
        ;; facts in the world, so they must be removed from occurrences. 
        (setq pre-facts
            (remove-if #'non-physical-fact
                pre-facts
;                (remove-duplicates pre-facts :test #'equal-fact)
            )
        )
        ; MCM (6/20) Commented this out so that var-symbols can be allowed to 
        ; remain unvalued; I suspect some prior functionality may rely on it, 
        ; though.
        ; (dolist (var-sym (set-difference (remove-if-not #'var-symbol-p (mapcar #'cdr blist)) (get-var-symbols pre-facts)))
            ; (push (make-fact :type 'unbound-var :args (list var-sym) :value t)
                ; pre-facts
            ; )
        ; )
        (push
            (make-occurrence
                :signature (cons (event-model-type ev-model) 
                                 (mapcar #'cdr 
                                     (mapcar+ #'assoc 
                                         (event-model-variables ev-model) 
                                         blist
                                     )
                                 )
                           )
                :pre pre-facts
                :post 
                    (mapcar #'only
                        (mapcar 
                            #'(lambda (cnd) 
                                (bind-condition-to-fact expgen cnd blist nil) 
                              )
                            (event-model-postconditions ev-model)
                        )
                    )
                :constraints constraints
                :point pt
                :is-event t
                :performer (sublis blist (event-model-performer ev-model))
            )
            ret-occs
        )
    )
    ret-occs
)

;;MCM (05/06) : Changed unify logic on first line because the assume-fn wasn't 
;; passed through, which had to be wrong, right?
(defun find-satisfying-forall-fact-sets (expgen cnd blist facts 
                                         &key (assume-fn #'assume-closed-world) 
                                              (open-precs nil) 
                                         &aux (ret-set '(())) (old-ret-set '(())))
    (dolist (blist2 (unify-set expgen (second (cnd-value cnd)) facts blist :assume-fn assume-fn))
        (setq old-ret-set ret-set)
        (setq ret-set nil)
        (dolist (fset (find-satisfying-fact-sets expgen (third (cnd-value cnd)) blist2 facts :assume-fn assume-fn :open-precs open-precs))
            (dolist (fset2 old-ret-set)
                (push 
                    (list
                        (append (first fset) (first fset2))
                        (append (second fset) (second fset2))
                    )
                    ret-set
                )
            )
        )
    )
    ret-set
)

(defun bind-condition (cnd blist)
    (make-cnd 
        :inequality (cnd-inequality cnd)
        :type (cnd-type cnd)
        :args (sublis blist (cnd-args cnd))
        :value (sublis blist (cnd-value cnd))
    )
)

    
;; Smaller method that packages up results of bind-condition-to-fact into all
;; occurrence permutations.
(defun find-satisfying-fact-sets (expgen cnds blist facts &key (assume-fn #'assume-closed-world) (open-precs nil) &aux fact-sets ret-sets)
    (when (null cnds)
        (return-from find-satisfying-fact-sets (list nil))
    )
    (setq fact-sets (find-satisfying-fact-sets expgen (cdr cnds) blist facts :assume-fn assume-fn :open-precs open-precs))
    (setq ret-sets nil)
    (when (eq (cnd-type (car cnds)) 'forall)
        (dolist (forall-set (find-satisfying-forall-fact-sets expgen (car cnds) blist facts :assume-fn assume-fn :open-precs open-precs))
            (dolist (fact-set fact-sets)
                (push 
                    (list 
                        (append (first fact-set) (first forall-set))
                        (append (second fact-set) (second forall-set))
                    )
                    ret-sets
                )
            )
        )
        (return-from find-satisfying-fact-sets ret-sets)
    )
    (when (and open-precs (cnd-inequality (car cnds)) (funcall assume-fn expgen (car cnds) facts blist))
        (let ((new-cnd (bind-condition (car cnds) blist)))
            (dolist (fact-set fact-sets)
                (push 
                    (list 
                        (first fact-set)
                        (cons (negate-condition new-cnd) (second fact-set))
                    )
                    ret-sets
                )
            )
        )
        (return-from find-satisfying-fact-sets ret-sets)
    )
    (dolist (fact (bind-condition-to-fact expgen (car cnds) blist facts))
        (dolist (fact-set fact-sets)
            (push (list (cons fact (first fact-set)) (second fact-set)) ret-sets)
        )
    )
    ret-sets
)


(defun simplify-var-syms (pair)
    (when (var-symbol-p (cdr pair))
        (dolist (eqn (var-symbol-eqn (cdr pair)))
            (when (not (var-or-num-symbols-in eqn))
                (return-from simplify-var-syms (cons (car pair) (eval eqn)))
            )
        )
    )
    pair
)
