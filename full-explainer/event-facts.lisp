 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: July 1, 2013
 ;;;; Project Name: Explanation
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file provides the fact and cnd classes and associated functions.

(in-package :artue)

#+SBCL
(defstruct (fact (:constructor create-fact) (:print-function print-fact)) ;(:type vector))
    (type nil :read-only t)              ; The type of the predicate to which this fact refers
    (args nil :read-only t)              ; The objects to which this fact refers
    (value nil :read-only t)             ; The value of this fact
    (hash-val nil :read-only t)
    (map-hash nil :read-only t)
    (is-real nil :read-only t)
)

#+ABCL
(defstruct (fact (:print-function print-fact))
    (type nil :read-only t)              ; The type of the predicate to which this fact refers
    (args nil :read-only t)              ; The objects to which this fact refers
    (value nil :read-only t)             ; The value of this fact
)

#+CLISP
(defstruct (fact (:constructor create-fact) (:print-function print-fact) (:type vector))
    (type nil :read-only t)              ; The type of the predicate to which this fact refers
    (args nil :read-only t)              ; The objects to which this fact refers
    (value nil :read-only t)             ; The value of this fact
    (hash-val nil) ;;should only be written in finish-fact-hashing
    (map-hash nil) ;;should only be written in finish-fact-hashing
    (is-real nil)  ;;should only be written in finish-fact-hashing
)

#+ALLEGRO
(defstruct (fact (:constructor create-fact) (:print-function print-fact))
    (type nil :read-only t)              ; The type of the predicate to which this fact refers
    (args nil :read-only t)              ; The objects to which this fact refers
    (value nil :read-only t)             ; The value of this fact
    (hash-val nil)  ;;should only be written in finish-fact-hashing
    (map-hash nil)  ;;should only be written in finish-fact-hashing
    (is-real nil)   ;;should only be written in finish-fact-hashing
)
;(utils::defun-print-readably fact ("TYPE" "ARGS" "VALUE"))

(defstruct (cnd (:print-function print-cnd)) ;; A condition
    (inequality nil)        ; Gives an inequality function over real numbers, or 
                            ; nil for single value match/prohibition
    (type nil)              ; Type of fact to match
    (args nil)              ; Args to match (variables)
    (value nil)             ; Value to match/prohibit
)

(defstruct (var-symbol (:print-function print-var-sym)) ;(:type vector))
    (id nil)                ; The type of the predicate to which this fact refers
    (name nil)              ; The objects to which this fact refers
    (return-type nil)       ; The value of this fact
    (= nil)
    (< nil)
    (> nil)
    (<= nil)
    (>= nil)
    (neq nil)
    (eqn nil)
    (adds-to nil)
    (update-function nil)
    (rooted nil)            ; t if this var symbol comes from a create, nil 
                            ; otherwise. Two rooted var-syms cannot be unified
    (temporary t)
)

#+CLISP
(defun replace-read-labels (item)
    (cond
        ((eq 'sys::read-label (type-of item)) (replace-read-labels (cdr (assoc item sys::*read-reference-table*))))
        ((consp item) 
            (cons (replace-read-labels (car item)) (replace-read-labels (cdr item)))
        )
        (t item)
    )
)
        
#+CLISP
(defun cnd-reader (stream subchar arg) 
  (declare (ignore subchar arg)) 
  (let ((cargs (read stream t nil t)))
      (setq cargs (replace-read-labels cargs))
      (make-cnd 
          :inequality (if (eq (first cargs) 'eq) nil (symbol-function (first cargs)))
          :type (second cargs) :args (third cargs) :value (fourth cargs)
      )
  )
)

#-CLISP
(defun cnd-reader (stream subchar arg) 
  (declare (ignore subchar arg)) 
  (let ((cargs (read stream t nil t)))
      (make-cnd 
          :inequality (if (eq (first cargs) 'eq) nil (symbol-function (first cargs)))
          :type (second cargs) :args (third cargs) :value (fourth cargs)
      )
  )
)

(set-dispatch-macro-character #\# #\D #'cnd-reader)

#+CLISP
(defun fact-reader (stream subchar arg) 
  (declare (ignore subchar arg)) 
  (let ((fargs (read stream t nil t)))
      (setq fargs (replace-read-labels fargs))
      (make-fact :type (first fargs) :args (second fargs) :value (third fargs))
  )
)

#-(or CLISP ALLEGRO)
(defun fact-reader (stream subchar arg) 
  (declare (ignore subchar arg)) 
  (let ((fargs (read stream t nil t)))
      (make-fact :type (first fargs) :args (second fargs) :value (third fargs))
  )
)

(defvar *unhashed-facts* nil)

#+ALLEGRO
(defun fact-reader (stream subchar arg) 
  (declare (ignore subchar arg)) 
  (let ((fargs (read stream t nil t)) fact)
      (setq fact
          (create-fact :type (first fargs) :args (second fargs) :value (third fargs))
      )
      (push fact *unhashed-facts*)
      fact
  )
)

(defun finish-fact-hashing ()
    (dolist (fact *unhashed-facts*)
        (let ((type (fact-type fact)) (args (fact-args fact)) (value (fact-value fact)))
            (multiple-value-bind (hash-val map-hash) (find-fact-hash type args value)
                (setf (fact-hash-val fact) hash-val)
                (setf (fact-map-hash fact) map-hash)
            )
            (setf (fact-is-real fact)
                (or (and (listp value) (not (null value))) 
                    (and (numberp value) (not (integerp value)))
                    (and (var-symbol-p value) 
                         (eq (get-return-type value) 'real)
                    )
                )
            )
        )
    )
    (setq *unhashed-facts* nil)
)
        

(set-dispatch-macro-character #\# #\F #'fact-reader)

(defun print-fact (f s k)
    (declare (ignore k))
    (if *print-readably*
        ;then
        (format s "#F(~S ~S ~S)" (fact-type f) (fact-args f) (fact-value f))
        ;else
        (format s "#F(~A ~A ~A)" (fact-type f) (fact-args f) (fact-value f))
    )
)

(defun print-cnd (c s k)
    (declare (ignore k))
    (format s (if *print-readably* "#D(~S ~S ~S ~S)" "#D(~A ~A ~A ~A)") 
        (if (null (cnd-inequality c))
            ;then
            'eq 
            ;else
            (nth-value 2 (function-lambda-expression (cnd-inequality c)))
        )
        (cnd-type c) (cnd-args c) (cnd-value c)
    )
)    

#+ABCL
(defun equal-fact (fact1 fact2)
    (confirm-fact-equality fact1 fact2)
) 

#+CLISP
(defun equal-fact (fact1 fact2)
    (or (and (not (svref fact1 5)) 
             (eql (svref fact1 3) (svref fact2 3))
             (confirm-fact-equality fact1 fact2))
        (and (fact-is-real fact1)
             (same-map fact1 fact2)
             (val-close-enough (fact-value fact1) (fact-value fact2))
        )
    )
)

#+(or SBCL ALLEGRO)
(defun equal-fact (fact1 fact2)
    (or (and (not (fact-is-real fact1))
             (eql (fact-hash-val fact1) (fact-hash-val fact2))
             (confirm-fact-equality fact1 fact2))
        (and (fact-is-real fact1)
             (same-map fact1 fact2)
             (val-close-enough (fact-value fact1) (fact-value fact2))
        )
    )
)

(defun repeat-fact (fact1 fact2)
    (and (equalp (fact-type fact1) (fact-type fact2))
         (equalp (fact-value fact1) (fact-value fact2))
         (every #'equal-arg (fact-args fact1) (fact-args fact2))
    )
)

(defun equal-arg (arg1 arg2 &key (blist nil))
    (cond
        ((var-symbol-p arg1) 
            (and (var-symbol-p arg2) (var-symbol-equivalent arg1 arg2 :blist blist))
        )
        (t (equalp arg1 arg2))
    )
)

(defun confirm-fact-equality (fact1 fact2)
    (and (eq (fact-type fact1) (fact-type fact2)) 
         (val-close-enough (fact-value fact1) (fact-value fact2))
         (equalp (fact-args fact1) (fact-args fact2))
    )
) 

;;ABCL doesn't need fact-p definition because it's available by default.
#+CLISP
(defun fact-p (fact)
    (and 
        (vectorp fact) 
        (eq (length fact) 6) 
        (listp (svref fact 1)) 
        (symbolp (svref fact 0))
        (numberp (svref fact 3))
        (numberp (svref fact 4))
    )
)

(defvar *initial-hash-value* 17)
(defvar *hash-constant* 79)

#| ;;Good, but slow to compute.
(defun hash-fact (fact &aux hash-value)
    (setq hash-value *initial-hash-value*)
    (setq hash-value (+ (* *hash-constant* hash-value) (sxhash (fact-type fact))))
    (setq hash-value (+ (* *hash-constant* hash-value) (sxhash (fact-value fact))))
    (dolist (arg (fact-args fact))
        (setq hash-value (+ (* *hash-constant* hash-value) (sxhash arg)))
    )
    hash-value
)
|#

; works, but allocates memory from going to bignums  
(defun find-fact-hash-old (type args value &aux hash-value)
    (declare (fixnum hash-value) (fixnum *hash-constant*) (fixnum *initial-hash-value*))
    (setf hash-value *initial-hash-value*)
    (setf hash-value (the fixnum (* hash-value *hash-constant*)))
    (setf hash-value (the fixnum (+ hash-value (the fixnum (sxhash type)))))
    (dolist (arg args)
        (setf hash-value (the fixnum (* hash-value *hash-constant*)))
        (setf hash-value (the fixnum (+ hash-value (hash-arg arg))))
    )
    (setf hash-value (the fixnum (* hash-value *hash-constant*)))
    (setf hash-value (the fixnum (mod hash-value 1000000)))
    (values (the fixnum (+ hash-value (mod (hash-arg value) 1000000))) hash-value) 
)


#+SBCL(defvar *bit-mask* 4323455642275676160)
#-SBCL(defvar *bit-mask* -16777216)

(defun find-fact-hash (type args value &aux (hash-value 0) (second-hash-value 0))
    #+ALLEGRO(declare (fixnum hash-value) (fixnum second-hash-value))
;    #+SBCL(declare (type fixnum hash-value second-hash-value))
    (setq hash-value (sxhash type))
    (setq hash-value (hash-manip hash-value))
    (dolist (arg args)
        (setq hash-value (fixnum-left-shift-4 hash-value))
        (setq hash-value (logand most-positive-fixnum (+ (hash-arg arg) hash-value)))
        (setq hash-value (hash-manip hash-value))
    )
    (setq second-hash-value (fixnum-left-shift-4 hash-value))
    (setq second-hash-value (logand most-positive-fixnum (+ (hash-arg value) second-hash-value)))
    (setq second-hash-value (hash-manip second-hash-value))
    (values second-hash-value hash-value)
)

#-SBCL(defvar *shift-length* 28)
#+SBCL(defvar *shift-length* 58)

(defun fixnum-left-shift-4 (fixnum1) 
    #+ALLEGRO(declare (fixnum fixnum1))
    (setq fixnum1 
        (dpb (ldb (byte *shift-length* 0) fixnum1) 
             (byte *shift-length* 4) fixnum1
        )
    )
    (dpb 0 (byte 4 0) fixnum1)
)

(defun hash-manip (hash-value &aux g)
    #+ALLEGRO(declare (fixnum hash-value) (fixnum g))
    (setq g (logand *bit-mask* hash-value))
    (when (not (zerop g))
        (setq hash-value (logxor hash-value (fixnum-right-shift-rest g)))
        (setq hash-value (logxor hash-value g))
    )
    hash-value
)

(defun fixnum-right-shift-rest (fixnum1) 
    #+ALLEGRO(declare (fixnum fixnum1))
    (setq fixnum1 
        (dpb (ldb (byte 4 *shift-length*) fixnum1) 
             (byte 4 0) fixnum1
        )
    )
    (dpb 0 (byte *shift-length* 4) fixnum1)
)


(defun hash-arg (arg)
    (cond
        ((numberp arg) (sxhash (floor arg)))
        ((var-symbol-p arg) (sxhash (var-symbol-id arg)))
        (t (sxhash arg))
    )
)


#-ABCL
(defun make-fact (&key (type nil) (args nil) (value nil))
    (multiple-value-bind (hash-val map-hash) (find-fact-hash type args value)
        (create-fact :type type :args args :value value 
            :hash-val hash-val
            :map-hash map-hash
            :is-real (or (and (listp value) (not (null value))) 
                         (and (numberp value) (not (integerp value)))
                         (and (var-symbol-p value) 
                              (eq (get-return-type value) 'real)
                         )
                     )
        )
    )
)

(defvar *true-fact* (make-fact :type 'true :args '(t) :value t))

(defun hash-fact (fact)
    (fact-hash-val fact)
)



(defun contradictory-fact (fact &optional (new-val :empty))
    (when (and (eq new-val :empty) (not (is-binary fact)))
        (error "Must supply new value for a non-binary fact.")
    )
    (when (eq new-val :empty) 
        (setq new-val (not (fact-value fact)))
    )
    
    (make-fact
        :type (fact-type fact)
        :args (fact-args fact)
        :value new-val
    )
)

(defun union-facts (fact-list1 fact-list2)
    (union fact-list1 fact-list2 :test #'equal-fact)
)

(defun union-fact-sets (fact-lists)
    (reduce #'union-facts fact-lists :initial-value nil)
)

#+ABCL
(defun same-map (fact1 fact2)
    (and (eq (fact-type fact1) (fact-type fact2)) 
         (equalp (fact-args fact1) (fact-args fact2))
    )
)

#-ABCL
(defun same-map (fact1 fact2)
    (or (eq fact1 fact2)
        (and (eq (fact-map-hash fact1) (fact-map-hash fact2))
             (eq (fact-type fact1) (fact-type fact2)) 
             (equalp (fact-args fact1) (fact-args fact2))
        )
    )
)

#+ABCL
(defun contradicts (fact1 fact2)
    (and (eq (fact-type fact1) (fact-type fact2)) 
         (not (val-close-enough (fact-value fact1) (fact-value fact2)))
         (test-var-sym-contradiction (fact-value fact1) (fact-value fact2))
         (equal (fact-args fact1) (fact-args fact2))
    )
)

;; Returns t iff one fact asserts something which is incompatible with the 
;; other.
#-ABCL
(defun contradicts (fact1 fact2)
    (and (eql (fact-map-hash fact1) (fact-map-hash fact2))
         (eq (fact-type fact1) (fact-type fact2)) 
         (not (val-close-enough (fact-value fact1) (fact-value fact2)))
         (test-var-sym-contradiction (fact-value fact1) (fact-value fact2))
         (equal (fact-args fact1) (fact-args fact2))
    )
)

(defun test-var-sym-contradiction (arg1 arg2)
    (cond 
        ((and (var-symbol-p arg1) (var-symbol-p arg2))
            (null (var-symbol-intersection arg1 arg2))
        )
        ((var-symbol-p arg1)
            (not (allowed-value arg1 arg2))
        )
        ((var-symbol-p arg2)
            (not (allowed-value arg2 arg1))
        )
        (t
            t ;; If no var symbols, no information gained, could contradict
        )
    )
)
            


#+ABCL
(defun could-contradict (fact1 fact2)
    (and (eq (fact-type fact1) (fact-type fact2)) 
         (not (val-close-enough (fact-value fact1) (fact-value fact2)))
         (equal (fact-args fact1) (fact-args fact2))
    )
)

;; Returns t iff one fact asserts something which is incompatible with the 
;; other.
#-ABCL
(defun could-contradict (fact1 fact2)
    (and (eql (fact-map-hash fact1) (fact-map-hash fact2))
         (eq (fact-type fact1) (fact-type fact2)) 
         (not (val-close-enough (fact-value fact1) (fact-value fact2)))
         (equal (fact-args fact1) (fact-args fact2))
    )
)


(defvar *next-symbol-id* 400000)

(defun print-var-sym (v s k)
    (declare (ignore k))
    (if *print-readably*
        ;then
        (format s "#V(~A ~A ~A ~A ~A ~A ~A ~A ~A ~A)" 
            (var-symbol-id v) (var-symbol-name v) (var-symbol-return-type v)
            (var-symbol-= v) (var-symbol-> v) (var-symbol-< v)
            (var-symbol-<= v) (var-symbol->= v) (var-symbol-neq v)
            (var-symbol-eqn v) (var-symbol-adds-to v)
        )
        ;else
        (format s (if (var-symbol-temporary v) "#V~ATemp" "#V~A") (var-symbol-id v))
    )
)

(defun add-var-symbol-eqn (vsym blist eqn)
    (when (var-symbols-in eqn)
        (error "No variable symbols should be found here.")
    )
    (when (member eqn (var-symbol-eqn vsym) :test #'equalp)
        (return-from add-var-symbol-eqn (values vsym blist))
    )
    (multiple-value-setq (vsym blist)
        (copy-var-sym vsym blist)
    )
    (push eqn (var-symbol-eqn vsym))
    (values vsym blist)
)
        

(defun change-var-symbol-eqn (vsym eqn)
    (when (var-symbols-in eqn)
        (error "No variable symbols should be found here.")
    )
    (if (null (var-symbol-eqn vsym))
        ;then
        (setf (var-symbol-eqn vsym) eqn)
        ;else
        (if (not (equalp eqn (var-symbol-eqn vsym)))
            ;then
            (error "Var sym wants two equations.")
        )
    )
)


#-CLISP
(defun var-sym-reader (stream subchar arg) 
  (declare (ignore subchar arg)) 
  (let ((vargs (read stream t nil t)))
      (make-var-symbol
          :id (first vargs) :name (second vargs) :return-type (third vargs)
          := (fourth vargs) :> (fifth vargs) :< (sixth vargs)
          :<= (seventh vargs) :>= (eighth vargs) :neq (ninth vargs)
          :eqn (tenth vargs)
      )
  )
)
#+CLISP
(defun var-sym-reader (stream subchar arg) 
  (declare (ignore subchar arg)) 
  (let ((vargs (read stream t nil t)))
      (setq vargs (replace-read-labels vargs))
      (make-var-symbol 
          :id (first vargs) :name (second vargs) :return-type (third vargs)
          := (fourth vargs) :> (fifth vargs) :< (sixth vargs)
          :<= (seventh vargs) :>= (eighth vargs) :neq (ninth vargs)
          :eqn (tenth vargs)
      )
  )
)

(defun var-sym-as-general-as (vs1 vs2)
    (and
        (eq (var-symbol-name vs1) (var-symbol-name vs2))
        (or (not (var-symbol-rooted vs1)) (var-symbol-rooted vs2))
        (dim-more-general vs1 vs2 #'var-symbol-> #'safe->=)
        (dim-more-general vs1 vs2 #'var-symbol->= #'safe->=)
        (dim-more-general vs1 vs2 #'var-symbol-< #'safe-<=)
        (dim-more-general vs1 vs2 #'var-symbol-<= #'safe-<=)
        (dim-more-general vs1 vs2 #'var-symbol-= nil)
        (dim-more-general vs1 vs2 #'var-symbol-neq #'subsetp)
        (dim-more-general vs1 vs2 #'var-symbol-eqn nil)
        (dim-more-general vs1 vs2 #'var-symbol-adds-to nil)
        (dim-more-general vs1 vs2 #'var-symbol-update-function nil)
    )
)

(defun safe->= (val1 val2)
    (and (numberp val1) (numberp val2) (>= val1 val2))
)

(defun safe-<= (val1 val2)
    (and (numberp val1) (numberp val2) (<= val1 val2))
)

(defun var-symbol-equivalent (vs1 vs2 &key (blist nil))
    (and (equalp (var-symbol-> vs1) (var-symbol-> vs2))
         (equalp (var-symbol-< vs1) (var-symbol-< vs2))
         (equalp (var-symbol->= vs1) (var-symbol->= vs2))
         (equalp (var-symbol-<= vs1) (var-symbol-<= vs2))
         (equalp (var-symbol-= vs1) (var-symbol-= vs2))
         (equalp (var-symbol-neq vs1) (var-symbol-neq vs2))
         (or (equalp (var-symbol-eqn vs1) (var-symbol-eqn vs2))
             (and blist 
                 (equalp 
                     (sublis (var-symbol-eqn vs1) blist)
                     (sublis (var-symbol-eqn vs2) blist)
                 )
             )
         )
         (equalp (var-symbol-adds-to vs1) (var-symbol-adds-to vs2))
         (equalp (var-symbol-update-function vs1) (var-symbol-update-function vs2))
         (eq (var-symbol-rooted vs1) (var-symbol-rooted vs2))
    )
) 

(defun dim-more-general (vs1 vs2 prop-fn comp-fn)
    (let ((val1 (funcall prop-fn vs1))
          (val2 (funcall prop-fn vs2)))
        (or (null val1)
            (equalp val1 val2)
            (and comp-fn
                 (funcall comp-fn val1 val2)
            )
        )
    )
)


(set-dispatch-macro-character #\# #\V #'var-sym-reader)

(defstruct (num-sym (:print-function print-num-sym))
    (num 0 :read-only t)
)
(defun num-sym-reader (stream subchar arg) 
  (declare (ignore subchar arg)) 
  (let ((vargs (read stream t nil t)))
      (make-num-sym 
          :num vargs
      )
  )
)

(defun print-num-sym (n s k)
    (declare (ignore k))
    (format s "#N~A" (num-sym-num n))
)

(set-dispatch-macro-character #\# #\N #'num-sym-reader)

(defun new-var-sym (&optional (var-type 'INT))
    (make-var-symbol 
        :id (incf *next-symbol-id*)
        :name (make-num-sym :num *next-symbol-id*)
        :return-type var-type
    )
)

(defun copy-var-sym (var binding-list &aux new-var)
    (setq new-var (new-var-sym (get-return-type var)))
    (setf (var-symbol-name new-var) (var-symbol-name var))
    (setf (var-symbol-neq new-var) (copy-list (var-symbol-neq var)))
    (setf (var-symbol-> new-var) (copy-list (var-symbol-> var)))
    (setf (var-symbol->= new-var) (copy-list (var-symbol->= var)))
    (setf (var-symbol-< new-var) (copy-list (var-symbol-< var)))
    (setf (var-symbol-<= new-var) (copy-list (var-symbol-<= var)))
    (setf (var-symbol-= new-var) (var-symbol-= var))
    (setf (var-symbol-eqn new-var) (copy-list (var-symbol-eqn var)))
    (setf (var-symbol-rooted new-var) (var-symbol-rooted var))
    (values new-var (subst new-var var binding-list))
)

(defun dupe-var-sym (var &aux new-var)
    (setq new-var (new-var-sym (get-return-type var)))
    (setf (var-symbol-neq new-var) (copy-list (var-symbol-neq var)))
    (setf (var-symbol-> new-var) (copy-list (var-symbol-> var)))
    (setf (var-symbol->= new-var) (copy-list (var-symbol->= var)))
    (setf (var-symbol-< new-var) (copy-list (var-symbol-< var)))
    (setf (var-symbol-<= new-var) (copy-list (var-symbol-<= var)))
    (setf (var-symbol-= new-var) (var-symbol-= var))
    (if (eq (var-symbol-rooted var) (var-symbol-name var))
        ;then
        (setf (var-symbol-rooted new-var) (var-symbol-name new-var))
        ;else
        (setf (var-symbol-rooted new-var) (var-symbol-rooted var))
    )
    (setf (var-symbol-eqn new-var) (copy-list (var-symbol-eqn var)))
    new-var
)

(defun var-symbol-constraint (ineq vsym)
    (case ineq
        (>   (var-symbol-> vsym))
        (>=  (var-symbol->= vsym))
        (<   (var-symbol-< vsym))
        (<=  (var-symbol-<= vsym))
        (=   (var-symbol-= vsym))
        (neq (var-symbol-neq vsym))
        (t (error "Expected an inequality symbol."))
    )
)

(defun change-var-symbol-constraint (ineq vsym constraint-value)
    (case ineq
        (>   (setf (var-symbol-> vsym)   constraint-value))
        (>=  (setf (var-symbol->= vsym)  constraint-value))
        (<   (setf (var-symbol-< vsym)   constraint-value))
        (<=  (setf (var-symbol-<= vsym)  constraint-value))
        (=   (setf (var-symbol-= vsym)   constraint-value))
        (neq (setf (var-symbol-neq vsym) constraint-value))
        (t (error "Expected an inequality symbol."))
    )
)

(defun restrict-inequality (ineq val fact-value binding-list)
    (when (not (var-symbol-p fact-value))
        (error "Expected a var-symbol for fact-value!")
    )
    (when (null val)
        (return-from restrict-inequality fact-value)
    )
    (let*
      ((inequality-type 
           (nth-value 2 
               (function-lambda-expression ineq)
           )
       )
       (existing-bound (var-symbol-constraint inequality-type fact-value))
       lower-bound upper-bound
       (orig-fvalue fact-value) (orig-blist binding-list)
      )
        (when (var-symbol-p val) 
            (when (member (var-symbol-name val) existing-bound)
                ;; bound already created
                (return-from restrict-inequality (values fact-value binding-list))
            )
            (when (eq (var-symbol-name val) (var-symbol-name fact-value))
                (if (member inequality-type '(< > neq))
                    ;then
                    (return-from restrict-inequality (values nil nil))
                    ;else
                    (return-from restrict-inequality (values fact-value binding-list))
                )
            )
        )
        (when (num-sym-p val)
            (when (member val existing-bound)
                ;; bound already created
                (return-from restrict-inequality (values fact-value binding-list))
            )
            (when (eq val (var-symbol-name fact-value))
                (if (member inequality-type '(< > neq))
                    ;then
                    (return-from restrict-inequality (values nil nil))
                    ;else
                    (return-from restrict-inequality (values fact-value binding-list))
                )
            )
        )

        (when (and (eq ineq #'neq) (member val existing-bound))
            ;; bound already created
            (return-from restrict-inequality (values fact-value binding-list))
        )

        (when (and (numberp (car existing-bound)) (numberp val) (not (eq ineq #'neq))
                   (funcall ineq (car existing-bound) val))
            ;; New inequality was already subsumed.
            (return-from restrict-inequality (values fact-value binding-list))
        )
        (multiple-value-setq (fact-value binding-list) 
            (copy-var-sym fact-value binding-list)
        )
        
        (when (var-symbol-p val)
            (multiple-value-bind (new-val new-blist) 
                    (restrict-inequality (switch-inequality-fn inequality-type) 
                        (var-symbol-name fact-value) val binding-list
                    )
                (setq binding-list (cons (cons val new-val) new-blist))
            )
            (setq val (var-symbol-name val))
        )

        (when (num-sym-p val) 
            (let (new-bound)
                (if (numberp (car existing-bound))
                    ;then 
                    (setq new-bound (cons (car existing-bound) (cons val (copy-list (cdr existing-bound)))))
                    ;else
                    (setq new-bound (cons val (copy-list existing-bound)))
                )
                (change-var-symbol-constraint inequality-type fact-value new-bound)
                (return-from restrict-inequality (values fact-value binding-list))
            )
        )

        (when (numberp (car existing-bound))
            (setq existing-bound (cdr existing-bound))
        )
        
        (change-var-symbol-constraint inequality-type fact-value (cons val existing-bound))
        
        ;; remove similars
        (when (and (if-number (var-symbol-> fact-value)) (if-number (var-symbol->= fact-value)))
            (if (> (car (var-symbol->= fact-value)) (car (var-symbol-> fact-value)))
                ;then
                (pop (var-symbol-> fact-value))
                ;else
                (pop (var-symbol->= fact-value))
            )
        )
        (when (and (if-number (var-symbol-< fact-value)) (if-number (var-symbol-<= fact-value)))
            (if (< (car (var-symbol-<= fact-value)) (car (var-symbol-< fact-value)))
                ;then
                (pop (var-symbol-< fact-value))
                ;else
                (pop (var-symbol-<= fact-value))
            )
        )
        
        ;;check for contradiction
        (setq lower-bound (or (if-number (var-symbol-> fact-value)) (if-number (var-symbol->= fact-value))))
        (setq upper-bound (or (if-number (var-symbol-< fact-value)) (if-number (var-symbol-<= fact-value))))
        (when (and lower-bound upper-bound (> lower-bound upper-bound))
           (return-from restrict-inequality (values nil nil))
        )
        (when (and lower-bound upper-bound (= lower-bound upper-bound))
            (when (or (null (if-number (var-symbol->= fact-value))) (null (if-number (var-symbol-<= fact-value))))
               (return-from restrict-inequality (values nil nil))
            )
            (setq binding-list (cons (cons fact-value lower-bound) (subst fact-value lower-bound binding-list)))
            (return-from restrict-inequality (values lower-bound binding-list))
        )
        (if (var-symbol-equivalent fact-value orig-fvalue)
            ;then
            (return-from restrict-inequality (values orig-fvalue orig-blist))
            ;else
            (return-from restrict-inequality (values fact-value binding-list))
        )
    )
)

(defun if-number (val)
    (if (numberp (car val)) (car val) nil)
)

(defun allowed-value (var value)
    (when (not (var-symbol-p var))
        (error "Incorrect input.")
    )
    (when (and (var-symbol-p value) (not (var-symbol-as-specific-as value var)))
        (return-from allowed-value nil)
    )
    (dolist (ineq-pair `((,#'< ,#'var-symbol-<) (,#'> ,#'var-symbol->) 
                         (,#'<= ,#'var-symbol-<=) (,#'>= ,#'var-symbol->=) 
                         (,#'neq ,#'var-symbol-neq)))
        (when (not (test-value-ineq-pair (first ineq-pair) (second ineq-pair) var value))
            (return-from allowed-value nil)
        )
    )
    t
)

(defun var-symbol-lower-bound (var)
    (if (null (var-symbol-< var))
        ;then
        (var-symbol-<= var)
        ;else
        (var-symbol-< var)
    )
)

(defun nonmember (item lst)
    (not (member item lst))
)

(defun var-symbol-upper-bound (var)
    (if (null (var-symbol-> var))
        ;then
        (var-symbol->= var)
        ;else
        (var-symbol-> var)
    )
)

(defun var-symbol-intersection (var1 var2 &aux ovar)
    (when (not (allowed-value var1 (var-symbol-name var2)))
        (return-from var-symbol-intersection nil)
    )
    (when (and (var-symbol-rooted var1) (var-symbol-rooted var2))
        (when (eq (var-symbol-rooted var1) (var-symbol-rooted var2))
            (break "Recombination occurs.")
        )
        (return-from var-symbol-intersection nil)
    )
    
    (setq ovar var1)
    (dolist (ineq-pair `((,#'< ,#'var-symbol-<) (,#'> ,#'var-symbol->) 
                         (,#'<= ,#'var-symbol-<=) (,#'>= ,#'var-symbol->=) 
                         (,#'neq ,#'var-symbol-neq)))
        (setq ovar (restrict-inequality-all (car ineq-pair) (funcall (second ineq-pair) var2) ovar))
        (when (null ovar)
            (return-from var-symbol-intersection nil)
        )
    )
    
    (when (not (eq (var-symbol-rooted var1) (var-symbol-rooted ovar)))
        (error "Rooted removed?")
    )
    
    (when (and (var-symbol-rooted var1) (var-symbol-rooted var2))
        (error "Diverged?")
    )
    
    (when (and (not (var-symbol-rooted ovar)) (var-symbol-rooted var2))
        (when (eq ovar var1)
            (setq ovar (copy-var-sym ovar nil))
        )
        (setf (var-symbol-rooted ovar) (var-symbol-rooted var2))
    )
    

    (when (or (var-symbol-rooted ovar) (var-symbol-rooted var1) (var-symbol-rooted var2))
        (format t "~%~%Var 1 name: ~A root: ~A" (var-symbol-name var1) (var-symbol-rooted var1))
        (format t "~%Var 2 name: ~A root: ~A" (var-symbol-name var2) (var-symbol-rooted var2))
        (format t "~%Ovar  name: ~A root: ~A" (var-symbol-name ovar) (var-symbol-rooted ovar))
    )
    
    ovar
)

(defun restrict-inequality-all (test-fn bound dest-var)
    (dolist (val bound)
        (setq dest-var (restrict-inequality test-fn val dest-var nil))
        (when (null dest-var)
            (return-from restrict-inequality-all nil)
        )
    )
    dest-var
)

(defun test-value-ineq-pair (test-fn access-fn var test-value &aux constraint-value)
    (setq constraint-value (funcall access-fn var))
    (when (var-symbol-p test-value)
        (setq test-value (var-symbol-name test-value))
    )
    (when (or (num-sym-p test-value) (eq test-fn #'nonmember))
        (return-from test-value-ineq-pair (nonmember test-value constraint-value))
    )
    (setq constraint-value (car (remove-if #'num-sym-p constraint-value)))
    (if constraint-value
        ;then
        (funcall test-fn test-value constraint-value)
        ;else
        t
    )
)

(defun num-symbols-in (tree)
    (when (not (consp tree))
        (return-from num-symbols-in (num-sym-p tree))
    )
    (when (and (consp tree) (not (listp (cdr tree))))
        (return-from num-symbols-in 
            (or (num-sym-p (car tree))
                (num-sym-p (cdr tree)))
        )
    )
    (some #'num-symbols-in tree)
)

(defun var-symbols-in (tree)
    (when (not (consp tree))
        (return-from var-symbols-in (var-symbol-p tree))
    )
    (when (and (consp tree) (not (listp (cdr tree))))
        (return-from var-symbols-in 
            (or (var-symbol-p (car tree))
                (var-symbol-p (cdr tree)))
        )
    )
    (some #'var-symbols-in tree)
)

(defun var-or-num-symbols-in (tree)
    (when (not (consp tree))
        (return-from var-or-num-symbols-in 
            (or (var-symbol-p tree) (num-sym-p tree))
        )
    )
    (when (and (consp tree) (not (listp (cdr tree))))
        (return-from var-or-num-symbols-in 
            (or (var-symbol-p (car tree))
                (var-symbol-p (cdr tree))
                (num-sym-p (car tree))
                (num-sym-p (cdr tree)))
        )
    )
    (some #'var-or-num-symbols-in tree)
)

(defun get-return-type (sym)
    (if (var-symbol-p sym)
        ;then
        (var-symbol-return-type sym)
        ;else
        nil
    )
)

(defun variables-in-fact (fact)
    (or (var-symbols-in (fact-args fact))
        (var-symbols-in (fact-value fact))
    )
)

(defun variables-in-cnd (cnd)
    (or (var-symbols-in (cnd-args cnd))
        (var-symbols-in (cnd-value cnd))
    )
)

(defun calculated-variables-in-cnd (cnd)
    (some #'var-symbol-eqn (get-var-symbols cnd))
)

(defun val-close-enough (val1 val2)
    (cond 
        ((and (integerp val1) (integerp val2))
            (eql val1 val2)
        )
        ((and (numberp val1) (numberp val2))
            (within val1 val2 .0001)
        )
        ((and (listp val1) (listp val2))
            (every #'val-close-enough val1 val2)
        )
        (t (return-from val-close-enough (eq val1 val2)))
    )
)

(defun within (real1 real2 percent)
    (or (<= (abs (- real1 real2)) (* percent (abs real1)))
        (< (- percent) real1 0.0 real2 percent)
        (< (- percent) real2 0.0 real1 percent)
    )
)

(defun negate-condition (cnd)
    (setq cnd (copy-cnd cnd))
    (cond
        ((and (null (cnd-inequality cnd)) (member (cnd-value cnd) '(t nil))) 
            (setf (cnd-inequality cnd) nil)
            (setf (cnd-value cnd) (not (cnd-value cnd)))
        )
        ((null (cnd-inequality cnd)) 
            (setf (cnd-inequality cnd) #'neq)
        )
        ((eq (cnd-inequality cnd) #'neq) (setf (cnd-inequality cnd) nil))
        ((eq (cnd-inequality cnd) #'<) (setf (cnd-inequality cnd) #'>=))
        ((eq (cnd-inequality cnd) #'>) (setf (cnd-inequality cnd) #'<=))
        ((eq (cnd-inequality cnd) #'<=) (setf (cnd-inequality cnd) #'>))
        ((eq (cnd-inequality cnd) #'>=) (setf (cnd-inequality cnd) #'<))
        (t (error "Cannot negate condition ~A." cnd))
    )
    cnd
)

(defun reverse-inequality (cnd)
    (setq cnd (copy-cnd cnd))
    (cond
        ((null (cnd-inequality cnd)))
        ((eq (cnd-inequality cnd) #'neq))
        ((eq (cnd-inequality cnd) #'<) (setf (cnd-inequality cnd) #'>=))
        ((eq (cnd-inequality cnd) #'>) (setf (cnd-inequality cnd) #'<=))
        ((eq (cnd-inequality cnd) #'<=) (setf (cnd-inequality cnd) #'>))
        ((eq (cnd-inequality cnd) #'>=) (setf (cnd-inequality cnd) #'<))
        (t (error "Cannot reverse inequality of condition ~A." cnd))
    )
    cnd
)

(defun switch-inequality (cnd)
    (setq cnd (copy-cnd cnd))
    (cond
        ((null (cnd-inequality cnd)))
        ((eq (cnd-inequality cnd) #'neq))
        ((eq (cnd-inequality cnd) #'<) (setf (cnd-inequality cnd) #'>))
        ((eq (cnd-inequality cnd) #'>) (setf (cnd-inequality cnd) #'<))
        ((eq (cnd-inequality cnd) #'<=) (setf (cnd-inequality cnd) #'>=))
        ((eq (cnd-inequality cnd) #'>=) (setf (cnd-inequality cnd) #'<=))
        (t (error "Cannot switch inequality of condition ~A." cnd))
    )
    cnd
)

(defun switch-inequality-name (sym)
    (case sym
        (neq 'neq)
        (< '>)
        (> '<)
        (<= '>=)
        (>= '<=)
        (t (error "Symbol ~A does not indicate an inequality." sym))
    )
)

(defun switch-inequality-fn (fn)
    (case fn
        (#'neq #'neq)
        (#'< #'>)
        (#'> #'<)
        (#'<= #'>=)
        (#'>= #'<=)
        (t (error "Function ~A is not an inequality." fn))
    )
)

(defun equal-cnd (cnd1 cnd2)
    (and (eq (cnd-inequality cnd1) (cnd-inequality cnd2))
         (eq (cnd-type cnd1) (cnd-type cnd2)) 
         (eql (cnd-value cnd1) (cnd-value cnd2))
         (equal (cnd-args cnd1) (cnd-args cnd2))
    )
)

(defun fact-to-cnd (fact)
    (make-cnd 
        :type (fact-type fact)
        :args (fact-args fact)
        :value (fact-value fact)
        :inequality nil
    )
)


(defun cnd-to-fact (cnd)
    (when (not (null (cnd-inequality cnd)))
        (error "Inequality corresponds to more than one fact.")
    )
    (make-fact
        :type (cnd-type cnd)
        :args (cnd-args cnd)
        :value (cnd-value cnd)
    )
)
;; Returns t iff one fact asserts something which is incompatible with the 
;; other.
(defun contradicts-cnd (fact1 fact2)
    (and (eq (cnd-type fact1) (cnd-type fact2)) 
         (not (eql (cnd-value fact1) (cnd-value fact2)))
         (not (is-variable (cnd-value fact1)))
         (not (is-variable (cnd-value fact2)))
         (null (cnd-inequality fact1))
         (null (cnd-inequality fact2))
         (equal (cnd-args fact1) (cnd-args fact2))
    )
)


(defun contradictions-in (facts)
    (loop while (cdr facts) do
        (when (find (car facts) (cdr facts) :test #'contradicts)
            (return-from contradictions-in t)
        )
        (setq facts (cdr facts))
    )
    nil
)

(defun contradictions-in-cnds (facts)
    (setq facts (remove 'assign facts :key #'cnd-type))
    (setq facts (remove 'create facts :key #'cnd-type))
    (loop while (cdr facts) do
        (when (find (car facts) (cdr facts) :test #'contradicts-cnd)
            (return-from contradictions-in-cnds t)
        )
        (setq facts (cdr facts))
    )
    nil
)

(defun is-binary (fact)
    (or (null (fact-value fact)) 
        (eq (fact-value fact) t) 
        (eq (fact-value fact) 'unk)
    )
)

(defun is-variable-term (sym)
    (if (not (symbolp sym))
        ;then
        nil
        ;else
        (case (get sym 'is-var)
            (:yes t)
            (:no nil)
            (otherwise 
                (if (eq (char (symbol-name sym) 0) #\?)
                    ;then
                    (progn
                        (setf (get sym 'is-var) :yes)
                        t
                    )
                    ;else
                    (progn
                        (setf (get sym 'is-var) :no)
                        nil
                    )
                )
            )
        )
    )
)

(defun is-variable (sym)
    (or (var-symbol-p sym)
        (is-variable-term sym)
    )
)

(defun is-assignable-variable (sym)
    (or (and (var-symbol-p sym)
             (var-symbol-temporary sym)
        )
        (is-variable-term sym)
    )
)


