 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: Jun 3, 2009
 ;;;; Project Name: Explanation
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Portions of this file created under CARPE project with US
 ;;;; Government Funding.
 ;;;; 
 ;;;; Program Name: A Cognitive Architecture for MCM Mission Planning, Execution and Replanning 
 ;;;; Sponsor Name: Office of Naval Research
 ;;;;
 ;;;; Contractor Name: Knexus Research Corp.                         
 ;;;; Contractor Address: 9120 Beachway Lane, Springfield
 ;;;;                     Virginia, 22153                               
 ;;;;
 ;;;; Classification: UNCLASSIFIED                             
 ;;;;----------------------------------------------------------------
 ;;;; Distribution Notice:
 ;;;; SBIR DATA RIGHTS Clause
 ;;;; The Government's rights to use, modify, reproduce, release,
 ;;;; perform, display, or disclose technical data or computer 
 ;;;; software marked with this legend are restricted during the 
 ;;;; period shown as provided in paragraph (b)(4) of the Rights in 
 ;;;; Non-commercial Technical Data and Computer Software--Small 
 ;;;; Business Innovative Research (SBIR) Program clause contained in 
 ;;;; the above identified contract.  No restrictions apply after the 
 ;;;; expiration date shown above.  Any reproduction of technical 
 ;;;; data, computer software, or portions thereof marked with this
 ;;;; legend must also reproduce the markings. 
 ;;;; Expiration of SBIR Data Rights Period: 18 May 2016
 ;;;;-----------------------------------------------------------------
 ;;;; Portions of this file created under the SBIR DATA RIGHTS 
 ;;;; are:
 ;;;; Function get-original-state
 ;;;;-----------------------------------------------------------------
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file is intended for functions equivalent to the old atms-funs 
 ;;;; explanation functions used in main/external-interface; hopefully this
 ;;;; file can be used in place of that one.

(in-package :artue)
 
(defvar *save-files* t) 
(defvar *initial-point* 1)
(defvar *history-gap-size* 15)

(defun discoverhistory-explanation-generator-initialize (domain-name
                                                         &rest init-keys
                                                         &aux expgen domain-model
                                                              expgen-class-name)
    (setq expgen-class-name (getf init-keys :expgen-class-name))
    (when (null expgen-class-name)
        (setq expgen-class-name 'discoverhistory-explanation-generator)
    )

    (setq domain-model (get domain-name :domain))
    
    (remf init-keys :expgen-class-name)
    (setq expgen
        (apply #'make-instance expgen-class-name
            :domain-file (domain-file domain-model)
            :domain domain-model
            :last-envmodel (get domain-name :pddls)
            init-keys
        )
    )
    (setf (get-self-fact expgen) (make-fact :type 'self :value (get-self expgen)))
        
    (setf (get-first-point expgen) (make-point expgen :observation :num *initial-point*))
    (setf (get-last-point expgen) (make-point expgen :observation :num *initial-point*))
    (determine-refinement-methods expgen)
    (setq *expgen* expgen)
)

(defmethod expgen-reset ((expgen discoverhistory-explanation-generator)
                         &key (include-graph t))
    (setf (get-last-ex expgen) nil)
    (setf (get-plausible-explanations expgen) nil)
    (setf (get-former-explanations expgen) nil)
    (setf (get-execution-history expgen) nil)
    (setf (get-uncertain-inits expgen) nil)
    (initialize-points expgen *initial-point*)
    (setf (get-possible-event-map expgen) (make-hash-table))
    (when include-graph
        (setf (get-last-hiddens expgen) nil)
        (setf (get-last-mutexes expgen) (make-hash-table))
        (setf (get-total-search-time expgen) 0)
        (setf (get-total-enumeration-time expgen) 0)
    )
    (setf (get-null-ex expgen) nil)
    (get-model-information expgen)
)

(defmethod expgen-advance ((expgen discoverhistory-explanation-generator)
                           observed-prob actions 
                           &key (do-explain t) (do-record t))
    (when (null (get-execution-history expgen))
        (when actions
            (error "Expected no actions with initial observation.")
        )
        (reset-problem-state expgen)
        (setf (get-execution-history expgen) (list (make-initial-observation-from-problem expgen observed-prob (get-first-point expgen))))
        (setf (get-plausible-explanations expgen)
            (list
                (make-explanation 
                    :prior-events  
                        (mapcar #'(lambda (cnd) (make-initial-assumption-occurrence expgen cnd))
                            (mapcar #'fact-to-cnd
                                (mapcar #'convert-problem-value
                                    (pddl-problem-assumed-values observed-prob)
                                )
                            )
                        )
                )
            )
        )
        (setf (get-former-explanations expgen) (get-plausible-explanations expgen))
        (return-from expgen-advance t)
    )
    (finish-output)
    (when do-record
        (log-problems expgen actions observed-prob)
    )
    (finish-output)
    (when (and (null actions) (null (cdr (get-execution-history expgen))))
        (setq actions '((!noop)))
    )
    (if (null actions)
        ;then
        (progn
           (format t "~%No action to verify.")
           ;;Replace old last observation with new one. Untested.
           (break "untested functionality")
           (let ((old-final-ob (car (last (get-execution-history expgen))))
                 (new-final-ob (make-observation-from-problem expgen observed-prob nil))
                )
               (setf (occurrence-point new-final-ob) (occurrence-point old-final-ob))
               (setf (get-execution-history expgen) 
                   (append (butlast (get-execution-history expgen)) (list new-final-ob))
               )
               (when (remove-if #'occurrence-point (get-execution-history expgen))
                   (error "Interval added to history?")
               )
           )
           (return-from expgen-advance t)
        )
        ;else
        (expgen-advance-actions expgen observed-prob actions :do-explain do-explain)
    )
)

(defmethod expgen-advance-actions
              ((expgen discoverhistory-explanation-generator)
               observed-prob actions 
               &key (do-explain t) &allow-other-keys
               &aux ob1 ob2) 
    (format t "~%Execution history:")
    (print-one-per-line (get-execution-history expgen))
    
    (setq ob1 (car (last (get-execution-history expgen))))
    ;(update-observation expgen ob1 observed-prob)
    
    (when (and (get-multi-agent expgen) (null (cdr actions)) (eq (caar actions) '!wait))
        (next-point expgen :exogenous-action)
        (dotimes (i *history-gap-size*)
            (next-point expgen :event)
        )
        ;(consider-possible-events 
            ;expgen nil 
            ;(next-point expgen) 
            ;ob1
            ;:consider-actions t
        ;)
    )

    (dolist (action actions)
        (add-actions-to-history expgen (list action))
        (if (and do-explain (get-envision-events expgen))
            ;then
            (consider-possible-events 
                expgen (list (car (last (get-execution-history expgen))))
                (occurrence-point (car (last (get-execution-history expgen)))) 
                ob1
                :consider-actions nil
                :ground-occurrences t
            )
            ;else
            (dotimes (i *history-gap-size*)
                (next-point expgen :event)
            )
        )
    )
        
    (setq ob2 
        (catch :projection-fail (make-observation-from-problem expgen observed-prob nil))
    )
    
    (maintain-mutexes expgen ob2)

    (when (not (occurrence-p ob2))
        (abandon-learned-event-knowledge expgen (car (get-plausible-explanations expgen)))
        (setq ob2 (make-observation-from-problem expgen observed-prob nil))
    )

    
    (setf (get-execution-history expgen) 
        (append (get-execution-history expgen) (list ob2))
    )
    (when (remove-if-not #'occurrence-interval (get-execution-history expgen))
        (error "Interval added to history?")
    )
    (setf (occurrence-point ob2)
        (next-point expgen :observation)
    )

    (format t "~%~% Observed observation:~%")
    (print-one-per-line 
        (sort 
            (copy-list
                (remove-if-not #'(lambda (fact) (is-transient expgen fact)) 
                    (occurrence-pre (car (last (get-execution-history expgen))))
                )
            )
            #'string-lessp 
            :key #'write-to-string
        )
    )
)

(defvar *temp-ex* nil)
(defun get-current-inconsistencies (expgen &aux (ret nil))
    (dolist (ex (get-plausible-explanations expgen))
        (let ((incs nil))
;            (when (and (or (null (explanation-final-pt ex)) 
;                           (> (- (explanation-final-pt ex) (occurrence-point (get-last-observation))) 10))
;                       (> (occurrence-point (get-last-observation)) 10))
;                (setf (get-temp-ex expgen) ex)
;                (break "Check on temp-ex.")
;            )
            (setq incs
                (find-inconsistencies expgen
                    (extend-explanation expgen ex nil)
                )
            )
            (when (null incs)
                (return-from get-current-inconsistencies nil)
            )
            (push incs ret)
        )
    )
    ret
)

(defmethod expgen-explain ((expgen discoverhistory-explanation-generator)
                           &key (do-explain t) (possible-actions nil)
                           &aux new-ex new-exes inc-list old-exes)
    (when (get-pass-through expgen)
        (return-from expgen-explain (get-plausible-explanations expgen))
    )
    (setq old-exes (get-plausible-explanations expgen))
    
    (when (null old-exes)
        (setq old-exes (list (get-null-explanation expgen)))
    )
    (when (get-max-explanation-count expgen)
        (loop while (> (length old-exes) (get-max-explanation-count expgen)) do
            (setq old-exes (remove (nth (random (length old-exes)) old-exes) old-exes))
        )
        (when (null old-exes) (break "What just happened?"))
    )
    (setq new-exes nil)
  
    (dolist (ex old-exes)
        (trim-cache expgen ex 
            (occurrence-point (get-prior-observation expgen))
        )
        (setq new-ex 
            (catch :projection-fail 
                (extend-explanation expgen (reset-explanation expgen ex :reuse nil) nil)
            )
        )
        (when (eq new-ex 'fail)
            (error "Maintained hypothesis fails.")
        )
        (when (eq new-ex :event-cycle)
            (abandon-learned-event-knowledge expgen nil)
            (setf (get-plausible-explanations expgen) 
                (mapcar #'(lambda (ex) (make-explanation :prior-events (get-assumptions ex))) 
                    old-exes
                )
            )
            (return-from expgen-explain (expgen-explain expgen :do-explain do-explain))
        )
        (push new-ex new-exes)
        (when possible-actions
            (push-all
                (add-actions-to-ex new-ex expgen 
                    (apply #'append 
                        (mapcar 
                            #'(lambda (act) 
                                (convert-pddl-action (second act) expgen nil 
                                    :performer (first act) 
                                    :assume-fn #'assume-hidden-and-closed-world
                                    :facts (facts-at expgen new-ex (occurrence-point (get-prior-observation expgen)))
                                )
                              )
                            (remove '!wait possible-actions :key #'caadr)
                        )
                    )
                )
                new-exes
            )
        )
    )
    (when (> (length new-exes) (length (remove-duplicates new-exes)))
        (break "Why duplication?")
    )
    (setf (get-plausible-explanations expgen) nil)
    (dolist (new-ex new-exes)
        (when (null (select-inconsistencies expgen new-ex (find-inconsistencies expgen new-ex)))
            (push new-ex (get-plausible-explanations expgen))
        )
    )
    (format t "~%~A former explanations. ~A with added actions. ~A are consistent."
        (length old-exes)
        (length new-exes)
        (length (get-plausible-explanations expgen))
    )
    (finish-output)

    (when (get-plausible-explanations expgen)
        (format t "~%~% Plausible explanations:~%")
        (loop for i = 1 then (+ i 1)
              for ex in (get-plausible-explanations expgen)
              do
            (format t "~%Explanation ~A:" i)
            (print-one-per-line
                (get-assumptions ex)
            )
        )
        (setf (get-last-ex expgen) (car (get-plausible-explanations expgen)))
        ;(when (cdr (get-plausible-explanations expgen))
            ;(break "Multiple plausible explanations known.")
        ;)
  
        (return-from expgen-explain nil)
    )
    (finish-output)

    (format t "~%~% Old explanations:~%")
    (loop for i = 1 then (+ i 1)
          for ex in new-exes
          do
        (format t "~&~%Explanation ~A:" i)
        (print-one-per-line
            (get-events ex)
        )
        (format t "~%~%Inconsistencies:")
        (print-one-per-line
            (find-inconsistencies expgen ex)
        )
    )
    (finish-output)

    (when (not do-explain)
        (when (null (get-plausible-explanations expgen))
            (let (new-first-ob)
                (setq new-first-ob (make-initial-observation-from-problem expgen (get-last-problem expgen) (get-first-point expgen)))
                ;; Need to retain initial givens as beliefs; therefore, simply 
                ;; observe hidden facts
                (push-all
                    (get-explanation-hiddens expgen (get-null-explanation expgen))
                    (occurrence-pre new-first-ob)
                )
                (format t "~%~%~%Explanation hiddens:")
                (print-one-per-line (get-explanation-hiddens expgen (get-null-explanation expgen)))
                (format t "~%~%~%New first ob:")
                (print-one-per-line (occurrence-pre new-first-ob))
                
                ;(break "resetting.")
                (expgen-reset expgen :include-graph nil)
                (setf (get-execution-history expgen) (list new-first-ob))
            )
            (setf (get-plausible-explanations expgen) (list (get-null-explanation expgen)))
        )
        (return-from expgen-explain (get-plausible-explanations expgen))
    )

    (setf (get-former-explanations expgen) new-exes)
    (setf (get-plausible-explanations expgen) 
        (find-plausible-explanations expgen new-exes)
    )
    (let ((exp-num 0))
        (dolist (ex (get-plausible-explanations expgen))
            (format t "~%~%Explanation ~A:~%" exp-num)
            (incf exp-num)
            (print-one-per-line 
                (sort
                    (copy-list (get-events ex))
                    #'occurrence-starts-earlier
                )
            )
            (setq inc-list (find-inconsistencies expgen ex))
            (when (remove-if #'inconsistency-ambiguity inc-list)
                (format t "~%Inconsistencies found in hypothesis:")
                (print-one-per-line (remove-if #'inconsistency-ambiguity inc-list))
                (format t "Discrepancies in discovered hypothesis!") 
            )
        )
    )
    (when (null (get-plausible-explanations expgen))
        (when *break-on-explanation-failure*
            (break "Failed to explain.")
        )
        (when (get-sample-ambiguity expgen)
            (setf (get-plausible-explanations expgen) new-exes)
            (setf (get-pass-through expgen) t)
            (return-from expgen-explain new-exes)
        )
        ;;rest is ignored for now.
        (when (null (get-last-ex expgen))
            (setf (get-last-ex expgen) (get-null-explanation expgen))
        )
        (setf (get-plausible-explanations expgen) (form-reset-hypothesis-with-hiddens expgen))
        (return-from expgen-explain (values (get-plausible-explanations expgen) :fail))
    )
    (setf (get-last-ex expgen) (car (get-plausible-explanations expgen)))
    (get-plausible-explanations expgen)
)

(defun form-reset-hypothesis (expgen &aux new-prob)
    (format t "~%~%Forming reset hypothesis.")
    (setq new-prob (copy-pddl-problem (get-last-problem expgen)))
    (add-certain-facts expgen new-prob)
    (expgen-reset expgen :include-graph nil)
    (setf (get-execution-history expgen) 
        (list (make-initial-observation-from-problem expgen new-prob (get-first-point expgen)))
    )
    (setf (get-last-ex expgen) 
        (construct-explanation 
            expgen
            nil
        )
    )
    (setf (get-former-explanations expgen) (get-plausible-explanations expgen))
    (initialize-points expgen (get-first-point expgen))
    (list (get-last-ex expgen))
)

(defun form-reset-hypothesis-with-hiddens (expgen &aux hiddens new-prob)
    (format t "~%~%Forming reset hypothesis.")
    (setq hiddens (get-hidden-trues expgen (get-last-ex expgen)))
    (setq new-prob (copy-pddl-problem (get-last-problem expgen)))
    (add-certain-facts expgen new-prob)
    (expgen-reset expgen :include-graph nil)
    (setf (get-execution-history expgen) 
        (list (make-initial-observation-from-problem expgen new-prob (get-first-point expgen)))
    )
    (setf (get-last-ex expgen) 
        (construct-explanation 
            expgen
            (mapcar #'(lambda (cnd) (make-initial-assumption-occurrence expgen cnd))
                (mapcar #'fact-to-cnd
                    hiddens
                )
            )
        )
    )
    (setf (get-former-explanations expgen) (get-plausible-explanations expgen))
    (initialize-points expgen (get-first-point expgen))
    (list (get-last-ex expgen))
)


(defun translate-inconsistency (inc)
    (list 
        (convert-discover-fact (cnd-to-fact (inconsistency-cnd inc))) 
        (inconsistency-prior-occ inc)
        (inconsistency-next-occ inc)
    )
)

(defun expect-no-win (expgen failure-fact &aux last-ob)
    (setq last-ob (get-last-observation expgen))
    (setf (get-execution-history expgen)
        (append
            (butlast (get-execution-history expgen))
            (list
                (make-occurrence 
                    :signature '(observation)
                    :is-observation t
                    :point (occurrence-point last-ob)
                    :pre (cons failure-fact (occurrence-pre last-ob))
                )
            )
        )
    )
)

(defun get-null-explanation (expgen)
    (when (null (get-null-ex expgen))
        (setf (get-null-ex expgen) (make-explanation))
    )
    (let ((known-facts (nth-value 1 (extend-explanation expgen (get-null-ex expgen) nil))))
        (reset-explanation expgen (get-null-ex expgen) :reuse t)
        (values (get-null-ex expgen) known-facts)
    )
)

(defun make-initial-observation-from-problem (expgen prob pt &aux init-facts initial-occ)
    (setq init-facts 
        (remove 'unknown
            (remove 'or 
                (remove 'oneof 
                    (remove 'invariant 
                        (pddl-problem-init prob) 
                        :key #'car
                    ) 
                    :key #'car
                ) 
                :key #'car
            )
            :key #'car
        )
    )
    (setf (get-uncertain-inits expgen) 
        (set-difference (pddl-problem-init prob) init-facts :test #'equalp)
    )
    
    (setq initial-occ (make-observation-from-problem expgen prob pt))
    (setf (occurrence-pre initial-occ) (remove-duplicates (occurrence-pre initial-occ) :test #'equal-fact))

    ;; Hidden-facts and last-hiddens only need to be called the first time 
    ;; an initial observation is created. This can happen again later if an 
    ;; unexplainable discrepancy occurs. In the unexplainable situation, 
    ;; last-hiddens are retained.
    (when (null (get-last-hiddens expgen))
        (setf (get-hidden-facts expgen) 
            (mapcar #'convert-problem-fact (facts-in (get-uncertain-inits expgen)))
        )
        (setf (get-hidden-facts expgen) 
            (append 
                (get-hidden-facts expgen)
                (mapcar #'contradictory-fact (get-hidden-facts expgen))
            )
        ) 
        
        (setf (get-hidden-facts expgen)
            (remove-duplicates (get-hidden-facts expgen) :test #'equal-fact)
        )

        (setf (get-last-hiddens expgen) 
            (append 
                (occurrence-pre initial-occ) 
                (get-hidden-facts expgen)
            )
        )
    )
    
    (setf (pddl-problem-init prob) init-facts)
    
    (add-uncertain-facts expgen (get-uncertain-inits expgen))
    (push-all (mapcar #'contradictory-fact (get-win-facts expgen)) 
        (occurrence-pre initial-occ)
    )
    ;(push-all (add-uncertain-facts (get-uncertain-inits expgen)) (occurrence-pre occ))
    ;(break "Initial occ created.")
    initial-occ
)

(defun facts-in (tree)
    (find-in-tree tree 
        #'(lambda (x) 
            (and (listp x) (atom (car x))
                 (not (member (car x) '(and or oneof unknown invariant)))
            )
          )
    )
)


(defun basic-facts-in (tree)
    (find-in-tree tree 
        #'(lambda (x) 
            (and (listp x) (atom (car x))
                 (not (member (car x) '(not and or oneof unknown)))
            )
          )
    )
)

(defun make-observation-from-problem (expgen prob pt &aux all-facts)
    (setf (get-last-problem expgen) prob)
    (setq all-facts
        (append
            (mapcar #'convert-problem-fact (pddl-problem-init prob))
            (mapcar #'convert-problem-value (pddl-problem-values prob))
            (convert-problem-objects (pddl-problem-objects prob)
                (get-last-envmodel expgen)
            )
        )
    )
    (setq all-facts
        (union
            all-facts
            (set-difference 
                (get-certain-unsensed-facts expgen)
                all-facts
                :test #'contradicts
            )
            :test #'equal-fact
        )
    )
    
    (validate-observation-facts all-facts (get-last-envmodel expgen))
    
    (make-occurrence
        :signature '(observation)
        :is-observation t
        :point pt
        :pre all-facts
    )
)

(defun validate-observation-facts (facts envmodel &aux preds fs)
    (setq preds (get-predicates envmodel))
    (setq fs (new-fact-set facts))
    (unless (every #'identity
                (mapcar 
                    #'(lambda (fact) 
                        (test-observation-fact fact 
                            (predicate-named (fact-type fact) preds)
                            fs
                            envmodel
                        )
                      )
                    facts
                )
            )
        (error "Observation contains facts that don't fit model.")
    )
)

(defun test-observation-fact (fact pred fs envmodel)
    (when (null pred)
        (format t "~%Error: Found observed fact of type ~a, no match in the model." (fact-type fact))
        (return-from test-observation-fact nil)
    )
    (when (not (= (length (predicate-arg-types pred)) (length (fact-args fact))))
        (format t "~%Error: Found observed fact ~a, expected ~a args." fact (predicate-arg-types pred))
        (return-from test-observation-fact nil)
    )
    (return-from test-observation-fact
        (every #'identity
            (mapcar 
                #'(lambda (arg-type arg) 
                    (test-for-is-fact arg-type arg fs fact envmodel)
                  )
                (mapcar #'second (predicate-arg-types pred)) 
                (fact-args fact)
            )
        )
    )
)

(defun test-for-is-fact (arg-type arg fs fact envmodel)
    (when (type-is-is (fact-type fact))
        (return-from test-for-is-fact t)
    )
    (when (numberp arg)
        (when (and (not (member arg-type '(int real))) (null (intersection (get-supertypes envmodel arg-type) '(int real))))
            (format t "~%Found value ~a in fact ~a where a value of type ~a was expected."
                arg fact arg-type
            )
            (return-from test-for-is-fact nil)
        )
        (return-from test-for-is-fact t)
    )
    (when (stringp arg)
        (when (and (not (member arg-type '(string))) (null (intersection (get-supertypes envmodel arg-type) '(string))))
            (format t "~%Found value ~a in fact ~a where a value of type ~a was expected."
                arg fact arg-type
            )
            (return-from test-for-is-fact nil)
        )
        (return-from test-for-is-fact t)
    )
    (when (not (find-in-fact-set 
                (make-fact :type (get-is-type arg-type) :args (list arg) :value t)
                fs
          )    )
        (format t "~%Found value ~a in fact ~a where a value of type ~a was expected."
            arg fact arg-type
        )
        (return-from test-for-is-fact nil)
    )
    (return-from test-for-is-fact t)
)        

(defun update-observation (expgen ob prob)
    (setf (occurrence-pre ob)
        (union 
            (occurrence-pre ob) 
            (convert-problem-objects (pddl-problem-objects prob)
                (get-last-envmodel expgen)
            )
            :test #'equal-fact
        )
    )
)

(defun get-certain-unsensed-facts (expgen)
    (when (null (get-execution-history expgen))
        (return-from get-certain-unsensed-facts nil)
    )
    (let ((temp-history (get-execution-history expgen)))
        (when (not (occurrence-is-observation (car (last (get-execution-history expgen)))))
            (setq temp-history 
                (append (get-execution-history expgen) 
                    (list 
                        (make-occurrence 
                            :is-observation t 
                            :point t
                        )
                    )
                )
            )
        )
        (remove-if-not 
            #'(lambda (fact) (certain expgen fact)) 
            (nth-value 1 
                (get-null-explanation 
                    (copy-generator expgen
                        :execution-history temp-history
                    )
                )
            )
        )
    )
)

(defun copy-full-generator (expgen)
    (copy-generator expgen
            :self                    (get-self expgen)
            :self-fact               (get-self-fact expgen)
            :envision-events         (get-envision-events expgen)
            :max-cost                (get-max-cost expgen)
            :cost-computation-method (get-cost-computation-method expgen)
            :use-time-cutoff         (get-use-time-cutoff expgen)
            :cutoff-seconds          (get-cutoff-seconds expgen)
            :explanations-requested  (get-explanations-requested expgen)
            :possible-event-map      (get-possible-event-map expgen)
            :removables              (get-removables expgen)
            :causables               (get-causables expgen)
            :last-problem            (get-last-problem expgen)
    )
)

(defun copy-generator (expgen &rest keys &aux std-args)
    (setq std-args
        (list
            :first-point     (get-first-point expgen)
            :last-point      (get-last-point expgen)
            :execution-history (get-execution-history expgen)
            :action-models   (get-action-models expgen)
            :ev-models       (get-ev-models expgen)
            :process-models  (get-process-models expgen)
            :certain-types   (get-certain-types expgen)
            :transient-types (get-transient-types expgen)
            :causable-events (get-causable-events expgen)
            :default-map     (get-default-map expgen)
            :domain          (get-domain expgen)
            :last-envmodel   (get-last-envmodel expgen)
            :point-types     (get-point-types expgen)
            :null-ex         (copy-explanation (get-null-explanation expgen))
        )
    )
            
    (apply #'make-instance 'discoverhistory-explanation-generator
        (append keys std-args)
    )
)

(defun certain (expgen fact)
    (and
        (member (fact-type fact) 
            (get-certain-types expgen)
        )
        (notany #'var-symbol-p (fact-args fact))
        (not (var-symbol-p (fact-value fact)))
    )
)

(defun certain-cnd (expgen cnd)
    ;(or (type-is-is (cnd-type cnd))
        (member (cnd-type cnd) 
            (get-certain-types expgen)
        )
    ;)
)

(defun convert-problem-fact (prob-fact &aux rest-fact val)
    (if (eq (first prob-fact) 'not)
        ;then
        (setq rest-fact (second prob-fact)
              val nil
        )
        ;else
        (setq rest-fact prob-fact
              val t
        )
    )
    (make-fact
        :type (car rest-fact)
        :args (rest rest-fact)
        :value val
    )
)

(defun convert-problem-value (prob-value)
    (make-fact
        :type (caar prob-value)
        :args (rest (car prob-value))
        :value (second prob-value)
    )
)

(defun convert-problem-objects (prob-objects envmodel)
    (mapcan (embed-expgen #'make-is-facts envmodel)
        (make-type-declarations prob-objects)
    )
)

(defun add-actions-to-history (expgen actions &aux pt new-acts)
    (setq new-acts
        (apply #'append (mapcar+ #'convert-pddl-action actions expgen t))
    )
    (when (remove-if #'occurrence-point (get-execution-history expgen))
        (error "Interval added to history?")
    )
    (setq pt (next-point expgen :action))
    (dolist (act new-acts)
        (setf (occurrence-point act) pt)
    )
    (setf (get-execution-history expgen) (append (get-execution-history expgen) new-acts))
)

(defun convert-pddl-action (action expgen pt &key (performer nil) (assume-fn :default) (facts :default) &aux am-set ret-actions self-fact)
    ;(when (not (occurrence-is-observation (car (last (get-execution-history expgen)))))
    ;    (error "Expected an observation before an action.")
    ;)
    (when (member (car action) '(!wait-for !wait))
        (return-from convert-pddl-action
            (list 
                (make-occurrence
                    :signature action
                    :point pt
                    :is-action t
                )
            )
        )
    )
    
    (when (eq :default assume-fn)
        (setq assume-fn
            (if (get-sensing-action-names (get-domain expgen)) 
                #'assume-hidden-and-closed-world
                #'default-closed-world-assume-fn
            )
        )
    )
    
    (when (eq :default facts)
        (setq facts (get-facts (facts-at expgen (get-null-explanation expgen) (occurrence-point (get-last-observation expgen)))))
        ;(setq facts (occurrence-pre (car (last (get-execution-history expgen)))))
    )
    (when (fact-set-p facts)
        (setq facts (copy-fact-set facts))
    )
    (when (listp facts)
        (setq facts (new-fact-set facts))
    )
        
    (setq am-set (remove (car action) (get-action-models expgen) :key #'event-model-type :test-not #'safe-action-name-equals))
    (when performer
        (setq self-fact (make-fact :type 'self :value performer))
        (setq facts 
            (remove-from-fact-set 
                (find-in-fact-set self-fact facts :test #'same-map)
                facts
            )
        )
        (setq facts (add-to-fact-set self-fact facts))
    )
    
    (when (null am-set)
        (error "Action model ~A could not be found." (car action))
    )
    
    ;; This will now enumerate all actions that could happen in any possible 
    ;; world, when preconditions are hidden.
    (setq ret-actions nil)
    (dolist (am am-set)
        (setq ret-actions 
            (append ret-actions
                (enumerate-occs expgen am facts pt
                    :bindings 
                        (pairlis 
                            (mapcar #'first (event-model-arg-types am)) 
                            (ntop (length (event-model-arg-types am)) (cdr action))
                        )
                    :assume-fn assume-fn
                )
            )
        )
    )
    
    (when (and (null ret-actions) (null performer))
        (format t "~%Unable to satisfy preconditions for action ~A." action)
        (format t "~%Diagnostic info:")
        (setq *trace-unify* t)
        (dolist (am am-set)
            (setq ret-actions 
                (append ret-actions
                    (enumerate-occs expgen am facts pt
                        :bindings 
                            (pairlis 
                                (mapcar #'first (event-model-arg-types am)) 
                                (cdr action)
                            )
                        :assume-fn assume-fn
                    )
                )
            )
        )
        (setq *trace-unify* nil)
        (error "Action ~A executed, but preconditions were not met?" action)
    )
    
    (dolist (ret-action ret-actions)
        (setf (occurrence-is-action ret-action) t)
        (setf (occurrence-is-event ret-action) nil)
    )
    
    ;;Temporary fix to prevent multiple actions, for now.
    (when ret-actions (setq ret-actions (list (first ret-actions))))
    
    ret-actions
)

(defun assume-if-standard (expgen cnd facts binding-list)
    (declare (ignore facts binding-list))
    (member (cnd-type cnd) 
        (mapcar #'predicate-name 
            (get-predicates (get-last-envmodel expgen))
        )
    )
)

(defun safe-action-name-equals (name1 name2)
    (equal (string-left-trim "!" (symbol-name name1)) (string-left-trim "!" (symbol-name name2)))
)


(defun log-problems (expgen actions observed-prob &optional (logfile-name nil) &aux problem)
    (when (null logfile-name)
        (setq logfile-name (concatenate 'string "last-transition-" *pid* ".log"))
    )
    ; (when (not (pathnamep file))
        ; (setq file (merge-pathnames file))
    ; )
    (let ((prev-file (concatenate 'string "previous-transition-" *pid* ".log")))
        (when (probe-file prev-file)
            (handler-case (delete-file prev-file)
                (error () (sleep 5))
            )
            (when (probe-file prev-file)
                (handler-case (delete-file prev-file)
                    (error () (error "Deletion of ~A produced error." prev-file))
                )
                (when (probe-file prev-file)
                    (error "Deletion of ~A did not succeed." prev-file)
                )
            )
        )
        (when (probe-file logfile-name)
            (handler-case (rename-file logfile-name prev-file)
                (error ()
                    (format t "~% Pausing due to file collision ...")
                    (finish-output)
                    (sleep 5)
                    (rename-file logfile-name prev-file)
                )
            )
        )
    )
    (with-open-file (out-stream (make-pathname :name logfile-name :defaults *default-pathname-defaults*) :direction :output)
        (write-session out-stream (get-domain-file expgen) nil -1)
        (setq *print-readably* t)
        (setq *print-circle* t)
        (setq problem
            (list
                (cons :name (envmodel-name (get-last-envmodel expgen)))
                (cons :actions actions)
                (cons :init (pddl-problem-init observed-prob))
                (cons :values (pddl-problem-values observed-prob))
                (cons :objs (pddl-problem-objs observed-prob))
                (cons :goal (pddl-problem-goal observed-prob))
                (cons :last-explanation (get-last-ex expgen))
                (cons :plausible-explanations (get-plausible-explanations expgen))
                (cons :execution-history (get-execution-history expgen))
                (cons :possible-event-map (hash-to-alist (get-possible-event-map expgen)))
                (cons :point-count (point-count expgen))
                (cons :last-hiddens (get-last-hiddens expgen))
                (cons :last-mutexes (hash-to-alist (get-last-mutexes expgen)))
                (cons :uncertain-inits (get-uncertain-inits expgen))
                (cons :symbol-counter (get-generated-symbol-counter))
            )
        )
;        (let ((var-syms (get-possible-vars)))
;            (push 
;                (cons :var-syms (pairlis var-syms (mapcar #'symbol-plist var-syms)))
;                problem
;            )
;        )
        (write (change-package problem *package*) :stream out-stream :pretty t)
        (setq *print-circle* nil)
        (setq *print-readably* nil)
    )
    (when *save-files*
        (copy-file logfile-name 
            (concatenate 'string "transition-" *pid* "-"
                (write-to-string (occurrence-point (car (last (get-execution-history expgen)))))
                ".log"
            )
        )
    )
)

(defun read-in-problems (&key (problem-file "last-transition.log") (reload nil) (do-explain t))
    (let 
      ((last-transition nil)
       (session-info nil)
       (expgen nil)
       observed-prob
      )
        (with-open-file (in-stream (pathname problem-file))
            (setq session-info (read-in-session in-stream))
            (setq last-transition (read in-stream nil 'eof))
            (when (eq last-transition 'eof)
                (if (find :before-init session-info :key #'car)
                    ;then
                    (setq last-transition session-info)
                    ;else
                    (error "No transition in transition file.")
                )
            )
        )
        (setq expgen 
            (discoverhistory-explanation-generator-initialize *domain-file* nil)
        )
        (finish-fact-hashing)
        
        (when reload 
            (expgen-reset expgen)
        )

        (setq observed-prob
            (make-pddl-problem
                :name (cdr (assoc :name last-transition))
                :domain (cdr (assoc :name last-transition))
                :objs (cdr (assoc :objs last-transition))
                :init (cdr (assoc :init last-transition))
                :values (cdr (assoc :values last-transition))
                :goal (cdr (assoc :goal last-transition))
            )
        )
            
        (setf (get-last-ex expgen) (cdr (assoc :last-explanation last-transition)))
        (setf (get-plausible-explanations expgen) (cdr (assoc :plausible-explanations last-transition)))
        (when (and (get-last-ex expgen) (null (get-plausible-explanations expgen))) 
            (setf (get-plausible-explanations expgen) (list (get-last-ex expgen)))
        )
        (setf (get-execution-history expgen) (cdr (assoc :execution-history last-transition)))
        (setf (get-possible-event-map expgen) (alist-to-hash (cdr (assoc :possible-event-map last-transition))))
        (initialize-points expgen (cdr (assoc :point-count last-transition)))
        (setf (get-last-hiddens expgen) (cdr (assoc :last-hiddens last-transition)))
        (setf (get-last-mutexes expgen) (cdr (assoc :last-mutexes last-transition)))
        (setf (get-uncertain-inits expgen) (cdr (assoc :uncertain-init last-transition)))
        (set-generated-symbol-counter (cdr (assoc :symbol-counter last-transition)))
        (dolist (sym-plist (cdr (assoc :var-syms last-transition)))
            (setf (symbol-plist (car sym-plist)) (cdr sym-plist))
        )
        (add-uncertain-facts expgen (get-uncertain-inits expgen))
        
        (if (null (cdr (assoc :actions last-transition)))
            ;then
            (progn
                (format t "~%No actions to verify.")
            )
            ;else
            (expgen-advance expgen
                observed-prob
                (cdr (assoc :actions last-transition))
            )
        )
        (when do-explain
            (setq *ex* (reset-explanation expgen (copy-explanation (car (get-plausible-explanations expgen))) :reuse t))
            (break "Before explain")
            (expgen-explain expgen t)
        )
    )
)


(defun get-possible-vars (expgen)
    (union (get-original-vars expgen) 
        (get-var-symbols (mapcar #'symbol-plist (get-original-vars expgen)))
    )
)

(defun get-original-vars (expgen)
    (get-var-symbols
        (mapcar #'occurrence-signature 
            (apply #'append 
                (mapcar #'rest (hash-to-alist (get-possible-event-map expgen)))
            )
        )
    )
)

(defvar *ex* nil) ;For debugging only.

(defun add-uncertain-facts (expgen uncertain-facts)
    ;; We simulate uncertain facts in the initial state by saying if conditions
    ;; were otherwise, it would cause a fact that doesn't occur (paradox). 
    ;; Thereafter, the discover-history algorithm will automatically prove the
    ;; facts true by contradiction from the paradox effect.
    (setf (get-initial-paradox-events expgen)
        (apply #'append
            (mapcar+ #'create-event-from-uncertain-fact 
                (remove 'unknown uncertain-facts :key #'car)
                (get-predicates (get-last-envmodel expgen))
                (get-last-envmodel expgen)
            )
        )
    )
    
    ;; In problems using uncertain initial statements, the closed world 
    ;; assumption applies to the initial state, but any fact showing in those 
    ;; uncertain statements could implicitly be true. Therefore, we give those
    ;; facts a "maybe" in the initial state.
    (setf (get-maybe-inits expgen)
        (remove-duplicates
            (remove (make-fact :type 'initializing-beliefs :args nil :value t)
                (mapcar #'cnd-to-fact
                    (apply #'append 
                        (mapcar #'event-model-conditions (get-initial-paradox-events expgen))
                    )
                )
                :test #'equal-fact
            )
            :test #'equal-fact
        )
    )
    
    (setf (get-maybe-inits expgen) (mapcar+ #'contradictory-fact (get-maybe-inits expgen) 'unk))
    
    (combine-event-models expgen)
    
    ; (when *sensing-action-names*
        ; (setf (get-action-models expgen) (append (get-init-action) *action-models*))
        ; (setf (get-ev-models expgen) (append (get-init-end-event) *ev-models*))
    ; )
    (remove-duplicates (get-maybe-inits expgen) :test #'equal-fact)
)

(defun reset-problem-state (expgen)
    (setf (get-success-models expgen) nil)
    (setf (get-initial-paradox-events expgen) nil)
)

(defun model-goal-success (expgen goal)
    (multiple-value-bind (new-models new-win-fact)
            (create-events-from-goal-success expgen goal 
                (get-predicates (get-last-envmodel expgen)) 
            )
        (setf (get-success-models expgen) (append (get-success-models expgen) new-models))
        (push new-win-fact (get-win-facts expgen))
        (combine-event-models expgen)
        (when (get-execution-history expgen)
            (push (contradictory-fact new-win-fact) 
                (occurrence-pre (car (get-execution-history expgen)))
            )
        )
        new-win-fact
    )
)


(defun combine-event-models (expgen)
    (setf (get-ev-models expgen) 
        (append 
            (get-domain-event-models expgen) 
            (get-initial-paradox-events expgen)
            (get-success-models expgen)
            (get-learned-event-models expgen)
        )
    )
)

(defun get-model-information (expgen &aux (new-events 'none) (preds nil) (dynamic-is-types nil)) 
    (setq preds (get-predicates (get-last-envmodel expgen)))
    (when (get-sense-pred-names (get-domain expgen))
        (setf (get-observable-types expgen) (get-sense-pred-names (get-domain expgen)))
    )
  
    (setf (get-domain-event-models expgen) 
        (create-event-models (get-last-envmodel expgen))
    )
    (setf (get-action-models expgen) 
        (create-action-models (get-last-envmodel expgen))
    )
    
    (setf (get-external-action-models expgen)
        (mapcar+ #'copy-with-self-constraint
            (remove-if 
                #'(lambda (model-type)
                    (or (starts-with "INTERNAL-ACTION" (symbol-name model-type))
                        (starts-with "!" (symbol-name model-type))
                    )
                  )
                (get-action-models expgen)
                :key #'model-type
            )
            expgen
        )
    )
    

    (dolist (model (append (get-domain-event-models expgen) (get-action-models expgen)))
        (dolist (pddl-cnd (model-conditions model))
            (when (eq (cnd-type pddl-cnd) 'create)
                (pushnew (get-is-type (first (cnd-value pddl-cnd))) dynamic-is-types)
            )
        )
    )
    
    (when (null (get-sense-pred-names (get-domain expgen)))
        (setf (get-observable-types expgen) 
            (set-difference
                (mapcar #'predicate-name preds)
                (append
                    dynamic-is-types
                    (envmodel-hidden (get-last-envmodel expgen))
                    (envmodel-internal (get-last-envmodel expgen))
                )
            )
        )
    )
    
    (setf (envmodel-hidden (get-last-envmodel expgen))
        (append
            (envmodel-hidden (get-last-envmodel expgen))
            (remove-if-not #'type-is-is (mapcar #'predicate-name preds))
        )
    )
    
    (setf (get-process-models expgen) 
        (apply #'append
            (mapcar+ #'build-process-model (envmodel-processes (get-last-envmodel expgen)) preds (get-last-envmodel expgen))
        )
    )
    (combine-event-models expgen)
    (setf (get-observation-events expgen) nil)
    (let ((sense-events
            (mapcar+ #'find (mapcar #'first (get-sensing-action-names (get-domain expgen))) (get-ev-models expgen) 
                :key #'event-model-type
            )
          )
          action-model
         )
        (dolist (ev-model sense-events)
            ;;Note: This assumes no "or" in action preconditions.
            (setq action-model 
                (find (cdr (assoc (event-model-type ev-model) (get-sensing-action-names (get-domain expgen))))
                    (get-action-models expgen) :key #'event-model-type
                )
            )
            (when (remove-if #'cnd-is-is (event-model-conditions action-model))
                (dolist (cnd (event-model-conditions ev-model))
                    (when (and (not (cnd-is-is cnd)) 
                               (null (get-trigger-action (cnd-type cnd))))
                        (let ((type-list (find (cnd-type cnd) (get-observation-events expgen))))
                            (if (null type-list)
                                ;then
                                (push (list (cnd-type cnd) ev-model) (get-observation-events expgen))
                                ;else
                                (push ev-model (cdr type-list))
                            )
                        )
                    )
                )
            )
        )
    )

    (setf (get-certain-types expgen)
        (append 
            (set-difference
                (remove-if-not #'type-is-is (mapcar #'predicate-name preds))
                dynamic-is-types
            )
            (envmodel-static (get-last-envmodel expgen)) 
            (envmodel-internal (get-last-envmodel expgen))
        )
    )

    (setf (get-noncausable-events expgen) (get-ev-models expgen))
    (setf (get-causable-events expgen) nil)
    (setf (get-causables expgen) (envmodel-hidden (get-last-envmodel expgen)))
    (setf (get-removables expgen) (envmodel-hidden (get-last-envmodel expgen)))
    
    (loop while new-events do
        (setq new-events nil)
        (dolist (ev (get-noncausable-events expgen))
            (when (or 
                    (intersection (get-causables expgen) 
                        (get-positive-cond-types 
                            (event-model-conditions ev)
                        )
                    )
                    (intersection (get-removables expgen) 
                        (get-negative-cond-types 
                            (event-model-conditions ev)
                        )
                    )
                  )
                (push ev new-events)
            )
        )
        (dolist (ev new-events)
            (setf (get-causables expgen) 
                (union (get-causables expgen) 
                    (get-positive-cond-types (event-model-postconditions ev))
                )
            )
            (setf (get-removables expgen) 
                (union (get-removables expgen) 
                    (get-negative-cond-types (event-model-postconditions ev))
                )
            )
;            (format t "~%~%New event: ~A" ev)
;            (format t "~%Causables: ~A" *causables*)
;            (format t "~%Removables: ~A" *removables*)
        )
        (setf (get-causable-events expgen) (union (get-causable-events expgen) new-events))
        (setf (get-noncausable-events expgen) (set-difference (get-noncausable-events expgen) new-events))
    )

    (setf (get-transient-types expgen)
        (remove-if #'type-is-is
            (union (get-causables expgen) (get-removables expgen))
        )
    )
    (mapcar
        #'(lambda (a)
            (setf (get-transient-types expgen) 
                (union (get-transient-types expgen) 
                    (get-positive-cond-types (event-model-postconditions a))
                )
            )
            (setf (get-transient-types expgen) 
                (union (get-transient-types expgen) 
                    (get-negative-cond-types (event-model-postconditions a))
                )
            )
        )
        (get-action-models expgen)
    )
    (mapcar
        #'(lambda (p)
            (setf (get-transient-types expgen) 
                (union (get-transient-types expgen) 
                    (mapcar #'cnd-type (process-model-effects p))
                )
            )
        )
        (get-process-models expgen)
    )
    (setf (get-default-map expgen) (make-hash-table))
    (mapcar 
        #'(lambda (pair) 
            (setf (gethash (first pair) (get-default-map expgen)) (second pair))
          )
        (envmodel-defaults (get-last-envmodel expgen))
    )
    (mapcar 
        #'(lambda (pred) 
            (when (eq 'fail (gethash (car pred) (get-default-map expgen) 'fail))
                (setf (gethash (car pred) (get-default-map expgen)) nil)
            )
          )
        (envmodel-predicates (get-last-envmodel expgen))
    )
)

(defun copy-with-self-constraint (act-model expgen &aux self-var self-cnd is-cnd)
    (setq act-model (copy-event-model act-model))
    (setq self-cnd (find 'self (model-conditions act-model) :key #'cnd-type))
    (when self-cnd
        (setq self-var (cnd-value self-cnd))
    )
    (setq is-cnd nil)
    (when (and self-var (is-variable-term self-var))
        (setq is-cnd 
            (find-if 
                #'(lambda (cnd) 
                    (and (cnd-is-is cnd) (equalp (cnd-args cnd) (list self-var)))
                  )
                (model-conditions act-model)
            )
        )
        (when is-cnd (setq is-cnd (list is-cnd)))
    )
    (setf (model-conditions act-model)
        (append 
            is-cnd
            (remove (car is-cnd) (model-conditions act-model))
            (list (make-cnd :inequality #'neq :type 'self :value (get-self expgen)))
        )
    )
    act-model
)

(defun get-positive-cond-types (cnd-list)
    (mapcar #'cnd-type (remove-if-not #'is-positive cnd-list))
)

(defun get-negative-cond-types (cnd-list)
    (mapcar #'cnd-type (remove-if #'is-positive cnd-list))
)

(defun convert-discover-fact (fact &aux conv-fact)
    (setq conv-fact
        (cons
            (fact-type fact)
            (fact-args fact)
        )
    )
    (case (fact-value fact)
        ((t) conv-fact)
        ((nil) (list 'not conv-fact))
        ('unk (list 'maybe conv-fact))
        (otherwise (append conv-fact (list (fact-value fact))))
    )
)

(defun convert-to-value-pair (fact)
    (list (cons (fact-type fact) (fact-args fact)) (fact-value fact))
)

(defun get-explanation-hiddens (expgen ex)
    (when (or (null (explanation-cached-hidden-results ex)) 
              (not (member (occurrence-point (get-last-observation expgen))
                           (mapcar #'car (explanation-cached-facts ex))
                           :test #'equalp)))
        (setf (explanation-cached-hidden-results ex)
            (remove-if #'fact-is-is
                (remove-if-not #'(lambda (fact) (is-transient expgen fact)) 
                    (remove-if #'(lambda (fact) (is-observable expgen fact))
                        (nth-value 1 
                            (extend-explanation expgen ex nil)
                        )
                    )
                )
            )
        )
    )
    (explanation-cached-hidden-results ex)
)

(defun get-explanation-hidden-facts (expgen ex)
    (mapcar #'convert-discover-fact
        (remove-if-not #'is-binary
            (get-explanation-hiddens expgen ex)
        )
    )
)

(defun get-explanation-hidden-values (expgen ex)
    (mapcar #'convert-to-value-pair
        (remove-if #'is-binary
            (get-explanation-hiddens expgen ex)
        )
    )
)

(defun get-explanation-hiddens-as-facts (expgen ex)
    (mapcar #'convert-discover-fact
        (get-explanation-hiddens expgen ex)
    )
)

;; This will create a SHOP2-type state object based on the initial observation 
;; from a generator's execution history. For purposes of retaining objects and 
;; name properly, requires an input pddl-problem.
(defun get-original-state (expgen ex sample-prob &aux orig-facts)
    (setq orig-facts
        (append
            (occurrence-pre (car (get-execution-history expgen)))
            (apply #'append
                (mapcar #'occurrence-pre 
                    (remove (get-first-point expgen) (get-events ex) :key #'occurrence-point :test-not #'eq)
                )
            )
        )
    )
    (let ((temp-prob (copy-pddl-problem sample-prob)))
        (setf (pddl-problem-init temp-prob)
            (mapcar #'convert-discover-fact 
                (remove-if-not #'is-binary 
                    orig-facts
                )
            )
        )
        (setf (pddl-problem-values temp-prob)
            (mapcar #'convert-to-value-pair 
                (remove-if #'is-binary 
                    orig-facts
                )
            )
        )
        (export-problem-to-shop temp-prob)
    )
)
                    

(defun get-certain-facts (expgen)
    (mapcar #'convert-discover-fact
        (remove-if-not #'is-binary
            (get-certain-unsensed-facts expgen)
        )
    )
)

(defun get-certain-values (expgen)
    (mapcar #'convert-to-value-pair
        (remove-if #'is-binary
            (get-certain-unsensed-facts expgen)
        )
    )
)

(defun get-last-hidden-facts (expgen)
    (mapcar #'convert-discover-fact
        (remove-if-not #'is-binary
            (set-difference (get-last-hiddens expgen) (get-last-hiddens expgen) :test #'contradicts)
        )
    )
)

(defun get-hidden-trues (expgen ex)
    (if (null ex)
        ;then
        nil
        ;else
        (remove-if-not #'fact-value
            (remove-if (embed-expgen #'certain expgen)
                (remove-if (embed-expgen #'is-observable expgen)
                    (nth-value 1 
                        (extend-explanation expgen ex nil)
                    )
                )
            )
        )
    )
)

(defun get-assumption-facts (ex)
    (mapcar #'occurrence-signature (get-assumptions ex))
)


(defun get-sensing-actions-from-model (expgen facts &aux sense-action-model sense-actions)
    (setq sense-actions nil)
    (setq facts (set-difference facts facts :test #'contradicts))
    (dolist (sense (mapcar #'cdr (get-sensing-action-names (get-domain expgen))))
        (setq sense-action-model (find sense (get-action-models expgen) :key #'event-model-type))
        (setq sense-actions 
            (append sense-actions 
                (mapcar #'occurrence-signature 
                    (enumerate-occs expgen sense-action-model facts -1
                        :assume-fn #'default-closed-world-assume-fn
                    )
                )
            )
        )
    )
    sense-actions
)

(defun get-resolver-facts (expgen &aux recent-ob ret)
    (setq recent-ob (find-if #'occurrence-is-observation (get-execution-history expgen) :from-end t))
    (format t "~%Confirmed explanation before:~%~A" *confirmed-explanation*)
    (when *confirmed-explanation* 
        (extend-explanation expgen *confirmed-explanation* nil)
        (when (explanation-new-events *confirmed-explanation*)
            (setf *confirmed-explanation* (reset-explanation expgen *confirmed-explanation* :reuse t))
        )
    )
    (setq ret 
        (append
            (mapcar #'convert-to-value-pair
                (remove-if #'is-binary
                    (occurrence-pre recent-ob)
                )
            )
            (mapcar #'convert-discover-fact
                (remove-if-not #'is-binary
                    (occurrence-pre recent-ob)
                )
            )
            (if *confirmed-explanation* (get-explanation-hidden-facts expgen *confirmed-explanation*))
            (if *confirmed-explanation* (get-explanation-hidden-values expgen *confirmed-explanation*))
        )
    )
    (format t "~%Confirmed explanation after:~%~A" *confirmed-explanation*)
    ret
)

(defun convert-dh-facts (facts)
    (append
        (mapcar #'convert-to-value-pair
            (remove-if #'is-binary
                facts
            )
        )
        (mapcar #'convert-discover-fact
            (remove-if-not #'is-binary
                facts
            )
        )
    )
)    

(defun convert-dh-condition (cnd)
    (cond 
        ((eq (cnd-type cnd) 'eval)
            (cnd-value cnd)
        )
        ((and (null (cnd-inequality cnd)) (null (cnd-value cnd)))
            (list 'not (convert-dh-condition (negate-condition cnd)))
        )
        ((and (null (cnd-inequality cnd)) (eq (cnd-value cnd) t))
            (cons (cnd-type cnd) (cnd-args cnd))
        )
        ((null (cnd-inequality cnd))
            (list 'eq (cons (cnd-type cnd) (cnd-args cnd)) (cnd-value cnd))
        )
        (t
            (list
                (nth-value 2 (function-lambda-expression (cnd-inequality cnd)))
                (cons (cnd-type cnd) (cnd-args cnd)) 
                (cnd-value cnd)
            )
        )
    )
)    

(defun build-resolver-state (expgen &aux recent-ob ret-facts)
    (setq recent-ob (find-if #'occurrence-is-observation (get-execution-history expgen) :from-end t))
    (setq ret-facts
        (append
            (mapcar #'(lambda (fct) (list 'is fct)) 
                (get-resolver-facts expgen)
            )
            (mapcar #'convert-to-is-a
                (remove-if-not #'fact-is-is 
                    (occurrence-pre recent-ob)
                )
            )
        )
    )
    
    (let ((temp-history (butlast (get-execution-history expgen))))
        (when temp-history
            (setq ret-facts
                (append ret-facts
                    (mapcar #'(lambda (fct) (list 'was fct))
                        (with-substitute (get-execution-history temp-history expgen)
                            (get-resolver-facts expgen)
                        )
                    )
                )
            )
        )
    )
    (remove-duplicates ret-facts :test #'equal)
)

(defmacro with-substitute (params &rest forms)
    (multiple-value-bind (accessor-fn temp-val obj) (values-list params)
        `(let ((_old-val (,accessor-fn ,obj)) _ret-val)
            (setf (,accessor-fn ,obj) ,temp-val)
            ,@(butlast forms)
            (setq _ret-val ,(car (last forms)))
            (setf (,accessor-fn ,obj) _old-val)
            _ret-val
         )
    )
)

(defun convert-to-is-a (fact)
    (list 'isa (first (fact-args fact)) (fact-is-is fact))
)

;; Found this function to have an error (get-default-transients does not exist);
;; also, it is unused. 7/1/2013 MCM
; (defun get-explanation-postfacts (ex &aux facts)
    ; (setq facts
        ; (remove-if-not #'(lambda (fact) (is-transient expgen fact)) 
            ; (nth-value 1 (construct-explanation (get-assumptions ex)))
        ; )
    ; )
    ; (setq facts 
        ; (append 
            ; (set-difference facts facts :test #'contradicts)
            ; (set-difference 
                ; (get-default-transients) 
                ; facts 
                ; :test #'contradicts
            ; )
        ; )
    ; )
    ; (setq facts (set-difference facts facts :test #'contradicts))
    ; facts
; )


(defun find-alternative-explanations (expgen explanations)
    (let ((original-explanations-requested (get-explanations-requested expgen)) 
          (found-exes nil)
          (new-found-exes nil)
          (constraints nil))
        (when (null explanations) (break "Problem in find-alternative-explanations."))
        (setf (get-explanations-requested expgen) (+ (length explanations) (get-explanations-requested expgen)))
        (setf (get-ev-models expgen) (append constraints (get-ev-models expgen)))
        (setq new-found-exes 
            (find-plausible-explanations expgen (get-former-explanations expgen))
        )
        (dolist (ex new-found-exes)
            (when (not (member (get-assumption-changes ex) 
                          explanations 
                          :key #'get-assumption-changes 
                          :test #'assumption-change-supersetp
                  )    )
                (push ex found-exes)
            )
        )
        (setf (get-explanations-requested expgen) original-explanations-requested)
        (when (null found-exes)
            (setq explanations (mapcar+ #'reset-explanation expgen explanations :reuse nil))
            (dolist (ex explanations)
                (setf (explanation-cached-inconsistencies ex)
                    (list 
                        (list (point-next expgen (get-first-point expgen)) 
                            (make-inconsistency
                                :cnd (make-cnd :type '_failure :args nil :value t)
                                :prior-occ nil
                                :next-occ (find-if #'occurrence-is-observation (cdr (get-execution-history expgen))) 
                            )
                        )
                    )
                )
            )
            (setq found-exes 
                (find-plausible-explanations expgen explanations :keep-exes t)
            )
            (when (null found-exes)
                (break "Failure to find alternative explanations.")
            )
        )
        (setf (get-plausible-explanations expgen) found-exes)
    )
)

(defun make-explanation-based-problem (expgen ex problem &aux facts)
    (setq problem (copy-pddl-problem problem))
    (setf (pddl-problem-name problem) 'explained)
    (setq facts
        (append
            (mapcar+ #'bind-fact-if-necessary 
                (occurrence-pre (get-last-observation expgen))
                (explanation-bindings ex)
            )
            (get-explanation-hiddens expgen ex)
            (get-certain-unsensed-facts expgen)
         )
    )

    (setf (pddl-problem-init problem)
        (mapcar #'convert-discover-fact
            (remove-if-not #'is-binary facts)
        )
    )
    (setf (pddl-problem-values problem)
        (mapcar #'convert-to-value-pair
            (remove-if #'is-binary facts)
        )
    )
    problem
)

(defun make-explanationless-problem (expgen problem &aux facts)
    (setq problem (copy-pddl-problem problem))
    (setq facts
        (append
            (occurrence-pre (get-last-observation expgen))
            (set-difference (get-last-hiddens expgen) (get-last-hiddens expgen) :test #'contradicts)
            (get-certain-unsensed-facts expgen)
         )
    )

    (setf (pddl-problem-init problem)
        (mapcar #'convert-discover-fact
            (remove-if-not #'is-binary facts)
        )
    )
    (setf (pddl-problem-values problem)
        (mapcar #'convert-to-value-pair
            (remove-if #'is-binary facts)
        )
    )
    problem
)

(defun determine-refinement-methods (expgen)
    (setq *learning-refinement-methods*
        (list 
            #'refine-by-giving-up-event-in-cycle
            #'refine-by-giving-up-learned-event
            #'refine-by-hypothesizing-unknown-event
        )
    )
            
    
    (setq *pregen-refinement-methods*
        (list 
            #'refine-by-removing-prior-occurrence
            #'refine-by-removing-next-occurrence
            #'refine-by-hypothesizing-initial-value
            #'refine-by-binding-prior-occ
            #'refine-by-binding-next-occ
            #'refine-by-reversing-order
            #'refine-by-hypothesizing-exogenous-action
            #'refine-by-hypothesizing-event-occurrence
            #'refine-by-uncovering-overwritten-effect
        )
    )
    
    (setq *not-pregen-refinement-methods*
        (list 
            #'refine-by-adding-occurrence
            #'refine-by-removing-prior-occurrence
            #'refine-by-removing-next-occurrence
            #'refine-by-hypothesizing-initial-value
            #'refine-by-binding-prior-occ
            #'refine-by-binding-next-occ
        )
    )
    
    (if (get-envision-events expgen)
        ;then
        (setq *refinement-methods* *not-pregen-refinement-methods*)
        ;else
        (setq *refinement-methods* *pregen-refinement-methods*)
    )
    
    (if (get-learning-allowed expgen)
        ;then
        (setq *refinement-methods* (union *refinement-methods* *learning-refinement-methods*))
        ;else
        (setq *refinement-methods* (set-difference *refinement-methods* *learning-refinement-methods*))
    )
)