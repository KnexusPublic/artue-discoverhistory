 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: August 12, 2014
 ;;;; Project Name: Explanation
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file provides functions for constructing plans that maximize 
 ;;;; information gain using DiscoverHistory-based projection. 

;; Construct a plan that will reduce the number of consistent explanations. 

(in-package :artue)

(defun construct-diagnostic-plan (expgen)
    (get-plan (car (search-diagnostics expgen (get-plausible-explanations expgen))))
)



(defclass diagnostic-node ()
  (
    (explanations
        :accessor get-explanations
        :initarg :explanations
    )
    (explanation-count
        :accessor get-explanation-count
        :initarg :explanation-count
    )
    (plan
        :accessor get-plan
        :initarg :plan
    )
    (cur-expgen
        :accessor get-expgen
        :initarg :expgen
    )
    (certain-consequences
        :accessor get-certain-consequences
        :initform nil
    )
    (consequence-table
        :accessor get-consequence-table
        :initform nil
    )
    (information-gain
        :accessor get-information-gain
        :initform nil
    )
  )
)

(defun remove-node-explanation (node explanation)
    (setf (get-explanations node) (remove explanation (get-explanations node)))
    (remhash explanation (get-consequence-table node))
)

(defun search-diagnostics (expgen explanations)
    (node-search 
        (list (make-instance 'diagnostic-node :explanations explanations :expgen expgen :plan nil :explanation-count (length explanations)))
        #'add-actions
        #'(lambda (dn) (length (get-plan dn)))
        ;#'added-info
        #'(lambda (dn) (< (length (get-explanations dn)) (get-explanation-count dn)))
        ;#'(lambda (dn) (null (get-explanations dn)))
        :solution-count 1
        :max-cost 30
        :max-seconds (if (get-use-time-cutoff expgen) (get-cutoff-seconds expgen) nil)
        :search-method :iterative-deepening
        :redundant-fn #'identity
        :node-limit 30000
        :search-type :achieve
    )
)


(defun search-diagnostics-exhaustive (expgen explanations depth)
    (node-search 
        (list (make-instance 'diagnostic-node :explanations explanations :expgen expgen :plan nil :explanation-count (length explanations)))
        #'add-actions
        #'added-info
        #'(lambda (dn) (< (length (get-explanations dn)) (get-explanation-count dn)))
        ;#'(lambda (dn) (null (get-explanations dn)))
        :solution-count 5
        :max-cost 10
        :max-seconds (if (get-use-time-cutoff expgen) (get-cutoff-seconds expgen) nil)
        :search-method :depth-first
        :redundant-fn #'identity
        :node-limit 30000
        :max-depth depth
        :search-type :achieve
    )
)

(defun add-actions (node &aux ret-nodes child-expgen)
    (format t "~%Plan: ~A Value: ~A" (get-plan node) (added-info node))
    (dolist (act (enumerate-possible-actions node))
        (setq child-expgen (copy-generator (get-expgen node)))
        (fake-advance child-expgen node act)
        (push
            (make-instance 'diagnostic-node
                :plan (append (get-plan node) (list act))
                :expgen child-expgen
                :explanations (mapcar #'copy-explanation (get-explanations node))
                :explanation-count (get-explanation-count node)
            )
            ret-nodes
        )
        (remove-unique-explanations (car ret-nodes))
    )
    ret-nodes
)

(defun added-info (node)
    (when (null (get-information-gain node))
        (setf (get-information-gain node)
            (+ 
                (* .1 (length (get-plan node)))
                (* 11 (log (get-explanation-count node) 2)) 
                (- (calculate-entropy node #'get-observable-consequences))
                (* .01 (calculate-fact-entropy node #'get-unobservable-consequences))
                (* -10 (calculate-entropy node #'get-unobservable-consequences))
            )
        )
    )
    (get-information-gain node)
)
    
(defun enumerate-possible-actions (node &aux actions expgen)
    (setq expgen (get-expgen node))
    (setq actions nil)
    (dolist (am (remove 'noop (remove '!replan (get-action-models expgen) :key #'model-type) :key #'model-type))
        (setq actions 
            (append actions
                (enumerate-occs expgen am 
                    (get-null-consequence node)
                    (1+ (get-last-point expgen))
                    :assume-fn #'default-closed-world-assume-fn
                    :ground-params t
                )
            )
        )
    )
    (dolist (act actions)
        (setf (occurrence-is-action act) t)
        (setf (occurrence-is-event act) nil)
    )
        
    actions
)


(defun fake-advance (expgen node action &aux ob)
    (next-point expgen :action)
    (dotimes (i 100)
        (next-point expgen :event)
    )
    (next-point expgen :observation)
    (setq ob (make-occurrence :is-observation t :point (get-last-point expgen)))
    (setf (get-execution-history expgen) 
        (append (get-execution-history expgen) (list action ob))
    )
    ;(setf (occurrence-pre ob) 
    ;    (append 
    ;        (get-null-consequence node)
    ;        (get-certain-unsensed-facts (get-expgen node))
    ;    )
    ;)

)

(defun remove-unique-explanations (node &aux consequences)
    (get-all-consequences node)
    (setq consequences
        (mapcar
            #'(lambda (expl)
                (cons 
                    (remove-if-not (embed-expgen #'is-observable (get-expgen node))
                        (gethash expl (get-consequence-table node))
                    )
                    expl 
                )
              )
            (get-explanations node)
        )
    )
    (loop while consequences do
        (multiple-value-bind (same-conseq diff-conseq)
            (split-if 
                #'(lambda (conseq-facts) 
                    (set-equal (car conseq-facts) (caar consequences) :test #'equal-fact)
                  )
                (cdr consequences)
            )
            (when (null same-conseq)
                (remove-node-explanation node (cdar consequences))
            )
            (setq consequences diff-conseq)
        )
    )
)

(defun calculate-entropy (node consequence-fn &aux consequences sum prob frac)
    (setq consequences (funcall consequence-fn node))
    (setq frac (/ 1 (get-explanation-count node)))
    ;; Initialize sum with the contributions of (already removed) unique 
    ;; explanations, each of which by definition belongs to a group of size 1.
    (setq sum (- (* (log frac 2) frac (- (get-explanation-count node) (length (get-explanations node))))))
    (loop while consequences do
        (multiple-value-bind (same-conseq diff-conseq)
            (split-if 
                #'(lambda (conseq-facts) 
                    (set-equal conseq-facts (car consequences) :test #'equal-fact)
                  )
                (cdr consequences)
            )
            
            ;; Add to summation value of -p_i * log_2(p_i), entropy contribution 
            ;; of one homogeneous group
            (setq prob (* frac (+ 1 (length same-conseq))))
            (decf sum (* prob (log prob 2)))
            ;(format t "~%Prob: ~A Sum: ~A" prob sum)
            
            (setq consequences diff-conseq)
        )
    )
    sum
)


(defun calculate-fact-entropy (node consequence-fn 
                               &aux sum prob frac unshared-facts remaining-facts
                                    new-remaining-facts unshared-by-ex)
    (when (null (get-explanations node)) (return-from calculate-fact-entropy -10))
    (multiple-value-setq (unshared-facts unshared-by-ex)
        (find-unshared-facts (funcall consequence-fn node))
    )
    (setq frac (/ 1 (get-explanation-count node)))
    (setq sum 0)
    ;; Initialize sum with the contributions of (already removed) unique 
    ;; explanations, each of which by definition belongs to a group of size 1.
    (dolist (unshared-fact unshared-facts)
        (setq remaining-facts 
            (remove nil (mapcar #'(lambda (one-conseq) (find unshared-fact one-conseq :test #'same-map)) unshared-by-ex)))
        (decf sum (* (log frac 2) frac (- (get-explanation-count node) (length (get-explanations node)))))
        (setq prob (* frac (- (length (get-explanations node)) (length remaining-facts))))
        (when (> prob 0)
            (decf sum (* prob (log prob 2)))
            ;(format t "~%Fact: none Prob: ~A Sum: ~A" prob sum)
        )
        (loop while remaining-facts do
            (setq new-remaining-facts (remove (car remaining-facts) remaining-facts :test #'equal-fact))
                
            ;; Add to summation value of -p_i * log_2(p_i), entropy contribution 
            ;; of one homogeneous group
            (setq prob (* frac (- (length remaining-facts) (length new-remaining-facts))))
            (decf sum (* prob (log prob 2)))
            ;(format t "~%Fact: ~A Prob: ~A Sum: ~A" (car remaining-facts) prob sum)
            (setq remaining-facts new-remaining-facts)
        )
    )
    (/ sum (log frac 2))
)

(defun find-unshared-facts (consequences &aux shared-facts)
    (setq shared-facts (reduce #'(lambda (facts1 facts2) (intersection facts1 facts2 :test #'equal-fact)) consequences))
    (setq consequences (mapcar+ #'set-difference consequences shared-facts :test #'equal-fact))
    (values 
        (reduce #'(lambda (facts1 facts2) (union facts1 facts2 :test #'same-map)) consequences :initial-value nil)
        consequences
    )
)    

(defun get-unobservable-consequences (node)
    (mapcar 
        #'(lambda (conseq-set) (remove-if (embed-expgen #'is-observable (get-expgen node)) conseq-set))
        (get-all-consequences node)
    )
)

(defun get-observable-consequences (node)
    (mapcar 
        #'(lambda (conseq-set) (remove-if-not (embed-expgen #'is-observable (get-expgen node)) conseq-set))
        (get-all-consequences node)
    )
)

(defun get-all-consequences (node)
    (when (null (get-consequence-table node))
        (setf (get-consequence-table node) (make-hash-table))
        (dolist (expl (get-explanations node))
            (setf (gethash expl (get-consequence-table node)) 
                (get-one-consequences expl (get-expgen node))
            )
        )
    )
    (mapcar #'cdr (hash-to-alist (get-consequence-table node)))
)

(defun get-null-consequence (node)
    (when (null (get-certain-consequences node))
        (setf (get-certain-consequences node)
            (append
                (remove-if-not (embed-expgen #'is-observable (get-expgen node))
                    (get-one-consequences (get-null-explanation (get-expgen node)) (get-expgen node))
                )
                (get-certain-unsensed-facts (get-expgen node))
            )
        )
    )
    (get-certain-consequences node)
)

(defun get-one-consequences (explanation expgen)
    (remove-if (embed-expgen #'is-default expgen)
        (nth-value 1 (extend-explanation expgen explanation nil))
    )
)

