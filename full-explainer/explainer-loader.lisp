 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: July 1, 2013
 ;;;; Project Name: Explanation
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Portions of this file created under CARPE project with US
 ;;;; Government Funding.
 ;;;; 
 ;;;; Program Name: A Cognitive Architecture for MCM Mission Planning, Execution and Replanning 
 ;;;; Sponsor Name: Office of Naval Research
 ;;;;
 ;;;; Contractor Name: Knexus Research Corp.                         
 ;;;; Contractor Address: 9120 Beachway Lane, Springfield
 ;;;;                     Virginia, 22153                               
 ;;;;
 ;;;; Classification: UNCLASSIFIED                             
 ;;;;----------------------------------------------------------------
 ;;;; Distribution Notice:
 ;;;; SBIR DATA RIGHTS Clause
 ;;;; The Government's rights to use, modify, reproduce, release,
 ;;;; perform, display, or disclose technical data or computer 
 ;;;; software marked with this legend are restricted during the 
 ;;;; period shown as provided in paragraph (b)(4) of the Rights in 
 ;;;; Non-commercial Technical Data and Computer Software--Small 
 ;;;; Business Innovative Research (SBIR) Program clause contained in 
 ;;;; the above identified contract.  No restrictions apply after the 
 ;;;; expiration date shown above.  Any reproduction of technical 
 ;;;; data, computer software, or portions thereof marked with this
 ;;;; legend must also reproduce the markings. 
 ;;;; Expiration of SBIR Data Rights Period: 18 May 2016
 ;;;;-----------------------------------------------------------------
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file contains logic necessary to load the explanation algorithm.

(in-package :artue)
 
;; Trace occurrence consideration
(defvar *trace-occurrence-consideration* nil)

;; Trace occurrence consideration
(defvar *trace-discover-history* nil)


(defclass explanation-generator () ())

(defgeneric expgen-explain (expgen &key &allow-other-keys))
(defgeneric expgen-advance (expgen actions observed-prob &key &allow-other-keys))
(defgeneric expgen-reset (expgen &key &allow-other-keys)) 

(defclass discoverhistory-explanation-generator (explanation-generator)
  (
    (observable-types
        :initform nil
        :accessor get-observable-types
        :initarg  :observable-types
    )
    (hidden-facts
        :initform nil
        :accessor get-hidden-facts
        :initarg  :hidden-facts
    )
    (last-envmodel
        :initform nil
        :accessor get-last-envmodel
        :initarg  :last-envmodel
    )
    (action-models
        :initform nil
        :accessor get-action-models
        :initarg  :action-models
    )
    (process-models
        :initform nil
        :accessor get-process-models
        :initarg  :process-models
    )
    (external-action-models
        :initform nil
        :accessor get-external-action-models
        :initarg  :external-action-models
    )
    (causables
        :initform nil
        :accessor get-causables
        :initarg  :causables
    )
    (removables
        :initform nil
        :accessor get-removables
        :initarg  :removables
    )
    (observation-events
        :initform nil
        :accessor get-observation-events
        :initarg  :observation-events
    )
    (plausible-explanations
        :initform nil
        :accessor get-plausible-explanations
        :initarg  :plausible-explanations
    )
    (transient-types
        :initform nil
        :accessor get-transient-types
        :initarg  :transient-types
    )

;; hashtable that maps fact types to default values
    (default-map
        :initform nil
        :accessor get-default-map
        :initarg  :default-map
    )

;;List of known event models that can be used in an explanation.
    (ev-models
        :initform nil
        :accessor get-ev-models
        :initarg  :ev-models
    )

;; Global list of observations received and actions taken so far during 
;; execution
    (execution-history
        :initform nil
        :accessor get-execution-history
        :initarg  :execution-history
    )

;; Lists the fact types which are always known (either static or internal).
    (certain-types
        :initform nil
        :accessor get-certain-types
        :initarg  :certain-types
    )
    (last-ex
        :initform nil
        :accessor get-last-ex
        :initarg  :last-ex
    )
    (causable-events
        :initform nil
        :accessor get-causable-events
        :initarg  :causable-events
    )                   
    (noncausable-events
        :initform nil
        :accessor get-noncausable-events
        :initarg  :noncausable-events
    )
    (uncertain-inits
        :initform nil
        :accessor get-uncertain-inits
        :initarg  :uncertain-inits
    )
    (maybe-inits
        :initform nil
        :accessor get-maybe-inits
        :initarg  :maybe-inits
    )
    (initial-paradox-events
        :initform nil
        :accessor get-initial-paradox-events
        :initarg  :initial-paradox-events
    )
    (success-models
        :initform nil
        :accessor get-success-models
        :initarg  :success-models
    )
    (domain-event-models
        :initform nil
        :accessor get-domain-event-models
        :initarg  :domain-event-models
    )
    (learned-event-models
        :initform nil
        :accessor get-learned-event-models
        :initarg  :learned-event-models
    )
    (former-explanations
        :initform nil
        :accessor get-former-explanations
        :initarg  :former-explanations
    )
;; This parameter controls the maximum cost before iterative deepening returns
;; without a solution
    (max-cost
        :initform 7
        :accessor get-max-cost
        :initarg  :max-cost
    )

;; This parameter controls the maximum time an iteration is allowed to go 
;; before being cut off with no solution returned
    (use-time-cutoff
        :initform t
        :accessor get-use-time-cutoff
        :initarg  :use-time-cutoff
    )

;; This parameter controls how long, in seconds, an iteration of explanation
;; search is allowed to go
    (cutoff-seconds
        :initform 240
        :accessor get-cutoff-seconds
        :initarg  :cutoff-seconds
    )

;; This global controls how many events are allowed in an explanation; search
;; must return with no result rather than searching past this cost. This is the
;; current cost of an iteration of iterative deepening search.
    (cost-limit
        :initform nil
        :accessor get-cost-limit
        :initarg  :cost-limit
    )

;; This global indicates the execution clock time when an iteration has gone too 
;; long. Search should return with no results any time after this mark. 
    (cutoff-time
        :initform 0
        :accessor get-cutoff-time
        :initarg  :cutoff-time
    )

;; If supplied, this function is run after every call to discover-history to update search 
;; parameters such as max-cost, cutoff-time, and the required solution count. See utils/search.lisp.
    (search-parameter-update-fn
        :initform #'utils::noop-parameter-update
        :accessor get-search-parameter-update-fn
        :initarg  :search-parameter-update-fn
    )

;; The last point created; points are always created in order, and the new one 
;; is created from the last, so the last must always be available.
;(defvar *last-point* nil)

;;Map from observations made to events that are known to be possible after them.
    (possible-event-map
        :initform (make-hash-table) 
        :accessor get-possible-event-map
        :initarg  :possible-event-map
    )

;; A special debugging variable. Generally having no assumptions after 
;; explanation is a problem, but in some cases assumption removal is a logical 
;; response.
    (break-on-no-assumptions
        :initform t
        :accessor get-break-on-no-assumptions
        :initarg  :break-on-no-assumptions
    )

;; For statistics
    (last-search-was-cutoff
        :initform nil
        :accessor get-last-search-was-cutoff
        :initarg  :last-search-was-cutoff
    )

;(defvar *cost-computation-method* :change-count)
    (cost-computation-method
        :initform :branch-based
        :accessor get-cost-computation-method
        :initarg  :cost-computation-method
    )

    (explanations-requested
        :initform 1
        :accessor get-explanations-requested
        :initarg  :explanations-requested
    )

    (learning-allowed
        :initform nil
        :accessor get-learning-allowed
        :initarg  :learning-allowed
    )

    (multi-agent
        :initform nil
        :accessor get-multi-agent
        :initarg  :multi-agent
    )

    (envision-events
        :initform nil
        :accessor get-envision-events
        :initarg  :envision-events
    )

    (completed-explanations
        :initform nil
        :accessor get-completed-explanations
        :initarg  :completed-explanations
    )

    (domain
        :initform nil
        :accessor get-domain
        :initarg  :domain
    )

    (last-mutexes
        :initform nil
        :accessor get-last-mutexes
        :initarg  :last-mutexes
    )

    (last-hiddens
        :initform nil
        :accessor get-last-hiddens
        :initarg  :last-hiddens
    )

    (win-facts
        :initform nil
        :accessor get-win-facts
        :initarg  :win-facts
    )
    (null-ex
        :initform nil
        :accessor get-null-ex
        :initarg  :null-ex
    )
    (last-problem
        :initform nil
        :accessor get-last-problem
        :initarg  :last-problem
    )
    (domain-file
        :initform nil
        :reader   get-domain-file
        :initarg  :domain-file 
    )
    
    (first-point
        :initform nil
        :accessor get-first-point
        :initarg  :first-point
    )
    (last-point
        :initform nil
        :accessor get-last-point
        :initarg  :last-point
    )
    (self
        :initform nil
        :accessor get-self
        :initarg  :self
    )
    (self-fact
        :initform nil
        :accessor get-self-fact
        :initarg  :self-fact
    )
    (point-types
        :initform (make-array '(10000))
        :accessor get-point-types
        :initarg  :point-types
    )
    (hypothetical-action-cache
        :initform nil
        :accessor get-hypothetical-action-cache
    )
    (inconsistency-selection-method
        :initform '(:prefer-unambiguous :prefer-recent :ignore-ambiguous)
        :accessor get-inconsistency-selection-method
        :initarg  :inconsistency-selection-method
    )
    (heuristic-method
        :initform '() ;possible :inconsistency-count
        :accessor get-heuristic-method
        :initarg  :heuristic-method
    )
    (pass-through
        :initform nil
        :accessor get-pass-through
    )
    (sample-ambiguity
        :initarg  :sample-ambiguity
        :initform nil
        :accessor get-sample-ambiguity
    )
    (max-explanation-count
        :initarg  :max-explanation-count
        :initform nil
        :accessor get-max-explanation-count
    )
    
    (total-enumeration-time
        :initform 0
        :accessor get-total-enumeration-time
    )
    (total-search-time
        :initform 0
        :accessor get-total-search-time
    )
  )
)


(bps-load-files 
    (make-pathname :directory '(:relative "full-explainer")) 
    '("algebra" "event-facts" "event-explain-model" "event-fact-set" 
      "event-domain-model" "explainer-utils" "event-bindings" "event-unify"
      "occurrence-enumeration" "explanation-construction"
      "event-enumeration" "new-event-explain" "new-transition-manager" "neq"
     )
    :action :compile-if-newer
)

(bps-load-files 
    (make-pathname :directory '(:relative "deductive-explainer")) 
    '("deductive-explainer")
    :action :compile-if-newer
)

(defun explanation-persistent-globals ()
    '(*plausible-explanations* *last-ex* *action-models* 
      *process-models* *observation-events* *causables* *removables*
      *causable-events* *noncausable-events* *transient-types* *uncertain-inits*
      *maybe-inits* *initial-paradox-events* *success-models* 
      *hidden-facts* *former-explanations* *max-cost* *use-time-cutoff* 
      *cutoff-seconds* *cost-computation-method* 
      *certain-types* *ev-models* *execution-history* *last-point* 
      *last-envmodel* *domain-event-models* *sense-pred-names* 
      *sensing-action-names* *possible-event-map* *last-hiddens*
      *last-mutexes* *observable-types* *default-map*)
)


(defun cut-search-short-at-halfway-point (&key solutions required-solution-count cost-limit max-cost cutoff-time &allow-other-keys)
    (if (and solutions (>= (length solutions) (/ required-solution-count 2)) (> max-cost (+ cost-limit 10)))
        ;then
        (list
            :cutoff-time (+ (get-internal-run-time) (* 600 internal-time-units-per-second))
            :max-cost (+ cost-limit 10)
        )
        ;else
        nil
    )
)

(defun thirty-seconds-past-halfway-point (&key solutions required-solution-count cost-limit max-cost cutoff-time &allow-other-keys)
    (if (and solutions (>= (length solutions) (/ required-solution-count 2)))
        ;then
        (list
            :cutoff-time (min cutoff-time (+ (get-internal-run-time) (* 30 internal-time-units-per-second)))
        )
        ;else
        nil
    )
)

