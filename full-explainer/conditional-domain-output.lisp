 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: July 1, 2013
 ;;;; Project Name: Explanation
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Portions of this file created under CARPE project with US
 ;;;; Government Funding.
 ;;;; 
 ;;;; Program Name: A Cognitive Architecture for MCM Mission Planning, Execution and Replanning 
 ;;;; Sponsor Name: Office of Naval Research
 ;;;;
 ;;;; Contractor Name: Knexus Research Corp.                         
 ;;;; Contractor Address: 9120 Beachway Lane, Springfield
 ;;;;                     Virginia, 22153                               
 ;;;;
 ;;;; Classification: UNCLASSIFIED                             
 ;;;;----------------------------------------------------------------
 ;;;; Distribution Notice:
 ;;;; SBIR DATA RIGHTS Clause
 ;;;; The Government's rights to use, modify, reproduce, release,
 ;;;; perform, display, or disclose technical data or computer 
 ;;;; software marked with this legend are restricted during the 
 ;;;; period shown as provided in paragraph (b)(4) of the Rights in 
 ;;;; Non-commercial Technical Data and Computer Software--Small 
 ;;;; Business Innovative Research (SBIR) Program clause contained in 
 ;;;; the above identified contract.  No restrictions apply after the 
 ;;;; expiration date shown above.  Any reproduction of technical 
 ;;;; data, computer software, or portions thereof marked with this
 ;;;; legend must also reproduce the markings. 
 ;;;; Expiration of SBIR Data Rights Period: 18 May 2016
 ;;;;-----------------------------------------------------------------
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file contains logic created for outputting domains as conditional.



(defun output-conditional-domain ()
    (get-model-information)
    (setq *hidden-types* (mapcar #'predicate-name (get-predicates *last-envmodel*)))
    (output-conditional-action (nth 12 *action-models*))
)


(defun model-all-variables (em)
    (remove-duplicates
        (vars-in
            (append
                (mapcar #'cnd-value
                    (model-conditions em)
                )
                (apply #'append
                    (mapcar #'cnd-args
                        (model-conditions em)
                    )
                )
            )
        )
    )
)

(defun output-conditional-action (am)
    (let* 
      ((args (mapcar #'generate-symbol
                  (mapcar+ #'subseq
                      (mapcar #'symbol-name (event-model-all-variables am))
                      1
                  )
             )
       )
       (arg-map (pairlis (event-model-all-variables am) args))
      )
        (setq *execution-history*
            (list
                (make-occurrence :is-observation t :point 1)
                (make-occurrence
                    :signature (cons (event-model-type am) args)
                    :pre 
                        (remove nil
                            (mapcar #'only
                                (mapcar+ #'bind-condition-to-fact 
                                    (remove-if #'cnd-inequality
                                        (event-model-conditions am) 
                                    )
                                    arg-map
                                    nil
                                )
                            )
                        )
                    :post
                        (remove nil
                            (mapcar #'only
                                (mapcar+ #'bind-condition-to-fact 
                                    (event-model-postconditions am) 
                                    arg-map
                                    nil
                                )
                            )
                        )
                    :point 2
                    :is-action t
                )
                (make-occurrence :is-observation t :point 10)
            )
        )
        (nth-value 4 
            (find-possible-events
                expgen
                (occurrence-pre (second *execution-history*))
                (occurrence-post (second *execution-history*))
                2
                nil
                nil
                0
            )
        )
    )
)

