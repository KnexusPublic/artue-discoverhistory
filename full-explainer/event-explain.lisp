 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: Jun 3, 2009
 ;;;; Project Name: Sandbox
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2009 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file provides the data backend for performing event-based 
 ;;;; explanation.

(in-package :artue)

(defstruct hypothesis
    (parent-hypothesis nil) ; simpler hypothesis forming the foundation of this
                            ; hypothesis
    (event-list nil)        ; list of events that happened according to this
                            ; hypothesis
    (event-removals nil)    ; list of parent hypotheses events that didn't
                            ; happen under this hypothesis
    (hidden-initials nil)   ; list of facts regarding hidden 
                            ; relationships true in the initial state
    (discrepancies nil)     ; list of discrepancies introduced by this 
                            ; hypothesis; should be nil once completely refined
    (instants nil)          ; list of instants that must be hypothesized
    (checked nil)           ; true when a hypothesis has already been found 
                            ; free of errors.
    (branches 0)
)

(defstruct history
    (fact-histories nil) ; list of history objects describing a history for
                         ; all known facts
)

(defstruct fact-history
    (hidden nil)  ; t iff history of a hidden fact
    (changes nil) ; ordered list of <event, value-before, value-after> tuples
                  ; describing changes to a single fact
)

(defstruct fact-change
    (event-time nil)   ; the event that changed the affected fact value
    (value-before nil) ; value of the fact directly before the event occurred
    (value-after nil)  ; value of the fact directly after the event occurred
)

(defstruct happening
    (timepoint nil)     ; the instant at which the cause occurred
    (hypothesized nil)    ; t if current hypothesis includes, nil otherwise
)

(defstruct (cause (:include happening) (:print-function print-cause) (:copier copy-cause-1))
    (name (gensym "C"))
    (action nil)
    (signature nil)
    (preconditions nil)   ; set of facts that are true before
    (postconditions nil)  ; set of facts that are true afterwards
    ; (proximate-cause nil) ; the last cause to precede this one, which meets
                          ; ; all preconditions which were not met prior to its
                          ; ; occurrence
)

(defstruct discrepancy
    (fact 'nada)    ; fact that's discrepant between the start of the instant 
                    ; and the end
    (instant 'nada) ; instant constraining when the discrepancy occurs
    (dependent nil) ; event that is time-dependent on the discrepancy
    (removals nil)  ; a list of events that must be removed to resolve 
                    ; discrepancy
)

(defstruct (observation (:include happening) (:print-function print-observation))
    (name (gensym "O"))
    (perceptions nil)   ; set of facts that are observed
)

(defstruct (instant (:print-function print-instant) (:copier copy-instant-1))
    (name (gensym "T"))
    (causes nil)
    (observation nil)
    (prior-times 'unk)  ; a list of events which happened prior to this event
    (next-times 'unk)   ; a list of events which happened next after this event
                        ; both time properties have a value of 'unk until they
                        ; can be populated. They indicate the closest known
                        ; events in the partial event lattice
    (hypothesized t)    ; t if current hypothesis includes, nil otherwise
    (next-moment 'none) ; instant immediately following, which includes events
                        ; caused in this instant
    (prior-moment 'none); instant immediately prior, which includes the events
                        ; that caused events occurring in this instant
    (final nil)         ; final instants are those that are part of no hypothesis,
                        ; and have immediate relationships to other final instants.
                        ; Non-final (initial) instants may be part of an 
                        ; explanation, but must be finalized to ensure 
                        ; plausibility.
    (fake nil)          ; a kludge which lets discrepancy processing know that 
                        ; the instant is not to be used, due to impossible
                        ; causality
    (link nil)          ; t denotes an instant that takes no time and is used 
                        ; only to associate a final instant with another instant
                        ; temporarily.
)

(defun print-instant (e s k)
    (declare (ignore k))
    (format s "<Instant ~A Prior: ~A Next: ~A>" 
        (instant-name e) 
        (mapcar #'instant-name (instant-prior-times e))
        (mapcar #'instant-name (instant-next-times e))
    )
)

(defun print-cause (e s k)
    (declare (ignore k))
    (format s "<Cause ~A ~A: ~A Time: ~A>" 
        (cause-name e) 
        (if (cause-action e) "Action" "Event")
        (cause-signature e)
        (instant-name (cause-timepoint e))
    )
)

(defun print-observation (e s k)
    (declare (ignore k))
    (format s "<Obs ~A Time: ~A>" 
        (observation-name e) 
        (instant-name (observation-timepoint e))
    )
)

(defun copy-cause (c)
    (setq c (copy-cause-1 c))
    (setf (cause-name c) 
        (intern (concatenate 'string (symbol-name (cause-name c)) "C") :artue)
    )
    c
)

(defun copy-instant (tp &key (derivative t))
    (setq tp (copy-instant-1 tp))
    (when (instant-final tp)
        (break "Copying final instant")
    )
    (if derivative
        (setf (instant-name tp) 
            (intern (concatenate 'string (symbol-name (instant-name tp)) "C") :artue)
        )
        (setf (instant-name tp) 
            (gensym "T")
        )
    )
    tp
)

(defstruct occurrence
    (template nil) ; pddl-event template for event that occurred
    (bindings nil) ; Bindings of the events
    (precs 'unk)   ; Preconditions of the occurrence
)

(defvar *history* nil)
(defvar *hiddens* nil)
(defvar *causables* nil)
(defvar *removables* nil)
(defvar *causable-events* nil)
(defvar *noncausable-events* nil)
(defvar *transients* nil)
(defvar *initial-time* nil)
(defvar *initial-obs* nil)
(defvar *observations* nil)
(defvar *latest-obs* nil)
(defvar *supertypes* (make-hash-table))
(defvar *obj-table* (make-hash-table))
(defvar *pred-types* (make-hash-table))
(defvar *base-hypothesis*)
(defvar *null-hypothesis*)
(defvar *current-hypothesis*)
(defvar *fringe-state* (shop2::make-fun-state nil))
(defvar *depth-limit* nil)
(defvar *cutoff-time* 0)
(defvar *cutoff-seconds* 240)
(defvar *use-time-cutoff* t)
(defvar *max-depth* 7)
(defvar *last-search-was-cutoff* nil)
(defvar *cost-computation-method* :change-count)
(defvar *estimate-best-discrepancy* t)
;(defvar *cost-computation-method* :branch-based)

(defun initialize-explainer ()
    (setq *null-hypothesis* (make-hypothesis))
    (setq *removables* nil)
    (setq *hiddens* (domain-hidden *domain-model*))

    (find-causables)

    (setq *transients* (union *causables* *removables*))
    (maphash 
        #'(lambda (key a)
            (declare (ignore key))
            (setq *transients* 
                (union *transients* 
                    (get-positive-pred-types (shop2::pddl-action-effect a))
                )
            )
            (setq *transients* 
                (union *transients* 
                    (get-negative-pred-types (shop2::pddl-action-effect a))
                )
            )
        )
        (shop2::domain-operators *domain-model*)
    )
    
    (setq *history* (make-history))
    (setq *initial-obs* (make-observation))
    (setf (observation-hypothesized *initial-obs*) t)
    (setq *observations* (list *initial-obs*))
    (setq *initial-time* (get-final-instant :prior-times nil :next-times nil))
    (setf (observation-timepoint *initial-obs*) (make-following-instant *initial-time*))
    (setf (instant-observation (observation-timepoint *initial-obs*)) *initial-obs*)
    (setq *latest-obs* nil)
    (setq *base-hypothesis* (make-hypothesis))
    (setq *current-hypothesis* *base-hypothesis*)
    (clrhash *supertypes*)
    (clrhash *obj-table*)
    (dolist (obj-type (domain-types *domain-model*))
        (setf (gethash obj-type *supertypes*) (shop2::get-supertypes *domain-model* obj-type))
        (setf (gethash obj-type *obj-table*) nil)
    )
    (clrhash *pred-types*)
    (dolist (pred (shop2::domain-predicates *domain-model*))
        (let ((arr (make-array (ceiling (length pred) 3))))
            (setf (gethash (car pred) *pred-types*) arr)
            (loop for i from 1 to (- (array-dimension arr 0) 1) do
;                (break "In init preds loop with i = ~A" i)
                (setf (aref arr i) (nth (* 3 i) pred))
            )
        )
    )
    (reset-trace-level)
)

(defun find-causables (&aux (new-events 'none))
    (setq *noncausable-events* (shop2::domain-events *domain-model*))
    (setq *causable-events* nil)
    (setq *causables* *hiddens*)
    (setq *removables* nil)
    (loop while new-events do
        (setq new-events nil)
        (dolist (ev *noncausable-events*)
            (when (or 
                    (intersection *causables* 
                        (get-positive-pred-types 
                            (shop2::pddl-event-precondition ev)
                        )
                    )
                    (intersection *removables* 
                        (get-negative-pred-types 
                            (shop2::pddl-event-precondition ev)
                        )
                    )
                  )
                (push ev new-events)
            )
        )
        (dolist (ev new-events)
            (setq *causables* 
                (union *causables* 
                    (get-positive-pred-types (shop2::pddl-event-effect ev))
                )
            )
            (setq *removables* 
                (union *removables* 
                    (get-negative-pred-types (shop2::pddl-event-effect ev))
                )
            )
;            (format t "~%~%New event: ~A" ev)
;            (format t "~%Causables: ~A" *causables*)
;            (format t "~%Removables: ~A" *removables*)
        )
        (setq *causable-events* (union *causable-events* new-events))
        (setq *noncausable-events* (set-difference *noncausable-events* new-events))
    )
)

(defun get-predicate-names ()
    (mapcar #'car (shop2::domain-predicates *domain-model*))
)

(defun get-positive-pred-types (effect)
    (case (car effect)
        ((<= < = > >=) nil)
        (and (reduce #'union (mapcar #'get-positive-pred-types (rest effect))))
        ((set decrease increase) nil)
        (not (get-negative-pred-types (second effect)))
        (otherwise (if (member (car effect) (get-predicate-names)) (list (car effect)) nil))
    )
)

(defun get-negative-pred-types (effect)
    (case (car effect)
        ((<= < = > >=) nil)
        (and (reduce #'union (mapcar #'get-negative-pred-types (rest effect))))
        ((set decrease increase) nil)
        (not (get-positive-pred-types (second effect)))
        (otherwise nil)
    )
)

(defun find-plausible-hypotheses (hypo &aux (found-hypos nil))
    (when (and *max-depth* (< *max-depth* 1))
        (return-from find-plausible-hypotheses (explain-discrepancies hypo))
    )
    (setq *depth-limit* 1)
    (setq *cutoff-time* (+ (get-internal-run-time) (* 100 internal-time-units-per-second)))
    (loop while (and (null found-hypos) (<= *depth-limit* *max-depth*)
                     (or (not *use-time-cutoff*)
                         (< (get-internal-run-time) *cutoff-time*)
                     )
                )
        do
;          (break)
          (format t "~%~%~%~A~% Using depth limit ~A. ~%~A~%~%"
            (make-string 80 :initial-element #\=) 
            *depth-limit*
            (make-string 80 :initial-element #\=) 
          )
          (if *use-time-cutoff*
              ;then
              (setq *cutoff-time* 
                  (+ (get-internal-run-time) 
                     (* *cutoff-seconds* internal-time-units-per-second)
                  )
              )
              ;else
              (setq *cutoff-time* -1)
          )
          (setq found-hypos (explain-discrepancies hypo))
          (incf *depth-limit*)
    )
    (setq *last-search-was-cutoff* (< 0 *cutoff-time* (get-internal-run-time)))
    found-hypos
)

(defun was-last-search-cutoff ()
    *last-search-was-cutoff*
)

(defun get-last-search-depth ()
    (- *depth-limit* 1)
)

(defun set-max-search-depth (depth)
    (setq *max-depth* depth)
)

(defun restrict-fact (fact tp &aux hypos (range-set nil))
    (setf (hypothesis-discrepancies *null-hypothesis*)
        (list
            (make-discrepancy
                :fact fact
                :instant tp
            )
        )
    )
    (setq hypos (explain-discrepancies *null-hypothesis*))
    (dolist (new-hypo hypos)
        (set-hypothesis new-hypo)
        (setq range-set (append (constrain-instant tp fact) range-set))
    )
    (combine-ranges range-set tp)
)

(defun combine-ranges (range-set tp &aux cur-tp in-range new-ranges last-start)
    (setq range-set (sort #'instant-ordering range-set))
    (setq cur-tp (car (instant-prior-times tp)))
    (loop while (not (eq cur-tp (car (instant-next-times tp)))) do
        (setq in-range 
            (notevery 
                #'(lambda (range) (time-disjoint cur-tp range)) 
                range-set
            )
        )
        (when (and in-range (null last-start)) 
            (setq last-start (instant-prior-moment cur-tp))
        )
        (when (and (not in-range) last-start)
            (push 
                (get-initial-instant 
                    :prior-time last-start 
                    :next-time (instant-prior-moment cur-tp)
                )
                new-ranges
            )
        )
    )
    (when last-start
        (push 
            (get-initial-instant 
                :prior-time last-start 
                :next-time (car (instant-next-times tp))
            )
            new-ranges
        )
    )
)

(defun prior-time (tp)
    (car (instant-prior-times tp))
)
    
(defun hypothesis-changes (hypo)
       (length (hypothesis-event-list hypo))
    ; (+ (length (hypothesis-event-removals hypo))
       ; (length (hypothesis-event-list hypo))
    ; )
)

;;Added to make API-compatible.
(defun explanation-is-trivial (hypo)
    (null (remove-if #'is-initial-value-event (hypothesis-event-list hypo)))
)

(defun explain-discrepancies (hypo &aux child-hypos removal-discs)
    (when (and (eq *cost-computation-method* :change-count) 
               (> (hypothesis-changes hypo) *depth-limit*)
          )
        (error "Considered hypothesis past depth limit.")
    )
    (when (and (eq (hypothesis-changes hypo) *depth-limit*)
               (eq *cost-computation-method* :change-count)
          )
        (my-trace-end (format nil "depth-limit reached."))
        (return-from explain-discrepancies nil)
    )
    (when (and (eq (hypothesis-branches hypo) *depth-limit*)
               (eq *cost-computation-method* :branch-based)
          )
        (my-trace-end (format nil "depth-limit reached."))
        (return-from explain-discrepancies nil)
    )
    (when (and *use-time-cutoff* (> (get-internal-run-time) *cutoff-time*))
        (my-trace-end (format nil "Time cutoff reached."))
        (return-from explain-discrepancies nil)
    )
            
    (setq removal-discs (remove-if-not #'discrepancy-removals (hypothesis-discrepancies hypo)))
    (if (not (null removal-discs))
        ;then
        (setq child-hypos (explain-using-removed-contradictions removal-discs hypo))
        ;else
        (setq child-hypos (handle-some-discrepancy hypo))
    )
    (apply #'append (mapcar #'refine-hypothesis child-hypos))
)

(defun handle-some-discrepancy (hypo &aux tp-set first-disc)
    (when *estimate-best-discrepancy*
        (setf (hypothesis-discrepancies hypo) 
            (sort 
                (copy-list (hypothesis-discrepancies hypo))
                #'<
                :key #'guess-branch-count 
            )
        )
    )
    (setq tp-set nil)
    (loop while (null tp-set) do
        (setq first-disc (first (hypothesis-discrepancies hypo)))
        (setq tp-set 
            (constrain-instant 
                (discrepancy-instant first-disc)
                (not-ify (discrepancy-fact first-disc))
                :find-all-links nil
            )
        )
        (when (null tp-set)
            (setf (hypothesis-discrepancies hypo) 
                (rest (hypothesis-discrepancies hypo))
            )
            (when (null (hypothesis-discrepancies hypo))
                (refine-hypothesis hypo)
            )
        )
        ;; Think this may occasionally be valid.
        ; (when (null tp-set)
            ; (error "Discrepancy invalid.")
        ; )
    )
    (when (or (cdr tp-set)
              (not (same-time (car tp-set) (discrepancy-instant first-disc)))
          )
        (if (and (discrepancy-dependent first-disc) (listp (discrepancy-dependent first-disc)))
            ;then
            (return-from handle-some-discrepancy 
                (handle-discrepancy-change hypo (car (last tp-set)) first-disc)
            ) 
            ;else
            ;;This is ok, because some discrepancies can be satisfied by multiple times
            ;;as broken up by constrain-instants
            ;;Example: We know sand-covered was nil *before* location-detected, but true
            ;;*before* the next observation. sand-covered can be set to true either 
            ;;at the same time as location-detected, or between the event and observation.
;            (error "Discrepancy changed unexpectedly.")
        )
    )
    (explain-discrepancy 
        first-disc 
        hypo 
    )
)

(defun handle-discrepancy-change (hypo tp disc &aux link-sets (hypo-list nil))
    (setq link-sets 
        (form-instant-link-sets 
            (car 
                (instant-next-times 
                    (discrepancy-instant disc)
                )
            )
            (list (discrepancy-fact disc))
        )
    )
    (when (null (car link-sets))
        (setq disc (copy-discrepancy disc))
        (setf (discrepancy-instant disc) tp)
        (pop (hypothesis-discrepancies hypo))
        (setf (hypothesis-discrepancies hypo) 
            (cons disc (hypothesis-discrepancies hypo) )
        )
        (return-from handle-discrepancy-change 
            (explain-discrepancy disc hypo)
        )
    )
    (unless (null (car link-sets))
        (dolist (link-set link-sets)
            (setq hypo (copy-hypothesis hypo))
            (push-all link-set (hypothesis-instants hypo))
            (push-all (refine-hypothesis hypo) hypo-list)
            (dolist (link link-set)
                (dehypothesize-instant link)
;                (format t "~%Removed ~A in handle-disc-change." (instant-name link))
                (remove-instant link)
            )
        )
        (return-from handle-discrepancy-change hypo-list)
    )
)


(defun same-time (tp1 tp2)
    (or (eq tp1 tp2)
        (and (equal (instant-next-times tp1) (instant-next-times tp2))
             (equal (instant-prior-times tp1) (instant-prior-times tp2))
        )
    )
)

(defun refine-hypothesis (hypo &aux new-discs tp bad-evs relevant-evs old-discs new-hypo ret-list)
    (setq hypo (copy-hypothesis hypo))
    (set-hypothesis hypo)
    (setq old-discs (hypothesis-discrepancies hypo))
    (setq old-discs 
        (append (remove-if-not #'discrepancy-removals old-discs)
                (reverse (remove-if #'discrepancy-removals old-discs))
        )
    )
    (let ((bad-tps (mapcar #'bad-instant (hypothesis-instants hypo))))
        (when (find 'next-time-found-earlier bad-tps :key #'car)
            (error "Cycle has been created.")
        )
        (when (remove nil bad-tps)
            (my-trace-end (format nil "Violation of causality found: ~A" bad-tps))
            (return-from refine-hypothesis nil)
        )
    )
    (when (member 'removed (hypothesis-event-list hypo) :key #'cause-signature)
        (error "Removed event in event list.")
    )
    (dolist (disc old-discs)
        (setq tp (discrepancy-instant disc)) 
        (cond
            ((discrepancy-removals disc) ;; Special handling, keep this one.
                (dolist (rc (discrepancy-removals disc))
                   (setf (cause-hypothesized rc) nil)
                )
                (push disc new-discs)
            )
            ((or (instant-fake tp) (bad-instant tp))
                (setq bad-evs nil)
                (setq relevant-evs
                    (remove-if #'is-observation
                        (mapcar #'first
                            (fact-history-changes 
                                (second (get-fact-info (discrepancy-fact disc)))
                            )
                        )
                    )
                )
                (setq relevant-evs                           
                    (append
                        (remove-if-not #'is-hypothesized relevant-evs)
                        (intersection relevant-evs
                            (append (hypothesis-event-removals hypo)
                                (apply #'append
                                    (mapcar #'discrepancy-removals new-discs)
                                )
                            )
                        )
                    )
                )
                (if (discrepancy-dependent disc)
                    ;then
                    (setq tp (car (instant-prior-times tp)))
                    ;else
                    (setq tp (car (instant-next-times tp)))
                )
                (when (instant-observation tp)
;                    (break "Observation occurs in fake instant.")
                    (return-from refine-hypothesis nil)
                )
                (when (not (discrepancy-dependent disc))
                    (dolist (ev (instant-causes tp))
                        (when (member ev relevant-evs)
                            (push ev bad-evs)
                        )
                    )
                )
                (when (discrepancy-dependent disc)
                    (dolist (ev relevant-evs)
                        (when (or (member ev (instant-causes tp))
                                  (eq tp (get-earlier-event-fact-constraint ev (discrepancy-fact disc)))
                              )
                            (when (not (member (discrepancy-fact disc)
                                               (cause-preconditions ev)
                                               :test #'equal
                                       )
                                  )
                                (push ev bad-evs)
                            )
                        )
                    )
                )
                (when (null bad-evs)
                    (when (null (intersection relevant-evs (instant-causes tp)))
                        ;;Otherwise, there are relevant events that are not
                        ;;hypothesized: success
                        (when (bad-instant (discrepancy-instant disc))
                            (format t "~%Not sure this is a good idea, but WTH.")
                            (break "Need for check on bad-instant circumstances in refine-hypo") 
                            (return-from refine-hypothesis nil)
                        )
                        (error "Faked instant necessitates an event removal, but none can be found to remove.")
                    )
                )
                ;;Must remove non-hypothesized events already partially removed 
                ;;by this function.
                (setq bad-evs (remove-if-not #'is-hypothesized bad-evs))
                (when bad-evs
                    (dolist (ev bad-evs)
                        (when (cause-action ev)
;                            (break "Action occurs in fake instant.")
                            (my-trace-end (format nil "Tried to remove action ~A" ev))
                            (return-from refine-hypothesis nil)
                        )
                        (setf (cause-hypothesized ev) nil)
                    )
                    (push 
                        (make-discrepancy 
                            :fact (discrepancy-fact disc) 
                            :instant tp 
                            :removals bad-evs
                        )
                        new-discs
                    )
                )
;                (break "Fake instant handling.")
            )
            ((and (not (discrepancy-fixed disc)) (not (discrepancy-irrelevant disc)))
                (push disc new-discs)
            )
;            ((not (discrepancy-fixed disc))
;                (break "Check on irrelevancy.")
;            )
        )
    )
    (setf (hypothesis-discrepancies hypo) new-discs)
    (when (and (null (hypothesis-discrepancies hypo)) (not (hypothesis-checked hypo)))
        (setq new-hypo (check-causality hypo))
        (when (null new-hypo)
            (my-trace-end "Inconsistent causality in hypothesis.")
;            (break "Inconsistent causality.")
            (return-from refine-hypothesis nil)
        )
        (setq new-hypo (add-implied-events new-hypo))
        (when (null new-hypo)
            (my-trace-end "Add-implied returns nil.")
            (break "Why does add-implied-events return nil?")
            (clean-up-hypo new-hypo hypo)
            (return-from refine-hypothesis nil)
        )
;        (break "Performed add-implied-events.")
        (when (and (> (hypothesis-changes new-hypo) *depth-limit*)
                   (eq *cost-computation-method* :change-count)
              )
          (my-trace-end "depth-limit reached after add-implied.")
          (clean-up-hypo new-hypo hypo)
          (return-from refine-hypothesis nil)
        )
        (when (hypothesis-discrepancies new-hypo)
            (setq ret-list (refine-hypothesis new-hypo))
            (clean-up-hypo new-hypo hypo)
            (return-from refine-hypothesis ret-list)
        )
        (setq hypo new-hypo)
    )
    (if (hypothesis-discrepancies hypo)
        ;then return
        (explain-discrepancies hypo)
        ;else return
        (progn
            (when (not (hypothesis-checked hypo))
                (check-hypothesis hypo)
            )
            (list hypo)
        )
    )
)

(defun adopt-hypothesis (hypo &aux (discs nil) (ret-discs nil))
    (set-hypothesis (hypothesis-parent-hypothesis hypo))
    (dolist (ev (hypothesis-event-removals hypo))
        (setf (cause-hypothesized ev) nil)
    )
    (dolist (ev (hypothesis-event-list hypo))
        (when (not (is-initial-value-event ev))
            (dolist (f (cause-postconditions ev))
                (push-all (history-insert-postcondition ev f) discs)
            )
            (dolist (f (cause-preconditions ev))
                (push-all (history-insert-precondition ev f) discs)
            )
        )
    )
    (set-hypothesis hypo)
    (dolist (disc discs)
        (when (and (not (discrepancy-fixed disc)) (not (discrepancy-irrelevant disc)))
            (push disc ret-discs)
        )
    )
    ret-discs
)


(defun check-hypothesis (hypo &aux fhist removes)
    (dolist (ev (hypothesis-event-list hypo))
        (when (not (is-initial-value-event ev))
            (dolist (f (append (cause-preconditions ev) (cause-postconditions ev)))
                (setq fhist (second (get-fact-info f)))
                (setf (fact-history-changes fhist) 
                    (remove ev (fact-history-changes fhist) :key #'first)
                )
            )
        )
    )
    (setq removes (remove-if-not #'is-removal-event (hypothesis-event-list hypo)))
    (setf (hypothesis-event-list hypo) (set-difference (hypothesis-event-list hypo) removes))
    (mapcar #'remove-event removes)
    (setf (hypothesis-checked hypo) t)
)

(defun is-removal-event (ev)
    (eq 'removal (car (cause-signature ev)))
)

(defun clean-up-hypo (new-hypo old-hypo)
    (dolist (c (set-difference (hypothesis-event-list new-hypo) (hypothesis-event-list old-hypo)))
;        (format t "~%Removed event ~A in clean-up-hypo." c)
        (remove-event c)
    )
    (dolist (tp (set-difference (hypothesis-instants new-hypo) (hypothesis-instants old-hypo)))
;        (format t "~%Removed ~A in clean-up-hypos." (instant-name tp))
        (remove-instant tp)
    )
)    

(defun instants-ordered (tp1 tp2)
    (or (eq tp1 tp2)
        (instant-earlier-than tp1 tp2)
        (instant-earlier-than tp2 tp1)
    )
)

(defun happening-ordering (h1 h2)
    (cond 
        ((not (hap-orderable h1)) (hap-orderable h2))
        ((not (hap-orderable h2)) nil)
        (t (instant-ordering (happening-timepoint h1) (happening-timepoint h2)))
    )
)

(defun hap-orderable (hap)
    (or (is-hypothesized hap) (instant-final (happening-timepoint hap)))
)

(defun instant-ordering (tp1 tp2)
    (cond
        ((not (instant-hypothesized tp1)) (instant-hypothesized tp2))
        ((not (instant-hypothesized tp2)) nil)
        ((eq tp1 tp2) nil)
        ((instant-later-than tp1 tp2) t)
        ((instant-later-than tp2 tp1) nil)
        ((null (set-exclusive-or (instant-prior-times tp1) (instant-prior-times tp2))) nil)
        ((member tp1 (apply #'append (mapcar #'list-later-instants (instant-prior-times tp2)))) t)
        (t nil)
    )
) 

(defun history-safe (hist &aux changes tp1 tp2)
    (setq changes (show-changes (fact-history-changes hist)))
    (setq tp2 (change-time (car changes)))
    (loop while (rest changes) do
        (setq tp1 tp2)
        (setq changes (rest changes))
        (setq tp2 (change-time (car changes)))
        (when (and (not (eq tp1 tp2)) (not (instant-later-than tp2 tp1)))
            (return-from history-safe nil)
        )
    )
    t
)

(defun instant-later-than (tp1 tp2)
    (cond
        ((eq tp1 tp2) nil)
        ((instant-hypothesized tp2) (member tp1 (list-later-instants tp2)))
        ((instant-hypothesized tp1) (member tp2 (list-earlier-instants tp1)))
       (t nil)
    )
)

(defun instant-earlier-than (tp1 tp2)
    (cond
        ((eq tp1 tp2) nil)
        ((instant-hypothesized tp1) (member tp2 (list-later-instants tp1)))
        ((instant-hypothesized tp2) (member tp1 (list-earlier-instants tp2)))
        (t nil)
    )
)

(defun cause-earlier-than (cause1 cause2)
    (member cause2 (list-later-happenings (cause-timepoint cause1)))
)

(defun is-initial-value-event (c)
    (eq (car (cause-signature c)) 'initially-true)    
)

(defun is-observation (hap)
    (typep hap 'observation)
)

(defun check-causality (hypo &aux new-cause links cause-list link-list)
    (set-hypothesis hypo)
    (setq hypo (copy-hypothesis hypo))
    (dolist (c (sort (copy-list (hypothesis-event-list hypo)) #'cause-earlier-than))
        (setq links (check-cause c))
        (when (null links)
            (dolist (bad-cause cause-list)
                (when (and (not (is-initial-value-event bad-cause)) 
                           (not (eq (car (cause-signature bad-cause)) 'removal))
                      )
                    ;;only initial value events are permanent
;                    (format t "~%Removed event ~A in check-causality." bad-cause)
                    (remove-event bad-cause)
                )
            )
            (dolist (tp link-list)
                (remove-instant tp)
            )
            (return-from check-causality nil)
        )
        (setq new-cause (car links))
        (setq links (cdr links))
        (setf (cause-hypothesized c) nil)
        (setf (cause-hypothesized new-cause) t)
        (push new-cause cause-list)
        (push-all links link-list)
    )
    ;; All links should become unnecessary as soon as last cause is converted.
    (dolist (tp link-list)
        (remove-instant tp)
    )
    (dolist (tp (hypothesis-instants hypo))
        (dehypothesize-instant tp)
    )
    (setf (hypothesis-instants hypo) nil)
    (setf (hypothesis-event-list hypo) cause-list)
;    (break "Causality checked.")
    (set-hypothesis hypo)
    hypo
)

(defun check-cause (c &aux finfo (causes nil))
    (when (null (cause-preconditions c))
        (when (is-initial-value-event c)
            (return-from check-cause (list c))
        )
        (error "Events cannot have 0 preconditions.")
    )
    (when (eql (car (cause-signature c)) 'removal)
        (return-from check-cause (list (copy-cause c)))
    )
    (dolist (prec (cause-preconditions c))
        (setq finfo (get-fact-info prec))
        (let ((hist (second finfo)) (val (third finfo)) change)
            (setq change (get-prior-switch c hist))
            (when 
                (not 
                    (or (and (eq (third change) 'none) (eql (second change) val))
                        (eql (third change) val)
                    )
                )
                ;;Event has somehow become impossible.
                (format t "~%Failure in check-cause; event ~A requires ~A." (cause-signature c) prec)
                (format t "~%History of ~A:" prec)
                (qp (show-changes hist))
                (return-from check-cause nil)
            )
;            (break "~%Causes: ~A" causes)
            (when
                (null
                    (intersection
                        causes
                        (list-later-instants (happening-timepoint (car change)))
                    )
                )
                    (setq causes
                        (cons 
                            (happening-timepoint (car change))
                            (set-difference causes 
                                (list-earlier-instants (happening-timepoint (car change)))
                            )
                        )
                    )
            )
        )
    )
;    (break "~%Final Causes: ~A" causes)
    (when (or (null causes) (not (null (cdr causes))))
        (error "All events must have a single cause time.")
    )
    (let 
      ((tp (instant-next-moment (first causes))) 
       (old-tp (cause-timepoint c))
       (true-cause (first causes))
       (new-event nil)
       (discs nil)
       (links nil)
       (new-tp nil)
      )
        ; (break "changing the instant")
        (when (instant-observation true-cause)
            (if (eq (instant-observation true-cause) *initial-obs*)
                ;then
                ;;The initial observation does not serve as a cause
                (progn
	            (my-trace-end (format nil "Event ~A initially caused." c)) 
                    (return-from check-cause nil)
                )
                ;else
                (error "Should not find an observation as a cause.")
            )
        )
        (setq new-event (copy-cause c))
        (cond
            ((eq tp old-tp)
                ;; The moments are already all set up. Could be a predicted 
                ;; event.
                (push new-event (instant-causes tp))
            )
            ((eq tp 'none)
                (progn
                    (when (instant-final old-tp)
                        (my-trace-end (format nil "Event ~A actual time ~A not at ~A." new-event new-tp old-tp)) 
                        (return-from check-cause nil)
                    )
                    (setq new-tp (copy-instant old-tp))
                    (setf (instant-final new-tp) t)
                    (setf (cause-timepoint new-event) new-tp)
                    (setf (instant-hypothesized new-tp) t)
                    (setf (instant-next-moment true-cause) new-tp
                          (instant-prior-moment new-tp) true-cause
                    )
                    (when (find (list 'mysterious-forces) (instant-causes true-cause) 
                            :key #'cause-signature :test #'equal
                          )
                        (break "Found mysterious-forces.")
                    )
                    (pushnew new-tp (instant-next-times true-cause))
                    (setf (instant-prior-times new-tp) (list true-cause))
                    (setf (instant-next-times new-tp) nil) 
                    (dolist (other-tp (remove-if-not #'instant-observation (instant-next-times true-cause)))
                        (link-instants new-tp other-tp)
                    )
                    (setf (instant-causes new-tp) (list new-event))
                    (when (not (in-interval new-tp old-tp))
;                        (break "Not in interval in check-cause. (No prior)")
                        (mapcan #'remove-instant links)
                        (my-trace-end (format nil "Event ~A actual time ~A not in interval ~A." new-event new-tp old-tp)) 
;                        (break "Event ~A actual time ~A not in interval ~A." new-event new-tp old-tp)
                        (return-from check-cause nil)
                    )
                    (when (instant-final old-tp)
                        (break "Needs explanation.")
                    )
                    (dolist (other-tp (instant-prior-times old-tp))
                        (when (eq new-tp other-tp)
                            (break "Causality loop starting.")
                        )
                        (when (not (instant-final other-tp))
                            (push (make-instant-link other-tp new-tp) links) 
                        )
;                        (pushnew ntp (instant-next-times otp))
                    )
                    (dolist (other-tp (instant-next-times old-tp))
                        (when (eq new-tp other-tp)
                            (break "Causality loop starting.")
                        )
                        (when (member other-tp (list-earlier-instants new-tp))
                            (break "Causality loop imminent.")
                            (mapcan #'remove-event links)
                            (return-from check-cause nil)
                        )
                        (when (not (instant-final other-tp))
                            (push (make-instant-link new-tp other-tp) links) 
                        )
;                        (pushnew ntp (instant-prior-times otp))
                    )
                    (when (remove new-tp (instant-causes new-tp) :key #'cause-timepoint)
                        (break "Wrong instant.")
                    )
                    (format t "~%~%New final instant in check-cause ~A" new-tp)
                )
            )
            (t
                (progn
                    (setq new-tp tp)
                    (when (not (in-interval new-tp old-tp))
;                        (break "Not in interval in check-cause.")
                        (my-trace-end (format nil "Event ~A actual time ~A not in interval ~A." new-event new-tp old-tp)) 
;                        (break "Event ~A actual time ~A not in interval ~A." new-event new-tp old-tp)
                        (return-from check-cause nil)
                    )
                    (setf (cause-timepoint new-event) new-tp
                          (instant-causes new-tp) (adjoin new-event (instant-causes new-tp))
                    )
                    (dolist (other-tp (instant-prior-times old-tp))
                        (when (not (instant-final other-tp))
                            (push (make-instant-link other-tp new-tp) links) 
;                            (pushnew tp  (instant-next-times  otp))
;                            (pushnew otp (instant-prior-times tp))
                        )
                    )
                    (dolist (other-tp (instant-next-times old-tp))
                        (when (not (instant-final other-tp))
                            (when (member other-tp (list-earlier-instants new-tp))
                                (break "Causality loop imminent.")
                                (mapcan #'remove-event links)
                                (return-from check-cause nil)
                            )
                            (push (make-instant-link new-tp other-tp) links) 
;                            (pushnew tp  (instant-prior-times otp))
;                            (pushnew otp (instant-next-times  tp))
                        )
                    )
                    ; (when (not (eq tp (cause-timepoint new-event)))
                        ; (break "Inconsistent.")
                    ; )
                )
            )
        )
        (setf (cause-hypothesized c) nil)
        (dolist (eff (cause-postconditions new-event))
            (push-all (history-insert-postcondition new-event eff) discs)
        )
        (dolist (prec (cause-preconditions new-event))
            (push-all (history-insert-precondition new-event prec) discs)
        )
        (when discs
;            (break "Recapitulation should not cause error.")
;            (format t "~%Removed event ~A created in check-cause." new-event)
            (remove-event new-event)
            (setf (cause-hypothesized c) t)
            (return-from check-cause nil)
        )
        (cons new-event links)
    )
)

(defun add-implied-events (hypo &aux cur-differences cur-events state 
                                     cur-sig hypos next-time cur-instant
                                     next-evs orig-evs ev)
    (setq hypo (copy-hypothesis hypo))
    (setq cur-instant *initial-time*)
    (format t "~%Adding events to checked hypothesis: ~%")
    (print-one-per-line (hypothesis-event-list hypo))
    (loop while (and (not (null cur-instant)) (null (hypothesis-discrepancies hypo))) do
        (setq cur-events 
            (remove cur-instant (hypothesis-event-list hypo) 
                :key #'cause-timepoint :test-not #'eq
            )
        )
;        (format t "~%Current instant: ~A" cur-instant)
;        (if (instant-observation cur-instant)
            ;then
;            (format t " (Observation)")
            ;else
;            (dolist (ev (instant-causes cur-instant))
;                (when (cause-hypothesized ev)
;                    (format t "~%Current event: ~A" ev)
;                )
;            )
;        )
        (dolist (ev cur-events)
            (format t "~%New Event: ~A" (cause-signature ev))
            (dolist (eff (cause-postconditions ev))
;                (format t "~%Effect: ~A" eff)
                (let* ((finfo (get-fact-info eff))
                       (new-val (third finfo)) 
                       (hist (second finfo)) 
                       (ch nil)
                       (old-val 'nada) 
                      )
                    (setq ch (find ev (fact-history-changes hist)))
                    (when (not (eq (second ch) 'unk))
                        (setq old-val (second ch))
                    )
                    (when (eq old-val 'nada)
                        (setq ch (first (search-back-for-event ev hist :exclusive t)))
                        (if (eq (third ch) 'none)
                            ;then
                            (setq old-val (second ch))
                            ;else
                            (setq old-val (third ch))
                        )
                    )
                    (when (not (eq old-val new-val))
                        (setq ch (search-forward-for-event ev hist :exclusive t))
                        (setq cur-differences 
                            (remove (not-ify eff) cur-differences 
                                :key #'first :test #'equal
                            )
                        )
                        (if (null ch)
                            ;then
                            (push 
                                (list eff (observation-timepoint *latest-obs*)) 
                                cur-differences
                            )
                            ;else
                            (push 
                                (list eff (happening-timepoint (caar ch))) 
                                cur-differences
                            )
                        )
                    )
                )
            )
        )
        (setq cur-differences (remove cur-instant cur-differences :key #'second))
;        (break "In add-implied-events.")
        (when (and (not (eq cur-instant *initial-time*)) 
                   (not (eq cur-instant (happening-timepoint *initial-obs*)))
              )
            (setq next-time (instant-next-moment cur-instant))
            (setq next-evs nil)
            (setq orig-evs nil)
            (when (not (eq next-time 'none))
                (when (instant-observation next-time)
                    (error "Observation should not be a next moment.")
                )
                (setq orig-evs
                    (remove 'mysterious-forces
                        (remove 'removal
                            (mapcar #'cause-signature
                                (remove next-time (get-all-events hypo) 
                                        :key #'cause-timepoint :test-not #'eq
                                )
                            )
                            :key #'car    
                        )
                        :key #'car    
                    )
                )
                (setq next-evs orig-evs)
            )
;            (when cur-differences
                (setq state (collect-instant-state cur-instant))
;            )
;            (dolist (diff (mapcar #'first cur-differences))
;                (dolist (occ (get-affected-occurrences diff state))
                (dolist (occ (predict-next-occurrence-set state))
                    (setq cur-sig (get-occ-sig occ))
                    (setq orig-evs (remove cur-sig orig-evs :test #'equal))
                    (unless (member cur-sig next-evs :test #'equal)
                        (format t "~%Adding event: ~A." cur-sig)
                        (push cur-sig next-evs)
                        (when (eq next-time 'none)
                            (setq next-time (make-following-instant cur-instant))
                            (setf (instant-next-times next-time) 
                                (remove next-time
                                    (instant-next-times cur-instant)
                                )
                            )
                            (when (member next-time (instant-next-times next-time))
                                (error "causality loop")
                            )
                            (setf (instant-next-times cur-instant) (list next-time))
                            (when (member cur-instant (instant-next-times cur-instant))
                                (error "causality loop")
                            )
                            (dolist (changed (instant-next-times next-time))
                                (setf (instant-prior-times changed)
                                    (adjoin next-time
                                        (remove cur-instant
                                            (instant-prior-times changed)
                                        )
                                    )
                                )
                            )
                            (format t "~%~%Following instant created in add-implied-events. ~A" next-time) 
                        )
                        (setq hypos (make-added-hypotheses hypo next-time occ (occurrence-precs occ)))
;;                        (break "In add-implied-events after an event is added.")
                        (when (cdr hypos)
                            (error "More than one possible hypothesis encountered in add-implied-events.")
                        )
                        (when (and (null hypos) (null (hypothesis-discrepancies hypo)))
                            (error "No hypotheses.")
                        )
                        (when hypos
                            (setq hypo (car hypos))
                            (set-hypothesis hypo)
                        )
                    )
                )
;            )
            (when orig-evs
;                (break "Before removal in add-implied-events.")
                (dolist (cur-sig orig-evs)
                    (format t "~%Removing Event: ~A" cur-sig)
                    (break "What's going on here?")
                    (setq ev 
                        (find cur-sig (instant-causes next-time) 
                            :key #'cause-signature
                        )
                    )
                    (when (null ev) (error "Can't find signature."))
                    (setf (cause-hypothesized ev) nil)
                    (push
                        (make-discrepancy
                            :fact nil
                            :instant next-time
                            :removals (list ev)
                        )
                        (hypothesis-discrepancies hypo) 
                    )
                )
;                (break "After removal in add-implied-events.")
            )
        )
        (if (eq (instant-next-moment cur-instant) 'none)
            ;then
            (let ((nexts (remove-if-not #'instant-hypothesized (instant-next-times cur-instant))))
                (when (null nexts)
                    (if (eq (observation-timepoint *latest-obs*) cur-instant)
                        ;then
                        (setq cur-instant nil)
                        ;else
                        (error "Shouldn't have a dead end except at most recent observation.")
                    )
                )
                (when (cdr nexts)
                    (error "Should not have an ambiguity about what instant comes next.")
                )
                (when (not (null cur-instant))
                    (setq cur-instant (car nexts))
                    (when (not (instant-observation cur-instant))
                        (error "Moments should only be unconnected before an observation.")
                    )
                )
            )
            ;else
            (setq cur-instant (instant-next-moment cur-instant))
        )
    )
    ; (when (not (null (hypothesis-discrepancies hypo)))
        ; (format t "~%New discrepancy in add-implied-events : ~%~A"
            ; (hypothesis-discrepancies hypo)
        ; )
    ; )
    hypo
)

(defun get-assumptions (hypo)
    (mapcar #'second
        (remove 'initially-true 
            (mapcar #'cause-signature (get-all-events hypo))
            :key #'car
            :test-not #'eq
        )
    )
)

(defun get-all-events (hypo)
    (if (null hypo)
        ;then
        nil
        ;else
        (append
            (set-difference 
                (get-all-events (hypothesis-parent-hypothesis hypo))
                (hypothesis-event-removals hypo)
            )
            (hypothesis-event-list hypo)
        )
    )
)

(defun make-following-instant (last-instant &aux new-instant)
    (setq new-instant (get-final-instant :prior-times nil :next-times nil))
    (setf (instant-prior-moment new-instant) last-instant)
    (setf (instant-next-moment last-instant) new-instant)
    (link-instants last-instant new-instant)
    new-instant
)

(defun sort-history (fhist &aux changes)
    (declare (ignore fhist changes))
    ; (setq changes 
        ; (stable-sort 
            ; (copy-list (fact-history-changes fhist)) 
            ; #'happening-ordering :key #'car
        ; )
    ; )
    ; (when (not (equal (show-changes changes) (show-changes (fact-history-changes fhist))))
        ; (format t "~%~%Original:")
        ; (print-one-per-line (show-changes (fact-history-changes fhist)))
        ; (format t "~%~%New:")
        ; (print-one-per-line (show-changes changes))
        ; (break "In sort-history.")
        ; (when (history-safe fhist)
            ; (error "Should not be re-ordered.")
        ; )
        ; (setf (fact-history-changes fhist) changes)
        ; (when (not (history-safe fhist))
            ; (error "History should be correct afterwards.")
        ; )
    ; )
)

(defun resort-history (fhist &aux changes)
    (setq changes 
        (stable-sort 
            (copy-list (fact-history-changes fhist)) 
            #'happening-ordering :key #'car
        )
    )
    (when (not (equal (show-changes changes) (show-changes (fact-history-changes fhist))))
        (format t "~%~%Original:")
        (print-one-per-line (show-changes (fact-history-changes fhist)))
        (format t "~%~%New:")
        (print-one-per-line (show-changes changes))
        (break "In sort-history.")
        (when (history-safe fhist)
            (error "Should not be re-ordered.")
        )
        (when (not (history-safe fhist))
            (error "History should be correct afterwards.")
        )
    )
    (setf (fact-history-changes fhist) changes)
)

;;This is used to make temporary instant links, which go away when no longer
;;used, so as not to pollute the relationships of final instants.
(defun make-instant-link (before after &key (hypo nil) (hypothesized t)) 
    (let ((tp (make-instant 
                    :prior-times (list before) 
                    :next-times (list after)
                    :hypothesized hypothesized       
                    :link t
              )
          )
          new-tp old-tp
         )
        ; (when (bad-instant tp)
            ; (error "Bad link instant.")
        ; )
        (link-instants before tp)
        (link-instants tp after)
        (when (eq (instant-final before) (instant-final after))
            (error "Both instants on link shouldn't be final.")
        )
        
        ;;This code switches causes over to a final instant when
        ;;the new link whittles an initial instant down to a single
        ;;final instant.
        (setq new-tp (should-be-final before))
        (setq old-tp after)
        (if (null new-tp)
            ;then
            (setq new-tp (should-be-final after))
            ;else
            (setq old-tp before)
        )
        (when new-tp 
            (when (null hypo)
                (error "No hypothesis available for use in make-instant-link.")
            )
;            (break "Changing instant to final.")
            (remove-instant tp)
            (substitute-timepoint hypo old-tp new-tp)
            ; (format t "~%~%Instant to final: ~A -> ~A~%~%" 
                ; (instant-name old-tp) (instant-name new-tp)
            ; )
            (return-from make-instant-link nil)
        )

        
        (dolist (l (intersection (get-relevant-literals before) 
                                 (get-relevant-literals after)
                                  :test #'equal
                   )
                )
            (let ((fhist (find-fact-history l)))
                (sort-history fhist)
            )
        )
        tp
    )
)

(defun get-relevant-literals (tp &aux (literal-list nil) (conds nil))
    (dolist (c (instant-causes tp))
        (setq conds (cause-postconditions c))
        (when (not (cause-action c))
            (setq conds (append (cause-preconditions c)))
        )
        (dolist (l conds)
            (if (eq (first l) 'not)
                ;then
                (pushnew (second l) literal-list :test #'equal)
                ;else
                (pushnew l literal-list :test #'equal)
            )
        )
    )
    literal-list
)

(defun link-instants (before after)
    (setf (instant-prior-times after) 
        (adjoin before (instant-prior-times after))
    )
    (setf (instant-next-times before) 
        (adjoin after (instant-next-times before))
    )
    (when (member before (instant-next-times before))
        (error "causality loop")
    )
)
            


(defun get-affected-occurrences (changed-fact state &aux occs bindings mod-conj)
    (dolist (template (shop2::domain-events *domain-model*))
        (dolist (conjunct (cnf (precondition-of template)))
            (dolist (prec conjunct)
                (setq bindings (shop2::unify prec changed-fact))
                (when (not (eq bindings (shop-fail)))
                    (setq mod-conj 
                        (cons 'and
                            (apply-substitution 
                                (remove prec conjunct)
                                bindings
                            )
                        )
                    )
                    (dolist (blist (shopthpr:find-satisfiers mod-conj state t))
                        (push 
                            (make-occurrence 
                                :template template 
                                :bindings (append bindings blist)
                                :precs 
                                    (apply-substitution 
                                        conjunct
                                        (append bindings blist)
                                    )
                            )
                            occs
                        )
                    )
                )
            )
        )
    )
    occs
)

(defun in-interval (timepoint timerange)
    (when (member timepoint (list-earlier-instants timerange :skip-first t))
        (return-from in-interval nil)
    )
    (dolist (tp (instant-next-times timerange))
        (if (instant-final tp)
            ;then
            (when (member timepoint (list-later-instants tp :skip-first t))
                (return-from in-interval nil)
            )
            ;else
            (when (member timepoint (list-later-instants tp))
                (return-from in-interval nil)
            )
        )
    )
    t
)

(defun time-disjoint (inner-time time-range)
    (not
        (and
            (subsetp 
                (instant-prior-times time-range) 
                (list-earlier-instants inner-time)
            )
            (subsetp 
                (instant-next-times time-range) 
                (list-later-instants inner-time)
            )
        )
    )
)

(defun get-prior-switch (ev fhist &aux change-ptr val (last-change nil))
    (setq change-ptr (search-back-for-event ev fhist))
    (loop while (eq (happening-timepoint ev) (change-time (car change-ptr))) do
        (setq change-ptr (rest change-ptr))
    )
    (setq val (get-value-after-changes change-ptr))
    (loop while (not (null change-ptr)) do
        (when (is-hypothesized (caar change-ptr))
            (setq last-change (first change-ptr))
            (when (not (eq (get-value-after-changes (cdr change-ptr)) val))
                (return-from get-prior-switch last-change)
            )
        )
        (setq change-ptr (cdr change-ptr))
    )
    (when (and (null change-ptr) (null last-change))
        (when (not (fact-history-hidden fhist))
            (error "Causality violated.")
        )
        (return-from get-prior-switch (list *initial-obs* nil nil))
    )
;;;    (when
;;;        (and (null change-ptr)
;;;             (not (eq (car last-change) *initial-obs*)) 
;;;             (not (fact-history-hidden fhist))
;;;        )
;;;        (error "Causality violated.")
;;;    )
    last-change
)

(defun explain-discrepancy (disc hypo)
    (let ((hypo-set (explain-using-added-events disc hypo)))
        (push-all (explain-using-removed-events disc hypo)
            hypo-set
        )
        (when (and (is-hidden (discrepancy-fact disc))
                   (null (instant-prior-times (discrepancy-instant disc)))
              )
            (push-all (explain-using-hidden-initials disc hypo)
                hypo-set
            )
        )
        hypo-set
    )
)



(defun explain-using-hidden-initials (disc hypo &aux c disc-fact ret)
    (setq disc-fact (discrepancy-fact disc))
    (setq hypo (copy-hypothesis hypo))
    (setq c (first (first (last (fact-history-changes (find-fact-history disc-fact))))))
    (when (typep c 'observation)
        ;;This happens when a hidden fact is known in the initial state.
        (return-from explain-using-hidden-initials nil)
    )
    (push c (hypothesis-event-list hypo))
    (when (not (equal disc-fact (discrepancy-fact (pop (hypothesis-discrepancies hypo)))))
        (error "Should be true by construction.")
    )
    (my-trace-in (format nil "hidden-initial ~A" disc-fact))
    (setq ret (refine-hypothesis hypo))
    (my-trace-out (format nil "hidden-initial ~A" disc-fact))
    ret
)

(defun form-hidden-initial-cause (fact hist &aux c)
    (setq c
        (make-cause
            :action nil
            :signature (list 'initially-true fact)
            :preconditions nil
            :postconditions (list fact)
            :hypothesized nil
            :timepoint *initial-time*
        )
    )
    (setf (fact-history-changes hist) (list (list c 'unk t)))
    (push c (instant-causes *initial-time*))
)

(defun set-hypothesis (hypo)
    (when (eq hypo *current-hypothesis*)
        (return-from set-hypothesis hypo)
    )
    (disable-hypothesis *current-hypothesis*)
    (enable-hypothesis hypo nil)
    (setq *current-hypothesis* hypo)
;    (setq *fringe-state* (collect-state-from-history))
)

(defun collect-discrepancy-state (tp &aux state old-state)
    (setq state (shop2::make-fun-state nil))
    (setq old-state *current-state*)
    (setq *current-state* state)
    (dolist (entry (history-fact-histories *history*))
        (let ((fact (first entry)) (hist (second entry))) 
            (when (not (shop2::is-funcall fact))
                (when (is-true-during tp hist)
                    (shop2::add-atom-to-state fact state 0 tp)
                )
            )
        )
    )
    (setq *current-state* old-state)
    state
)

(defun is-true-during (tp hist)
    (let (prior-event-ptr next-event-ptr)
        (when (not (null (instant-prior-times tp)))
            (setq prior-event-ptr (search-back-for-event tp hist :exclusive t))
        )
        (setq next-event-ptr (search-forward-for-event tp hist :exclusive t))
        (when (null next-event-ptr)
            (setq next-event-ptr (cons nil (fact-history-changes hist)))
        )
        (when (not (tailp prior-event-ptr next-event-ptr))
            (error "Violated basic assumption of history.")
        )
        (loop while (not (eq next-event-ptr prior-event-ptr)) do
            (setq next-event-ptr (rest next-event-ptr))
            (when (and next-event-ptr
                       (is-hypothesized (caar next-event-ptr)) 
                       (get-value-after-changes next-event-ptr))
                (return-from is-true-during t)
            )
        )
    )
    nil
)
    
(defun collect-instant-state (tp &aux state old-state)
    (setq state (shop2::make-fun-state nil))
    (setq old-state *current-state*)
    (setq *current-state* state)
    (dolist (f (history-fact-histories *history*))
        (let*
          ((fact (first f)) (fhist (second f)) val)
            (setq val (get-value-at-time fhist tp))
            (if (shop2::is-funcall fact)
                ;then
                (eval `(setf ,fact ',val))
                ;else
                (when val
                    (shop2::add-atom-to-state fact state 0 tp)
                )
            )
        )
    )
    (setq *current-state* old-state)
    state
)

(defun collect-state-from-history (&aux old-state state)
    (setq state (shop2::make-fun-state nil))
    (setq old-state *current-state*)
    (setq *current-state* state)
    (dolist (f (history-fact-histories *history*))
        (let*
          ((fact (first f)) (fhist (second f)) 
           (changes (find-most-recent-changes fhist))
          )
            (if (shop2::is-funcall fact)
                ;then
                (eval `(setf ,fact ',(get-value-after-changes changes)))
                ;else
                (when (get-value-after-changes changes)
                    (shop2::add-atom-to-state fact state 0 (caar changes))
                )
            )
        )
    )
    (setq *current-state* old-state)
    state
)
    

(defun enable-hypothesis (hypo removed-events)
    (dolist (ev (hypothesis-event-list hypo))
        (when (not (member ev removed-events))
;            (when (and (not (instant-final (cause-timepoint ev))) (not (cause-hypothesized ev)))
;                (break "Non-final event de-hypo'ed.")
;            )
            (setf (cause-hypothesized ev) t)
        )
    )
    (dolist (tp (hypothesis-instants hypo))
        (setf (instant-hypothesized tp) t)
    )
    (when (not (null (hypothesis-parent-hypothesis hypo)))
        (enable-hypothesis 
            (hypothesis-parent-hypothesis hypo) 
            (append removed-events (hypothesis-event-removals hypo))
        )
    )
)

(defun disable-hypothesis (hypo)
    (dolist (ev (hypothesis-event-list hypo))
;        (when (and (not (instant-final (cause-timepoint ev))) (cause-hypothesized ev))
;            (break "Non-final event de-hypo'ed.")
;        )
        (setf (cause-hypothesized ev) nil)
    )
    (dolist (tp (hypothesis-instants hypo))
        (dehypothesize-instant tp)
    )
    (when (not (null (hypothesis-parent-hypothesis hypo)))
        (disable-hypothesis (hypothesis-parent-hypothesis hypo))
    )
)


(defun explain-using-removed-events (disc hypo)
    (append
        (explain-using-removed-predecessor disc hypo)
        (explain-using-removed-successor disc hypo)
    )
)

(defun explain-using-removed-predecessor (disc hypo)
    (set-hypothesis hypo)
    (let*
      ((disc-fact (discrepancy-fact disc))
       (possible-instant (discrepancy-instant disc))
       (finfo (get-fact-info disc-fact))
       (changes (cons nil (search-back-for-event possible-instant (second finfo))))
       (after-val (third finfo))
       (found nil)
       (ch nil)
       ev-list
       ret
      )
        (when (eq after-val 'unk)
            (error "Value after discrepancy should be known.")
        )
        (loop while (not found) do
            (setq changes (rest changes))
            (setq ch (first changes))
            (cond
                ;;No more changes means that this requires initial conditions
                ((null ch) (return-from explain-using-removed-predecessor nil))
                ;;This cause isn't in use, so skip it.
                ((not (is-hypothesized (first ch))))
                ;;If an observation comes before a switching event, no event
                ;;removal will help
                ((typep (first ch) 'observation) 
                    (return-from explain-using-removed-predecessor nil)
                )
                ;;Value could be used as a precondition but no effect on it,
                ;;keep going.
                ; ((eq (third ch) 'none))
                ;;The change is due to an action, can't be removed.
                ((cause-action (first ch))
                    (return-from explain-using-removed-predecessor nil)
                )
                ;;If the value after this event is the same, then no discrepancy
                ;;should exist. This can happen, when an event that's removed
                ;;was causing more than one discrepancy.
                ; ((eq after-val (third ch))
                    ; (return-from explain-using-removed-predecessor
                        ; (remove-disappeared-discrepancy hypo possible-instant)
                    ; )
                ; )
                ;;To prevent infinite looping, don't remove any events added in the
                ;;same session
                ((member (first ch) (hypothesis-event-list hypo))
                    (return-from explain-using-removed-predecessor nil)
                )
                ;;The value is where the change occurred, this is an eligible
                ;;event
                (t (setq found t))
            )
        )
        (setq changes (cons nil changes))
        (loop 
            do
                (setq changes (rest changes))
                (setq ch (first changes))
                (when (and (is-hypothesized (first ch)) 
                           (not (eq after-val (get-value-after-changes changes)))
                      )
                    (push (first ch) ev-list)
                )
            while 
                (and ch (eq (change-time ch) (change-time (second changes))))
        )
        
        (when (null ev-list)
;            (error "No predecessor to discrepancy.")
            (return-from explain-using-removed-predecessor nil)
        )
        (my-trace-in (format nil "remove-pred ~A ~A" disc-fact (first ev-list)))
        (setq ret (explain-using-multiple-removed-events disc ev-list hypo))
        (my-trace-out (format nil "remove-pred ~A ~A" disc-fact (first ev-list)))
        ret
    )
)

(defun explain-using-removed-successor (disc hypo)
    (set-hypothesis hypo)
    (when (discrepancy-dependent disc)
        (return-from explain-using-removed-successor nil)
    )
    (let*
      ((disc-fact (discrepancy-fact disc))
       (possible-instant (discrepancy-instant disc))
       (finfo (get-fact-info disc-fact))
       (changes (search-forward-for-event possible-instant (second finfo) :exclusive t))
       (ch (first changes))
       (after-val (third finfo))
       ev-list
       ret
      )
        (when (eq after-val 'unk)
            (error "Value after discrepancy should be known.")
        )
        (when (shop2::is-funcall disc-fact)
            ;;Can't handle here.
            (return-from explain-using-removed-successor nil)
        )
        (cond
            ;;No more changes means that the successor has already been removed.
            ((null ch)
                ;;This should have already occurred in pred
                (return-from explain-using-removed-successor
                    (remove-disappeared-discrepancy hypo possible-instant)
                )
            )
            ;;This cause isn't in use, which means an instant with multiple 
            ;;events.
            ; ((not (is-hypothesized (first ch)))
                ; (loop while 
                    ; (and (not (is-hypothesized (first ch)))
                         ; (eq (change-time ch) (change-time (second changes)))
                    ; ) do
                    ; (setq changes (rest changes))
                ; )
                ; (setq ch (first changes))
                ; (when (not (is-hypothesized ch))
                    ; (error "Change returned should have been hypothesized.")
                ; )
                ; (when (member (first ch) (hypothesis-event-list hypo))
                    ; (return-from explain-using-removed-successor nil)
                ; ) 
            ; )
            ;;If the event after was added recently, can't be fixed. 
            ((member (first ch) (hypothesis-event-list hypo))
                (return-from explain-using-removed-successor nil)
            )
            ;;If the value after is not as predicted, no need to go on. Either
            ;;the value afterwards is fine, or is 'unk, meaning it no longer
            ;;matters.
            ; ((not (eq after-val (second ch)))
                ; ;;This should have already occurred in pred
                ; (return-from explain-using-removed-successor
                    ; (remove-disappeared-discrepancy hypo possible-instant)
                ; )
            ; )
            ;;If an observation comes after the discrepancy, no way to remove
            ((typep (first ch) 'observation) 
                (return-from explain-using-removed-successor nil)
            )
            ;;The change is due to an action, can't be removed.
            ((cause-action (first ch))
                (error "There should be no action successors to discrepancies.")
            )
        )
        (loop 
            do
                (setq ch (first changes))
                (when (and (is-hypothesized (first ch)) (eq after-val (second ch)))
                    (push (first ch) ev-list)
                )
                (setq changes (rest changes))
            while 
                (and ch (eq (change-time ch) (change-time (first changes))))
        )
        (when (null ev-list)
            (error "No successor to discrepancy.")
        )
        ;;All the cond tests were passed.
        (my-trace-in (format nil "remove-succ ~A ~A" disc-fact (first ev-list)))
        (setq ret (explain-using-multiple-removed-events disc ev-list hypo))
        (my-trace-out (format nil "remove-succ ~A ~A" disc-fact (first ev-list)))
        ret
    )
)

(defun explain-using-removed-contradictions (discs hypo &aux new-hypo-list rems)
    (setq rems (reduce #'union (mapcar #'discrepancy-removals discs)))
    (when (intersection rems (hypothesis-event-list hypo))
        ;;Two events could not co-exist at the same time. One was added earlier
        ;;this session, so the hypothesis with both is void.
        (return-from explain-using-removed-contradictions nil)
    )
    (setf (hypothesis-discrepancies hypo) 
        (set-difference (hypothesis-discrepancies hypo) discs)
    )
    (my-trace-in (format nil "remove-contra ~A" rems))
    (setq new-hypo-list (explain-using-multiple-removed-events nil rems hypo))
    (my-trace-out (format nil "remove-contra ~A" rems))
    new-hypo-list
)

(defun explain-using-multiple-removed-events (disc ev-list hypo 
                                                   &aux hypo-list new-hypo-list)
    (set-hypothesis hypo)
    (when (and (> (+ (hypothesis-changes hypo) (length ev-list)) *depth-limit*)
               (eq *cost-computation-method* :change-count)
          )
        (return-from explain-using-multiple-removed-events nil)
    )
    (when (intersection ev-list (hypothesis-event-list hypo))
        (return-from explain-using-multiple-removed-events nil)
    )
    (setq hypo (copy-hypothesis hypo))
    (setf (hypothesis-discrepancies hypo) 
        (remove disc (hypothesis-discrepancies hypo))
    )
    (setq hypo-list (list hypo))
    (setq new-hypo-list nil)
    (dolist (ev ev-list)
        (dolist (h hypo-list)
            (push-all 
                (form-removed-event-hypothesis nil h ev :refine nil) 
                new-hypo-list
            )
        )
        (setq hypo-list new-hypo-list
              new-hypo-list nil
        )
    )
    (dolist (h hypo-list)
;        (break "before refine in mult.")
        (push-all (refine-hypothesis h) new-hypo-list)
;        (break "after refine in mult.")
    )
    (dolist (new-ev (reduce #'union (cons nil (mapcar #'hypothesis-event-list hypo-list))))
        (when (not (member new-ev (hypothesis-event-list hypo)))
;                (format t "~%Removed event ~A created for a child." new-ev)
            (remove-event new-ev)
        )
    )
    new-hypo-list
)

(defun remove-disappeared-discrepancy (hypo instant &aux child-hypos disc-fact)
    (setq disc-fact (discrepancy-fact (first (hypothesis-discrepancies hypo))))
    (break "In remove-disappeared-discrepancy")
    (setq hypo (copy-hypothesis hypo))
    (setf (hypothesis-discrepancies hypo) (rest (hypothesis-discrepancies hypo)))
    (my-trace-in (format nil "remove-disappeared ~A ~A" disc-fact instant))
    (setq child-hypos (refine-hypothesis hypo))
    (my-trace-out (format nil "remove-disappeared ~A ~A" disc-fact instant))
    child-hypos
)

(defun form-kludge-hypothesis (hypo &aux last-tp new-cause)
    (setq hypo (copy-hypothesis hypo))
    (setq last-tp 
        (first
            (sort 
                (copy-list (instant-prior-times (happening-timepoint *latest-obs*)))
                #'instant-ordering
            )
        )
    )
    (when (or (not (instant-final last-tp)) 
              (not (eq (instant-next-moment last-tp) 'none))
              (instant-observation last-tp))
        (error "Did not find correct last instant.")
    )
    (when (instant-causes last-tp)
        (setq last-tp (make-following-instant last-tp))
        (link-instants last-tp (happening-timepoint *latest-obs*))
        (format t "~%~%Following instant created for kludge hypo. ~A" last-tp) 
    )
    
    
    (setq new-cause
        (make-cause
            :signature (list 'mysterious-forces)
            :preconditions nil
            :postconditions nil
            :timepoint last-tp
            :hypothesized t
        )
    )

    (push new-cause (instant-causes last-tp))
    (push new-cause (hypothesis-event-list hypo))
;    (break "Check kludge.")
    
    (dolist (disc (hypothesis-discrepancies hypo))
        (when (not (equal (instant-next-times (discrepancy-instant disc))
                          (list (happening-timepoint *latest-obs*))
              )    )
            (error "Discrepancy does not concern latest observation.")
        )
        (push (discrepancy-fact disc) (cause-postconditions new-cause))
        (history-insert-postcondition new-cause (discrepancy-fact disc))
;        (break "Check kludge 2.")
        (when (not (discrepancy-fixed disc))
            (error "Discrepancy not corrected.")
        )
    )
    
    
    (setf (hypothesis-discrepancies hypo) nil)
    (list hypo)
)

(defun form-restart-hypothesis (&aux assumptions hypo)
    (setq hypo (make-hypothesis))
    (dolist (hist-item (history-fact-histories *history*))
        (let ((fact (first hist-item)) (fhist (second hist-item)))
            (when (and (fact-history-hidden fhist) 
                       (get-value-after-changes (fact-history-changes fhist))
                  )
                (push fact assumptions)
            )
        )
    )
    (initialize-explainer)
    (handle-initial-observations)
    (dolist (fact assumptions)
        (let ((fhist (make-unobserved-fact-history fact)))
            (push (first (first (last (fact-history-changes fhist))))
                (hypothesis-event-list hypo)
            )
        )
    )
    (list hypo)
)


(defun form-removed-event-hypothesis (disc hypo ev &key (refine t)
                                                   &aux hypo-list effects)
    (setq hypo (copy-hypothesis hypo))
    (push ev (hypothesis-event-removals hypo))
    (setf (cause-hypothesized ev) nil)
;    (setq tp (cause-timepoint ev))
    ; (setf (instant-hypothesized tp)
        ; (or (instant-observation tp) 
            ; (some #'is-hypothesized (instant-causes tp))
        ; )
    ; )
    (setf (hypothesis-discrepancies hypo)
        (remove disc (hypothesis-discrepancies hypo))
    )
    (setq effects (cause-postconditions ev))
    (dolist (disc (hypothesis-discrepancies hypo))
        (cond
            ((discrepancy-removals disc))
            ((extend-discrepancy-later-during-removal hypo disc ev))
            (t 
                (setq effects 
                    (extend-discrepancy-earlier-during-removal 
                        hypo disc ev effects
                    )
                )
            )
        )
    )
    (dolist (eff (append (cause-postconditions ev) (cause-preconditions ev)))
        (let*
          ((finfo (get-fact-info eff))
           (fhist (second finfo))
           (next-change (first (search-forward-for-event ev fhist :exclusive t)))
           (val (second next-change))
           (prior-switch nil)
           (old-val nil)
           (switch-time nil)
          )
            (when (and next-change (not (eq val 'unk)))
                (setq prior-switch (first (search-back-for-event (cause-timepoint ev) fhist)))
                (when (null prior-switch)
                    (setq prior-switch (list *initial-obs* 'unk nil))
                )
                (setq switch-time (change-time prior-switch))
                (if (eq (third prior-switch) 'none)
                    ;then
                    (let ((ptp (instant-prior-moment (change-time prior-switch))))
                        (setq old-val (second prior-switch))
                        (when (not (eq ptp 'none))
                            (setq switch-time ptp)
                        )
                    )
                    ;else
                    (setq old-val (third prior-switch))
                )
                (when (not (eq old-val val))
                    (when (eq (happening-timepoint (first next-change)) 'none)
                        (error "Can't put none in a next list.")
                    )
                    ; (when (eq (car (first finfo)) 'rover::attempting-move)
                        ; (break "In remove event problem.")
                    ; )
                    (push 
                        (make-discrepancy 
                            :fact 
                                (if val
                                    ;then
                                    (first finfo)
                                    ;else
                                    (list 'not (first finfo))
                                )
                            :instant
                                (get-initial-instant 
                                    :prior-time
                                        switch-time
                                    :next-time 
                                        (change-time next-change)
                                )
                        )
                        (hypothesis-discrepancies hypo)
                    )
                )
            )
        )
    )
    
;    (when (equal (cause-signature ev) 
;            '(rover::rover-moves rover::rover1 rover::north rover::waypoint25 
;                                 rover::north rover::north rover::waypoint19
;             )
;          )
;        (break "Event under removal.")
;    )
    
    (setq hypo-list nil)
    (dolist (prec (cause-preconditions ev))
        (let* 
          ((finfo (get-fact-info prec))
           (new-hypo (copy-hypothesis hypo))
           (last-changes
               (search-back-for-event ev 
                   (second finfo)
                   :exclusive t
               )
           )
           (refined-hypos nil)
           discs
           new-cause
          )
            (set-hypothesis new-hypo)
            (setq new-cause
                (make-cause
                    :signature (list 'removal (cause-signature ev))
                    :preconditions (list (not-ify prec))
                    :postconditions nil
                    :timepoint (cause-timepoint ev)
                    :hypothesized t
                )
            )
            (push new-cause (instant-causes (cause-timepoint ev)))
            (push new-cause (hypothesis-event-list new-hypo))
            (setq discs
                (history-insert-precondition 
                    new-cause
                    (not-ify prec)
                )
            )
            ;; Fake instants appear to be correct here; they are place holders for 
            ;; when new removals may be necessary.
;            (when (notany #'instant-fake (mapcar #'discrepancy-instant discs))
                (when discs (push-all discs (hypothesis-discrepancies new-hypo)))
;                (break "Mystery.")
                
                (when (eq (not (third finfo)) (get-value-after-changes last-changes))
                    (return-from form-removed-event-hypothesis
                        (if refine
                            ;then
                            (refine-hypothesis hypo)
                            ;else
                            (list hypo)
                        )
                    )
                )
                (when (eq (instant-prior-moment (cause-timepoint ev)) 'none)
                    (error "Can't put none in a next list.")
                )
    
                (when refine
                    (setq refined-hypos (refine-hypothesis new-hypo))
                    (push-all refined-hypos hypo-list)
                )
                (when (not refine)
                    (push new-hypo hypo-list)
                )
;            )
        )
    )
;    (when hypo-list 
;        (break "Successful event removal: ~A." ev)
;    )
    hypo-list
)

(defun extend-discrepancy-earlier-during-removal (hypo disc ev effects &aux tp tpn fact finfo changes)
    (setq tp (discrepancy-instant disc) 
          fact (not-ify (discrepancy-fact disc))
          finfo (get-fact-info fact) 
          changes (fact-history-changes (second finfo))
    )
    (when (eq (car (instant-prior-times tp)) (cause-timepoint ev))
;        (break "Relevant discrepancy.")
    )
    (when (instant-final tp)
        (return-from extend-discrepancy-earlier-during-removal effects)
    )
    (when (not (instant-final tp))
        (setq tpn (car (instant-next-times tp)))
        (setq tp (car (instant-prior-times tp)))
    )
    (when (and (eq tp (cause-timepoint ev))
               (member ev changes :key #'first)
               (null 
                   (intersection  
                       (remove ev (instant-causes (cause-timepoint ev)))
                       (mapcar #'first changes)
                   )
               )
          )
        (setq effects (remove fact effects :test #'equal))
        (setf (hypothesis-discrepancies hypo) (remove disc (hypothesis-discrepancies hypo)))
        (let* 
          ((changes (search-back-for-event tp (second finfo)))
           (prior-time nil) 
          )
;            (break "Reconsidering discrepancies.")
            (when (eq (get-value-after-changes changes) (third finfo))
                (when changes
                    (setq prior-time (happening-timepoint (caar changes)))
                )
                (push
                    (make-discrepancy
                        :fact (discrepancy-fact disc)
                        :instant 
                            (get-initial-instant 
                                :prior-time prior-time
                                :next-flush-time tpn
                            )
                        :dependent (discrepancy-dependent disc)
                    )
                    (hypothesis-discrepancies hypo)
                )
            )
;            (break "Discrepancy added.")
        )
    )
    effects
)


(defun extend-discrepancy-later-during-removal (hypo disc ev 
                                                &aux tp tpp fact finfo changes ret)
    (setq tp (discrepancy-instant disc) 
          tpp (car (instant-prior-times tp)) 
          fact (discrepancy-fact disc)
          finfo (get-fact-info fact) 
          changes (fact-history-changes (second finfo))
          ret nil
    )
    (if (instant-final tp)
        ;then
        (setq tp nil)
        ;else
        (setq tp (car (instant-next-times tp)))
    )
    (when (and tp (not (eq (instant-next-moment tp) 'none)))
        (setq tp (instant-next-moment tp))
    )
    (when (eq tp (cause-timepoint ev))
        (setq ret t)
;        (break "Relevant discrepancy.")
    )
    (when (and (not (null tp))
               (null (discrepancy-dependent disc))
               (not (instant-final tp))
               ;; removing end-of-disc event
               (eq (car (instant-next-times tp)) (cause-timepoint ev))
               ;; event affects disc literal
               (member ev changes :key #'first)
               ;; no other simultaneous event affects disc literal
               (null 
                   (intersection  
                       (remove ev (instant-causes (cause-timepoint ev)))
                       (mapcar #'first changes)
                   )
               )
          )
        (setf (hypothesis-discrepancies hypo) (remove disc (hypothesis-discrepancies hypo)))
        (let* 
          ((changes (search-forward-for-event tp (second finfo)))
           (next-time nil)
          )
;            (break "Reconsidering discrepancies.")
            (when (and changes (eq (get-value-after-changes changes) (third finfo)))
                (when changes
                    (setq next-time (happening-timepoint (caar changes)))
                )
                (when (discrepancy-dependent disc)
                    (break "Extending past dependent?" )
                )
                (push
                    (make-discrepancy
                        :fact fact
                        :instant 
                            (get-initial-instant 
                                :prior-time tpp
                                :next-time next-time
                            )
                        :dependent (discrepancy-dependent disc)
                    )
                    (hypothesis-discrepancies hypo)
                )
            )
;            (break "Discrepancy added.")
        )
    )
    ret
)    

(defun push-instant (tp hypo)
    (when (not (instant-final tp))
        (push tp (hypothesis-instants hypo))
    )
)

;; This is intended to handle the initial instants that are created for 
;; reasoning about events before causality is nailed down.
(defun get-initial-instant (&key (prior-time nil) (next-time nil) (next-flush-time nil) &aux orig-next-time)
    (when next-flush-time (setq next-time next-flush-time))
    (setq orig-next-time next-time)
    (when (and prior-time next-time
              (or (member next-time (list-earlier-instants prior-time))
                  (member prior-time (list-later-instants next-time))
              )
          )
        (error "Causality threat.")
    )
    (when (and (null next-flush-time) (not (eq (instant-prior-moment next-time) 'none)))
        (setq next-time (instant-prior-moment next-time))
    )
    (when (eq prior-time (instant-prior-moment next-time))
        (return-from get-initial-instant next-time)
    )
    (let (tp)
        (setq tp
            (make-instant
                :prior-times (if (null prior-time) nil (list prior-time))
                :next-times (if (null next-time) nil (list next-time))
                :hypothesized nil
                :final nil
            )
        )
        (when (bad-instant tp)
;            (break "Setting instant-fake.")
            (setf (instant-fake tp) t)
            (setf (instant-next-times tp) (if (null orig-next-time) nil (list orig-next-time)))
        )
        tp
    )
)

(defun get-final-instant (&key (prior-times nil) (next-times nil))
    (make-instant
        :prior-times prior-times
        :next-times next-times
        :hypothesized t
        :final t
    )
)


(defun find-cycle (tp &aux chains (limit 100) (ctr 0))
    (setq chains (mapcar #'list (remove-if-not #'instant-hypothesized (instant-prior-times tp))))
    (loop while (and (not (find tp chains :key #'car)) (< ctr limit)) do
         (setq chains (add-link-to-chains chains))
         (incf ctr)
    )
    (if (< ctr limit)
        ;then
        (append (find tp chains :key #'car) (list tp))
        ;else
        :no-cycle
    )
)

(defun add-link-to-chains (chains &aux new-chains)
    (dolist (chain chains)
        (dolist (tp (instant-prior-times (car chain)))
            (when (instant-hypothesized tp)
                (push (cons tp chain) new-chains)
            )
        )
    )
    new-chains
)

; disc = discrepancy to be explained; one fact
; hypo = current hypothesis that must be refined
(defun explain-using-added-events (disc hypo &aux (branches 0))
    (let ((hypotheses nil)
          (possible-instant (discrepancy-instant disc))
          (disc-fact (discrepancy-fact disc))
          (state nil)
          binding-sets
         )
        (when (instant-fake possible-instant)
            ;;Actually, not an error: this just means adding won't work.
;            (error "Fake instant in use.")
            (return-from explain-using-added-events nil)
        )
        (my-trace-in (format nil "added-events ~A" disc-fact))
        (dolist (occ (find-causing-occurrences disc-fact))
            (dolist (conjunct (cnf (get-preconditions occ)))
                (if (> 100 (check-estimated-branch-count disc :usable t))
                    ;then
                    (setq binding-sets (find-usable-bindings conjunct))
                    ;else
                    (progn
                        (when (null state) 
                            (setq state (collect-discrepancy-state possible-instant))
                        )
                        (setq binding-sets (find-compatible-bindings conjunct state))
                    )
                )
                (dolist (blist binding-sets)
                    (let 
                      ((occ2 
                          (make-occurrence 
                              :template (occurrence-template occ)
                              :bindings (append (occurrence-bindings occ) blist)
                              :precs (shop2::apply-substitution conjunct blist)
                          )
                      ))
                        (setq hypotheses 
                            (append hypotheses 
                                (find-hypotheses 
                                    occ2 hypo possible-instant 
                                    (occurrence-precs occ2)
                                )
                            )
                        )
                        (incf branches)
                    )
                )
            )
        )
        (when (eq 0 branches)
            (my-trace-end "No events explain fact.")
        )
;        (cache-result disc-fact hypo hypotheses)
        (record-branch-count disc-fact branches)
        (my-trace-out (format nil "added-events ~A" disc-fact))
        hypotheses
    )
)

(defvar *cached-results* nil)

(defun is-cached-failure (disc orig-hypo)
    (declare (ignore disc orig-hypo))
)

(defun cache-failure (disc orig-hypo &aux cache)
    (set-hypothesis orig-hypo)
    (setq cache (assoc disc *cached-results*))
    (when (null cache)
        (setq cache nil)
        (push (list disc) *cached-results*) 
    )
    (push
        (list-earlier-happenings 
            (car (instant-next-times (discrepancy-instant disc)))
        )
        (cdr cache)
    )
)

(defvar *httb* (make-hash-table))
(defvar *htfb* (make-hash-table))

(defun estimate-branch-counts ()
    (dolist (prop (domain-predicates *domain-model*))
        (let ((disc nil))
            (dotimes (i (/ (- (length prop) 1) 3))
                (push (gensym) disc) 
            )
            (push (car prop) disc)
            (setf (gethash (car prop) *httb*) (estimate-branch-count disc)) 
            (setf (gethash (car prop) *htfb*) (estimate-branch-count (list 'not disc))) 
        )
    )
)

(defun estimate-branch-count (disc &aux (usable-branches 0) (compatible-branches 0))
    (dolist (occ (find-causing-occurrences disc))
        (dolist (conjunct (cnf (get-preconditions occ)))
            (setq usable-branches 
                (+ usable-branches (find-usable-bindings-count conjunct))
            )
            (setq compatible-branches 
                (+ compatible-branches
                    (length
                        (find-bindings
                            (set-difference 
                                (find-vars (list conjunct)) 
                                (find-vars (get-pos-literals (get-uncausables conjunct)))
                                :test #'equal
                            )
                        )
                    )
                )
            )
        )
    )
    (list usable-branches compatible-branches)
)

(defun guess-branch-count (disc &aux branches)
    (setq branches (check-branch-count disc))
    (when (null branches)
        (setq branches (check-estimated-branch-count disc))
    )
    branches
)

(defun check-estimated-branch-count (disc &key (usable nil) &aux branches)
    (when (typep disc 'discrepancy)
        (setq disc (discrepancy-fact disc))
    )
    (setq branches
        (if (eq (car disc) 'not)
            ;then
            (gethash (caadr disc) *htfb*)
            ;else
            (gethash (car disc) *httb*)
        )
    )
    (if (null branches)
        ;then
        0
        ;else
        (if usable
            ;then
            (first branches)
            ;else
            (second branches)
        )
    )
)



(defvar *branch-map-true* (make-hash-table))
(defvar *branch-map-false* (make-hash-table))
(defvar *branch-map-memory-true* (make-hash-table))
(defvar *branch-map-memory-false* (make-hash-table))

(defun record-branch-count (disc branches)
    (if (eq (car disc) 'not)
        ;then
        (compute-average-branch-count 
            (caadr disc) branches 
            *branch-map-false* *branch-map-memory-false*
        )
        ;else
        (compute-average-branch-count 
            (car disc) branches 
            *branch-map-true* *branch-map-memory-true*
        )
    )
)

(defun compute-average-branch-count (fact-type branches branch-map branch-memory)
    (let ((mem (gethash fact-type branch-memory)))
        (when (>= (length mem) 5)
            (setq mem (butlast mem))
        )
        (setq mem (cons (* branches branches) mem))
        (setf (gethash fact-type branch-memory) mem)
        (setf (gethash fact-type branch-map) (/ (apply '+ mem) (length mem)))
    )
)

(defun check-branch-count (disc &aux disc-fact)
    (setq disc-fact (discrepancy-fact disc))
    (if (eq (car disc-fact) 'not)
        ;then
        (gethash (caadr disc-fact) *branch-map-false*)
        ;else
        (gethash (car disc-fact) *branch-map-true*)
    )
)

(defvar *do-trace* nil)
(defvar *my-trace-level* 0)
(defvar *trace-times* nil)

(defun reset-trace-level ()
    (setq *my-trace-level* 0)
    (setq *trace-times* nil)
)

(defun my-trace-in (display-string)
    (when (not *do-trace*)
        (return-from my-trace-in nil)
    )
    (incf *my-trace-level*)
    (format t "~%~A~Aa. ~A" 
        (make-string (- *my-trace-level* 1) :initial-element #\Space) 
        *my-trace-level* 
        display-string
    )
    (push (get-internal-run-time) *trace-times*)
)

(defun my-trace-out (display-string)
    (when (not *do-trace*)
        (return-from my-trace-out nil)
    )
    (format t "~%~A~Ab. ~A Time: ~A" 
        (make-string (- *my-trace-level* 1) :initial-element #\Space) 
        *my-trace-level* 
        display-string 
        (- (get-internal-run-time) (pop *trace-times*))
    )
    (decf *my-trace-level*)
)
    
(defun my-trace-end (display-string)
    (when (not *do-trace*)
        (return-from my-trace-end nil)
    )
    (format t "~%~A~A. Bottom. ~A" 
        (make-string *my-trace-level* :initial-element #\Space) 
        (+ 1 *my-trace-level*)
        display-string
    )
)



(defun find-eligible-conjuncts (precs)
    (reduce #'append (mapcar #'find-bound-versions (cnf precs)))
)

(defun find-bound-versions (conjunct)
    (bind-variables (get-free-variables conjunct) conjunct)
)

(defun get-pos-literals (conjunct)
    (remove-if 
        #'(lambda (prec)
            (member (first prec) '(not eval > < <= >=))
          )
        conjunct
    )
)

(defun find-compatible-bindings (preconditions state)
    (let ((bindings nil) blists-causable blists-uncausable)
        (setq blists-causable
            (find-bindings
                (set-difference 
                    (find-vars preconditions) 
                    (find-vars (get-pos-literals (get-uncausables preconditions)))
                    :test #'equal
                )
            )
        )
        (setq blists-uncausable 
            (shopthpr:find-satisfiers 
                (cons 'and 
                    (remove 'not (get-uncausables preconditions)
                        :key #'car
                    )
                )
                state
                nil
            )
        )
        (dolist (b1 blists-causable)
            (dolist (b2 blists-uncausable)
                (push (append b1 b2) bindings)
            )
        )
        
        bindings
    )
)


(defun find-usable-bindings (preconditions)
    (find-bindings (find-vars preconditions))
)

(defun find-usable-bindings-count (preconditions)
    (find-bindings-count (find-vars preconditions))
)

(defun find-bindings (vars &optional (bindings nil))
    (if vars
        ;then
        (apply #'append
            (mapcar 
                #'(lambda (obj)
                    (find-bindings 
                        (rest vars)
                        (cons
                            (make-binding (caar vars) obj) 
                            bindings
                        )
                    )
                  )
                (enumerate-objects (cadar vars))
            )
        )
        ;else
        (list bindings)
    )
)

(defun find-bindings-count (vars)
    (if vars
        ;then
        (* (length (enumerate-objects (cadar vars)))
           (find-bindings-count (cdr vars))
        )
        ;else
        1
    )
)

(defun find-vars (tree &optional (vars nil))
    (cond
        ((atom tree) vars)
        ((all-atoms tree) (setq vars (find-vars-in-fact tree vars)))
        ((listp tree) 
            (setq vars (find-vars (car tree) vars))
            (find-vars (cdr tree) vars)
        )
    )
)

(defun find-vars-in-fact (fact vars)
    (let ((i 0))
        (setq i 0)
        (dolist (sym fact)
            (when (and (variablep sym) (not (member sym vars :key #'first)))
                (push (list sym (get-arg-type (car fact) i)) vars)
            )
            (incf i)
        )
        vars
    )
)

(defun get-free-variables (conjunct)
    (let ((vars nil) (i 0))
        (dolist (fact conjunct)
            (setq i 0)
            (when (eq (first fact) 'not)
                (setq fact (second fact))
            )
            (dolist (sym fact)
                (when (and (variablep sym) (not (member sym vars :key #'first)))
                    (push (list sym (get-arg-type (car fact) i)) vars)
                )
                (incf i)
            )
        )
        vars
    )
)

(defun get-arg-type (fact-type index)
    (let ((var-list (gethash fact-type *pred-types*)))
        (when (null var-list)
            (error 
                "Found predicate reference of type ~A that was not in the initial predicate list."
                fact-type
            )
        )
        (when (>= index (length var-list))
            (error "Found more than ~A arguments in a reference to predicate ~A."
                (- (length var-list) 1)
                fact-type
            )
        )
        (aref var-list index)
    )
)
                        
                        

(defun bind-variables (vars conjunct &aux (bound-conjuncts nil))
    (if (null vars)
        ;then
        (list conjunct)
        ;else
        (progn
            (dolist (obj (enumerate-objects (cadar vars)))
                (setq bound-conjuncts
                    (union
                        (bind-variables (cdr vars) (subst obj (car (car vars)) conjunct))
                        bound-conjuncts
                        :test #'equal
                    )
                )
            )
            bound-conjuncts
        )
    )
)

(defun enumerate-objects (obj-type)
    (gethash obj-type *obj-table*)
)
    
(defun parse-objects (obj-list &aux sym)
    (let ((names nil))
        (loop while obj-list do
            (setq sym (car obj-list))
;            (format t "~%Symbol: ~A Names: ~A" sym names)
            (if (eq sym '-)
                ;then
                (progn
                    (add-objs-to-table (second obj-list) names)
                    (setq names nil)
                    (setq obj-list (cddr obj-list))
                )
                ;else
                (progn
                    (push sym names)
                    (setq obj-list (cdr obj-list))
                )
            )
        )
    )
)

(defun add-objs-to-table (obj-type names)
    (dolist (cur-type (cons obj-type (gethash obj-type *supertypes*)))
        (setf (gethash cur-type *obj-table*)
            (union names (gethash cur-type *obj-table*))
        )
    )
)

(defvar *global-tp-name* nil)

;conjunct is a sufficient set of conditions that will make occ occur.
(defun find-hypotheses (occ hypo disc-time conjunct)
    (when (intersection conjunct (mapcar #'not-ify conjunct) :test #'equal)
        (return-from find-hypotheses nil)
    )
    (my-trace-in (format nil "find-hypos ~A" (get-occ-sig occ)))
    (let 
      ((branches nil)
       (new-branches nil)
       (true-branch-instant nil)
       (false-branch-instant nil)
       (hypotheses nil)
       (possible-instants (list disc-time))
       (linked-hypo nil)
       (links nil)
      )
        (set-hypothesis hypo)
        (dolist (precondition (get-uncausables conjunct))
;            (break "Constricting uncausable instants.")
            (setq possible-instants
                (constrain-instants possible-instants precondition)
            )
            (when (null possible-instants)
                (my-trace-end (format nil "Prec never true: ~A" precondition))
;                (break "Nonsense.")
                (my-trace-out "find-hypos")
                (return-from find-hypotheses nil)
            )
        )
        (setq branches (list (list possible-instants nil)))
        (dolist (precondition (append (get-causables conjunct) (get-postconditions occ)))
;            (break "Constricting instants.")
            (dolist (branch branches)
                (setq true-branch-instant (constrain-instants (car branch) precondition))
                (when (not (null true-branch-instant)) 
                    (push (list true-branch-instant (second branch)) new-branches)
                )
                (setq false-branch-instant (constrain-instants (car branch) (not-ify precondition)))
                (when (not (null false-branch-instant)) 
                    (push (list false-branch-instant (cons precondition (second branch))) new-branches)
                )
            )
            (setq branches new-branches)
            (setq new-branches nil)
        )
        (setq possible-instants (remove-duplicates (apply #'append (mapcar #'first branches))))
        (dolist (tp possible-instants)
            (setq linked-hypo (copy-hypothesis hypo))
            (incf (hypothesis-branches linked-hypo))
            (set-hypothesis linked-hypo)
            (when (instant-final tp)
;                (format t "~&Before prior times loop.")
                (dolist (ptp (instant-prior-times disc-time))
                    (when (not (instants-ordered ptp tp))
                        (push (make-instant-link ptp tp :hypo linked-hypo) links)
                    )
                )
;                (format t "~&Before next times loop.")
                (dolist (ntp (instant-next-times disc-time))
                    (when (not (instants-ordered tp ntp))
                        (push (make-instant-link tp ntp :hypo linked-hypo) links)
                    )
                )
                (setq links (remove nil links))
                (when (notany #'bad-instant links)
                    (push-all links (hypothesis-instants linked-hypo))
;                    (fix-history-order links)
;                    (format t "~&Before link-set loop.")
                    (dolist (link-set 
                                (form-instant-link-sets 
                                    tp (append conjunct (get-postconditions occ))
                                )
                            )
                        (when (notany #'bad-instant link-set)
;                            (fix-history-order link-set)
                            (setq hypotheses
                                (append
                                    (form-added-hypotheses 
                                        linked-hypo tp occ 
                                        conjunct 
                                        link-set
                                    )
                                    hypotheses
                                )
                            )
                        )
                    )
                )
;                (format t "~&Before link removal.")
                (dolist (old-link links)
                    (when (not (listp (instant-name old-link)))
;                        (format t "~%Removed ~A in find-hypos." (instant-name old-link))
                        (remove-instant old-link)
                    )
                )
;                (format t "~&Before event removal.")
                (dolist (ev (hypothesis-event-list linked-hypo))
                    (when (not (member ev (hypothesis-event-list hypo)))
                        (when (not (member (car (cause-signature ev)) '(initially-true removal)))
;                            (break "link event removal ~A" ev)
                            (remove-event ev)
                        )
                    )
                )
;                (format t "~&Before instant removal.")
                (dolist (otp (hypothesis-instants linked-hypo))
                    (when (not (member otp (hypothesis-instants hypo)))
                        (when (not (listp (instant-name otp)))
                            (remove-instant otp)
                        )
                    )
                )
            )
            (unless (instant-final tp)
                (when (instant-causes tp)
                    (setq tp (copy-instant tp))
;                    (format t "~&Copied instant ~A." (instant-name tp))
                    (substitute-timepoint 
                        linked-hypo 
                        (cause-timepoint (car (instant-causes tp)))
                        tp
                    )
;                    (when (null *global-tp-name*)
;                        (setq *global-tp-name* (instant-name tp))
;                    )
;                    (break "Substituted non-final timepoint.")
                )
                (setq hypotheses
                    (append
                        (form-added-hypotheses linked-hypo tp occ conjunct)
                        hypotheses
                    )
                )
                (when (instant-causes tp)
;                    (format t "~&Removed copied instant ~A." (instant-name tp))
;                    (when (eq (instant-name tp) *global-tp-name*)
;                        (setq *global-tp-name* nil)
;                    )
                    (mapcan #'remove-event (instant-causes tp))
                    (when (not (listp (instant-name tp)))
                        (break "Instant should have been automatically removed.")
                    )
                )
            )
        )
        (my-trace-out (format nil "find-hypos ~A Instants: ~A" (get-occ-sig occ) (length possible-instants)))
        hypotheses
    )
)

(defun basic-fact (f)
    (if (eq (car f) 'not)
        ;then
        (second f)
        ;else
        f
    )
)

(defun relevant-facts (tp &aux (ret nil))
    (dolist (ev (instant-causes tp))
        (when (cause-hypothesized ev)
            (setq ret (union (mapcar #'basic-fact (cause-preconditions ev)) ret :test #'equal)) 
            (setq ret (union (mapcar #'basic-fact (cause-postconditions ev)) ret :test #'equal))
        )
    )
    (when (instant-observation tp)
        (setq ret (union (mapcar #'basic-fact (observation-perceptions (instant-observation tp))) ret :test #'equal))
    )
    ret
)    

(defun fix-history-order (links)
    (let (prior-facts next-facts (linked-histories nil))
        (dolist (link links)
            (setq prior-facts nil next-facts nil)
            (dolist (tp (instant-prior-times link))
                (setq prior-facts (union prior-facts (relevant-facts tp) :test #'equal))
            )
            (dolist (tp (instant-next-times link))
                (setq next-facts (union next-facts (relevant-facts tp) :test #'equal))
            )
            (setq linked-histories                            
                (union linked-histories
                    (intersection prior-facts next-facts :test #'equal)
                    :test #'equal
                )
            )
        )
        (when links (break "sorting histories"))
        (dolist (fact linked-histories)
            (let ((hist (find-fact-history fact)))
                (setf (fact-history-changes hist)
                    (stable-sort (fact-history-changes hist) #'instant-ordering 
                        :key #'change-time
                    )
                )
            )
        )
    )
)

(defun form-instant-link-sets (new-tp lit-set) 
    (let ((relative-tp-list (append (list-earlier-instants new-tp :skip-first t) 
                                    (list-later-instants new-tp :skip-first t)
                                    (list new-tp)
                            )
          )
          (timepoints nil) (link-sets nil)
         )
        (dolist (literal lit-set)
            (setq timepoints
                (mapcar #'happening-timepoint
                    (remove-if-not #'hap-orderable
                        (mapcar #'first 
                            (fact-history-changes
                                (second (get-fact-info literal))
                            )
                        )
                    )
                )
            )
            (dolist (old-tp timepoints)
                (when (and (instant-hypothesized old-tp) 
                           (not (member old-tp relative-tp-list))
                      )
                    (setq link-sets (add-to-link-sets old-tp new-tp link-sets))
                    (push old-tp relative-tp-list)
                )
            )
        )
;        (when (> (length link-sets) 2)
;            (break "Check link-sets.")
;        )
        (if (null link-sets)
            ;then return
            (list nil)
            ;else return
            link-sets
        )
    )
)

(defun add-to-link-sets (old-tp new-tp link-sets &aux (new-link-sets nil))
    (when (null link-sets)
        (return-from add-to-link-sets
            (let 
              ((prior-link
                    (make-instant 
                        :prior-times nil 
                        :next-times nil
                        :hypothesized nil
                        :link t
                    )
               )
               (next-link
                    (make-instant 
                        :prior-times nil 
                        :next-times nil
                        :hypothesized nil
                        :link t
                    )
               )
              )
                (link-instants prior-link new-tp)
                (link-instants new-tp next-link)
                (list
                    (list
                        prior-link
                        (make-instant-link new-tp old-tp :hypothesized nil)
                    )
                    (list
                        (make-instant-link old-tp new-tp :hypothesized nil)
                        next-link
                    )
                )
            )
        )
    )
    (dolist (link-set link-sets)
        (let* ((prior-link (car link-set)) 
               (next-link (cadr link-set))
               (new-prior-link (make-instant :prior-times nil :next-times nil :hypothesized nil :link t))
               (new-next-link  (make-instant :prior-times nil :next-times nil :hypothesized nil :link t))
              )
            (link-instants new-prior-link new-tp)
            (link-instants old-tp new-prior-link)
            (link-instants new-next-link old-tp)
            (link-instants new-tp new-next-link)
            (dolist (otp (instant-prior-times prior-link))
                (link-instants otp new-prior-link)
            )
            (dolist (otp (instant-next-times next-link))
                (link-instants new-next-link otp)
            )
            (push (list prior-link new-next-link) new-link-sets)
            (push (list new-prior-link next-link) new-link-sets)
        )
    )
    new-link-sets
)

(defun make-added-hypotheses (hypo instant occ conjunct &aux new-event)
    (when (or (instant-observation instant)
              (and (instant-final instant) 
                   (not (eq (instant-prior-moment instant) 'none))
                   (instant-observation (instant-prior-moment instant))
              )
          )
        ;;An event may not occur directly following an observation.
        (my-trace-end (format nil "Can't have event at observation time."))
        (return-from make-added-hypotheses nil)
    )
    (let 
      ((old-event 
          (find (get-occ-sig occ) 
              (hypothesis-event-removals hypo) 
              :key #'cause-signature
          )
      ))
        (when old-event
            (break "Deciding whether to allow the addition of a removed event.")
            (when (eq (happening-timepoint old-event) instant)
                (return-from make-added-hypotheses nil)
            )
        )
    )
    (setq hypo (copy-hypothesis hypo))
    (setq new-event 
        (make-cause
            :signature (get-occ-sig occ)
            :preconditions conjunct
            :postconditions (get-postconditions occ)
            :timepoint instant
            :hypothesized t
        )
    )
    (push new-event (instant-causes instant))
    (when (not (eq instant (cause-timepoint new-event)))
        (break "Inconsistent.")
    )
;    (break "Ready to form new hypothesis.")
    (form-added-event-hypothesis 
        :parent-hypothesis hypo 
        :new-event new-event
    )
)

(defmethod get-occ-sig ((occ occurrence) &aux vars)
    (setq vars
        (mapcar #'first 
            (append 
                (parameters-of (occurrence-template occ))
                (reverse (find-vars (precondition-of (occurrence-template occ))))
            )
        )
    )
    (setq vars (remove-duplicates vars :from-end t))
    (cons                                                                       
        (name-of (occurrence-template occ))
        (mapcar
            #'shop2.unifier::binding-val 
            (remove nil
                (mapcar+
                    #'find
                    vars
                    (occurrence-bindings occ)
                    :key #'shop2.unifier::binding-var
                )
            )
        )
    )
)

(defun should-be-final (tp &key (check-bad t) &aux endpoint)
    (when (instant-final tp)
        (return-from should-be-final nil)
    )
    (when (and check-bad (bad-instant tp :should t))
        (return-from should-be-final nil)
    )
    (setq endpoint
        (first
            (sort 
                (remove-if-not #'instant-final 
                    (instant-next-times tp)
                )
                #'instant-ordering
            )
        )
    )
    (if
        (and endpoint 
            (member (instant-prior-moment endpoint) 
                    (list-earlier-instants tp)
            )
        )
        ;then
        endpoint
        ;else
        nil
    )
)

;;This finds initial instants which need to be contracted to final instants and 
;;makes the necessary adjustments.
(defun substitute-timepoint (hypo old-tp new-tp &aux new-ev discs links)
    (setf (hypothesis-instants hypo) 
        (remove old-tp (hypothesis-instants hypo))
    )
    (when (instant-final old-tp)
        (error "Substituting for final instant.")
    )
    (dehypothesize-instant old-tp)
    (dolist (ev (instant-causes old-tp))
        (setf (instant-causes new-tp) (remove ev (instant-causes new-tp)))
        (setf (hypothesis-event-list hypo) 
            (remove ev (hypothesis-event-list hypo))
        )
        (setf (cause-hypothesized ev) nil)
        (setq new-ev (copy-cause ev))
        (push new-ev (hypothesis-event-list hypo))
        (setf (cause-timepoint new-ev) new-tp)
        (push new-ev (instant-causes new-tp))
        (setf (cause-hypothesized new-ev) t)
        (setq discs nil)
        (dolist (disc (hypothesis-discrepancies hypo))
            (when (eq (discrepancy-dependent disc) ev)
                (setq disc (copy-discrepancy disc))
                (setf (discrepancy-dependent disc) new-ev)
            )
            (push disc discs)
        )
        (setf (hypothesis-discrepancies hypo) (reverse discs))
        (dolist (post (cause-postconditions new-ev))
            (history-insert-postcondition new-ev post)
        )
        (dolist (prec (cause-preconditions new-ev))
            (history-insert-precondition new-ev prec)
        )
    )
    (dolist (other-tp (instant-prior-times old-tp))
        (when (not (instant-final other-tp))
            (if (instant-final new-tp)
                ;then
                (push (make-instant-link other-tp new-tp) links) 
                ;else
                (link-instants other-tp new-tp)
            )
        )
    )
    (dolist (other-tp (instant-next-times old-tp))
        (when (not (instant-final other-tp))
            (when (not (member other-tp (list-earlier-instants new-tp)))
                (if (instant-final new-tp)
                    ;then
                    (push (make-instant-link new-tp other-tp) links) 
                    ;else
                    (link-instants new-tp other-tp)
                )
            )
        )
    )
    (push-all (remove nil links) (hypothesis-instants hypo))
    (setq discs nil)
    (setf (instant-hypothesized new-tp) t)
    (dolist (disc (hypothesis-discrepancies hypo))
        (when (not (instant-final (discrepancy-instant disc)))
            (when (member old-tp (instant-prior-times (discrepancy-instant disc)))
                (setq disc (copy-discrepancy disc))
                (setf (discrepancy-instant disc) (copy-instant (discrepancy-instant disc)))
                (setf (instant-prior-times (discrepancy-instant disc))
                    (cons new-tp 
                        (remove old-tp (instant-prior-times (discrepancy-instant disc)))
                    )
                )
            )
            (when (member old-tp (instant-next-times (discrepancy-instant disc)))
                (setq disc (copy-discrepancy disc))
                (setf (discrepancy-instant disc) (copy-instant (discrepancy-instant disc)))
                (setf (instant-next-times (discrepancy-instant disc))
                    (cons new-tp 
                        (remove old-tp (instant-next-times (discrepancy-instant disc)))
                    )
                )
                ; (when (null (constrain-instant 
                                 ; (discrepancy-instant disc)
                                 ; (not-ify (discrepancy-fact disc))
                                 ; :find-all-links nil
                            ; )
                      ; )
                    ; 
                    ; (break "What the hell?!")
                ; )
                (when (or (discrepancy-irrelevant disc) (discrepancy-fixed disc))
                    (let ((new-disc-tp
                           (car (constrain-instant 
                                 (discrepancy-instant disc)
                                 (not-ify (discrepancy-fact disc))
                                 :find-all-links nil
                                )
                           )
                         ))
                        (when (and (not (instant-final new-disc-tp))
                                   (or (cdr (instant-prior-times new-disc-tp))
                                       (cdr (instant-next-times new-disc-tp))
                                   )
                              )
                            (break "Strange discrepancy exists.")
                        )
                        (setf (discrepancy-instant disc) new-disc-tp)
                    )
                    (when (null (discrepancy-instant disc))
                        (return-from substitute-timepoint 'link-breaks-causality)
                    )
                )
            )
        )
        (push disc discs)
    )
    (setf (hypothesis-discrepancies hypo) (reverse discs))
)

(defun check-links (hypo link-set &aux (init-tps nil) new-tp (affected-facts nil))
    (dolist (link link-set)
        (setq init-tps (union init-tps (instant-prior-times link)))
        (setq init-tps (union init-tps (instant-next-times link)))
    )
    (setq init-tps (remove-if #'instant-final init-tps))
    (dolist (old-tp init-tps)
        (setq new-tp (should-be-final old-tp))
        (when new-tp
            (dolist (link link-set)
                (setf (instant-prior-times old-tp)
                    (remove link (instant-prior-times old-tp))
                )
                (setf (instant-next-times old-tp)
                    (remove link (instant-next-times old-tp))
                )
                (setf (instant-prior-times link)
                    (remove old-tp (instant-prior-times link))
                )
                (setf (instant-next-times link)
                    (remove old-tp (instant-next-times link))
                )
            )
            (when (eq (substitute-timepoint hypo old-tp new-tp) 'link-breaks-causality)
                (return-from check-links 'link-breaks-causality)
            )
        )
    )
    (dolist (link link-set)
        (when (and (instant-prior-times link) (instant-next-times link))
            (let ((relevant-before (reduce #'equal-union (mapcar #'get-relevant-literals (instant-prior-times link)) :initial-value nil))
                  (relevant-after  (reduce #'equal-union (mapcar #'get-relevant-literals (instant-next-times link))  :initial-value nil)))
                (setq affected-facts (union affected-facts (intersection relevant-before relevant-after :test #'equal) :test #'equal))
;                (break "Checking affected-facts.")
            )
        )
    )
;    (break "Checking affected-facts again.")
    (dolist (fact affected-facts)
        (let ((fhist (find-fact-history fact)))
;            (break "Testing for resort.")
            (resort-history fhist)
        )
    )
)
        
(defun equal-union (list1 list2)
    (union list1 list2 :test #'equal)
)


(defun form-added-hypotheses (hypo instant occ conjunct &optional (link-set nil))
;    (setq hypo (copy-hypothesis hypo))
    (dolist (link link-set)
        (setf (instant-hypothesized link) t)
        (when (member link (list-later-instants link :skip-first t))
            ;;Cycles in causality can be caused by link combinations.
;            (break "Checking cyclicality correctness.")
            (dolist (bad-link link-set)
;                (format t "~%Removed ~A in form-added 3." (instant-name bad-link))
                (remove-instant bad-link)
            )
            (return-from form-added-hypotheses nil)
        )
    )
    (when (eq (check-links hypo link-set) 'link-breaks-causality)
        (return-from form-added-hypotheses nil)
    )
    (set-hypothesis hypo)
    (dolist (link link-set)
        (setf (instant-hypothesized link) t)
        (when (member link (list-later-instants link :skip-first t))
            ;;Cycles in causality can be caused by link combinations.
;            (break "Checking cyclicality correctness.")
            (dolist (bad-link link-set)
;                (format t "~%Removed ~A in form-added 3." (instant-name bad-link))
                (remove-instant bad-link)
            )
            (break "2nd link check in use.")
            (return-from form-added-hypotheses nil)
        )
    )
;    (break "After 2nd link check.")
    (my-trace-in 
        (format nil "form-added ~A" instant)
    )
    (push-all link-set (hypothesis-instants hypo))
;    (setq hypo (contract-event-timeframes hypo))
    (let ((hypotheses nil) new-hypos ret-hypos)
        (setq new-hypos (make-added-hypotheses hypo instant occ conjunct))
        (when (not (instant-final instant))
            (push instant (hypothesis-instants hypo))
        )
        (when (some #'null 
                (mapcar #'cause-timepoint 
                  (apply #'append (mapcar #'hypothesis-event-list new-hypos))
                )
              )
            (error "Cause-timepoint should never be null.")
        )
        ;    (break "New hypothesis formed.")
        (dolist (new-hypo new-hypos)
            (when (member 
                    (car (hypothesis-discrepancies hypo)) 
                    (hypothesis-discrepancies new-hypo)
                  )
                (error "Failed to eliminate discrepancy.")
            )
            (set-hypothesis new-hypo)
            (setq ret-hypos (refine-hypothesis new-hypo))
            (setq hypotheses (append hypotheses ret-hypos))
        )
        (when new-hypos
            (dolist (new-ev (reduce #'union (mapcar #'hypothesis-event-list new-hypos)))
                (when (not (member new-ev (hypothesis-event-list hypo)))
;                    (format t "~%Removed event ~A created for a child." new-ev)
                    (remove-event new-ev)
                )
            )
            (dolist (tp (reduce #'union (mapcar #'hypothesis-instants new-hypos))) 
                (when (and (not (listp (instant-name tp))) 
                           (not (member tp (hypothesis-instants hypo))))
;                    (break "Removing link instant.")
;                    (format t "~%Removed ~A in form-added." (instant-name tp))
                    (remove-instant tp)
                )
            )
        )
        (dolist (link link-set)
            (dehypothesize-instant link)
;            (format t "~%Removed ~A in form-added 2." (instant-name link))
            (remove-instant link)
            (setf (hypothesis-instants hypo) 
                (set-difference (hypothesis-instants hypo) link-set)
            )
        )
        (my-trace-out 
            (format nil "form-added ~A" instant)
        )
        hypotheses
    )
)    

(defun remove-event (ev)
;    (break "Removing event ~A" ev)
    (when (eq (cause-signature ev) 'removed)
        (error "Removing event twice.")
    )
    (let ((tp (cause-timepoint ev)))
        (setf (instant-causes tp) (remove ev (instant-causes tp)))
        (when (and (null (instant-causes tp)) (not (instant-final tp)))
;            (format t "~%Removed ~A in remove-event." (instant-name tp))
            (remove-instant tp)
        )
    )
    (dolist (pre (append (cause-preconditions ev) (cause-postconditions ev)))
        (let ((fhist (second (get-fact-info pre))))
            (setf (fact-history-changes fhist) ;;setf fhc
                (remove ev (fact-history-changes fhist) :key #'car)
            )
        )
    )
    (setf (cause-action ev) nil)
    (setf (cause-signature ev) 'removed)
    (setf (cause-preconditions ev) nil)  
    (setf (cause-postconditions ev) nil) 
    (setf (cause-hypothesized ev) nil)   
)

(defun remove-instant (tp)
;    (when (eq (instant-name tp) *global-tp-name*)
;        (break "Global tp being removed.")
;        (setq *global-tp-name* nil)
;    )
    (when (remove 'removed (instant-causes tp) :key #'cause-signature)
        (break "Instant removed before related causes.")
    )
    (when (listp (instant-name tp))
        (break "removing instant twice")
    )
    (dolist (tpn (instant-next-times tp))
        (setf (instant-prior-times tpn) (remove tp (instant-prior-times tpn)))
    )
    (dolist (tpp (instant-prior-times tp))
        (setf (instant-next-times tpp) (remove tp (instant-next-times tpp)))
    )
    (setf (instant-name tp) (list 'removed (instant-name tp)))
    (setf (instant-next-times tp) nil)
    (setf (instant-prior-times tp) nil)
)

(defun form-added-event-hypothesis (&key parent-hypothesis new-event)
    (let 
      ((hypo 
;          (if (hypothesis-discrepancies parent-hypothesis)
;              ;then
              (copy-hypothesis parent-hypothesis)
;              ;else
;              (make-hypothesis :parent-hypothesis parent-hypothesis)
;          )
       )
       (discs nil)
       (fixed-discs nil)
       (old-instants nil)
       (new-instants nil)
       (instant-map nil)
       (cur-ev nil)
;       (trash-instants nil)
      )
        (push-instant (happening-timepoint new-event) hypo)
        (setf (instant-hypothesized (happening-timepoint new-event)) t)
        (push new-event (hypothesis-event-list hypo))
        (dolist (tp (instant-prior-times (happening-timepoint new-event)))
            (pushnew (happening-timepoint new-event) (instant-next-times tp))
        )
        (dolist (tp (instant-next-times (happening-timepoint new-event)))
            (pushnew (happening-timepoint new-event) (instant-prior-times tp))
        )
        (dolist (effect (cause-postconditions new-event))
;            (break "Adding postcondition ~A" effect)
            (setq discs (history-insert-postcondition new-event effect))
;            (break "Added postcondition ~A" effect)
            (when discs (push-all discs (hypothesis-discrepancies hypo)))
            (dolist (disc (hypothesis-discrepancies hypo))
                (when (and (equal (discrepancy-fact disc) effect)
                           (or (discrepancy-fixed disc) (discrepancy-irrelevant disc))
                      )
                    (when (not (discrepancy-fixed disc))
;                       (break "Check on irrelevancy.")
                    )
                    (setq cur-ev (discrepancy-dependent disc))
                    (when (and cur-ev (typep cur-ev 'cause) (not (instant-final (cause-timepoint cur-ev)))) 
                        (setq old-instants (cdr (assoc cur-ev instant-map)))
                        (when (null old-instants)
                            (setq old-instants (list (cause-timepoint cur-ev)))
                        )
                        (setq new-instants
                            (constrain-instants 
                                old-instants 
                                (discrepancy-fact disc)
                                :find-all-links nil
                            )
                        )
                        (when (> (length new-instants) 1)
                            (break "Branching in form-added-event")
                        )
                        (when (null new-instants)
;                            (break "impossible added hypothesis.")
                            (my-trace-end (format nil "Hypothesis causality impossible."))
                            (remove-event new-event)
                            (return-from form-added-event-hypothesis nil)
                        )
                        (unless (and (eq (length new-instants) 1) 
                                     (same-time (car new-instants)
                                                (car old-instants)))
                            (push (cons cur-ev new-instants) instant-map)
                        )
                    )
                    (push disc fixed-discs)
                )
            )
        )
;        (break "removing discs")
        (when fixed-discs 
            (setf (hypothesis-discrepancies hypo)
                (set-difference (hypothesis-discrepancies hypo)
                    fixed-discs
                )
            )
        )
        (dolist (prec (cause-preconditions new-event))
            (setq discs (history-insert-precondition new-event prec))
            (when discs (push-all discs (hypothesis-discrepancies hypo)))
        )
        (let ((ret (brachiate hypo instant-map)))
            ; (when instant-map
                ; (break "testing brachiate")
            ; )
            ; (when (member new-event (mapcar #'first instant-map))
                ; (remove-event new-event)
            ; )
            (when (null ret)
                (my-trace-end "No results from brachiate.")
                (break "No results from brachiate.")
            )
            ret
        )
    )
)

(defun brachiate (hypo instant-map)
    (when (null instant-map)
        (return-from brachiate (list hypo))
    )
;    (break "brachiate")
    (let*
      ((cur-ev (caar instant-map))
       (instants (cdar instant-map))
       (instant-map (remove cur-ev instant-map :key #'first))
       (hypos nil)
      )
        (dolist (tp instants)
            (push-all
                (brachiate
                    (make-branch-hypothesis hypo cur-ev tp)
                    instant-map
                )
                hypos
            )
        )
        hypos
    )
)

(defun make-branch-hypothesis (hypo1 ev tp &aux (new-discs nil) hypo tp-old)
    (setf (hypothesis-instants hypo1) 
        (remove (cause-timepoint ev) (hypothesis-instants hypo1))
    )
    (setq hypo (copy-hypothesis hypo1))
    (setq tp-old (cause-timepoint ev))
    (setf (hypothesis-event-list hypo) (remove ev (hypothesis-event-list hypo))) 
;    (break "Before discrepancy checking in make-branch.")
    (dolist (disc (hypothesis-discrepancies hypo))
        (unless
          (and (not (instant-final (discrepancy-instant disc)))
               (or
                 (eq (car (instant-prior-times (discrepancy-instant disc))) (cause-timepoint ev))
                 (eq (car (instant-next-times  (discrepancy-instant disc))) (cause-timepoint ev))
               )
          )
            (push disc new-discs)
        )
    )
    (setf (hypothesis-discrepancies hypo) new-discs)
    (setf (cause-hypothesized ev) nil)
    (setf (instant-causes tp) (remove ev (instant-causes tp)))
    (setq ev (copy-cause ev))
    (setf (cause-hypothesized ev) t)
    (setf (cause-timepoint ev) tp)
    (setf (instant-hypothesized tp) t)
    (when (not (instant-final tp))
        ;;Means this is a newly created instant that must have back-links created.
        (dolist (otp (instant-prior-times tp))
            (when (eq tp otp)
                (break "Causality loop starting.")
            )
            (pushnew tp (instant-next-times otp))
        )
        (dolist (otp (instant-next-times tp))
            (when (eq tp otp)
                (break "Causality loop starting.")
            )
            (pushnew tp (instant-prior-times otp)) 
        )
    )
    
    (dolist (otp (instant-prior-times tp-old))
        (when (eq tp otp)
            (break "Causality loop starting.")
        )
        (if (member otp (instant-prior-times tp))
            ;then the instant was copied, and the other instants must be notified
            (pushnew tp (instant-next-times otp))
            ;else the instant collapsed to a final, and the other instants should be temporarily linked to it
            (push (make-instant-link otp tp) (hypothesis-instants hypo))
        )
    )
    (dolist (otp (instant-next-times tp-old))
        (when (eq tp otp)
            (break "Causality loop starting.")
        )
        (if (member otp (instant-next-times tp))
            ;then the instant was copied, and the other instants must be notified
            (pushnew tp (instant-prior-times otp)) 
            ;else the instant collapsed to a final, and the other instants should be temporarily linked to it
            (push (make-instant-link tp otp) (hypothesis-instants hypo))
        )
    )
    (when (bad-instant tp)
        (error "Created a bad instant.")
    )
    (dolist (effect (cause-postconditions ev))
        (let ((discs (history-insert-postcondition ev effect)))
            (when discs (push-all discs (hypothesis-discrepancies hypo)))
        )
    )
    (dolist (prec (cause-preconditions ev))
        (let ((discs (history-insert-precondition ev prec)))
            (when discs (push-all discs (hypothesis-discrepancies hypo)))
        )
    )
    (when (not (instant-final tp))
        (dehypothesize-instant tp)
    )
    (setf (cause-hypothesized ev) nil)
    (push ev (instant-causes tp))
    (when (not (eq tp (cause-timepoint ev)))
        (break "Inconsistent.")
    )
    (push ev (hypothesis-event-list hypo))
    (when (bad-instant tp)
        (error "Why a bad instant?")
    )
    (push-instant tp hypo)
    (dolist (old-ev (remove tp (instant-causes tp) :key #'cause-timepoint))
        (let ((new-ev (copy-cause old-ev)))
            (setf (cause-timepoint new-ev) tp)
            (push new-ev (instant-causes tp))
            (setf (cause-hypothesized new-ev) nil)
            (push new-ev (hypothesis-event-list hypo))
        )
    )
    (setf (instant-causes tp) 
        (remove tp (instant-causes tp) :key #'cause-timepoint :test-not #'eq)
    )
    (when (remove tp (instant-causes tp) :key #'cause-timepoint)
        (break "Wrong instant.")
    )
;    (break "Problem in brachiate.")
    hypo
)

;;Should return false when the discrepancy is no longer relevant, due to the
;;fact that a new event is interposed between the two contradictory happenings.
;;However, discrepancies 
(defun discrepancy-irrelevant (disc)
    (when (and (discrepancy-dependent disc) (listp (discrepancy-dependent disc)))
        ;;A removal dependency must be fixed.
        (return-from discrepancy-irrelevant nil)
    )
    (when (discrepancy-removals disc)
        ;;Always perform removals.
        (return-from discrepancy-irrelevant nil)
    )
    (let*
      ((finfo (get-fact-info (discrepancy-fact disc)))
       (fhist (second finfo))
       (next-change-ptr 
           (search-forward-for-event (discrepancy-instant disc) fhist :exclusive t)
       )
       (prior-change-ptr 
           (if (null (instant-prior-times (discrepancy-instant disc)))
               ;then
               (last (fact-history-changes fhist))
               ;else
               (search-back-for-event (discrepancy-instant disc) fhist :exclusive t)
           )
       )
       (tps (append (instant-prior-times (discrepancy-instant disc)) 
                    (instant-next-times (discrepancy-instant disc))
            )
       )
       (prior-tp (car (instant-prior-times (discrepancy-instant disc))))
       (next-tp (car (instant-next-times (discrepancy-instant disc))))
       (known-times (mapcar #'change-time (show-changes (fact-history-changes fhist))))
       recent-changes
      )
        (when (instant-final (discrepancy-instant disc))
            (when (not (member 
                    (instant-next-moment (discrepancy-instant disc))
                    known-times
                  ))
;                (break "Irrelevant final instant.")
            )
;            (break "Irrelevant final instant 2.")
            (return-from discrepancy-irrelevant
                (not
                    (member 
                        (instant-next-moment (discrepancy-instant disc))
                        known-times
                    )
                )
            )
        )
        (when (cdr (instant-next-times (discrepancy-instant disc)))
            (error "Discrepancy has multiple next-times.")
        )
        (when (cdr (instant-prior-times (discrepancy-instant disc)))
            (error "Discrepancy has multiple prior-times.")
        )
        (when (and (instant-final next-tp) (not (instant-observation next-tp)))
            (setq next-tp (instant-next-moment next-tp))
        )
        (when (not (member next-tp known-times))
            ;;The events in the history no longer contain the discrepancy times.
;            (break "New irrelevancy condition on next.")
            (return-from discrepancy-irrelevant t)
        )
        (when (and prior-tp (not (member prior-tp known-times)))
          (cond 
              ((and (eq prior-tp (happening-timepoint *initial-obs*))
                    (fact-history-hidden fhist)
               )
                ;;It's allowed to refer to the initial observation time
                ;;as the time when a literal was assumed false, even though
                ;;it doesn't appear.
                
;                (when (not (third finfo))
;                    (error "Should have assumed false.")
;                ) This is legal when the fact is given, so removed.
;                (break "Contradiction with standard assumption of false is relevant.")
;                (return-from discrepancy-irrelevant nil)
              )
              ;;The events in the history no longer contain the discrepancy times.
              ((and (not (eq (instant-next-moment prior-tp) 'none))
                    (null 
                       (remove 'none
                          (remove 
                              (instant-next-moment prior-tp)
                              (fact-history-changes fhist)
                              :key #'change-time
                              :test-not #'eq
                          )
                          :key #'third
                       )
                    )
               )
;                  (break "Changing tp.")
                  (setq prior-change-ptr (search-back-for-event (instant-next-moment prior-tp) fhist))
              )
              (t  
;                  (break "New irrelevancy condition on prior.")
                  (return-from discrepancy-irrelevant t)
              )
          )
        )
        (when (and (discrepancy-dependent disc) (listp (discrepancy-dependent disc)))
            (if (null next-change-ptr)
                ;then
                (setq recent-changes (fact-history-changes fhist))
                ;else
                (setq recent-changes (rest next-change-ptr))
            )
            (when (not (eq (third finfo) (get-value-after-changes recent-changes)))
;                (break "Irrelevant final instant 3.")
                (return-from discrepancy-irrelevant nil)
            )
        )
        (when (not (null next-change-ptr))
            (let ((initial-tp (change-time (first next-change-ptr))))
                (when (and (not (eq (instant-prior-moment initial-tp) 'none))
                           (in-interval initial-tp (discrepancy-instant disc))
                           (eq (third finfo) (get-value-before-recent-changes next-change-ptr))
                      )
;                    (break "New discrepancy-irrelevant criterion.")
                    (return-from discrepancy-irrelevant t)
                )
            )
            (loop while (and (cdr next-change-ptr)
                             (eq (change-time (first next-change-ptr))
                                 (change-time (second next-change-ptr))
                             )
                        ) do
                (setq next-change-ptr (rest next-change-ptr))
            )
;            (setq next-change-ptr (rest next-change-ptr))
            (when (null next-change-ptr)
                (error "confused about where discrepancy is.")
            )
        )
        (when (null next-change-ptr)
            (setq next-change-ptr (fact-history-changes fhist))
        )
        (when (not (tailp prior-change-ptr next-change-ptr))
            (error "Violated basic assumption of history.")
        )
        (when (not (eq next-change-ptr prior-change-ptr))
            (setq next-change-ptr (cdr next-change-ptr))
        )
        (loop while (not (eq next-change-ptr prior-change-ptr)) do
;            (break "irrelevancy test")
            (when (and (is-hypothesized (caar next-change-ptr))
                       (not 
                           (member 
                              (get-constraining-earlier-time (car next-change-ptr)) 
                              tps
                           )
                       )
                       ;;The or condition checks for situations where a relevant
                       ;;event occurs just after the beginning of the discrepancy
                       ;;interval, but does not change the value of the fact
                       (or (null prior-tp)
                           (not (eq (change-time (car next-change-ptr)) 
                                    (instant-next-moment prior-tp)
                                )
                           )
                           (not (eq (third (car next-change-ptr)) 'none))
                       )
                  )
;                (break "old irrelevancy condition")
                (when (not (instant-earlier-than 
                             (get-constraining-earlier-time (car next-change-ptr)) 
                             next-tp
                           )
                      )
                    (error "failure to find interposing instant.")
                )
;                (break "old irrelevancy condition")
                (return-from discrepancy-irrelevant t)
            )
            (setq next-change-ptr (cdr next-change-ptr))
        )
;        (break "Trivial exit")
        nil
   )
)

(defun discrepancy-fixed (disc)
    (when (listp (discrepancy-instant disc))
        (error "Should not find a discrepancy with a removed instant.")
    )
    (when 
        (some #'listp 
            (mapcar #'instant-name
                (append (instant-prior-times (discrepancy-instant disc))
                        (instant-next-times  (discrepancy-instant disc))
                )
            )
        )
        (error "Should not find a removed instant adjacent to discrepancy.")
    )
    (when (discrepancy-removals disc) 
        (return-from discrepancy-fixed nil)
    )
    (let*
      ((finfo (get-fact-info (discrepancy-fact disc)))
       (fhist (second finfo))
       (next-change-ptr 
           (search-forward-for-event (discrepancy-instant disc) fhist :exclusive t)
       )
       (prior-change-ptr 
           (if (null (instant-prior-times (discrepancy-instant disc)))
               ;then
               (last (fact-history-changes fhist))
               ;else
               (search-back-for-event (discrepancy-instant disc) fhist :exclusive t)
           )
       )
       cur-pre-val cur-post-val next-val  
       (new-tp nil) (last-tp nil) nvpre nvpost
       last-constraining-tp
      )
        (when (and (discrepancy-dependent disc) (instant-final (discrepancy-instant disc)))
            (return-from discrepancy-fixed
                (eq (third finfo)
                    (get-value-after-changes
                        (search-back-for-event (discrepancy-instant disc) fhist)
                    )
                )
            )
        )
        (when (and (not (discrepancy-dependent disc)) (instant-final (discrepancy-instant disc)))
            (let ((cur-before-val
                    (get-value-after-changes
                        (search-back-for-event (discrepancy-instant disc) fhist)
                    )
                  )
                  (cur-after-val
                    (get-value-before-changes
                        (search-forward-for-event (discrepancy-instant disc) fhist
                            :exclusive t
                        )
                    )
                  )
                 )
                (return-from discrepancy-fixed (eq cur-before-val cur-after-val))
            )
        )
        (when (null next-change-ptr)
            (setq next-change-ptr (cons nil (fact-history-changes fhist)))
        )
        (when (null prior-change-ptr)
            ;;fhist is hidden and initially-true is not hypothesized
            (when (not (fact-history-hidden fhist))
               (error "There should always be a prior event for a non-hidden fact.")
            )
            (when (is-hypothesized (first (first (last (fact-history-changes fhist)))))
                (error "The initially true fact should have functioned as a prior.")
            )
        )
        (when (first next-change-ptr)
            (let ((first-tp (change-time (first next-change-ptr))))
                (when (and (instant-final first-tp)
                           (not (instant-observation first-tp))
                           (not (instant-final (discrepancy-instant disc)))
                           (in-interval first-tp (discrepancy-instant disc))
                      )
                    (push nil next-change-ptr)
;                    (break "Final instant problem.")
                )
            )
        )
        (when (null (car next-change-ptr))
            (setq cur-pre-val 'unk)
            (setq cur-post-val 'nada)
            (setq next-val cur-pre-val)
            (setq last-tp nil)
        )
        (when (not (tailp prior-change-ptr next-change-ptr))
            (when (bad-instant (discrepancy-instant disc))
                (return-from discrepancy-fixed nil)
            )
            (error "Violated basic assumption of history.")
        )
        (loop while 
                (and (not (null prior-change-ptr))
                     (eq (change-time (first prior-change-ptr))
                         (change-time (second prior-change-ptr))
                     )
                ) do
            (setq prior-change-ptr (rest prior-change-ptr))
        )
        
        (when (not (null (car next-change-ptr)))
            (loop while (not (is-hypothesized (caar next-change-ptr))) do
                (setq next-change-ptr (rest next-change-ptr))
            )
            (setq cur-pre-val (second (first next-change-ptr)))
            (setq cur-post-val (third (first next-change-ptr)))
            (setq last-tp (change-time (first next-change-ptr)))
            (if (eq last-tp (change-time (second next-change-ptr)))
                ;then
                (setq next-val 'unk)
                ;else
                (setq next-val cur-pre-val)
            )
            (loop while 
                    (and (not (null (car next-change-ptr)))
                         (eq (change-time (first next-change-ptr))
                             (change-time (second next-change-ptr))
                         )
                    ) do
                (setq next-change-ptr (rest next-change-ptr))
            )
        )
;        (break "before dependent check.")
        (when (and (discrepancy-dependent disc) (not (eq cur-pre-val (third finfo))))
            ;;When dependents are involved, the discrepancy can not be fixed 
            ;;without the correct value being true at the end.
            (if (and (not (null (caar next-change-ptr)))
                     (member (happening-timepoint (caar next-change-ptr)) 
                             (instant-next-times (discrepancy-instant disc))
                     )
                )
                ;then
                (progn
;                    (break "dependency nil")
                    (return-from discrepancy-fixed nil)
                )
                ;else
                (setq next-val (third finfo)
                      cur-pre-val (third finfo)
                )
                
            )
        )
;        (break "After dependent check.")
        (loop while (not (eq next-change-ptr prior-change-ptr)) do
            (setq next-change-ptr (cdr next-change-ptr))
            (when (and next-change-ptr (is-hypothesized (first (first next-change-ptr))))
                (setq new-tp (change-time (first next-change-ptr)))
;                (break "Inside fixed check loop.")
                (if (eq last-tp new-tp)
                    ;then
                    (progn
                        (setq nvpre (second (first next-change-ptr)))
                        (setq nvpost (third (first next-change-ptr)))
                        (if (or (eq cur-pre-val 'unk) (eq nvpre 'unk))
                            ;then
                            (setq cur-pre-val nvpre)
                            ;else
                            (when (not (eq cur-pre-val nvpre))
                                (error "Inconsistent simultaneous precondition values.")
                            )
                        )
                        (if (or (eq cur-post-val 'none) (eq nvpost 'none))
                            ;then
                            (setq cur-post-val nvpost)
                            ;else
                            (when (not (eq cur-post-val nvpost))
                                (error "Inconsistent simultaneous postcondition values.")
                            )
                        )
                    )
                    ;else
                    (progn
                        (setq next-val cur-pre-val)
                        (setq cur-post-val (third (first next-change-ptr)))
                        (setq cur-pre-val (second (first next-change-ptr)))
;                        (when (and (not 
;                                     (eq new-tp 
;                                        (car (instant-prior-times (discrepancy-instant disc)))
;                                     )
;                                   )
;                                   (eq last-constraining-tp 
;                                       (car (instant-prior-times (discrepancy-instant disc)))
;                                   )
;                              )
                        (when (eq last-constraining-tp 
                                  (car (instant-prior-times (discrepancy-instant disc)))
                              )
                            ;;When we've gone past the real start event, stop
                            ;;the loop with a success. If a constraining point matches,
                            ;;the discrepancy is either fixed or irrelevant, since that
                            ;;constraining point is either between the intended start
                            ;;point and ending point, or it existed when the 
                            ;;discrepancy was formed, and therefore must be the start
                            ;;point
                            (setq next-val 'unk)
                            (setq prior-change-ptr next-change-ptr)
                        )
                    )
                )
;                (break "Testing.")
                (let ((post-val (get-value-after-changes next-change-ptr)))
                    (when (and (not (eq post-val next-val))
                               (not (eq next-val 'unk))
                               (not (eq post-val 'none)) 
                          )
;                        (break "nil in discrepancy-fixed")
                        (return-from discrepancy-fixed nil)
                    )
                )
                (setq last-tp new-tp)
                (setq last-constraining-tp (get-constraining-earlier-time (car next-change-ptr)))
            )
        )
;        (break "About to exit discrepancy-fixed.")
        (if (or (null prior-change-ptr) (null (instant-prior-times (discrepancy-instant disc))))
            ;then
            (or (eq cur-pre-val nil) (eq cur-pre-val 'unk) 
                (and last-constraining-tp
                    (eq last-constraining-tp 
                        (car (instant-prior-times (discrepancy-instant disc)))
                    )
                )
            )
            ;else
            t
        )
    )
)

(defun constrain-instants (instants fact &key (find-all-links t))
    (reduce #'append (mapcar+ #'constrain-instant instants fact :find-all-links find-all-links))
)

(defun constrain-instant (instant fact &key (find-all-links t))
    (when (instant-fake instant)
        (error "Fake instant in use.")
    )
    (let*
      ((finfo (get-fact-info fact))
       (fact (first finfo))
       (hist (second finfo))
       (val (third finfo))
       (prior-event-ptr nil)
       (next-event-ptr nil)
       (cur-ptr nil)
       (new-instants nil)
       (prior-new-time nil)
       (new-time nil)
       (last-time nil)
       (prior-time nil)
;;;       (open-ended-start nil)
       cur-val prior-val
      )
        (when (null hist) 
            (setq hist (make-unobserved-fact-history fact))
        )
        
        ;;A final instant isn't an interval to be subdivided.
        (when (or (instant-final instant) 
                  (and find-all-links
                       (instant-causes instant)
                       (intersection (instant-causes instant) 
                                     (mapcar #'first (fact-history-changes hist))
                                     :test #'equal
                       )
                  )
              )
            (if (eq val (get-value-before-time hist instant))
                ;then
                (return-from constrain-instant (list instant))
                ;else
                (return-from constrain-instant nil)
            )
        )
        ; (when (and find-all-links (instant-causes instant))
            ; (break "Continuing to break up new instant.")
        ; )
        (when (notany #'is-hypothesized (mapcar #'first (fact-history-changes hist)))
            (if val
                ;then
                (return-from constrain-instant nil)
                ;else
                (return-from constrain-instant (list instant))
            )
        )
        (when (instant-prior-times instant)
            (setq prior-event-ptr 
               (search-back-for-event 
                   instant 
                   hist
                   :exclusive t
                   :search-all find-all-links
               )
            )
        )

        (setq next-event-ptr
           (search-forward-for-event 
               instant
               hist
               :exclusive t
               :search-all find-all-links
           )
        )

        (setq last-time nil)
        (if (null next-event-ptr)
            ;if nothing is later than this, the whole list should be what is 
            ;found
            ;Normally, the first event to be inspected is outside the scope
            ;of the instant, but not in this case.
            (progn
                (setq next-event-ptr (fact-history-changes hist))
                (setq cur-ptr next-event-ptr)
            )
            ;else, move past the last instant
            (let ((initial-tp (change-time (first next-event-ptr))))
                (when (and (not (eq (instant-prior-moment initial-tp) 'none))
                           (in-interval initial-tp instant)
                           (not (eq val (get-value-before-changes next-event-ptr)))
                      )
;                    (break "before change to last-time")
                    (setq last-time (instant-prior-moment initial-tp))
                )
                
                (setq cur-ptr (rest next-event-ptr))
                (loop while (eq initial-tp (change-time (first cur-ptr))) do
                    (setq cur-ptr (rest cur-ptr))
                )
            )
        )

        (when (not (tailp prior-event-ptr next-event-ptr))
            (error "Next-event-ptr or prior-event-ptr didn't work.")
        )
        
;        (break "Before loop in constrain-instant.")

        (loop while (not (eq cur-ptr prior-event-ptr)) do
            (let ((cur-ev (caar cur-ptr)))
                (setq new-time (happening-timepoint cur-ev))
;                (break "In outer loop.")
                (when (and ;; This event breaks up the instants
                           (if find-all-links
                               ;then
                               (hap-orderable cur-ev)
                               ;else
                               (is-hypothesized cur-ev)
                           ) 
                           ;; ... and this isn't the instant we're breaking up?
                           (not (eq (happening-timepoint cur-ev) instant))
                           ;; ... and we didn't just consider this time as a breaker
                           (not (eq new-time prior-new-time))
                           ;; ... and something else that doesn't make sense
                           ; (or (null last-time)
                               ; (not (eq new-time (instant-next-moment last-time)))
                           ; )
                      )
;                    (break "In loop.")
                    (setq prior-new-time new-time)  
                    
                    (setq cur-val (get-value-after-changes cur-ptr))
                    (setq prior-time (instant-prior-moment new-time))
                    (when (not (in-interval prior-time instant))
                        (setq prior-time 'none)
                    )
                    (setq prior-val 'unk)
                    (when (not (eq prior-time 'none))
                        (setq prior-val (get-value-before-changes cur-ptr))
                    )
                    (when (and (not (instant-final new-time))
                               (not (instants-ordered new-time instant))
                               (null (instant-causes instant))
                          )
                        (setq prior-val (get-value-before-changes cur-ptr))
                    )
                    (when (eq val prior-val)
                        ;; We must keep all instants in histories 
                        ;; non-intersecting, but this particular instant,
                        ;; already found in the history, is a plausible
                        ;; time. It can't be conflated with others without
                        ;; risking later intersection of times in a history.
;                        (break "Adding to new-instants 1.")
                        (if (instant-final new-time)
                            ;then
                            (push new-time new-instants)
                            ;else
                            (push (combine-constraints instant new-time) new-instants)
                        )
                    )
                    (when (eq cur-val val)
;                        (break "Adding to new-instants 2.")
                        (push
                            (make-constrained-instant new-time last-time instant)
                            new-instants
                        )
                    )
                    (if (not (eq prior-time 'none))
                        ;then
                        (setq last-time prior-time)
                        ;else
                        (setq last-time new-time)
                    )
                )
            )
            (setq cur-ptr (rest cur-ptr))
        )
        
        ;; Avoid adding a time interval before initial time.
        (when (not (and (null (instant-prior-times instant)) 
                        (eq last-time *initial-time*)
                   )
              )
            (when (eq val (get-value-after-changes cur-ptr))
;	        (break "Adding to new-instants 3.")
                (push 
                    (make-constrained-instant nil last-time instant)
                    new-instants
                )
            )
        )
        (remove nil new-instants)
    )
)

;;Unlike get-value-after-changes, this one only returns the value true *before*
;;the most *recent* timepoint, if known. Otherwise, it's unknown, returns 'unk.
(defun get-value-before-recent-changes (changes)
    (when (null changes)
        (return-from get-value-before-recent-changes 'unk)
    )
    (let ((tp (happening-timepoint (caar changes))))
        (dolist (ch changes)
            (when (not (eq tp (happening-timepoint (car ch))))
                (return-from get-value-before-recent-changes 'unk)
            )
            (when (and (is-hypothesized (car ch)) (not (eq (second ch) 'unk)))
                (return-from get-value-before-recent-changes (second ch))
            )
        )
    )
)

(defun get-value-before-changes (changes &aux val)
    ;; If nothing is in changes, nothing happened before, so closed world assumption applies.
    (when (null changes) (return-from get-value-before-changes nil))
    (setq val (get-value-before-recent-changes changes))
    (when (not (eq val 'unk)) 
        (return-from get-value-before-changes val)
    )
    (let ((tp (change-time (first changes))))
        (loop while (eq tp (change-time (first changes))) do
            (setq changes (rest changes))
        )
        (get-value-after-changes changes)
    )
)

(defun get-value-after-changes (changes)
    (loop while (and changes (not (is-hypothesized (first (first changes))))) do
        (setq changes (rest changes))
    )
    (when (null changes)
        (return-from get-value-after-changes nil)
    )
    (let ((tp (happening-timepoint (caar changes))) new-tp ch)
        (setq ch (first changes))
;        (when (and (instant-final tp) (null (instant-observation tp)))
        (when (null (instant-observation tp))
            (setq new-tp tp)
            (loop while (and (eq tp new-tp) (eq (third ch) 'none)) do
                (when (and (cause-hypothesized (caar changes))
                           (not (eq (third (first changes)) 'none))
                      )
                    (setq ch (first changes))
                )
                (setq changes (rest changes))
                (if (null changes)
                    ;then
                    (setq new-tp nil)
                    ;else
                    (setq new-tp (happening-timepoint (first (first changes))))
                )
            )
        )
        (get-value-after-change ch)
    )
)

(defun get-value-after-change (change)
    (if (eq (third change) 'none)
        ;then
        (second change)
        ;else
        (third change)
    )
)

(defun old-constrain-instant (instant fact)
    (let*
      ((val (if (eq (first fact) 'not) nil 't))
       (fact (if val fact (second fact)))
       (hist (find-fact-history fact))
       (prior-event-ptr nil)
       (next-event-ptr nil)
       (cur-ptr nil)
       (new-instants nil)
       (cur-endpoint nil)
       (cur-startpoint nil)
       (open-ended-start nil)
       cur-val
      )
        (when (null hist) 
            (setq hist (make-unobserved-fact-history fact))
        )
        (when (instant-prior-times instant)
            (setq prior-event-ptr 
               (search-back-for-event 
                   instant 
                   hist
                   :exclusive t
               )
            )
        )
        (when (or (null (instant-prior-times instant)) (null prior-event-ptr))
            (setq prior-event-ptr (last (fact-history-changes hist)))
            (when (not val)
                (let ((first-event (caar prior-event-ptr)))
                    (when (and (not (eq first-event *initial-obs*))
                               (not (eq (happening-timepoint first-event) *initial-time*))
                          )
                        (push
                            (get-initial-instant
                                :prior-time (observation-timepoint *initial-obs*)
                                :next-time (happening-timepoint (caar prior-event-ptr))
                            )
                            new-instants
                        )
                    )
                )
            )
            (setq open-ended-start t)
        )
        ; (when (null prior-event-ptr)
            ; (when (not (fact-history-hidden hist))
                ; (break "No prior-event-ptr.")
            ; )
            ; (if val
                ; ;hidden has never been seen to be true
                ; (return-from constrain-instant nil)
                ; ;hidden assumed to be false
                ; (return-from constrain-instant (list instant))
            ; )
        ; )
        (setq next-event-ptr
           (search-forward-for-event 
               instant
               hist
               :exclusive t
           )
        )
        (when (null next-event-ptr)
            (when open-ended-start
                ;there's nothing at all in the list
                (when (not (fact-history-hidden hist))
                    (error "Non-hidden has no changes.")
                )
                (if val
                    ;hidden has never been seen to be true
                    (return-from old-constrain-instant nil)
                    ;hidden assumed to be false
                    (return-from old-constrain-instant (list instant))
                )
            )
            ;if nothing is later than this, the whole list should be what is 
            ;found
            (setq next-event-ptr (cons nil (fact-history-changes hist)))
        )
        (when (not (tailp prior-event-ptr next-event-ptr))
            (error "Next-event-ptr or prior-event-ptr didn't work.")
        )
        (setq cur-ptr next-event-ptr)
;        (when (eq (caar cur-ptr) (second interval))
;            (setq cur-ptr (rest cur-ptr))
;        )
        (setq cur-endpoint nil)
        (setq cur-startpoint nil)
        (loop while (not (eq cur-ptr prior-event-ptr)) do
            (setq cur-ptr (rest cur-ptr))
            (when (is-hypothesized (first (first cur-ptr)))
                (setq cur-val (third (first cur-ptr)))
                (when (eq cur-val 'none)
                    (setq cur-val (second (first cur-ptr)))
                )
                (if (not (eq cur-val val))
                    ;then value after next event does not satisfy, it may be the 
                    ;end of an interval
                    (progn
                        (when (not (null cur-startpoint))
                            (push 
                                (make-constrained-instant 
                                    cur-startpoint cur-endpoint instant
                                )
                                new-instants
                            )
                            (setq cur-startpoint nil)
                        )
                        (setq cur-startpoint nil)
                        (setq cur-endpoint 
                            (happening-timepoint (first (first cur-ptr)))
                        )
                    )
                    ;else value after next event does satisfy, may be the
                    ;start of an interval
                    (setq cur-startpoint
                        (happening-timepoint (first (first cur-ptr)))
                    )
                )
            )
#|              (break "Current event: ~A Interval Start: ~A End: ~A" 
                (first cur-ptr) interval-start interval-end
            )
|#
        )
        (when cur-startpoint
            ;;Special case when the earliest time found was still in the
            ;;interval, and the value is true, then the last startpoint found
            ;;is relevant, because we assume the value was false before.
            (if (and val open-ended-start)
                ;then
                (push 
                    (make-constrained-instant 
                        cur-startpoint cur-endpoint instant
                    )
                    new-instants
                )
                ;else
                (push 
                    (make-constrained-instant 
                        nil cur-endpoint instant
                    )
                    new-instants
                )
            )
        )
        (remove nil new-instants)
    )
)

(defun make-constrained-instant (startpoint endpoint instant)
    (when (eq endpoint instant)
        (when startpoint
            (error "Didn't think this was possible.")
        )
        (return-from make-constrained-instant instant)
    )
    (when (and endpoint (member endpoint (list-later-instants instant)))
        (error "This throws off the make-constrained algorithm.")
    )
    (when (instant-final instant)
        (error "A final instant cannot be constrained.")
    )
    (setq instant (copy-instant instant :derivative nil))
    ;;I'm not entirely sure this is kosher, but I've decided to defer this particular rabbit hole.
    ; (when (remove instant (instant-causes instant) :key #'cause-timepoint)
        ; (break "Wrong instant.")
    ; )
    
    (when startpoint
        (setf (instant-prior-times instant)
            (adjoin startpoint
                (set-difference 
                    (instant-prior-times instant)
                    (list-earlier-instants startpoint)
                )
            )
        )
    )
    (when endpoint
        (setf (instant-next-times instant)
            (adjoin endpoint
                (set-difference 
                    (instant-next-times instant)
                    (list-later-instants endpoint)
                )
            )
        )
        (when (member instant (instant-next-times instant))
            (error "causality loop")
        )
    )

    (let ((tp-set
            (intersection 
                (mapcar #'instant-prior-moment (instant-next-times instant)) 
                (instant-prior-times instant)
            )
         ))
        (when (cdr tp-set)
            (error "Causality violated a lot.")
        )
        (when tp-set
            (return-from make-constrained-instant
                (instant-next-moment (car tp-set))
            )
        )
    )
    (let ((final-tp (should-be-final instant)))
        (when final-tp
            (return-from make-constrained-instant final-tp)
        )
    )
    (if (bad-instant instant)
        ;then
        nil
        ;else
        instant
    )
)

(defun combine-constraints (tp1 tp2)
    (dolist (otp (instant-prior-times tp2))
        (setq tp1 (make-constrained-instant otp nil tp1))
        (when (null tp1)
            (return-from combine-constraints nil)
        )
        (when (instant-final tp1)
            (if (in-interval tp1 tp2)
                ;then
                (return-from combine-constraints tp1)
                ;else
                (return-from combine-constraints nil)
            )
        )
    )
    (dolist (otp (instant-next-times tp2))
        (setq tp1 (make-constrained-instant nil otp tp1))
        (when (null tp1)
            (return-from combine-constraints nil)
        )
        (when (instant-final tp1)
            (if (in-interval tp1 tp2)
                ;then
                (return-from combine-constraints tp1)
                ;else
                (return-from combine-constraints nil)
            )
        )
    )
    (setf (instant-causes tp1) (instant-causes tp2))
    tp1
)

(defun make-unobserved-fact-history (fact)
    (let ((hist (make-fact-history)))
        (push (list fact hist) (history-fact-histories *history*))
        (when (member (first fact) (domain-hidden *domain-model*))
            (setf (fact-history-hidden hist) t)
            (form-hidden-initial-cause fact hist)
            (return-from make-unobserved-fact-history hist)
        )
        (dolist (ob (reverse *observations*))
            (push (list ob nil nil) (fact-history-changes hist))
        )
        hist
    )
)
            
(defun change-time (change)
    (if (null change)
        ;then 
        nil
        ;else
        (happening-timepoint (first change))
    )
)

(defgeneric search-forward-for-event (ev hist &key exclusive search-all)
)

(defmethod search-forward-for-event ((hap happening) fhist &key (exclusive nil) (search-all nil))
    (when (and (not exclusive) (not (is-hypothesized hap)))
        (setq exclusive t)
    )
    (when (not exclusive)
        (let ((ret-list (member hap (fact-history-changes fhist) :key #'first)))
            (when ret-list
                (return-from search-forward-for-event ret-list)
            )
        )
    )
    (search-forward-for-event (happening-timepoint hap) fhist :exclusive t :search-all search-all)
)


(defmethod search-forward-for-event ((tp instant) fhist &key (exclusive nil) (search-all nil))
    (let 
      ((later-evs nil)
       (ordered-fact-evs (fact-history-changes fhist))
       (ev-list nil)
       (the-ev nil)
      )
        (if (not exclusive)
            ;then
            (let ((ret (member tp ordered-fact-evs :key #'change-time)))
                (loop while (and ret (not search-all) (not (is-hypothesized (caar ret)))) do
                    (setq ret (rest ret))
                )
                (when (or (null ret) 
                          (not (eq tp (change-time (car ret))))
                      )
                    (setq ret (search-forward-for-event tp fhist :exclusive t :search-all search-all))
                )
                ret
            )
            ;else
            (progn
                (setq later-evs 
                    (list-later-happenings tp 
                        :remove-hypotheticals (not search-all)
                        :hidden (fact-history-hidden fhist)
                    )
                )
                (setq ev-list (mapcar #'first ordered-fact-evs)) 
                (setq the-ev (last-in-common ev-list later-evs))
                ; (when (not (eq the-ev
                               ; (car
                                ; (find-if 
                                    ; #'(lambda (e2) (member e2 later-evs)) 
                                    ; ordered-fact-evs
                                    ; :key #'first
                                    ; :from-end t
                                ; )
                               ; )
                           ; )
                      ; )
                    ; (break "Failed mimicry.")
                ; )
;                (break "Successful mimicry?")
                (if the-ev
                    ;then
                    (member 
;                        (change-time the-ev) 
                        (happening-timepoint the-ev) 
                        ordered-fact-evs
                        :key #'change-time
                        :test #'eq
                    )
                    ;else
                    nil
                )
            )
        )
    )
)

(defun last-in-common (list1 list2 &aux ret)
    (when (null list1) (return-from last-in-common nil))
    (setq ret (last-in-common (cdr list1) list2))
    (if (null ret)
        ;then
        (find (car list1) list2)
        ;else
        ret
    )
)



(defgeneric search-back-for-event (ev hist &key exclusive search-all)
)

(defmethod search-back-for-event ((hap happening) fhist &key (exclusive nil) (search-all nil))
    (when (and (not exclusive) (not (is-hypothesized hap)))
        (setq exclusive t)
    )
    (when (not exclusive)
        (let ((ret-list (member hap (fact-history-changes fhist) :key #'first)))
            (when ret-list
                (return-from search-back-for-event ret-list)
            )
        )
    )
    (search-back-for-event (happening-timepoint hap) fhist :exclusive t :search-all search-all)
)

(defmethod search-back-for-event ((tp instant) fhist &key (exclusive nil) (search-all nil))
    (let 
      ((earlier-evs nil)
       (ordered-fact-evs (fact-history-changes fhist))
       (ev-list nil)
       (the-ev nil)
      )
        (if (not exclusive)
            ;then
            (let ((ret (member tp ordered-fact-evs :key #'change-time)))
                (loop while (and ret (not search-all) (not (is-hypothesized (caar ret)))) do
                    (setq ret (rest ret))
                )
                (when (or (null ret) 
                          (not (eq tp (change-time (car ret))))
                      )
                    (setq ret (search-back-for-event tp fhist :exclusive t :search-all search-all))
                )
                ret
            )
            ;else
            (progn
                (setq earlier-evs 
                    (list-earlier-happenings tp 
                        :remove-hypotheticals (not search-all)
                        :hidden (fact-history-hidden fhist)
                    )
                )
                (setq ev-list (mapcar #'first ordered-fact-evs)) 
                (setq the-ev (first-in-common ev-list earlier-evs))
                ; (when (not (eq the-ev
                               ; (car
                                ; (find-if 
                                    ; #'(lambda (e2) (member e2 earlier-evs)) 
                                    ; ordered-fact-evs
                                    ; :key #'first
                                ; )
                               ; )
                           ; )
                      ; )
                    ; (break "Failed mimicry.")
                ; )
;                (break "Successful mimicry?")
                (if the-ev
                    ;then
                    (member 
;                        (change-time the-ev) 
                        (happening-timepoint the-ev) 
                        ordered-fact-evs
                        :key #'change-time
                        :test #'eq
                    )
                    ;else
                    nil
                )
            )
        )
    )
)

(defun first-in-common (list1 list2 &aux ret)
    (loop while list1 do
          (setq ret (find (car list1) list2 :test #'eq))
          (when ret
              (return-from first-in-common ret)
          )
          (setq list1 (cdr list1))
    )
;    (when (null list1) (return-from first-in-common nil))
;    (setq ret (find (car list1) list2))
;    (if (null ret)
;        ;then
;        (first-in-common (cdr list1) list2)
;        ;else
;        ret
;    )
)

(defun get-happenings (instant &key (remove-hypotheticals t) &aux (ret-list nil))
    ;;Matt: This function has been optimized for Allegro.
    (if (instant-observation instant)
        ;then
        (list (instant-observation instant))
        ;else
        (progn
            (if remove-hypotheticals
                ;then
                (dolist (c (instant-causes instant))
                    (when (cause-hypothesized c)
                        (push c ret-list)
                    )
                )
                ;else
                (dolist (c (instant-causes instant))
                    (when (or (cause-hypothesized c) (instant-final (cause-timepoint c)))
                        (push c ret-list)
                    )
                )
            )
            ret-list
        )
        ; (if remove-hypotheticals
            ; ;then
            ; (remove-if-not #'cause-hypothesized (instant-causes instant))
            ; ;else
            ; (remove-if-not #'hap-orderable (instant-causes instant))
        ; )
    )
)

(defun list-later-happenings (instant &key (remove-hypotheticals t) (hidden t))
    (apply #'append
        (mapcar+ #'get-happenings 
            (list-later-instants
                instant
                :hidden hidden
                :skip-first t
            )
            :remove-hypotheticals remove-hypotheticals
        )
    )
)

; (defun list-later-instants-old (instant &key (hidden t))
    ; (when (not (instant-hypothesized instant))
        ; (return-from list-later-instants-old nil)
    ; )
    ; (if (and (not hidden) (instant-observation instant))
        ; ;then
        ; (list instant)
        ; ;else
        ; (let ((nexts (instant-next-times instant)))
            ; (cons instant
                ; (reduce #'union 
                    ; (mapcar+ #'list-later-instants-old nexts :hidden hidden) 
                    ; :initial-value nil
                ; )
            ; )
        ; )
    ; )
; )

(defun list-later-instants (instant &key (hidden t) (skip-first nil))
    (when (and (not skip-first) (not (instant-hypothesized instant)))
        (return-from list-later-instants nil)
    )
    
    (let ((non-expanded (list instant)) (expanded nil))
        (when skip-first 
            (setq non-expanded 
                (remove-if-not #'instant-hypothesized 
                               (instant-next-times instant)
                )
            )
        )
        (loop while non-expanded do
            (push (car non-expanded) expanded)
            (dolist (tp (instant-next-times (pop non-expanded)))
                (when (and (instant-hypothesized tp)
                           (not (member tp expanded))
                           (not (member tp non-expanded))
                      )
                    (if (and (not hidden) (instant-observation tp))
                        ;then
                        (push tp expanded)
                        ;else
                        (push tp non-expanded)
                    )
                )
            )
        )
        expanded
    )
)

(defun list-earlier-happenings (instant &key (remove-hypotheticals t) (hidden t))
    (apply #'append
        (mapcar+ #'get-happenings 
            (list-earlier-instants 
                instant
                :hidden hidden
                :skip-first t
            )
            :remove-hypotheticals remove-hypotheticals
        )
    )
)

; (defun list-earlier-instants (instant &key (hidden t))
    ; (when (not (instant-hypothesized instant))
        ; (return-from list-earlier-instants nil)
    ; )
    ; (when (and (not hidden) (instant-observation instant))
        ; (list instant)
    ; )
    ; (let ((priors (instant-prior-times instant)))
        ; (cons instant
            ; (reduce #'union 
                ; (mapcar #'list-earlier-instants priors)
                ; :initial-value nil
            ; )
        ; )
    ; )
; )

(defun list-earlier-instants (instant &key (hidden t) (skip-first nil))
    (when (and (not skip-first) (not (instant-hypothesized instant)))
        (return-from list-earlier-instants nil)
    )
    
    (let ((non-expanded (list instant)) (expanded nil))
        (when skip-first 
            (setq non-expanded 
                (remove-if-not #'instant-hypothesized 
                               (instant-prior-times instant)
                )
            )
        )
        (loop while non-expanded do
            (push (car non-expanded) expanded)
            (dolist (tp (instant-prior-times (pop non-expanded)))
                (when (and (instant-hypothesized tp)
                           (not (member tp expanded))
                           (not (member tp non-expanded))
                      )
                    (if (and (not hidden) (instant-observation tp))
                        ;then
                        (push tp expanded)
                        ;else
                        (push tp non-expanded)
                    )
                )
            )
        )
        expanded
    )
)

(defun test-constrain ()
    (setq *history* (make-history))
    (let 
      ((hist1 (make-fact-history))
       (t1 (make-instant :name 't1))
       (t2 (make-instant :name 't2))
       (t3 (make-instant :name 't3))
       (t4 (make-instant :name 't4))
       (t5 (make-instant :name 't5))
       (t6 (make-instant :name 't6))
       (t7 (make-instant :name 't7))
       (t8 (make-instant :name 't8))
       (t9 (make-instant :name 't9))
       (t10 (make-instant :name 't10))
       (t11 (make-instant :name 't11))
       (t12 (make-instant :name 't12))
       (t13 (make-instant :name 't13))
       e1 e2 e3 e4 e5 e6 e7 e8 e9 e10 e11 e12 e13
       t45
      )
        (setf (instant-prior-times t1) nil)
        (setf (instant-next-times t1) (list t2 t3))
        (setf (instant-prior-times t2) (list t1))
        (setf (instant-next-times t2) (list t4))
        (setf (instant-prior-times t3) (list t1))
        (setf (instant-next-times t3) (list t4))
        (setf (instant-prior-times t4) (list t2 t3))
        (setf (instant-next-times t4) (list t5))
        (setf (instant-prior-times t5) (list t4))
        (setf (instant-next-times t5) (list t6 t7))
        (setf (instant-prior-times t6) (list t5))
        (setf (instant-next-times t6) (list t8))
        (setf (instant-prior-times t7) (list t5))
        (setf (instant-next-times t7) (list t8))
        (setf (instant-prior-times t8) (list t6 t7))
        (setf (instant-next-times t8) (list t9 t10))
        (setf (instant-prior-times t9) (list t8))
        (setf (instant-next-times t9) (list t11))
        (setf (instant-prior-times t10) (list t8))
        (setf (instant-next-times t10) (list t12))
        (setf (instant-prior-times t11) (list t9))
        (setf (instant-next-times t11) (list t13))
        (setf (instant-prior-times t12) (list t10))
        (setf (instant-next-times t12) (list t13))
        (setf (instant-prior-times t13) (list t11 t12))
        (setf (instant-next-times t13) nil)
        (setq e1 (make-cause :hypothesized t :timepoint t1 :name 'e1))
        (setq e2 (make-cause :hypothesized t :timepoint t2 :name 'e2))
        (setq e3 (make-cause :hypothesized t :timepoint t3 :name 'e3))
        (setq e4 (make-cause :hypothesized t :timepoint t4 :name 'e4))
        (setq e5 (make-cause :hypothesized t :timepoint t5 :name 'e5))
        (setq e6 (make-cause :hypothesized t :timepoint t6 :name 'e6))
        (setq e7 (make-cause :hypothesized t :timepoint t7 :name 'e7))
        (setq e8 (make-cause :hypothesized t :timepoint t8 :name 'e8))
        (setq e9 (make-cause :hypothesized t :timepoint t9 :name 'e9))
        (setq e10 (make-cause :hypothesized t :timepoint t10 :name 'e10))
        (setq e11 (make-cause :hypothesized t :timepoint t11 :name 'e11))
        (setq e12 (make-cause :hypothesized t :timepoint t12 :name 'e12))
        (setq e13 (make-cause :hypothesized t :timepoint t13 :name 'e13))
        (setf (instant-causes t1) (list e1))
        (setf (instant-causes t2) (list e2))
        (setf (instant-causes t3) (list e3))
        (setf (instant-causes t4) (list e4))
        (setf (instant-causes t5) (list e5))
        (setf (instant-causes t6) (list e6))
        (setf (instant-causes t7) (list e7))
        (setf (instant-causes t8) (list e8))
        (setf (instant-causes t9) (list e9))
        (setf (instant-causes t10) (list e10))
        (setf (instant-causes t11) (list e11))
        (setf (instant-causes t12) (list e12))
        (setf (instant-causes t13) (list e13))
        (setf (fact-history-changes hist1)
            `( (,e13 nil nil) (,e11 t nil) (,e9 nil t) (,e8 nil nil) 
               (,e6 t nil) (,e4 t t) (,e2 nil t) (,e1 t nil) )
        )
        (push `((blocked rover0) ,hist1)
            (history-fact-histories *history*)
        )
        (setq t45 (make-instant :prior-times (list t4) :next-times (list t5)))
;        (setq e45 
            (make-cause
                :postconditions '((blocked rover0))
                :timepoint t45
            )
;        )
        (break)
        (print (find-fact-history '(blocked rover0)))
    )
)


(defun get-fact-info (fact)
    (let ((val nil) (func nil) (hist nil))
      (cond 
          ((eq (first fact) 'decrease)
              (setq val (third fact)) 
              (setq fact (second fact))
              (setq func #'-)
          )
          ((eq (first fact) 'increase)
              (setq val (third fact)) 
              (setq fact (second fact))
              (setq func #'+)
          )
          ((eq (first fact) 'set)
              (setq val (third fact)) 
              (setq fact (second fact))
          )
          ((eq (first fact) 'not) 
              (setq val nil) 
              (setq fact (second fact))
          ) 
          ((listp (first fact)) 
              (setq val (second fact))
              (setq fact (first fact))
          )
          (t (setq val t))
      )
      (setq hist (find-fact-history fact))
      (when (null hist)
          (setq hist (make-unobserved-fact-history fact))
      )
      (list fact hist val func)
    )
)
    

; returns a fact-history object for the fact queried.
(defun find-fact-history (fact)
    (dolist (fhist (history-fact-histories *history*))
        (when (equal fact (car fhist))
            (return-from find-fact-history (second fhist))
        )
    )
    nil
)

; returns the most recent event to modify the state of the queried fact
; (defun find-most-recent-happening (fact &aux fhist)
    ; (setq fhist (find-fact-history fact))
    ; (when (null fhist)
        ; (setq fhist (make-unobserved-fact-history fact))
    ; )
    ; (first (find-most-recent-change fhist))
    ; ; (when (and (not (is-hidden fact)) 
               ; ; (not (eq (get-prior-observation hap) (second *observations*)))
          ; ; )
        ; ; (second *observations*)
    ; ; )
; )

(defun get-prior-observation (hap)
    (let ((tn (happening-timepoint hap)))
        (loop while (null (instant-observation tn)) do
            (setq tn (car (instant-prior-times tn)))                          
        )
        (instant-observation tn)
    )
)

(defun find-most-recent-changes (fhist)
    (let*
      ((next-fact-change (fact-history-changes fhist))
       (ret-event (first (first next-fact-change)))
      )
        (when (null next-fact-change)
            (return-from find-most-recent-changes nil)
        )
        (when (eq ret-event *latest-obs*)
            (setq next-fact-change (rest next-fact-change))
            (setq ret-event (first (first next-fact-change)))
        )
        (loop while (and ret-event (not (is-hypothesized ret-event))) do
            (setq next-fact-change (rest next-fact-change))
            (setq ret-event (first (first next-fact-change)))
        )
        next-fact-change
    )
)

(defun is-hypothesized (h)
    (happening-hypothesized h)
)

; returns a list of event templates that could account for a fact becoming true 
(defun find-causing-occurrences (fact)
    (let 
      ((templates *causable-events*) 
        possible-occurrences
      )
        (dolist (template templates)
            (dolist (effect (shop2::pddl-event-effect template))
                (let ((bindings (shop2::unify effect fact)))
                    (when (not (eq (shop-fail) bindings))
                        (push
                            (make-occurrence
                                :template template
                                :bindings bindings
                            )
                            possible-occurrences
                        )
                    )
                )
            )
        )
        possible-occurrences
    )
)


(defun get-preconditions (occ)
    (apply-substitution 
        (shop2::pddl-event-precondition (occurrence-template occ)) 
        (occurrence-bindings occ)
    )
)


(defun get-postconditions (occ)
    (remove 'and
        (shop2::apply-substitution 
            (effect-of (occurrence-template occ))
            (occurrence-bindings occ)
        )
    )
)

(defun get-uncausables (conjunct)
    (remove-if #'is-causable conjunct)
)


(defun get-causables (conjunct)
    (remove-if-not #'is-causable conjunct)
)

(defun is-transient (pattern)
    (if (eq (car pattern) 'not)
        ;then
        (member (caadr pattern) *transients*)
        ;else
        (member (car pattern) *transients*)
    )
)

(defun is-causable (pattern)
    (if (eq (car pattern) 'not)
        ;then
        (member (caadr pattern) *removables*)
        ;else
        (member (car pattern) *causables*)
    )
)

(defun is-hidden (pattern)
    (cond
        ((eq (car pattern) 'not)  (member (caadr pattern) *hiddens*))
        ((listp (car pattern))    (member (caar pattern) *hiddens*))
        (t                        (member (car pattern) *hiddens*))
    )
)

(defun first-in (sym lst)
    (and (listp lst) (eq (car lst) sym))
)

(defun first-of (lst sym)
    (and (listp lst) (eq (car lst) sym))
)


(defun get-fact-value (fact)
    (get-latest-value (find-fact-history fact))
)
(defun get-latest-value (hist)
    (get-value-after-changes (fact-history-changes hist))
)

(defun check-fact-at-time (fact tp &aux finfo)
    (setq finfo (get-fact-info fact))
    (let ((hist (second finfo)) (val (third finfo)))
        (eq (get-value-at-time hist tp) val) 
    )
)

(defun get-value-at-time (hist-or-fact ev-or-tp)
    (when (listp hist-or-fact)
        (setq hist-or-fact (find-fact-history hist-or-fact))
    )
    (get-value-after-changes (search-back-for-event ev-or-tp hist-or-fact))
)

(defun get-value-before-time (hist-or-fact ev-or-tp)
    (when (listp hist-or-fact)
        (setq hist-or-fact (find-fact-history hist-or-fact))
    )
    (let ((changes (search-back-for-event ev-or-tp hist-or-fact)))
        (if (or (eq ev-or-tp (caar changes)) (eq ev-or-tp (change-time (car changes))))
            ;then
            (get-value-before-changes changes)
            ;else
            (get-value-after-changes changes)
        )
    )
)

(defun history-set (ev fact)
    (cond
      ((eq (first fact) 'not) (history-add ev (second fact) 'unk nil))
      ((eq (first fact) 'decrease) 
          (history-calc ev #'- (second fact) (third fact)))
      ((eq (first fact) 'increase) 
          (history-calc ev #'+ (second fact) (third fact)))
      ((eq (first fact) 'set) 
          (history-add ev (second fact) 'unk 
              (coerce (third fact) 'double-float)
          )
      )
      ((listp (first fact)) (history-add ev (first fact) 'unk (second fact)))
      (t (history-add ev fact 'unk t))
    )
)

(defun history-calc (ev func fact arg)
    (let ((last-val (get-fact-value fact)))
        (history-add
            ev
            fact
            last-val
            (coerce (funcall func last-val arg) 'double-float)
        )
    )
)
            
(defun history-insert-precondition (ev fact &aux finfo rest-changes discs)
    (setq finfo (get-fact-info fact))
    (let ((f (first finfo)) (hist (second finfo)) (val (third finfo)) changes)
        (setq changes (search-forward-for-event (happening-timepoint ev) hist :search-all t))
        (when (null changes)
            (sort-history hist)
            (setq changes (find-most-recent-changes hist))
            (history-add ev f val 'none)
            (when (null changes)
                ;;Only a hidden value would have no changes at all, and it 
                ;;starts out with a value of nil.
                (if val
                    ;then
                    (return-from history-insert-precondition 
                        (list 
                            (make-discrepancy
                                :fact fact 
                                :instant
                                    (get-initial-instant
                                        :prior-time nil
                                        :next-time (happening-timepoint ev)
                                    )
                                :dependent ev
                            )
                        )
                    )
                    ;else
                    (return-from history-insert-precondition nil)
                )
            )
            (when (not (eql val (get-value-after-changes changes)))
                (return-from history-insert-precondition 
                    (list 
                        (make-discrepancy
                            :fact fact 
                            :instant
                                (get-initial-instant
                                    :prior-time (get-constraining-earlier-time (car changes))
                                    :next-time (happening-timepoint ev)
                                )
                            :dependent ev
                        )
                    )
                )
            )
            (return-from history-insert-precondition nil)
        )
        (if (and ;(instant-final (happening-timepoint ev)) 
                 (not (eq ev (caar changes)))
                 (eq (happening-timepoint ev) 
                     (happening-timepoint (caar changes))
                 )
            )
            ;then
            (let ((non-compats nil)
                  (tp (cause-timepoint ev)) 
                  next-old-ev old-ev vpre
                 )
                (setq next-old-ev (caar changes))
                (setq changes (cons nil changes))
                (loop while (and next-old-ev (eq tp (happening-timepoint next-old-ev))) do
                    (setq old-ev next-old-ev)
                    (setq changes (rest changes))
                    (setq vpre (second (first changes)))
                    (when (and (cause-hypothesized old-ev) (eq (not val) vpre))
                        (push old-ev non-compats)
                        (setf (cause-hypothesized old-ev) nil)
                    )
                    (setq next-old-ev (first (second changes)))
                )
                (when non-compats
                    (setq discs 
                        (list
                            (make-discrepancy 
                                :fact (not-ify f)
                                :instant (cause-timepoint ev)
                                :removals non-compats
                                :dependent nil
                            )
                        )
                    )
                )
                (if (eq ev (caar changes))
                    ;then
                    (progn
                        (setf (cdr (first changes)) (cons val (cddr (first changes))))
                        (setq rest-changes (rest changes))
                    )
                    ;else
                    (progn
                        (push (list ev val 'none) (cdr changes))
                        (setq rest-changes (rest (rest changes)))
                    )
                )
;                (break "Final instant in insert-precondition")
            )
            ;else not a final instant re-use
            (let ()
                (when (eq ev (first (first changes)))
                    (setf (cdr (first changes)) (cons val (cddr (first changes))))
                )
                ; (setq next-val (second (first changes)))
                (loop while (and (cdr changes)
                                 (eq (change-time (first changes)) 
                                     (change-time (second changes))
                                 )
                            ) do
                    (setq changes (rest changes))
                    ; (when (and (is-hypothesized (caar changes)) 
                               ; (not (eq (cadar changes) 'unk))
                          ; )
                        ; (if (and (not (eq next-val 'unk)) 
                                 ; (not (eq (cadar changes) next-val))
                            ; )
                            ; ;then
                            ; (error "Incompatible prior values.")
                            ; ;else
                            ; (setq next-val (cadar changes))
                        ; )
                    ; )
                )
                (setq rest-changes (rest changes))
;                (break "before when")
                (when (not (eq ev (first (first changes))))
                    (setf (cdr changes) (cons (list ev val 'none) (cdr changes)))
                    (when (not (is-hypothesized (first (first changes))))
                        (setq changes (search-forward-for-event ev hist :exclusive t))
                    )
                    (when (and changes 
                            (not (eql (get-value-before-recent-changes changes) val)) 
                            (not (eql (get-value-before-recent-changes changes) 'unk))
                          )
                        (push 
                            (make-discrepancy
                                :fact (not-ify fact)
                                :instant
                                    (get-initial-instant
                                        :prior-time (happening-timepoint ev)
                                        :next-time (happening-timepoint (caar changes))
                                    )
                                :dependent nil
                            )
                            discs
                        )
                    )
                )
            )
        )
;        (break "before rest")
        (sort-history hist)
        (setq rest-changes (search-back-for-event (happening-timepoint ev) hist :exclusive t))
        (loop while (and rest-changes
                         (not (is-hypothesized (first (first rest-changes))))
                    ) do
            (setq rest-changes (rest rest-changes))
        )
        (when (null rest-changes)
            (if (eq val nil)
                ;then
                (return-from history-insert-precondition discs)
                ;else
                (return-from history-insert-precondition
                    (cons 
                        (make-discrepancy
                            :fact fact
                            :instant
                                (get-initial-instant
                                    :prior-time nil
                                    :next-time (happening-timepoint ev)
                                )
                            :dependent ev
                        ) 
                        discs
                    )
                )
            )
        )
;        (break "before new-val")
        (let ((new-val (get-value-after-changes rest-changes)))
            (if (not (eql val new-val))
                ;then
                (return-from history-insert-precondition
                    (cons 
                        (make-discrepancy
                            :fact fact
                            :instant
                                (get-initial-instant
                                    :prior-time 
                                        (get-constraining-earlier-time (first rest-changes))
                                        ; (happening-timepoint 
                                            ; (first (first rest-changes))
                                        ; )
                                    :next-time (happening-timepoint ev)
                                )
                            :dependent ev
                        )
                        discs
                    )
                )
                ;else
                (return-from history-insert-precondition discs)
            )
        )
    )
)

(defun get-constraining-earlier-time (change &aux prior-time)
    (setq prior-time (happening-timepoint (first change)))
    (when (eq (third change) 'none)
        (when (and (instant-final prior-time) (not (instant-observation prior-time)))
            (setq prior-time (instant-prior-moment prior-time))
        )
        (when (not (instant-final prior-time))
            (setq prior-time (car (instant-prior-times prior-time)))
        )
    )    
    prior-time
)

(defun get-earlier-event-fact-constraint (ev f)
    (get-constraining-earlier-time
        (find ev (fact-history-changes (second (get-fact-info f))) :key #'first)
    )
)
            
(defun history-insert-postcondition (ev f &aux finfo)
    (setq finfo (get-fact-info f))
    (let 
      ((fact (first finfo)) 
       (hist (second finfo)) 
       (val (third finfo)) 
       (func (fourth finfo))
       (discs nil)
       changes
      )
        (setq changes (search-forward-for-event (happening-timepoint ev) hist :search-all t))
        (when (null changes)
            (when func
                (error "Function value adjusted with no prior.")
            )
            (history-add ev fact 'unk val)
            (sort-history hist)
            (return-from history-insert-postcondition nil)
        )
        (if (and ;(instant-final (happening-timepoint ev)) 
                 (not (eq ev (caar changes)))
                 (eq (happening-timepoint ev) 
                     (happening-timepoint (caar changes))
                 )
            )
            ;then
            (let ((non-compats nil) 
                  (tp (cause-timepoint ev)) 
                  next-old-ev old-ev vpost
                 )
                (setq next-old-ev (caar changes))
                (setq changes (cons nil changes))
                (loop while (and next-old-ev (eq tp (happening-timepoint next-old-ev))) do
                    (setq old-ev next-old-ev)
                    (setq changes (rest changes))
                    (setq vpost (third (first changes)))
                    (when (and (cause-hypothesized old-ev) (eq (not val) vpost))
                        (push old-ev non-compats)
                        (setf (cause-hypothesized old-ev) nil)
                    )
                    (setq next-old-ev (first (second changes)))
                )
                (when non-compats
                    (setq discs 
                        (list
                            (make-discrepancy 
                                :fact (not-ify f)
                                :instant (cause-timepoint ev)
                                :removals non-compats
                                :dependent nil
                            )
                        )
                    )
                )
                (push (list ev 'unk val) (cdr changes))
;                (break "Final instant in insert-postcondition")
            )
            ;else
            (progn
                (let ((cur-tp (change-time (car changes))))
                    (loop while (and ;(instant-final cur-tp) 
                                     (eq cur-tp (change-time (second changes)))
                                ) do
                        (setq changes (rest changes))
                    )
                )
                (if func
                    ;then
                    (let ((past changes) (old-val nil))
                        (loop while (and past (not (is-hypothesized (first (first past))))) do
                            (setq past (rest past))
                        )
                        (when (null past)
                            (error "Function value adjusted with no prior.")
                        )
                        (setq old-val (third (first past)))
                        (setq val (funcall func old-val val))
                        (setf (cdr changes) (cons (list ev old-val val) (cdr changes)))
                    )
                    ;else
                    (setf (cdr changes) (cons (list ev 'unk val) (cdr changes)))
                )
            )
        )
        (sort-history hist)
        (setq changes (search-forward-for-event ev hist :exclusive t))
        (let ((next-val 'unk) (cur-tp (change-time (first changes))))
            (loop while (and changes 
                             (eq next-val 'unk)
                             (eq cur-tp (change-time (first changes)))
                        ) do
                (when (is-hypothesized (first (first changes)))
                    (setq next-val (second (first changes)))
                )
                (setq changes (rest changes))
            )
            (if (and (not (eql val next-val))
                     (not (eql 'unk next-val))
                )
                ;then
                (return-from history-insert-postcondition
                    (cons 
                        (make-discrepancy
                            :fact (not-ify f)
                            :instant
                                (get-initial-instant 
                                    :prior-time (happening-timepoint ev)
                                    :next-time  cur-tp
                                )
                            :dependent nil
                        )
                        discs
                    )
                )
                ;else
                (return-from history-insert-postcondition discs)
            )
        )
    )
)

(defun history-add (ev fact before-val after-val)
    (let ((hist (find-fact-history fact)))
        (when (null hist) 
            (setq hist (make-unobserved-fact-history fact))
#|            (push (list fact hist) (history-fact-histories *history*))
            (when (member (first fact) (domain-hidden *domain-model*))
                (setf (fact-history-hidden hist) t)
            )
|#        )
        (history-put fact hist ev before-val after-val)
    )
)

(defun history-put (fact hist hap before-val after-val)
    (set-latest-val fact hap before-val after-val)
    (push (list hap before-val after-val) (fact-history-changes hist))
)

(defun set-latest-val (fact ev before-val after-val)
    (case after-val
        (none nil)
        ((nil) (shop2::delete-atom-from-state fact *fringe-state* 0 ev))
        ((t) (shop2::add-atom-to-state fact *fringe-state* 0 ev))
        (unk (set-latest-val fact ev before-val before-val))
        (otherwise (eval `(setf ,fact ',after-val))) 
    )
)


(defun cnf (and-or-tree)
    (cond
        ((null and-or-tree) (list nil))
        ((eq (car and-or-tree) 'not)
            (list (list and-or-tree))
        )
        ((eq (car and-or-tree) '<=)
            (list (list and-or-tree))
        )
        ((eq (car and-or-tree) '>=)
            (list (list and-or-tree))
        )
        ((eq (car and-or-tree) '<)
            (list (list and-or-tree))
        )
        ((eq (car and-or-tree) '>)
            (list (list and-or-tree))
        )
        ((eq (car and-or-tree) '=)
            (list (list and-or-tree))
        )
        ((eq (car and-or-tree) 'or)
            (apply #'append
               (mapcar #'cnf
                   (cdr and-or-tree)
               )
            )
        )
        ((eq (car and-or-tree) 'and)
            (cross-product
                (mapcar #'cnf
                       (cdr and-or-tree)
                )
            )
        )
        ((eq (car and-or-tree) 'eval)
            (list (list and-or-tree))
        )
        ((eq (car and-or-tree) 'assign)
            (list (list and-or-tree))
        )
        ((eq (car and-or-tree) 'forall) ; becomes a normal fact
            (list (list and-or-tree))
        )
        ((all-atoms and-or-tree) (list (list and-or-tree)))
        ((member (car and-or-tree) '(decrease increase)) (list (list and-or-tree)))
        (t (error "Did not expect ~A" (car and-or-tree)))
    )
)

(defun all-atoms (atom-list)
    (and 
        (listp atom-list)
        (not (find-if-not #'atom atom-list))
    )
)

(defun cross-product (fact-set-lists &optional (facts-so-far nil))
   (let 
    ((conjunct-sets nil))
      (if fact-set-lists
         (loop for fact-set in (car fact-set-lists) do
            (setq conjunct-sets
               (append 
                  conjunct-sets
                  (cross-product 
                     (cdr fact-set-lists) 
                     (append facts-so-far fact-set)
                  )
               )
            )
         )
         ;else
         (push facts-so-far conjunct-sets)
     )
     conjunct-sets
   )
)

(defun not-ify (cause)
    (case (first cause)
        (not (second cause))
        (assign (list 'assign (second cause) (list 'not (third cause))))
        (eval (list 'eval (list 'not (second cause))))
        (>= (cons '< (rest cause)))
        (<= (cons '> (rest cause)))
        (=  (list 'eval (list 'not (cons 'eq (rest cause)))))
        ;MCM (10/4): I can't deal with this at the moment, but it may be 
        ;necessary later.
;        (forall (list 'not (butlast (convert-forall-to-fact cause :x))))
        (otherwise (list 'not cause))
    )
)

(defun show-changes (changes)
    (when (typep changes 'fact-history)
        (setq changes (fact-history-changes changes))
    )
    (reverse (remove-if-not #'is-hypothesized changes :key #'first)) 
)

(defun is-link-instant (tp)
    (instant-link tp)
)

(defun bad-instant (tp &key (should nil) &aux (next-times nil) (prior-times nil))
    (dolist (ntp (instant-next-times tp))
        (if (and (instant-hypothesized ntp) (is-link-instant ntp)) 
            ;then
            (push-all (instant-next-times ntp) next-times)
            ;else
            (push ntp next-times)
        )
    )
    ; (setq next-times
        ; (mapcar 
            ; #'(lambda (ntp)
                ; (cond
                    ; ((eq (instant-next-moment ntp) 'none) ntp)
                    ; ((instant-observation ntp) ntp)
                    ; (t (instant-next-moment ntp))
                ; )
              ; )
            ; next-times
        ; )
    ; )
    (dolist (ptp (instant-prior-times tp))
        (if (and (instant-hypothesized ptp) (is-link-instant ptp))
            ;then
            (push-all (instant-prior-times ptp) prior-times)
            ;else
            (push ptp prior-times)
        )
    )
    (let 
      ((earlier-times
        (apply #'append 
            (mapcar #'list-earlier-instants prior-times)
        )
       )
       (later-times
           (set-difference
               (apply #'append
                   (mapcar #'list-later-instants next-times)
               )
               (remove-if-not #'instant-final next-times)
           )
       )
      )
        (cond
            ((instant-fake tp) nil)
            ((and (null (instant-prior-times tp)) (null (instant-next-times tp))
                  (not (instant-final tp))
             )
               'all-null-neighbors
            )
            ((intersection earlier-times (instant-next-times tp))
                (list 'next-time-found-earlier 
                      (intersection earlier-times (instant-next-times tp))
                )
            )
            ((is-link-instant tp) nil)
            ((intersection 
                (mapcar #'instant-next-moment prior-times)
                later-times
             )
                (list 'splits-up-moments 
                      (intersection 
                          (mapcar #'instant-next-moment prior-times)
                          later-times
                      )
                )
            )
            ((intersection 
                (mapcar #'instant-prior-moment next-times)
                earlier-times
             )
                (if (should-be-final tp :check-bad nil)
                    ;then
                    (when (not should)
                        (break "~%~%Instant ~A should-be-final!~%~%"
                            (instant-name tp)
                        )
                    )
                    ;else
                    (list 'splits-up-next-time-moments 
                        (intersection 
                            (mapcar #'instant-prior-moment next-times)
                            earlier-times
                        )
                    )
                )
            )
        )
    )
)

(defun squeezed-instant (tp)
    (and 
        (instant-next-times tp) 
        (eq (car (instant-prior-times tp)) 
            (instant-prior-moment (car (instant-next-times tp)))
        )
    )
)

(defun dehypothesize-instant (tp)
    (when (instant-final tp)
        (error "Finals always hypothesized.")
    )
    (setf (instant-hypothesized tp) nil)
)

#+:CLISPNotNow 
(trace (make-instant 
        :suppress-if t 
        :post-break-if 
            (and
                (or (squeezed-instant (car ext:*trace-values*))
;                    (bad-instant (car ext:*trace-values*))
                )
                (progn
                    (when (squeezed-instant (car ext:*trace-values*))
                        (format t "~%Squeezed instant: ~A" (car ext:*trace-values*))
                    )
                    (when (bad-instant (car ext:*trace-values*))
                        (format t "~%Bad instant: ~A" (car ext:*trace-values*))
                    )
                    t
                )
            )
)      )

