 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: 2009
 ;;;; Project Name: ARTUE
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file provides loading functionality to load Lisp files in ways that 
 ;;;; prefer compiled versions, and create them if necessary.


#-CLISP (require 'asdf)
(when (null (find-package "ASDF"))
    (load (make-pathname :directory '(:relative "asdf") :name "asdf"))
)

(defpackage :utils
    (:use :common-lisp
#+ALLEGRO :mop
#+ABCL    :mop
#+CLISP   :clos
    )
    (:export local-executor load-in-package mapcar+ plan-var-p var-names 
             print-hash symbols-in vars-in bps-load-file bps-load-files 
             find-lists-starting-with has-plan-vars flatten grep-trees
             trace-recursion find-subtrees find-zero-anywhere maptree find-zero
             load-file load-files maplist+ map-if map-if+ algebra-reduce
             replace-with-reduction algebra-test find-quoted-portions
             special-vars-in replace-with-reduced-test algebra-indirect-test
             coerce-numbers subst-members push-all special-var-p ntop
             print-one-per-line find-in-tree is-in-tree beep copy-file
             switch-symbols-to-package change-package read-file-to-list
             round-off qp get-time-string start run round-to-significants
             lexicographic-sort debug-format coerce-some-numbers
             get-generated-symbol-counter set-generated-symbol-counter 
             generate-symbol only starts-with get-date-time-dashes
             hash-to-alist alist-to-hash node-search combine-strings-symbols
             fixnum-right-shift fixnum-left-shift random-subset find-min
             new-reservoir-sampler sample get-results get-population-size
             split split-if count-unique cycle-has-formed arg-fn cycle-at-start
             precedes
    )
)

(in-package :utils)

(defvar *default-source-type*
  #-ACLPC "lisp"
  #+ACLPC "lsp"
)

;; compiled file type
(defvar *default-bin-type*
  #+RT "bbin"
  #+(:AND :LUCID :RIOS) "rbin"
  #+:IRIX "mbin"
  #+MCL "fasl"
  #+ACLPC "fsl"
  #+:ABCL "abcl"
  #+:ALLEGRO "fasl"
  #+:SBCL "fasl"
  #+:CLISP "fas"
  #+:CMUCL "sse2f"
 )

;; Where to get stuff
(defvar *default-pathname* 
  #+:ILS "/u/bps/code/"
  #+:PARC "virgo:/virgo/dekleer/bps/"
  #+:MCL "Macintosh HD:BPS:"
  #+:ACLPC "e:\\code\\"
  #+:ABCL "c:\\Programs\\test\\BPS\\"
  #+:ALLEGRO "c:\\Programs\\test\\BPS\\"
  #+:CLISP "/cygdrive/c/Programs/test/BPS/"
  #+:CMUCL "/home/molinemc"
  #+:SBCL "/home/molinemc"
)

(defun make-bps-path (folder)
	(concatenate 'string *default-pathname* folder 
		#+:ABCL "\\"
		#-:ABCL "/"
        )
)

(defun get-distribution-directory ()
    #+ALLEGRO "allegro-bin"
    #+CLISP "clisp-bin"
    #+SBCL "sbcl-bin"
    #+ABCL "abcl-bin"
    #+CMUCL "cmucl-bin"
    #+RT "rt-bin"
    #+LUCID "lucid-bin"
    #+IRIX "irix-bin"
    #+MCL "mcl-bin"
    #+ACLPC "aclpc-bin"
)    

(defun get-compile-path (path &aux comp-pname)
    (when (not (pathnamep path))
        (setq path (pathname path))
    )
    (when (or (not (listp (pathname-directory path))) (not (eq (car (pathname-directory path)) :relative)))
        (error "expected relative pathname")
    )
    (setq comp-pname
        (make-pathname
            :host (pathname-host path)
            :device (pathname-device path)
            :version (pathname-version path)
            :directory
                (cons :relative
                    (cons (get-distribution-directory) 
                        (cdr (pathname-directory path))
                    )
                )
            :name (pathname-name path)
            :type (pathname-type path)
        )
    )
    (ensure-directories-exist comp-pname)
    comp-pname
)

(defun suffix (file-name)
   (subseq file-name (+ 1 (position #\. file-name)))
)

(defun has-suffix (file-name)
   (position #\. file-name)
)

(defun add-suffix (file-name new-suffix)
   (concatenate 'string file-name "." new-suffix)
)

(defun remove-suffix (file-name)
   (subseq file-name 0 (position #\. file-name))
)

(defun adapt-file-name (path file-name new-suffix)
   (cond
      ((has-suffix file-name) 
		   (when (pathnamep path)
			   (setq path (pathname-directory path))
		   )
          (make-pathname 
             :directory path 
             :name (remove-suffix file-name)
             :type new-suffix
          )
      )
      (t (merge-pathnames path (add-suffix file-name new-suffix)))
   )
)

(defun adapt-file-name-if-not-suffixed (path file-name new-suffix)
   (when (pathnamep path)
	   (setq path (pathname-directory path))
   )
   (cond
      ((has-suffix file-name) 
		   (when (pathnamep path)
			   (setq path (pathname-directory path))
		   )
          (make-pathname 
             :directory path 
             :name (remove-suffix file-name)
             :type (suffix file-name)
          )
      )
      (t
         (make-pathname 
            :directory path 
            :name file-name
            :type new-suffix
         )
      )
   )
)

(defun load-file-list (file-list &optional (path *default-pathname*)
			     (type *default-bin-type*))
  (dolist (file file-list)
	  (load (adapt-file-name path file type))
  )
)

(defun compile-files (file-list
		       &optional (path *default-pathname*))
  (dolist (file file-list)
    (format t "~% Compiling ~A..."
       (adapt-file-name-if-not-suffixed 
          path 
          file
          *default-source-type*
       )
    )
    (compile-file
       (adapt-file-name-if-not-suffixed 
          path 
          file
          *default-source-type*
       )
       :OUTPUT-FILE
       (adapt-file-name
          (get-compile-path path) 
          file
          *default-bin-type*
       )
    )
  )
)

(defun load-from (file path)
  (load (concatenate 'string path file)))

;;;; Compiling and loading files

(defun compile-if-newer-files (file-list
		       &optional (path *default-pathname*))
  (setq path (pathname path))
  (dolist (file file-list)
    (let* 
	   ((source-file
		    (adapt-file-name-if-not-suffixed 
                path 
                file
                *default-source-type*
            )
        )
	    (source-file-date (if (probe-file source-file) (file-write-date source-file) nil))
	    (exec-file
	        (adapt-file-name 
                (get-compile-path path) 
                file
                *default-bin-type*
            )
        )
	    (exec-file-date (if (probe-file exec-file) (file-write-date exec-file) nil))
	   )
;    (format t "~%>> Source file ~A ~A." source-file source-file-date)
;    (format t "~%>> Compiled file ~A ~A." exec-file exec-file-date)
	(when (null source-file-date)
	    (if (null exec-file-date)
	        ;then
	        (error "Neither source nor executable present for ~A!" file)
	        ;else
	        (setq source-file-date exec-file-date)
        )
    )
    (when (or (null exec-file-date) (< exec-file-date source-file-date)) 
	    (format t "~% Compiling ~A..." source-file)
	    (compile-file source-file :OUTPUT-FILE 
	        (merge-pathnames exec-file *default-pathname-defaults*)
        )
    )
    (unless (probe-file exec-file)
        (error "~% Failed to load ~A!" exec-file)
    )
    (load exec-file)
    (format t "~% Loaded ~A..." exec-file)
   )
  )
)

(defun compile-load-files (file-list
			    &optional (path *default-pathname*)
			   (pre-load? t) 
			    &aux out-path)
  (setq path (pathname path))
  (dolist (file file-list)
     (setq out-path 
        (merge-pathnames
            (adapt-file-name 
                (get-compile-path path) 
                file
                *default-bin-type*
            )
            *default-pathname-defaults*
        )
     )
     (when pre-load?
        (load
            (adapt-file-name-if-not-suffixed 
                path 
                file
                *default-source-type*
            )
        )
        (compile-file
           (adapt-file-name-if-not-suffixed 
              path 
              file
              *default-source-type*
           )
	       :OUTPUT-FILE out-path
        )
        (load out-path)
     )
  )
)

(defun load-no-compile-files (file-list
			    &optional (path *default-pathname*)
			   (pre-load? t) 
			    &aux out-path)
  (dolist (file file-list)
	(load
           (adapt-file-name-if-not-suffixed 
              path 
              file
              *default-source-type*
           )
        )
  )
)

(defun load-files (path
		       file-list
		       &key (action :compile-if-newer)
		       )
  (cond 
    ((eq action :compile)
	    (compile-files file-list path))
    ((eq action :load)
	    (load-no-compile-files file-list path))
    ((eq action :load-source)
	    (load-file-list file-list path "lisp"))
    ((eq action :compile-load)
	    (compile-load-files file-list path))
    ((eq action :compile-if-newer)
	    (compile-if-newer-files file-list path))
    (t (error "Action argument unrecognized")))
)
    
(defun bps-load-files (path
		       file-list
		       &key (action :compile-if-newer)
		       )
    (load-files path file-list :action action)
)

(defun load-file (path
		       file
		       &key (action :compile-if-newer)
		       )
	(load-files path (list file) :action action)
)

(defun bps-load-file (path
		       file
		       &key (action :compile-if-newer)
		       )
	(load-file path (list file) :action action)
)


(load-files (make-pathname :directory '(:relative "utils")) '("general" "search"))

