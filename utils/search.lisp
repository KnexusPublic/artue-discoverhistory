 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: 2012
 ;;;; Project Name: ARTUE
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file describes functions that run various abstract 
 ;;;; search strategies using functions passed in for computing
 ;;;; expansions, costs, termination conditions, etc.


(in-package :utils)

(defun node-search
        (
            initial-nodes expand-fn cost-fn solution-test-fn 
            &key (solution-count 1) (max-cost 100000) (max-seconds nil)
                 (search-method :breadth-first) (redundant-fn #'eq) 
                 (node-limit 30000) (max-depth 10000)
                 (search-type :achieve)
                 (parameter-update-fn #'(lambda (&key &allow-other-keys) nil))
            &aux (cutoff-time nil)
        )
    (when max-seconds
        (setq cutoff-time 
            (+ (get-internal-run-time) 
               (* max-seconds internal-time-units-per-second)
            )
        )
    )
    (catch :out-of-time
        (case search-method
            (:depth-first 
                (mapcar #'first
                    (depth-first-search  
                        initial-nodes
                        expand-fn cost-fn solution-test-fn
                        :cutoff-time cutoff-time 
                        :solution-count solution-count 
                        :max-cost max-cost
                        :redundant-fn redundant-fn
                        :node-limit node-limit
                        :max-depth max-depth
                        :search-type search-type
                    )
                )
            )
            (:breadth-first 
                (breadth-first-search  
                    initial-nodes
                    expand-fn cost-fn solution-test-fn
                    :cutoff-time cutoff-time 
                    :solution-count solution-count 
                    :max-cost max-cost
                    :redundant-fn redundant-fn
                    :node-limit node-limit
                )
            )
            (:iterative-deepening
                (mapcar #'first
                    (iterative-deepening-search  
                        initial-nodes
                        expand-fn cost-fn solution-test-fn
                        :cutoff-time cutoff-time 
                        :solution-count solution-count 
                        :max-cost max-cost
                        :redundant-fn redundant-fn
                        :node-limit node-limit
                    )
                )
            )
            (:best-first
                (best-first-search  
                    initial-nodes
                    expand-fn cost-fn solution-test-fn
                    :cutoff-time cutoff-time 
                    :solution-count solution-count 
                    :max-cost max-cost
                    :redundant-fn redundant-fn
                    :node-limit node-limit
                    :parameter-update-fn parameter-update-fn
                )
            )
        )
    )
)

(defun breadth-first-search 
        (
            initial-nodes expand-fn cost-fn solution-test-fn 
            &key (solution-count 1) (max-cost 100000) (cutoff-time nil)
                 (redundant-fn #'identity) (node-limit 30000)
            &aux (cost-limit 0) (solutions nil) (cur-layer nil) 
                 (known-nodes nil) (non-redundant-solutions nil)
                 (cur-layer-count 0) (nodes-searched 0)
                 (child-nodes nil) (expand-time 0) (expand-start 0)
                 (max-expand-time 0)
        )
    (setq known-nodes initial-nodes)
    (loop while (and (< (length solutions) solution-count) 
                   (<= cost-limit max-cost)
                   (or (null cutoff-time)
                       (< (get-internal-run-time) cutoff-time)
                   )
              ) do
        (format t "~%~%~%~A~% Using cost limit ~A/~A. ~%~A~%~%"
            (make-string 80 :initial-element #\=) 
            cost-limit
            max-cost
            (make-string 80 :initial-element #\=) 
        )
        (setq cur-layer-count 0)
        (setq cur-layer (remove cost-limit known-nodes :key cost-fn :test #'<))
        (setq known-nodes (remove cost-limit known-nodes :key cost-fn :test #'>=))
        #+ALLEGRO(excl:gc)
  
        (loop while (and cur-layer 
                         (or (null cutoff-time) 
                             (< (get-internal-run-time) cutoff-time)
                         )
                    )  do
;            (when (< (funcall cost-fn (car cur-layer)) cost-limit)
;                (break "Cost reduced.")
;            )
            (setq expand-start (get-internal-run-time))
            (setq child-nodes (funcall expand-fn (first cur-layer)))
            (setq expand-time (- (get-internal-run-time) expand-start))
            (setq max-expand-time (max expand-time max-expand-time))
            ;(when (> expand-time 3000)
                ;(setq *temp-current-node* (car cur-layer))
                ;(break "High expansion time.")
            ;)

            (setq known-nodes
                (append
                    known-nodes
                    (remove cost-limit child-nodes :key cost-fn :test #'>=)
                )
            )
              
            (pop cur-layer)
              
            (dolist (node (remove cost-limit child-nodes :key cost-fn :test #'<))
                (if (funcall solution-test-fn node)
                    ;then
                    (push node solutions)
                    ;else
                    (push node cur-layer)
                )
            )
              
            (incf cur-layer-count)
            (incf nodes-searched)
            (when (and (= 1 solution-count) solutions)
                (setq cur-layer nil)
            )
            (format t "~%Node ~A explored, ~A children." cur-layer-count (length child-nodes))
            (format t "~%Remaining nodes in layer ~A: ~A" 
                cost-limit (length cur-layer)
            )
            (format t "~%Nodes found from lower layers: ~A~%" 
                (length known-nodes)
            )
            (format t "~%Time spent: ~A~%" 
                expand-time
            )
            (when (>= (+ (length cur-layer) (length known-nodes)) node-limit)
                (return-from breadth-first-search :exceeded-memory-bound)
            )
        )
        
        ;(when (> (length known-nodes) 1000)
            ;(setq *current-nodes* known-nodes)
            ;(break "More than 1000 known-nodes.")
        ;)
        
        (setq non-redundant-solutions (funcall redundant-fn solutions))

        (format t "~%~A nodes searched with cost ~A.~&~A explanations searched total.~&Maximum expansion time ~A." 
            cur-layer-count cost-limit nodes-searched max-expand-time
        )
          
        (format t "~%Found ~A/~A solution nodes. ~A Redundant."
            (length non-redundant-solutions) solution-count 
            (- (length solutions) (length non-redundant-solutions))
        )
        (setq solutions non-redundant-solutions)
          
        (incf cost-limit)
    )
    (if (and (null solutions) cutoff-time (>= (get-internal-run-time) cutoff-time))
        ;then
        :out-of-time
        ;else
        solutions
    )
)

(defvar *node-history* nil)

(defun best-first-search 
        (
            initial-nodes expand-fn cost-fn solution-test-fn 
            &key (solution-count 1) (max-cost 100000) (cutoff-time nil)
                 (redundant-fn #'identity) (node-limit 30000) 
                 (parameter-update-fn #'(lambda (&key &allow-other-keys) nil))
            &aux (cost-limit -1) (solutions nil) (cur-layer nil) 
                 (non-redundant-solutions nil) (cur-layer-count 0) 
                 (nodes-searched 0) (child-nodes nil) (expand-time 0) 
                 (expand-start 0) (max-expand-time 0) (open-nodes nil)
        )
    (setq *node-history* nil)
    (setq open-nodes (sort (copy-list initial-nodes) #'< :key cost-fn))
    (loop while (and (< (length solutions) solution-count) 
                     open-nodes
                     (or (null cutoff-time)
                         (< (get-internal-run-time) cutoff-time)
                     )
                ) do
        (when (not (eql cost-limit (funcall cost-fn (car open-nodes))))
            (when (< 0 cost-limit)
                (format t "~%~A nodes searched with cost ~A.~&~A explanations searched total.~&Maximum expansion time ~A." 
                    cur-layer-count cost-limit nodes-searched max-expand-time
                )
                  
                (format t "~%Found ~A/~A solution nodes. ~A Redundant."
                    (length non-redundant-solutions) solution-count 
                    (- (length solutions) (length non-redundant-solutions))
                )
            )
            (setq cost-limit (funcall cost-fn (car open-nodes)))
            (format t "~%~%~%~A~% Using cost limit ~A/~A. ~%~A~%~%"
                (make-string 80 :initial-element #\=) 
                cost-limit
                max-cost
                (make-string 80 :initial-element #\=) 
            )
            (setq cur-layer-count 0)
        )
        
        #+ALLEGRO(excl:gc)
        
        (setq expand-start (get-internal-run-time))
        (push (car open-nodes) *node-history*)
        (setq child-nodes (funcall expand-fn (pop open-nodes)))
        (setq expand-time (- (get-internal-run-time) expand-start))
        (setq child-nodes (remove max-cost child-nodes :key cost-fn :test #'<))
        (setq max-expand-time (max expand-time max-expand-time))
              
        (setq solutions (append (remove-if-not solution-test-fn child-nodes) solutions))
        (setq child-nodes (remove-if solution-test-fn child-nodes))
        (incf cur-layer-count)
        (format t "~%Node ~A explored, ~A children." cur-layer-count (length child-nodes))
        (setq open-nodes (merge 'list (sort child-nodes #'< :key cost-fn) open-nodes #'< :key cost-fn))
              
        (incf nodes-searched)
        (when (and (= 1 solution-count) solutions)
            (setq open-nodes nil)
        )
        (format t "~%Nodes searched: ~A Nodes in memory: ~A" 
            nodes-searched (length open-nodes)
        )
        (format t "~%Time spent: ~A~%" 
            expand-time
        )
        (when (>= (length open-nodes) node-limit)
            (return-from best-first-search :exceeded-memory-bound)
        )
        (let ((new-params 
                (funcall parameter-update-fn 
                    :solutions solutions 
                    :required-solution-count solution-count 
                    :open-nodes open-nodes 
                    :cost-limit cost-limit 
                    :max-cost max-cost 
                    :cutoff-time cutoff-time 
                    :node-limit node-limit
                )
             ))
            (when new-params
                (setq solutions (or (getf new-params :solutions) solutions))
                (setq solution-count (or (getf new-params :required-solution-count) solution-count))
                (setq open-nodes (or (getf new-params :open-nodes) open-nodes))
                (setq max-cost (or (getf new-params :max-cost) max-cost))
                (setq cutoff-time (or (getf new-params :cutoff-time) cutoff-time))
                (setq node-limit (or (getf new-params :node-limit) node-limit))
                (when (atom solutions)
                    (setq solutions nil)
                )
                (when (atom open-nodes)
                    (setq open-nodes nil)
                )
                (when (getf new-params :max-cost)
                    (setq open-nodes (remove max-cost open-nodes :key cost-fn :test #'<))
                )
            )
        )
        
        (setq non-redundant-solutions (funcall redundant-fn solutions))
        (setq solutions non-redundant-solutions)
    )
    (if (and (null solutions) cutoff-time (>= (get-internal-run-time) cutoff-time))
        ;then
        :out-of-time
        ;else
        solutions
    )
)

(defun depth-first-search 
        (
            initial-nodes expand-fn cost-fn solution-test-fn 
            &key (solution-count 1) (max-cost 100000) (cutoff-time nil)
                 (redundant-fn #'identity) (node-limit 30000) (max-depth 10000)
                 (search-type :achieve)
            &aux (solutions nil) cur-cost cur-results
        )
        
    (when (>= 0 max-depth)
        (return-from depth-first-search nil)
    )
    (when (> (get-internal-run-time) cutoff-time)
        (throw :out-of-time :out-of-time)
    )
    (dolist (node initial-nodes)
        (cond
            ((and (eq search-type :achieve) (>= (length solutions) solution-count))
                (setq solutions (funcall redundant-fn solutions))
                (when (>= (length solutions) solution-count)
                    (return-from depth-first-search solutions)
                )
            )
            ((funcall solution-test-fn node)
                (push (list node (funcall cost-fn node)) solutions)
            )
            ((<= (setq cur-cost (funcall cost-fn node)) max-cost)
                (setq cur-results
                    (depth-first-search 
                        (funcall expand-fn node)
                        expand-fn cost-fn solution-test-fn
                        :max-cost max-cost 
                        :solution-count solution-count 
                        :cutoff-time cutoff-time
                        :redundant-fn redundant-fn
                        :search-type search-type
                        :max-depth (1- max-depth)
                    )
                )
                (when (eq search-type :maximize)
                    (push (list node cur-cost) cur-results)
                )
                (setq solutions
                    (merge 'list
                        (sort cur-results #'> :key #'second)
                        solutions
                        #'> :key #'second
                    )
                )
                ;(setq solutions (last solutions solution-count))
                ;(format t "Solutions: ~A" solutions) 
            )
        )
    )
    solutions
)

(defun iterative-deepening-search
        (
            initial-nodes expand-fn cost-fn solution-test-fn 
            &key (solution-count 1) (max-cost 100000) (cutoff-time nil)
                 (redundant-fn #'identity) (node-limit 30000)
            &aux (cost-limit 0) (solutions nil)
        )
    (when (> (get-internal-run-time) cutoff-time)
        (return-from iterative-deepening-search nil)
    )
    
    (loop while (and (<= cost-limit max-cost)) do
        (incf cost-limit)
        (format t "~%~%~%~A~% Using cost limit ~A/~A. ~%~A~%~%"
            (make-string 80 :initial-element #\=) 
            cost-limit
            max-cost
            (make-string 80 :initial-element #\=) 
        )
        (setq solutions
            (depth-first-search  
                initial-nodes
                expand-fn cost-fn solution-test-fn
                :cutoff-time cutoff-time 
                :solution-count (* 2 solution-count) 
                :max-cost cost-limit
                :redundant-fn redundant-fn
            )
        )
        (when (>= (length solutions) solution-count)
            (return-from iterative-deepening-search solutions)
        )
        (format t "~%Found ~A/~A solution nodes."
            (length solutions) solution-count 
        )
    )
    
    solutions
)

(defun noop-parameter-update (&key &allow-other-keys) nil)
