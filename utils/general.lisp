 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: Jun 3, 2009
 ;;;; Project Name: ARTUE
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file handles general-use functions.

(in-package :utils)

(defmacro local-executor ()
    (format t "~%Local Prompt: ")
    (do*
        ((form (read) (read)))
        ((eq form 'done))
      (format t "~%~A~%Local Prompt: " (eval form))
    )
)

(defun load-in-package (new-package file)
    (let ((dom-list nil)
          (last-package *package*))
        (eval `(in-package ,(package-name new-package)))
        (with-open-file (in-stream file)
            (setq dom-list (read in-stream))
        )
        (eval `(in-package ,(package-name last-package)))
        (eval dom-list)
    )
)

(defun read-file-to-string (file-name)
  (with-open-file (fin file-name :direction :input)
    (let ((eof (cons nil nil))
          (ret ""))
      (do ((count 0 (1+ count))
           (form (read-line fin nil :eof) (read-line fin nil :eof)))
          ((eq form :eof) ret )
	(setq ret (concatenate 'string ret form))))))

(defun read-file-to-list (file-name)
  (with-open-file (fin file-name :direction :input)
    (let ((eof (cons nil nil))
           ret)
      (do ((count 0 (1+ count))
           (form (read fin nil eof) (read fin nil eof)))
          ((or (eq form eof) (eq form :eof)) ret )
        ;; (print form)
	(push form ret)))))

(defun replace-all (string part replacement &key (test #'char=))
"Returns a new string in which all the occurences of the part 
is replaced with replacement."
    (with-output-to-string (out)
      (loop with part-length = (length part)
            for old-pos = 0 then (+ pos part-length)
            for pos = (search part string
                              :start2 old-pos
                              :test test)
            do (write-string string out
                             :start old-pos
                             :end (or pos (length string)))
            when pos do (write-string replacement out)
            while pos)))

(defun mapcar+ (fun target &rest all-else)
    (mapcar #'(lambda (x) (apply fun x all-else)) target)
)

(defun maplist+ (fun target &rest all-else)
    (maplist #'(lambda (x) (apply fun x all-else)) target)
)

(defun print-hash (table)
    (maphash #'(lambda (key val) (format t "~%Key: ~A Val: ~A" key val)) table)
)


(defun var-names (variable-list)
    (remove-if-not #'plan-var-p variable-list)
)

(defun flatten (tree)
    (cond 
        ((null tree) nil)
        ((atom tree) (list tree))
        ((listp tree) (append (flatten (car tree)) (flatten (cdr tree))))
        (t (error "Tree too complicated. Found ~A." tree))
    )
)

(defun has-plan-vars (tree)
    (eval (cons 'or (mapcar #'plan-var-p (flatten tree))))
)

(defun symbols-in (eval-tree)
    (cond
        ((null eval-tree) nil)
        ((atom eval-tree) 
            (if 
                (or (not (symbolp eval-tree)) (plan-var-p eval-tree))
                nil 
                (list eval-tree)
            )
        )
        ((consp eval-tree) 
            (append 
                (symbols-in (car eval-tree)) 
                (symbols-in (cdr eval-tree))
            )
        )
        (t (error "Tree not formatted as expected. Found a ~A." 
                (class-of eval-tree)
           )
        )
    )
)

(defun special-vars-in (eval-tree)
    (cond
        ((null eval-tree) nil)
        ((atom eval-tree) 
            (if 
                (special-var-p eval-tree)
                (list eval-tree)
                nil 
            )
        )
        ((consp eval-tree) 
            (append 
                (special-vars-in (car eval-tree)) 
                (special-vars-in (cdr eval-tree))
            )
        )
        (t (error "Tree not formatted as expected. Found a ~A." 
                (class-of eval-tree)
           )
        )
    )
)

(defvar *next-symbol-number* 40000)

(defun get-generated-symbol-counter ()
    *next-symbol-number*
)

(defun set-generated-symbol-counter (num)
    (setq *next-symbol-number* num)
)

;(defun generate-symbol (name-str &optional (pkg *package*))
;    (incf *next-symbol-number*)
;    (intern 
;        (concatenate 'string name-str "-" (write-to-string *next-symbol-number*)) 
;        pkg
;    )
;)

(defun vars-in (eval-tree)
    (cond
        ((null eval-tree) nil)                       
        ((atom eval-tree) 
            (if 
                (plan-var-p eval-tree)
                (list eval-tree)
                nil 
            )
        )
        ((consp eval-tree) 
            (append 
                (vars-in (car eval-tree)) 
                (vars-in (cdr eval-tree))
            )
        )
        (t (error "Tree not formatted as expected. Found a ~A." 
                (class-of eval-tree)
           )
        )
    )
)

(defun plan-var-p (sym)
    (if 
        (symbolp sym)
        (eq (char (symbol-name sym) 0) #\?)
        nil
    )
)

(defun special-var-p (sym)
    (if 
        (symbolp sym)
        (eq (char (symbol-name sym) 0) #\%)
        nil
    )
)

; This function has been changed to a no-op to test out removing change-package
; everywhere. Old change-package functionality is renames switch-symbols-to-package
(defun change-package (sym-tree new-package)
    (declare (ignore new-package))
    sym-tree
)

(defun switch-package-object (obj new-package)
    (declare (ignore obj new-package))
    (error "Switching object between packages not defined!")
)

; This function is intended to put all symbols in a typical s-expression tree
; into a particular package. It will ignore keyword symbols.
(defun switch-symbols-to-package (sym-tree new-package)
    (cond 
        ((null sym-tree) nil)
        ((pathnamep sym-tree) nil)
        ((is-object sym-tree) (switch-package-object sym-tree new-package))
        ((consp sym-tree) 
             (cons (switch-symbols-to-package (car sym-tree) new-package)
                   (switch-symbols-to-package (cdr sym-tree) new-package))
        )
        ((hash-table-p sym-tree) 
            (let 
              ((new-hash 
                  (make-hash-table
                      :test (hash-table-test sym-tree)
                      :size (hash-table-size sym-tree)
                      :rehash-size (hash-table-rehash-size sym-tree)
                      :rehash-threshold (hash-table-rehash-threshold sym-tree)
                  )
              ))
               (maphash 
                   #'(lambda (key val) 
                       (setf 
                           (gethash 
                               (switch-symbols-to-package key new-package) 
                               new-hash
                           ) 
                           (switch-symbols-to-package val new-package)
                       )
                     )
                   sym-tree
               )
               new-hash
           )
        )
        ((keywordp sym-tree) sym-tree)
        ((and (symbolp sym-tree) (not (eq (symbol-package sym-tree) (find-package :keyword)))) 
            (intern (symbol-name sym-tree) new-package))
        (t sym-tree)
    )
)

(defun is-in-tree (sym-tree symbol &key (test #'equal))
    (cond
        ((funcall test sym-tree symbol) t)
        ((null sym-tree) nil)
        ((listp sym-tree) 
            (or 
                (is-in-tree (car sym-tree) symbol :test test) 
                (is-in-tree (cdr sym-tree) symbol :test test)
            )
        )
        ((atom sym-tree) nil)
    )
)

(defun find-subtrees (tree predicate)
    (cond
        ((funcall predicate tree) (list tree))
        ((null tree) nil)
        ((listp tree) 
            (append
                (find-subtrees (car tree) predicate) 
                (find-subtrees (cdr tree) predicate) 
            )
        )
        ((atom tree) nil)
    )
)

(defun grep-trees (sym-trees symbol &key (test #'equal))
    (map-if+
        #'is-in-tree
        sym-trees
        symbol 
        :test test
    )
)

(defun maptree (fn tree)
    (cond
        ((null tree) nil)
        ((consp tree) (cons (maptree fn (car tree)) (maptree fn (cdr tree))))
        (t (funcall fn tree))
    )
)

(defun find-in-tree (tree fn)
    (cond
        ((null tree) nil)
        ((funcall fn tree) (list tree))
        ((listp tree) 
          (apply #'append 
              (mapcar+ #'find-in-tree tree fn)
          )
        )
        (t nil)
    )
)
      

(defun map-if (fun &rest lists)
    (remove nil (apply #'mapcar #'(lambda (x) (when (funcall fun x) x)) lists))
)

(defun map-if+ (fun list &rest other-args)
    (map-if #'(lambda (x) (apply fun x other-args)) list)
)

(defun find-lists-starting-with (tree symbols)
    (cond 
        ((null tree) nil)
        ((atom tree) nil)
        ((member (car tree) symbols) (list tree))
        ((listp tree) 
            (append 
                (find-lists-starting-with (car tree) symbols)
                (find-lists-starting-with (cdr tree) symbols)
            )
        )
    )
)

(defmacro trace-recursion (function-name)
#+:CLISP    `(trace (,function-name :step-if (>= system::*trace-level* 50)))
#-:CLISP    (declare (ignore function-name))
#-:CLISP    `(error "~%Only works in CLISP.")
)

;;Fortran-offset array-reference.  Fortran arrays start at 0, lisp
;;arrays at 1.
(defmacro fref (array &rest indicies)
  `(aref ,array
	 ,@(mapcar #'(lambda (index) `(1- ,index)) indicies)))

;;Return X with the sign of Y.  Various routines like this exist
;;in the book, but they differ about what to do when Y is 0.
;;This one gives X rather than -X.
(defun signp (x y)
  (if (minusp y) (- x) x))

    
(defvar *util-debug*)
(setq *util-debug* nil)

(defmacro debug-format (format-string &rest format-args)
    `(when *util-debug* (format t ,format-string ,@format-args))
)


#+allegro
(excl:defadvice 
  compile :before
  (let ((name (car excl::arglist))            
        (def (cadr excl::arglist)))
    (if (excl::interpreted-function-object-p def)
        (setq excl::arglist (list name (excl::fn_code def)))
    )
  )
)

(defmacro make-defun (var fun)
	`(defun ,var (x)
		(- (funcall ,fun x))
	 )
)


(defun dereference-symbols (tree)
    (cond 
        ((null tree) nil)
        ((symbolp tree) tree)
        ((and (listp tree) (eq (car tree) 'quote)) tree)
        ((and (listp tree) (vars-in tree)) (cons 'list (cons (list 'quote (car tree)) (mapcar #'dereference-symbols (cdr tree)))))
        (t tree)
    )
)


(defmacro replace-with-reduction (statement)
    `(algebra-reduce ,(dereference-symbols statement))
)

(defmacro replace-with-reduced-test (statement)
    ;; Special rules for evaluation when it's NOT qexpanded.
    ; (if (and (listp statement) (not (eq (car statement) 'list)))
        ; ;then
        ; (if (and (null (special-vars-in statement)) (null (vars-in statement)))
            ; ;then
            ; statement
            ; ;else
            ; `(algebra-indirect-test ',(dereference-symbols statement))
        ; )
        ;else
        (if (and (null (special-vars-in statement)) (null (vars-in statement)))
            ;then
            statement
            ;else
            `(algebra-indirect-test ,(dereference-symbols statement))
        )
    ; )
;    `(algebra-test ,(list 'quote (car statement)) ,@(cdr statement))
)

(defun algebra-indirect-test (test)
    (apply #'algebra-test test)
)

(defun algebra-test (ineq arg1 arg2)
    (let
      ((result (algebra-reduce `(- ,arg1 ,arg2))))
        (if (numberp result)
            ;then
            (or (funcall ineq result 0) (< (abs result) .0001))
            ;else
            nil
        )
    )
)


(defun algebra-reduce (statement)
    (cond
        ((null statement) nil)
        ((atom statement)
            statement
        )
        ((null (special-vars-in statement))
            (eval statement)
        )
        ((and (eq (car statement) '-) (equal (second statement) (third statement)))
            0
        )
        (t
            (mapcar #'algebra-reduce statement)
        )
    )
)

(defun find-quoted-portions (tree)
    (cond
        ((null tree) nil)
        ((atom tree) nil)
        ((eq (car tree) 'quote) (list tree))
        (t (apply #'append (mapcar #'find-quoted-portions tree)))
    )
)

(defun coerce-some-numbers (elem)
    (cond
        ((numberp elem) (coerce elem 'double-float))
        ((and (listp elem) (eq (car elem) 'expt))
            (list 'expt (coerce-some-numbers (second elem)) (third elem))
        )
        ((listp elem) (mapcar #'coerce-some-numbers elem))
        (t elem)
    )
)

(defun coerce-numbers (elem)
    (if (numberp elem)
        ;then
        (coerce elem 'double-float)
        ;else
        elem
    )
)

(defun subst-members (new a-set tree)
    (subst-if new #'(lambda (x) (member x a-set)) tree)
)

(defmacro push-all (list1 list2)
    `(progn
        (dolist (sym (reverse ,list1))
            (push sym ,list2)
        )
        ,list2
     )
)

;nthcdr from the other end
(defun ntop (num lista)
    (if (< (length lista) num)
        ;then
        lista
        ;else
        (reverse (nthcdr (- (length lista) num) (reverse lista)))
    )
)

;;short name for print-one-per-line (quick print).
(defun qp (some-list)
    (print-one-per-line some-list)
)

(defun print-one-per-line (lista &key (stream *standard-output*) &aux (ctr 0))
    (dolist (item lista)
        (format stream "~%~A. ~A" ctr item)
        (incf ctr)
    )
    nil
)

;; By Petter Gustad
;; Found online at 
;; http://coding.derkeiler.com/Archive/Lisp/comp.lang.lisp/2006-08/msg00698.html
(defun copy-file (inpathname outpathname &optional (buffersize #.(* 8 1024)))
    "a standard buffered file copy routine"
    (with-open-file (is inpathname :direction :input)
        (with-open-file (os outpathname :direction :output
                :if-exists :supersede
                :if-does-not-exist :create)
            (let 
              ((buffer 
                  (make-array buffersize :element-type (stream-element-type is))
              ))
                (do 
                  ((nread (read-sequence buffer is) (read-sequence buffer is))
                   (total 0 (+ total nread))
                  )
                    ((zerop nread) total)
                    (write-sequence buffer os :end nread)
                )
            )
        )
    )
)

#+ABCL
(defun beep ()
    (let* 
      ((toolkitclass (cl-user::jclass "java.awt.Toolkit"))
;       (intclass (jclass "int"))
       (toolkit (cl-user::jstatic "getDefaultToolkit" "java.awt.Toolkit"))
       (toolkitBeepMethod (cl-user::jmethod toolkitclass "beep"))
      )
      (cl-user::jcall toolkitBeepMethod toolkit)
    )
)

(defun round-off (elem)
    (cond
        ((member (type-of elem) '(single-float double-float)) 
            (round-to-significants elem 5)
        )
        ((typep elem 'fixnum) (coerce elem 'single-float)) 
        (t elem)
    )
)

(defun round-to-significants (elem digits)
    (when (< elem 0)
        (return-from round-to-significants 
            (- (round-to-significants (- elem) digits))
        )
    )
    (when (not (or (< elem 0) (> elem 0)))  
        (return-from round-to-significants 0f0)
    )
    (let
      ((upper (expt 10 digits))
       (lower (expt 10 (1- digits)))
       (times 1)
      )
        (when (> elem upper)
            (loop while (> elem upper) do
                (setq elem (/ elem 10))
                (setq times (* 10 times))
            )
            (return-from round-to-significants 
                (* times (round elem))
            )
        )
        (loop while (< elem lower) do
            (setq elem (* elem 10))
            (setq times (* 10 times))
        )
        (setq elem (float (/ (round elem) times)))
        (coerce elem 'single-float)
    )
)


(defun get-time-string ()
    (let ((time-values (multiple-value-list (get-decoded-time))))
        (format nil "~A/~A/~A ~A:~A:~A" 
            (fifth time-values)
            (fourth time-values)
            (sixth time-values)
            (third time-values)
            (second time-values)
            (first time-values)
        )
    )
)

(defun get-date-time-dashes ()
    (let ((time-values (multiple-value-list (get-decoded-time))))
        (format nil "~A-~A-~A-~A-~A-~A" 
            (fifth time-values)
            (fourth time-values)
            (sixth time-values)
            (third time-values)
            (second time-values)
            (first time-values)
        )
    )
)

(defmacro defun-print-readably (structure-name slot-names &aux get-fun fun-name args fun)
    (setq fun-name 
        (intern (concatenate 'string "PRINT-READABLY-" (symbol-name structure-name))
            (symbol-package structure-name)
        )
    )
    (setq args
        (mapcar 
            #'(lambda (slot-name)
                (setq get-fun
                    (intern 
                        (concatenate 'string 
                            (symbol-name structure-name) "-" 
                            slot-name
                        )
                        (symbol-package structure-name)
                    )
                )
                `(,get-fun obj) 
              )
            slot-names
        )
    )
    (setq fun
        `(defun ,fun-name (obj str k &aux lst)
            (declare (ignore k))
            (setq lst ,(cons 'list args))
            (pprint-logical-block 
                (str lst 
                :prefix "#S(" 
                :suffix ")")
                (write (quote ,structure-name) :stream str)
            )
         )
    )
    (dolist (slot-name slot-names)
        (setf (cdr (last (sixth fun)))
            (list
                `(write-char #\space str)
                `(pprint-newline :linear str)
                `(write ,(intern slot-name :keyword) :stream str)
                `(write-char #\space str)
                `(pprint-newline :linear str)
                `(write (pop lst) :stream str)
            )
        )
    )
    fun
)

; (defun hash-to-alist (hash &aux (alist nil))
    ; (maphash #'(lambda (key val) (push (cons key val) alist)) hash)
    ; alist
; )
; 
; 
; (defun alist-to-hash (alist &aux (hash (make-hash-table)))
    ; (mapcar #'(lambda (apair) (setf (gethash (car apair) hash) (cdr apair))) alist)
    ; hash
; )

; (defun instance-to-alist (inst &aux (alist nil) slot-name)
    ; (dolist (slot-def (clos::class-slots (class-of inst)))
        ; (setq slot-name (clos::slot-definition-name slot-def))
        ; (push (list slot-name (slot-value inst slot-name)) alist)
    ; )
    ; alist
; )
; 
; (defun to-tree (item &optional (hist nil))
    ; (cond
        ; ((member item hist) :repeat)
        ; ((and (listp item) (not (listp (cdr item))))
            ; (cons (to-tree (car item) (cons item hist)) 
                  ; (to-tree (cdr item) (cons item hist))
            ; )
        ; )
        ; ((listp item) (mapcar+ #'to-tree item (cons item hist)))
        ; ((hash-table-p item) (to-tree (hash-to-alist item) (cons item hist)))
        ; ((typep item 'structure-object) 
            ; (to-tree (instance-to-alist item) (cons item hist))
        ; )
        ; (t item)
    ; )
; )

(defun lexicographic-sort (any-lst)
    (if (null (cdr any-lst))
        ;then
        any-lst
        ;else
        (sort 
            (copy-list any-lst)
            #'string-lessp 
            :key #'write-to-string
        )
    )
)


;; Returns the first element of a list, and checks that the list in fact has only
;; the one element
(defun only (lst)
    (when (or (not (listp lst)) (null lst) (cdr lst))
        (error "Expected lst with one element.")
    )
    (first lst)
)

(defun starts-with (str1 str2)
    (and (> (length str2) (length str1))
         (equal (subseq str2 0 (length str1)) str1)
    )
)



;; MOP functions
(defun is-object (item)
    (or (typep item (find-class 'structure-object))
        (typep item (find-class 'standard-object))
    )
)

; (defun change-package-object (object new-package)
; #+ABCL (abcl-change-package-object object new-package)
; #-ABCL (mop-change-package-object object new-package)
; )
; 
; (defun mop-change-package-object (object new-package)
    ; (let*
      ; ((objclass (find-class (type-of object)))
       ; (slot-defs (class-slots objclass))
       ; (new-object (allocate-instance objclass))
      ; )
        ; (dolist (slot-def slot-defs)
            ; (let ((slot-name (slot-definition-name slot-def)))
                ; (setf (slot-value new-object slot-name)
                    ; (change-package (slot-value object slot-name) new-package)
                ; )
            ; )
        ; )
        ; new-object
    ; )
; )
; 
; (defun abcl-change-package-object (object new-package)
    ; (when (typep object (find-class 'standard-object))
        ; (return-from abcl-change-package-object 
            ; (mop-change-package-object object new-package)
        ; )
    ; )
    ; #+ABCL
    ; (let*
      ; ((objclass (find-class (type-of object)))
       ; (new-object (allocate-instance objclass))
      ; )
        ; (dotimes (counter (system:structure-length object))
            ; (system:structure-set new-object counter
                ; (change-package (system:structure-ref object counter) new-package)
            ; )
        ; )
        ; new-object
    ; )
; )

(defun hash-to-alist (hash &aux (alist nil))
    (maphash #'(lambda (key val) (push (cons key val) alist)) hash)
    alist
)


(defun alist-to-hash (alist &aux (hash (make-hash-table)))
    (mapcar #'(lambda (apair) (setf (gethash (car apair) hash) (cdr apair))) alist)
    hash
)

(defun combine-strings-symbols (&rest lst)
    (let ((name "") (str nil) (pkg *package*)) 
        (dolist (obj lst)
            (setq str nil)
            (when (symbolp obj)
                (setq str (symbol-name obj))
#+SBCL          (when (not (sb-ext:package-locked-p (symbol-package obj)))
                    (setq pkg (symbol-package obj))
                )
            )
            (when (stringp obj)
                (setq str obj)
            )
            (when (null str)
                (setq str (write-to-string obj))
            )
            (setq name (concatenate 'string name str))
        )
        (intern name pkg)
    )
)
 
(defun partial-order-sort (lst precedences &key (test #'eq) &aux sorted-list)
    (when (null lst)
        (return-from partial-order-sort nil)
    )
    (setq sorted-list (set-difference lst (mapcar #'second precedences) :test test))
    (when (null sorted-list)
        (setq precedences
            (remove-if #'(lambda (prec) (not (member (car prec) lst))) precedences)
        )
        (setq sorted-list (set-difference lst (mapcar #'second precedences) :test test))
    )
    
    (when (null sorted-list)
        (setq sorted-list
            (set-difference lst 
                (mapcar #'second (remove-if #'third precedences))
                :test test
            )
        )
    )
    
    (when (null sorted-list)
        (error "Panic!")
    )
    
    (append sorted-list
        (partial-order-sort 
            (set-difference lst sorted-list :test test)
            (remove-if #'(lambda (prec) (member (car prec) sorted-list)) precedences)
        )
    )
)

(defvar *fixnum-bits* 32)
#+ALLEGRO(setq *fixnum-bits* 30) 

(defun fixnum-left-shift (fixnum1 bits)
    (setq fixnum1 
        (dpb (ldb (byte (- *fixnum-bits* bits) 0) fixnum1) 
             (byte (- *fixnum-bits* bits) bits) fixnum1
        )
    )
    (dpb 0 (byte bits 0) fixnum1)
)


(defun fixnum-right-shift (fixnum1 bits &aux depo-num)
    (if (< fixnum1 0)
        ;then
        (setq depo-num -1)
        ;else
        (setq depo-num 0)
    )
    (setq fixnum1 
        (dpb (ldb (byte (- *fixnum-bits* bits) bits) fixnum1) 
             (byte (- *fixnum-bits* bits) 0) fixnum1
        )
    )
    (dpb depo-num (byte bits (- *fixnum-bits* bits)) fixnum1)
)


(defun random-subset (lst new-length &aux new-lst)
    (when (< (length lst) new-length)
        (return-from random-subset lst)
    )
    (loop while (< (length new-lst) new-length) do
        (pushnew (nth (random (length lst)) lst) new-lst)
    )
    new-lst
)

(defun insert-in-ordered-list (item lst test-fn &aux tail)
    (when (funcall test-fn item (car lst))
        (return-from insert-in-ordered-list (cons item lst))
    )
    (setq tail lst)
    (loop while (cdr tail) do
        (when (funcall test-fn item (second lst))
            (setf (cdr tail) (cons item (cdr tail)))
            (return-from insert-in-ordered-list lst)
        )
        (setq tail (cdr tail))
    )
    (setf (cdr tail) (list item))
    lst
)

(defun find-min (lst &key (key #'identity) (test #'<) &aux ret ret-key)
    (setq ret (car lst))
    (setq ret-key (funcall key ret))
    (dolist (item (cdr lst))
        (when (funcall test (funcall key item) ret-key)
            (setq ret item)
            (setq ret-key (funcall key item))
        )
    )
    ret
)

(defclass reservoir-sampler ()
    (
        (size 
            :initarg :size 
            :initform 0
        )
        (reservoir
            :initform nil
        )
        (min-value 
            :initform -1
        )
        (population-size
            :initform 0
            :accessor get-population-size
        )
    )
)

(defun new-reservoir-sampler (size)
    (make-instance 'reservoir-sampler :size size)
)

(defun split (item lst &key (key #'identity) (test #'eq) &aux (tlist nil) (flist nil))
    (dolist (lst-item lst)
        (if (funcall test item (funcall key lst-item))
            ;then
            (push lst-item tlist)
            ;else
            (push lst-item flist)
        )
    )
    (values tlist flist)
)

(defun split-if (fn lst &aux (tlist nil) (flist nil))
    (dolist (item lst)
        (if (funcall fn item)
            ;then
            (push item tlist)
            ;else
            (push item flist)
        )
    )
    (values tlist flist)
)

(defmethod sample ((sampler reservoir-sampler) item &aux new-value)
    (setq new-value (random most-positive-fixnum))
    (incf (get-population-size sampler))
    (with-slots (size reservoir min-value) sampler
        (when (< new-value min-value)
            (return-from sample nil)
        )
        (setf reservoir 
            (merge 'list 
                (list (list item new-value)) 
                reservoir
                #'> :key #'second
            )
        )
        (when (> (length reservoir) size)
            (setf min-value (second (nth (1- size) reservoir)))
            (setf reservoir (remove min-value reservoir :test #'> :key #'second))
        )
    )
)
    
(defmethod get-results ((sampler reservoir-sampler) &aux ret)
    (with-slots (size reservoir) sampler
        (setq ret (copy-list reservoir))
        (loop while (> (length ret) size) do
            (setq ret (remove (nth (random (length ret)) ret) ret))
        )
    )
    (mapcar #'first ret)
)

(defun count-unique (lst &key (key #'identity) (test #'eq) &aux (counts nil) (count-pair nil))
    (dolist (item lst)
        (setq count-pair (assoc item counts :key key :test test))
        (if (null count-pair)
            ;then
            (push (cons item 1) counts)
            ;else
            (incf (cdr count-pair))
        )
    )
    counts
)
    

(defun cycle-has-formed (new-occs occ-hist &key (test #'equal))
    (let ((idx (search new-occs occ-hist :test test :start2 (length new-occs)))
           seq1 seq2
         )
        (when (null idx) 
            (return-from cycle-has-formed nil)
        )
        (when (< (length occ-hist) (* idx 2))
            (return-from cycle-has-formed nil)
        )
        (setq seq1 (subseq occ-hist 0 idx))
        (setq seq2 (subseq occ-hist idx (* idx 2)))
        (null (mismatch seq1 seq2 :test test))
    )
)

(defun cycle-at-start (header lst &key (test #'equal))
    (let ((idx (length header))
           seq1 seq2
         )
        (loop do
            (setq idx (search header lst :test test :start2 idx))
            (when (null idx)
                (return-from cycle-at-start nil)
            )
            (when (< (length lst) (* idx 2))
                (return-from cycle-at-start nil)
            )
            (setq seq1 (subseq lst 0 idx))
            (setq seq2 (subseq lst idx (* idx 2)))
            (when (null (mismatch seq1 seq2 :test test))
                (return-from cycle-at-start t)
            )
            (incf idx)
        )
    )
)

(defun arg-fn (num)
    #'(lambda (&rest args) (nth num args))
)

(defun precedes (iteme iteml lst &key (test #'equal))
    (when (null lst)
        (return-from precedes nil)
    )
    (cond
        ((funcall test (car lst) iteme) t)
        ((funcall test (car lst) iteml) nil)
        (t (precedes iteme iteml (cdr lst)))
    )
)
