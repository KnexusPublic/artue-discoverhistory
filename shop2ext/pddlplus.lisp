 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: Jun 3, 2009
 ;;;; Project Name: ARTUE
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This class converts extra types from PDDL+, processes and events, into 
 ;;;; a format useable by SHOP.

(in-package :shop2)
(export '(def-pddlplus-domain make-type-declarations decompose-types 
          *use-continuous-time* pddl-plus-domain 
          *time-step-length*
          increase decrease ?__dur shop2.common:domain
          round-to-time-step
          parameters-of precondition-of effect-of name-of
          action-named event-named process-named
          domain-hidden domain-hidden-types domain-internal domain-events 
          domain-supertypes domain-id
          domain-predicates domain-functions domain-func-defs domain-defaults
          neq
          action-named event-named process-named
          domain-constants domain-file domain-processes
          output-domain make-pddl-param-list initializing-beliefs
          int
         ) 
    :shop2)
(use-package :utils)


(defvar *use-continuous-time* t)
(defvar *calculate-minimum* t)
(defvar *time-step-length* -1)
(defvar *gen-funs* nil)
(defvar *ignore-functionless-processes* nil)

(when (not (find-package "SHOP2.FUNCTIONS"))
    (make-package "SHOP2.FUNCTIONS")
)

; (defun get-pddlplus-domain-from-file (file)
    ; (set-dispatch-macro-character #\# #\t        ;dispatch on #t
        ; #'(lambda (s c n) (declare (ignore s c n)) '?__dur)
    ; )
    ; (let ((dom-list nil))
; ;; MEK [10/6/2009] This seems excessive, because it makes all the symbols be in the shop2 package
; ;; I would much prefer to just add shop2:: to the define and domain in the file
        ; (let ((old-package *package*))
            ; (in-package :shop2)
            ; (with-open-file (in-stream file)
                ; (setq dom-list (read in-stream))
            ; )
            ; (eval `(in-package ,(package-name old-package)))
        ; )
        ; (eval dom-list)
        ; (format t "~%Loaded domain from file: ~A" file)
    ; )
; )

; (defmacro define (&rest domain)
    ; (cond 
        ; ((eq (caar domain) 'domain) 
            ; (let ((domain-name (cadr (assoc 'domain domain))))
                ; `(shop2:defdomain (,domain-name :type pddl-plus-domain) ,(cddr domain))
            ; )
        ; )
        ; ((eq (caar domain) 'problem) `(read-pddl-problem ',domain))
    ; )
; )

(defclass process-mixin ()
   ()
)

(defclass event-mixin ()
   ()
)

(defclass pddl-plus-domain ( pddl-domain process-mixin event-mixin )
  (
    (id
        :accessor domain-id
        :initform nil
    )

    (full-actions
        :accessor domain-full-actions
        :initform nil
    )
    
    (events
       :initarg :events
       :initform nil
       :accessor domain-events

    )
    (processes
       :initarg :processes
       :initform nil
       :accessor domain-processes
    )
    (functions
       :initarg :functions
       :initform nil
       :accessor domain-functions
    )
    (func-defs
       :initarg :func-defs
       :initform nil
       :reader domain-func-defs
    )
    (supertypes
       :initform nil
       :reader domain-supertypes
    )
    (function-names
       :initform nil
       :reader domain-function-names
    )
    (continuous-function-names
       :initform nil
       :accessor domain-continuous-function-names
    )
    (hidden 
       :initform nil
       :accessor domain-hidden
    )
    (hidden-types
       :initform nil
       :accessor domain-hidden-types
    )
    (internal 
       :initform nil
       :accessor domain-internal
    )
    (defaults
       :initform nil
       :reader domain-defaults
    )
    (constants 
       :initform nil
       :reader domain-constants
    )
    (remainder
       :initform nil
       :accessor domain-remainder
    )
    (file 
       :initform nil
       :accessor domain-file
    )
  )
  (:documentation "A new class of SHOP2 domain that permits inclusion of
                   PDDL operator definitions."
  )
)

(defgeneric output-domain (domain))
(defmethod output-domain ((domain pddl-plus-domain))
;    (desanitize-items
        domain
        `((domain ,(domain-name domain))
            (:types ,@(domain-types domain))
            (:predicates ,@(domain-predicates domain))
            (:hidden ,@(domain-hidden domain))
            (:hidden-types ,@(domain-hidden-types domain))
            (:defaults ,@(domain-defaults domain))
            (:functions ,@(domain-func-defs domain))
            (:supertypes ,@(domain-supertypes domain))
            ,@(domain-remainder domain)
            ,@(domain-full-actions domain)
            ,@(mapcar #'output-event (domain-events domain))
            ,@(mapcar #'output-process (domain-processes domain))
         )
;    )
)


(defgeneric domain-objects (domain))
(defmethod domain-objects ((domain pddl-plus-domain))
    (append (domain-full-actions domain) (domain-events domain) (domain-processes domain))
)


(defstruct pddl-process 
    name
    parameters
    precondition
    effect
    (functions nil)
    (events nil)
    domain
)

(defun output-process (process)
    `(:process ,(pddl-process-name process)
      :parameters ,(pddl-process-parameters process)
      :precondition ,(pddl-process-precondition process)
      :effect ,(pddl-process-effect process)
     )
)

(defstruct pddl-event
    name
    parameters
    precondition
    likelihood
    effect
    (static-preconditions nil)                                                
    (functions nil)
    domain
    (initial nil)
)

(defun output-event (event)
    `(:event ,(pddl-event-name event)
      :parameters 
        ,(make-pddl-param-list 
            (mapcar #'first (pddl-event-parameters event))
            (pddl-event-parameters event)
         )
      :likelihood ,(pddl-event-likelihood event)
      :precondition ,(pddl-event-precondition event)
      :effect ,(pddl-event-effect event)
     )
)

(defstruct pddl-function
    name
    parameters
    param-types
    type
)

(defun output-functions (domain)
    (apply #'append (mapcar #'output-function (domain-functions domain)))
)

(defun output-function (func)
    (list
        (cons
            (pddl-function-name func)
            (apply #'append
                (mapcar #'list
                    (pddl-function-parameters func)
                    (make-list (length (pddl-function-parameters func)) 
                        :initial-element '-
                    )
                    (pddl-function-param-types func)
                )
            )
        )
        '-                                                   
        (pddl-function-type func)
    )
            
)

(defgeneric name-of (p))
(defmethod name-of ((p pddl-process))
    (pddl-process-name p)
)

(defmethod name-of ((p pddl-event))
    (pddl-event-name p)
)

(defmethod name-of ((p cons))
    (car (pddl-action-head p))
)

(defgeneric precondition-of (p))
(defmethod precondition-of ((p pddl-process))
    (pddl-process-precondition p)
)

(defmethod precondition-of ((p pddl-event))
    (pddl-event-precondition p)
)

(defmethod precondition-of ((p cons))
    (pddl-action-precondition p)
)

(defgeneric effect-of (p))
(defmethod effect-of ((p pddl-process))
    (pddl-process-effect p)
)

(defmethod effect-of ((p pddl-event))
    (pddl-event-effect p)
)

(defmethod effect-of ((p cons))
    (pddl-action-effect p)
)

(defgeneric parameters-of (p))
(defmethod parameters-of ((p pddl-process))
    (pddl-process-parameters p)
)

(defmethod parameters-of ((p pddl-event))
    (pddl-event-parameters p)
)

(defmethod parameters-of ((p cons))
    (rest (second p))

)

(defun event-named (domain name)
    (find name (domain-events domain) :key #'pddl-event-name :test #'equal)
)

(defun process-named (domain name)
    (find name (domain-processes domain) :key #'pddl-process-name :test #'equal)
)

(defun action-named (domain name)
    (gethash name (domain-operators domain))

)

(defgeneric get-effects (process))
(defmethod get-effects ((process pddl-process))
    (let ((eff (pddl-process-effect process)))
        (when (null eff)
            (return-from get-effects nil)
        )
        (when (eq (car eff) 'and)
            (setq eff (cdr eff))
        )
        (when (not (listp (car eff)))
            (setq eff (list eff))
        )
        eff
    )
)
    
(defmethod parse-domain-item :around ((domain simple-pddl-domain) (item-key (eql ':types)) item)
    (with-slots (predicates) domain
        (setf predicates (append predicates (mapcar #'create-is-predicate (cdr item))))
    )
    (call-next-method domain item-key item)
    (values)
)

(defun create-is-predicate (type-name)
    (list
        (make-is-type type-name)
        '?obj
        '-
        ; MCM (3/23/2016): Replaced type-name with :any because should return 
        ;                  nil for objects of other types.
        :any
    )
)

(defun make-is-type (type-name &aux sym)
    (setq sym (combine-strings-symbols 'is- type-name))
    (setf (get sym :is) type-name)
    sym
)

(defun is-is-literal (lit)
    (get (first lit) :is)
)

(defun get-is-literal-var-type (lit)
    (get (first lit) :is)
)
    

(defmethod parse-domain-item ((domain pddl-plus-domain) (item-key (eql ':process)) item)
    (with-slots (processes) domain
;        (setq item (sanitize-items domain item))
        (when (not (member :parameters item))
            (error "Process description missing :parameters field: ~A" item)
        )
        (setf 
            (cadr (member :parameters item)) 
            (make-type-declarations (cadr (member :parameters item)))
        )
        (let ((new-process (apply #'make-pddl-process :domain domain :name (cdr item))))
            (push new-process processes)
            (dolist (eff (get-effects new-process))
                (when (listp (second eff))
                    (push (first (second eff)) (domain-continuous-function-names domain))
                )
            )
        )
    )
)

(defgeneric check-process (process domain))
(defmethod check-process ((process pddl-process) domain)
    (clear-types (pddl-process-precondition process))
    (clear-types (pddl-process-effect process))
    (clear-types (pddl-process-parameters process))
    (dolist (pair (pddl-process-parameters process))
        (test-type (second pair) (first pair) domain)
    )
    (let ((used-functions 
              (intersection 
                  (symbols-in (pddl-process-precondition process)) 
                  (domain-continuous-function-names domain)
              )
         ))
        (when (not (null used-functions))
            (error "The values of continuous functions ~A are referred to in the preconditions of process ~A; this is not supported. Create events and discrete facts to control when the process starts and stops."
                used-functions
                (pddl-process-name process)
            )
        )
    )
    (when 
      (member '?__dur (vars-in (pddl-process-precondition process)))
        (error "The #t symbol was found in the preconditions of process ~A. This symbol is only allowed in process effects."
            (pddl-process-name process)
        )
    )
    (dolist (effect (get-effects process))
        (unless 
            (and (member (car effect) '(set increase decrease)) 
                (or (member '?__dur (vars-in (third effect)))
                    (intersection (symbols-in (third effect))
                        (domain-continuous-function-names domain)
                    )
                )
            )
                    ; (find-in-tree 
                        ; (third effect)
                        ; #'(lambda (x) 
                            ; (is-funcall x)
                          ; )
                    ; )
            (error "The effect ~A of process ~A is discrete; processes may only have continuous effects."
                effect (pddl-process-name process)
            )
        )
    )
    
    (check-preconditions "process" (pddl-process-name process) (pddl-process-precondition process) domain)
    (check-effects "process" (pddl-process-name process) (pddl-process-effect process) domain)
    (clear-types (pddl-process-precondition process))
    (clear-types (pddl-process-effect process))
    (clear-types (pddl-process-parameters process))

    (when (not *use-continuous-time*)
        (with-slots (effect) process
            (setq effect (subst 1 '?__dur effect))
        )
    )
)

(defmethod parse-domain-item :around ((domain simple-pddl-domain) (item-key (eql ':action)) item)
    (with-slots (full-actions) domain
        (push (copy-list item) full-actions)
    )
    (call-next-method domain item-key item) ;(sanitize-items domain item))
)

(defmethod parse-domain-item :around ((domain pddl-plus-domain) (item-key (eql ':method)) item)
    (call-next-method domain item-key item); (sanitize-items domain item))
)




;List of hidden predicates.

(defmethod parse-domain-item ((domain pddl-plus-domain) (item-key (eql ':hidden)) item)
    (with-slots (hidden) domain
        (setq hidden (append hidden (cdr item)))
    )
)

;List of hidden types.

(defmethod parse-domain-item ((domain pddl-plus-domain) (item-key (eql ':hidden-types)) item)
    (with-slots (hidden-types) domain
        (setq hidden-types (append hidden-types (cdr item)))
    )
)

;List of internal predicates. Does not need parsing, yet.

(defmethod parse-domain-item ((domain pddl-plus-domain) (item-key (eql ':internal)) item)
    (with-slots (internal) domain
        (setq internal (append internal (cdr item)))
    )
)

(defmethod parse-domain-item ((domain pddl-plus-domain) (item-key (eql ':defaults)) item)
    (with-slots (defaults) domain
        (setq defaults (append defaults (cdr item)))
    )
)

(defmethod parse-domain-item ((domain pddl-plus-domain) (item-key (eql ':constants)) item)
    (with-slots (constants) domain
        (setq constants (append constants (cdr item)))
    )
)

(defmethod parse-domain-item ((domain pddl-plus-domain) (item-key (eql ':event)) item)
    (let 
      ((event nil))
        (with-slots (events) domain
;            (setq item (sanitize-items domain item))
            (setf 
                (cadr (member :parameters item)) 
                (make-type-declarations (cadr (member :parameters item)))
            )
            (when (getf item :initial)
                (let ((precs (getf item :precondition)))
                    (cond 
                        ((null precs) (setq precs '(initializing-beliefs)))
                        ((eq (first precs) 'and) 
                            (setq precs (append '(and (initializing-beliefs)) (cdr precs))))
                        (t (setq precs (append '(and (initializing-beliefs)) precs)))
                    )
                    (setf (getf item :precondition) precs)
                )
            )
                        
            (setq event (apply #'make-pddl-event :domain domain (subst :name :event item)))
;            (when (member 'assign (symbols-in (pddl-event-precondition event)))
            (push event events)
        )
    )
)

(defmethod parse-domain-item ((domain pddl-plus-domain) (item-key (eql ':functions)) item)
    ;;handled in parse-functions
    (declare (ignore domain item-key item))
)

(defmethod parse-domain-item ((domain pddl-plus-domain) (item-key (eql ':file)) item)
    (setf (domain-file domain) (cdr item))
)

(defgeneric check-event (event domain))
(defmethod check-event ((event pddl-event) domain)
    (clear-types (pddl-event-precondition event))
    (clear-types (pddl-event-effect event))
    (clear-types (pddl-event-parameters event))
    (dolist (pair (pddl-event-parameters event))
        (test-type (second pair) (first pair) domain)
    )
    (when (member '?__dur (vars-in (pddl-event-precondition event)))
        (error "The #t symbol was found in event ~A. This symbol is only allowed in process effects."
            (pddl-event-name event)
        )
    )
    (when (member '?__dur (vars-in (pddl-event-effect event)))
        (error "The #t symbol was found in event ~A. This symbol is only allowed in process effects."
            (pddl-event-name event)
        )
    )
    (when (find-subtrees (pddl-event-precondition event) #'(lambda (x) (is-function-use x (pddl-event-domain event))))
        (error "Assignment from function value found in event ~A; this is not supported."
            (pddl-event-name event)
        )
    )
    (check-preconditions "event" (pddl-event-name event) (pddl-event-precondition event) domain)
    (check-effects "event" (pddl-event-name event) (pddl-event-effect event) domain)
    (clear-types (pddl-event-precondition event))
    (clear-types (pddl-event-effect event))
    (clear-types (pddl-event-parameters event))
)

(defun check-action (action domain)
    (let ((precs (getf action :precondition))
          (effs (getf action :effect))
          (params (getf action :parameters))
          (name (getf action :action))
          )
        (when (starts-with "INTERNAL" (symbol-name name))
            (return-from check-action nil)
        )
        (clear-types (list name precs params))
        (dolist (pair (make-type-declarations params))
            (test-type (second pair) (first pair) domain)
        )
        (when (member '?__dur (vars-in precs))
            (error "The #t symbol was found in action ~A. This symbol is only allowed in process effects."
                name
            )
        )
        (when (member '?__dur (vars-in effs))
            (error "The #t symbol was found in action ~A. This symbol is only allowed in process effects."
                name
            )
        )
        (check-preconditions "action" name precs domain)
        (check-effects "action" name effs domain)
        (clear-types (list name precs params))
    )
)

(defun clear-types (lst)
    (dolist (var (vars-in lst))
        (setf (get var 'type) nil)
    )
)
    

(defun check-preconditions (ob-type title precs domain)
    (when (eq (car precs) 'and)
        (setq precs (cdr precs))
    )
    (when (and (atom (car precs)) (not (null precs)))
        (setq precs (list precs))
    )
    (dolist (prec precs)
        (case (car prec)
            ((and or) 
                (check-preconditions ob-type title (cdr prec) domain)
            )
            (eval 
                (when (null (cdr prec))
                    (error "Expected Lisp statement as argument to eval in preconditions of ~A ~A, found none"
                        ob-type title
                    )
                )
                (when (cddr prec)
                    (error "Expected one argument to eval in preconditions of ~A ~A, found ~A: ~A"
                        ob-type title (- (length prec) 1) prec
                    )
                )
            )
            (set
                (error "Illegal set statement found in effects of ~A ~A."
                    ob-type title
                )
            )
            
            (create
                (when (or (cddddr prec) (< (length prec) 4)) 
                    (error "Expected three arguments to create in preconditions of ~A ~A, found ~A: ~A"
                        ob-type title (- (length prec) 1) prec
                    )
                )
                (when (not (symbolp (second prec)))
                    (error "Expected type symbol as first argument to create in precondition of ~A ~A: ~A"
                        ob-type title prec
                    )
                )
                (when (not (variablep (third prec)))
                    (error "Expected variable as second argument to create in precondition of ~A ~A: ~A"
                        ob-type title prec
                    )
                )
                (when (find-var-type (third prec))
                    (error "Expected unbound variable as second argument to create in precondition of ~A ~A: ~A"
                        ob-type title prec
                    )
                )
                (set-var-type (third prec) (second prec))
            )

            (is
                (when (or (cdddr prec) (< (length prec) 3)) 
                    (error "Expected two arguments to is in preconditions of ~A ~A, found ~A: ~A"
                        ob-type title (- (length prec) 1) prec
                    )
                )
                (when (not (symbolp (second prec)))
                    (error "Expected type symbol as first argument to is in precondition of ~A ~A: ~A"
                        ob-type title prec
                    )
                )
                (when (not (variablep (third prec)))
                    (error "Expected variable as second argument to is in precondition of ~A ~A: ~A"
                        ob-type title prec
                    )
                )
                (test-type (third prec) (second prec) domain)
            )

            (assign
                (when (null (cdr prec))
                    (error "Expected variable and Lisp statement as arguments to assign in preconditions of ~A ~A, found ~A"
                        ob-type title prec
                    )
                )
                (when (cdddr prec)
                    (error "Expected two arguments to assign in preconditions of ~A ~A, found ~A: ~A"
                        ob-type title (- (length prec) 1) prec
                    )
                )
                (when (not (variablep (second prec)))
                    (error "Expected variable as first argument to assign in precondition of ~A ~A: ~A"
                        ob-type title prec
                    )
                )
            )
            ((= <= >= < >)
                (when (or (cdddr prec) (null (cddr prec)))
                    (error "Expected two arguments to ~A in preconditions of ~A ~A, found ~A: ~A"
                        (car prec) ob-type title (- (length prec) 1) prec
                    )
                )
                (when (not (test-numeric (second prec) domain t))
                    (error "First argument of inequality is non-numeric: ~A in ~A ~A."
                        prec ob-type title
                    )
                )
                (when (not (test-numeric (third prec) domain t))
                    (error "Second argument of inequality is non-numeric: ~A in ~A ~A."
                        prec ob-type title
                    )
                )
                (when (and (is-funcall (second prec) domain) (not (test-function (second prec) 'numeric domain)))
                    (error "Function ~A in precondition ~A of ~A ~A is poorly formed or non-numeric."
                        (first (second prec)) prec ob-type title
                    )
                )
                (when (and (is-funcall (third prec) domain) (not (test-function (third prec) 'numeric domain)))
                    (error "Value of precondition ~A of ~A ~A is poorly formed or non-numeric."
                        prec ob-type title
                    )
                )
            )
            ((eq neq) 
                (when (listp (third prec))
                    (error "Value of literal ~A in ~A ~A can not be set by a function."
                        prec ob-type title
                    )
                )
                (test-eq-literal prec domain 
                    (format nil "preconditions of ~A ~A" ob-type title)
                )
            )
                
            (not
                (when (or (null (cdr prec)) (cddr prec))
                    (error "Expected one argument to not in preconditions of ~A ~A, found ~A: ~A"
                        ob-type title (- (length prec) 1) prec
                    )
                )
                (when (not (test-predicate (second prec) domain))
                    (error "Literal ~A is badly formed in ~A ~A."
                        prec ob-type title
                    )
                )
            )
            (t 
                (when (not (test-predicate prec domain))
                    (error "Literal ~A is badly formed in ~A ~A."
                        prec ob-type title
                    )
                )
            )
        )
    )
)

(defun check-effects (ob-type title effs domain)
    (when (eq (car effs) 'and)
        (setq effs (cdr effs))
    )
    (when (and (atom (car effs)) (not (null effs)))
        (setq effs (list effs))
    )
    (dolist (eff effs)
        (case (car eff)
            ((and or) 
                (error "Illegal ~A statement found in effects of ~A ~A."
                    (car eff) ob-type title
                )
            )
            (eval 
                (error "Illegal eval statement found in effects of ~A ~A."
                    ob-type title
                )
            )
            (assign
                (when (null (cdr eff))
                    (error "Expected variable and Lisp statement as arguments to assign in effects of ~A ~A, found ~A"
                        ob-type title eff
                    )
                )
                (when (cdddr eff)
                    (error "Expected two arguments to assign in effects of ~A ~A, found ~A: ~A"
                        ob-type title (- (length eff) 1) eff
                    )
                )
                (when (not (variablep (second eff)))
                    (error "Expected variable as first argument to assign in effects of ~A ~A: ~A"
                        ob-type title eff
                    )
                )
            )
            (create
                (error "Illegal create statement found in effects of ~A ~A."
                    ob-type title
                )
            )
            (eq
                (error "Illegal eq statement found in effects of ~A ~A. Use set."
                    ob-type title
                )
            )
            (neq
                (error "Illegal eq statement found in effects of ~A ~A. Use set."
                    ob-type title
                )
            )
            (set 
                (test-eq-literal eff domain 
                    (format nil "effects of ~A ~A" ob-type title)
                )
            )
            ((increase decrease)
                (when (null (cdr eff))
                    (error "Expected function and Lisp statement as arguments to ~A in preconditions of ~A ~A, found ~A"
                        (car eff) ob-type title eff
                    )
                )
                (when (cdddr eff)
                    (error "Expected two arguments to ~A in preconditions of ~A ~A, found ~A: ~A"
                        (car eff) ob-type title (- (length eff) 1) eff
                    )
                )
                (when (not (test-function (second eff) 'numeric domain))
                    (error "Incorrect statement ~A in ~A ~A."
                        eff ob-type title
                    )
                )
            )
            ((= <= >= < >)
                (error "Illegal comparison statement found in effects of ~A ~A."
                    ob-type title
                )
            )
            (not
                (when (or (null (cdr eff)) (cddr eff))
                    (error "Expected one argument to not in effects of ~A ~A, found ~A: ~A"
                        ob-type title (- (length eff) 1) eff
                    )
                )
                (when (not (test-predicate (second eff) domain))
                    (error "Literal ~A is badly formed in ~A ~A."
                        eff ob-type title
                    )
                )
            )
            (t 
                (when (not (test-predicate eff domain))
                    (error "Literal ~A is badly formed in ~A ~A."
                        eff ob-type title
                    )
                )
            )
        )
    )
)

(defun test-numeric (item domain lisp-allowed)
    (when (numberp item)
        (return-from test-numeric t)
    )
    (when (variablep item)
        (let ((var-type (find-var-type item)))
            (when (null var-type)
                (set-var-type item 'numeric)
                (return-from test-numeric t)
            )
            (return-from test-numeric (is-numeric-type var-type domain))
        )
    )
    (when (and (listp item) (car item))
        (let ((func (find (car item) (domain-functions domain) :key #'pddl-function-name)))
            (when (null func)
                (return-from test-numeric (and lisp-allowed (fboundp (car item))))
            )
            (return-from test-numeric (is-numeric-type (pddl-function-type func) domain))
        )
    )
    nil
)

(defun find-var-type (var)
    (get var 'type)
)

(defun set-var-type (var ret-type)
    (setf (get var 'type) ret-type)
)

(defun test-predicate (literal domain)
    (when (is-forall literal)
        (return-from test-predicate t)
    )
    (let ((pred-def (assoc (car literal) (domain-predicates domain))))
        (when (null pred-def)
            (format t "Predicate type ~A not found." (car literal))
            (return-from test-predicate nil)
        )
        (let ((ptypes (make-type-declarations (cdr pred-def))))
            (when (not (= (length ptypes) (length (cdr literal))))
                (return-from test-predicate nil)
            )
            (mapcar 
                #'(lambda (var-type item) 
                    (when (not (test-type var-type item domain))
                        (return-from test-predicate nil)
                    )
                  )
                (mapcar #'second ptypes) 
                (cdr literal)
            )
            (when (is-is-literal literal)
                (set-var-type (second literal) (get-is-literal-var-type literal))
            )
        )
    )
    t
)

(defun test-eq-literal (lit domain loc-str)
    (when (or (null lit) (cdddr lit)) 
        (error "Expected two arguments to eq in ~A, found ~A: ~A"
            loc-str (- (length lit) 1) lit
        )
    )
    (let ((func (get-domain-function (caadr lit) domain)))
        (when (null func)
            (error "Function type ~A seen in ~A not found." (caadr lit) loc-str)
        )
        (when (and (not (listp (third lit))) 
                   (not (test-type (pddl-function-type func) (third lit) domain)))
            (error "Type of function ~A does not match constraint ~A in ~A: ~A"
                (caadr lit) (third lit) loc-str lit 
            )
        )
        (when (not (test-function (second lit) (pddl-function-type func) domain))
            (error "Literal ~A is badly formed in ~A."
                lit loc-str
            )
        )
    )
)

(defun get-domain-function (name domain)
    (when (gethash name *fun-name-map*)
        (setq name (gethash name *fun-name-map*))
    )
    (find name (domain-functions domain) :key #'pddl-function-name)
)

(defun test-function (literal ret-type domain)
    (let ((func (get-domain-function (car literal) domain)))
        (when (null func)
            (return-from test-function nil)
        )
        (let ((ftypes (pddl-function-param-types func)) 
              (func-type (pddl-function-type func))
             )
            (when (not (= (length ftypes) (length (cdr literal))))
                (return-from test-function nil)
            )
            (when 
                (notevery 
                    #'(lambda (var-type item) (test-type var-type item domain))
                    ftypes 
                    (cdr literal)
                )
                (format t "~%Arguments in literal ~A can not be constrained to match declared predicate parameter types." literal)
                (return-from test-function nil)
            )
            (if (eq ret-type 'numeric)
                ;then
                (return-from test-function (is-numeric-type func-type domain))
                ;else
                (return-from test-function (eq ret-type func-type))
            )
        )
    )
)
    
(defun test-type (var-type item domain)
    (when (and (not (variablep item)) (is-numeric-type var-type domain))
        (return-from test-type (test-numeric item domain nil))
    )
    (when (not (symbolp item))
        (return-from test-type nil)
    )
    (let ((item-var-type (find-var-type item)))
        (when (null item-var-type)
            (set-var-type item var-type)
            (return-from test-type t)
        )
        (when (or (eq var-type 'numeric) (eq var-type 'real))
            (return-from test-type (is-numeric-type item-var-type domain))
        )
        (when (is-supertype item-var-type var-type domain)
            (set-var-type item var-type)
            (return-from test-type t)
        )
        (is-supertype var-type item-var-type domain)
    )
)

(defun is-supertype (var-type value-type domain)
    (when (eq var-type value-type) 
        (return-from is-supertype t)
    )
    (when (null value-type)
        (return-from is-supertype nil)
    )
    (is-supertype 
        var-type
        (second (assoc value-type (domain-supertypes domain)))
        domain
    )
)

(defun is-numeric-type (var-type domain)
    (intersection (cons var-type (get-supertypes domain var-type)) 
                  '(numeric int real)
    )
)

(defgeneric parse-functions (domain item))
(defmethod parse-functions ((domain pddl-plus-domain) item)
    (with-slots (functions func-defs) domain
;        (setq functions (mapcar #'parse-pddl-function (remove-if-not #'listp item)))
        (setq functions 
            (append functions
                (mapcar #'parse-pddl-function (make-type-declarations (cdr item)))
            )
        )
        (setq func-defs 
            (append func-defs
                (cdr item)
;                (sanitize-items domain (cdr item))
            )
        )
    )
)

(defun parse-pddl-function (function-list)
    (make-pddl-function 
        :name (caar function-list)
        :parameters (mapcar #'first (make-type-declarations (cdar function-list)))
        :param-types (mapcar #'second (make-type-declarations (cdar function-list)))
        :type (cadr function-list)
    )
)
        
(defmethod parse-domain-item ((domain pddl-plus-domain) (item-key (eql ':supertypes)) item)
    (with-slots (supertypes) domain
        (setq supertypes (cdr item))
    )
)

        
;; Default fallback method 
(defmethod parse-domain-item (domain item-key item)
)

(defconstant *known-item-keys* 
    '(:types :predicates :defaults :functions :supertypes :action :event :process :hidden)
)

(defmethod parse-domain-items :around ((domain pddl-plus-domain) items)
    ; (if (find-package "SHOP2.FUNCTIONS") 
        ; (delete-package "SHOP2.FUNCTIONS")
    ; )
    ; (make-package "SHOP2.FUNCTIONS")
    (when (not (find-package "SHOP2.FUNCTIONS"))
        (make-package "SHOP2.FUNCTIONS")
    )
    (setf (domain-id domain) (domain-name domain))
    (parse-functions domain (assoc ':functions items))
    (call-next-method domain items)
    (establish-functions domain)
    (establish-cross-refs domain)
    (setf (domain-remainder domain)
        (append (domain-remainder domain)
            (remove-if #'(lambda (key) (member key *known-item-keys*)) items :key #'car)
        )
    )
)



(defgeneric establish-functions (domain))
(defmethod establish-functions ((domain pddl-plus-domain))
    (setq *gen-funs* nil)
    (setq *fun-name-map* (make-hash-table))
    (dolist (f (domain-functions domain))
        (make-planning-function (pddl-function-name f) (length (pddl-function-parameters f)) domain)
    )
    (with-slots (function-names) domain
        (setf function-names (mapcar #'pddl-function-name (domain-functions domain)))
    )
)

(defun make-planning-function (name arg-count domain)
;    (let*
;        ((funname-symbol (make-lisp-name name domain))
;         (funname-setf-symbol (intern (concatenate 'string "__setf-" (symbol-name funname-symbol)) "SHOP2.FUNCTIONS"))
;        )
;      (push funname-symbol *gen-funs*)
      (push name *gen-funs*)
;      (setf (gethash funname-symbol *fun-name-map*) name)
      ; (eval 
          ; `(defun ,funname-symbol (&rest params)
              ; (get-fun-value ',name params ,arg-count)
           ; )
      ; )
      ; (eval 
          ; `(defsetf ,funname-symbol ,funname-setf-symbol)
      ; )
      ; (eval
          ; `(defun ,funname-setf-symbol (&rest params &aux val)
              ; (setq val (car (last params)))
              ; (put-fun-value ',name 
                  ; (butlast params) 
                  ; (if (or (numberp val) (listp val)) 
                      ; ;then
                      ; val
                      ; ;else
                      ; (list 'quote val)
                  ; )
                  ; ,arg-count
              ; )
           ; )
      ; )
      nil
;    )
)

(defun list-planning-functions ()
    (let ((retlist nil))
        (do-symbols (sym (find-package "SHOP2.FUNCTIONS"))
            (push sym retlist)
        )
        retlist
    )
)
    

; (defun make-planning-function (name arg-count)
    ; (let
        ; ((funname-symbol (intern (symbol-name name) "SHOP2.FUNCTIONS"))
         ; (funhash-symbol (intern (concatenate 'string (symbol-name name) "-hash") "SHOP2.FUNCTIONS"))
         ; (old-package *package*)
        ; )
      ; (eval `(defvar ,funhash-symbol (make-hash-table :test #'equal)))
      ; (eval 
          ; `(defun ,funname-symbol (&rest vals)
               ; (if 
                   ; (not (eq (length vals) ,arg-count))
                   ; ;then
                   ; (error "~%Expected ~A args to ~A, found ~A: ~A." ,arg-count ',name (length vals) (cons name vals))
               ; )
               ; (if 
                   ; (not (null (var-names vals)))
                   ; ;then
                   ; (error "~%Function call not fully bound: ~A." (cons ',name vals))
               ; )
               ; (let ((ret (gethash vals ,funhash-symbol 'fail)))
                   ; (if 
                       ; (eq ret 'fail)
                       ; ;then
                       ; (error "~%No value found for ~A." (cons ',name vals))
                   ; )
                   ; ret
               ; )
           ; )
      ; )
      ; (eval 
          ; `(defun (setf ,funname-symbol) (new-val &rest vals)
               ; (if 
                   ; (not (null (var-names vals)))
                   ; ;then
                   ; (error "~%Function call not fully bound: ~A." (cons name vals))
               ; )
               ; (setf (gethash vals ,funhash-symbol) new-val) 
           ; )
      ; )
      ; nil
    ; )
; )

    
(defgeneric establish-cross-refs (domain))
(defmethod establish-cross-refs ((domain pddl-plus-domain))
    (let 
      ((function-names *gen-funs*))
        (with-slots (events processes) domain
            (dolist (ev events)
                (check-event ev domain)
                (setf (pddl-event-functions ev) 
                    (intersection 
                        function-names 
                        (symbols-in (pddl-event-precondition ev))
                    )
                )
                (setf (pddl-event-static-preconditions ev) (make-static-preconditions ev))
            )
            (dolist (p processes)
                (check-process p domain)
                (setf (pddl-process-functions p) 
                    (intersection 
                        function-names 
                        (symbols-in (pddl-process-effect p))
                    )
                )
                (dolist (ev events)
                    (if 
                        (intersection 
                            (pddl-process-functions p)
                            (pddl-event-functions ev)
                        )
                        (push (pddl-event-name ev) (pddl-process-events p))
                    )
                )
            )
            (dolist (a (domain-full-actions domain))
                (check-action a domain)
            )
        )
    )
)

(defgeneric list-func-refs (domain))
(defmethod list-func-refs ((domain pddl-plus-domain))
    (mapcar 
        #'(lambda (ev) 
            (format t "~%Event: ~A Functions: ~A"   
                (pddl-event-name ev)  
                (pddl-event-functions ev)
            )
          )
        (domain-events domain)
    )
    (mapcar 
        #'(lambda (p)  
            (format t "~%Process: ~A Functions: ~A Events: ~A" 
                (pddl-process-name p) 
                (pddl-process-functions p)
                (pddl-process-events p)
            )
          )
        (domain-processes domain)
    )
    nil
)


(defun make-pddl-param-list (vars type-decls &aux (ret-list nil))
    (dolist (var vars)
        (setq ret-list 
            (append ret-list
                (list var '- (second (assoc var type-decls)))
            )
        )
    )
    ret-list
)

(defun make-type-declarations (params)
    (let 
      ((param-alist nil)
       (ptr params)
       (names nil)
      )
       (loop while ptr do
           (cond
               ((eq (car ptr) '-)
                   (setq param-alist
                       (append param-alist
                           (mapcar #'(lambda (x) (list x (second ptr))) (reverse names))
                       )
                   )
                   (setq names nil)
                   (setq ptr (cddr ptr))
               )
               (t
                   (push (car ptr) names)
                   (setq ptr (cdr ptr))
               )
           )
       )
       (when names 
           (setq param-alist
               (append param-alist
                   (mapcar #'(lambda (x) (list x 'obj)) (reverse names))
               )
           )
       )
       param-alist
    )
)

(defun decompose-types (object-sym-list ht)
    (when
        (null object-sym-list)
        (return-from decompose-types ht)
    )
    (do* ((sym-list object-sym-list (cdr sym-list))
          (names nil names)
         )
         (nil)
;        (format t "~%sym-list: ~A names: ~A" sym-list names)
        (cond
            ((null sym-list)
                (mapc 
                    #'(lambda (sym) (setf (gethash sym ht) 'obj)) 
                    names
                )
                (return-from decompose-types ht)
            )
            ((eq (car sym-list) '-) 
                (mapc 
                    #'(lambda (sym) (setf (gethash sym ht) (cadr sym-list))) 
                    names
                )
                (decompose-types (cddr sym-list) ht)
                (return-from decompose-types ht)
            )
            (t
                (push (car sym-list) names)
            )
        )
    )
    ht
)

(defun is-funcall (lst domain)
    (and 
        (listp lst) 
        (symbolp (car lst)) 
        (member (car lst) (domain-functions domain) :key #'pddl-function-name)
    )
)

(defun print-candidate-events (event-functions state)
    (when (not (find :events (shop-trace)))
        (return-from print-candidate-events nil)
    )
    (trace-print :events nil state "~%Events being considered now:~%")
    (dolist (event-info event-functions)
        (let
          ((fx (car event-info))
           (event (caadr event-info))
           (params (cadadr event-info))
          )
          
            (trace-print :events nil state "~%~A" 
                (cons (pddl-event-name event) params)
            )
            (trace-print :events nil state "~%~A" (function-lambda-expression fx))
            (trace-print :events nil state 
                "~%Current value: ~A~%" 
                (funcall fx 0d0)
            )
        )
    )
    (finish-output)
)
    


; (defun find-zero (fn curt maxt)
    ; (let
      ; ((f0 (funcall fn curt))
       ; (fmax (funcall fn maxt))
       ; (range nil)
       ; (ret -1)
      ; )
        ; (when (> (* f0 fmax) 0)
            ; (setq range (bracket-first-root fn curt maxt 10))
            ; (when (null range)
                ; (return-from find-zero -1)
            ; )
            ; (when (eq (length range) 1)
                ; (return-from find-zero (car range))
            ; )
            ; (setq f0 (car range))
            ; (setq fmax (cadr range))
        ; )
        ; (setq ret (find-root-brent fn curt maxt 1e-6))
        ; (if (< (funcall fn ret) 0)
            ; ;then
            ; (+ ret 1e-6) ;Must be great enough to satisfy the trigger.
            ; ;else
            ; ret
        ; )
    ; )
; )


(defun is-forall (x)
    (and
        (consp x)
        (eq (car x) 'forall)
    )
)

(defun make-dynamic-preconditions ()
)

(defgeneric make-static-preconditions (event))
(defmethod make-static-preconditions ((event pddl-event))
    (let 
      ((precs (pddl-event-precondition event))
      )
        (subst-if '(eval t) #'(lambda (prec) (is-threshold prec event)) precs)
    )
)

(defun is-function-use (lst domain)
    (declare (ignore domain))
    (and (listp lst) (eq (car lst) 'assign) 
        (intersection 
            (symbols-in lst) 
            *gen-funs*
        )
    )
)

(defun is-function-call (lst)
    (and (listp lst)  
        (member (car lst) *gen-funs*)
    )
)

;(defun is-assign (lst)
;    (and  (listp lst) (eq (car lst) 'assign) )
;)

(defun is-threshold (lst event)
    (declare (ignore event))
    (and (listp lst) (member (car lst) '(<= = >= < >)) 
        (intersection 
            (symbols-in lst) 
            *gen-funs*
        )
    )
)

 
(defgeneric get-direct-supertypes (domain subtype))
(defmethod get-direct-supertypes ((domain pddl-plus-domain) subtype)
    (mapcar #'second (remove subtype (domain-supertypes domain) :key #'car :test-not #'eq))
)

#|
(defmethod is-constant-type ((model envmodel) type)
    (if (member type (envmodel-constant-types model))
        t
        nil
    )
)

(defmethod is-numeric-type ((model envmodel) type)
    (if (member type (envmodel-numeric-types model))
        t
        nil
    )
)
|#

(defgeneric get-supertypes (domain subtype))
(defmethod get-supertypes ((domain pddl-plus-domain) subtype)
    (let
      ((new-supertypes (get-direct-supertypes domain subtype))
       (supertype-list nil)
      )
        (loop while (not (null (set-difference new-supertypes supertype-list))) do
            (setq supertype-list (union supertype-list new-supertypes))
            (setq new-supertypes 
                (apply #'append
                    (mapcar #'(lambda (x) (get-direct-supertypes domain x)) new-supertypes)
                )
            )
        )
        supertype-list
    )
)

(defgeneric get-subtypes (domain supertype))
(defmethod get-subtypes ((domain pddl-plus-domain) supertype)
    (map-if #'(lambda (x) (when (member supertype (get-supertypes domain x)) t)) (domain-types domain))
)

