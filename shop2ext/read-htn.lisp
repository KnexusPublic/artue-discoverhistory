 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: Jun 3, 2009
 ;;;; Project Name: ARTUE
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file contains functions that load in an HTN file separate from a 
 ;;;; domain description and combines them appropriately for use by SHOP.

(in-package :shop2)
(use-package :utils)

(export '(read-htn htn) :shop2)

(defun read-htn (htn)
    (let*
      ((htn-name (second (assoc 'htn htn)))
       (domain (second (assoc :domain htn)))
       (domain-file (cdr (assoc :domain-file htn)))
       (known-dom (find-domain domain :absent))
       (new-dom nil)
      )
        (when domain-file
            (load (apply #'make-pathname domain-file))
        )
        (setq known-dom (find-domain domain :absent))
        (when (eq known-dom :absent)
            (error "Domain ~A not found in file ~A." domain domain-file)
        )
        (setq new-dom (copy-domain known-dom))
        (set-variable-property new-dom htn)	; set primitive and variable properties
;        (setq htn (sanitize-items new-dom htn))
        
        ;; For some reason, shop2 assumes the items come in in reverse order.
        
        (parse-domain-items new-dom 
            (remove '(:domain :domain-file htn) htn :key #'car 
                :test #'(lambda (lst item) (member item lst))
            )
        )
        (with-slots (domain-name) new-dom
            (setf domain-name htn-name)
        )
        ; (loop for item in (reverse htn) do
            ; (case (car item) 
                ; (otherwise (parse-domain-item new-dom (car item) item))
            ; )
        ; )
        (install-domain new-dom)
        (setf *domain* new-dom)
        new-dom
    )
)

(defgeneric copy-domain (old-dom))
(defmethod copy-domain ((old-dom domain))
  (let ((new-dom (make-instance (class-name (class-of old-dom)) :name (domain-name old-dom))))
    (with-slots (axioms operators methods) new-dom
        (setf operators (shop2.common::copy-hash-table (domain-operators old-dom)))
        (setf methods (shop2.common::copy-hash-table (domain-methods old-dom)))
        (setf axioms (shop2.common::copy-hash-table (domain-axioms old-dom)))
    )
    new-dom
  )
)

(defmethod copy-domain :around ((old-dom simple-pddl-domain))
  (let ((new-dom (call-next-method old-dom)))
    (with-slots (types predicates) new-dom
        (setf types (copy-list (domain-types old-dom)))
        (setf predicates (copy-list (domain-predicates old-dom)))
    )
    new-dom
  )
)

(defmethod copy-domain :around ((old-dom pddl-plus-domain))
  (let ((new-dom (call-next-method old-dom)))
      (with-slots 
          (events processes functions supertypes function-names
           hidden defaults func-defs constants full-actions id
           remainder file continuous-function-names hidden-types
          ) 
          new-dom
        (setf hidden (copy-list (domain-hidden old-dom)))
        (setf hidden-types (copy-list (domain-hidden-types old-dom)))
        (setf defaults (copy-list (domain-defaults old-dom)))
        (setf events (copy-list (domain-events old-dom)))
        (setf processes (copy-list (domain-processes old-dom)))
        (setf functions (copy-list (domain-functions old-dom)))
        (setf supertypes (copy-list (domain-supertypes old-dom)))
        (setf function-names (copy-list (domain-function-names old-dom)))
        (setf func-defs (copy-list (domain-func-defs old-dom)))
        (setf constants (copy-list (domain-constants old-dom)))
        (setf full-actions (copy-list (domain-full-actions old-dom)))
        (setf continuous-function-names (copy-list (domain-continuous-function-names old-dom)))
        (setf remainder (copy-list (domain-remainder old-dom)))
        (setf file (domain-file old-dom))
        (setf id (domain-id old-dom))
      )
    new-dom
  )
)

(defmethod parse-domain-items ((domain domain) items)
    (when (null (domain-operators domain))
        (with-slots (axioms operators methods) domain
            (setf axioms (make-hash-table :test 'eq))
            (setf operators (make-hash-table :test 'eq))
            (setf methods (make-hash-table :test 'eq))
        )
    )
    (set-variable-property domain items)	; set primitive and variable properties
    (dolist (x (reverse items))
        (parse-domain-item domain (car x) x)
    )
    (values)
)

