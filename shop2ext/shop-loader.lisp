 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: 2009
 ;;;; Project Name: ARTUE
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file loads the extended SHOP2 for PDDL+, including the original SHOP2
 ;;;; and extensions in the shop2ext directory.
 
(load (make-pathname :directory '(:relative "utils") :name "loader"))
(when (not (find-package "ASDF"))
    (utils:load-file (make-pathname :directory '(:relative "asdf")) "asdf" :action :compile-if-newer)
)
(push (make-pathname :directory '(:relative "shop2")) asdf:*central-registry*)
;(asdf:oos 'asdf:load-source-op :shop2 :force t)
(asdf:oos 'asdf:load-op :shop2)
(when (not (find-package :shop2.common))
    (asdf:oos 'asdf:load-op :shop2 :force t)
)

; (defpackage :shop2ext
    ; (:shadow name domain unify define) 
    ; (:use :common-lisp :shop2 :shop2.common :utils)
    ; (:export 
        ; get-named-function-hash set-state-value make-fun-state
        ; get-objects-of-type def-pddlplus-domain make-type-declarations 
        ; decompose-types get-pddlplus-domain-from-file *use-continuous-time*
        ; pddl-problem-name pddl-problem-domain pddl-problem-objects
        ; pddl-problem-init pddl-problem-values pddl-problem-goal
        ; pddl-problem-types export-problem-to-shop
    ; )
; )

(defpackage :shop2
    (:use :common-lisp :shop2.unifier :shop2.common :shop2.theorem-prover :utils)
)
#+:ABCL(use-package :utils :shop2)


(defpackage :shop2.common
    (:use :common-lisp :shop2.unifier :utils)
)
#+:ABCL(use-package :utils :shop2.common)

(defvar shop2::*fun-name-map* nil)

(utils:load-files (make-pathname :directory '(:relative "shop2ext")) '( "shop-utils" "fun-state" "pddlplus" "pddl-sensing" "root-search" "time-projection" "read-htn") :action :compile-if-newer)

