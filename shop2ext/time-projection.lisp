 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: July 1, 2013
 ;;;; Project Name: ARTUE
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file contains functions for projecting the effects of events and 
 ;;;; processes.

(in-package :shop2)

(defvar *event-has-occurred* nil)
(defvar *large-time-threshold* 10000)

(defun pass-time-until (state domain event-name &key (params nil) (max -1))
    (let ((*event-has-occurred* nil)
          (f-event-check 
              (get-named-event-trigger-fn 
                  `((wait-for ,event-name ,@params))
              )
          )
          (time-passed 0)
         )
        (check-for-event-trigger state f-event-check)
        (loop until *event-has-occurred* do
            (multiple-value-bind (events time-taken)
                (get-next-event state (if (< 0 max 100) max 100) domain :waiting-for-event t)
                
                (decf max time-taken)
                (when (< (abs max) .0001)
                    (setq *event-has-occurred* t)
                )
                (check-for-event-trigger state f-event-check)
                
                (when (< time-taken 0)
                    (if *return-wait-times*
                        ;then
                        (error "Waiting for an event, ~A, that can never occur."
                            (cons event-name params)
                        )
                        ;else
                        (setq *event-has-occurred* t
                              time-passed 0
                        )
                    )
                )
                (incf time-passed time-taken)
            )
            (when (and (< max 0) (>= time-passed *large-time-threshold*))
                (cerror "Continue"
                    "A large amount of time (~A units) has passed while waiting for the event ~A to occur. ARTUE is now suspicious that it is in an infinite loop waiting for this event. Change the value of shop2::*large-time-threshold* to allow larger amounts of time to pass before triggering this error."
                    time-passed
                    (cons event-name params)
                )
            )
        )
        time-passed
    )
)

(defun pass-simultaneous-waits (wait-actions max-duration state 
                                &aux f-event-check remaining-duration)
    (setq f-event-check (get-named-event-trigger-fn wait-actions))
    (if (null max-duration)
        ;then
        (setq remaining-duration *large-time-threshold*)
        ;else
        (setq remaining-duration max-duration)
    )
    (loop until (or *event-has-occurred* (<= remaining-duration 0)) do
        (multiple-value-bind (recent-events time-taken)
            (get-next-event state remaining-duration *domain*)
            (decf remaining-duration time-taken)
            (when recent-events
                (setq recent-events 
                    (check-for-event-trigger f-event-check state)
                )
            )
        )
    )
    (when (null max-duration)
        (when (<= remaining-duration 0)
            (error "None of these awaited events have occurred after waiting for ~A time units, probably indicating a problem: ~A" *large-time-threshold* wait-actions)
        )
        (return-from pass-simultaneous-waits (values recent-events (- *large-time-threshold* remaining-duration)))
    )
    (values recent-events (- max-duration remaining-duration))
)
    

(defun get-named-event-trigger-fn (wait-actions)
    #'(lambda (name params)
        (dolist (wait-action wait-actions)
            (let ((event-name (second wait-action))
                  (event-params (cddr wait-action)))
                (when (null event-name)
                    (setq *event-has-occurred* t)
                )
                (when (and (eq event-name name) (null event-params))
                    (setq *event-has-occurred* t)
                )
                (when (and (eq event-name name) (equal event-params params))
                    (setq *event-has-occurred* t)
                )
            )
        )
      )
)
 
(defun pass-time (duration state)
    (when (eq (check-for-event-trigger state #'list) 'fail)
        (return-from pass-time 'fail)
    )
    (if *use-continuous-time*
        ;then
        (pass-time-continuous duration state)
        ;else
        (pass-time-discrete duration state)
    )
)

(defun pass-time-continuous (duration state)
;    (break)
    (loop while (> duration 0) do
        (multiple-value-bind (events time) (get-next-event state duration *domain*)
            (when events
                (check-for-event-trigger state #'list)
            )
            (if (>= time 0)
                ;then
                (setq duration (- duration time))
                ;else
                (setq duration 0)
            )
        )
    )
    (check-for-event-trigger state #'list)
    nil
)

(defun pass-time-discrete (duration state)
    (loop repeat duration do
        (when 
            (any
                (mapcar+ #'execute-process (domain-processes *domain*) state)
            )
            (check-for-event-trigger state #'list)
        )   
    )
)


 
(defun advance-time (state new-time functions domain)
    (tag-state state)
    (setq functions (ensure-ordering functions domain))
    (trace-print :functions nil state
        "~%Function values updated, clock delta is ~A~%~20A ~35A ~9A ~9A"
        new-time
        "Process"
        "Function"
        "Old value"
        "New value"
    )
    (dolist (func functions)
        (let*
          ((function-call (car (car func)))
           (function-def (cdr (car func)))
           (function-name (car function-call))
           (function-params (cdr function-call))
           (process-name (second func))
           (new-value (state-eval state (subst new-time '?__dur function-def)))
           (old-value (get-state-value function-call state))
          )
            (trace-print :functions nil state
                "~%~20,1,0A ~35,1,0,'=A ~9,4F ~9,4F"
                process-name function-call old-value new-value
            )
            (set-state-value state function-name function-params new-value)
        )
    )
)

(defun combine-functions (process-functions)
    (let ((new-funs nil) cur-funs first-fun new-fun)
        (loop while process-functions do
            (setq first-fun (car process-functions))
            (setq process-functions (cdr process-functions))
            (setq cur-funs
                (mapcar #'cdar
                    (remove (caar first-fun) process-functions :key #'caar :test-not #'equal)
                )
            )
            (when (null cur-funs)
                (push first-fun new-funs)
            )
            (unless (null cur-funs)
                (setq process-functions
                    (remove (caar first-fun) process-functions :key #'caar :test #'equal)
                )
                (push (cdar first-fun) cur-funs)
                (setq new-fun
                    (mapcar 
                        #'(lambda (fun)
                            (case (car fun)
                                (+ (third fun))
                                (- (list '- (third fun)))
                                (otherwise (error "Attempting to combine non-combinable function."))
                            )
                          )
                        cur-funs
                    )
                )
                (push 
                    (list
                        (append 
                            (list (first (car first-fun)) '+ (third (car first-fun)))
                            new-fun
                        )
                        (second first-fun)
                    )
                    new-funs
                )
            )
        )
        new-funs
    )
)

(defun ground-out-process-functions (process-functions state domain)
  (declare (ignore state))
    (let
      ((new-funs nil)
       (sub-list nil)
       (func-call nil)
       (func-def nil)
       (process-name nil)
       (call-map nil)
       (do-integration nil)
      )
        (setq process-functions (ensure-ordering (combine-functions process-functions) domain))
        (dolist (procfun process-functions)
            (setq func-call (caar procfun))
            (setq func-def (cdar procfun))
            (setq process-name (cadr procfun))
            (setq sub-list (mapcar #'first new-funs))
            (setq do-integration nil)
;            (format t "~%~%Call: ~A~%Def: ~A~%Process: ~A" func-call func-def process-name)
            (setq func-def
                (sublis 
                    (remove func-call sub-list :key #'car :test #'equal)
                    func-def
                    :test #'equal
                )
            )
            (when (not (equal func-def (cdar procfun)))
;                (break "Integration-necessary.")
                (setq do-integration t)
            )
;            (format t "~%Def after subbing other process functions:~%~A" func-def)
            (setq call-map
                (mapcar #'(lambda (call) (cons call (state-eval state call)))
                    (find-in-tree
                        func-def
                        #'(lambda (item) (is-funcall item domain))
                    )
                )
            )
            (setq call-map (cons (cons func-call '?y) call-map))
            (setq func-def (sublis call-map func-def :test #'equal))
;            (format t "~%Def after subbing other calls:~%~A" func-def)

            
            ;; Now take the derivative for the (* #t x) types.
            (when (and do-integration
                       (eq (first (third func-def)) '*)
                       (eq (second (third func-def)) '?__dur))
                (ensure-maxima-loaded)
                (setq func-def 
                    ;(list (first func-def) (second func-def)
                        (integrate-lisp-expr (third (third func-def)) '?y '?__dur
                            (* (second func-def) (if (eq (first func-def) '-) -1 1)) 0
                        )
                    ;)
                )
;                (break "~%Def after integration:~%~A" func-def)
            )
            
            (push (list (cons func-call func-def) process-name) new-funs)
;            (format t "~%Resulting functions:~%~A" new-funs)
        )
        new-funs
    )
)


(defun ensure-ordering (functions domain)
    (when (null functions) (return-from ensure-ordering nil))
    (let*
      ((func (car functions))
       (function-def (cdr (car func)))
       (nested-calls 
           (find-in-tree
               function-def
               #'(lambda (item) (is-funcall item domain))
           )
       )
       (updating-function-calls (mapcar #'caar (cdr functions)))
      )
        (if (intersection nested-calls updating-function-calls :test #'equal)
          ;then
          (ensure-ordering (append (cdr functions) (list func)) domain)
          ;else
          (cons func (ensure-ordering (cdr functions) domain))
        )
    )
)


(defun check-for-event-trigger (state f-event-check &aux (occ-tuples t) (prior-occs nil) (new-occs t) (occs-count 0))
    (loop while new-occs do
        (when (and (zerop (nth-value 1 (floor occs-count 1000))) (> occs-count 1000))
            (cerror "Continue"
                "Many events have occurred without any time passing. ARTUE is now suspicious that it is in an infinite loop of instantaneous events."
                time-passed
                (cons event-name params)
            )
        )
            
        (setq occ-tuples
            (apply #'append
                (mapcar
                    #'(lambda (evt-def)
                        (check-an-event evt-def state)
                      )
                    (domain-events *domain*)
                )
            )
        )
        (setq new-occs nil)
        (dolist (occ-tuple occ-tuples)
            ; (when (pddl-event-likelihood (second occ-tuple))
                ; (break "Likelihood of event ~A: ~A" 
                    ; (cons (pddl-event-name (second occ-tuple)) (first occ-tuple))
                    ; (state-eval (third occ-tuple) (sublis (first occ-tuple) (pddl-event-likelihood (second occ-tuple))))
                ; )
            ; )
            (when (or (null (pddl-event-likelihood (second occ-tuple)))
                      (< (random 1.0) 
                         (state-eval (third occ-tuple) (sublis (first occ-tuple) (pddl-event-likelihood (second occ-tuple))))
                      )
                  )
                (execute-event 
                    (first occ-tuple)  ;blist
                    (second occ-tuple) ;evt-def
                    (third occ-tuple)  ;state
                    f-event-check
                )
                (push (apply #'get-event-sig occ-tuple) new-occs)
            )
        )
        (setq prior-occs (append new-occs prior-occs))
        (when (and new-occs (cycle-has-formed new-occs prior-occs))
            (format t "~%Event cycle detected in check-for-event-trigger!")
            (format t "~%This could indicate a poorly defined domain or inconsistent beliefs.")
            (format t "~%Cycle:")
            (print-one-per-line prior-occs)
            (throw :projection-fail :event-cycle)
        )
        (incf occs-count)
    )
    prior-occs
)

(defun get-event-sig (blist evt-def state)
    (declare (ignore state))
    (cons (pddl-event-name evt-def) 
        (loop for binding in blist
              collect (shopunif::binding-val binding)
        )
    )
)



;; This function returns a list of evaluatable functions that will reach 0 when
;; the event may execute, based on the non-static preconditions of the event. If
;; such a precondition is already satisfied, it is not returned. 
(defun get-event-zero-functions (event params state time-funs)
    (let*
      ((unifiers
          (mapcar+ #'compose-substitutions
              (is-possible event params state :just1 nil)
              (unify 
                  (mapcar #'car (pddl-event-parameters event)) 
                  params
              )
          )
       )
       (thresholds 
          (apply #'append
              (mapcar+ #'(lambda (u constraint) (apply-substitution constraint u))
                  unifiers
                  (find-subtrees 
                      (subst-if t #'is-forall (pddl-event-precondition event)) 
                      #'(lambda (x) (is-threshold x event))
                  )
              )
          )
       )
       (functions nil)
      )
        (dolist (threshold thresholds)
            (when (not (eq (length threshold) 3))
                (error "Expected two arguments to ~A in event ~A, found ~A."
                    (car threshold) 
                    (pddl-event-name event) 
                    (1- (length threshold))
                )
            )
            (when (vars-in threshold)
                (error "Unbound variable in threshold ~A in event ~A"
                    threshold 
                    (pddl-event-name event) 
                )
            )
            (when (not (shopthpr:find-satisfiers threshold state t))
                (let (subbed-threshold)
                    (case (car threshold)
                        (< (setq subbed-threshold (list '- (second threshold) (list '* (third threshold) (- 1 *rounding-precision-constant*)))))
                        (> (setq subbed-threshold (list '- (second threshold) (list '* (third threshold) (+ 1 *rounding-precision-constant*)))))
                        (t (setq subbed-threshold (cons '- (cdr threshold))))
                    )
                    (setq subbed-threshold (sublis time-funs subbed-threshold :test #'equal))
                    (when (grep-trees subbed-threshold '?__dur)
                        (push 
                            (list
                                (make-function state subbed-threshold)
                                (list event params)
                            )
                            functions
                        )
                    )
                )
            )
        )
        functions
    )
)

#-:ABCL
(defun make-function (state calc)
    (let
      ((fun-name 'CUR-FUN) 
       (eqn
           (coerce-equation
               (reduce-constants calc state)
           )
       )
      )
        (eval
            `(defun ,fun-name (?__dur)
                ,eqn
             )
        )
        (compile fun-name)
        (setf (get 'fun-def fun-name) eqn)
        (symbol-function fun-name)
    )
)

#+:ABCL
(defun make-function (state calc)
    `(lambda (?__dur)
        ,(maptree #'coerce-numbers 
            (reduce-constants calc state)
         )
     )
)

(defun coerce-equation (elem)
    (cond
        ((numberp elem)
            (coerce elem 'double-float))
        ((and (listp elem) (eq (first elem) 'expt))
            (list 'expt (coerce-equation (second elem)) (third elem)))
        ((listp elem)
            (mapcar #'coerce-equation elem))
        (t elem)
    )
)

(defun reduce-constants (fun-desc state)
    (cond
        ((null fun-desc) nil)
        ((atom fun-desc)
            fun-desc
        )
        ((member '?__dur (vars-in fun-desc))
            (mapcar+ #'reduce-constants fun-desc state)
        )
        ((is-function-call fun-desc)
            (state-eval state fun-desc)
        )
        (t
            (state-eval state fun-desc)
        )
    )
)


;;;needs to return t if an event firing
;;;event firing side effects by changing the state object
;;;MEK: Assumes that all participants in continuous conditions
;;;also participate in the static conditions
(defun check-an-event (evt-def state)
   (let* ((conditions (sorted-conditions evt-def)))
    (when (null conditions)
        (error "event ~A with no conditions" evt-def)
    )
    (mapcar 
        #'(lambda (blist) 
            (list blist evt-def state)
          )
        (shopthpr:find-satisfiers conditions state nil)
    )
  )
)

(defun make-param-unifier (blist event &aux vars)
    (setq vars
        (mapcar #'first 
            (remove-if #'(lambda (x) (is-numeric-type (second x) *domain*))
                (pddl-event-parameters event)
            )
        )
    )
    (when (> (length vars) (length blist))
        (return-from make-param-unifier nil)
    )
    (when (< (length vars) (length blist))
        (error "How can there be more values than variables?")
    )
    (mapcar
        #'(lambda (pair) (make-binding (car pair) (cdr pair)))
        (pairlis vars blist)
    )
)




;;;assumes all blists have the same variables
(defun consider-object-types (blists evt-def state)
  (let ((p-types (pddl-event-parameters evt-def)))
    (setq blists (apply #'append (mapcar+ #'fill-blist blists p-types state))) 
    (cond ((every #'(lambda (p-type) 
		      (shop2.unifier::find-binding (car p-type)(car blists)))
		  p-types)
	   ;;;simple case, every participant is mentioned in the first blist
	   (remove-if-not 
	    #'(lambda (blist)
		(blist-satisfies-p-types p-types blist state))
	    blists))
	  (t (error "every participant is not mentioned in each blist")))))

(defun fill-blist (blist param-types state &aux param-type)
    (loop while param-types do
        (setq param-type (car param-types))
        (setq param-types (cdr param-types))
        (when (null (shop2.unifier::find-binding (car param-type) blist))
            (return-from fill-blist
                (apply #'append
                    (mapcar 
                        #'(lambda (obj) 
                            (fill-blist 
                                (cons (shop2.unifier::make-binding (first param-type) obj) blist) 
                                param-types state
                            )
                          )
                        (get-objects-of-type state (second param-type))
                    )
                )
            )
        )
    )
    (list blist)
)

;;could probably condense with above function
(defun blist-satisfies-p-types (p-types blist state)
  (every
   #'(lambda (p-type)
       (member 
	(shop2.unifier::binding-list-value (car p-type)blist)
	(shop2.common::get-objects-of-type state (second p-type))))
   p-types))

(defun execute-event (blist evt-def state f-event-check)
  (let ((head-subbed (cons (pddl-event-name evt-def)
			   (params-from-blist blist evt-def))))
    (trace-print :events nil state "~%Event ~A: fired with bindings ~A"
		 head-subbed blist)
    (apply-pddl-effects 
     state head-subbed 
     (apply-substitution (pddl-event-effect evt-def) blist)
     nil 0)
;;; MEK: Hold over from old code
    (funcall f-event-check
        (pddl-event-name evt-def) 
        (cdr head-subbed)
    )
  )
)

(defun params-from-blist (blist evt-def)
  (mapcar #'(lambda (p-type)
	      (shop2.unifier::binding-list-value (car p-type)blist))
	  (pddl-event-parameters evt-def)))
  
;;MEK: the copy-list here is because I was breaking things later
;(defun sorted-conditions (evt-def) 
;      (let ((conditions (copy-list (pddl-event-precondition evt-def))))
;	(cond ((eql (car conditions) 'cl-user::and)
;	       (sort conditions #'>static))
;	      (t conditions))))

(defun sorted-conditions (evt-def)
    (pddl-event-precondition evt-def)
)

;;;true only if q1 is static and q2 is continuous
; (defun >static (q1 q2)
  ; (cond ((and (continuous-stmt? q1)
	      ; (continuous-stmt? q2))
	 ; nil)
	; ((continuous-stmt? q1) nil)
	; ((continuous-stmt? q2) t)
	; (t nil)))
	      ; 
; (defun continuous-stmt? (stmt)
  ; (and (listp stmt) (member (car stmt) '(<= = >= < >))))

;;;;End edits


#|
;; MEK [1/19/2010] to increase effeciency, I'm going to rewrite this to 
;; match the conditions first, then check the types.
(defun check-an-event (event state)
    (any
        (mapcar+ #'execute-event-for-params
            (permutations
                (mapcar 
                    #'(lambda (x) (shop2.common:get-objects-of-type state (cadr x)))
                    (pddl-event-parameters event)
                )
            )
            event
            state
        )
    )
)

(defun execute-event-for-params (params event state)
    (let
        ((unifier (shop2.unifier::unify (mapcar #'car (pddl-event-parameters event)) params))
         (preconditions (pddl-event-precondition event))
         (head (cons (pddl-event-name event) params))
        )
      ; (when (eq (car head) 'hit-iceberg) 
          ; (break "~%Considering event ~A." head)
      ; )
      ;; first check the preconditions, if any
      (when preconditions
          (let* ((pre (apply-substitution preconditions unifier))
    	         (pu (shopthpr:find-satisfiers pre state t))
                )
              (setq unifier (compose-substitutions unifier (first pu)))
              (unless pu
                  (return-from execute-event-for-params nil)
              )
          )
      )
      (trace-print :events nil state "~%Event ~A: fired " head)
      (apply-pddl-effects 
          state head 
          (apply-substitution (pddl-event-effect event) unifier)
          nil 0
      )
;      (break)
       ;;Replaced check-for-awaited-event with lambda.
;      (funcall f-event-check (pddl-event-name event) params) 
      t
    )
)
|#


(defgeneric is-active (process params state))
(defmethod is-active ((process pddl-process) params state)
    (let*
      ((preconditions (pddl-process-precondition process))
       (unifier (shop2.unifier::unify (mapcar #'car (pddl-process-parameters process)) params))
       (pre (apply-substitution preconditions unifier))
       (pu (shopthpr:find-satisfiers pre state t))
      )
	  pu
    )
)

(defgeneric get-process-unifier (process params state))
(defmethod get-process-unifier ((process pddl-process) params state)
    (let*
      ((preconditions (pddl-process-precondition process))
       (unifier (shop2.unifier::unify (mapcar #'car (pddl-process-parameters process)) params))
       (pre (apply-substitution preconditions unifier))
       (pu (shopthpr:find-satisfiers pre state t))
      )
      (compose-substitutions (first pu) unifier)
    )
)

(defgeneric is-possible (event params state &key just1))
(defmethod is-possible ((event pddl-event) params state &key (just1 t))
    (let*
      ((preconditions (pddl-event-static-preconditions event))
       (unifier (shop2.unifier::unify (mapcar #'car (pddl-event-parameters event)) params))
       (pre (apply-substitution preconditions unifier))
       (pu (shopthpr:find-satisfiers pre state just1))
      )
      pu
    )
)

(defgeneric is-firing (event params state))
(defmethod is-firing ((event pddl-event) params state)
    (let*
      ((preconditions (pddl-event-precondition event))
       (unifier (shop2.unifier::unify (mapcar #'car (pddl-event-parameters event)) params))
       (pre (apply-substitution preconditions unifier))
       (pu (shopthpr:find-satisfiers pre state t))
      )
      pu
    )
)

(defun get-next-event (state maxtime domain &key (waiting-for-event nil))
    (let
      ((processes (apply #'append (mapcar+ #'enumerate-actives (domain-processes domain) state)))
       (events    (apply #'append (mapcar+ #'enumerate-possibles (domain-events domain) state)))
       (curfuncs nil)
       (process-functions nil)
       (event-functions nil)
       (statetag (tag-state state))
      )
        (when (and (null events) waiting-for-event)
            (return-from get-next-event (values nil -1))
        )
        (dolist (process processes)
            (setq curfuncs 
                (get-process-time-functions 
                    (car process) (cadr process) state
                )
            )
;            (format t "Process: ~A" (pddl-process-name (car process)))
            
            (when (and (null curfuncs) (not *ignore-functionless-processes*))
                (warn "~%Found no functions for active process ~A with params ~A!" 
                    (pddl-process-name (car process))
                    (cadr process)
                )
                (trace-print :functions nil state
                    "~%Found no functions for active process ~A with params ~A!" 
                    (pddl-process-name (car process))
                    (cadr process)
                )
                (break)
            )
	     
            (setq process-functions (append process-functions curfuncs))
        )
        (setq process-functions (ground-out-process-functions process-functions state domain))
        (dolist (event events)
            ;; finds functions that determine when event will fire.
            (setq curfuncs 
                (get-event-zero-functions 
                    (car event) (cadr event) 
                    state (mapcar #'first process-functions)
                )
            )
            
            (if (null curfuncs)
                ;then 
                ;;if an event could fire now, and is not process dependent, it's 
                ;; an error. Should have already fired.
                (when (is-firing (car event) (cadr event) state)
                    (error "Event ~A with params ~A is firing continuously; this is illegal."
                        (pddl-event-name (car event)) 
                        (cadr event)
                    )
                )
                ;else
                (setq event-functions (append event-functions curfuncs))
            )
        )
        (print-candidate-events event-functions state)
        (do
          ;vars
          ((earliest-time (round-to-time-step maxtime) (round-to-time-step maxtime))
           (earliest-events nil nil)
           (curtime 0d0 (+ earliest-time 1d-6))
           (newtime -1 -1)
          )
          ;endtest results
          (nil)
            (when (< curtime earliest-time)
                (dolist (event-function event-functions)
                    (setq newtime (find-zero (car event-function) curtime earliest-time))
;                    (setq newtime (time (find-zero (car event-function) curtime earliest-time)))
;                    (break)
                    (when (not (null newtime))
                        ;(setq newtime (round-to-time-step newtime))
                        (when (and (> newtime curtime) (<= newtime earliest-time))
                            (when (< newtime earliest-time)
                                (setq earliest-events nil)
                            )
                            (push (cadr event-function) earliest-events)
                            (setq earliest-time newtime)
                            ;;MCM 06/21/13: Removed rounding from prior line. 
                            ;; This appears to cause a problem later when 
                            ;; rounding drops below time threshold, event is not 
                            ;; technically firing yet.
                        )
                    )
                )
            )

            (when (null earliest-events)
                (trace-print :events nil state  "~%No events found to fire.")
                (advance-time state earliest-time process-functions domain)
                (return-from get-next-event (values nil maxtime))
            )
            
;            (break "After event function loop.")
            (setq earliest-events (remove-duplicates earliest-events :test #'equal))
            (trace-print :events nil state 
                "~%Advanced by ~A. Earliest events are ~A." 
                earliest-time 
                (if earliest-events 
                    (mapcar 
                        #'(lambda (event)
                            (cons (pddl-event-name (car event)) (cadr event))
                          )
                        earliest-events
                    )
                    nil
                )
            )
            

            ;; MCM: Special case occurs in the final extremity, when we want the
            ;;      real amount of remaining time, not the rounded-off portion.
            (unless (null earliest-events)
                (let 
                  ((rounded-time (round-to-time-step earliest-time))
                   (rounded-events nil)
                  )
                    (advance-time state rounded-time process-functions domain)
                    (setq rounded-events
                        (remove-if-not 
                            #'(lambda (x) (is-firing (car x) (cadr x) state))
                            earliest-events
                        )
                    )
                    (when (and (null rounded-events)
                               (<= rounded-time earliest-time))
                        (setq rounded-time (increment-time rounded-time))
                        (advance-time state rounded-time process-functions domain)
                        (setq rounded-events 
                            (remove-if-not 
                                #'(lambda (x) (is-firing (car x) (cadr x) state))
                                earliest-events
                            )
                        )
;                        (break "rounding check.")
                    )
                    (setq earliest-events rounded-events)
                    (setq earliest-time rounded-time)
                )
            )
            (trace-print :events nil state
                "~%Firing events are ~A."
                (if earliest-events 
                    (mapcar 
                        #'(lambda (event) 
                            (cons (pddl-event-name (car event)) (cadr event))
                          )
                        earliest-events
                    )
                    nil
                )
            )
            (when (or earliest-events (>= earliest-time maxtime)) 
                (when (>= earliest-time (round-to-time-step maxtime))
                    (setq earliest-time maxtime)
                )
                (return-from get-next-event (values earliest-events earliest-time))
            )
            (retract-state-changes state statetag)
        )
    )
)

(defun increment-time (time-val)
    (if (> *time-step-length* 0)
        ;then
        (+ time-val *time-step-length*)
        ;else
        (+ time-val 1d-6)
    )
)

(defun round-to-time-step (time-val)
    (when (> *time-step-length* 0)
        (setq time-val 
            (* *time-step-length* 
                (max (ceiling (- (/ time-val *time-step-length*) 1d-6)) 1)
            )
        )
    )
    time-val
)



(defun get-process-time-functions (process params state)
    (let*
      ((effects (get-effects process))
       (unifier (get-process-unifier process params state))
       (eff (apply-substitution effects unifier))
       (functions nil)
       (call nil)
       (value nil)
      )
        (dolist (e eff)
            (setq call (second e))
            (setq value 
                (get-state-value call state)
            )
            (push 
                (list 
                    (cons 
                        call
                        (case (first e)
                            (increase 
                                (list '+ value (third e))
                            )
                            (decrease 
                                (list '- value (third e))
                            )
                            (set
                                (third e)
                            )
                         )
                    )
                    (pddl-process-name process)
                )
                functions
            )
        )
        functions
    )
)


(defun execute-process (process state)
    (any  
        (mapcar+ #'execute-process-for-params
            (permutations
                (mapcar 
                    #'(lambda (x) (shop2.common:get-objects-of-type state (cadr x)))
                    (pddl-process-parameters process)
                )
            )
            process
            state
        )
    )
)

(defun any (truth-list)
    (some #'identity truth-list)
)

(defun execute-process-for-params (params process state)
    (let
        ((unifier (shop2.unifier::unify (mapcar #'car (pddl-process-parameters process)) params))
         (preconditions (pddl-process-precondition process))
         (head (cons (pddl-process-name process) params))
        )
        
;      (format t "~%Considering process ~A: " head)
      ;; first check the preconditions, if any
      (when preconditions
          (let* ((pre (apply-substitution preconditions unifier))
    	         (pu (shopthpr:find-satisfiers pre state t))
                )
              (setq unifier (compose-substitutions unifier (first pu)))
              (unless pu
                  (return-from execute-process-for-params nil)
              )
          )
      )
      (trace-print :events nil state "~%Process ~A: fired " head)
      (apply-pddl-effects 
          state head 
          (apply-substitution (pddl-process-effect process) unifier)
          nil 0
      )
      t
    )
)



(defun permutations (list-of-lists &optional cur-perm)
    (cond 
        ((null list-of-lists) (list cur-perm))
        (t 
            (apply #'append
                (mapcar 
                    #'(lambda (sym) (permutations (cdr list-of-lists) (append cur-perm (list sym)))) 
                    (car list-of-lists)
                )
            )
        )
    )
)

(defun enumerate-possibles (event state)
;    (break "~%Event: ~A" (pddl-event-name event))
    (let 
      ((perms         
            (permutations
                (mapcar 
                    #'(lambda (x) (shop2.common:get-objects-of-type state (cadr x)))
                    (pddl-event-parameters event)
                )
            )
       )
       (possibles nil)
      )

        (dolist (params perms)
            (when (is-possible event params state)
                (push (list event params) possibles)
            )
        )
        possibles
    )
)

(defun enumerate-actives (process state)
    (let 
      ((perms         
            (permutations
                (mapcar 
                    #'(lambda (x) (shop2.common:get-objects-of-type state (cadr x)))
;;                    This doesn't work because it doesn't recognize supertypes.
                    (pddl-process-parameters process)
                )
            )
       )
       (actives nil)
      )
;        (format t "~%Process: ~A Perms: ~A" (pddl-process-name process) perms)
        (dolist (params perms)
            (when (is-active process params state)
                (push (list process params) actives)
            )
        )
        actives
    )
)
    

(defvar *maxima-loaded* nil)
(defun ensure-maxima-loaded ()
    (when (not *maxima-loaded*)
        (load (make-pathname :directory '(:relative "shop2ext") :name "maxima-loader"))
        (setq *maxima-loaded* t)
    )
)


(defun integrate-lisp-expr (expr dvar ivar dinit iinit)
    (declare (ignore expr dvar ivar dinit iinit))
    (error "Maxima not loaded.")
)

