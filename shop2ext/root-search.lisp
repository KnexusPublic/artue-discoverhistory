 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: March 19, 2013
 ;;;; Project Name: ARTUE
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file provides the root search code used to determine when an event 
 ;;;; will occur as variables are continuously updated.

(in-package :shop2)

(defvar *golden-ratio* (/ (+ (sqrt 5d0) 1d0) 2d0))
(defvar *mr-dist* (/ 1d0 *golden-ratio*))
(defvar *ml-dist* (- 1 *mr-dist*))

(defun find-zero (fx l r &key (max-depth 100) (eps 1d-6) (iter-per 50))
    (let
      ((range 
          (find-zero-anywhere 
              fx (coerce l 'double-float) (coerce r 'double-float) 0 
              :eps (coerce eps 'double-float)
              :iter-per iter-per 
              :max-depth max-depth
          )
       )
       (rounded-x l)
       (multiplier 1))
        (when (= (length range) 1)
            (return-from find-zero (first range))
        )
        (when (= (length range) 0)
            (return-from find-zero nil)
        )
        (setq l (first range))
        (setq r (second range))
        (loop while (and (< eps multiplier) (or (> rounded-x r) (< rounded-x l))) do
            (setq l (* 10 l))
            (setq r (* 10 r))
            (setq rounded-x (coerce (ceiling (* 10 l)) 'double-float))
            (setq multiplier (/ multiplier 10))
        )
        (setq l (* l multiplier))
        (setq r (* r multiplier))
        (setq rounded-x (* rounded-x multiplier))
        (if (and (< l rounded-x r) (< (abs (funcall fx rounded-x)) eps))
            ;then return
            rounded-x
            ;else return
            r
        )
    )
)

(defun find-zero-anywhere (fx l r depth &key (max-depth 100) (eps 1d-6) (iter-per 50))
    (declare (type double-float l r eps))
    (declare (type fixnum iter-per))
    (when (>= depth max-depth)
        (error "Could not find zero within the maximum depth. This indicates
                a very large number of critical points on a process function,
                which makes continuous time planning intractable."
        )
    )
    (when (> l r)
        (break "Assumption broken.")
    )
    (setq depth (1+ depth))
    (let
      ((fl (funcall fx l))
       (fr (funcall fx r))
       zero
       new-zero
      )
        (debug-format "~%find-zero-anywhere l: ~A r: ~A" l r)
        (cond
            ((= fl 0) (list l))
            ((= fr 0) 
                (setq zero (list r))
                (setq new-zero (find-zero-anywhere fx l (- r eps) depth :eps eps :iter-per iter-per))
                (if (null new-zero) zero new-zero)
            )
            ((same-sign fl fr)
                (find-root-matched-sign fx l r fl depth :eps eps :iter-per iter-per)
            )
            ((not (same-sign fl fr))
                (find-root-mismatched-sign fx l r depth :eps eps :iter-per iter-per)
            )
        )
    )
)

(defun find-root-matched-sign (fun l r sign depth &key (max-depth 100) (eps 1d-6) (iter-per 50))
    (debug-format "~%find-root-matched-sign l: ~A r: ~A" l r)
    (let (zero opt
          (fx (if (< 0 sign) fun (negative-fun fun)))
          (neg-fx (if (< 0 sign) (negative-fun fun) fun))
         )
        (setq opt (find-minimum fx l r :eps eps :max-iter iter-per))
        (debug-format "~%found minimum opt: ~A" opt)
        (when (null opt)
            (setq opt (find-minimum neg-fx l r :eps eps :max-iter iter-per))
            (debug-format "~%found maximum opt: ~A" opt)
            (when (null opt)
                (return-from find-root-matched-sign nil)
            )
        )
        (setq zero (find-zero-anywhere fx l (car opt) depth :max-depth max-depth :eps eps :iter-per iter-per))
        (when zero
            (return-from find-root-matched-sign zero)
        )
        (find-zero-anywhere fun (cadr opt) r depth :max-depth max-depth :eps eps :iter-per iter-per)
    )
)

(defun find-root-mismatched-sign (fx l r depth &key (max-depth 100) (eps 1d-6) (iter-per 50))
    (debug-format "~%find-root-mismatched-sign l: ~A r: ~A" l r)
    (let (zero new-zero)
        (setq zero (find-root-with-bisection fx l r :eps eps :max-iter iter-per))
        (setq new-zero (find-zero-anywhere fx l (car zero) depth :max-depth max-depth :eps eps :iter-per iter-per))
        (return-from find-root-mismatched-sign (if (null new-zero) zero new-zero))
    )
)

(defun find-root-with-bisection (fx l r &key (eps 1d-6) (max-iter 50))
    (let 
      ((over l)
       (under r)
       (fover (funcall fx l))
       (funder (funcall fx r))
       (iter 0) 
       m fm
      )
        (when (< 0 funder)
            (rotatef over under)
            (rotatef fover funder)
            (when (< 0 funder)
                (error "Root must be bracketed.")
            )
        )
        (loop until (or (< (abs (- over under)) eps) (= iter max-iter)) do
            (setq iter (1+ iter))
            (setq m (/ (+ over under) 2))
            (setq fm (funcall fx m))
            (if (< fm 0)
                ;then
                (setq under m)
                ;else
                (setq over m)
            )
        )
        (if (< under over)
            ;then
            (list under over (funcall fx under) (funcall fx over) iter)
            ;else
            (list over under (funcall fx over) (funcall fx under) iter)
        )
    )
)

(defun find-minimum (fx orig-l orig-r &key (eps 1d-6) (max-iter 50) (bracket-first nil))
    (declare (type double-float orig-l orig-r eps))
    (declare (type fixnum max-iter))
    (debug-format "~%find-minimum: Attempting to find a minimum in (~A, ~A)." orig-l orig-r)
    (when bracket-first
        (debug-format " Will begin by attempting to bracket.")
    )
    (when (not bracket-first)
        (debug-format " Will not begin by attempting to bracket.")
        (setq bracket-first (< (derivative-at fx orig-l) 0))
        (when bracket-first
            (debug-format "~%find-minimum: Reversal: will bracket first, due to negative derivative at ~A." orig-l)
        )
    )
    (let
      ((l (if (< orig-l orig-r) orig-l orig-r))
       (r (if (< orig-l orig-r) orig-r orig-l))
       (ml 0d0)
       (mr 0d0)
       (fml 0d0)
       (fmr 0d0)
       (keep-r t)
       (iter 0d0)
      )
        (setq mr (+ l (* *ml-dist* (- r l))))
        (when bracket-first
            (let 
              ((bounds (bracket-minimum fx l r :eps eps :max-iter max-iter)))
                (when (> (length bounds) 2)
                    (setq l (first bounds))
                    (setq mr (second bounds))
                    (setq r (third bounds))
                    (debug-format "~%find-minimum: Using the following bracket from bracket-minimum: ~A ~A ~A" l mr r)
                )
                (when (< (length bounds) 3)
                    (debug-format "~%find-minimum: bracket-minimum failed to discover a bracket.")
                )
            )
        )
;        (setq fml (funcall fx ml))
        (setq fmr (funcall fx mr))
        (loop until (or (< (- r l) eps) (= iter max-iter)) do
            (debug-format "~%find-minimum: In iteration ~A." iter)
            (if keep-r
                ;then
                (progn
                    (setq ml mr)
                    (setq mr (+ ml (* *ml-dist* (- r ml))))
                    (setq fml fmr)
                    (setq fmr (funcall fx mr))
                )
                ;else
                (progn
                    (setq mr ml)
                    (setq ml (+ l (* *mr-dist* (- mr l))))
                    (setq fmr fml)
                    (setq fml (funcall fx ml))
                )
            )
            
            (setq keep-r (> fml fmr))
            (when (and (eql fml fmr) (<= (funcall fx r) fmr))
                ;very special case of unbracketed golden section and exactly equal y values.
                (setq keep-r t)
            )
            (debug-format "~%find-minimum: ~%Points: ~%(~A, ~A) ~%(~A, ~A) ~%(~A, ~A) ~%(~A, ~A)~%keep-r: ~A~%~%" 
                l (funcall fx l) ml fml mr fmr r (funcall fx r)
                keep-r
            )

            (when (not (<= l ml mr r))
                (error "~%find-minimum: Points out of order.")
            )

            (setq l (if keep-r ml l))
            (setq r (if keep-r r mr))
            (setq iter (1+ iter))
        )
        (when (minimum-at-boundary orig-l orig-r l r eps)
            (if bracket-first
                ;then return
                (return-from find-minimum nil)
                ;else return
                (return-from find-minimum
                    (find-minimum fx orig-l orig-r :eps eps :max-iter max-iter :bracket-first t)
                )
            )
        )
        (list l r (funcall fx l) (funcall fx r) iter)
    )
)

(defun minimum-at-boundary (orig-l orig-r new-l new-r eps)
    (or (< (- new-l orig-l) eps) (< (- orig-r new-r) eps))
)

(defun bracket-minimum (fx l r &key (eps 1d-6) (max-iter 50))
    (debug-format "~%bracket-minimum: Attempting to bracket a minimum in (~A, ~A)." l r)
    (when (>= (derivative-at fx l) 0)
        (when (<= (derivative-at fx r) 0)
            (debug-format "~%No negative derivative to bracket minimum in (~A, ~A)." l r)
            (return-from bracket-minimum (list l r))
        )
        (debug-format "~%Reversing function to bracket minimum in (~A, ~A)." l r)
        (let ((rev-bracket (bracket-minimum (reverse-fn fx) (- r) (- l) :eps eps :max-iter max-iter)))
            (return-from bracket-minimum (mapcar #'- (reverse rev-bracket)))
        )
    )
    
    (let 
      ((fl (funcall fx l))
       (fr (funcall fx r))
       (ml (+ l eps))
       (fml 0)
       (dfml 0)
       (mr l)
       (fmr 0)
       (step-size 1d0)
       (iter 0)
      )
        (setq fml (funcall fx ml))
        (loop until (> iter max-iter) do
			(when (< (- r l) eps)
				(return-from bracket-minimum nil)
			)
            (setq iter (1+ iter))
            (debug-format "~%bracket-minimum: iteration ~A." iter)
;            (break)
            (setq step-size (- (quadratic-interpolation fx l ml (/ (+ l ml) 2d0) ) ml)) 
            (debug-format "~%bracket-minimum: Chose step-size ~A by quadratic interpolation." step-size)
            (setq dfml (derivative-at fx ml))
            (when (or (not (curvature-condition fx ml dfml step-size)) (< step-size 1d-10))
                (setq step-size (* *golden-ratio* (- ml l)))
                (debug-format "~%bracket-minimum: Reselected step-size ~A using golden ratio." step-size)
                (when (< step-size 1d-10) (error "~%bracket-minimum: Step-size is 0 or negative.")) 
;                (when (< dfml (derivative-at (+ ml step-size))) ;Just right of maximum; curvature condition too restrictive.
;                )
                (loop while (not (curvature-condition fx ml dfml step-size)) do
                    (setq step-size (* step-size 2d0))
                    (debug-format "~%bracket-minimum: Increased step-size to ~A." step-size)
                    (when (> (+ ml step-size) r)
                        (debug-format "~%bracket-minimum: Curvature condition could not be satisfied!")
                        ; MCM: If the golden ratio has been checked all the way
                        ; out, adding a smaller step size will almost never help
;                        (setq step-size (* *golden-ratio* (- ml l)))
;                        (return)
                        (return-from bracket-minimum nil)
                    )
                )
            )
            (loop while (or (not (sufficient-decrease-condition fx ml fml dfml step-size)) (> (+ ml step-size) r)) do
                (setq step-size (/ step-size 2d0))
                (debug-format "~%bracket-minimum: Reduced step-size to ~A." step-size)
                (when (or (not (curvature-condition fx ml dfml step-size)) (< step-size 1d-10))
                    (debug-format "~%bracket-minimum: Could not meet both Wolfe conditions.")
                    (setq step-size (* step-size 2d0))
;                    (break)
                    (return)
                )
            )
            (debug-format "~%bracket-minimum: Selected step-size is ~A." step-size)
            (when (not (eq (type-of step-size) 'double-float))
                (error "~%bracket-minimum: Step-size is not a double-float!")
            )
            
            (setq mr (+ ml step-size))
            (setq fmr (funcall fx mr))
            
            (when (> mr r)
                (return-from bracket-minimum nil)
            )
                

            (debug-format "~%bracket-minimum: ~%Points: ~%(~A, ~A) ~%(~A, ~A) ~%(~A, ~A) ~%(~A, ~A)~%~%" 
                l (funcall fx l) ml fml mr fmr r (funcall fx r)
            )

            (when (not (<= l ml mr r))
                (error "~%bracket-minimum: Points out of order.")
            )

            (cond 
                ((> fmr fml) ;stepped over a minimum
                    (return-from bracket-minimum (list l ml mr))
                )
                ((> fr fmr) ;found a point less than the right endpoint
                    (return-from bracket-minimum (list ml mr r))
                )
            )
            (setq 
                l ml
                fl fml
                ml mr
                fml fmr
            )
        )
        nil
    )
)

(defun quadratic-interpolation (fx a b c)
    (let
      ((fa (funcall fx a))
       (fb (funcall fx b))
       (fc (funcall fx c))
      )
      (coerce
        (- b
            (safe-divide
              (-
                (* (expt (- b a) 2) (- fb fc))
                (* (expt (- b c) 2) (- fb fa))
              )
              (* 2
                (-
                  (* (- b a) (- fb fc))
                  (* (- b c) (- fb fa))
                )
              )
            )
        )
        'double-float
      )
    )
)


(defvar *wolfe-constant* .1d0)
(defvar *wolfe-constant-2* .9d0)

(defun sufficient-decrease-condition (fx i fi dfi step-size)
    (<= (funcall fx (+ i step-size)) (+ fi (* *wolfe-constant* step-size dfi)))
)

;MCM: This is not the standard curvature condition I looked up; the books don't 
;seem to consider the case where the slope may be increasing at i rather than
;decreasing. Near a maximum, the condition will be very difficult to meet.
(defun curvature-condition (fx i dfi step-size)
    (let ((dfnewi (derivative-at fx (+ i step-size))))
        (or
            (>= dfnewi (* *wolfe-constant-2* dfi))
            (>= dfi (* *wolfe-constant-2* dfnewi))
        )
    )
)

(defun reverse-fn (fx)
;#+:CLISP(ignore-errors (compile nil #'(lambda (x) (funcall fx (- x)))))
#+:ABCL #'(lambda (x) (funcall fx (- x)))
#-:ABCL 
    (handler-bind 
      ((warning 
          #'(lambda (c) 
              (declare (ignore c))
              (if (find-restart 'muffle-warning)
                  ;then
                  (invoke-restart 'muffle-warning)
                  ;else
                  (invoke-restart 'continue)
              )
            )
      ))
        (compile nil #'(lambda (x) (funcall fx (- x))))
    )
;#-:ABCL (handler-case (compile nil #'(lambda (x) (funcall fx (- x)))) (warning () (muffle-warning)) )
)

(defun negative-fun (fun)
;#+:CLISP(ignore-errors (compile nil #'(lambda (x) (- (funcall fun x)))))
;#-:CLISP #'(lambda (x) (- (funcall fun x)))
;		  ((var (intern (symbol-name (gensym)) :utils)))
;			(format t "~%Function name: ~A" var)
;			(make-defun var fun)
;			(format t "~%Funcall(0): ~A" (funcall var 0))
;			(compile var)
;			(format t "~%Funcall(0) (compiled): ~A" (funcall var 0))
;			(fdefinition var)
;		)
#+:ABCL #'(lambda (x) (- (funcall fun x)))
#-:ABCL 
    (handler-bind 
      ((warning 
          #'(lambda (c) 
              (declare (ignore c))
              (if (find-restart 'muffle-warning)
                  ;then
                  (invoke-restart 'muffle-warning)
                  ;else
                  (invoke-restart 'continue)
              )
            )
      ))
        (compile nil #'(lambda (x) (- (funcall fun x))))
    )
)


(defun derivative-at (fx m &key (shift 1d-7))
    (let
      ((fl (funcall fx (- m shift)))
       (fr (funcall fx (+ m shift)))
       (fm (funcall fx m))
      )
        (let
          ((slope1 (/ (- fm fl) shift))
           (slope2 (/ (- fr fm) shift)))
            (return-from derivative-at (/ (+ slope1 slope2) 2))
        )
    )
)


(defun same-sign (x1 x2)
    (> (* x1 x2) 0)
)

(defun safe-divide (num denom)
    (if (< (abs denom) 1d-10)
        ;then
        (* num 1d10)
        ;else
        (/ num denom)
    )
)


;=== Unused functions ===
(defun verify-no-minimum (fx l r step-size)
    (when (> l r)
        (error "~%verify-no-minimum: Improper arguments received.")
    )
    (when (> (funcall fx l) (funcall fx r))
        (return-from verify-no-minimum (verify-no-minimum (reverse-fn fx) (- r) (- l) step-size))
    )
    (let
      ((m (+ l step-size))
       (fl (funcall fx l))
       (fr (funcall fx r))
       (fm 0d0)
      )
        (loop while (< m r) do
            (setq fm (funcall fx m))
            (when (and (> fl fm) (> fr fm))
                (return-from verify-no-minimum nil)
            )
            (setq l m)
            (setq fl fm)
            (setq m (+ m step-size))
        )
        t
    )
)

;;Convert a number to a double-float
(defun dfloat (x)
  (coerce x 'double-float))

;;Check that a number returned from a user function is a double-float
(defun dfloat-check (x)
  (check-type x double-float)
  x)

;;Given a function fx defined on the interval [x1 x2], subdivide the interval
;;into n equally spaced segments and search for zero crossings of the function.
;;Returns the x,y for 
;;the first bracketing of a root.
(defun bracket-first-root (fx x1 x2 n)
 (declare (type double-float x1 x2))
 (declare (type fixnum n))

 (prog (
  (x 0d0) (fp 0d0) (fc 0d0) (dx 0d0))

  (declare (type double-float x fp fc dx))


  (setf x x1) 
  (setf dx (/ (- x2 x1) (dfloat n))) 
  (setf fp (dfloat (funcall fx x))) 

    (when (eq fp 0d0)
     (return-from bracket-first-root (list x))
    )
  (do ((i 1 (+ i 1)))
      ((> i n) t)
      (declare (type fixnum i))
    (setf x (+ x dx))
    (setf fc (dfloat (funcall fx x)))
;    (debug-format "~%Range: [~A,~A ~A,~A]" (- x dx) fp x fc)
;    (break)
    (when (equal fc 0d0)
     (return-from bracket-first-root (list x))
    )
    (when 
     (< (* fc fp) 0d0)
     (return-from bracket-first-root (list (- x dx) x))
    )
    (setf fp fc)
  ) 
   
  (return nil)
 )
)




