(define (domain :rovers-sandstorm)
(:requirements :typing :fluents)
(:types rover waypoint dir time stage)

(:predicates 
    (destination ?x - rover ?y - waypoint) 
    (finished ?x - rover ?y - waypoint) 
    (can_traverse ?x - waypoint ?y - waypoint ?dir - dir)
    (available ?r - rover)
    (visible ?w - waypoint ?p - waypoint)
    (in_sun ?w - waypoint)
    (attempting-move ?r - rover ?dir - dir)
    (blocked ?r - rover)
    (sand-pit-at ?w - waypoint)
    (dug-out ?r - rover ?w - waypoint)
    (sandy ?w - waypoint)
    (windy ?w - waypoint)
    (detects-location ?r - rover ?w - waypoint)
    (sand-covered ?r - rover)
    (recharging ?r - rover)
    (digging ?r - rover)
    (next-compass-point ?dir1 - dir ?dir2 - dir)
    (compass-moved ?r - rover)
    (real-direction ?assumedNorth - dir ?attemptedDir - dir ?realDir - dir)
    (latitude ?w - waypoint ?lat - int) 
    (longitude ?w - waypoint ?long - int) 
    (succeeded ?r - rover)
    (failed ?r - rover)

    (cycle-stage ?stage - stage) 
    (next-stage ?stage1 - stage ?stage2 - stage)
    (current-time ?time - time)
    (next-time ?time1 - time ?time2 - time)
    (storm-begin ?time - time)
    (storm-end ?stage - stage)
    (compass-malfunctions ?time - time ?rover - rover)
    (bumpy ?loc - waypoint)
)

(:hidden sand-pit-at dug-out 
         sandy windy 
         compass-points at compass-moved
;         cycle-stage
         storm-begin storm-end
         compass-malfunctions
         succeeded failed finished 
         blocked
)
(:internal attempting-move)
(:functions 
    (energy ?r - rover) - int 
    (at ?r - rover) - waypoint 
    (compass-points ?r - rover) - dir
)

(:action navigate
    :parameters (?r - rover ?dir - dir) 
    :precondition 
        (and (available ?r) 
             (>= (energy ?r) 8)
        )
    :effect 
        (and (attempting-move ?r ?dir)
             (decrease (energy ?r) 8)
        )
)

;; triggered by navigate
(:event rover-moves
    :parameters (?r - rover)
    :precondition 
        (and (attempting-move ?r ?dir) 
             (not (blocked ?r))
             (eq (at ?r) ?src) 
             (eq (compass-points ?r) ?newNorth)
             (real-direction ?dir ?newNorth ?realDir)
             (can_traverse ?src ?dest ?realDir)
             (visible ?src ?dest) 
         )
     :effect 
        (and (set (at ?r) ?dest))
)

;; triggered by navigate
(:event time-progresses
    :parameters ()
    :precondition (and (attempting-move ?rover ?dir) (cycle-stage ?prior-stage) 
                       (next-stage ?prior-stage ?next-stage) (current-time ?prior-time)
                       (next-time ?prior-time ?new-time))
    :effect (and (not (cycle-stage ?prior-stage)) (cycle-stage ?next-stage)
                 (not (current-time ?prior-time)) (current-time ?new-time)
                 (not (attempting-move ?rover ?dir)))
)

;; triggered by navigate or sandstorm-ends, stopped by self-effect
(:event location-detected
    :parameters (?r - rover)
    :precondition 
        (and (eq (at ?r) ?w) (not (sand-covered ?r)) (not (detects-location ?r ?w)))
    :effect (and (detects-location ?r ?w))
)

;; triggered by navigate, stopped by self-effect
(:event location-left
    :parameters (?r - rover)
    :precondition 
        (and (detects-location ?r ?w) (neq (at ?r) ?w))
    :effect (and (not (detects-location ?r ?w)))
)

;triggered by time-progresses, stopped by self-effect
(:event sandstorm-starts
    :parameters (?rover - rover)
    :precondition (and (is rover ?rover)
                       (storm-begin ?cur-time) (current-time ?cur-time) 
                       (cycle-stage ?cur-stage) 
                       (next-stage ?prior-stage ?cur-stage)
                       
                       ;; ensures that starts don't happen during sandstorm
                       (not (storm-end ?prior-stage)))
    :effect (and (sand-covered ?rover) (storm-end ?prior-stage))
)

;triggered by time-progresses, stopped by self-effect
(:event sandstorm-ends
    :parameters (?rover - rover)
    :precondition (and (is rover ?rover) (storm-end ?end-stage) (cycle-stage ?end-stage)
    
                       ;; ensures that ends doesn't happen at same time as starts
                       (current-time ?cur-time) (not (storm-begin ?cur-time))

                       ;; ensures that ends only happens while sandstorm on
                       (sand-covered ?rover))
    :effect (and (not (sand-covered ?rover)) (not (storm-end ?end-stage)))
)

;triggered by time-progresses, stopped by self-effect
(:event compass-malfunction-occurs
    :parameters (?r - rover)
    :precondition (and (current-time ?time) (compass-malfunctions ?time ?r)
                       (eq (compass-points ?r) north)
                  )
    :effect (and (set (compass-points ?r) south))
)

;triggered by navigate, stopped by self-effect
(:event sees-bump
    :parameters (?r - rover ?l - waypoint)
    :precondition (and (sand-pit-at ?l) (eq (at ?r) ?l2) 
                       (visible ?l ?l2) (not (bumpy ?l)))
    :effect (and (bumpy ?l))
)

;triggered by navigate, stopped by self-effect
(:event becomes-trapped-in-pit
    :parameters (?r - rover)
    :precondition 
        (and (eq (at ?r) ?w) 
             (sand-pit-at ?w) 
             (not (blocked ?r)) 
             (not (dug-out ?r ?w))
        )
    :effect (and (blocked ?r))
)

;triggered by time-progresses, stopped by self
(:event sandstorm-blows
    :parameters (?rover - rover)
    :precondition (and (current-time ?time) 
                       (storm-begin ?time)
                       (forall (?any-stage - stage)
                           (is-stage ?any-stage)
                           (not (storm-end ?any-stage))
                       )
                       (eq (at ?rover) ?loc) 
                       (can_traverse ?loc ?new-loc west)
                       (visible ?loc ?new-loc)
                       (not (blocked ?rover))
                       (not (sand-covered ?rover))
                  )
    :effect (and (set (at ?rover) ?new-loc) (sand-covered ?rover))
)



(:action dig-out
    :parameters (?r - rover)
    :precondition 
        (and )
    :effect
        (and (decrease (energy ?r) 80) (digging ?r) )
)

(:event dig-out-event
    :parameters (?r - rover)
    :precondition 
        (and (digging ?r) (eq (at ?r) ?w))
    :effect
        (and (not (digging ?r)) (dug-out ?r ?w) (not (blocked ?r)))
)

(:event left-sand-pit
    :parameters (?r - rover)
    :precondition (and (dug-out ?r ?w) (neq (at ?r) ?w))
    :effect (and (not (dug-out ?r ?w)))
)


(:action recharge
    :parameters (?r - rover)
    :precondition (and )
    :effect (and (recharging ?r)) 
)

(:event recharge-event
    :parameters (?r - rover)
    :precondition (and (recharging ?r) (eq (at ?r) ?w) (in_sun ?w))
    :effect (and (set (energy ?r) 100) )
)

(:event recharging-over
    :parameters (?r - rover)
    :precondition (and (recharging ?r))
    :effect (and (not (recharging ?r)))
)

(:action finish
    :parameters (?r - rover)
    :precondition ()
    :effect (and (not (available ?r)))
)

(:event finished-event
    :parameters (?r - rover ?w - waypoint)
    :precondition (and (eq (at ?r) ?w) (not (available ?r)) (not (finished ?r ?w)))
    :effect (and (finished ?r ?w))
)

(:event success
    :parameters (?r - rover ?w - waypoint)
    :precondition (and (finished ?r ?w) (destination ?r ?w) (not (succeeded ?r)))
    :effect (and (succeeded ?r))
)

(:event failure
    :parameters (?r - rover ?w - waypoint)
    :precondition (and (finished ?r ?w) (not (destination ?r ?w)) (not (failed ?r)))
    :effect (and (failed ?r))
)

)
