(define (domain :stunt-crash)
(:requirements :typing)
(:types physobj)

(:predicates 
    (acceleration-controllable ?obj - physobj)
    (under-acceleration ?obj - physobj)
    (wall-at ?x - real)
    (collision-speed ?obj - physobj ?vel - real)
    (moving-object ?obj - physobj)
    (lame ?obj - physobj)
    (successful ?obj - physobj)
    (dead ?obj - physobj)
)

(:functions
    (pos ?obj - physobj) - real
    (vel ?obj - physobj) - real
    (accel ?obj - physobj) - real
)


(:process apply-acceleration
 :parameters (?obj - physobj)
 :precondition (under-acceleration ?obj)
 :effect (and (increase (vel ?obj) (* #t (accel ?obj))))
)

(:process change-position
    :parameters (?obj - physobj)
    :precondition ()
    :effect
    (and 
        (increase (pos ?obj) (* #t (vel ?obj)))
    )
)


(:event stops
 :parameters (?obj - physobj)
 :precondition 
 (and
    (under-acceleration ?obj)
    (<= (vel ?obj) 0)
 )
 :effect 
    (and 
        (set (accel ?obj) 0)
        (not (under-acceleration ?obj))
        (not (moving-object ?obj))
    )
)
(:event hit-wall
    :parameters (?obj - physobj)
    :precondition 
    (and
        (moving-object ?obj)
        (wall-at ?x)
        (>= (pos ?obj) ?x)
        (<= (pos ?obj) (+ ?x .1))
        (> (vel ?obj) 0)
    )
    :effect
    (and
        (assign ?speed (vel ?obj))
        (collision-speed ?obj ?speed)
        (not (moving-object ?obj))
        (set (vel ?obj) 0)
        (set (accel ?obj) 0)
    )
)

(:event lame-crash
    :parameters (?obj - physobj)
    :precondition
        (and 
            (collision-speed ?obj ?speed)
            (<= ?speed 8.94)
            (not (lame ?obj))
        )
    :effect (and (lame ?obj))
)

(:event cool-crash
    :parameters (?obj - physobj)
    :precondition
        (and 
            (collision-speed ?obj ?speed)
            (< ?speed 13.4)
            (> ?speed 8.94)
            (not (successful ?obj))
        )
    :effect (and (successful ?obj))
)

(:event deadly-crash
    :parameters (?obj - physobj)
    :precondition
        (and 
            (collision-speed ?obj ?speed)
            (>= ?speed 13.4)
            (not (dead ?obj))
        )
    :effect (and (dead ?obj))
)

(:action start-accelerating 
    :parameters (?obj - physobj)
    :precondition (acceleration-controllable ?obj)
    :effect 
        (and 
            (moving-object ?obj)
            (set (accel ?obj) 25)
            (under-acceleration ?obj)
        )
)

(:action start-braking 
    :parameters (?obj - physobj)
    :precondition (acceleration-controllable ?obj)
    :effect 
        (and
            (set (accel ?obj) -25)
            (under-acceleration ?obj)
        )
)

;;;Wait for a amount of time ;;automatic in all domains
;(:action wait
;   :parameters (?dur - int)
;   :duration ?dur
;   :precondition ()
;   :effect ()
;)


(:method (do-cool-crash ?physobj)
    finished
    (
        (successful ?physobj)
    )
    (
    )
)

(:method (do-cool-crash ?physobj)
    keep-on
    (
        (not (collision-speed ?physobj ?x))
        (moving-object car)
    )
    (
        (!wait .1)
        (do-cool-crash ?physobj)
    )
)

(:method (do-cool-crash ?physobj)
    time-to-brake
    (
        (not (collision-speed ?physobj ?x))
        (not (under-acceleration ?physobj))
        (moving-object car)
    )
    (
        (!start-braking ?physobj)
        (do-cool-crash ?physobj)
    )
)

)

