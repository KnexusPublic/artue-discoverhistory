(define (domain :planety)
(:requirements :typing :durative-actions :fluents :time
:negative-preconditions :timed-initial-literals)
(:types equipment)
(:constants unit - equipment)
(:predicates (day) (commsOpen) (readyForObs1) (readyForObs2)
(gotObs1) (gotObs2) (is_charging) (is_discharging))
(:functions (demand) (supply) (soc) (charge_rate) (daytime)
(heater_rate) (dusk) (dawn)
(fullprepare_durtime) (prepareobs1_durtime) (prepareobs2_durtime)
(observe1_durtime) (observe2_durtime) (obs1_rate) (obs2_rate)
(A_rate) (B_rate) (C_rate) (D_rate) (safeLevel)
(solar_const) - real)



(:event nightfall
:parameters ()
:precondition (and (day) (>= (daytime) (dusk)))
:effect (and (set (daytime) (- (dawn)) )
(not (day)))
)
(:event daybreak
:parameters ()
:precondition (and (not (day)) (>= (daytime) 0))
:effect (day)
)

(:event charging_starts
:parameters ()
:precondition (and (not (is_charging)) (< (demand) (supply)) (day))
:effect (is_charging)
)

(:process charging
:parameters ()
:precondition (is_charging)
:effect 
    (and 
        (increase (soc) 
            (* #t 
                (* (* (- (supply) (demand)) (charge_rate)) (- 100 (soc))))))
)


(:event charging_ends
:parameters ()
:precondition (and (is_charging) (or (>= (demand) (supply)) (not (day))))
:effect (not (is_charging))
)



(:event discharging_starts
:parameters ()
:precondition (and (not (is_discharging)) (> (demand) (supply)))
:effect (is_discharging)
)

(:process discharging
:parameters ()
:precondition (is_discharging)
:effect (decrease (soc) (* #t (- (demand) (supply))))
)

(:event discharging_ends
:parameters ()
:precondition (and (is_discharging) (<= (demand) (supply)))
:effect (not (is_discharging))
)


(:process generating
:parameters ()
:precondition (day)
:effect 
    (and (increase (supply) 
            (* #t (* (* (solar_const) (daytime)) (+ (* (daytime) (- (* 4 (daytime)) 90)) 450))))
         (increase (daytime) (* #t 1))
    )
)

; ds = .3t *(t(4t - 90) + 450) dt
; ds = .3t(4t^2 - 90t + 450) dt
; ds = (1.2t^3 - 27t^2 + 450t) dt
; s_1 = .03t^4 - .9t^3 + .03*225t^2 + s_0 
; s_1 = .03 * 6561 - .9 * 729 + .03 * 225 * 81 + s_0 | t = 9 
; s_1 = 196.83 - 656.1 + 546.75 + s_0 | t = 9
; s_1 = 87.48 + s_0 | t = 9

(:process night_operations
:parameters ()
:precondition (not (day))
:effect (and (increase (daytime) (* #t 1))
             (decrease (soc) (* #t (heater_rate)))
        )
)

(:durative-action fullPrepare
:parameters ()
:duration (= ?duration (fullprepare_durtime))
:condition (over all (> (soc) (safelevel)))
:effect (and (at start (increase (demand) (A_rate)))
(at end (decrease (demand) (A_rate)))
(at end (readyForObs1))
(at end (readyForObs2)))
)

(:durative-action prepareObs1
:parameters ()
:duration (= ?duration (prepareobs1_durtime))
:condition (over all (> (soc) (safelevel)))
:effect (and (at start (increase (demand) (B_rate)))
(at end (decrease (demand) (B_rate)))
(at end (readyForObs1)))
)

(:durative-action prepareObs2
:parameters ()
:duration (= ?duration (prepareobs2_durtime))
:condition (over all (> (soc) (safelevel)))
:effect (and (at start (increase (demand) (C_rate)))
(at end (decrease (demand) (C_rate)))
(at end (readyForObs2)))
)

(:durative-action observe1
:parameters ()
:duration (= ?duration (observe1_durtime))
:condition (and (at start (readyForObs1))
(over all (> (soc) (safelevel)))
(over all (not (commsOpen))))
:effect (and (at start (increase (demand) (obs1_rate)))
(at end (decrease (demand) (obs1_rate)))
(at end (not (readyForObs1)))
(at end (gotObs1)))
)
(:durative-action observe2
:parameters ()
:duration (= ?duration (observe2_durtime))
:condition (and (at start (readyForObs2))
(over all (> (soc) (safelevel)))
(over all (not (commsOpen))))
:effect (and (at start (increase (demand) (obs2_rate)))
(at end (decrease (demand) (obs2_rate)))
(at end (not (readyForObs2)))
(at end (gotObs2)))
)
)

