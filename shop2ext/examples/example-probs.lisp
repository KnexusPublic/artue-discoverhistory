 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: Jun 3, 2009
 ;;;; Project Name: ARTUE
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; Examples that illustrate usage of SHOP2-PDDL+.

(load "main/artue-loader")
(in-package :artue)

;; This flag from pddlplus.lisp controls whether reporting occurs when a process
;; sets no continuous variables. Ordinarily, this is an important warning, but 
;; the durative action conversion process creates processes during which nothing 
;; happens.
(setq shop2::*ignore-functionless-processes* t)

(defun skydiver-test1 (&aux prob init-state final-state)
    (load (make-pathname :directory '(:relative "shop2ext" "examples") :name "skydiver.pddl"))
    (load (make-pathname :directory '(:relative "shop2ext" "examples") :name "skydiver1"))
    (setq prob (get :skydiver1 :probs))
    (setq init-state
        (shop2.common::make-fun-state 
            (artue::pddl-problem-init prob) 
            :types (artue::pddl-problem-types prob)
            :fun-values (artue::pddl-problem-values prob)
        )
    )
    
    (setq final-state
        (extrapolate-state
            (get :skydiver :domain) 
            init-state
            '(!wait 500)
        )
    )
    
    (let
      ((final-velocity (third (first (grep-trees (state-atoms final-state) 'artue::final-velocity))))
       (crash-time (third (first (grep-trees (state-atoms final-state) 'artue::crash-time))))
      )
        (format t "~&~%Falling from 10000 feet, crashes at ~A seconds, traveling at ~A m/s."
            crash-time final-velocity
        )
        (when (or (> crash-time 46) (< crash-time 45) (> final-velocity -442) 
                  (< final-velocity -443))
            (format t "~%~%Fail.")
        )
    )
)

(defun stunt-crash-test1 ()
    (load (make-pathname :directory '(:relative "shop2ext" "examples") :name "stunt-crash.pddl"))
    (load (make-pathname :directory '(:relative "shop2ext" "examples") :name "stunt-crash1"))
    (artue::export-problem-to-shop (get :stunt-crash1 :probs))
    (find-plans :stunt-crash1 :domain (get :stunt-crash :domain) :verbose :plans :gc nil)
)

(defun rovers-sandstorm-test1 ()
    (load (make-pathname :directory '(:relative "shop2ext" "examples") :name "rovers-sandstorm" :type "htn"))
    (load (make-pathname :directory '(:relative "shop2ext" "examples") :name "rovers-sandstorm1"))
    (export-problem-to-shop (get :rovers-sandstorm1 :probs))
    (find-plans :rovers-sandstorm1 :domain (get :rovers-sandstorm-htn :domain) :verbose :plans :gc nil)
)

(defun planety-test1 ()
    (load "shop2ext/examples/conversion.lisp")
    (convert-da->pddl+
        (make-pathname 
            :directory '(:relative "shop2ext" "examples") 
            :name "planety.pddl"
        )
        (make-pathname 
            :directory '(:relative "shop2ext" "examples") 
            :name "planety-converted.pddl"
        )
    )
    (load (make-pathname :directory '(:relative "shop2ext" "examples") :name "planety" :type "htn"))
    (load (make-pathname :directory '(:relative "shop2ext" "examples") :name "planety1"))
    (export-problem-to-shop (get :planety1 :probs))
    (find-plans :planety1 :domain (get :planety-htn :domain) :verbose :plans :gc nil)
)
