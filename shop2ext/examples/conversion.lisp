 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; Date created: January 2010
 ;;;; Project Name: ARTUE
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; Functions for automatic conversion of durative actions to actions, 
 ;;;; processes and events, allows SHOP2-PDDL+ to plan for durative actions.

(in-package :artue)

(defun convert-rover-domain ()
  (convert-da->pddl+
   (make-pathname 
    :directory '(:relative "shop2ext" "examples" "singlerover") 
    :name "roverscontinuous.pddl")
   (make-pathname 
    :directory '(:relative "shop2ext" "examples" "singlerover") 
    :name "roverscontinuous-converted.pddl")))

(defparameter *colin-rover-problems*
  '("pfile01" "pfile02" "pfile03" "pfile04" "pfile05" "pfile06" "pfile07" "pfile08" "pfile09" "pfile10" 
    "pfile11" "pfile12" "pfile13" "pfile14" "pfile15" "pfile16" "pfile17" "pfile18" "pfile19" "pfile20")) 

(defun convert-rover-problems (&key (problems *colin-rover-problems* ))
  (dolist (problem problems)
    (convert-rover-problem problem)))

(defun convert-rover-problem (problem)
    (convert-problem
     (make-pathname 
      :directory '(:relative "shop2ext" "examples" "singlerover") 
      :name problem)
     (make-pathname 
      :directory '(:relative "shop2ext" "examples" "singlerover") 
      :name (concatenate 'string problem "-converted"))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; converting problems, namely adding the time fluent and setting it to zero

(defun convert-problem (input-file output-file)
  (with-open-file (ifile input-file :direction :input)
    (with-open-file (ofile output-file :direction :output :if-exists :error)
      (do ((form (read ifile nil 'eof) (read ifile nil 'eof)))
	  ((eql form 'eof) output-file)
	(cond ((and (consp form)
		    (eql (car form) 'define))
	       (format ofile "~%~S" (convert-problem-def form)))
	      (t (format ofile "~%~S" form)))))))

(defun convert-problem-def (problem-def)
  (let ((new-def (list (car problem-def))))
    (dolist (entry (cdr problem-def))
      (cond ((eq (car entry) :init)
	     (push (remove-if #'fluent? entry) new-def)
	     (push (make-values-entry entry) new-def))
	    (t (push entry new-def))))
    (reverse new-def)))

(defun fluent? (fact)
  (and (consp fact)
       (eql (car fact) '=)))

(defun make-values-entry (entry)
  (cons ':values
	(cons
	 '((current-time) 0)
	 (mapcar #'rest (remove-if-not #'fluent? entry)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; converting domains

(defun convert-da->pddl+ (input-file output-file)
  (set-dispatch-macro-character 
   #\# #\t        ;dispatch on #t
   #'(lambda (s c n) (declare (ignore s c n)) '?__dur))
  (with-open-file (ifile input-file :direction :input)
    (with-open-file (ofile output-file :direction :output :if-exists :supersede)
      (do ((form (read ifile nil 'eof) (read ifile nil 'eof)))
	  ((eql form 'eof) output-file)
	(cond ((and (consp form)
		    (eql (car form) 'define))
	       (format ofile "~%~S" (switch-symbols-to-package (convert-domain-def form) *package*)))
	      (t (format ofile "~%~S" form)))))))

(defun convert-domain-def (domain-def )
  (add-new-predicates 
   (mapcan #'convert-form domain-def)
   domain-def))

;;;Definitely not the best way to do this
(defun add-new-predicates (new-domain-def old-domain-def)
  (setf (cdr (last (assoc :predicates (cdr new-domain-def))))
        (new-predicate-defs old-domain-def))
  new-domain-def)

(defun new-predicate-defs (old-domain-def)
  (mapcar
   #'(lambda (da)
       (append (make-duration-stmt (second da) (get-keyword :parameters da)) '(- real)))
   (das-in-definition old-domain-def)))

;;;can't add predicate defs this way
(defun convert-form (form)
  (cond ((and (consp form) (eql (car form) :functions))
	 (list (add-time-def form)))
	((and (consp form) (eql (car form) :durative-action))
	 (durative-action->pddl+ form))
	(t (list form))))
	 
	       
;;not adding yet
(defun add-predicate-defs (domain-def)
  (car domain-def))

(defun add-time-def (fluent-defs)
  (append fluent-defs '((current-time) - real)))

  

;; each durative action should become 1 action, 1 process, and 2 events 
;; one for success and one for failure
(defun durative-action->pddl+ (da)
  (let* ((da (clean-da da))
	 (name (second da))
	 (parameters (get-keyword :parameters da))
	 (duration (third (get-keyword :duration da)))
	 (start-conds (start-conds da))
	 (ongoing-conds (ongoing-conds da))
	 (end-conds (end-conds da)) ;;not sure what to do about end-condition
	 (start-effects (start-effects da))
	 (ongoing-effects (ongoing-effects da))
	 (end-effects (end-effects da)))
    (declare (ignore end-conds))
    (remove nil
	    (list
	     (make-start-action name parameters duration start-conds ongoing-conds start-effects)
	     (make-process name parameters ongoing-effects)
	     (make-event-success name parameters ongoing-conds end-effects)
	     (when (continuous-conditions ongoing-conds)
	       (make-event-failure name parameters ongoing-conds end-effects))
	     ))))

;;; makes all inequalities <= or >=
(defun clean-da (da)
  (sublis '((< . <=)(> . >=)) da))
  

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; start-action

(defun make-start-action (name parameters duration start-conds ongoing-conds start-effects)
  (list :action
	(append-symbol name "-START")
	:parameters
	parameters
	:precondition
	(start-action-conds start-conds ongoing-conds)
	:effect
	(start-action-effect name parameters duration start-effects)
	)
      )

(defun start-action-conds (start-conds ongoing-conds)
  (cons 'and (append start-conds ongoing-conds)))

(defun start-action-effect (name parameters duration start-effects)
  (append (list 'and 
		(make-assign-stmt duration)
		(make-duration-stmt name parameters))
	  start-effects))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; process

(defun make-process (name parameters ongoing-effects)
  (list :process
	(append-symbol name "-PROCESS")
	:parameters
	parameters
	:precondition
	(process-conds name parameters)
	:effect
	(process-effs ongoing-effects)
	)
      )

;;; Currently all the ongoing conds will be dealt with in the events and actions
(defun process-conds (name parameters)
  (make-duration-stmt name parameters))

(defun process-effs (ongoing-effects)
  (cond ((= (length ongoing-effects) 1)
	 (car ongoing-effects))
	(t
	 (cons 'and ongoing-effects))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; event end succecss

(defun make-event-success (name parameters ongoing-conds end-effects)
  (list :event
	(append-symbol name "-SUCCESS")
	:parameters
	parameters
	:precondition
	(event-success-conds name parameters ongoing-conds)
	:effect
	(event-success-effects name parameters end-effects)
	)
      )

(defun event-success-conds (name parameters ongoing-conds)
  (append
   (list 'and
	 (make-duration-stmt name parameters)
	 (make-time-comparison-stmt))
   ongoing-conds))
	
(defun event-success-effects (name parameters end-effects)
  (append
   `(and (not ,(make-duration-stmt name parameters)))
   end-effects))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; event end failure (not sure we need this)

(defun make-event-failure (name parameters ongoing-conds end-effects)
  (list :event
	(append-symbol name "-FAILURE")
	:parameters
	parameters
	:precondition
	(event-failure-conds name parameters ongoing-conds)
	:effect
	(event-failure-effects name parameters end-effects)
	)
      )

;;; not sure Shop2 handles disjuncts
(defun event-failure-conds (name parameters ongoing-conds)
  (list 'and (make-duration-stmt name parameters)
	(if (= (length (continuous-conditions ongoing-conds)) 1)
	    (flip-comparison (car (continuous-conditions ongoing-conds)))
	    (cons 'or (mapcar #'flip-comparison (continuous-conditions ongoing-conds))))))
	
(defun event-failure-effects (name parameters end-effects)
  (declare (ignore end-effects))
  (cons 'not (list (make-duration-stmt name parameters))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; statements
(defun make-duration-stmt (name parameters)
  (cons (append-symbol name "-DURATION")
	(append (parameter-names parameters)
		'(?end-duration-time))))

(defun make-assign-stmt (duration)
  `(assign ?end-duration-time (+ (current-time) ,duration)))

(defun make-time-comparison-stmt ()
  `(>= (current-time) ?end-duration-time))

(defun parameter-names (list)
  (remove-if-not #'variablep? list))
		     
(defun variablep? (sym)
  (and (symbolp sym) (equal (elt (symbol-name sym) 0) #\?)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; accessors

(defun get-conditions (da)
  (let ((conds (get-keyword :condition da)))
    (if (eql (car conds) 'and)
	(cdr conds)
	(list conds))))

(defun start-conds (da)
  (mapcar #'third
	  (remove-if-not 
	   #'(lambda (cond)
	       (and (eql (first cond) 'at)
		    (eql (second cond) 'start)))
	   (get-conditions da))))

(defun ongoing-conds (da)
  (mapcar #'third
	  (remove-if-not 
	   #'(lambda (cond)
	       (and (eql (first cond) 'over)
		    (eql (second cond) 'all)))
	   (get-conditions da))))

(defun end-conds (da)
  (mapcar #'third
	  (remove-if-not 
	   #'(lambda (cond)
	       (and (eql (first cond) 'at)
		    (eql (second cond) 'end)))
	   (get-conditions da))))
   
(defun get-effs (da)
  (let ((effs (get-keyword :effect da)))
    (when effs
      (if (eql (car effs) 'and)
	  (cdr effs)
	  (list effs)))))

(defun start-effects (da)
  (mapcar #'third
	  (remove-if-not 
	   #'(lambda (effect)
	       (and (eql (first effect) 'at)
		    (eql (second effect) 'start)))
	   (get-effs da))))

(defun ongoing-effects (da)
  (remove-if-not 
   #'(lambda (effect)
       (or (eql (first effect) 'decrease)
	   (eql (first effect) 'increase)))
   (get-effs da)))

(defun end-effects (da)
  (mapcar #'third
	  (remove-if-not 
	   #'(lambda (effect)
	       (and (eql (first effect) 'at)
		    (eql (second effect) 'end)))
	   (get-effs da))))
   

(defun get-keyword (key da-def)
  (second (member key da-def)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; utils

(defun append-symbol (symbol string)
  (intern (concatenate 'string (symbol-name symbol) string) :cl-user))

(defun continuous-conditions (conds)
  (remove-if-not 
   #'(lambda (cond)
       (member (car cond) '(>= <=)))
   conds))

(defun flip-comparison (cond)
  (cond ((eql (car cond) '>=) (cons '<= (cdr cond)))
	((eql (car cond) '<=) (cons '>= (cdr cond)))
	(t (error "expected comparison operator in first position ~A" cond)))) 

(defun das-in-definition (domain-def)
  (remove-if-not #'(lambda (item)
		     (and (consp item)
			  (eql (car item) :durative-action)))
		 domain-def))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; End of Code
