(define (domain :skydiver)
(:requirements :typing)
(:types physobj)

(:predicates 
    (is-falling ?obj - physobj)
    (final-velocity ?obj - physobj ?vel - real)
    (crash-time ?obj - physobj ?time - real)
)

(:functions
    (current-time) - real
    (xpos ?obj - physobj) - real
    (ypos ?obj - physobj) - real
    (zpos ?obj - physobj) - real
    (xvel ?obj - physobj) - real
    (yvel ?obj - physobj) - real
    (zvel ?obj - physobj) - real
)


(:process track-time
 :parameters ()
 :precondition ()
 :effect (increase (current-time) (* #t 1))
)

(:process acceleration-due-to-gravity
    :parameters (?obj - physobj)
    :precondition 
        (and (is-falling ?obj))
    :effect (and (decrease (zvel ?obj) (* #t 9.8)))
)

(:process change-position
    :parameters (?obj - physobj)
    :precondition ()
    :effect
    (and 
        (increase (xpos ?obj) (* #t (xvel ?obj)))
        (increase (ypos ?obj) (* #t (yvel ?obj)))
        (increase (zpos ?obj) (* #t (zvel ?obj)))
    )
)

(:event hit-ground
    :parameters (?obj - physobj)
    :precondition 
    (and
        (is-falling ?obj)
        (< (zvel ?obj) 0)
        (<= (zpos ?obj) 0)
    )
    :effect
    (and
        (assign ?t (current-time))
        (assign ?final-vel (zvel ?obj))
        (crash-time ?obj ?t)
        (final-velocity ?obj ?final-vel)
        (not (is-falling ?obj))
        (set (zvel ?obj) 0)
    )
)

;;Wait for a amount of time ;;Wait action now added automatically.
;(:action wait
;   :parameters (?dur - int)
;   :duration ?dur
;   :precondition ()
;   :effect ()
;)

)
