 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: Jun 3, 2009
 ;;;; Project Name: ARTUE
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; The "fun-state" class extends the fast "mixed-state" class for 
 ;;;; representing environment state with information about the values of 
 ;;;; multiple valued functions, as well as object types.

(in-package :shop2.common) 

(export '(get-named-function-hash make-fun-state 
          get-objects-of-type get-object-type is-assignable-from
;;	  list-state-values list-state-values-as-atoms
	  )
)
(setq *state-encoding* :fun)

;;New state that inherits from mixed-state, but adds function values for 
;;extra pddl functionality.
(defstruct (fun-state (:include mixed-state)
			(:constructor makefunstate)
			(:copier nil))
  fun-hash
  types
  subtypes
)

(defun make-fun-state (atoms 
                        &key (types (make-hash-table :test #'eq)) 
                             (fun-values nil)
                             (supertypes nil)

                      )
    (when (eq (type-of atoms) 'fun-state)
      (return-from make-fun-state (copy-state atoms))
    )
    (let ((st (makefunstate)))
        (setf (state-body st) (make-hash-table :test #'eq))
        (setf (fun-state-fun-hash st) (make-hash-table :test #'eq))
        (setf (fun-state-types st) types)
        (setf (fun-state-subtypes st) 
            (if supertypes
                ;then
                (make-subtype-table supertypes)
                ;else
                (make-hash-table :test #'eq)
            )
        )
        (mapcar+ #'init-value fun-values st)
        (dolist (atom atoms) (insert-atom atom st))
        st
    )
)

(defun make-reproduce-state-call (state)
    (when (null state)
        (return-from make-reproduce-state-call nil)
    )
    (when (not (eq (type-of state) 'fun-state))
        (error "Only fun-state can be passed to make-reproduce-state-call, not ~A." (type-of state))
    )
    (list
        'make-fun-state
        (list 'quote (state-atoms state))
        :types (list 'utils::alist-to-hash (list 'quote (utils::hash-to-alist (fun-state-types state))))
        :fun-values (list 'quote (list-state-values state))
        :supertypes (list 'quote (list-fun-state-supertypes state))
    )
)

(defun list-fun-state-supertypes (st)
    (mapcar #'(lambda (lst) (list (car (last lst)) (car lst)))
        (utils::hash-to-alist (fun-state-subtypes st))
    )
)

(defun make-subtype-table (dom-supers)
    (let
      ((hash (make-hash-table :test #'eq))
       (cur-list nil)
       sub super
      )
        (dolist (super-relation dom-supers)
            (setq sub (first super-relation))
            (setq super (second super-relation))
            (setq cur-list (cons sub (gethash super hash)))
            (setq cur-list (append cur-list (gethash sub hash)))
            (setf (gethash super hash) cur-list)
            (maphash 
                #'(lambda (key val) 
                    (when (member super val) 
                        (push sub (gethash key hash))
                    )
                  )
                hash
            )
        )
        hash
    )
)
    

(defgeneric init-value (fun-value st))
(defmethod init-value (fun-value (st fun-state))
    (set-state-value st (caar fun-value) (cdar fun-value) (cadr fun-value))
)

(defmethod copy-state ((st fun-state))
  (let ((the-copy (make-fun-state nil)))
    (setf (state-body the-copy) (copy-hash-table (state-body st) #'copy-tree))
    (setf (fun-state-fun-hash the-copy) 
        (copy-hash-table (fun-state-fun-hash st) #'copy-hash-table)
    )
    (setf (tagged-state-tags-info the-copy)
	  (copy-tree (tagged-state-tags-info st))
    )
    (setf (fun-state-types the-copy)
	  (copy-hash-table (fun-state-types st) #'copy-tree)
    )
    (setf (fun-state-subtypes the-copy)
	  (copy-hash-table (fun-state-subtypes st) #'copy-tree)
    )
    the-copy
  )
)

;; Global for getting a particular function hash from the state. Used by 
;; auto-created functions in shop2.functions (see pddlplus.lisp).
;; Automatically generates a new function hash if the given name is absent.
(defgeneric get-named-function-hash (fun-st fun-name))
(defmethod get-named-function-hash ((fun-st fun-state) fun-name)
    (let* ((fun-hash (fun-state-fun-hash fun-st))
           (named-hash (gethash fun-name fun-hash 'fail))
         )
        (when (eq named-hash 'fail)
            (setq named-hash (make-hash-table :test #'equal))
            (setf (gethash fun-name fun-hash) named-hash)
        )
        named-hash
    )
)

(defgeneric list-state-values-as-atoms (fun-st))
(defmethod list-state-values-as-atoms ((fun-st fun-state))
    (mapcar #'(lambda (x) (append (car x) (cdr x))) (list-state-values fun-st))
)

(defgeneric list-state-values (fun-st))
(defmethod list-state-values ((fun-st fun-state))
    (let ((value-list nil))
        (maphash 
            #'(lambda (key val) 
                (setq value-list (append value-list (list-fun-values key val)))
              )
            (fun-state-fun-hash fun-st)
        )
        value-list
    )
)

(defun list-fun-values (fun-name fun-hash)
    (let ((value-list nil))
        (maphash 
            #'(lambda (key val) (push (list (cons fun-name key) val) value-list))
            fun-hash
        )
        value-list
    )
)

(defmethod set-state-value ((st fun-state) fun-name fun-params new-val &key (provenance *cur-head*))
    (let ((old-val (gethash fun-params (get-named-function-hash st fun-name))))
        (setf (gethash fun-params (get-named-function-hash st fun-name))
            ;;MCM (1/23): took out coercion to simplify fact matching; might break 
            ;; something.
            ;(if (numberp new-val)
                ;(coerce new-val 'double-float)
                new-val
            ;)
        )
        ;;This records the change for backtracking purposes.
        (push 
            (make-fun-change 
                :action (if old-val 'set-value 'add-value)
                :name   fun-name
                :params fun-params 
                :newval new-val 
                :oldval old-val
                :literal (cons fun-name fun-params)
                :provenance provenance
            )
            (rest (first (fun-state-tags-info st)))
        )
    )
    nil
)

(defmethod add-object-to-state ((st fun-state) obj-name obj-type &key (provenance *cur-head*) &allow-other-keys)
    (setf (gethash obj-name (fun-state-types st)) obj-type)
    ;;This records the change for backtracking purposes.
    (push 
        (make-fun-change 
            :action 'create
            :name   obj-name
            :params obj-type 
            :literal (list 'create obj-name)
            :provenance provenance
        )
        (rest (first (fun-state-tags-info st)))
    )
)

(defmethod remove-object-from-state ((st fun-state) obj-name &key &allow-other-keys)
    (remhash obj-name (fun-state-types st))
)

(deftype action-type () '(member add delete add-value set-value create))

(defstruct (fun-change (:include state-update))
;  (action 'add :type action-type)
;  (literal nil :type list)
    name
    params
    newval
    oldval
)

(defmethod state-eval ((st fun-state) form &aux (params nil))
    (when (symbolp form)
        (return-from state-eval form)
    )
    (when (not (listp form))
        (return-from state-eval (eval form))
    )
    (when (eq (car form) 'quote)
        (return-from state-eval (eval form))
    )
    (setq params (mapcar #'(lambda (param) (state-eval st param)) (cdr form)))
    (if (fboundp (car form))
        ;then
        (handler-case (eval (cons (car form) (mapcar #'(lambda (param) (list 'quote param)) params)))
            (type-error () :fail)
        )
        ;else
        (get-state-value (cons (car form) params) st)
    )
)
        

(defmethod retract-state-changes ((st fun-state) tag)
  (multiple-value-bind (new-tags-info changes)
      (pull-tag-info (fun-state-tags-info st) tag)
    (setf (tagged-state-tags-info st) new-tags-info)
    (dolist (change changes)
      (ecase (state-update-action change)
          (add
              (remove-atom (state-update-literal change) st)
          )
          (delete
              (insert-atom (state-update-literal change) st)
          )
          (set-value
              (setf 
                  (gethash 
                      (fun-change-params change)
                      (get-named-function-hash st (fun-change-name change))
                  ) 
                  (fun-change-oldval change)
              )
          )
          (add-value
              (remhash 
                  (fun-change-params change)
                  (get-named-function-hash st (fun-change-name change))
              )
          )
          (create
              (remove-object-from-state st (state-update-literal change))
          )
      )
    )
  )
)

(defgeneric get-objects-of-type (st type-symbol))
(defmethod get-objects-of-type ((st fun-state) type-symbol)
    (let 
      ((objs nil)
       (types (cons type-symbol (gethash type-symbol (fun-state-subtypes st))))
      )
        (maphash 
            #'(lambda (key val)
                (when 
                    (member val types)
                    ;then
                    (push key objs)
                )
              )
            (fun-state-types st)
        )
        objs
    )
)

(defgeneric is-assignable-from (st ancestor descendant))
(defmethod is-assignable-from ((st fun-state) ancestor descendant)
    (or (eq ancestor descendant) 
        (member descendant (gethash ancestor (fun-state-subtypes st)))
    )
)

(defgeneric get-object-type (st obj))
(defmethod get-object-type ((st fun-state) obj)
    (gethash obj (fun-state-types st)) 
)


(defun make-state (atoms &optional (state-encoding *state-encoding*))
  (ecase state-encoding
    (:list (make-list-state atoms))
    (:mixed (make-mixed-state atoms))
    (:hash (make-hash-state atoms))
    (:bit (make-bit-state atoms))
    (:fun (make-fun-state atoms))))

(deftype action-type () '(member add delete set-value add-value))


(defun print-state-values (state)
    (format t "~%~%State values:~%")
    (maphash 
        #'(lambda (key1 value1)
		    (maphash 
		        #'(lambda (key2 value2)
		            (format t "~% Fluent: ~A Value: ~A" (list key1 key2) value2)
		          )
		        value1
            )
          )
	    (shop2.common::fun-state-fun-hash state)
    )
)

(defmethod state-candidate-atoms-for-goal :around ((st fun-state) goal)
    (if (eq (first goal) 'eq)
        ;then
        (state-candidate-atoms-for-eq st goal)
        ;else
        (call-next-method st goal)
    )
)

(defun state-candidate-atoms-for-eq (st goal)
    (cond
        ((groundp goal)
            (if (eq (get-state-value (second goal) st) (third goal))
                ;then
                (list goal)
                ;else
                nil
            )
        )
        ((groundp (second goal))
            (list (list 'eq (second goal) (get-state-value (second goal) st)))
        )
        (t
            (let* ((name (car (second goal)));(gethash (caadr goal) shop2::*fun-name-map*))
                   (arg-lists
                    (shop2::permutations
                        (mapcar 
                            #'(lambda (x) (shop2.common::get-objects-of-type st x))
                            (shop2::pddl-function-param-types 
                                (find name (shop2::domain-functions *domain*) 
                                      :key #'shop2::pddl-function-name
                                )
                            )
                        )
                    )
                   )
                 )
                ;; 6/10/16: Added the hash-contents call because fluents with 
                ;; varsyms were not being returned as candidates. Previously, 
                ;; only the permutation values were returned.
                (remove-duplicates
                    (append
                        (hash-contents st name) 
                        (mapcar 
                            #'(lambda (arg-list)
                                (let ((call (cons (caadr goal) arg-list)))
                                    (list 'eq call (get-state-value call st))
                                )
                              )
                            arg-lists
                        )
                    )
                    :test #'equalp
                )
            )
        )
    )
)

(defmethod get-state-value (call (state fun-state) &aux hash ret)
    (setq hash (gethash (car call) (fun-state-fun-hash state) 'fail))
    (when (eq hash 'fail)
        (setq hash (gethash (car call) (state-body state)))
        (when (null hash)
           (setq ret (assoc (car call) (shop2::domain-defaults *domain*)))
           (when (null ret)
               (error "~%No value found for ~A." call)
           )
           (return-from get-state-value (second ret))
        )
        (return-from get-state-value (member (cdr call) hash :test #'equalp))
    )
    (setq ret (gethash (cdr call) hash 'fail))
    (when (eql ret 'fail)
        (setq ret (assoc (car call) (shop2::domain-defaults *domain*)))
        (when (null ret)
            (error "~%No value found for ~A." call)
        )
        (setq ret (second ret))
    )
    ret
)

(defun hash-contents (state fun-name &aux hash ret)
    (setq hash (gethash fun-name (fun-state-fun-hash state) 'fail))
    (when (eq hash 'fail)
        (return-from hash-contents nil)
    )
    (setq ret nil)
    (maphash 
        #'(lambda (key val)
            (push (list 'eq (cons fun-name key) val) ret)
          )
        hash
    )
    ret
)

(defun get-fun-value (name params arg-count &optional (state *current-state*))
    (check-fun-call name params arg-count)
    (when (some #'special-var-p params)
        (return-from get-fun-value 'unknown)
    )
    (let ((ret (gethash params (get-named-function-hash state name) 'fail)))
       (when (eql ret 'fail)
           (setq ret (assoc name (shop2::domain-defaults *domain*))) 
           (when (null ret)
               (error "~%No value found for ~A." (cons name params))
           )
           (setq ret (second ret))
       )
       ret
    )
)

(defun put-fun-value (name params new-val arg-count &optional (state *current-state*))
    ; (format t "Name: ~A Params: ~A New-val: ~A" name params new-val)
;    (when (eq (symbol-package name) (find-package :shop2.functions))
;        (break "put-fun-value")
;    )
	(when (null new-val)
		(format t "~%Name: ~A Params: ~A" name params)
		(break)
	)
    (check-fun-call name params arg-count)
    (when (null state)
        (error "put-fun-value called with no usable state.")
    )
    (set-state-value state name params new-val)
    new-val
)


(defun check-fun-call (name params arg-count)
    (if 
        (not (eql (length params) arg-count))
        ;then
        (error "~%Expected ~A args to ~A, found ~A: ~A." arg-count name (length params) (cons name params))
    )
    (if 
        (not (null (var-names params)))
        ;then
        (error "~%Function call not fully bound: ~A." (cons name params))
    )
)


