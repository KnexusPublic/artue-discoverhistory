 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: 2011
 ;;;; Project Name: ARTUE
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file describes a derived shop2 domain that handles sensing 
 ;;;; actions.
(in-package :shop2)

(export '(pddl-plus-sensing-domain get-sense-count get-sense-pred-names 
          get-sensing-action-names initializing-beliefs paradox))

(defclass pddl-plus-sensing-domain ( pddl-plus-domain  )
  (
    (sense-count
       :initform nil
       :accessor get-sense-count
       :initarg  :sense-count
    )

;; Lists the fact types which model sensing information. These facts are created
;; automatically when reading sensing-based domains.
    (sense-pred-names
       :initform nil
       :accessor get-sense-pred-names
       :initarg  :sense-pred-names
    )
    (sensing-action-names
       :initform nil
       :accessor get-sensing-action-names
       :initarg  :sensing-action-names
    )
    (occurring-preds
       :initform nil
       :accessor get-occurring-preds
       :initarg  :occurring-preds
    )
    (observes-exist
       :initform nil
       :accessor get-observes-exist
       :initarg  :observes-exist
    )
  )
  (:documentation "A new class of SHOP2 domain that permits inclusion of
                   PDDL sensing actions, and converts to events."
  )
)

(defmethod parse-domain-item ((domain pddl-plus-sensing-domain) (item-key (eql ':sensor)) item)
    (parse-domain-item domain :action 
        (list 
            :action (second item)
            :parameters (getf item :parameters)
            :precondition (getf item :condition)
            :observe (getf item :sensed)
        )
    )
)
            

(defmethod parse-domain-item :around ((domain pddl-plus-sensing-domain) (item-key (eql ':action)) item)
    (when (starts-with "INTERNAL_ACTION" (symbol-name (second item)))
        (return-from parse-domain-item (call-next-method domain item-key item))
    )
    (let 
      ((name (second item)) (ptr item)
       (added-events 0) (event-trigger-fact (get-trigger-fact domain item))
       (effects-ptr nil) (eff-ptr nil))
        (loop while (not (null (cdr ptr))) do
            (cond
                ;;Observations must be stripped and turned into events
                ((eq (second ptr) :observe)
                    (incf added-events)
                    (let ((observe-name
                            (handle-observe domain (third ptr) 
                                event-trigger-fact added-events
                            )
                         ))
                        (push (cons observe-name name) (get-sensing-action-names domain))
                    )
                    (setf (cdr ptr) (cdddr ptr))
                    (setf (get-observes-exist domain) t)
                )
                ;;Effects must have conditionals stripped and turned into events.
                ((eq (second ptr) :effect)
                    (setq eff-ptr (cdr ptr))
                    (setq effects-ptr (cons 'and (process-effects (third ptr))))
                    ;;Handle the case where the effect is a single conditional
                    (when (eq (car effects-ptr) 'when)
                        (incf added-events)
                        (handle-conditional domain (third ptr) event-trigger-fact added-events)
                        (setf (third ptr) '(and))
                        (setq effects-ptr (third ptr))
                    )
                    ;;Error on an or.
                    (when (eq (car effects-ptr) 'or)
                        (error "Non-deterministic effects not supported.")
                    )
                    ;;Handle the case where the effects is a conjunction 
                    ;;including conditionals
                    (when (eq (car effects-ptr) 'and)
                        (dolist (cnd-eff (remove 'when (cdr effects-ptr) :key #'car :test-not #'eq))
                            (incf added-events)
                            (handle-conditional
                                domain
                                cnd-eff
                                event-trigger-fact
                                added-events
                            )
                        )
                        (let ((new-effs (remove 'when (cdr effects-ptr) :key #'car :test #'eq)))
                            (if new-effs
                                ;then
                                (setf (third ptr) (cons 'and new-effs))
                                ;else
                                (setf (third ptr) nil)
                            )
                        )
                    )
                    (setq ptr (cddr ptr))
                )
                (t (setq ptr (cdr ptr)))
            )
        )
        (when (> added-events 0)
            ;;First, add the event-trigger fact to the list of effects
            (cond
              ((null eff-ptr)
                (setq item 
                    (append item 
                        (list :effect (strip-types event-trigger-fact)))))
              ((eq (car (second eff-ptr)) 'and)
                (setf (cdr (second eff-ptr)) 
                    (append 
                        (list (strip-types event-trigger-fact) '(acting))
                        (cdr (second eff-ptr)))))
              (t
                (setf (second effects-ptr) 
                    (list 'and (strip-types event-trigger-fact) 
                          '(acting)
                          (second eff-ptr))))
            )
            ;; Second, make sure the event trigger fact is in the list of predicates 
            (add-predicate domain event-trigger-fact)
            ;; Third, make sure the event trigger fact goes away.
            (parse-domain-item 
                domain
                :event
                (list 
                    :event (combine-strings-symbols 'stops- (car event-trigger-fact))
                    :parameters (cdr event-trigger-fact)
                    :precondition (list 'and (strip-types event-trigger-fact))
                    :effect (list 'and (list 'not (strip-types event-trigger-fact)))
                )
            )
                
        )
    )
    (call-next-method domain item-key item)
)

(defmethod parse-domain-items :after ((domain pddl-plus-sensing-domain)  items)
    (declare (ignore items))
    (when (get-observes-exist domain)
        (when (null (domain-hidden domain))
            (setf (domain-hidden domain) 
                (set-difference 
                    (append
                        (mapcar #'first (domain-predicates domain))
                        (mapcar #'shop2::pddl-function-name (domain-functions domain))
                    )
                    (union (get-occurring-preds domain) (get-sense-pred-names domain))
                )
            )
        )
        (push 'initializing-beliefs (get-sense-pred-names domain))
        (push 'paradox (get-sense-pred-names domain))
        (parse-domain-item
            domain
            :event
            '( 
                :event acting-ends
                :parameters ()
                :precondition (acting)
                :effect (not (acting))
             )
        )

        (parse-domain-item
            domain
            :action
            '(
                :action _init 
                :parameters () 
                :precondition () 
                :effect (initializing-beliefs)
             )
        )

        (parse-domain-item
            domain
            :event
            '( 
                :event _init-end 
                :parameters () 
                :precondition (initializing-beliefs) 
                :effect (not (initializing-beliefs))
             )
        )

        (dolist (action (mapcar #'cdr (hash-to-alist (domain-operators domain))))
            (when (eq (car action) 'shop2::pddl-action)
                (setf (shop2::pddl-action-effect action)
                    (conjoin-clause '(acting) (shop2::pddl-action-effect action))
                )
            )
        )
        (dolist (action (shop2::domain-full-actions domain))
            (setf (getf action :effect) 
                (conjoin-clause '(acting) (getf action :effect))
            )
        )
    )
    (push '(acting) (domain-predicates domain))
    (push '(paradox) (domain-predicates domain))
)


;; This creates the fact that occurs when an action executes to trigger 
;; events that must follow, including observations and conditionals. Fact 
;; includes types
(defun get-trigger-fact (domain action &aux trigger-name)
    (setq trigger-name (combine-strings-symbols 'occurring- (second action)))
    (push trigger-name (get-occurring-preds domain))
    (setf (get trigger-name 'trigger) (second action))
    (cons trigger-name ;name
        (second (member :parameters action)) ;parameter list
    )
)


(defun handle-observe (domain observe trigger-fact added-events)
    (let ((vars (intersection (strip-types (cdr trigger-fact))
                              observe
                )
          )
          (sense-type (combine-strings-symbols 'sense- (incf (get-sense-count domain))))
          (observe-name (combine-strings-symbols (car trigger-fact) '-observe- added-events))
          sense-pred
         )
        (push sense-type (get-sense-pred-names domain))
        
        (setq sense-pred 
            (cons sense-type
                (make-pddl-param-list
                    vars
                    (make-type-declarations (cdr trigger-fact))
                )
            )
        )
        (add-predicate domain sense-pred)
        
        (parse-domain-item 
            domain
            :event
            (list 
                :event observe-name
                :parameters (cdr trigger-fact)
                :precondition (list 'and (strip-types trigger-fact) observe)
                :effect (cons sense-type vars)
            )
        )
        (parse-domain-item 
            domain
            :event
            (list 
                :event (combine-strings-symbols (car trigger-fact) '-disappear- added-events)
                :parameters (cdr sense-pred)
                :precondition 
                    (list 'and 
                        '(acting)
                        (cons sense-type vars)
                        (list 'not (strip-types trigger-fact))
                    )
                :effect (list 'not (cons sense-type vars))
            )
        )
        observe-name
    )
)

(defun handle-conditional (domain conditional trigger-fact added-events)
    (parse-domain-item 
        domain
        :event
        (list 
            :event (combine-strings-symbols (car trigger-fact) '-event- added-events)
            :parameters (cdr trigger-fact)
            :precondition (list 'and (strip-types trigger-fact) (second conditional))
            :effect (third conditional)
        )
    )
)

(defun strip-types (params &aux new-list ptr)
    (setq new-list (copy-list params))
    (setq ptr new-list)
    (loop while (not (null (cdr ptr))) do
        (if (eq (second ptr) '-)
            ;then
            (setf (cdr ptr) (cdddr ptr))
            ;else
            (setq ptr (cdr ptr))
        )
    )
    new-list
)


;;Responsible for breaking an effects clause down to a single and. 
;;Foralls and exists are stripped.
(defun process-effects (eff-clause)
    (when (not (listp eff-clause))
        (error "Effects badly formed: ~A" eff-clause)
    )
    (case (car eff-clause)
        (and (apply #'append (mapcar #'process-effects (cdr eff-clause))))
        (or (error "Should not have or in effects."))
        ((forall exists) (process-effects (third eff-clause)))
        ((nil) nil)
        (t (list eff-clause))
    )
)

(defun conjoin-clause (new-clause orig-clause)
    (if (eq (car orig-clause) 'and)
        ;then
        (cons 'and (cons new-clause (cdr orig-clause)))    
        ;else
        (list 'and new-clause orig-clause)
    )
)

(defun add-predicate (domain pred)
    (push pred (domain-predicates domain))
)


