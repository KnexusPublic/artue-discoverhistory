 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: March 20, 2013
 ;;;; Project Name: ARTUE
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file interfaces with Maxima to produce algebraic derivatives and 
 ;;;; integrations.

(in-package :maxima)

(meval* `((msetq) $FILE_SEARCH_MAXIMA
             ((mlist)
              ,(namestring (merge-pathnames (make-pathname :directory '(:relative "shop2ext" "maxima" "extras" "share") :name "###.mac")))
              ,(namestring (merge-pathnames (make-pathname :directory '(:relative "shop2ext" "maxima" "extras" "share" "diffequations") :name "###.mac")))
              ,(namestring (merge-pathnames (make-pathname :directory '(:relative "shop2ext" "maxima" "extras" "share" "simplification") :name "###.mac")))
              )
          )
)

(setf (get '+ 'maxima-op) 'mplus)
(setf (get '- 'maxima-op) 'mminus)
(setf (get '* 'maxima-op) 'mtimes)
(setf (get '/ 'maxima-op) 'mquotient)
(setf (get 'expt 'maxima-op) 'mexpt)
(setf (get '< 'maxima-op) 'mlessp)
(setf (get '> 'maxima-op) 'mgreaterp)
(setf (get '>= 'maxima-op) 'mgeqp)
(setf (get '<= 'maxima-op) 'mleqp)
(setf (get '= 'maxima-op) 'mequal)
(setf (get 'progn 'maxima-op) 'progn)
(setf (get 'abs 'maxima-op) 'mabs)
(setf (get 'setq 'maxima-op) 'msetq)
(setf (get 'not 'maxima-op) 'mnot)
(setf (get 'and 'maxima-op) 'mand)
(setf (get 'or 'maxima-op) 'mor)
(setf (get 'cl::sin 'maxima-op) '%sin)
(setf (get 'cl::cos 'maxima-op) '%cos)
(setf (get 'cl::tan 'maxima-op) '%tan)
(setf (get 'cl::asin 'maxima-op) '%asin)
(setf (get 'cl::acos 'maxima-op) '%acos)
(setf (get 'cl::atan 'maxima-op) '$atan2)

(defvar *remembered-symbols* (make-hash-table))

(defun convert-lisp-to-maxima (expr)
    (when (atom expr)
        (return-from convert-lisp-to-maxima expr)
    )
    (when (and (eq (car expr) '/) (cdddr expr))
        (return-from convert-lisp-to-maxima
            (convert-lisp-to-maxima
                (list '/ (second expr) (cons '* (cddr expr)))
            )
        )
    )
    (when (eq (car expr) '-)
        (return-from convert-lisp-to-maxima
            (convert-lisp-to-maxima
                (cons '+ 
                    (cons (second expr) 
                        (mapcar 
                            #'(lambda (sub-expr)
                                (list '* -1 sub-expr)
                              )
                              (cddr expr)
                        )
                    )
                )
            )
        )
    )
        
    (when (eq (car expr) 'sqrt)
        (return-from convert-lisp-to-maxima
            (convert-lisp-to-maxima
                (list 'expt (second expr) '(/ 1 2))
            )
        )
    )
        
    (let ((maxima-op (get (car expr) 'maxima-op)))
        (when (null maxima-op) 
            (setq maxima-op 
                (intern
                    (concatenate 'string "$" (symbol-name (car expr)))
                    :maxima
                )
            )
            (setf (gethash maxima-op *remembered-symbols*) (car expr))
        )
        (cons (list maxima-op 'maxima::simp)
            (mapcar #'convert-lisp-to-maxima (cdr expr))
        )
    )
)

(defun shop2::integrate-lisp-expr (expr dvar ivar initial-dval initial-ival)
    (declare (ignore dvar initial-ival))
    (list '+ initial-dval (lisp-standardize (expr-to-cl (mfuncall '$integrate (convert-lisp-to-maxima expr) ivar))))
)

(defun shop2::new-integrate-lisp-expr (expr dvar ivar initial-dval initial-ival &aux meq)
    (setq meq
        (meval* 
           `(($ic1)
                (($ode2) 
                    ((mequal) 
                        ((%derivative) ,dvar ,ivar 1) 
                        ,(convert-lisp-to-maxima expr)) 
                    ,dvar ,ivar
                )
                ((mequal) ,dvar ,initial-dval) ((mequal) ,ivar ,initial-ival)
            )
        )
    )
    
    (return-right-side
        (lisp-standardize 
            (expr-to-cl 
                (meval* `(($solve) ,meq ,dvar))
            )
        )
        dvar
    )
)

;; Takes in an equation where the left hand side is the dependent variable only
;; and returns the right hand side. Error if the input is not as described.
(defun return-right-side (expr dvar)
    (when (or (eq (car expr) 'mlist) (and (listp (car expr)) (eq (caar expr) 'mlist)))
        (setq expr (second expr))
    )
    (when (or (not (eq (first expr) 'mequal)) (not (eq (second expr) dvar)))
        (error "Bad equation not in the form y = f(x).")
    )
    (third expr)
)

(defun shop2::find-roots-lisp-expr (expr var &aux new-eqs)
    (setq new-eqs
        (utils::coerce-some-numbers 
            (cdr 
                (expr-to-cl 
                    (mfuncall '$solve (convert-lisp-to-maxima expr) var)
                )
            )
        )
    )
    (if (and new-eqs (not (eq (second (first new-eqs)) var)))
        ;then
        'shop2::fail
        ;else 
        (mapcar #'eval (mapcar #'third new-eqs))
    )
)

(defun shop2::find-roots-lisp-expressions (exprs vars)
    (cdr (expr-to-cl 
            (mfuncall '$solve 
                (cons '(maxima::mlist maxima::simp)
                    (mapcar #'convert-lisp-to-maxima exprs)) 
                (cons '(maxima::mlist maxima::simp) vars))))
)

(defun lisp-standardize (expr)
    (when (atom expr)
        (return-from lisp-standardize expr)
    )
    (when (and (eq (car expr) 'expt) (floatp (third expr)))
        (let ((round-val (round (third expr))))
            (when (< -1d-5 (- (third expr) round-val) 1d-5)
                (setf (third expr) round-val)
            )
        )
    )
    (when (and (eq (car expr) 'expt) (eql (third expr) 2))
        (let ((val (lisp-standardize (second expr))))
            (return-from lisp-standardize (list '* val val))
        )
    )
    (cons (car expr) (mapcar #'lisp-standardize (cdr expr)))
)
        

