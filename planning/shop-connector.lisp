 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: 2012
 ;;;; Project Name: ARTUE
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Portions of this file created under CARPE project with US
 ;;;; Government Funding.
 ;;;; 
 ;;;; Program Name: A Cognitive Architecture for MCM Mission Planning, Execution and Replanning 
 ;;;; Sponsor Name: Office of Naval Research
 ;;;;
 ;;;; Contractor Name: Knexus Research Corp.                         
 ;;;; Contractor Address: 9120 Beachway Lane, Springfield
 ;;;;                     Virginia, 22153                               
 ;;;;
 ;;;; Classification: UNCLASSIFIED                             
 ;;;;----------------------------------------------------------------
 ;;;; Distribution Notice:
 ;;;; SBIR DATA RIGHTS Clause
 ;;;; The Government's rights to use, modify, reproduce, release,
 ;;;; perform, display, or disclose technical data or computer 
 ;;;; software marked with this legend are restricted during the 
 ;;;; period shown as provided in paragraph (b)(4) of the Rights in 
 ;;;; Non-commercial Technical Data and Computer Software--Small 
 ;;;; Business Innovative Research (SBIR) Program clause contained in 
 ;;;; the above identified contract.  No restrictions apply after the 
 ;;;; expiration date shown above.  Any reproduction of technical 
 ;;;; data, computer software, or portions thereof marked with this
 ;;;; legend must also reproduce the markings. 
 ;;;; Expiration of SBIR Data Rights Period: 18 May 2016
 ;;;;-----------------------------------------------------------------
 ;;;; Portions of this file created under the SBIR DATA RIGHTS 
 ;;;; are:
 ;;;; All functions other than plan-for.
 ;;;; Global variables *adapt-plan*, *plan-trees*, and *last-problem*.
 ;;;; Statements in function plan-for that maintain these variables 
 ;;;; or call functions from this file other than plan-for. 
 ;;;;----------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file provides a planning interface to the SHOP2 planner that's 
 ;;;; interchangeable with other planners.
 
(in-package :artue)

(export '(_top_goal))

(defvar *adapt-plan* nil)
(defvar *plan-trees* nil)
(defvar *last-goal* nil)
(defvar *last-problem* nil)
(defvar *last-prob* nil)
(setq *plan-trees* nil)
(defvar *use-achieve-syntax* nil)

(defvar *newest-state* nil)
(defvar *last-state* nil)

(defun planner-supports-literal-goals ()
    nil
)

(defun shop-plan-for (goal believed-prob &key (time-limit-secs 300) (which :first) (depth-cutoff 2500) (domain-model *domain*))
    (when (or (not (equal *last-goal* goal))
              (not (eq *last-problem* (pddl-problem-name believed-prob)))
          )
        (setq *plan-trees* nil)
    )
    (setq *last-goal*    goal)
    (setq *last-problem* (pddl-problem-name believed-prob))
    (when (listp (first goal))
        (setq goal (first goal))
    )
    (when *use-achieve-syntax*
        (when (not (eq (first goal) 'achieve))
            (if (eq (first goal) 'and)
                ;then
                (setq goal (list (list 'achieve (rest goal))))
                ;else
                (setq goal (list (list 'achieve (list goal))))
            )
        )
    )
    (when (atom (car goal))
        (setq goal (list goal))
    )
    (when *plan-trees*
        (return-from shop-plan-for (adapt-plan believed-prob goal time-limit-secs depth-cutoff domain-model))
    )
    (let (*current-state* plans plan-run-time)
        (declare (special *current-state*))
        (setq *current-state* (export-problem-to-shop believed-prob :goal goal))
        (setq *last-prob* (copy-pddl-problem believed-prob))
        (multiple-value-setq (plans plan-run-time *plan-trees*)  
            (find-plans 
                (pddl-problem-name believed-prob)
                :domain domain-model
                :verbose :plans 
                :which which;:first
                :time-limit time-limit-secs
                :depth-cutoff depth-cutoff ;2500
                :optimize-cost nil
                :gc nil
                :plan-tree *adapt-plan*
            )
        )
        (setq plans (mapcar #'(lambda (plan) (remove-if #'numberp plan)) plans))
        (setq plans (subst goal '_top_goal plans))
        (values plans plan-run-time)
    )
)

(defun adapt-plan (believed-prob goal time-limit-secs depth-cutoff domain-model)
    (let ((cur-decomp (caar *plan-trees*))
          (*newest-state* nil)
          (*last-state* nil)
          (cost 0)
          (cutoff-time 
              (+ (get-internal-run-time) 
                 (* time-limit-secs internal-time-units-per-second)
              )
          )
          (plan nil)
         )
        
        (setq *newest-state* (export-problem-to-shop believed-prob :goal goal))
        (setq *last-state* (export-problem-to-shop *last-prob* :goal goal))
        ;; Here, *plan-trees* contains the decomposition of the last plan.
        ;; *newest-state* contains the assumed current state, 
        ;; *last-state* the assumed state at the time the last plan was generated
	;;	(create-causal-link-list (caar decomposition))
        
        (loop while (and (null plan) (< (get-internal-run-time) cutoff-time)) do
            (incf cost 5)
            (setq plan 
                (do-adaptation 
                    believed-prob
                    cur-decomp *newest-state* cost cutoff-time time-limit-secs
                    depth-cutoff domain-model
                )
            )
        )
        (break "Plan decomposition available.")
    )
)

(defun create-causal-link-list (decomposition &aux decomp-operators)
	;if car is an atom return list of input
	(setq decomp-operators (get-operators-from-decomposition decomposition))
;	(dolist (op decomp-operators) 
;	)
	
)

(defun get-effects (op &aux action bindings)
	(setq action (shop2::standardize (shop2::operator *domain* (car op))))
	(setq bindings (shop2::unify op (shop2::pddl-action-head action)))
	(apply-substitution (shop2::pddl-action-effect action) bindings)
)

(defun get-operators-from-decomposition(decomposition)
	(if (atom (car decomposition))
	;then
		(list decomposition)
	;else
		(apply #'append (mapcar #'get-operators-from-decomposition (cddr decomposition)))
	)
		
)


(defun do-adaptation (believed-prob decomp state budget cutoff-time time-limit-secs depth-cutoff domain-model &aux orig-goal)
    (when (> (get-internal-run-time) cutoff-time)
        (return-from do-adaptation nil)
    )
    (let ((traversal (create-plan-decomposition-traversal decomp))
          current-method-stack failed-method-stack 
;          (adapt-prob (copy-pddl-problem believed-prob))
          old-tree bk-tree
          remaining-traversal
          new-state
          completed
          solution
          plans plan-run-time small-plan-trees
         )
        (multiple-value-setq (remaining-traversal current-method-stack completed)
            (find-unexecuted-plan-portion traversal (get-original-state expgen *confirmed-explanation* *observed-prob*))
        )
        (break "Before execute-to")
        (if completed
            ;then
            (multiple-value-setq (failed-method-stack new-state)
                (execute-to-first-failed-task remaining-traversal 
                    (shop2::copy-state state) current-method-stack
                )
            )
            ;else
            (setq failed-method-stack current-method-stack)
        )
        (when (null failed-method-stack)
            (return-from do-adaptation
              (mapcar #'second 
                (remove :action 
                        (remove :return 
                                (create-plan-decomposition-traversal decomp)
                        )
                        :key #'first
                        :test-not #'eq
                )
              )
            )
        )
              
        (break "Before loop")
        (loop while (and (> budget 0) (< (get-internal-run-time) cutoff-time) failed-method-stack (null solution)) do
            (break "Attempting to replace method: ~A" (first failed-method-stack))
            (setq new-state
                (execute-to-method remaining-traversal 
                    (shop2::copy-state state) 
                    (first failed-method-stack)
                )
            )
            (setf (get 'adapt-prob :state) new-state)
            (setf (get 'adapt-prob :tasks) 
                (shop2::process-task-list (second (first failed-method-stack)))
            )
            (multiple-value-setq (plans plan-run-time small-plan-trees)  
                (find-plans 
                    'adapt-prob
                    :domain domain-model 
                    :verbose :plans 
                    :which :all;:first
                    :time-limit time-limit-secs
                    :depth-cutoff depth-cutoff ;2500
                    :optimize-cost nil
                    :gc nil
                    :plan-tree t
                )
            )
            (break "After partial plan")
            (when plans
                (setq old-tree (find-tree-with-head (second (first failed-method-stack)) decomp))
                (setq bk-tree (cdr (first old-tree)))
                (setf (cdar old-tree) (cdr (first (first small-plan-trees))))
                (setq solution
                    (do-adaptation 
                        believed-prob
                        decomp state 
                        (- budget (depth-of-tree bk-tree))
                        cutoff-time time-limit-secs depth-cutoff
                        domain-model
                    )
                )
                (setf (cdr old-tree) bk-tree)
            )
            (setq failed-method-stack (cdr failed-method-stack))
            (setq budget (- budget 1))
        )
        solution
    )
)

(defun execute-to-method (traversal state method-obj &aux idx)
    (setq idx (position method-obj traversal))
    (when (null idx)
        (return-from execute-to-method state)
    )
    (setq traversal (remove :method (remove :return (ntop idx traversal)) :key #'first))
    (dolist (action traversal)
        (break "~%Executing action ~A" (second action))
        (setq state (extrapolate-state *domain* state (fix-action (second action))))
    )
    state
)

(defun create-plan-decomposition-traversal (decomp)
    (if (atom (car decomp))
        ;then
        (list (list :action decomp))
        ;else
        (append 
            (list (list :method (first decomp) (second decomp) (third decomp)))
            (apply #'append (mapcar #'create-plan-decomposition-traversal (cdddr decomp)))
            (list :return)
        )
    )
)

(defun find-unexecuted-plan-portion (traversal original-state &aux (method-stack nil) remaining-history obj wait-finished remaining-time)
    (setq remaining-history (sort (copy-list (append (get-events *confirmed-explanation*) *execution-history*)) #'< :key #'occurrence-point))
    (loop while traversal do
        (setq obj (car traversal)
              traversal (cdr traversal)
        )
        (loop while (and remaining-history (not (occurrence-is-action (car remaining-history))))
              do (pop remaining-history)
        )
        (cond 
            ((null remaining-history) 
                (return-from find-unexecuted-plan-portion (values traversal method-stack t))
            )
            ((eq obj :return) (pop method-stack))
            ((eq (car obj) :method) 
                (break "Method: ~A ~A" (second obj) (third obj))
                (push obj method-stack)
                (when (not (shop-satisfied (get-method-precs (second obj) (third obj) (fourth obj)) original-state))
                    (return-from find-unexecuted-plan-portion (values traversal method-stack nil))
                )
            )
            ((and (eq (car obj) :action) 
                  (not (shop-satisfied (get-action-precs (second obj)) original-state))
             )
                (return-from find-unexecuted-plan-portion (values traversal method-stack nil))
            )
            ((and (eq (car obj) :action) (action-matches (second obj) (occurrence-signature (first remaining-history))))
                (pop remaining-history)
                (setq original-state (take-action (second obj) original-state))
            )
            ((and (eq (car obj) :action) (eq (car (second obj)) '!wait-for))
                (setq wait-finished nil)
                (loop while (not wait-finished) do 
                    (pop remaining-history)
                    (cond 
                        ((null remaining-history)
                            (setq wait-finished t)
                            (push obj traversal)
                        )
                        ((occurrence-is-action (car remaining-history))
                            (when (not (member (car (occurrence-signature (car remaining-history))) '(!wait !wait-for)))
                                (error "Next action does not match. Planned = ~A Taken = ~A" (second obj) (car remaining-history))
                            )
                        )
                        ((and (occurrence-is-event (car remaining-history))
                              (eq (car (occurrence-signature (car remaining-history)))
                                  (second (second obj))
                              )
                         )
                            (setq wait-finished t)
                        )
                    )
                )
                (setq original-state (take-action (second obj) original-state))
            )
            ((and (eq (car obj) :action) (eq (car (second obj)) '!wait))
                (setq wait-finished nil)
                (setq remaining-time (second (second obj)))
                (loop while (not wait-finished) do 
                    (pop remaining-history)
                    (cond 
                        ((null remaining-history)
                            (setq wait-finished t)
                            (push '(:action (!wait remaining-time)) traversal)
                        )
                        ((occurrence-is-action (car remaining-history))
                            (when (not (member (car (occurrence-signature (car remaining-history))) '(!wait !wait-for)))
                                (error "Next action does not match. Planned = ~A Taken = ~A" (second obj) (car remaining-history))
                            )
                        )
                        ((and (occurrence-is-event (car remaining-history))
                              (eq (car (occurrence-signature (car remaining-history)))
                                  'time-passes
                              )
                         )
                            (setq remaining-time (- remaining-time (second (occurrence-signature (car remaining-history)))))
                            (when (< -0.001 remaining-time 0.001)
                                (setq wait-finished t)
                            )
                            (when (<= remaining-time -0.001)
                                (error "Wait time exceeded.")
                            )
                        )
                    )
                )
                (setq original-state (take-action (second obj) original-state))
            )
            ((eq (car obj) :action)
                (error "Next action does not match. Planned = ~A Taken = ~A" (second obj) (car remaining-history))
            )
            (t (error "Unexpected item in traversal ~A." obj))
        )
    )
)

(defun action-matches (shop-action dh-action-sig)
    (and (safe-action-name-equals (car shop-action) (car dh-action-sig))
        (equal (cdr shop-action) (subseq dh-action-sig 1 (length shop-action)))
    )
)


(defun execute-to-first-failed-task (traversal state method-stack &aux precs)
    (dolist (obj traversal)
        (cond ((eq obj :return) (pop method-stack) (setq precs nil))
              ((eq (car obj) :method)
                  (setq precs (get-method-precs (second obj) (third obj) (fourth obj)))
                  (push obj method-stack)
              )
              ((eq (car obj) :action)
                  (setq precs (get-action-precs (second obj)))
              )
        )
        (when (not (shop-satisfied precs state))
            (return-from execute-to-first-failed-task (values method-stack state))
        )
        (when (and (listp obj) (eq (car obj) :action))
            (setq state (take-action (second obj) state))
        )
    )
)

(defun take-action (action state)
    (break "~%Executing action ~A" action)
    (setq state (extrapolate-state *domain* state (fix-action action)))
)    

(defun get-action-precs (action-head)
    (when (eq (car action-head) '!wait-for)
        (return-from get-action-precs nil)
    )
    (let ((action (shop2::standardize (shop2::operator *domain* (car action-head)))))
        (shop2::apply-substitution
            (shop2::pddl-action-precondition action)
            (shop2::unify action-head (shop2::pddl-action-head action))
        )
    )
)

(defun get-method-precs (method-head method-label unifier &aux methods m-tail)
    (setq methods (shop2::methods *domain* (car method-head)))
    (dolist (m methods)
        (setq m-tail (member method-label m))
        (when m-tail
            (return-from get-method-precs
                (shop2::standardize
                    (shop2::apply-substitution
                        (second m-tail)
                        (destandardize unifier (shop2::extract-variables (second m-tail)))
                    )
                )
            )
        )
    )
)

(defun destandardize (unifier variables &aux sub-list cur-var standard-vars)
    (setq variables (reverse (lexicographic-sort variables)))
    (setq standard-vars (mapcar #'shop2.unifier::binding-var unifier))
    (setq sub-list nil)
    (dolist (var variables)
        (setq cur-var nil)
        (loop for standard-var in standard-vars while (null cur-var) do
            (when (starts-with (symbol-name var)  (symbol-name standard-var))
                (setq cur-var standard-var)
            )
        )
        (setq standard-vars (remove cur-var standard-vars))
        (push (cons cur-var var) sub-list)
    )
    (replace-vars sub-list unifier)
)

(defun replace-vars (sub-list unifier &aux (new-unifier nil))
    (dolist (b unifier)
        (push 
            (shop2.unifier::make-binding
                (sublis sub-list (shop2.unifier::binding-var b))
                (sublis sub-list (shop2.unifier::binding-val b))
            )
            new-unifier
        )
    )
    new-unifier
)
        
    

(defun shop-satisfied (precs state)
    (shopthpr:find-satisfiers precs state t)
)

(defun find-tree-at (method-queue tree)
    (when (null method-queue)
        (return-from find-tree-at tree)
    )
    (find-tree-at (cdr method-queue) (fourth (assoc (car method-queue) tree)))
)


(defun find-tree-with-head (method-sig tree)
    (find-in-tree tree 
        #'(lambda (subtree) (and (listp subtree) (eq method-sig (car subtree))))
    )
)

(defun depth-of-tree (tree)
    (cond
        ((null tree) 1)
        ((listp tree)
            (1+ (apply #'max (mapcar #'depth-of-tree tree)))
        )
        (t 1)
    )
)

