 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: 2017
 ;;;; Project Name: ARTUE
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file defines a standard ARTUE agent (also, in DiscoverHistory 
 ;;;; papers, "DHAgent"), and is responsible for observation, decision 
 ;;;; making, and measurement.



(in-package :artue)

(defclass artue-agent (agent)
  (
    ;; prob = problem
    (believed-prob
        :initform nil
        :accessor get-believed-prob
        :initarg  :believed-prob
    )
    
    (plan
        :initform nil
        :accessor get-plan
        :initarg  :plan
    )
    (goal
        :initform nil
        :accessor get-goal
        :initarg  :goal
    )
    (last-goal
        :accessor get-last-goal
    )
    
    (ensure-unobservable-goals
        :initform nil
        :accessor get-ensure-unobservable-goals
        :initarg  :ensure-unobservable-goals
    )
    (possible-wins
        :initform nil
        :accessor get-possible-wins
    )
    
    (current-explanations
        :initform nil
        :accessor get-current-explanations
        :initarg  :current-explanations
    ) 
    (last-explanations
        :initform nil
        :accessor get-last-explanations
        :initarg  :last-explanations
    )
    
    (planning-time-limit
        :initform 60
        :accessor get-planning-time-limit
        :initarg  :planning-time-limit
    )
    
    (start-time
        :initform nil
        :accessor get-start-time
    )
    
    (explanation-time
        :initform 0
        :accessor get-explanation-time
        :initarg  :explanation-time
    )
    (explanation-count
        :initform 0
        :accessor get-explanation-count
        :initarg  :explanation-count
    )
    (explanation-changes
        :initform 0
        :accessor get-explanation-changes
        :initarg  :explanation-changes
    )
    (explanation-failures
        :initform 0
        :accessor get-explanation-failures
        :initarg  :explanation-failures
    )
    (explanations-cutoff
        :initform 0
        :accessor get-explanations-cutoff
        :initarg  :explanations-cutoff
    )
    (explanation-depth-total
        :initform 0
        :accessor get-explanation-depth-total
        :initarg  :explanation-depth-total
    )
    (explanation-depth-max
        :initform 0
        :accessor get-explanation-depth-max
        :initarg  :explanation-depth-max
    )
    (explanation-assumptions-change-total
        :initform 0
        :accessor get-explanation-assumptions-change-total
        :initarg  :explanation-assumptions-change-total
    )
    (explanation-assumptions-change-max
        :initform 0
        :accessor get-explanation-assumptions-change-max
        :initarg  :explanation-assumptions-change-max
    )
    (explanation-final-assumption-count
        :initform 0
        :accessor get-explanation-final-assumption-count
        :initarg  :explanation-final-assumption-count
    )
    
    (actions-executed
        :initform 0
        :accessor get-actions-executed
        :initarg  :actions-executed
    )
    
    (learned-event-models
        :initform nil
        :accessor get-learned-event-models
        :initarg  :learned-event-models
    )
    
    (domain-model
        :initform nil
        :reader get-domain-model
        :initarg  :domain-model
    )

    (performed-actions
        :initform nil
        :accessor get-actions
        :initarg  :actions
    )
    (executing-actions ;;Actions removed from the plan but whose consequences
                       ;;are not yet observed.
        :initform nil
        :accessor get-executing-actions
    )
    
    
    (observability
        :initform :partial
        :accessor get-observability
        :initarg  :observability
    )

    (do-explain
        :initform nil
        :accessor get-do-explain
        :initarg  :do-explain
    )
    
    (expgen 
        :initform nil
        :reader   get-expgen
        :initarg  :expgen
    )
    
    (total-planning-time
        :initform 0
        :accessor get-total-planning-time
    )
    
    (planning-calls
        :initform 0
        :accessor get-planning-calls
    )
    
    (max-actions-expected 
        :initform -1
        :reader get-max-actions-expected
        :initarg :max-actions-expected
    )
    
    (record-logs
        :initform t
        :accessor get-record-logs
        :initarg  :record-logs
    )
    
    (time-step-length
        :initform -1
        :accessor get-time-step-length
        :initarg  :time-step-length
    )
    
    (start-with-noop
        :initform nil
        :reader   get-start-with-noop
        :initarg  :start-with-noop
    )
   
    (learning-allowed 
        :reader  _is-learning-allowed
        :initarg :learning-allowed
    )
    (observation-pre-processing-fn
        :initform nil
        :reader  get-observation-pre-processing-fn
        :initarg :observation-pre-processing-fn
    )
    (belief-post-processing-fn
        :initform nil
        :reader  get-belief-post-processing-fn
        :initarg :belief-post-processing-fn
    )
    (last-estimated-wait
        :initform -1
        :accessor get-last-estimated-wait
    )
    (motivations-file
        :initform nil
        :initarg :motivations-file
        :reader get-motivations-file
    )
    (motivation-rules
        :accessor get-motivation-rules
    )
    (motivations
        :accessor get-motivations
    )
    (observation-count
        :initform 0
        :accessor get-observation-count
    )
    (true-ex
        :initform nil
        :accessor get-true-ex
    )
    (explanation-results-file
        :initform nil
        :initarg :explanation-results-file
        :accessor get-explanation-results-file
    )
    (experiment-condition
        :initform nil
        :initarg :experiment-condition
        :accessor get-experiment-condition
    )
    (action-prediction-fn
        :initform nil
        :initarg :action-prediction-fn
        :accessor get-action-prediction-fn
    )
    (predicted-next-actions
        :initform nil
        :initarg :predicted-next-actions
        :accessor get-predicted-next-actions
    )
    (diagnose 
        :initform nil
        :initarg :diagnose
        :reader get-diagnose
    )
    (diagnosing 
        :initform nil
        :accessor is-diagnosing
    )
    (learner
        :initform (make-instance 'foil-learner)
        :accessor get-learner
    )
    (stop-on-failed-explanation
        :initarg :stop-on-failed-explanation
        :initform t
        :reader is-stop-on-fail
    )
    (goal-change-hook-fn
        :initarg :goal-change-hook-fn
        :initform #'null
        :reader get-goal-change-hook-fn
    )
  )
)

;; This is the default method for constructing an agent in the code base, and 
;; it delegates to create-artue-agent. Designed to be overridden to avoid 
;; default behavior.
(defun create-agent (agent-name domain-name &rest agent-keys)
    (apply #'create-artue-agent agent-name domain-name agent-keys) 
)

;; Create-artue-agent should be called with initialization parameters to 
;; construct an ARTUE agent object that will act in an environment in an
;; experiment. 
(defun create-artue-agent (agent-name domain-name &rest agent-keys)
    (agent-identity-layer-initialize 
        (apply #'artue-agent-initialize domain-name 
            agent-keys
        )
        agent-name
    )
)

;; Called to initialize an agent's experimental parameters at the outset of an 
;; experiment. Should not be called after each trial.
(defun artue-agent-initialize (domain-name
                               &rest agent-keys
                               &aux ag)
    (when (null (getf agent-keys :expgen))
        (push
            (apply #'discoverhistory-explanation-generator-initialize
                domain-name
                :learning-allowed (getf agent-keys :learning-allowed)
                nil
            )
            agent-keys
        )
        (push :expgen agent-keys)
    )
    
    (setq ag 
        (apply #'make-instance 'artue-agent 
            :domain-model (get domain-name :domain)
            agent-keys
        )
    )
    
    (when (get-learning-allowed (get-expgen ag))
        (use-updated-knowledge (make-instance 'foil-learner) (get-expgen ag)) ;; In full-explainer/foil-ps-interface
    )
    
    (when (or (pathnamep (get-motivations-file ag)) (stringp (get-motivations-file ag))) 
        (load (get-motivations-file ag))
        (setf (get-motivations ag) *motivations-dt*)
        (setf (get-motivation-rules ag) *dt-rules*)
    )
    ag
)

;; This method is called to give the agent its first observation; the agent 
;; sets up its explainer, beliefs, and plan to initial values based on the 
;; observation.
(defmethod agent-initial-observation ((ag artue-agent) prob)
    (setf (get-start-time ag) (get-internal-run-time))
    (with-slots (believed-prob goal expgen plan) ag
        (expgen-reset expgen)
        
        ;; This call gives the initial problem to the agent and 
        ;; cause it to be processed. Prior functionality also called
        ;; for explanation, but I think this is unnecessary for initial 
        ;; observation.
        (expgen-advance expgen prob nil :do-record (get-record-logs ag))

        (setf believed-prob (copy-pddl-problem prob))
        (when (null goal)
            (setf goal (pddl-problem-goal prob))
        )
        (when (listp (first goal))
            (setf goal (first goal))
        )
        (setf (pddl-problem-name believed-prob) 'beliefs)
        (setf (pddl-problem-goal believed-prob) goal)
        
        (when (and (get-ensure-unobservable-goals ag) (eq 'and (car goal)))
            (dolist (subgoal (weaken-goal goal))
                (push (cons subgoal (model-goal-success expgen (cons 'and subgoal))) ;;syncs with full-explainer
                    (get-possible-wins ag)
                )
            )
        )
        
        ;; This updates the believed-prob with given hidden facts
        (when (get-plausible-explanations expgen)
            (add-hidden-facts expgen (car (get-plausible-explanations expgen)) believed-prob)
        )

        (cond 
            ((get-start-with-noop ag)
                (setq plan '((noop) (!replan)))
            ) 
            ((eq (get-observability ag) :sensing) 
                (setq plan '((_init) (!replan)))
            )
            (t
                (format t "~&Constructing initial plan.")
                (agent-construct-plan ag)
            )
        )
    )
)

;; This method produces a plan to achieve a goal. By default, this goal is the 
;; agent's current goal, and the plan starts from a state corresponding to the 
;; current state that the agent believes in, but either can be replaced using 
;; parameters. Default behavior allows the agent to try to achieve a single  
;; non-trivial accomplishable goal from a set of goals if a plan cannot be found
;; that will accomplish all of them; this can be overridden by passing in 
;; the parameters '(:partial-satisfaction nil). 
(defmethod agent-construct-plan ((ag artue-agent) 
                                 &key (ext-goal nil) (problem nil) 
                                      (partial-satisfaction t)
                                 &aux goal)
    (setf (get-executing-actions ag) (remove '!wait (get-executing-actions ag) :key #'car))
    (when (and (get-diagnose ag) (cdr (get-plausible-explanations (get-expgen ag))))
        (return-from agent-construct-plan (agent-construct-diagnostic-plan ag))
    )
    (when (is-diagnosing ag)
        (setf (is-diagnosing ag) nil)
        (learn-from-explanation (get-learner ag) (get-expgen ag) (car (get-plausible-explanations (get-expgen ag))))
    )
    
    (with-slots (plan believed-prob) ag
        (when (null problem) (setq problem believed-prob))
        (if (null ext-goal)
            ;then
            (setq goal (get-goal ag))
            ;else
            (setq goal ext-goal)
        )
        (setq plan (agent-call-planner ag goal))
        (when plan 
            (setq plan (car plan))
            (return-from agent-construct-plan plan)
        )
        ;; Try for achievement of a subgoal (one conjunct) if possible.
        ;; But don't bother if there's only one conjunct.
        (when (and partial-satisfaction (cdr (weaken-goal goal)))
            (let ((subgoals (weaken-goal goal)))
                (loop while (and subgoals (null plan)) do
                    (setq plan (car (agent-call-planner ag (pop subgoals))))
                )
            )
        )
        ;; In case of partial satisfaction, this ensures that the agent recalls 
        ;; planning for the overall goal rather than a partial goal
        (setf (get-last-goal ag) goal)
        plan
    )
)

;; This function is called on a top-level goal and returns a list of partial 
;; satisfaction goals. This function has a trivial implementation due to the 
;; assumption that all composite goals will be of the form 
;; (achieve goal1 goal2... goaln) or (and goal1 goal2... goaln). More complex 
;; goal structures will require re-implementation.
(defun weaken-goal (goal)
    (mapcar #'list 
        (rest goal)
    )
)



;; Allows a diagnostic version of ARTUE (unpublished as of 12/6/2017) to 
;; construct a plan that, instead of accomplishing a goal, verifies or 
;; eliminates one or more consistent explanations.
(defmethod agent-construct-diagnostic-plan ((ag artue-agent))
    (setf (is-diagnosing ag) t)
    (with-slots (plan expgen) ag
        (setq plan (mapcar+ #'get-lisp-action (construct-diagnostic-plan expgen) (get-expgen ag)))
    )
)

;; This method is called to determine whether a simulation should continue; the 
;; agent returns true when it has no interesting plan actions left to signal 
;; that it believes it has accomplished all goals it can.
(defmethod agent-not-finished ((ag artue-agent))
    (or (not (null (remove-internal-actions (get-plan ag))))
        (not (null (get-executing-actions ag)))
    )
)

;; This method is where most of the agent work gets done. This is called to
;; respond to every observation, and informs the agent of which of its actions 
;; have finished executing. Each call is responsible for one iteration of 
;; the GDA loop, with discrepancy detection, explanation, goal formulation and 
;; management, and replanning when necessary.
(defmethod agent-observe ((ag artue-agent) actions observable-prob 
                          &aux assumptions expgen)
    (check-off-actions ag actions)
    (incf (get-observation-count ag))
    (setq expgen (get-expgen ag))
    (when (get-observation-pre-processing-fn ag)
        (setq observable-prob (funcall (get-observation-pre-processing-fn ag) observable-prob))
    )
    
    ;; In the partial observability case (we normally evaluate ARTUE this way), 
    ;; we always maintain an explanation of the world; this explanation may or 
    ;; may not be updated by calling DiscoverHistory, but it always reflects 
    ;; actions taken by the agent. Projections off of this explanation are used 
    ;; to create new beliefs for the agent.
    (when (not (eq (get-observability ag) :full))
        (let ((result (obtain-explanations ag actions observable-prob)))
            (when (and (eq result :fail) (is-stop-on-fail ag))
                (return-from agent-observe :fail)
            )
        )
        (if (get-plausible-explanations expgen)
            ;then
            (setf (get-believed-prob ag) 
                (make-explanation-based-problem 
                    expgen 
                    (car (get-plausible-explanations expgen)) 
                    observable-prob
                )
            )
            ;else
            (setf (get-believed-prob ag) 
                (make-explanation-based-problem 
                    expgen 
                    (get-null-explanation expgen)
                    observable-prob
                )
            )
        )
    )
    (when (eq (get-observability ag) :full)
        (setf (get-believed-prob ag) (copy-pddl-problem observable-prob))
    )
    (when (get-belief-post-processing-fn ag)
        (setf (get-believed-prob ag) (funcall (get-belief-post-processing-fn ag) observable-prob (get-believed-prob ag) (get-plausible-explanations expgen)))
    )
  
    
    ;; Only used when learning models. This ensures that the agent eventually 
    ;; leaves a (presumed infinite) action execution loop that can be caused by 
    ;; repeated replanning and incorrect learned knowledge. Learned knowledge is 
    ;; abandoned to prevent this loop from recurring.
    (when (and (< 0 (get-max-actions-expected ag) (get-actions-executed ag)) (get-learned-event-models ag))
        (when (null (get-current-explanations ag))
            (setf (get-current-explanations ag) (get-plausible-explanations expgen))
        )
        (setf (get-current-explanations ag)
            (list (abandon-learned-event-knowledge (car (get-current-explanations ag)) (get-domain-model ag)))
        )
    )
    ;; explanations is nil when no change needs to be made; next block handles 
    ;; when explanations are generated
    (when (and (get-do-explain ag) (get-current-explanations ag))
        (setf (get-last-explanations ag) (get-current-explanations ag))
        (when (get-last-search-was-cutoff (get-expgen ag))
            (incf (get-explanations-cutoff ag))
        )
        (incf (get-explanation-changes ag))

        (extend-explanation (get-expgen ag) (car (get-current-explanations ag)) nil)
        (push '(explain) *performed-actions*)
        (setq assumptions (get-assumption-facts (car (get-current-explanations ag))))
        
        (maintain-explanation-stats ag (get-current-explanations ag) assumptions)
        (format t "~&Explanation has changed, replanning.")
        (agent-construct-plan ag :ext-goal (determine-goal ag (get-believed-prob ag)))
    )
    
    ;; Runs goal formulation and management, replans.
    (let ((new-goal (determine-goal ag (get-believed-prob ag))))
        (when (not (equal new-goal (get-last-goal ag)))
            (funcall (get-goal-change-hook-fn ag) new-goal)
            (setf (get-last-goal ag) new-goal)
            (format t "~&Goal has changed, replanning.")
            (agent-construct-plan ag :ext-goal new-goal)
        )
    )
    
    ;; Checks that plan is still satisfiable.
    (when (not (is-plan-satisfiable ag (get-believed-prob ag)))
        (format t "~&Former plan can no longer be executed, replanning.")
        (agent-construct-plan ag :ext-goal (get-last-goal ag))
    )
        
    
    (with-slots (plan goal) ag
        ;; When no plan has been found, try replanning.
        (when (and (null plan) (null (get-executing-actions ag)))
            (push '(run-out-of-actions-replan) *performed-actions*)
            (format t "~&No actions remaining, replanning.")
            (agent-construct-plan ag :ext-goal (get-last-goal ag))
        )
        
        ;; Execute a replan when plan calls for it.
        (loop while (and (eq (caar (get-executing-actions ag)) '!replan)) do
            (let ((replan-goal (second (first (get-executing-actions ag)))))
                (pop (get-executing-actions ag))
                (when (null replan-goal)
                    (setq replan-goal (get-last-goal ag))
                )
                (format t "~&Executing !replan action.")
                (agent-construct-plan ag :ext-goal replan-goal)
            )
        )
        
        ;; Saves explanations for later recovery.
        (when (and (null plan) (get-current-explanations ag))
            (setf (get-last-explanations ag) (get-current-explanations ag))
        )
        
        ;; Whenever literal hidden goals are in use, this helps to reason about 
        ;; whether the goal may have not been achieved.
        (when (and (null plan) (get-possible-wins ag))
            (plan-for-extra-win-conditions ag observable-prob)
        )
    )
)

;; This method determines which goal to pursue now, using motivation knowledge.
;; Prints all goals found. In GDA framework, this can be considered as goal 
;; formulation + goal management. Only one goal (the highest priority goal) is 
;; pursued at a time.
(defun determine-goal (ag observable-prob &aux resolver-state goals)
    (when (null (get-motivations-file ag))
        (return-from determine-goal (get-goal ag))
    )
    (setq resolver-state (build-motivation-state observable-prob))
    (setq goals 
        (mapcar #'first
            (sort
                (generate-goals 
                    resolver-state (get-motivations ag) (get-motivation-rules ag) 
                )
                #'(lambda (x y) 
                    (cond
                        ((eql (funcall (second x) nil) (funcall (second y) nil))
                            (string-lessp 
                                (write-to-string (first x))
                                (write-to-string (first y))
                            )
                        )
                        (t (> (funcall (second x) nil) (funcall (second y) nil)))
                    )
                  )
                            
            )
        )
    )
    (format t "~%~%Goals:")
    (print-one-per-line goals)
    ;(break "Inspect goals.")
    (if (null goals)
        ;then
        (get-goal ag)
        ;else
        (car goals)
        ;(cons 'and goals)
    )
)

;; This helper function is responsible for assembling a list of facts that are 
;; currently true to drive motivation rules.
(defun build-motivation-state (observable-prob)
    (append 
        (mapcar #'(lambda (fct) (list 'is fct t))
            (pddl-problem-init observable-prob)
        )
        (mapcar #'(lambda (fct) (cons 'is fct))
            (pddl-problem-values observable-prob)
        )
        (get-isa-facts observable-prob)
    )
)

;; Check to make sure plan is still possible. This should always return true 
;; when explanation is in use, as replanning is triggered by the revised 
;; explanation. When explanation is not in use, replanning can be triggered by 
;; a satisfiability failure.
(defun is-plan-satisfiable (ag observable-prob &aux predicted-state)
    (setq predicted-state (export-problem-to-shop observable-prob))
    (dolist (action (get-plan ag))
        (setq predicted-state  
            (extrapolate-state (get-domain-model ag) predicted-state (fix-action action))
        )
        (when (eq predicted-state 'fail)
            (return-from is-plan-satisfiable nil)
        )
    )
    t
)

;; This helper method is responsible for tracking what actions are currently 
;; executing in asynchronous environments with durative actions. In particular, 
;; it tracks what waits should still be going on so that planned actions after 
;; waits do not occur early.
(defun check-off-actions (ag actions &aux exec-acts)
    (setq exec-acts (get-executing-actions ag))
    (dolist (action actions)
        (cond 
            ((equalp action (first exec-acts))
                (setq exec-acts (cdr exec-acts))
                (push action (get-actions ag))
            )
            ((and (eq (car action) '!wait) (eq (caar exec-acts) '!wait))
                (if (> (second action) (second (first exec-acts)))
                    ;then
                    (setq exec-acts (cdr exec-acts))
                    ;else
                    (setq exec-acts 
                        (cons 
                            `(!wait ,(- (second (first exec-acts)) (second action)))
                            (cdr exec-acts)
                        )
                    )
                )
                (when (> (second action) 0)
                    (push action (get-actions ag))
                )
            )
            ((and (eq (car action) '!wait) (eq (caar exec-acts) '!wait-for) (null (cdar exec-acts)))
                (push action (get-actions ag))
                (pop exec-acts)
            )
            ((and (eq (car action) '!wait) (eq (caar exec-acts) '!wait-for) (> (get-last-estimated-wait ag) 0))
                (when (> (second action) 0)
                    (push action (get-actions ag))
                )
                (when (>= (second action) (- (get-last-estimated-wait ag) .0001))
                    (pop exec-acts)
                )
            )
            ; ((and (eq (car action) '!wait) (null (get-plan ag)))
                ; ;;Safe to wait after plan end.
                ; (push action (get-actions ag))
            ; )
            ; ((and (eq (car action) '!wait) (numberp (second action)) (< (second action) .01))
                ; ;;Safe to wait for short amounts of time (reaction time).
                ; (push action (get-actions ag))
            ; )
            ((and (eq (car action) '!wait))
                (push action (get-actions ag))
            )
            (t (error "Action does not match what's supposed to be executing."))
        )
    )
    (setf (get-executing-actions ag) exec-acts)
)

;; This method is called to update an ARTUE agent's explanation of the world 
;; (and indirectly, its beliefs). When the "do-explain" field of the agent is 
;; t, DiscoverHistory will search for one or more consistent explanations.
;; Otherwise, explanations will be maintained only by adding actions and 
;; predictable events. If the current explanation is inconsistent, it will be 
;; reset to match the current observation, but will not infer causes for 
;; discrepancies.
(defmethod obtain-explanations ((ag artue-agent) actions observed-prob 
                                &aux (init-time (get-internal-run-time)) possible-actions)
    (expgen-advance (get-expgen ag) observed-prob actions :do-explain (get-do-explain ag) :do-record (get-record-logs ag))
    (when (get-record-logs ag)
        (log-problems 
            (get-expgen ag) 
            (list (first (get-actions ag))) 
            observed-prob
        )
    )
    (setq possible-actions (get-predicted-next-actions ag))
    (multiple-value-bind (explanations extra-result)  
            (expgen-explain (get-expgen ag) :do-explain (get-do-explain ag) :possible-actions possible-actions)
        (setf (get-current-explanations ag) explanations)
        (when (eq extra-result :fail)
            (return-from obtain-explanations :fail)
        )
    )
    (incf (get-explanation-count ag))
    (incf (get-explanation-time ag) 
        (+ 0.0 
           (/ (- (get-internal-run-time) init-time) 
              internal-time-units-per-second
           )
        )
    )
    (get-current-explanations ag)
)

;; When a goal cannot be observed, it is sometimes prudent to consider whether 
;; the agent's belief that it has been achieved could be false. This function 
;; attempts to consider the possibility that one or more goals have not been 
;; achieved, and make a plan that ensures they are achieved in possible worlds
;; where they have not. Note: This assumes that goals are absorbing states; if 
;; goals can be undone in one possible world by accomplishing them in another, 
;; it could cause thrashing. In the presence of such goals, be sure to turn off 
;; this behavior by seeting the "ensure-unobservable-goals" property of the 
;; agent to nil.
(defun plan-for-extra-win-conditions (ag observable-prob 
                                      &aux leftover-fails temp-expgen temp-exes 
                                           result possible-hiddens cur-prob)
    ;; This checks whether the winning facts known for the current domain 
    ;; (created by calling model-goal-success) are unambiguously known based on the 
    ;; execution history, as determined by the planning layer logic found in 
    ;; DiscoverHistory's event-enumeration.lisp file.
    (when (null (get-prior-observation (get-expgen ag)))
        (return-from plan-for-extra-win-conditions nil)
    )
    (setq possible-hiddens (get-last-hiddens (get-expgen ag)))
    (setq leftover-fails nil)
    (dolist (possible-win (get-possible-wins ag))
        (when (or (not (member (cdr possible-win) possible-hiddens :test #'equal-fact))
                  (member (contradictory-fact (cdr possible-win)) possible-hiddens :test #'equal-fact)
              )
            (push possible-win leftover-fails)
        )
    )
            
    (when (not (get-do-explain ag))
        (return-from plan-for-extra-win-conditions nil)
    )
    (dolist (fail-condition leftover-fails) 
        (setq temp-expgen (copy-full-generator (get-expgen ag)))
        (expect-no-win temp-expgen (contradictory-fact (cdr fail-condition)))
        (setf (get-plausible-explanations temp-expgen) 
            (mapcar #'copy-explanation (get-plausible-explanations (get-expgen ag))))
        (multiple-value-setq (temp-exes result) (expgen-explain temp-expgen))
        (when (and temp-exes (not (eq result :fail)))
            (when (get-record-logs ag)
                (log-problems 
                    (get-expgen ag) 
                    (list (first (get-actions ag))) 
                    observable-prob
                )
            )
            (setq cur-prob 
                (make-explanation-based-problem 
                    (get-expgen ag) (car temp-exes) observable-prob
                )
            )
            (setf (get-plan ag) 
                (car 
                    (agent-call-planner ag (car fail-condition)
                        :problem cur-prob 
                        :exes temp-exes
                    )
                )
            )
            (when (get-plan ag)
                (setf (get-current-explanations ag) temp-exes)
                (setf (get-plausible-explanations (get-expgen ag)) temp-exes)
                (setf (get-believed-prob ag) cur-prob)
                (return-from plan-for-extra-win-conditions t)
            )
        )
    )
)

;; This function is called after explanation generation and maintains statistics 
;; used to understand and report on the behavior of an explanation generator.
(defun maintain-explanation-stats (ag explanations assumptions)
    (if (explanation-is-trivial (get-expgen ag) (car explanations))
        ;then
        (progn
            (incf (get-explanation-failures ag))
            (push (cons 'reset-assumptions assumptions)
                *performed-actions*
            )
        )
        ;else
        (progn
            (incf (get-explanation-depth-total ag) (get-last-search-depth (get-expgen ag)))
            (setf (get-explanation-depth-max ag) 
                (max (get-explanation-depth-max ag) (get-last-search-depth (get-expgen ag)))
            )
            (let ((assump-count-change (- (length assumptions) (get-explanation-assumptions-change-total ag))))
                (incf (get-explanation-assumptions-change-total ag)
                    assump-count-change
                )
                (setf (get-explanation-assumptions-change-max ag)
                    (max (get-explanation-assumptions-change-max ag)
                        assump-count-change
                    )
                )
            )
            (push (cons 'adopt-assumptions assumptions)
                *performed-actions*
            )
        )
    )
    (setf (get-explanation-final-assumption-count ag) (length assumptions))
)    


;; This function is provided to allow an experimenter to request an action 
;; from the agent. ARTUE replans during the agent-observe function, so this 
;; function only needs to pull the next action from the start of the plan. 
;; Non-standard behavior implements "if" actions, and waits if no actions are in 
;; the current plan. Discrete time agents are generally finished when they have 
;; no actions left to execute, so will not need to respond with an action after 
;; the plan is exhausted.
(defmethod agent-get-next-action ((ag artue-agent) &aux action)
    (when (get-action-prediction-fn ag)
        (setf (get-predicted-next-actions ag) (funcall (get-action-prediction-fn ag)))
    )
    (with-slots (plan) ag
        (setf (get-last-estimated-wait ag) -1)
        (setq action (car plan))
        (when (null plan)
            (setq action `(!wait ,shop2::*large-time-threshold*))
        )
        (when (eq (car action) 'if)
            (when (not (eq (car (second action)) 'confirm))
                (error "Expected if condition to be a confirm.")
            )
            (if (member (cdr (second action)) (pddl-problem-init (get-believed-prob ag)) :test #'equal)
                ;then
                (setq plan (append (third action) (cdr plan)))
                ;else
                (setq plan (append (fourth action) (cdr plan)))
            )
            (setq action (car plan))
        )
        (when (eq (car action) '!wait-for)
            (let (time-required)
                (setq time-required 
                    (apply #'shop2::pass-time-until 
                        (export-problem-to-shop (get-believed-prob ag))
                        (get-domain-model ag)
                        (if (cdr action) (cdr action) (list nil))
                    )
                )
                (when (< time-required 0)
                    (setq time-required shop2::*large-time-threshold*)
                )
                (setq time-required (max time-required .0001)) ;; This is necessary because agent can 
                                                               ;; incorrectly prevent an immediate event
                (setf (get-last-estimated-wait ag) time-required)
                (setq action (list '!wait time-required))
            )
        )
        action
    )
)

;; This function is provided to allow an experimenter to request actions 
;; from the agent in an asynchronous environment. In this case, the agent
;; provides all actions that it wishes to execute immediately, rather than a 
;; single action.
(defmethod agent-get-actions-to-execute ((ag artue-agent) &aux action act-list)
    (loop while (not (member (car action) '(!wait !replan))) do
        (setq action (agent-get-next-action ag))
        (push action act-list)
        (agent-start-action-executing ag action)
    )
    (when (eq (car action) '!replan)
        (pop act-list)
    )
    (reverse act-list)
)


;; This function informs the agent when an action from its plan starts to 
;; execute.
(defmethod agent-start-action-executing ((ag artue-agent) action)
    (setf (get-executing-actions ag) 
        (append (get-executing-actions ag) (list action))
    )
    (when (equalp (car (get-plan ag)) action) (pop (get-plan ag)))
)

;; This method is responsible for calling the planner and updating related 
;; state. It updates the agent's memory of what it has planned for, handles 
;; planning failure conditions, maintains statistics, and logs problems for 
;; later reproduction.
(defun agent-call-planner (ag goal &key (problem nil) (exes nil))
    (setf (get-last-goal ag) goal)
    (format t "~%~%~%~%Planning for goal: ~A." goal)
    (when (null problem) (setq problem (get-believed-prob ag)))
    (when (null exes) (setq exes (get-current-explanations ag)))
    (log-planning-problem 
        problem
        (domain-name (get-domain-model ag)) 
        (domain-file (get-domain-model ag)) 
        (get-time-step-length ag)
        :goal goal
        :id (second (assoc '(self) (pddl-problem-values problem) :test #'equal))
        :seq-number (get-observation-count ag)
    )
    (multiple-value-bind (plans plan-run-time)  
        (catch :projection-fail
            (plan-for
                goal
                problem
                :time-limit-secs (get-planning-time-limit ag)
                :exes exes
                :domain-model (get-domain-model ag)
                :expgen (get-expgen ag)
            )
        )
        (when (and (get-learning-allowed (get-expgen ag)) (eq plans :event-cycle))
            (abandon-learned-event-knowledge (car exes) (get-domain-model ag))
            (return-from agent-call-planner 
                (agent-call-planner ag goal)
            )
        )
        (when (eq plans :event-cycle)
            (error "Event cycle detected.")
        )
        (incf (get-total-planning-time ag) plan-run-time)
        (incf (get-planning-calls ag))
        ;(mapcar #'remove-internal-actions plans)
        plans
    )
)

;; In order to adapt ARTUE to work in contingent planning domains with sensing 
;; actions, we spam the sensing actions. This determines what sensing actions 
;; the agent will execute.
(defmethod get-sensing-actions ((ag artue-agent) &aux sensing-actions)
    (when (not (eq (get-observability ag) :sensing))
        (return-from get-sensing-actions nil)
    )
    (get-sensing-actions-from-model (get-expgen ag) (get-last-hiddens (get-expgen ag)))
)

;; Reporting methods
(defmethod get-learning-allowed ((ag artue-agent))
    (get-learning-allowed (get-expgen ag))
)

(defgeneric get-total-run-time (ag))
(defmethod get-total-run-time ((ag artue-agent))
    (- (get-internal-run-time) (get-start-time ag))
)


(defmethod agent-executed-action-count ((ag artue-agent))
    (get-actions-executed ag)
)


