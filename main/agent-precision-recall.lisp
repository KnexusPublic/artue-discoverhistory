 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: 2017
 ;;;; Project Name: ARTUE
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file includes functions necessary to compute the precision, recall, 
 ;;;; and other accuracy values of an agent's explanation through comparison 
 ;;;; of agent fields to ground truth.

;; This method is a backchannel used to inform the agent of the ground truth 
;; initial state of a partially observable environment. This is to be used only  
;; for measuring the difference between an agent's beliefs and reality (eg, for 
;; calculating precision and recall). If an agent makes any decisions based on 
;; this information, it invalidates partial observability experiments.
(defmethod initialize-ground-truth ((ag artue-agent) ground-truth-state)
    (when (get-explanation-results-file ag)
        (with-open-file (str (get-explanation-results-file ag)
                         :direction :output 
                         :if-does-not-exist :create 
                         :if-exists :append)
            (format str "~%domain, actions, time, duration, simulated-seconds, FN, FP, TP, events, TP-ratio, f-measure, explanation count")
        )
        (setf (get-true-ex ag) (make-explanation))
        (dolist (fact (pddl-problem-init ground-truth-state))
            (add-initial-ground-truth-if-hidden ag (make-fact :type (car fact) :args (cdr fact) :value t))
        )
        (dolist (fact (pddl-problem-values ground-truth-state))
            (add-initial-ground-truth-if-hidden ag (make-fact :type (caar fact) :args (cdar fact) :value (second fact)))
        )
    )
)

(defun add-initial-ground-truth-if-hidden (ag fact &aux temp-ex)
    (when (and (is-hidden (get-expgen ag) fact) (is-legal-assumption (get-expgen ag) (fact-to-cnd fact)))
        (setq temp-ex
            (car 
                (refine-by-hypothesizing-initial-value (get-expgen ag) (get-true-ex ag)
                    (make-inconsistency
                        :cnd (fact-to-cnd fact)
                        :prior-occ nil
                        :next-occ nil
                    )
                )
            )
        )
        (when (null temp-ex)
            (break "Failed hypothesis.")
        )
        (setf (get-true-ex ag) temp-ex)
    )
)
    
;; This method is a backchannel used to inform the agent of the ground truth 
;; actions taken by other agents as they occur, even those that are 
;; unobservable. This is to be used only for 
;; measuring the difference between an agent's beliefs and reality (e.g., for 
;; calculating precision and recall). If an agent makes any decisions based on 
;; this information, it invalidates partial observability experiments.
(defmethod measure-with-ground-truth ((ag artue-agent) ground-truth-actions &aux best-ex)
    (when (or (null (get-explanation-results-file ag)) (null (get-prior-observation (get-expgen ag))))
        (return-from measure-with-ground-truth nil)
    )
    (setq *ag* ag)
    (let* ((expgen (get-expgen ag))
           (true-ex (get-true-ex ag))
           (pt (point-next expgen 
                    (occurrence-point (get-prior-observation expgen))))
           (facts (facts-at expgen true-ex pt))
           inc-set
           acts
          )
        (setq acts 
            (apply #'append 
                (mapcar 
                    #'(lambda (action-performer-pair)
                        (make-actions expgen 
                            (first action-performer-pair)
                            (second action-performer-pair)
                            facts
                            pt
                        )
                      )
                    ground-truth-actions
                )
            )
        )
        ;(break "~%Continuing true-ex with actions ~A from point ~A." ground-truth-actions pt)
        (trim-cache expgen true-ex (point-prior expgen pt))
        (extend-explanation expgen true-ex acts)
        (setq inc-set t)
        (let (new-true-ex)
            (loop while inc-set do
                (setq inc-set (find-inconsistencies expgen true-ex))
                (loop while (and inc-set (inconsistency-remains-ambiguous (car inc-set) expgen true-ex)) do
                    (pop inc-set)
                )
                (when inc-set
                    (setq new-true-ex (first (refine-by-binding-next-occ expgen true-ex (car inc-set))))
                    (when (null new-true-ex)
                        (setq new-true-ex 
                            (first (refine-by-binding-prior-occ expgen true-ex (car inc-set)))
                        )
                    )
                    (when (null new-true-ex)
                        (setq inc-set (cdr inc-set))
                        (when (null inc-set)
                            (error "True explanation should not have inconsistencies.")
                        )
                    )
                    (when new-true-ex
                        (setq true-ex new-true-ex)
                    )
                )
            )
        )
        (setf (get-true-ex ag) true-ex)
    )
    (if (null (get-plausible-explanations (get-expgen ag)))
        ;then
        (setq best-ex (get-null-explanation (get-expgen ag)))
        ;else
        (setq best-ex 
            (find-min 
                (mapcar #'(lambda (poss-ex) (bind-to-true-var-symbols (get-expgen ag) poss-ex (get-true-ex ag))) 
                    (get-plausible-explanations (get-expgen ag))
                )
                :key #'(lambda (ex) (calculate-fmeasure (get-expgen ag) ex (get-true-ex ag)))
                :test #'>
            )
        )
    )
    (let* ((ratios (get-true-positive-ratios (get-expgen ag) best-ex (get-true-ex ag)))
           (FN (length (list-false-negatives (get-expgen ag) best-ex (get-true-ex ag))))
           (FP (length (list-false-positives (get-expgen ag) best-ex (get-true-ex ag))))
           (TP (length ratios))
           (TP-ratio (apply #'+ ratios))
           (true-event-count (calculate-event-count (get-true-ex ag))) 
           (poss-event-count (calculate-event-count best-ex)))
        (when (not (= (+ TP FP) poss-event-count))
            (error "miscounted positives")
        )
        ;(when (not (= (+ TP FN) true-event-count))
        ;    (error "miscounted reals")
        ;)
        ;(break "FP: ~A FN: ~A TP: ~A TP-ratio: ~A" FP FN TP TP-ratio)
        (with-open-file (str (get-explanation-results-file ag) :direction :output :if-exists :append)
            (format str "~%~A,~A,~A,~A,~A,~A,~A,~A,~A,~A,~A,~A"
                (get-experiment-condition ag)
                (get-explanation-count ag)
                (get-time-string)
                (get-explanation-time ag)
                (+ 0.0
                    (fact-value 
                        (find 'current-time (occurrence-pre (get-last-observation (get-expgen ag)))
                            :key #'fact-type
                        )
                    )
                )
                FN
                FP
                TP
                true-event-count
                TP-ratio
                (calculate-fmeasure (get-expgen ag) best-ex (get-true-ex ag))
                (length (get-plausible-explanations (get-expgen ag)))
            )
        )
    )
)

(defun calculate-event-count (ex)
    (length (remove-if #'is-time-passes-occ (get-events ex)))
)

(defun list-false-negatives (expgen poss-ex true-ex &aux (ret nil) (leftover-events nil) (poss-ev nil))
    (setq leftover-events (get-events poss-ex))
    (dolist (ev (remove-if #'is-time-passes-occ (remove (get-first-point expgen) (get-events true-ex) :key #'occurrence-point)))
        (setq poss-ev (find-if #'(lambda (occ) (occurrence-as-specific-as ev occ)) leftover-events))
        (when (null poss-ev)
            (push ev ret)
        )
        (when poss-ev
            (setq leftover-events (remove poss-ev leftover-events))
        )
    )
    ret
)

(defun list-false-positives (expgen poss-ex true-ex &aux (ret nil))
    (dolist (ev (remove-if #'is-time-passes-occ (get-events poss-ex)))
        (when (null (find-if #'(lambda (occ) (occurrence-as-specific-as occ ev)) (get-events true-ex)))
            (push ev ret)
        )
    )
    ret
)

(defun get-true-positive-ratios (expgen poss-ex true-ex &aux (ret nil) (args nil) (true-evs nil) (true-args nil) (ratios nil))
    (dolist (ev (remove-if #'is-time-passes-occ (get-events poss-ex)))
        (setq true-evs (remove-if-not #'(lambda (occ) (occurrence-as-specific-as occ ev)) (get-events true-ex)))
        (setq ratios (list 0.0))
        (dolist (true-ev true-evs)
            (setq args (cdr (occurrence-signature ev)))
            (setq true-args (cdr (occurrence-signature true-ev)))
            (push 
                (+ 0.0 
                    (/ 
                        (count t (mapcar #'equal args true-args))
                        (length args) 
                    )
                )
                ratios
            )
            ; (format t "~%Possible occ: ~A ~%True occ: ~A ~%TP Ratio: ~A~%"
                ; ev true-ev (car ratios)
            ; )
        )
        (when true-evs
            (push (apply #'max ratios) ret)
        )
    )
    ret
)

(defun bind-to-true-var-symbols (expgen poss-ex true-ex &aux blist total-blist est-act)
    (dolist (act (remove-if-not #'occurrence-is-action (get-events true-ex)))
        (setq est-act (find-if #'(lambda (occ) (occurrence-as-specific-as act occ)) (get-events poss-ex)))
        (when est-act
            (setq blist (pairlis (cdr (occurrence-signature est-act)) (cdr (occurrence-signature act))))
            (setq blist (remove-if-not #'var-symbol-p blist :key #'car))
            (setq blist (remove-if-not #'var-symbol-p blist :key #'cdr))
            (setq total-blist (append blist total-blist))
        )
    )
    (bind-explanation expgen poss-ex nil total-blist)
)

;; Expands action signatures to create a grounded model, usable by the 
;; explainer, of ground truth actions.
(defun make-actions (expgen action performer facts pt &aux model new-facts acts)
    (setq model
        (find (car action) 
            (get-action-models expgen)
            :key #'model-type
            :test #'safe-action-name-equals  
        )
    )
    (setq new-facts
        (remove-from-fact-set
            (get-self-fact expgen)
            (copy-fact-set facts)
        )
    )
    (when performer
        (setq new-facts 
            (add-to-fact-set 
                (make-fact :type 'self :value performer)
                new-facts
            )
        )
    )
    (setq acts
        (enumerate-occs expgen model new-facts pt
            :assume-fn #'default-closed-world-assume-fn
            :ground-all-vars nil
            :bind-var-symbols nil
            :bindings
                (pairlis (mapcar #'first (event-model-arg-types model))
                    (cdr action)
                )
        )
    )
    (setq acts (remove-duplicates acts :test #'occurrence-equal))
    (dolist (act acts)
        (setf (occurrence-is-action act) t)
        (setf (occurrence-is-event act) nil)
        (setf (occurrence-is-assumption act) t)
        (setf (occurrence-pre act) (remove 'self (occurrence-pre act) :key #'fact-type))
    )
    acts
)

(defun calculate-fmeasure (expgen found-ex true-ex)
    (let* ((ratios (get-true-positive-ratios expgen found-ex true-ex))
           (FN (length (list-false-negatives expgen found-ex true-ex)))
           (FP (length (list-false-positives expgen found-ex true-ex)))
           (TP (length ratios))
           (TP-ratio (apply #'+ ratios))
           (precision (safe-/ TP-ratio (+ TP FP)))
           (recall    (safe-/ TP-ratio (+ TP FN))))
      
        (if (and (= 0 precision) (= 0 recall))
            ;then
            0
            ;else
            (* 2 (/ (* precision recall) (+ precision recall)))
        )
    )
)

(defun safe-/ (div quo)
    (if (= 0 quo)
        ;then 
        1
        ;else
        (/ div quo)
    )
)
    

