 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: 2009
 ;;;; Project Name: ARTUE
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file defines the ARTUE package and loads algorithm files necessary 
 ;;;; to support the current experimental condition, as described in global 
 ;;;; variables.
 
 
(defpackage :artue
    (:shadow name unify define trace-print) 
    (:use :common-lisp :shop2 :utils)
    (:export rover-domain *model* explain explain-fact fact-form find-fact 
        get-env-assumps get-envs make-facts query rules-for-model unifies
        in-nodes out-nodes consistent-nodes datum-tms-node considering! 
        true? in? out? consistent-with? why? show-data show-context show
        assumption-nodes true-nodes assumption-count rule in-atre create-atre
        atre-atms *atre* build-resolver-state generate-goals read-file-to-list 
        define test-event-explanation
        ;make-atre-for-pddl
    )
)

(in-package :artue)

(import 'shop2::domain)
(export 'domain)

(defvar *performed-actions* nil)

(defvar *explainer-version* 2)
(defvar *atms* 'nada)
(defvar *model* nil)
(defvar *save-files* t)
(defvar *uncertain-init* nil)
(defvar *define-fn* nil)
(defvar *pid* "")
(defvar *planner* 'shop2)
(defvar *problem-name* nil)
(defvar *prior-plan-prob* nil)

(defvar *record-logs* t)
(defvar *domain-name* 'nada)
(defvar *domain-file* 'nada)
(defvar *mot-state* 'nada)
(defvar *motivation-file* nil)
(defvar *dom-package* 'nada)
(defvar *confirmed-explanation* nil)

(when (boundp 'cl-user::*explainer-version*)
    (setq *explainer-version* cl-user::*explainer-version*)
)


; MCM (05/19): Lisp complains about reading in the "#t" from processes as 
; defined in PDDL+.
(set-dispatch-macro-character #\# #\t        ;dispatch on #t
    #'(lambda (s c n) (declare (ignore s c n)) '?__dur) 
)


;; These symbols must be pre-registered in the artue domain for the define 
;; code to work as expected.

(defmacro define (&rest obj-def)
   (when *define-fn*
       (return-from define (funcall *define-fn* obj-def))
   )
   (setq obj-def (switch-symbols-to-package obj-def :artue))
   (push (cons :file *load-pathname*) (cdr obj-def))
   (cond 
      ((eq (caar obj-def) 'domain) 
          (let ((domain-name (cadr (assoc 'domain obj-def))))
              (setq *domain-name* domain-name)
              `(progn
                  (shop2:defdomain (,domain-name :type pddl-plus-sensing-domain) ,(cdr obj-def))
                  (when (> *explainer-version* 1)
                      (read-pddl-domain (output-domain *domain*))
                  )
               )
          )
      )
      ((eq (caar obj-def) 'problem) `(read-pddl-problem ',obj-def))
      ((eq (caar obj-def) 'htn) 
          `(progn 
              (setq *domain-name* (domain-name (read-htn ',obj-def)))
              (setf (get *domain-name* :pddls) (get ',(second (assoc :domain obj-def)) :pddls))
              (when (> *explainer-version* 1)
                  (read-pddl-domain (output-domain *domain*))
              )
           )
      )
   )
)

(defun load-artue-files (&key (action :compile-if-newer))
    (bps-load-files (make-pathname :directory '(:relative "main")) '("artue-utils") :action :load)
    (bps-load-files (make-pathname :directory '(:relative "main")) '("planning-logs") :action :load)
    (bps-load-files (make-pathname :directory '(:relative "main")) '("problem") :action action)
    (if (eq *explainer-version* 1)
        ;then
        (bps-load-files (make-pathname :directory '(:relative "full-explainer")) '("event-explain" "transition-manager") :action action)
        ;else
        (bps-load-files (make-pathname :directory '(:relative "full-explainer")) '("explainer-loader") :action action)
    )
    (bps-load-files (make-pathname :directory '(:relative "full-explainer")) '("diagnostic-planner") :action :compile-load)
    (bps-load-files (make-pathname :directory '(:relative "planning")) '("shop-connector") :action action)
    (bps-load-files (make-pathname :directory '(:relative "motivations")) '("query" "goal-generation") :action action)
    (bps-load-files (make-pathname :directory '(:relative "main")) '("planning-sim" "agents" "artue-control" "explanation-test") :action action)
    (bps-load-files (make-pathname :directory '(:relative "main")) '("artue-utils") :action :compile-load)
    (bps-load-files (make-pathname :directory '(:relative "learning")) '("foil-utils" "examples" "clauses" "foil-ps" "foil-ps-interface") :action :compile-load)
)

(defun plan-for (goal believed-prob &key (exes nil) (time-limit-secs 300) (which :first) (depth-cutoff 2500) (domain-model *domain*) (expgen nil))
    (case *planner* 
        (shop2 (shop-plan-for goal believed-prob :time-limit-secs time-limit-secs :which which :depth-cutoff depth-cutoff :domain-model domain-model)) 
        (carpe (carpe-plan-for goal believed-prob :time-limit-secs time-limit-secs :which which :depth-cutoff depth-cutoff)) 
        (ff (ff-plan-for goal believed-prob :time-limit-secs time-limit-secs :which which :depth-cutoff depth-cutoff :exes exes :expgen expgen))
        (t (error "No planner with name ~A" *planner*))
    )
)


(load-artue-files)

