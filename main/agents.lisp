 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: 2014
 ;;;; Project Name: Explanation
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file provides the agent-layer class which performs delegation
 ;;;; to facilitate combination of Lisp agents capable of functioning 
 ;;;; within the ARTUE ecosystem.
 
(in-package :artue)

(defclass agent ()
    ()
)

(defgeneric agent-get-next-action (agent))
(defgeneric agent-get-actions-to-execute (agent))
(defgeneric agent-start-action-executing (agent action))
(defgeneric agent-initial-observation (agent prob))
(defgeneric agent-construct-plan (agent &key ext-goal))
(defgeneric agent-not-finished (agent))
(defgeneric agent-observe (agent actions observable-prob))
(defgeneric agent-executed-action-count (agent))
(defgeneric measure-with-ground-truth (agent ground-truth-actions))
(defgeneric initialize-ground-truth (agent ground-truth-state))


(defclass agent-layer (agent)
    (
        (next-layer
            :initform nil
            :accessor get-next-layer
            :initarg  :next-layer
        )
    )
)

(defmethod agent-initial-observation ((agent agent-layer) observable-prob)
    (agent-initial-observation (get-next-layer agent) observable-prob)
)

(defmethod agent-get-actions-to-execute ((agent agent-layer))
    (agent-get-actions-to-execute (get-next-layer agent))
)

(defmethod agent-get-next-action ((agent agent-layer))
    (agent-get-next-action (get-next-layer agent))
)

(defmethod agent-start-action-executing ((agent agent-layer) action)
    (agent-start-action-executing (get-next-layer agent) action)
)

(defmethod agent-construct-plan ((agent agent-layer) &rest args)
    (apply #'agent-construct-plan (get-next-layer agent) args)
)


(defmethod agent-not-finished ((agent agent-layer))
    (agent-not-finished (get-next-layer agent))
)

(defmethod agent-observe ((agent agent-layer) actions observable-prob)
    (agent-observe (get-next-layer agent) actions observable-prob)
)

(defmethod agent-executed-action-count ((agent agent-layer))
    (agent-executed-action-count (get-next-layer agent))
)

(defmethod initialize-ground-truth ((agent agent-layer) ground-truth-state)
    (initialize-ground-truth (get-next-layer agent) ground-truth-state)
)

(defmethod measure-with-ground-truth ((agent agent-layer) ground-truth-actions)
    (measure-with-ground-truth (get-next-layer agent) ground-truth-actions)
)

(defclass agent-identity-layer (agent-layer)
    (
        (id 
            :initarg  :identity
            :accessor get-identity
        )
        (added-values
            :initform nil
            :accessor get-added-values
            :initarg  :added-values
        )
    )
)

(defmethod agent-initial-observation ((agent agent-identity-layer) observable-prob)
    (setq observable-prob (copy-pddl-problem observable-prob))
    (setf (pddl-problem-values observable-prob) 
        (append (get-added-values agent) (pddl-problem-values observable-prob))
    )
    (call-next-method agent observable-prob)
)

(defmethod agent-observe ((agent agent-identity-layer) actions observable-prob)
    (setq observable-prob (copy-pddl-problem observable-prob))
    (setf (pddl-problem-values observable-prob) 
        (append (get-added-values agent) (pddl-problem-values observable-prob))
    )
    (format t "~&Agent observing: ~A" (get-identity agent))
    (call-next-method agent actions observable-prob)
)

(defun agent-identity-layer-initialize (next-layer id)
    (make-instance 'agent-identity-layer 
        :next-layer next-layer
        :identity id
        :added-values `(((self) ,id))
    )
)

