 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: 2010
 ;;;; Project Name: ARTUE
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file provides the experimentation main loop for domains that are 
 ;;;; implemented by executing the planner on them, e.g., rover, satellite.

(in-package :artue)

(export '(achieve *cur-state* *believed-state* 
          play-recording 
          adopt-assumptions reset-assumptions explain accept-no-explanation 
          run-out-of-actions-replan 
         )
)

(defvar *debug-cache-sim* nil)

(defvar *break-at-trial-end* nil)


(defun test-event-explanation (
            ag htn-file problem-file
            &key ;(observability :partial) 
                 (problem-type nil)
                 (trigger-fn nil)
                 (generator-fn #'identity)
            &aux sim action result); observable-prob)
    (setq sim (shop2-simulation-initialize htn-file))
    (setq *debug-cache-sim* sim)
    
    (sim-load-problem sim problem-file problem-type generator-fn)
    (agent-initial-observation ag
        (sim-observe-initial sim
            nil (get-observability (get-next-layer ag))
        )
    )

    (loop while (and (agent-not-finished ag) 
                     (< (agent-executed-action-count ag) 500)) do
        (setq action (agent-get-next-action ag))
        (when trigger-fn 
            (funcall trigger-fn (get-cur-state sim) action)
        )
        (agent-start-action-executing ag action)
        (sim-execute-action sim action)
        (execute-sensing-actions sim ag)
        (setq result
            (agent-observe ag (list action)
                (sim-observe-state sim nil (get-observability (get-next-layer ag)))
            )
        )
        (when (eq result :fail)
            (format nil "~%Failure to continue explanation.")
            (when *break-at-trial-end*
                (setq *sim* sim)
                (setq *ag* ag)
                (break "Trial failed.")
            )
            (return-from test-event-explanation nil)
        )
    )
    (when *break-at-trial-end*
        (setq *sim* sim)
        (setq *ag* ag)
        (break "Trial ended naturally.")
    )
    sim
)


(defun run-multi-agent-planning-simulation (
            agent-list htn-file problem-file
            &key
                 (problem-type nil)
                 (trigger-fn nil)
                 (generator-fn #'identity)
            &aux sim) 
    (setq sim (shop2-simulation-initialize htn-file))
    (sim-load-problem sim problem-file problem-type generator-fn)
    (run-multi-agent-simulation sim agent-list :trigger-fn trigger-fn)
)

(defun run-multi-agent-simulation (sim agent-list &key (trigger-fn nil) &aux cur-actions min-wait action result)
    (setq *debug-cache-sim* sim)
    (dolist (ag agent-list)
        (agent-initial-observation ag 
            (sim-observe-initial sim
                (get-identity ag) (get-observability (get-next-layer ag))
            )
        )
        (initialize-ground-truth ag 
            (sim-observe-initial sim (get-identity ag) :full)
        )
    )
    

    (loop while (and (some #'agent-not-finished agent-list) 
                     (< (apply #'min (mapcar #'agent-executed-action-count agent-list)) 500)) do
        (setq min-wait 10000)
        (setq cur-actions nil)
        (dolist (ag agent-list)
            (setq action (agent-get-next-action ag))
            (when (eq (car action) '!wait)
                (setq min-wait (min (second action) min-wait)) 
                (setq action nil)
            )
            (when (eq (car action) '!wait-for)
                (error "Agent should not return wait-for.")
            )
            (when action
                (push (list action (get-identity ag)) cur-actions)
            )
        )
          
        (format t "~&Current plans: ")
        (dolist (ag agent-list)
            (format t "~&~A: ~A" (get-identity ag) (get-plan (get-next-layer ag)))
        )
        
        (format t "~&Current actions: ")
        (print-one-per-line cur-actions)  
        
        (when (cdr cur-actions)
            (setq cur-actions (list (nth (random (length cur-actions)) cur-actions)))
            ;(setq cur-actions (list (first cur-actions)))
        )
        
        (when cur-actions
            (setq cur-actions (append cur-actions (list (list (list '!wait (/ 1 1000)) nil)))) 
        )
        (format t "~%~%Filtered actions: ")
        (print-one-per-line cur-actions)  
        
        (when (and (>= min-wait 10000) (null cur-actions))
            (format t "~%~%No more actions.")
            (when *break-at-trial-end*
                (setq *sim* sim)
                (setq *ag-list* agent-list)
                (break "Trial ended with all agents waiting indefinitely.")
            )
            (return-from run-multi-agent-simulation)
            ;(break "Long wait in multi-agent loop.")
        )
        (when (null cur-actions)
            (setq cur-actions (list (list (list '!wait min-wait) nil)))
        )
        (when trigger-fn 
            (funcall trigger-fn (get-cur-state sim) cur-actions)
        )
        (dolist (action-id cur-actions)
            (if (null (second action-id))
                ;then
                (dolist (ag agent-list)
                    (agent-start-action-executing ag (first action-id))
                )
                ;else
                (let ((ag (get-agent-by-id (second action-id) agent-list)))
                    (when ag
                        (agent-start-action-executing ag (first action-id))
                    )
                )
            )
            (sim-execute-action sim (first action-id) :id (second action-id))
        )

        (format t "~&Injuries: ~A" 
            (shopthpr:find-satisfiers '(person-has-property ?p injured) (get-cur-state sim) nil)
        )
        
        ; (break "Before observation.")

        (dolist (ag agent-list)
            (execute-sensing-actions sim ag)
            (setq result
                (agent-observe ag (get-my-actions (get-identity ag) cur-actions)
                    (sim-observe-state sim
                        (get-identity ag) 
                        (get-observability (get-next-layer ag))
                    )
                )
            )
            (when (eq result :fail)
                (format nil "~%Failure to continue explanation.")
                (when *break-at-trial-end*
                    (setq *sim* sim)
                    (setq *ag-list* agent-list)
                    (break "Trial failed.")
                )
                (return-from run-multi-agent-simulation nil)
            )
            (measure-with-ground-truth ag 
                (remove-if #'is-internal-action
                    (get-others-actions (get-identity ag) cur-actions)
                    :key #'first
                )
            )
        )
    )
    (when *break-at-trial-end*
        (setq *sim* sim)
        (setq *ag-list* agent-list)
        (break "Trial ended naturally.")
    )
)

(defun get-agent-by-id (id agent-list)
    (dolist (ag agent-list)
        (when (eq (get-identity ag) id)
            (return-from get-agent-by-id (get-next-layer ag))
        )
    )
    nil
)

(defun run-havok-simulation (sim agent-list &key (trigger-fn nil) &aux cur-actions) 
    (setq *debug-cache-sim* sim)
    (dolist (ag agent-list)
        (agent-initial-observation ag 
            (sim-observe-initial sim
                (get-identity ag) (get-observability (get-next-layer ag))
            )
        )
        (initialize-ground-truth ag 
            (sim-observe-initial sim (get-identity ag) :full)
        )
    )
    

    (loop while (and (some #'agent-not-finished agent-list) 
                     (< (apply #'min (mapcar #'agent-executed-action-count agent-list)) 500)
                     (not (sim-over sim))) do
        (format t "~&Current plans: ")
        (dolist (ag agent-list)
            (format t "~&~A: ~A" (get-identity ag) (get-plan (get-next-layer ag)))
        )

        (dolist (ag agent-list)
            (cond 
                ((and (sim-completed-plan sim) (get-plan (get-next-layer ag)))
                    (setq cur-actions (agent-get-actions-to-execute ag))
                    (sim-replace-plan sim (get-identity ag) cur-actions)
                    (format t "~&Actions started executing: ")
                    (print-one-per-line cur-actions)  
                    ; (when trigger-fn 
                        ; (funcall trigger-fn (get-cur-state sim) cur-actions)
                    ; )
                )
                ((not (sim-completed-plan sim))
                    (sleep 1)
                )
            )
        )


        (dolist (ag agent-list)
            (let (ob actions result)
                (setq ob
                    (sim-observe-state sim
                        (get-identity ag) 
                        (get-observability (get-next-layer ag))
                    )
                )
                (setq actions (get-and-clear-recent-actions sim))
                (when (equalp actions '((!wait 0.0)))
                    (format t "~%No new observations.")
                    (setq actions nil)
                )
                (when actions
                    (format t "~%Actions finished executing:")
                    (print-one-per-line actions)
                    (setq result (agent-observe ag actions ob))
                    (when (eq result :fail)
                        (format t "~%Failure to continue explanation.")
                        (return-from run-havok-simulation nil)
                    )
                )
            )
        )
    )
)

(defun get-my-actions (id cur-actions &aux my-actions)
    (setq my-actions
        (append 
            (remove id cur-actions :key #'second :test-not #'eq)
            (remove nil cur-actions :key #'second :test-not #'eq)
        )
    )
    (when (null my-actions)
        (setq my-actions '(((!wait 0) nil)))
    )
    (mapcar #'first my-actions)
)

(defun get-others-actions (id cur-actions)
    (remove-if #'(lambda (performer) (or (eq performer id) (null performer)))
        cur-actions :key #'second
    )
)

(defun get-performed-actions ()
    (reverse
        (remove-if 
            #'(lambda (item) (member item 
                '(explain adopt-assumptions accept-no-explanation 
                  run-out-of-actions-replan reset-assumptions)
                 )
              )
            *performed-actions*
            :key #'first
        )
    )
)

(defun prepare-model (domain-file &key (new-name nil) (unknown-events nil) 
                                       (unknown-processes nil) (unknown-functions nil)
                                  &aux domain-model envmodel)
    (setq *domain-name* nil)
    (load domain-file)
    (when (null *domain-name*)
        (error "File ~A contains no domain." domain-file)
    )
    (when (null new-name)
        (setq new-name *domain-name*)
    )
    (setq envmodel (copy-envmodel (get *domain-name* :pddls)))
    (setq domain-model (shop2::copy-domain (get *domain-name* :domain)))

    (remove-events-from-model envmodel domain-model unknown-events)    
    (remove-processes-from-model envmodel domain-model unknown-processes)
    (remove-functions-from-model envmodel domain-model unknown-functions)
    (remove-functions-from-transitions envmodel domain-model unknown-functions)
    
    (setf (get new-name :pddls) envmodel)
    (setf (get new-name :domain) domain-model)

)


(defun remove-events-from-model (envmodel domain-model unknown-events 
                                 &aux orig-event-count)
    (setq orig-event-count (length (domain-events domain-model)))
    (when (not (eql (length (envmodel-events envmodel)) orig-event-count))
        (error "Envmodel events do not match domain events in ~A." (domain-file domain-model))
    )
    
    (setf (domain-events domain-model)
        (set-difference 
            (domain-events domain-model)
            unknown-events
            :test #'(lambda (ev name) (eq (name-of ev) name))
        )
    )
    (setf (envmodel-events envmodel)
        (set-difference 
            (envmodel-events envmodel)
            (mapcar #'list unknown-events)
            :key #'car
        )
    )
    
    (when (not (eql (length (domain-events domain-model)) (- orig-event-count (length unknown-events))))
        (error "Unknown events removal from domain unsuccessful in ~A." (domain-file domain-model))
    )
    
    (when (not (eql (length (envmodel-events envmodel)) (- orig-event-count (length unknown-events))))
        (error "Unknown events removal from envmodel unsuccessful in ~A." (domain-file domain-model))
    )
)

(defun remove-processes-from-model (envmodel domain-model unknown-processes 
                                    &aux orig-process-count)
    (setq orig-process-count (length (domain-processes domain-model)))
    (when (not (eql (length (envmodel-processes envmodel)) orig-process-count))
        (error "Envmodel processes do not match domain processes in ~A." (domain-file domain-model))
    )
    
    (setf (domain-processes domain-model)
        (set-difference 
            (domain-processes domain-model)
            unknown-processes
            :test #'(lambda (proc name) (eq (name-of proc) name))
        )
    )
    (setf (envmodel-processes envmodel)
        (set-difference 
            (envmodel-processes envmodel)
            (mapcar #'list unknown-processes)
            :key #'car
        )
    )
    
    (when (not (eql (length (domain-processes domain-model)) (- orig-process-count (length unknown-processes))))
        (error "Unknown processes removal from domain unsuccessful in ~A." (domain-file domain-model))
    )
    
    (when (not (eql (length (envmodel-processes envmodel)) (- orig-process-count (length unknown-processes))))
        (error "Unknown processes removal from envmodel unsuccessful in ~A." (domain-file domain-model))
    )
)

(defun remove-functions-from-model (envmodel domain-model unknown-functions 
                                 &aux orig-function-count)
    (setq orig-function-count 
        (+ (length (domain-functions domain-model))
           (length (domain-predicates domain-model))
        )
    )
    (when (not 
            (eql 
                (+ (length (remove-if #'atom (envmodel-functions envmodel))) 
                   (length (envmodel-predicates envmodel))) 
                orig-function-count
            )
          )
        (error "Envmodel functions do not match domain functions in ~A." (domain-file domain-model))
    )
    
    (setf (domain-functions domain-model)
        (set-difference 
            (domain-functions domain-model)
            unknown-functions
            :test #'(lambda (func name) (eq (shop2::pddl-function-name func) name))
        )
    )
    (remove-function-predicates envmodel unknown-functions)
    
    (setf (domain-predicates domain-model)
        (set-difference 
            (domain-predicates domain-model)
            unknown-functions
            :test #'(lambda (pred name) (eq (car pred) name))
        )
    )

    (setf (envmodel-predicates envmodel)
        (set-difference 
            (envmodel-predicates envmodel)
            unknown-functions
            :test #'(lambda (pred name) (eq (car pred) name))
        )
    )

    (when (not 
            (eql 
                (+ (length (domain-functions domain-model)) 
                   (length (domain-predicates domain-model))) 
                (- orig-function-count (length unknown-functions))
            )
          )
        (error "Unknown functions removal from domain unsuccessful in ~A." (domain-file domain-model))
    )
    
    (when (not 
            (eql 
                (+ (length (remove-if #'atom (envmodel-functions envmodel))) 
                   (length (envmodel-predicates envmodel))) 
                (- orig-function-count (length unknown-functions))
            )
          )
        (error "Unknown functions removal from envmodel unsuccessful in ~A." (domain-file domain-model))
    )
)

(defun remove-function-predicates (envmodel unknown-functions 
                                   &aux new-functions found-pred ptr)
    (setq ptr (envmodel-functions envmodel))
    (setq new-functions nil)
    (setq found-pred nil)

    (loop while ptr do
        (cond
            ((eq (car ptr) '-)
                (when (not (atom (second ptr)))
                    (error "Expected atom next.")
                )
                (when found-pred
                    (push '- new-functions)
                    (push (second ptr) new-functions)
                    (setq found-pred nil)
                )
                (setq ptr (cddr ptr))
            )
            ((not (listp (car ptr)))
                (error "Expected function signature next.")
            )
            ((not (member (caar ptr) unknown-functions))
                (push (car ptr) new-functions)
                (setq found-pred t)
                (setq ptr (cdr ptr))
            )
            (t ;; Must be an unknown function on top.
                (setq ptr (cdr ptr))
            )
        )
    )
    (setf (envmodel-functions envmodel) (reverse new-functions))
)

(defun remove-functions-from-transitions (envmodel domain-model unknown-functions)
    (dolist (process-model (domain-processes domain-model))
        (setf (shop2::pddl-process-precondition process-model)
            (remove-function-calls 
                (shop2::pddl-process-precondition process-model) 
                unknown-functions
                (mapcar #'car (shop2::pddl-process-parameters process-model))
            )
        )
        (setf (shop2::pddl-process-effect process-model)
            (remove-functional-effects (shop2::pddl-process-effect process-model) unknown-functions)
        )
    )
    (dolist (act-model (remove-if-not #'shop2::pddl-action-p (mapcar #'cdr (hash-to-alist (domain-operators domain-model)))))
        (setf (shop2::pddl-action-precondition act-model)
            (remove-function-calls 
                (shop2::pddl-action-precondition act-model) 
                unknown-functions
                (cdr (shop2::pddl-action-head act-model))
            )
        )
        (setf (shop2::pddl-action-effect act-model)
            (remove-functional-effects (shop2::pddl-action-effect act-model) unknown-functions)
        )
    )
    (dolist (ev-model (domain-events domain-model))
        (setf (shop2::pddl-event-precondition ev-model)
            (remove-function-calls 
                (shop2::pddl-event-precondition ev-model) 
                unknown-functions
                (mapcar #'car (shop2::pddl-event-parameters ev-model))
            )
        )
        (setf (shop2::pddl-event-effect ev-model)
            (remove-functional-effects (shop2::pddl-event-effect ev-model) unknown-functions)
        )
    )
    (dolist (pddl-model (append (envmodel-events envmodel) (envmodel-actions envmodel) (envmodel-processes envmodel)))
        (setf (pddl-model-preconditions pddl-model)
            (remove-function-calls 
                (pddl-model-preconditions pddl-model) 
                unknown-functions
                (mapcar #'car (make-type-declarations (pddl-model-parameters pddl-model)))
            )
        )
        (setf (pddl-model-effects pddl-model)
            (remove-functional-effects (pddl-model-effects pddl-model) unknown-functions)
        )
    )
)

                
(defun remove-function-calls (pddl-cond unknown-functions known-vars)
    (first (remove-orphaned-assigns (list (remove-calls pddl-cond unknown-functions)) known-vars))
)

(defun remove-orphaned-assigns (pddl-cond &optional (known-vars nil) 
                                          &aux new-stmt new-cond)
    (dolist (stmt pddl-cond)
        (cond
            ((eq stmt t))
            ((eq stmt 'and) (push 'and new-cond))
            ((eq (car stmt) 'and)
                (multiple-value-setq (new-stmt known-vars)
                    (remove-orphaned-assigns (cdr stmt) known-vars)
                )
                (push (cons 'and new-stmt) new-cond)
            )
            ((and (eq (car stmt) 'assign) 
                  (null 
                      (set-difference 
                          (remove-if-not #'plan-var-p (flatten (third stmt))) 
                          known-vars
                      )
                  )
                    (setq known-vars (adjoin (second stmt) known-vars))
                    (push stmt new-cond)
                )
            )
            ((eq (car stmt) 'assign)
                (setq known-vars (adjoin (second stmt) known-vars))
                (push `(create object ,(second stmt) (unknown-fn)) new-cond)
            )
            (t
                (setq known-vars 
                    (union known-vars
                        (remove-if-not #'plan-var-p (flatten stmt))
                    )
                )
                (push stmt new-cond)
            )
        )
    )
    (values (reverse new-cond) known-vars)
)
        
                
                
    
(defun remove-calls (pddl-cond unknown-functions)
    (cond
        ((atom pddl-cond)
            pddl-cond
        )
        ((member (car pddl-cond) '(and or))
            (cons (car pddl-cond) (mapcar+ #'remove-calls (cdr pddl-cond) unknown-functions))
        )
        ((member (car pddl-cond) unknown-functions)
            t
        )
        ((and (member (car pddl-cond) '(eq neq < >)) 
              (listp (second pddl-cond))
              (member (car (second pddl-cond)) unknown-functions)
         )
            t
        )
        (t pddl-cond)
    )
)
    
(defun remove-functional-effects (pddl-effect unknown-functions)
    (cond
        ((atom pddl-effect)
            pddl-effect
        )
        ((eq (car pddl-effect) 'and)
            (cons (car pddl-effect) 
                (remove nil 
                    (mapcar+ #'remove-functional-effects (cdr pddl-effect) unknown-functions)
                )
            )
        )
        ((intersection (flatten pddl-effect) unknown-functions)
            nil
        )
        (t pddl-effect)
    )
)

;; This executes all sensing actions an agent wants simultaneously. Not 
;; appropriate for costly or time-consuming sensing actions.
(defun execute-sensing-actions (sim ag &aux sensing-actions)
    (setq sensing-actions (get-sensing-actions (get-next-layer ag)))
    (dolist (sensor sensing-actions)
        (sim-execute-action sim sensor)
    )
)


