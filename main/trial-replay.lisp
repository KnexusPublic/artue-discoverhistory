 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: 2012
 ;;;; Project Name: ARTUE
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file provides functionality for replay of a recorded ARTUE 
 ;;;; trace. Needs fixing to bring up to date.

(in-package :artue)

(defun play-recording (htn-file problem-file recording depict-fn 
                       &key (observability :partial)
                            (start-at 0))
    (let ((past-recording nil) (future-recording recording) (done nil) 
          (action nil) (past-states nil) (output t) (input t) (goals nil))
        (load-scenario htn-file problem-file)
        (setf (pddl-problem-domain *problem*) (domain-name *domain*))
        (setq past-recording (reverse (subseq recording 0 start-at)))
        (setq future-recording (subseq recording start-at))
        (setq past-states 
            (run-through-recording (reverse past-recording) :observability observability)
        )
        (setq goals (cdr (first (pddl-problem-goal *problem*))))
        (funcall depict-fn *cur-state* *believed-state* goals)
        (loop while (not done) do
            (when input
                (clear-input)
                (format t "~%Next action: ~A" (car future-recording))
                (format t "~%[N]ext, [P]revious, [B]reak, [Q]uit, e[X]plain, p[L]an? ")
            )
            (setq input t)
            (setq output t)
            (case (read-char)
                ((#\n #\N) 
                    (when (null future-recording)
                        (format t "~%~% Reached final state.")
                        (setq output nil)
                    )
                    (unless (null future-recording)
                        (setq action (pop future-recording)) 
                        (push action past-recording)
                        (push 
                            (list (copy-state *cur-state*) (copy-state *believed-state*))
                            past-states
                        )
                        (if (equal (car action) 'adopt-assumptions)
                            ;then
                            (run-through-recording (reverse past-recording) :observability observability)
                            ;else
                            (step-states action nil)
                        )
                    )
                )
                ((#\p #\P)
                    (when (null past-states)
                        (format t "~%~% Reached initial state.")
                        (setq output nil)
                    )
                    (unless (null past-states)
                        (setq action (pop past-recording))
                        (push action future-recording)
                        (setq action (car past-recording))
                        (let ((states (pop past-states)))
                            (setq *cur-state* (first states)
                                  *believed-state* (second states)
                            )
                        )
                    )
                )
                ((#\x #\X)
                    (initiate-explanation past-recording 
                        (cons (list *cur-state* *believed-state*) past-states)
                    )
                    (setq output nil)
                )
                ((#\l #\L)
                    (setq *believed-prob* (convert-state-to-problem *believed-state*))
                    (setf (pddl-problem-goal *believed-prob*) (pddl-problem-goal *problem*))
                    (setf (pddl-problem-objects *believed-prob*) (pddl-problem-objects *problem*))
                    (when (null (form-plans))
                        (setf (pddl-problem-goal *believed-prob*)
                            (cdr (pddl-problem-goal *believed-prob*))
                        )
                        (form-plans)
                    )
                )
                ((#\b #\B)
                    (break)
                )
                ((#\q #\Q)
                    (setq done t)
                    (setq output nil)
                )
                (otherwise
                    (setq input nil)
                    (setq output nil)
                )
            )
            (when output
                (format t "~%~%Time ~A" (length past-recording))
                (funcall depict-fn *cur-state* *believed-state* goals)
                (case (car action)
                    ('explain 
                        (format t "~%~% Discrepancy found:")
                        (print-one-per-line
                            (append
                                (set-difference
                                    (remove-unexpectable
                                        (state-atoms *cur-state*)
                                        :hidden-only t
                                    )
                                    (remove-unexpectable
                                        (state-atoms *believed-state*)
                                        :hidden-only t
                                    )
                                    :test #'equal
                                )
                                (mapcar #'not-ify
                                    (set-difference
                                        (remove-unexpectable
                                            (state-atoms *believed-state*)
                                            :hidden-only t
                                        )
                                        (remove-unexpectable
                                            (state-atoms *cur-state*)
                                            :hidden-only t
                                        )
                                        :test #'equal
                                    )
                                )
                            )
                        )
                                    
                        (format t "~%~% Explanation occurs.")
                    )
                    ('accept-no-explanation
                        (format t "~%~% Explanation fails.")
                    )
                    ('adopt-assumptions
                        (format t "~%~% New assumptions:")
                        (print-one-per-line (cdr action))
                    )
                    ('reset-assumptions
                        (format t "~%~% Reset using assumptions:")
                        (print-one-per-line (cdr action))
                    )
                    ('run-out-of-actions-replan
                        (format t "~%~% No more actions in plan.")
                    )
                    (otherwise
                        (format t "~%~% Action executed: ~A" action)
                    )
                )
            )
        )
    )
)

(defun initiate-explanation (past-actions past-states)
    (when (null past-actions)
        (format t "~%No time for events to occur.")
        (return-from initiate-explanation nil)
    )
    (when (member (caar past-actions) 
                  '(explain adopt-assumptions
                    accept-no-explanation run-out-of-actions-replan
                   )
          )
        (initiate-explanation (cdr past-actions) (cdr past-states))
        (return-from initiate-explanation nil)
    )
    (acquire-history (cdr past-actions) (cdr past-states) nil)
    (setq *observed-prob* (convert-state-to-observable-problem (caar past-states)))
    (find-and-explain-discrepancies (list (car past-actions)))
)

(defun acquire-history (past-actions past-states assumptions)
    (when (null past-actions)
        (start-explaining
            (append 
                (remove-unexpectable (pddl-problem-init *problem*))
                (pddl-problem-given *problem*)
                assumptions
            )
            (append 
                (remove-unexpectable (pddl-problem-values *problem*))
                (pddl-problem-given-values *problem*)
            )
        )
        (return-from acquire-history nil)
    )
    (when (eq (caar past-actions) 'reset-assumptions)
        (break "Resetting assumptions.")
        (start-explaining
            (append 
                (remove-unexpectable (state-atoms (second (first past-states))))
                (if assumptions assumptions (cdar past-actions))
            )
            (list-state-values (second (first past-states)))
        )
        (return-from acquire-history nil)
    )
    (when (eq (caar past-actions) 'adopt-assumptions)
        (setq assumptions (cdar past-actions))
    )
    (acquire-history (cdr past-actions) (cdr past-states) assumptions)
    (setq *observed-prob* (convert-state-to-observable-problem (caar past-states)))
    (when (not (member (caar past-actions) 
                       '(explain adopt-assumptions accept-no-explanation run-out-of-actions-replan)))
        (handle-new-transition (list (car past-actions)))
        (add-observation)
        (setq *believed-prob* (copy-pddl-problem *observed-prob*))
        (fill-latest-observation *believed-prob* *current-hypothesis*)
    )
)

(defun start-explaining (start-facts start-values)
    (setq *current-state* (shop2::make-fun-state nil))
    (setq *observed-prob* (copy-pddl-problem *problem*))
    (setf (pddl-problem-init *observed-prob*) start-facts)
    (setf (pddl-problem-values *observed-prob*) start-values)
    (initialize-explainer)
    (handle-initial-observations)
)

(defun load-scenario (htn-file problem-file)
    (initialize-session htn-file "motivations/empty.meld" 1)
    (reset-session)
    (load problem-file)
)    

(defun run-through-recording (recording &key (observability :partial) &aux (past-states nil) (assumps nil) (last-reset nil))
    (setq *observed-prob* (copy-pddl-problem *problem*))
    (setf (pddl-problem-name *observed-prob*) 'simulation)
    (when (eq observability :partial)
        (setf (pddl-problem-init *observed-prob*) 
            (append
                (remove-unexpectable
                    (pddl-problem-init *observed-prob*)
                    :hidden-only t
                )
                (pddl-problem-given *observed-prob*)
            )
        )
        (setf (pddl-problem-values *observed-prob*) 
            (append
                (remove-unexpectable
                    (pddl-problem-values *observed-prob*)
                    :hidden-only t
                )
                (pddl-problem-given-values *observed-prob*)
            )
        )
    )
    (setq *believed-prob* (copy-pddl-problem *observed-prob*))
    (setq assumps (cdr (find 'adopt-assumptions recording :key #'car :from-end t)))
    ; (setf (pddl-problem-init *believed-prob*)
        ; (append
            ; (remove 'not (pddl-problem-init *believed-prob*) :key #'car)
            ; assumps
        ; )
    ; )
    (setq *cur-state* (export-problem-to-shop (change-package *problem* :shop2)))
    (setq *believed-state* (export-problem-to-shop (change-package *believed-prob* :shop2)))
    (push 
        (list (copy-state *cur-state*) (copy-state *believed-state*))
        past-states
    )
    (setq last-reset (find 'reset-assumptions recording :key #'car :from-end t))
    (setq assumps (cdr (find 'adopt-assumptions (member last-reset recording) :key #'car :from-end t)))
    (dolist (action recording) 
        (push 
            (list (copy-state *cur-state*) (copy-state *believed-state*)) 
            past-states
        )
        (if (eq last-reset action)
            ;then
            (step-states action assumps)
            ;else
            (step-states action nil)
        )
    )
    past-states
)

(defun step-states (action assumps)
    (when (member (car action) '(adopt-assumptions explain run-out-of-actions-replan))
        (return-from step-states nil)
    )
    (when (eq (car action) 'reset-assumptions)
        (setf (pddl-problem-init *believed-prob*) 
            (append
                (remove-unexpectable
                    (state-atoms *cur-state*)
                    :hidden-only t
                )
                (if (null assumps)
                    ;then
                    (cdr action)
                    ;else
                    assumps
                )
            )
        )
        (setf (pddl-problem-values *believed-prob*) 
            (append
                (remove-unexpectable
                    (list-state-values *cur-state*)
                    :hidden-only t
                )
            )
        )
        (setq *believed-state* 
            (export-problem-to-shop 
                (change-package *believed-prob* :shop2)
            )
        )
        (return-from step-states nil)
    )
    (when (equal action '(accept-no-explanation))
        (setf (pddl-problem-init *believed-prob*) 
            (append
                (remove-unexpectable
                    (state-atoms *cur-state*)
                    :hidden-only t
                )
                (set-difference
                    (state-atoms *believed-state*)
                    (remove-unexpectable
                        (state-atoms *believed-state*)
                        :hidden-only t
                    )
                    :test #'equal
                )
            )
        )
        (setf (pddl-problem-values *believed-prob*) 
            (append
                (remove-unexpectable
                    (list-state-values *cur-state*)
                    :hidden-only t
                )
                (set-difference
                    (list-state-values *believed-state*)
                    (remove-unexpectable
                        (list-state-values *believed-state*)
                        :hidden-only t
                    )
                    :test #'equal
                )
            )
        )
        (setq *believed-state* 
            (export-problem-to-shop 
                (change-package *believed-prob* :shop2)
            )
        )
        (return-from step-states nil)
    )
    
    (shop-trace :events)
    (let ((temp-astate nil) (temp-bstate nil))
        (format t "~%~%Expected events: ")
        (setq temp-bstate
            (extrapolate-state 
                *domain*
                *believed-state*
                (artue::fix-action action)
            )
        )
        (when (equal temp-bstate (shop-fail))
            (format t "~%Error predicting future; beliefs do not permit action ~A."
                action
            )
;            (break "What now?")
            (return-from step-states nil)
        )
        (format t "~%~%Actual events: ")
        (setq temp-astate
            (extrapolate-state 
                *domain*
                *cur-state*
                (artue::fix-action action)
            )
        )
        (when (equal temp-astate (shop-fail))
            (format t "~%Error processing next action; world does not permit action ~A."
                action
            )
            (return-from step-states nil)
        )
        (setq *believed-state* temp-bstate
              *cur-state* temp-astate
        )
;        (break "Really?")
    )
    (shop-untrace :events)
)


