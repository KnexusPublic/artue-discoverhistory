 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: Jun 3, 2009
 ;;;; Project Name: ARTUE
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This class imports problems.

(in-package :artue)

(export '(pddl-problem-name pddl-problem-domain pddl-problem-objects
          pddl-problem-init pddl-problem-values pddl-problem-goal
          pddl-problem-types pddl-problem-given
          pddl-problem-assumed-values
          pddl-problem-given-values export-problem-to-shop get-isa-facts
          make-pddl-problem copy-pddl-problem problem *problem*
          remove-unexpectable all-objects-of-type))
 
(defvar *problem* nil)

(defstruct pddl-problem
    (name 'unnamed)
    (file nil)
    (domain 'unknown)
    (objs nil)
    (init nil)
    (given nil)
    (values nil)
    (given-values nil)
    (assumed-values nil)
    (goal nil)
    (metric nil)
)

(defun pddl-problem-objects (prob)
    (append (pddl-problem-objs prob)
        (domain-constants (get (pddl-problem-domain prob) :domain))
    )
)

(defsetf pddl-problem-objects (prob) (new-objs)
    `(setf (pddl-problem-objs ,prob) ,new-objs)
)

(defun export-problem-to-shop (prob &key (domain *domain*) (goal nil))
  (setf 
      (get (pddl-problem-name prob) :state) 
        (shop2.common::make-fun-state 
            (change-package (pddl-problem-init prob) :shop2) 
            :types (change-package (pddl-problem-types prob) :shop2)
            :fun-values (change-package (pddl-problem-values prob) :shop2)
            :supertypes (shop2::domain-supertypes domain)
        )
  )
  (when (null goal) 
      (setq goal (pddl-problem-goal prob))
  )
  (setf (get (pddl-problem-name prob) :tasks) 
      (shop2::process-task-list (remove-and goal))
  )
  (get (pddl-problem-name prob) :state)
)

(defun convert-state-to-problem (state &aux prob)
    (setq prob (make-pddl-problem))
    (setf (pddl-problem-init prob) 
        (state-atoms state)
    )
    (setf (pddl-problem-values prob) 
        (list-state-values state)
    )
    prob
)

(defun convert-state-to-observable-problem (state &key (domain *domain*) &aux prob)
    (setq prob (make-pddl-problem))
    (setf (pddl-problem-init prob) 
        (remove-unexpectable (state-atoms state))
    )
    (setf (pddl-problem-values prob) 
        (remove-unexpectable (list-state-values state))
    )
    (setf (pddl-problem-domain prob) (domain-name domain))
    (setf (pddl-problem-objects prob) 
        (process-state-objs-to-pddl st)
    )
    prob
)

(defun process-state-objs-to-pddl (st &key (unknown-types nil))
    (mapcan 
        #'(lambda (pair) 
            (if (member (cdr pair) unknown-types) 
                ;then 
                nil 
                ;else
                (list (car pair) '- (cdr pair))
            )
          )
        (hash-to-alist (shop2.common::fun-state-types st))
    )
)


(defun remove-and (condition-lists)
    ;;when single condition
    (when (not (listp (car condition-lists)))
        (setq condition-lists (list condition-lists))
    )
    (mapcar 
        #'(lambda (conditions)
            (if (eq (car conditions) 'and)
                ;then
                (cdr conditions)
                ;else
                conditions
            )
          )
        condition-lists
    )
)

; (defun modify-problem-init (problem init)
	; (setq init (remove-duplicates (utils::change-package init :shop2) :test #'equal))
    ; (setf (pddl-problem-init problem) init)
; )
; 
; (defun modify-problem-values (problem values)
	; (setq values (utils::change-package values :shop2))
    ; (setf (pddl-problem-values problem) values)
; )
; 
(defun modify-problem-goal (problem goal)
	(setq goal (utils::change-package goal :shop2))
    (setf (pddl-problem-goal problem) goal)
)
; 
; (defun modify-problem-objects (problem objects)
	; (setq objects (utils::change-package objects :shop2))
    ; (setf (pddl-problem-objects problem) objects)
; )

(defun function-names (domain)
	(mapcar #'shop2::pddl-function-name (shop2::domain-functions domain))
)

(defun read-pddl-problem (problem)
    (let ((dom-name (second (assoc :domain problem)))
          (domain *domain*)
         )
        (when dom-name
            (setq domain (get dom-name :domain))
            (when (null domain) 
                (setq domain *domain*)
            )
        )
        (setf (get (cadr (assoc 'problem problem)) :probs) 
              (create-problem (switch-symbols-to-package problem :artue))
        )
    )
)

(defun create-problem (problem)
    (let*
      ((p (apply #'make-pddl-problem 
                  (subst :objs :objects (get-problem-arg-list problem))))
       (init (pddl-problem-init p))
      )
        (when (and (null (cdr init)) (eq (caar init) 'and))
            (setq init (cdar init))
        )
        (setq init (remove-duplicates init :test #'equal)) 
        (setf (pddl-problem-init p) init)
        (setq *problem* p)
        p
    )
)

(defun get-problem-arg-list (problem)
   (let ((arg-list nil)
        )
       (loop for list-obj in problem do
               (cond 
                  ((eq (car list-obj) 'problem)
                      (push (cadr list-obj) arg-list)
                      (push :name arg-list)
                  )
                  ((eq (car list-obj) :domain)
                      (push (cadr list-obj) arg-list)
                      (push :domain arg-list)
                  )
                  (t
                      (push (cdr list-obj) arg-list)
                      (push (car list-obj) arg-list)
                  )
               )
      )
      arg-list
   )
)

(defun pddl-problem-types (p)
    (decompose-types (pddl-problem-objects p) (make-hash-table))
)

(defun get-isa-facts (prob)
    (let
      ((facts nil))
        (dolist (decl (make-type-declarations (pddl-problem-objects prob)))
            (push (cons 'isa decl) facts)
        )
        facts
    )
)

(defvar *expected-init* 'nada)
(defvar *expected-values* 'nada)
(defvar *actual-init* 'nada)
(defvar *actual-values* 'nada)
    
(defun find-discrepancies (envmodel observed-problem expected-state)
    (progn
      (setq *expected-init*   
           (maptree #'round-off (state-atoms expected-state))
       )
       (setq *expected-values* 
           (maptree #'round-off (list-state-values expected-state))
       )
       (setq *actual-init*     
           (maptree #'round-off (pddl-problem-init observed-problem))
       )
       (setq *actual-values*   
           (maptree #'round-off (pddl-problem-values observed-problem))
       )
	)
	(let* 
       ((missing-facts 
          (remove-unexpectable 
              (set-difference *expected-init* *actual-init* :test #'equal)
              :model envmodel
          )
       )
       (surprising-facts 
          (remove-unexpectable 
              (set-difference *actual-init* *expected-init* :test #'equal)
              :model envmodel
          )
       )
       (surprising-values 
          (remove-unexpectable 
              (set-difference *actual-values* *expected-values* :test #'close-enough)
              :model envmodel
          )
       )
      )
        (format t "~%Before discrepancy search.")
        (finish-output)
        (unless
            (or
                (remove-unexpectable
                    (set-exclusive-or
                        *actual-init*
                        *expected-init*
                        :test #'equal
                    )
                    :model envmodel
                )
                (remove-unexpectable
                    (set-exclusive-or 
                        (maptree #'round-off *actual-values*)
                        (maptree #'round-off *expected-values*)
                        :test #'close-enough
                    )
                    :model envmodel
                )
            )
            (format t "~%All facts match. No explanation necessary.")
    ;        (break)
            (return-from find-discrepancies nil)
        )
        
        
        (format t "~%Expected facts that did not occur:")
        (finish-output)
        (mapcar #'(lambda (x) (format t "~%~A" x))  missing-facts)
        (format t "~%~%Observed facts that were not expected:")
        (mapcar #'(lambda (x) (format t "~%~A" x))  surprising-facts)
        (format t "~%~%~40A ~9A ~9A" "Values:" "Observed" "Expected")
        (mapcan
            #'(lambda (x) 
                (let 
                  ((actual (round-off (second (assoc x *actual-values* :test #'equal))))
                   (expected (round-off (second (assoc x *expected-values* :test #'equal))))
                  )
                    (when (or (null actual) (null expected) (not (within-one-percent actual expected)))
                        (format t "~%~40,1,0,'=A ~9,4F ~9,4F"
                            x 
                            (if actual actual "<Not Set>")
                            (if expected expected "<Not Set>")
                        )
                    )
                )
              ) 
            (sort 
                (union 
                    (mapcar #'first *expected-values*) 
                    (mapcar #'first *actual-values*)
                    :test #'equal
                )
                #'(lambda (x1 x2) (string-lessp (write-to-string x1) (write-to-string x2)))
            )
        )
        (append 
            surprising-facts
            (mapcar #'not-ify missing-facts)
            surprising-values
        )
    )
)

(defun convert-kplanner-problem (prob)
    (setf (pddl-problem-given prob) (pddl-problem-init prob))
)

(defun convert-kplanner-modified-problem (prob)
    (setf (pddl-problem-init prob) (remove 'invariant (pddl-problem-init prob) :key #'first))
    (setf (pddl-problem-given prob) (pddl-problem-init prob))
)

(defun all-objects-of-type (prob type-name)
    (mapcar #'first
        (remove type-name
            (make-type-declarations (pddl-problem-objects prob))
            :key #'second
            :test-not #'eq
        )
    )
)
