 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: 2009
 ;;;; Project Name: ARTUE
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file provides general-use functions needed by ARTUE algorithms.
 
 (in-package :artue)

(defun close-enough (pred pred2)
    (cond
        ((null pred) t)
        ((numberp pred)
            (and 
                (numberp pred2)
                (within-one-percent pred pred2)
            )
        )
        ((numberp (car pred))
            (and 
                (numberp (car pred2))
                (within-one-percent (car pred) (car pred2))
                (close-enough (cdr pred) (cdr pred2))
            )
        )
        ((listp (car pred)) 
            (and
                (listp (car pred2))
                (close-enough (car pred) (car pred2))
                (close-enough (cdr pred) (cdr pred2))
            )
        )
        ((eq (car pred) (car pred2)) (close-enough (cdr pred) (cdr pred2)))
        (t nil)
    )
)

(defun within-one-percent (num1 num2)
    (<= (abs (- num1 num2)) (* 1e-4 (abs num1)))
)

(defun write-session (out-stream domain-file motivation-file time-step-length)
    (write
        (list
            (cons :file 
                (list 
                    :directory (pathname-directory domain-file) 
                    :name (pathname-name domain-file)
                    :type (pathname-type domain-file)
                )
            )
            (cons :motivations-file 
                (if motivation-file
                    ;then
                    (list 
                        :directory (pathname-directory motivation-file) 
                        :name (pathname-name motivation-file)
                        :type (pathname-type motivation-file)
                    )
                    ;else
                    nil
                )
            )
            (cons :time-step time-step-length)
        )
        :stream out-stream 
        :pretty t
    )
    (format out-stream "~%~%")
)

(defun read-in-session (in-stream)
    (let 
      ((domain-file nil)
       (motivations-file nil)
       (time-step-length nil)
       (session-info nil)
      )
        (setq session-info (read in-stream))
        (setq domain-file (apply #'make-pathname (cdr (assoc :file session-info))))
        (setq motivations-file (cdr (assoc :motivations-file session-info)))
        (when motivations-file
            (setq motivations-file (apply #'make-pathname motivations-file))
        )
        (setq time-step-length (cdr (assoc :time-step session-info)))
        (load domain-file)
        (setq shop2::*time-step-length* time-step-length)
        session-info
    )
)

(defun add-hidden-facts (expgen ex prob)
    (push-all (get-explanation-hidden-facts expgen ex) 
        (pddl-problem-init prob)
    )
    (push-all (get-explanation-hidden-values expgen ex) 
        (pddl-problem-values prob)
    )
)

(defun add-certain-facts (expgen prob)
    (push-all (get-certain-facts expgen) (pddl-problem-init prob))
    (push-all (get-certain-values expgen) (pddl-problem-values prob))
    (setf (pddl-problem-init prob)
        (remove-duplicates (pddl-problem-init prob) :test #'equal)
    )
    (setf (pddl-problem-values prob) 
        (remove-duplicates (pddl-problem-values prob) :test #'equal)
    )
)



(defun remove-unexpectable (facts 
                            &key (model nil) (hidden-only nil)
                            &aux unexpectable-types)
    
    (cond
        ((typep model 'envmodel)
            (setq unexpectable-types (envmodel-hidden model))
            (when (not hidden-only)
                (push-all (envmodel-internal model) unexpectable-types)
            )
        )
        ((typep model 'pddl-plus-domain)
            (setq unexpectable-types (domain-hidden model))
            (when (not hidden-only)
                (push-all (domain-internal model) unexpectable-types)
            )
        )
        (t (error "Model argument must be a domain or envmodel."))
    )
    (remove-if 
        #'(lambda (x) 
            (member 
                (cond 
                    ((listp (car x)) (car (car x)))
                    ((eq (car x) 'not) (car (second x)))
                    (t (car x))
                )
                unexpectable-types
            )
          ) 
        facts
    )
)


(defun not-ify (x)
    (if (and (listp x) (eq (first x) 'not))
        ;then
        (second x)
        ;else
        (list 'not x)
    )
)

(defun remove-internal-actions (plan)
    (remove-if #'is-internal-action plan)
)

(defun is-internal-action (action)
    (starts-with "!INTERNAL-ACTION" (symbol-name (car action)))
)

(defun get-lisp-action (internal-action expgen &aux name model)
    (setq name (occurrence-type internal-action))
    (setq model (find name (get-action-models expgen) :key #'model-type))
    (cons (get-shop-action-name name)
        (ntop 
            (length (model-arg-types model)) 
            (cdr (occurrence-signature internal-action))
        )
    )
)

(defun get-shop-action-name (name)
    (if (starts-with "!" (symbol-name name))
        ;then
        name
        ;else
        (intern (concatenate 'string "!" (symbol-name name)) (symbol-package name))
    )
)
