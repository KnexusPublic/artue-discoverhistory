 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: 2016
 ;;;; Project Name: ARTUE
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file defines a standard simulation class and implementation 
 ;;;; provided by SHOP2, which simply maintains a state and updates it 
 ;;;; in the same way as when planning.


(in-package :artue)

(defclass simulation () ())
(defgeneric sim-load-problem (sim problem-file &key &allow-other-keys)) 
(defgeneric sim-observe-initial (sim agent-id observability &key &allow-other-keys))
(defgeneric sim-execute-action (sim action &key id &allow-other-keys))
(defgeneric sim-observe-state (sim agent-id observability &key &allow-other-keys))
(defgeneric sim-shutdown (sim &key &allow-other-keys))
(defgeneric sim-over (sim &key &allow-other-keys))

;; For debugging only.
(defvar *last-sim* nil)

(defclass shop2-simulation (simulation)
  (
    (cur-state
        :initform nil
        :accessor get-cur-state
        :initarg  :cur-state
    )
    (actions-executed
        :initform 0
        :accessor get-actions-executed
        :initarg  :actions-executed
    )
    (performed-actions
        :initform nil
        :accessor sim-performed-actions
        :initarg  :performed-actions
    )
    (initial-problem
        :initform nil
        :accessor get-initial-problem
        :initarg  :initial-problem
    )
    (domain
        :initform nil
        :accessor get-domain
        :initarg  :domain
    )
    (scenario-file
        :initform nil
        :accessor get-scenario-file
    )
    (stopped
        :initform nil
        :accessor sim-stopped
    )
  )
)

(defun shop2-simulation-initialize (domain-file)
    (setq *domain-name* nil)
    (load domain-file)
    (when (null *domain-name*)
        (error "Could not load a domain from ~A." domain-file)
    )
    (setq *last-sim*
        (make-instance 'shop2-simulation 
            :domain (get *domain-name* :domain)
        )
    )
)

(defmethod sim-load-problem ((sim shop2-simulation) problem-file 
                             &key (problem-type nil) (problem-mutator-fn #'identity) 
                             &aux problem
                            )
    (setq *problem* nil)
    (setf (get-scenario-file sim) problem-file)
    (load problem-file)
    (when (null *problem*)
        (error "Could not load a problem from ~A." problem-file)
    )
    (setq problem *problem*)
    (setf (pddl-problem-domain problem) (domain-name (get-domain sim)))
    (when (eq problem-type :kplanner)
        (convert-kplanner-problem problem)
    )
    (when (eq problem-type :kplanner-modified)
        (convert-kplanner-modified-problem problem)
    )
    
    (setq problem (funcall problem-mutator-fn problem))
    (setf (get-initial-problem sim) problem)
    (setf (get-cur-state sim) (export-problem-to-shop problem))

    (setq *performed-actions* nil)
)

(defmethod sim-execute-action ((sim shop2-simulation) action 
                               &key (id nil) &allow-other-keys 
                               &aux observable-prob)
    (when (or (null action) (not (listp action))) 
        (error "Action must be a non-empty list.")
    )
    (setq action (fix-action action))
    (push action *performed-actions*)
    (push (list action id) (sim-performed-actions sim))
    (incf (get-actions-executed sim))
    
    (set-state-value (get-cur-state sim) 'self nil id :provenance 'performer)
    
    (when (gethash (car action) (domain-operators (get-domain sim)))
        (format t "~&~%Executing action: ~A from performer ~A" action id)
        (let ((new-state
                (extrapolate-state 
                    (get-domain sim)
                    (get-cur-state sim)
                    action
                )
             ))
            (when (eq new-state (shop-fail))
                (break "Failed to extrapolate.")
            )
            (setf (get-cur-state sim) new-state)
        )
        (format t "~&Action completed.~%~%")
    )
)
(defmethod sim-observe-initial ((sim shop2-simulation) agent-id observability
                                &key &allow-other-keys
                                &aux observable-prob)
    (when (eq observability :sensing)
        (setq observable-prob (initialize-sensing-observability-state sim))
    )

    (when (eq observability :partial)
        (setq observable-prob (copy-pddl-problem (get-initial-problem sim)))
        (setf (pddl-problem-init observable-prob)
            (append
                (remove-unexpectable
                    (pddl-problem-init observable-prob)
                    :model (get-domain sim)
                    :hidden-only t
                )
                (pddl-problem-given observable-prob)
            )
        )
        (setf (pddl-problem-values observable-prob)
            (append
                (remove-unexpectable
                    (pddl-problem-values observable-prob)
                    :model (get-domain sim)
                    :hidden-only t
                )
                (pddl-problem-given-values observable-prob)
            )
        )
    )

    (when (eq observability :full)
        (setq observable-prob (copy-pddl-problem (get-initial-problem sim)))
    )
    
    (setf (pddl-problem-values observable-prob)
        (cons `((self) ,agent-id)
            (remove 'self (pddl-problem-values observable-prob) :key #'caar)
        )
    )

    (setf (pddl-problem-name observable-prob) 'observation)
    observable-prob
)


(defmethod sim-observe-state ((sim shop2-simulation) agent-id observability
                              &key &allow-other-keys
                              &aux observable-prob)
    (setq observable-prob (copy-pddl-problem (get-initial-problem sim)))

    (set-state-value (get-cur-state sim) 'self nil agent-id :provenance 'performer)
    
    (setf (pddl-problem-name observable-prob) 'observation)
    (setf (pddl-problem-init observable-prob) 
        (if (eq observability :full) 
            ;then
            (state-atoms (get-cur-state sim))
            ;else
            (remove-unexpectable
                (state-atoms (get-cur-state sim))
                :model (get-domain sim)
                :hidden-only t
            )
        )
    )
    (setf (pddl-problem-values observable-prob) 
        (if (eq observability :full)
            ;then
            (list-state-values (get-cur-state sim))
            ;else
            (remove-unexpectable 
                (list-state-values (get-cur-state sim))
                :model (get-domain sim)
                :hidden-only t
            )
        )
    )
    (setf (pddl-problem-objects observable-prob)
        (process-state-objs-to-pddl (get-cur-state sim) :unknown-types (domain-hidden-types (get-domain sim)))
    )
    observable-prob
)

(defun initialize-sensing-observability-state (sim &aux sim-prob)
    (setq sim-prob (copy-pddl-problem (get-initial-problem sim)))
    (setq sim-prob (choose-random-initial-state sim-prob))
    (setf (pddl-problem-init sim-prob)
        (remove 'invariant (pddl-problem-init sim-prob) :key #'car)
    )
    (setf (pddl-problem-init sim-prob)
        (remove 'not (pddl-problem-init sim-prob) :key #'car)
    )
    (setf (pddl-problem-init sim-prob)
        (remove 'unknown (pddl-problem-init sim-prob) :key #'car)
    )
    (setf (pddl-problem-init sim-prob)
        (remove 'oneof (pddl-problem-init sim-prob) :key #'car)
    )
    (setf (pddl-problem-init sim-prob)
        (remove 'or (pddl-problem-init sim-prob) :key #'car)
    )
    (format t "Initial state: ")
    (print-one-per-line (pddl-problem-init sim-prob))
    sim-prob
)

(defun choose-random-initial-state (problem &aux (hidden-clauses nil) (hidden-facts nil))
    (dolist (clause (pddl-problem-init problem))
        (case (car clause) 
            (unknown 
                (setq hidden-facts (append (basic-facts-in clause) hidden-facts))
            )
            (or
                (setq hidden-facts (append (basic-facts-in clause) hidden-facts))
                (setq hidden-clauses (append (dnf clause) hidden-clauses))
            )
            (oneof
                (setq hidden-facts (append (basic-facts-in clause) hidden-facts))
                (setq hidden-clauses (append (dnf clause) hidden-clauses))
            )
        )
    )
    (when (and (null hidden-facts) (null hidden-clauses))
        (return-from choose-random-initial-state problem)
    )
    (setq hidden-facts 
        (remove-duplicates 
            (remove 'not (basic-facts-in hidden-clauses) :key #'car)
            :test #'equal
        )
    )
    (setq problem (copy-pddl-problem problem))
    (setf (pddl-problem-init problem) 
        (append
            (remove '__remove
                (find-random-assignments 
                    hidden-facts
                    '__remove
                    t
                    hidden-clauses 
                    nil
                )
            )
            (pddl-problem-init problem)
        )
    )
    problem
)

(defun oneof (&rest forms &aux (found-true nil))
    (dolist (form forms)
        (when (eval form)
            ;then
            (if found-true
                ;then
                (return-from oneof nil)
                ;else
                (setq found-true t)
            )
        )
    )
    found-true
)
        

(defvar *trace-random* t)
(defvar *random-trace-depth* 0)

(defun find-random-assignments (hidden-facts assign-fact assigned-val 
                                hidden-clauses assignments 
                                &aux remaining-clauses)
    (incf *random-trace-depth*)
    (format t "~%~A. Assign ~A = ~A." *random-trace-depth* assign-fact assigned-val)
    (setq hidden-clauses (subst assigned-val assign-fact hidden-clauses :test #'equal))
    (when assigned-val
        (push assign-fact assignments)
    )
    (setq hidden-clauses (remove-if #'satisfied hidden-clauses))
    (setq remaining-clauses hidden-clauses)
    ; (format t "~%Clauses:")
    ; (qp hidden-clauses)
    (loop while remaining-clauses do
        (let ((remaining-values (remove '(not t) (remove nil (car remaining-clauses)) :test #'equal))
               val elem
             )
            (when (null remaining-values)
                ;;Clause cannot be satisfied
                (format t "~%~A. Clause failed." *random-trace-depth*)
                (decf *random-trace-depth*)
                (return-from find-random-assignments nil)
            )
            (when (null (cdr remaining-values))
                ;;Clause can only be satisfied by remaining value.
                (setq elem (car remaining-values))
                (setq val (not (and (listp elem) (eq (car elem) 'not))))
                (when (not val) (setq elem (second elem)))
                (when val
                    (push elem assignments)
                )
                (format t "~%~A. Infer ~A = ~A." *random-trace-depth* elem val)
                (setq hidden-clauses (subst val elem hidden-clauses :test #'equal))
                (setq hidden-clauses (remove-if #'satisfied hidden-clauses))
                ; (format t "~%Fixed clause: ~A" (car remaining-clauses))
                ; (format t "~%Assignment: ~A ~A" elem val)
                ; (format t "~%Clauses:")
                ; (qp hidden-clauses)
                (setq remaining-clauses hidden-clauses)
                (setq hidden-facts (remove elem hidden-facts :test #'equal))
            )
            (when (cdr remaining-values)
                (setq remaining-clauses (cdr remaining-clauses))
            )
        )
    )
;    (break "After loop.")
    (when (null hidden-facts)
        (when hidden-clauses
            (error "Clauses left!?")
        )
        (format t "~%~A. Found ~A." *random-trace-depth* assignments)
        (decf *random-trace-depth*)
        (return-from find-random-assignments assignments)
    )
    ; (when (unsatisfiable hidden-clauses) 
        ; (break "unsatisfiable: ~A" hidden-clauses) 
        ; (return-from find-random-assignments nil)
    ; )
    (let* ((rfact (nth (random (length hidden-facts)) hidden-facts))
           (other-facts (remove rfact hidden-facts)) 
           new-assigns val)
        (setq val (> (random 1.0) 0.5))
        (setq new-assigns
            (find-random-assignments
                other-facts
                rfact
                val
                hidden-clauses
                assignments
            )
        )
        (when (null new-assigns)
            (setq new-assigns
                (find-random-assignments
                    other-facts
                    rfact
                    (not val)
                    hidden-clauses
                    assignments
                )
            )
        )
        (decf *random-trace-depth*)
        new-assigns
    )
)

(defun satisfied (or-clause)
    (or (member t or-clause) (member '(not nil) or-clause :test #'equal))
)

(defmethod sim-shutdown ((sim shop2-simulation) &key &allow-other-keys)
    (setf (sim-stopped sim) t)
)

(defmethod sim-over ((sim shop2-simulation) &key &allow-other-keys)
    (setf (sim-over sim) t)
)

; (defun unsatisfiable (clause)
    ; (cond 
        ; ((eq clause t) nil)
        ; ((eq clause nil) t)
        ; (t
            ; (case (car clause)
                ; (and (cond
                        ; ((some #'unsatisfiable (cdr clause)) t)
                        ; (t nil)
                     ; )
                ; )
                ; (or (cond
                        ; ((every #'unsatisfiable (cdr clause)) t)
                        ; (t nil)
                    ; )
                ; )
                ; (oneof 
                    ; (let ((found-satisfied nil) (found-unknown nil))
                        ; (dolist (subclause (cdr clause))
                            ; (cond 
                                ; ((satisfied subclause) 
                                    ; (if found-satisfied
                                        ; ;then
                                        ; (return-from unsatisfiable t)
                                        ; ;else
                                        ; (setq found-satisfied t)
                                    ; )
                                ; )
                                ; ((and (not found-unknown)
                                      ; (not (unsatisfiable subclause))
                                 ; )
                                    ; (setq found-unknown t)
                                ; )
                            ; )
                        ; )
                        ; (and (not found-unknown) (not found-satisfied))
                    ; )
                ; )
                ; (not (cond 
                        ; ((satisfied (second clause)) t)
                        ; (t nil)
                     ; )
                ; )
                ; (t nil)
            ; )
        ; )
    ; )
; )
; 
; (defun satisfied (clause)
    ; (cond 
        ; ((eq clause t) t)
        ; ((eq clause nil) nil)
        ; (t
            ; (case (car clause)
                ; (and (cond
                        ; ((every #'satisfied (cdr clause)) t)
                        ; (t nil)
                     ; )
                ; )
                ; (or (cond
                        ; ((some #'satisfied (cdr clause)) t)
                        ; (t nil)
                    ; )
                ; )
                ; (oneof 
                    ; (let ((found-satisfied nil))
                        ; (dolist (subclause (cdr clause))
                            ; (cond 
                                ; ((satisfied subclause) 
                                    ; (if found-satisfied
                                        ; ;then
                                        ; (return-from satisfied nil)
                                        ; ;else
                                        ; (setq found-satisfied t)
                                    ; )
                                ; )
                                ; ((not (unsatisfiable subclause))
                                    ; (return-from satisfied nil)
                                ; )
                            ; )
                        ; )
                        ; found-satisfied
                    ; )
                ; )
                ; (not (cond 
                        ; ((unsatisfiable (second clause)) t)
                        ; (t nil)
                     ; )
                ; )
                ; (t nil)
            ; )
        ; )
    ; )
; )

;(defun satisfied (clause)
;    (and (null (basic-facts-in clause)) (eval (cons 'and clause)))
;)




