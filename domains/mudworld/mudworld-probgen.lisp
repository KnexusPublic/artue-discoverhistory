 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: January 14, 2011
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2009 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file generates problems in the mudworld.

(in-package :artue)

;;; Default Values for Problem-Generation Parameters
(defparameter *robot-num* 1)
(defparameter *width* 6)
(defparameter *length* 6)
(defparameter *muddy-prob* .2d0)


;;; Random Problem Generator
(defun genMudworldProb (prob-num
		       &key (robots *robot-num*)
		       (width *width*)
		       (len *length*)
		       (muddy-prob *muddy-prob*)
		       (relative-dir-name "a")
		       )
     (ensure-directories-exist 
         (make-pathname :directory (list :relative "domains" "mudworld-probs" relative-dir-name))
     )
     
     (let (init-locs dest-locs muddy-locs wp)
	  (loop for pnum from 1 to prob-num do
	       (let ((filename 
                   (make-pathname 
                       :directory (list :relative "domains" "mudworld-probs" relative-dir-name)
                       :name (format nil "mudworld-~a" pnum)
                       :type "prob"
                   )
                ))
             (setq init-locs nil)
             (setq dest-locs nil)
             (loop for o from 0 to (- robots 1) do
                 (let ((init-loc (list (random width) (random len))) 
                       (dest-loc '(0 0))
                       (dist 0))
                     (push init-loc init-locs)
                     (loop while (< dist 4) do
                         (setq dest-loc (list (random width) (random len)))
                         (setq dist 
                             (+ (abs (- (car dest-loc)
                                        (car init-loc)))
                                (abs (- (cadr dest-loc)
                                        (cadr init-loc))))
                         )
                     )
                     (push dest-loc dest-locs)
                 )
             )

             (setq muddy-locs nil)
             (setq *random-count* 0)
             (loop for x from 0 to (- width 1) do
                 (loop for y from 0 to (- len 1) do
                     (when (not (member (list x y) init-locs :test #'equal))
                         (when (< (random 1d0) muddy-prob)
                             (push (list x y) muddy-locs)
                             (incf *random-count*)
                         )
                     )
                 )
             )
		    (with-open-file (str filename :direction :output :if-does-not-exist :create :if-exists :supersede)
;			 (format str "~%(when (not (find-package :rover))~%~4T(make-package :rover)~%)")
;			 (format str "~%~%(in-package :rover)")
			 (format str "~%;;; Version: 1.0 Prob = .4")
			 (let ((footnotes nil))
                 (loop for y from (- len 1) downto 0 do
                     (format str "~%;;; ~a " y)
                     (loop for x from 0 to (- width 1) do
                         (multiple-value-bind (chr footnote)
                                (get-character x y init-locs dest-locs muddy-locs)
                             (format str "~a " chr)
                             (push footnote footnotes)
                         )
                     )
                 )
                 (format str "~%;;; ")
                 (loop for x from 0 to (- width 1) do
                     (format str "  ~a" x)
                 )
                 (dolist (footnote (remove nil footnotes))
                     (format str "~%;;; ~a" footnote)
                 )
             )

			 (format str "~%(define (problem mud-prob~a) (:domain :mudworld)" pnum)
			 (format str "~%~4T(:objects")

			 (loop for o from 0 to (- robots 1) do 
			     (format str "~%~8Trobot~a - robot" o)
             )
			 (format str "~%~4T)")
			 
			 (format str "~%~4T(:init")

             (format t "~%~%Random problem #~A" pnum)
             (format t "~%Muddy-prob: ~A" muddy-prob)
             (dolist (loc muddy-locs)
                 (format t "~%Location: ~A" loc)
                 (format str "~%~8T(muddy ~a ~a)" (first loc) (second loc))
             )
             (format t "~%Random count: ~A" *random-count*)
             
			 (format str "~%~4T)")


			 (format str "~%~%~4T(:values")
             (loop for o from 0 to (- robots 1) do 
                 (format str "~%~8T((robot-x-loc robot~a) ~a)" o (car (nth o init-locs)))
                 (format str "~%~8T((robot-y-loc robot~a) ~a)" o (cadr (nth o init-locs)))
                 (format str "~%~8T((time-taken robot~a) 0)" o)
             )
			 (format str "~%~4T)")

			 (format str "~%~%~4T(:goal (and")
			 (loop for o from 0 to (- robots 1) do 
			     (format str "~%~8T(do-quickly (arrive-at robot~a ~a ~a))" 
			         o (car (nth o dest-locs)) (cadr (nth o dest-locs))
                 )
             )
			 (format str "~%~4T))")
				   
			 (format str "~%)")
         )
        )
   )
  )
)


(defun get-character (x y init-locs dest-locs muddy-locs &aux cur-pos-list (chr nil) (footnote nil))
    (setq cur-pos-list (all-positions (list x y) init-locs))
    (cond 
        ((null cur-pos-list) nil)
        ((null (cdr cur-pos-list)) 
            (setq chr (string (character (+ (car cur-pos-list) (char-int #\0)))))
        )
        (t (setq chr "*") (setq footnote (format nil "Init location for: ~A" cur-pos-list)))
    )
    (setq cur-pos-list
        (mapcar #'(lambda (pos) (string (character (+ pos (char-int #\A))))) 
            (all-positions (list x y) dest-locs)
        )
    )
    (cond 
        ((null cur-pos-list) nil)
        ((equal chr "*")
            (setq footnote 
                (format nil "~a Destination for: ~A" footnote cur-pos-list)
            )
        )
        ((not (null chr))
            (setq footnote 
                (format nil "Init location for: ~a Destination for: ~A" 
                    chr cur-pos-list
                )
            )
        )
        ((null (cdr cur-pos-list)) (setq chr (car cur-pos-list)))
        (t (setq chr "*") 
            (setq footnote (format nil "Init location for: ~A" cur-pos-list))
        )
    )
    
    (when footnote
        (setq footnote (format nil "(~A ~A): ~a" x y footnote))
    )
    
    (if (null chr)
        ;then
        (if (member (list x y) muddy-locs :test #'equal)
            ;then
            (values "~~" nil)
            ;else
            (values "--" nil)
        )
        ;else
        (if (member (list x y) muddy-locs :test #'equal)
            ;then
            (values (format nil "~~~a" chr) footnote)
            ;else
            (values (format nil "-~a" chr) footnote)
        )
    )
)
    
(defun all-positions (item lst &optional (start 0))
    (let ((pos (position item lst :start start :test #'equal)))
        (if pos
            ;then
            (cons pos (all-positions item lst (+ pos 1)))
            ;else
            nil
        )
    )
)
