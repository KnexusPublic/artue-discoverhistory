(define (domain :MudWorld)
(:requirements :typing :fluents)
(:types robot long lat distchange time speed)
(:supertypes (speed int) (long int) (lat int) (distchange int) (time int))

(:predicates 
    (muddy ?x - long ?y - lat) 
    (moving ?r - robot ?xchange - distchange ?ychange - distchange)
    (see-mud ?x - long ?y - lat)
)

(:hidden muddy self)

(:internal)
(:static)

(:functions
    (robot-x-loc ?r - robot) - long
    (robot-y-loc ?r - robot) - lat
    (robot-speed ?r - robot) - speed
    (time-taken ?r - robot) - time
    (self) - robot
)



(:defaults
    (robot-speed 1)
    (time-taken 0)
)

(:action north
    :parameters (?r - robot) 
    :precondition 
        ()
    :effect 
        (moving ?r 0 1)
)

(:action south
    :parameters (?r - robot) 
    :precondition 
        ()
    :effect 
        (moving ?r 0 -1)
)


(:action west
    :parameters (?r - robot) 
    :precondition 
        ()
    :effect 
        (moving ?r -1 0)
)

(:action east
    :parameters (?r - robot) 
    :precondition 
        ()
    :effect 
        (moving ?r 1 0)
)

(:event changes-position
    :parameters (?r - robot)
    :precondition (and (moving ?r ?x ?y) (eq (robot-speed ?r) ?speed))
    :effect (and (increase (robot-x-loc ?r) ?x)
                 (increase (robot-y-loc ?r) ?y)
                 (increase (time-taken ?r) ?speed)
                 (not (moving ?r ?x ?y))
            )
)

(:event mud-slows-down
    :parameters (?r - robot)
    :precondition 
        (and (eq (robot-x-loc ?r) ?x)
             (eq (robot-y-loc ?r) ?y)
             (muddy ?x ?y)
             (neq (robot-speed ?r) 2d0)
        )
    :effect (and (set (robot-speed ?r) 2d0))
)


(:event no-more-mud
    :parameters (?r - robot)
    :precondition 
        (and (eq (robot-x-loc ?r) ?x)
             (eq (robot-y-loc ?r) ?y)
             (not (muddy ?x ?y))
             (neq (robot-speed ?r) 1.0d0)
        )
    :effect (and (set (robot-speed ?r) 1.0d0))
)

(:event see-mud-west
    :parameters (?r - robot)
    :precondition 
        (and (eq (robot-x-loc ?r) ?x)
             (eq (robot-y-loc ?r) ?y)
             (assign ?mudx (- ?x 1))
             (muddy ?mudx ?y)
             (not (see-mud ?mudx ?y))
        )
    :effect
        (and (see-mud ?mudx ?y))
)

(:event see-mud-east
    :parameters (?r - robot)
    :precondition 
        (and (eq (robot-x-loc ?r) ?x)
             (eq (robot-y-loc ?r) ?y)
             (assign ?mudx (+ ?x 1))
             (muddy ?mudx ?y)
             (not (see-mud ?mudx ?y))
        )
    :effect
        (and (see-mud ?mudx ?y))
)

(:event see-mud-north
    :parameters (?r - robot)
    :precondition 
        (and (eq (robot-x-loc ?r) ?x)
             (eq (robot-y-loc ?r) ?y)
             (assign ?mudy (+ ?y 1))
             (muddy ?x ?mudy)
             (not (see-mud ?x ?mudy))
        )
    :effect
        (and (see-mud ?x ?mudy))
)

(:event see-mud-south
    :parameters (?r - robot)
    :precondition 
        (and (eq (robot-x-loc ?r) ?x)
             (eq (robot-y-loc ?r) ?y)
             (assign ?mudy (- ?y 1))
             (muddy ?x ?mudy)
             (not (see-mud ?x ?mudy))
        )
    :effect
        (and (see-mud ?x ?mudy))
)
)

