
;(when (not (find-package :rover))
;    (make-package :rover)
;)

;(in-package :rover)

(define (domain :rovers-compass)
(:requirements :typing :fluents)
(:types rover waypoint store camera mode lander objective dir)

(:constants NOWHERE - waypoint
            NODIR - dir)

(:predicates 
    (visible ?w1 - waypoint ?w2 - waypoint)
    (blocked ?r - rover)
    (sand-pit-at ?w - waypoint)
    (dug-out ?r - rover ?w - waypoint)
    (sandy ?w - waypoint)
    (windy ?w - waypoint)
    (in-sun ?w - waypoint)
    (detects-location ?r - rover ?w - waypoint)
    (sensors-function ?r - rover)
    (sand-covered ?r - rover)
    (recharging ?r - rover)
    (digging ?r - rover)
    (attempting-move ?r - rover ?d - dir)
    (compass-moved ?r - rover)
    (finished ?r - rover ?w - waypoint)
    (failed ?r - rover)
    (succeeded ?r - rover ?w - waypoint)
)


(:hidden sand-pit-at sandy windy compass-moved compass-points at succeeded failed)

(:functions 
    (energy ?r - rover) - int 
    (recharges) - int 
    (self) - rover
    (next-compass-point ?dir1 - dir) - dir
    (compass-points ?r - rover) - dir
    (at ?r - rover) - waypoint 
    (can-traverse ?w - waypoint ?d - dir) - waypoint
    (real-direction ?assumedNorth - dir ?attemptedDir - dir) - dir
    (latitude ?w - waypoint) - int 
    (longitude ?w - waypoint) - int 
)

(:defaults
    (can-traverse NOWHERE)
    (real-direction NODIR)
)

(:action navigate
    :parameters (?r - rover ?dir - dir) 
    :precondition 
        (>= (energy ?r) 8)
    :effect 
        (and (attempting-move ?r ?dir)
             (decrease (energy ?r) 8)
        )
    :cost 12.0
)

(:event rover-moves
    :parameters (?r - rover)
    :precondition 
        (and (attempting-move ?r ?dir) 
             (not (blocked ?r))
             (eq (at ?r) ?src) 
             (eq (compass-points ?r) ?newNorth)
             (eq (real-direction ?dir ?newNorth) ?realDir)
             (visible ?src ?dest) 
             (eq (can-traverse ?src ?realDir) ?dest)
         )
     :effect 
        (and (set (at ?r) ?dest) 
             (not (attempting-move ?r ?dir))
             (not (compass-moved ?r))
        )
)

(:event rover-nowhere-to-go
    :parameters (?r - rover)
    :precondition 
        (and (attempting-move ?r ?dir) 
             (not (blocked ?r))
             (eq (at ?r) ?src) 
             (eq (compass-points ?r) ?newNorth)
             (eq (real-direction ?dir ?newNorth) ?realDir)
             (eq (can-traverse ?src ?realDir) NOWHERE)
         )
     :effect 
        (and (not (attempting-move ?r ?dir))
             (not (compass-moved ?r))
        )
)

(:event rover-move-fails
    :parameters (?r - rover)
    :precondition 
        (and (attempting-move ?r ?dir) 
             (blocked ?r)
         )
     :effect 
        (and (not (attempting-move ?r ?dir))
             (not (compass-moved ?r))
        )
)

(:event rover-gets-sandy
    :parameters (?r - rover)
    :precondition (and (eq (at ?r) ?w) (sandy ?w) (not (sand-covered ?r)))
    :effect (and (sand-covered ?r) (not (sensors-function ?r)))
)

(:event rover-cleaned
    :parameters (?r - rover)
    :precondition (and (eq (at ?r) ?w) (not (sandy ?w)) (windy ?w) (sand-covered ?r))
    :effect (and (not (sand-covered ?r)) (detects-location ?r ?w) (sensors-function ?r))
)

(:event location-detected
    :parameters (?r - rover)
    :precondition 
        (and (eq (at ?r) ?w) (not (sandy ?w)) 
             (not (sand-covered ?r)) (not (detects-location ?r ?w)))
    :effect (and (detects-location ?r ?w) (sensors-function ?r))
)

(:event location-left
    :parameters (?r - rover)
    :precondition 
        (and (detects-location ?r ?w) (neq (at ?r) ?w))
    :effect (and (not (detects-location ?r ?w)))
)

(:event compass-confused
    :parameters (?r - rover)
    :precondition (and (eq (at ?r) ?w) (windy ?w) (eq (compass-points ?r) ?oldNorth)
                       (eq (next-compass-point ?oldNorth) ?newNorth)
                       (not (compass-moved ?r))
                  )
    :effect (and (compass-moved ?r) 
                 (set (compass-points ?r) ?newNorth)
            )
)

(:event becomes-trapped-in-pit
    :parameters (?r - rover)
    :precondition 
        (and (eq (at ?r) ?w) 
             (sand-pit-at ?w) 
             (not (blocked ?r)) 
             (not (dug-out ?r ?w))
        )
    :effect (and (blocked ?r))
)

(:action dig-out
    :parameters (?r - rover)
    :precondition 
        (and )
    :effect
        (and (decrease (energy ?r) 50) (digging ?r) )
)

(:event dig-out-event
    :parameters (?r - rover)
    :precondition 
        (and (digging ?r) (eq (at ?r) ?w))
    :effect
        (and (not (digging ?r)) (dug-out ?r ?w) (not (blocked ?r)))
)

(:event left-sand-pit
    :parameters (?r - rover)
    :precondition (and (dug-out ?r ?w) (neq (at ?r) ?w))
    :effect (and (not (dug-out ?r ?w)))
)


(:action recharge
    :parameters (?r - rover)
    :precondition (and (not (sand-covered ?r)))
    :effect (and (recharging ?r)) 
)

(:event recharge-event
    :parameters (?r - rover)
    :precondition (and (recharging ?r) (eq (at ?r) ?w) (in-sun ?w))
    :effect (and (set (energy ?r) 100.0d0) (increase (recharges) 1) (not (recharging ?r)))
)

(:event no-recharge-event
    :parameters (?r - rover)
    :precondition (and (recharging ?r) (eq (at ?r) ?w) (not (in-sun ?w)))
    :effect (and (not (recharging ?r)))
)

(:action finish
    :parameters (?r - rover ?w - waypoint)
    :precondition ()
    :effect (and (finished ?r ?w))
)

(:event success
    :parameters (?r - rover ?w - waypoint)
    :precondition (and (finished ?r ?w) (eq (at ?r) ?w))
    :effect (and (succeeded ?r ?w) (not (failed ?r)) (not (finished ?r ?w)))
)

(:event failure
    :parameters (?r - rover ?w - waypoint)
    :precondition (and (finished ?r ?w) (neq (at ?r) ?w))
    :effect (and (failed ?r) (not (finished ?r ?w)))
)

)
