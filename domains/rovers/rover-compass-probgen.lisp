 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: January 2018
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file generates problems for the Mars Rovers domain with 
 ;;;; surprises.

(in-package :artue)

;;; Default Values for Problem-Generation Parameters
(defparameter *rover-num* 3)
(defparameter *waypoint-num* 36)
(defparameter *width* 6)
(defparameter *length* 6)
; (defparameter *sandy-loc-num* 3)
; (defparameter *windy-loc-num* 3)
; (defparameter *sunny-loc-num* 3)
(defparameter *windy-prob* .3d0)
(defparameter *sandy-prob* .3d0)
(defparameter *sunny-prob* .3d0)
(defparameter *pit-prob* .3d0)
(defparameter *numeric* t)

;;; Random Problem Generator
(defun genRoverProb (prob-num  
		       &key (rovers *rover-num*)
		       (waypoints *waypoint-num*)
		       (width *width*)
		       (len *length*)
		       ; (sandy-loc-num *sandy-loc-num*)
		       ; (sunny-loc-num *sunny-loc-num*)
		       ; (windy-loc-num *windy-loc-num*)
		       ; (pit-num *windy-loc-num*)
		       (windy-prob *windy-prob*)
		       (sandy-prob *sandy-prob*)
		       (sunny-prob *sunny-prob*)
		       (pit-prob *pit-prob*)
		       (numeric *numeric*)
		       (relative-dir-name "a")
		       )
     (ensure-directories-exist 
         (make-pathname :directory (list :relative "domains" "rovers" relative-dir-name))
     )
     
     (let (init-locs dest-locs wp (init-rand (make-random-state *random-state*)))
	  (loop for pnum from 1 to prob-num do
	       (let ((filename 
                   (make-pathname 
                       :directory (list :relative "domains" "rovers" relative-dir-name)
                       :name (format nil "pfile-gen~a" pnum)
                       :type "prob"
                   )
                ))
             (setq init-locs nil)
             (setq dest-locs nil)
             (loop for o from 1 to rovers do
                 (let ((init-loc (+ 1 (random waypoints))) 
                       (dest-loc 0)
                       (dist 0))
                     (push init-loc init-locs)
                     (loop while (< dist 4) do
                         (setq dest-loc (+ 1 (random waypoints)))
                         (setq dist 
                             (+ (abs (- (rem (- init-loc 1) width)
                                        (rem (- dest-loc 1) width)))
                                (abs (- (floor (- init-loc 1) width)
                                        (floor (- dest-loc 1) width)))))
                     )
                     (push dest-loc dest-locs)
                 )
             )
		    (with-open-file (str filename :direction :output :if-does-not-exist :create :if-exists :supersede)
;			 (format str "~%(when (not (find-package :rover))~%~4T(make-package :rover)~%)")
;			 (format str "~%~%(in-package :rover)")
			 (format str "~%;;; Version: 11.0")
			 (format str "~%;;; Windy probability:  ~a" *windy-prob*)
			 (format str "~%;;; Sandy probability:  ~a" *sandy-prob*)
			 (format str "~%;;; Pit probability:    ~a" *pit-prob*)
			 (format str "~%;;; In-sun probability: ~a" *sunny-prob*)
			 (format str "~%;;; Random state: ~a" init-rand)
			 (format str "~%(define (problem rovers-compass-~a) (:domain :rovers-compass)" pnum)
			 (format str "~%~4T(:objects")

			 (loop for o from 1 to rovers do 
			     (format str "~%~8Trover~a - rover" o)
             )

			 (loop for o from 1 to waypoints do 
			     (format str "~%~8Twaypoint~a - waypoint" o)
             )

             (when (not numeric)
                 (loop for o from 0 to 100 do 
                     (format str "~%~8Tl~a - level" o)
                 )
             )

             (format str "~%~8Tnorth south east west - dir")
			 
			 (format str "~%~4T)")
			 
			 (format str "~%~4T(:init")

			 (loop for o from 1 to waypoints do 
			     (let ((lat (+ 1 (rem (- o 1) width))) 
			           (long (+ 1 (floor (- o 1) width)))
			          )
                     (when (> long 1)
                         (format str "~%~8T(visible waypoint~a waypoint~a)" o (- o width))
                     )
                     (when (< lat width)
                         (format str "~%~8T(visible waypoint~a waypoint~a)" o (+ o 1))
                     )
                     (when (< long len)
                         (format str "~%~8T(visible waypoint~a waypoint~a)" o (+ o width))
                     )
                     (when (> lat 1)
                         (format str "~%~8T(visible waypoint~a waypoint~a)" o (- o 1))
                     )
                 )
             )
             (when (not numeric)
                 (loop for o from 1 to rovers do 
                     (format str "~%~8T(energy rover~a l100)" o)
                 )
                 (loop for o from 1 to 100 do 
                     (format str "~%~8T(level-decreases-by-8 l~a l~a)" o (max 0 (- o 8)))
                 )
                 (loop for o from 1 to 100 do 
                     (format str "~%~8T(level-decreases-by-50 l~a l~a)" o (max 0 (- o 50)))
                 )
             )

             (format t "~%~%Random problem #~A" pnum)
             (format t "~%Sandy-prob: ~A" sandy-prob)
             (format t "~%Windy-prob: ~A" windy-prob)
             (format t "~%Sunny-prob: ~A" sunny-prob)
             (format t "~%Pit-prob: ~A" pit-prob)
             (setq *random-count* 0)
             (loop for o from 1 to waypoints do
                 (when (not (member o init-locs))
                     (format t "~%Waypoint: ~A" o)
                     (when (< (random 1d0) sandy-prob)
                         (format str "~%~8T(sandy waypoint~a)" o)
                         (incf *random-count*)
                     )
                     (when (< (random 1d0) windy-prob)
                         (format str "~%~8T(windy waypoint~a)" o)
                         (incf *random-count*)
                     )
                     (when (< (random 1d0) sunny-prob)
                         (format str "~%~8T(in-sun waypoint~a)" o)
                         (incf *random-count*)
                     )
                     (when (< (random 1d0) pit-prob)
                         (format str "~%~8T(sand-pit-at waypoint~a)" o)
                         (incf *random-count*)
                     )
                 )
             )
             (format t "~%Random count: ~A" *random-count*)
             
			 (loop for o from 1 to rovers do 
			     (format str "~%~8T(detects-location rover~a waypoint~a)" o (nth (- o 1) init-locs))
			     (format str "~%~8T(sensors-function rover~a)" o)
             )
		    
			 ; (loop for o from 1 to rovers do 
			     ; (format str "~%~8T(destination rover~a waypoint~a)" o (nth (- o 1) dest-locs))
             ; )
			 (format str "~%~4T)")
			 
			 
			 (format str "~%~%~4T(:values")
             (when numeric
                 (loop for o from 1 to rovers do 
                     (format str "~%~8T((at rover~a) waypoint~a)" o (nth (- o 1) init-locs))
                     (format str "~%~8T((energy rover~a) 100)" o)
                     (format str "~%~8T((compass-points rover~a) north)" o)
                 )
             )
             (format str "~%~8T
        ((next-compass-point north) west)
        ((next-compass-point west) south)
        ((next-compass-point south) east)
        ((next-compass-point east) north)
        ((real-direction north north) north)
        ((real-direction north east) east)
        ((real-direction north south) south)
        ((real-direction north west) west)
        ((real-direction south north) south)
        ((real-direction south east) west)
        ((real-direction south south) north)
        ((real-direction south west) east)
        ((real-direction east north) east)
        ((real-direction east east) south)
        ((real-direction east south) west)
        ((real-direction east west) north)
        ((real-direction west north) west)
        ((real-direction west east) north)
        ((real-direction west south) east)
        ((real-direction west west) south)"
             )
             (loop for o from 1 to waypoints do
                 (output-traverses str 'north o (- o width) rovers waypoints)
                 (output-traverses str 'east o (+ o 1) rovers waypoints)
                 (output-traverses str 'south o (+ o width) rovers waypoints)
                 (output-traverses str 'west o (- o 1) rovers waypoints)
			     (let ((lat (+ 1 (rem (- o 1) width))) 
			           (long (+ 1 (floor (- o 1) width)))
			          )
			         (when numeric
			             (format str "~%~%~%~8T((latitude waypoint~a) ~a)" o lat)
			             (format str "~%~8T((longitude waypoint~a) ~a)" o long)
                     )
                     (when (not numeric)
			             (format str "~%~%~%~8T((latitude waypoint~a) l~a)" o lat)
			             (format str "~%~8T((longitude waypoint~a) l~a)" o long)
                     )
                 )
             )
             (format str "~%~8T((recharges) 0)")
			 (format str "~%~4T)")
			 

			 (format str "~%~%~4T(:given-values")
			 (loop for o from 1 to rovers do 
                 (format str "~%~8T((at rover~a) waypoint~a)" o (nth (- o 1) init-locs))
			     (format str "~%~8T((compass-points rover~a) north)" o)
             )
			 (format str "~%~4T)")

			 (format str "~%~%~4T(:goal (and")
			 (loop for o from 1 to rovers do 
			     (format str "~%~8T(succeeded rover~a waypoint~a)" o (nth (- o 1) dest-locs))
;			     (format str "~%~8T(at rover~a waypoint~a)" o (nth (- o 1) dest-locs))
;			     (format str "~%~8T(succeeded rover~a)" o)
             )
			 (format str "~%~4T))")
				   
			 (format str "~%)")
         )
        )
   )
  )
)
			 

(defun random-except (upper-limit exceptions &aux choice)
    (setq choice (+ 1 (random upper-limit)))
    (loop while (member choice exceptions) do
        (setq choice (+ 1 (random upper-limit)))
    )
    choice
)

(defun output-traverses (str dir point1 point2 rovers waypoints &aux (wrap nil))
    (when (and (eq dir 'west) (eq (mod point2 6) 0))
        (setq wrap t)
    )
    (when (and (eq dir 'east) (eq (mod point1 6) 0))
        (setq wrap t)
    )
    (if (and (< 0 point2) (<= point2 waypoints) (not wrap))
        ;then
        (format str "~%~8T((can-traverse waypoint~a ~a) waypoint~a)" point1 dir point2)
        ;else
        (format str "~%~8T((can-traverse waypoint~a ~a) nowhere)" point1 dir)
    )
)
