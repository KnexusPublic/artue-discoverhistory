(define (domain :satellite-events)
  (:requirements :typing :fluents :equality)
  (:types satellite direction instrument mode fuel-level capacity)
  (:constants NO-DIRECTION - direction NO-FUEL - fuel-level EMPTY - capacity)
  (:predicates 

       (supports ?i - instrument ?m - mode)
       (power-avail ?s - satellite)
       (power-on ?i - instrument)

       (calibrated ?i - instrument)
       (calibrating ?s - satellite ?i - instrument)

       (have-image ?s - satellite ?d - direction ?m - mode)
       (taking-image ?s - satellite ?i - instrument ?m - mode)

       (transmitting ?s - satellite)
       (transmitted-image ?d - direction ?m - mode)

       (late-to-turn ?s - satellite ?d - direction)
       (changing-direction ?s - satellite ?d - direction)

       (electro-magnetic-field ?d - direction)
       (fuel-leaking ?s - satellite)

       (supernova-exploded ?d - direction)
       (broken ?i - instrument)
       (shuttered ?i - instrument)

       (finished ?d - direction ?m - mode)
       (test-success ?d - direction ?m - mode)
       (succeeded ?d - direction ?m - mode)
       (failed ?d - direction ?m - mode)
  )
 
  (:hidden electro-magnetic-field 
           supernova-exploded 
           late-to-turn
           last-direction-change 
           transmitted-image
           fuel-leaking 
           have-image
  )

  (:functions 
      (pointing ?s - satellite) - direction
      (last-direction-change ?s - satellite) - direction

      (on-board ?i - instrument) - satellite
      (calibration-target ?i - instrument) - direction

      (fuel-level ?s - satellite) - fuel-level
      (next-level ?level - fuel-level) - fuel-level
      (data-capacity ?s - satellite) - capacity
      (next-capacity ?current-capacity - capacity) - capacity
      
      (data-center) - direction
      (self) - object
  )
  
  (:defaults
      (last-direction-change NO-DIRECTION)
      (next-level NO-FUEL)
      (next-capacity EMPTY)
  )

  (:assumption-likelihood
      (have-image none)
  )
  
  (:action turn-to
   :parameters (?s - satellite ?d-new - direction)
   :precondition (and (eq (fuel-level ?s) ?level) 
                      (eq (next-level ?level) ?new-level)
                      (neq (fuel-level ?s) NO-FUEL))
   :effect (and  (set (fuel-level ?s) ?new-level)
                 (changing-direction ?s ?d-new)
           )
  )
 
  (:action switch-on
   :parameters (?i - instrument ?s - satellite)
   :precondition (and (eq (on-board ?i) ?s) (power-avail ?s))
   :effect (and (power-on ?i) 
                (not (calibrated ?i))
                (not (power-avail ?s))
           )
  )

  (:action switch-off
   :parameters (?i - instrument ?s - satellite)
   :precondition (and (eq (on-board ?i) ?s) (power-on ?i))
   :effect (and (not (power-on ?i)) 
                (power-avail ?s)
                (not (calibrated ?i))
           )
  )

  (:action calibrate
   :parameters (?s - satellite ?i - instrument)
   :precondition (and (eq (on-board ?i) ?s)
                      (power-on ?i))
   :effect (and (calibrating ?s ?i)))

  (:action take-image
   :parameters (?s - satellite ?m - mode)
   :precondition (and (calibrated ?i) (eq (on-board ?i) ?s) 
                      (supports ?i ?m) (power-on ?i) 
                      (eq (data-capacity ?s) ?current-capacity)
                      (eq (next-capacity ?current-capacity) ?next-capacity)
                      (not (broken ?i)))
   :effect (and (set (data-capacity ?s) ?next-capacity) (taking-image ?s ?i ?m)))
                
  (:action transmit-images
   :parameters (?s - satellite)
   :precondition (and )
   :effect (and (transmitting ?s)))

  (:action shutter
   :parameters (?i - instrument)
   :precondition (and (eq (on-board ?i) ?s)
                      (eq (fuel-level ?s) ?level) 
                      (eq (next-level ?level) ?new-level)
                      (neq (fuel-level ?s) NO-FUEL)) 
   :effect (and (set (fuel-level ?s) ?new-level)
                (shuttered ?i)))
   
  (:action finish
   :parameters (?d - direction ?m - mode)
   :precondition ()
   :effect (and (finished ?d ?m) (test-success ?d ?m))
  )
  
  (:event success
   :parameters (?d - direction ?m - mode)
   :precondition (and (test-success ?d ?m) (transmitted-image ?d ?m))
   :effect (and (succeeded ?d ?m) (not (test-success ?d ?m)))
  )
                
  (:event failure
   :parameters (?d - direction ?m - mode)
   :precondition (and (test-success ?d ?m) (not (transmitted-image ?d ?m)))
   :effect (and (failed ?d ?m) (not (test-success ?d ?m)))
  )

  (:event change-direction-on-time
   :parameters (?s - satellite)
   :precondition (and (eq (pointing ?s) ?d) (changing-direction ?s ?d-new)
                      (neq (pointing ?s) ?d-new)
                      (eq (last-direction-change ?s) NO-DIRECTION))
   :effect (and (set (pointing ?s) ?d-new)
                (not (changing-direction ?s ?d-new))))
                
  (:event keep-direction
   :parameters (?s - satellite)
   :precondition (and (eq (pointing ?s) ?d) (changing-direction ?s ?d))
   :effect (and (not (changing-direction ?s ?d))))
  
  (:event change-direction-late
   :parameters (?s - satellite)
   :precondition (and (eq (pointing ?s) ?d)
                      (changing-direction ?s ?d-new)
                      (neq (last-direction-change ?s) NO-DIRECTION)
                      (eq (last-direction-change ?s) ?d-old)
                      (neq (last-direction-change ?s) ?d-new)
                      (neq (pointing ?s) ?d-old))
   :effect (and (set (pointing ?s) ?d-old)
                (set (last-direction-change ?s) ?d-new)
                (not (changing-direction ?s ?d-new))
           )
  )
   
  (:event keep-direction-late
   :parameters (?s - satellite)
   :precondition (and (eq (pointing ?s) ?d)
                      (changing-direction ?s ?d-new)
                      (eq (last-direction-change ?s) ?d)
                      (neq (last-direction-change ?s) ?d-new))
   :effect (and (set (last-direction-change ?s) ?d-new)
                (not (changing-direction ?s ?d-new))
           )
  )
   
  (:event keep-direction-late2
   :parameters (?s - satellite)
   :precondition (and (eq (pointing ?s) ?d)
                      (changing-direction ?s ?d-old)
                      (eq (last-direction-change ?s) ?d-old)
                      (neq (pointing ?s) ?d-old))
   :effect (and (set (pointing ?s) ?d-old)
                (not (changing-direction ?s ?d-old))
           )
  )
   
  (:event keep-direction-late3
   :parameters (?s - satellite)
   :precondition (and (eq (pointing ?s) ?d)
                      (changing-direction ?s ?d)
                      (eq (last-direction-change ?s) ?d))
   :effect (and (not (changing-direction ?s ?d)))
  )
                
  (:event change-direction-fail
   :parameters (?s - satellite)
   :precondition (and (eq (pointing ?s) ?d) (late-to-turn ?s ?d))
   :effect (and (set (last-direction-change ?s) ?d) 
                (not (late-to-turn ?s ?d))
           )
  )
                
  (:event acquire-image
   :parameters (?s - satellite)
   :precondition (and (taking-image ?s ?i ?m) (eq (pointing ?s) ?d)
                      (not (supernova-exploded ?d)))
   :effect (and (have-image ?s ?d ?m) (not (taking-image ?s ?i ?m)))
  )
                
  (:event acquire-shuttered-image
   :parameters (?s - satellite)
   :precondition (and (taking-image ?s ?i ?m) (eq (pointing ?s) ?d)
                      (supernova-exploded ?d) (shuttered ?i))
   :effect (and (have-image ?s ?d ?m) (not (taking-image ?s ?i ?m)))
  )
                
  (:event image-fail
   :parameters (?i - instrument ?d - direction)
   :precondition (and (eq (pointing ?s) ?d) (supernova-exploded ?d) (taking-image ?s ?i ?m) 
                      (not (shuttered ?i)))
   :effect (and (not (taking-image ?s ?i ?m)) (broken ?i))
  )

  (:event acquire-calibration
   :parameters (?s - satellite)
   :precondition (and (calibrating ?s ?i) (eq (pointing ?s) ?d) 
                      (eq (calibration-target ?i) ?d))
   :effect (and (calibrated ?i) (not (calibrating ?s ?i)))
  )
    
  (:event fail-calibration
   :parameters (?s - satellite)
   :precondition (and (calibrating ?s ?i) (eq (pointing ?s) ?d) 
                      (neq (calibration-target ?i) ?d))
   :effect (and (not (calibrated ?i)) (not (calibrating ?s ?i)))
  )

  (:event increase-capacity
   :parameters (?s - satellite)
   :precondition (and (transmitting ?s) (eq (data-capacity ?s) ?cap))
   :effect (and (set (data-capacity ?s) cap15)
                (not (transmitting ?s))))

  (:event destroy-images
   :parameters (?s - satellite ?dp - direction ?m - mode)
   :precondition (and (transmitting ?s) (have-image ?s ?dp ?m)) 
   :effect (and (not (have-image ?s ?dp ?m)))
  )

  (:event complete-transmission
   :parameters (?s - satellite ?dp - direction ?m - mode)
   :precondition (and (transmitting ?s) (have-image ?s ?dp ?m) 
                      (eq (pointing ?s) ?center) (eq (data-center) ?center))
   :effect (and (transmitted-image ?dp ?m))
  )

  (:event fuel-leak-starts
   :parameters (?s - satellite ?level - fuel-level ?new-level - fuel-level)
   :precondition (and (eq (fuel-level ?s) ?level)
                      (eq (next-level ?level) ?new-level)
                      (not (fuel-leaking ?s))
                      (eq (pointing ?s) ?d) (electro-magnetic-field ?d))
   :effect (and (set (fuel-level ?s) ?new-level)
                (fuel-leaking ?s)
           )
  )

  (:event fuel-leak-continues
   :parameters (?s - satellite ?level - fuel-level ?new-level - fuel-level)
   :precondition (and (changing-direction ?s ?any)
                      (eq (fuel-level ?s) ?level)
                      (eq (next-level ?level) ?new-level)
                      (fuel-leaking ?s))
   :effect (and (set (fuel-level ?s) ?new-level)))

  (:event power-offline
   :parameters (?s - satellite ?i - instrument)
   :precondition (and (eq (fuel-level ?s) NO-FUEL) (power-avail ?s))
   :effect (and (not (power-avail ?s)))
  )

  (:event power-off-instrument
   :parameters (?s - satellite ?i - instrument)
   :precondition (and (eq (fuel-level ?s) NO-FUEL) (eq (on-board ?i) ?s) (power-on ?i))
   :effect (and (not (power-on ?i)))
  )

  (:event supernova-explosion
   :parameters (?i - instrument ?d - direction)
   :precondition (and (eq (calibration-target ?i) ?d) 
                      (calibrated ?i) (not (broken ?i))
                      (supernova-exploded ?d))
   :effect (and (broken ?i))
  )
)

