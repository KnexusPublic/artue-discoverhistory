;; This is a knowledge-intensive HTN designed to greedily satisfy image goals 
;; without backtracking. May not achieve the best solution (defined as getting
;; as many requested images as possible), but finishes quickly.

(define (htn :satellite-events-htn)
(:domain :satellite-events)
(:domain-file 
     :DIRECTORY (:RELATIVE "domains" "satellite") 
     :NAME "satellite-events" :TYPE "pddl")

     
;; Internal state predicate scheduled-image has two internal actions
;; that set and remove it. It's used to allow the HTN knowledge to reason 
;; about what goals remain to be addressed.
(:predicates 
    (scheduled-image ?d - direction ?m - mode)
)
     
(:internal scheduled-image)
     
(:action internal-action-schedule-image
 :parameters (?d - direction ?m - mode)
 :effect (scheduled-image ?d ?m)
)
     
(:action internal-action-unschedule-image
 :parameters (?d - direction ?m - mode)
 :effect (not (scheduled-image ?d ?m))
)

;; The "achieve" task converts a set of state literals into a set of tasks to 
;; be achieved. It handles the conversion, essentially, between goals and tasks
;; that is sometimes necessary.

;; The "there-is-no-try" method requires every goal to succeed.
(:method (achieve ?goals)
    there-is-no-try
    (
    )
    (
        (do-achieve ?goals)
        (perform-schedule)
;        (search-for-satisficing-schedule ?goals 3)
    )
)

;; The "there-is-a-try" method allows goals to fail. Only attempted when the 
;; prior method is unsuccessful.
;(:method (achieve ?goals)
;    there-is-a-try
;    (
;    )
;    (
;        (try-to-achieve ?goals)
;    )
;)
              


;; The task try-to-achieve attempts a set of goals one at a time, and abandons
;; a goal if it must to achieve the others.
(:method (try-to-achieve ?goals)
    abandon-first
    (
        (assign ?first-goal (car '?goals))
        (assign ?rest-of-goals (cdr '?goals))
    )
    (
        (achieve ?rest-of-goals)
    )
)

(:method (try-to-achieve ?goals)
    try-all
    (
        (assign ?first-goal (car '?goals))
        (assign ?rest-of-goals (cdr '?goals))
    )
    (
        (shop2::!!inop)
        ?first-goal
        (achieve ?rest-of-goals)
    )
)
              
(:method (try-to-achieve ?goals)
    abandon-first-for-rest
    (
        (assign ?rest-of-goals (cdr '?goals))
    )
    (
        (do-achieve ?rest-of-goals)
    )
)

;; The task do-achieve attempts a set of goals one at a time; it fails if 
;; any goal cannot be achieved.
(:method (do-achieve ?goals)
    done
    (
        (eval (null '?goals))
    )
    (
        ;;rest on laurels
    )
    do-all
    (
        (assign ?first-goal (car '?goals))
        (assign ?rest-of-goals (cdr '?goals))
    )
    (
        (shop2::!!inop)
        ?first-goal
        (do-achieve ?rest-of-goals)
    )
)

;; The task search-for-satisficing-schedule tries multiple greedy searches via 
;; the "perform-schedule" method, looking for one that achieves all goals.
;; The last greedy solution is accepted if none achieves all goals.
(:method (search-for-satisficing-schedule ?goals ?tries-left)
    last-try
    (
        (<= ?tries-left 1)
        (assign ?new-goals (random-subset (copy-list '?goals) (length '?goals)))
    )
    (
        (do-achieve ?new-goals)
        (perform-schedule)
    )
    look
    (
        (assign ?new-goals (random-subset (copy-list '?goals) (length '?goals)))
    )
    (
        (do-achieve ?new-goals)
        (perform-schedule)
        (do-achieve ?new-goals)
        (test-for-no-images-left)
    )
)

(:method (search-for-satisficing-schedule ?tries-left)
    try-again
    (
        (assign ?new-tries-left (- ?tries-left 1))
    )
    (
        (search-for-best-schedule ?new-tries-left)
    )
)

(:method (test-for-no-images-left)
    ((not (scheduled-image ?d ?m)))
    ()
    ;; If an image is still scheduled, fails
)


;; This method corresponds to a goal; instead of directly trying to accomplish 
;; the goal, the goal is added to the state as a "scheduled image", which must 
;; be taken before planning completes.
(:method (succeeded ?d ?m)
    ((not (scheduled-image ?d ?m)) (not (finished ?d ?m)))
    ((!internal-action-schedule-image ?d ?m))
    ()
    ()
)

;; The task "perform-schedule" is responsible for ensuring that all possible 
;; image goals are met. Called recursively until images are dealt with.

;; This is the base case; all images have been dealt with.
(:method (perform-schedule)
    out-of-images
    ((not (scheduled-image ?d ?m)))
    ()
)

;; If a goal image has been transmitted, we're finished with it, we register the 
;; image as dealt with by unscheduling it, and repeat.
(:method (perform-schedule)
    finished-remove-from-schedule
    ((scheduled-image ?d ?m)
     (transmitted-image ?d ?m))
    ((!finish ?d ?m)
     (!internal-action-unschedule-image ?d ?m)
     (perform-schedule)
    )
)

;; If an image can no longer be taken, because all capable instruments are 
;; broken, it's descheduled. Then perform-schedule repeats.
(:method (perform-schedule)
    remove-impossible
    ((scheduled-image ?d ?m)
     (not (have-image ?s ?d ?m))
     (not (transmitted-image ?d ?m))
     (forall (?i) (eq (on-board ?i) ?s)
        (or (broken ?i)
            (not (supports ?i ?m))
        )
     )
    )
    ((!internal-action-unschedule-image ?d ?m)
     (perform-schedule)
    )
)

;; This method is responsible for actually acquiring an individual image. To be
;; successful, a satellite must have enough fuel remaining after taking the 
;; image to transmit it as well. Perform-schedule repeats.
(:method (perform-schedule)
    choose-image
    ((scheduled-image ?d ?m)
     (not (have-image ?s ?d ?m))
     (not (transmitted-image ?d ?m))
    )
    ((do-image ?d ?m)
     (test-can-transmit-image ?d ?m)
     (perform-schedule)
    )
)

;; This method is responsible for transmitting images to base. In order to 
;; make this occur in batches, the transmit method is performed only after no 
;; additional images can be taken to transmit. Perform-schedule repeats.
(:method (perform-schedule)
    transmit-when-needed
    ((have-image ?s ?d ?m))
    ((submit-data ?s)
     (perform-schedule)
    )
)

;; This method is responsible for re-acquiring an individual image when the 
;; original satellite can't transmit it. Perform-schedule repeats.
(:method (perform-schedule)
    choose-image
    ((scheduled-image ?d ?m)
     (not (transmitted-image ?d ?m))
    )
    ((do-image ?d ?m)
     (test-can-transmit-image ?d ?m)
     (perform-schedule)
    )
)


;; This method allows a scheduled image to be removed from the schedule. This 
;; method acknowledges failure, and so is called only if no images can be 
;; taken or transmitted. Perform-schedule repeats.
(:method (perform-schedule)
    allow-failure
    ((scheduled-image ?d ?m))
    ((!internal-action-unschedule-image ?d ?m)
     (perform-schedule)
    )
)



;(:method (do-image ?d ?m)
;     ;;; select a satellite that does not leak and will not start
;     avoid-leak
;     ((eq (on-board ?i) ?s) 
;      (supports ?i ?m) 
;      (not (fuel-leaking ?s))
;      (not (electro-magnetic-field ?d))
 ;    )
 ;    ((take-image-with-satellite ?s ?d ?m))
;)

;(:method (do-image ?d ?m)
;     ;;; select a leaking satellite
;     require-leaky
;     ((eq (on-board ?i) ?s) 
;      (supports ?i ?m) 
;      (fuel-leaking ?s)
;      (electro-magnetic-field ?d)
;     )
;     ((take-image-with-satellite ?s ?d ?m))
;)


;(:method (do-image ?d ?m)
;     ;;; select a satellite that will start to leak
;     allow-leak
;     ((eq (on-board ?i) ?s) 
;      (supports ?i ?m) 
;      (fuel-leaking ?s)
;      (not (electro-magnetic-field ?d))
;     )
;     ((take-image-with-satellite ?s ?d ?m))
;)

;(:method (do-image ?d ?m)
;     ;;; select a satellite that will start to leak
;     allow-leak2
;     ((eq (on-board ?i) ?s) 
;      (supports ?i ?m) 
;      (not (fuel-leaking ?s))
;      (electro-magnetic-field ?d)
;     )
;     ((take-image-with-satellite ?s ?d ?m))
;)

;; The task do-image is responsible for actually acquiring a goal image (but 
;; not transmitting it). This includes handling power and calibration as 
;; necessary.  Different methods for this task are ordered by preference; 
;; we try to avoid using leaky or delayed instruments along the greedy path, 
;; and if an instrument can take an image without re-calibration, it's better.
;; Worst is to actually cause a working satellite to become leaky or delayed.
(:method (do-image ?d ?m)
     ;;; select a non-leaking satellite that will continue to not leak
     use-calibrated-non-leaking-on-time
     ((eq (on-board ?i) ?s) 
      (supports ?i ?m) 
      (not (fuel-leaking ?s))
      (calibrated ?i)
      (eq (last-direction-change ?s) NO-DIRECTION)
     )
     ((take-image ?s ?i ?d ?m)
      (test-not-fuel-leaking ?s)
      (test-on-time ?s)
     )
)

(:method (do-image ?d ?m)
     ;;; select a non-leaking satellite that will continue to not leak
     use-calibrated-non-leaking-late
     ((eq (on-board ?i) ?s) 
      (supports ?i ?m) 
      (not (fuel-leaking ?s))
      (calibrated ?i)
      (neq (last-direction-change ?s) NO-DIRECTION)
     )
     ((take-image ?s ?i ?d ?m)
      (test-not-fuel-leaking ?s)
     )
)

(:method (do-image ?d ?m)
     ;;; select a non-leaking satellite that will continue to not leak
     use-uncalibrated-non-leaking-on-time
     ((eq (on-board ?i) ?s) 
      (supports ?i ?m) 
      (not (fuel-leaking ?s))
      (not (calibrated ?i))
      (eq (last-direction-change ?s) NO-DIRECTION)
     )
     ((take-image ?s ?i ?d ?m)
      (test-not-fuel-leaking ?s)
      (test-on-time ?s)
     )
)

(:method (do-image ?d ?m)
     ;;; select a non-leaking satellite that will continue to not leak
     use-uncalibrated-non-leaking-late
     ((eq (on-board ?i) ?s) 
      (supports ?i ?m) 
      (not (fuel-leaking ?s))
      (not (calibrated ?i))
      (neq (last-direction-change ?s) NO-DIRECTION)
     )
     ((take-image ?s ?i ?d ?m)
      (test-not-fuel-leaking ?s)
     )
)

(:method (do-image ?d ?m)
     ;;; select any non-leaking satellite that's calibrated
     use-calibrated-non-leaking
     ((eq (on-board ?i) ?s) 
      (supports ?i ?m) 
      (not (fuel-leaking ?s))
      (calibrated ?i)
     )
     ((take-image ?s ?i ?d ?m)
     )
)

(:method (do-image ?d ?m)
     ;;; select any non-leaking satellite that's not calibrated
     use-uncalibrated-non-leaking
     ((eq (on-board ?i) ?s) 
      (supports ?i ?m) 
      (not (fuel-leaking ?s))
      (not (calibrated ?i))
     )
     ((take-image ?s ?i ?d ?m)
     )
)

(:method (do-image ?d ?m)
     ;;; select any leaking satellite that's calibrated
     use-calibrated-leaking
     ((eq (on-board ?i) ?s) 
      (supports ?i ?m) 
      (fuel-leaking ?s)
      (calibrated ?i)
     )
     ((take-image ?s ?i ?d ?m)
     )
)

(:method (do-image ?d ?m)
     ;;; select any leaky, uncalibrated satellite
     whatever
     ((eq (on-board ?i) ?s) 
      (supports ?i ?m) 
      (fuel-leaking ?s)
      (not (calibrated ?i))
     )
     ((take-image ?s ?i ?d ?m)
     )
)


;(:method (take-image-with-satellite ?s ?d ?m)
;     ;; use an instrument that's already powered
;     use-powered-instrument
;     ((eq (on-board ?i) ?s) (supports ?i ?m) (power-on ?i))
;     ((take-image ?s ?i ?d ?m))
;)

;(:method (take-image-with-satellite ?s ?d ?m)
;     ;; use an instrument that's not already powered
;     use-unpowered-instrument
;     ((eq (on-board ?i) ?s) (supports ?i ?m) (not (power-on ?i)))
;     ((take-image ?s ?i ?d ?m))
;)
     
(:method (test-not-fuel-leaking ?s)
    fuel-level-fine
    ((not (fuel-leaking ?s)))
    (
    )
)

(:method (test-on-time ?s)
    on-time
    ((eq (last-direction-change ?s) NO-DIRECTION))
    (
    )
)

;; This tests that an acquired image can be transmitted. Must determine which 
;; satellite has the image, then checks whether that satellite can transmit.
(:method (test-can-transmit-image ?d ?m)
    ((have-image ?s ?d ?m))
    ((test-can-transmit-images ?s))
)


;; This tests whether a satellite can transmit. Requires enough fuel to turn 
;; to the data center, unless the satellite already points there.
(:method (test-can-transmit-images ?s)
    set
    ((eq (data-center) ?center)
     (eq (pointing ?s) ?center)
    )
    (
    )
    on-time
    ((eq (last-direction-change ?s) NO-DIRECTION)
     (neq (fuel-level ?s) NO-FUEL)
    )
    (
    )
    late-but-ready
    ((eq (data-center) ?center)
     (eq (last-direction-change ?s) ?center)
     (neq (fuel-level ?s) NO-FUEL)
    )
    (
    )
    late-not-ready-leaky
    ((eq (data-center) ?center)
     (neq (last-direction-change ?s) ?center)
     (neq (last-direction-change ?s) NO-DIRECTION)
     (fuel-leaking ?s)
     (neq (fuel-level ?s) NO-FUEL)
     (eq (fuel-level ?s) ?fl)
     (neq (next-level ?fl) NO-FUEL)
     (eq (next-level ?fl) ?nfl)
     (neq (next-level ?nfl) NO-FUEL)
    )
    (
    )
    late-not-ready-not-leaky
    ((eq (data-center) ?center)
     (neq (last-direction-change ?s) ?center)
     (neq (last-direction-change ?s) NO-DIRECTION)
     (neq (fuel-level ?s) NO-FUEL)
     (eq (fuel-level ?s) ?fl)
     (neq (next-level ?fl) NO-FUEL)
    )
    (
    )
)

;; Deals with ensuring that all necessary calibration, powering, and turning
;; actions take place to take a single image, without using extra fuel.
(:method (take-image ?s ?i ?d ?m)
     already-taken
     ((have-image ?s ?d ?m))
     (; had this image already
     )
     
     ;;; If we have the image, then finish.
;     submit
;     ((have-image ?s ?d ?m))
;     ((submit-data ?s) (!finish ?d ?m))

     ;;; Prepare the instrument for taking the image
     shutter
     ((supernova-exploded ?d) (not (shuttered ?i)) (calibrated ?i))
     ((!shutter ?i) (take-image ?s ?i ?d ?m))

     ;;; Ready to take the shot
     take-image
     ((calibrated ?i) 
      (eq (pointing ?s) ?d) 
      (not (have-image ?s ?d ?m))
     )
     ((!take-image ?s ?m)
      (take-image ?s ?i ?d ?m)
     )
    
     ;;; Prepare the instrument for taking the image
     calibrate
     ((not (calibrated ?i)))
     ((turn-on-instrument ?s ?i)
	  (calibrate-instrument ?s ?i ?d)
	  (take-image ?s ?i ?d ?m)
	 )
    
	 ;;; Set/adjust/verify the direction of the satellite
     turn-on-time
     ((eq (pointing ?s) ?d-prev) 
      (different ?d-prev ?d) 
      (eq (last-direction-change ?s) NO-DIRECTION)
     )
     ((!turn-to ?s ?d) 
	  (take-image ?s ?i ?d ?m)
	 )
    
	 turn-once-when-late
     ((eq (pointing ?s) ?d-prev) 
      (eq (last-direction-change ?s) ?d)
      (different ?d-prev ?d) 
      (eq (data-center) ?center)
     )
     ((!turn-to ?s ?center) 
	  (take-image ?s ?i ?d ?m)
	 )
    
	 turn-twice-when-late
     ((eq (pointing ?s) ?d-prev) 
      (eq (last-direction-change ?s) ?d-last)
      (different ?d-prev ?d) 
      (different ?d ?d-last)
      (eq (fuel-level ?s) ?level) 
      (eq (next-level ?level) ?new-level) 
      (eq (data-center) ?center)
     )
     ((!turn-to ?s ?d)
      (!turn-to ?s ?center) 
	  (take-image ?s ?i ?d ?m)
	 )
	
	 ;;Should never arrive here.
	 error
	 (eval (error))
	 ()
)

;; Ensures that a needed instrument is on.
(:method (turn-on-instrument ?s ?i)
     ((power-on ?i))
     ()
       
     ((power-avail ?s))
     (:ordered  (!switch-on ?i ?s))

     ;; if there is an another instrument on, shut it off first.
     ((eq (on-board ?j) ?s) (power-on ?j))  ;; ?i != ?j
     (:ordered  (!switch-off ?j ?s)  (!switch-on ?i ?s))
)

;; Ensures that a needed instrument is calibrated; knows what direction it 
;; needs to point afterward to make use of a delayed instrument more efficient.
(:method (calibrate-instrument ?s ?i ?imaged)  ;; assume power-on
     ensure-power-on
     ((not (power-on ?i)))
     ((fail))

     already-calibrated
     ((calibrated ?i))
     ()

     ready-to-calibrate
     ((eq (pointing ?s) ?d) (eq (calibration-target ?i) ?d) (not (supernova-exploded ?d)))
     (:ordered  (!calibrate ?s ?i))

     needs-shuttering
     ((eq (pointing ?s) ?d) (eq (calibration-target ?i) ?d) (supernova-exploded ?d))
     (:ordered  (!shutter ?i) (!calibrate ?s ?i))

     turn-to-target
     ((eq (calibration-target ?i) ?d) (eq (last-direction-change ?s) NO-DIRECTION))
     (:ordered 
	  (!turn-to ?s ?d)
	  (calibrate-instrument ?s ?i ?imaged)
	 )

     turn-to-target-late
     ((eq (calibration-target ?i) ?d) (eq (last-direction-change ?s) ?d)
      (eq (fuel-level ?s) ?level) (eq (next-level ?level) ?new-level)
     )
     (:ordered 
	  (!turn-to ?s ?imaged)
	  (calibrate-instrument ?s ?i ?imaged)
	 )

    turn-to-target-late-wrong-dir-queued
    ((eq (calibration-target ?i) ?d) (neq (last-direction-change ?s) ?d))
    (:ordered 
	  (!turn-to ?s ?d)
	  (!turn-to ?s ?imaged)
	  (calibrate-instrument ?s ?i ?imaged)
    )
)


;; Handles transmission of images to a data center.
(:method (submit-data ?s)
     pointing-toward-receiver
     ((eq (data-center) ?center) (eq (pointing ?s) ?center))
     ((!transmit-images ?s))

     turn-to-receiver-on-time
     ((eq (data-center) ?center) (eq (pointing ?s) ?d)
      (eq (last-direction-change ?s) NO-DIRECTION)
     )
     ((!turn-to ?s ?center)
      (!transmit-images ?s)
     )

     turn-to-receiver-late
     ((eq (data-center) ?center)
      (eq (last-direction-change ?s) ?center)
     )
     ((!turn-to ?s ?center) 
      (!transmit-images ?s)
     )

     turn-to-receiver-later
     ((eq (data-center) ?center) 
      (neq (last-direction-change ?s) ?center)
     )
     ((!turn-to ?s ?center)
      (!turn-to ?s ?center) 
      (!transmit-images ?s)
     )
)

;;; Helper axioms
(:- (different ?x ?y) ((not (same ?x ?y))))
(:- (same ?x ?x) nil)

;; A basic task, fail, with no methods, which will therefore always cause 
;; backtracking.
(:method (fail))

)

