 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: January 2018
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file generates problems for the Satellite domain with 
 ;;;; surprising events.


;;; Default Values for Problem-Generation Parameters
(defparameter *satellite-num* 3)
(defparameter *instruments-per-satellite* 5)
(defparameter *targets-num* 20)
(defparameter *mode-num* 8)
(defparameter *goal-num* 8)
(defparameter *possible-modes* '(infrared thermograph spectrograph plain))
(defparameter *target-types* '(phenomenon planet star asteroid nebula))
(defvar *maximum-capacity* 15)
(defvar *maximum-fuel* 10)
(defvar *emp-prob* .2)
(defvar *sup-prob* .2)
(defvar *late-prob* .2)

;;; Random Problem Generator
(defun genSatProb (prob-num  
    	           &key (satellites *satellite-num*)
                        (instruments *instruments-per-satellite*)
                        (targets *targets-num*)
                        (modes *mode-num*)
                        (late-prob *late-prob*)
                        (sup-prob *sup-prob*)
                        (emp-prob *emp-prob*)
                        (target-list *target-types*)
                        (relative-dir-name "a")
                   &aux pdir)
     
     (ensure-directories-exist 
         (make-pathname :directory (list :relative "domains" "satellite" relative-dir-name))
     )
     (let (direction-list mode-list)
	  (loop for pnum from 1 to prob-num do
	       (let ((filename 
	                 (make-pathname 
	                     :directory (list :relative "domains" "satellite" relative-dir-name)
	                     :name (format nil "pfile-gen~a.prob" pnum)
                     )
                ))
		    (with-open-file (str filename :direction :output :if-does-not-exist :create :if-exists :supersede)

			 (format str "~%(define (problem satellite-events)" pnum)
			 (format str "~%;;Version 6.0 Prob = .2")
			 (format str "~%(:domain satellite)")
			 (format str "~%(:objects")

			 ;; We have only two orbits. 
			 (format str "~%      ")
			     
			 ;; Satellites
			 (loop for s from 1 to satellites do
			      (format str "~%    satellite~a - satellite" s))

			 ;; Instruments
			 (loop for x from 1 to instruments do 
			     (loop for s from 1 to satellites do
			         (format str "~%    instrument~a-~a - instrument" s x)))
				 
			 ;; Photo-taking reqs -- modes
			 (setq mode-list 
			      (loop for m from 1 to modes collect
			          (format nil "~s~a"  (nth (random (length *possible-modes*)) *possible-modes*) m)))
			 (remove-duplicates mode-list :test #'equal)
			 (loop for m in mode-list do
			      (format str "~%    ~a - mode" m))

			 ;; Targets -- directions
			 (setq direction-list 
			      (loop for x from 1 to targets collect 
			          (format nil "~s~a"  (nth (random (length target-list)) target-list) x)))
             (format str "~%    GroundCenter0 - direction")
			 (loop for direction in direction-list do
			      (format str "~%    ~a - direction" direction))
			 
			 ;; Capacities, fuel-levels
			 (loop for c from 0 to *maximum-capacity* do
                 (format str "~%    cap~a - capacity" c))
			 (loop for f from 1 to *maximum-fuel* do
                 (format str "~%    fuel~a - fuel-level" f))
				 
			 (format str "~%)")
			 
			 (format str "~%(:init")

			 ;; Satellites
			 (loop for s from 1 to satellites do
                (format str "~%    (power-avail satellite~a)" s)
			 )

			 ;; Computing the SUPPORTS
			 (let (instr sat1 sat2)
			     (loop for m in mode-list do
                     (setq sat1 (random satellites))
                     (setq sat2 (random-other-than satellites sat1))
                     (format str "~%    (supports instrument~a-~a ~a)" (1+ sat1) (1+ (random instruments)) m)
                     (format str "~%    (supports instrument~a-~a ~a)" (1+ sat2) (1+ (random instruments)) m)
				 )
			 )

             (loop for dir in direction-list do
                 (when (< (random 1d0) emp-prob)
                     (format str "~%    (electro-magnetic-field ~a)" dir)
                 )
                 (when (< (random 1d0) sup-prob)
                     (format str "~%    (supernova-exploded ~a)" dir)
                 )
                 (when (< (random 1d0) late-prob)
                     (format str "~%    (late-to-turn satellite~a ~a)"
                         (1+ (random satellites))
                         dir
                     )
                 )
             )

			 (format str "~%)")


			 (format str "~%(:values")
			 ;; Instruments
			 (loop for sat from 1 to satellites do
                 (loop for instr from 1 to instruments do
                    (format str "~%    ((calibration-target instrument~a-~a) ~a)" 
                        sat instr (nth (random (length direction-list)) direction-list))
                    (format str "~%    ((on-board instrument~a-~a) satellite~a)" 
                        sat instr sat)
                 )
             )
		      
			 (loop for s from 1 to satellites do
                (format str "~%    ((fuel-level satellite~a) fuel~a)" s *maximum-fuel*)
                (format str "~%    ((data-capacity satellite~a) cap~a)" s *maximum-capacity*)
                (format str "~%    ((pointing satellite~a) GroundCenter0)" s)
			 )

			 ;; Data center
			 (format str "~%    ((data-center) GroundCenter0)")

			 ;; Predicates for modeling fuel level and data capacity change
			 (loop for c from *maximum-fuel* downto 2 do
			      (format str "~%    ((next-level fuel~a) fuel~a)" c (- c 1))
			 )
             (format str "~%    ((next-level fuel1) NO-FUEL)")

			 (loop for c from *maximum-capacity* downto 1 do
			      (format str "~%    ((next-capacity cap~a) cap~a)" c (- c 1))
			 )
			 (format str "~%)")

			 (format str "~%(:given")
			 (loop for s from 1 to satellites do
                (format str "~%    (not (fuel-leaking satellite~a))" s)
			 )
			 (format str "~%)")

			 (format str "~%(:given-values")
			 (loop for s from 1 to satellites do
                (format str "~%    ((last-direction-change satellite~a) NO-DIRECTION)" s)
			 )
			 (format str "~%)")

			 (format str "~%(:goal (and")
			 (let ((goal-direction-base nil)
			       (goal-directions-num *goal-num*)  
			       (goal-direction-list nil)
			      )
			      (setq goal-direction-base (remove-duplicates goal-direction-base :test #'equal))
			      
			      ;; Get the random directions that will appear in the goals
			      (loop while (< (length goal-direction-list) goal-directions-num) do
			          (pushnew (nth (random (length direction-list)) direction-list)
			              goal-direction-list
                      )
                  )
			      
			      ;; for each direction, generate a mode to take the image in from the list of supported modes
			      ;; This latter is for not to generate unsolvable problems
			      (loop for d in goal-direction-list do
			          (format str "~%    (succeeded ~a ~a)" d  (nth (random (length mode-list)) mode-list))
				  )
			 )
			 (format str "~%))")
			 (format str "~%)"))))))
			 

(defun random-other-than (max-val forbidden-val &aux gen-val)
    (setq gen-val (random (1- max-val)))
    (if (>= gen-val forbidden-val)
        ;then
        (1+ gen-val)
        ;else
        gen-val
    )
)