 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: January 19, 2018
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file runs an experiment comparing goal achievement performance
 ;;;; with and without explanation in a Mars Rover domain with surprises.
 
(load "main/artue-loader.lisp")
(in-package :artue)


(defun run-rover-experiment 
           (&key (results-file "rover-results.csv") (relative-dir-name "a")
                 (record-logs nil) (do-explain t) (max-search-depth 7)
                 (cost-computation-method :change-count) (start-num 1)
                 (requested-explanations 1) (planning-time-limit 60)
                 (ensure-unobservable-goals t)
            &aux expgen agent-settings-list expgen-settings-list domain-model)
    (setq agent-settings-list
        `(
            :observability :partial 
            :do-explain ,do-explain
            :record-logs ,record-logs
            :explanation-results-file nil
            :experiment-condition ,relative-dir-name
            :diagnose t
            :stop-on-failed-explanation nil
            :planning-time-limit ,planning-time-limit
            :ensure-unobservable-goals ,ensure-unobservable-goals
         )
    )
    
    (setq expgen-settings-list 
        `(
            :self :rover-controller
            :learning-allowed nil
            :max-cost ,max-search-depth
            :multi-agent nil
            :envision-events t
            :cutoff-seconds 300
            :inconsistency-selection-method nil
            :explanations-requested ,requested-explanations
            :max-explanation-count ,requested-explanations
            :cost-computation-method ,cost-computation-method
         )
    )

    (write-file-header results-file 
        (append agent-settings-list expgen-settings-list)
    )

    (setq domain-model (prepare-model "domains/rovers/rovers-compass-non-depth.htn" :new-name 'modified-rover-htn))
    (dotimes (i (- 26 start-num))
        (setq expgen
            (apply #'discoverhistory-explanation-generator-initialize
                (domain-name domain-model)
                expgen-settings-list
            )
        )
        
        (rover-run domain-model
            (format nil "domains/rovers/~a/pfile-gen~A.prob" relative-dir-name (+ i start-num)) 
            expgen agent-settings-list 
            :results-file results-file
        )
    )
)

(defun rover-run (domain-model scenario-file expgen agent-settings-list 
                  &key (results-file "test-exp-output.csv") (model-name nil) 
                  &aux agent-list sim)
    (setq agent-list nil)
    
    (push 
        (apply #'create-agent nil (domain-name domain-model)
            :expgen expgen
            agent-settings-list
        )
        agent-list
    )
    
    (setq sim (test-event-explanation (car agent-list) (domain-file domain-model) scenario-file))
    (record-rover-performance sim expgen (get-next-layer (car agent-list)) results-file)
)

(defun write-file-header (results-file settings)
    (with-open-file (str results-file :direction :output 
            :if-does-not-exist :create :if-exists :append)
        (loop while settings do
            (format str "~%~s: ~s" (first settings) (second settings))
            (setq settings (cddr settings))
        )
        (format str "~%~A,~A,~A,~A,~A,~A,~A,~A,~A,~A,~A,~A,~A,~A,~A,~A,~A,~A,~A,~A,~A,~A"
            "scenario"
            "start time"
            "goals achieved"
            "goals failed"
            "energy spent"
            "time spent explaining"
            "time spent enumerating"
            "time spent searching hypothesis space"
            "explanation attempts"
            "explanation changes"
            "explanation failures"
            "explanations cut off"
            "total explanation depth"
            "max explanation depth"
            "total assumptions changed"
            "max assumptions changed"
            "final assumption count"
            "time spent planning"
            "calls to planner"
            "total run time"
            "action count"
            "actions"
        )
    )
)

(defun record-rover-performance (sim expgen ag results-file &aux energy-cost actions expgen)
    (setq actions (mapcar #'car (sim-performed-actions sim)))
    (setq energy-cost
        (+
            (* 8 (count '!navigate actions :key #'car))
            (* 8 (count '!navigate-detected actions :key #'car))
            (* 50 (count '!dig-out actions :key #'car))
        )
    )
    
    (setq expgen (get-expgen ag))
    
    (with-open-file (str results-file :direction :output 
            :if-does-not-exist :create :if-exists :append)
        (format str "~%~A,~A,~A,~A,~A,~A,~A,~A,~A,~A,~A,~A,~A,~A,~A,~A,~A,~A,~A,~A,~A,"
            (get-scenario-file sim) 
            (get-time-string)
            (length (grep-trees (artue::state-atoms (get-cur-state sim)) 'succeeded))
            (length (grep-trees (artue::state-atoms (get-cur-state sim)) 'failed)) 
            energy-cost
            (get-explanation-time ag) 
            (if expgen (get-total-enumeration-time expgen) "N/A")
            (if expgen (get-total-search-time expgen) "N/A")
            (get-explanation-count ag)
            (get-explanation-changes ag)
            (get-explanation-failures ag)
            (get-explanations-cutoff ag)
            (get-explanation-depth-total ag)
            (get-explanation-depth-max ag)
            (get-explanation-assumptions-change-total ag)
            (get-explanation-assumptions-change-max ag)
            (get-explanation-final-assumption-count ag)
            (get-total-planning-time ag)
            (get-planning-calls ag)
            (coerce (/ (get-total-run-time ag) internal-time-units-per-second) 'float)
            (length actions)
        ) 
        (write (reverse actions) :stream str :pretty nil)
    )
)

