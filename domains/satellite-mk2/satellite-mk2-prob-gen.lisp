 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: January 2018
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file generates problems for the Satellites domain with 
 ;;;; lens burn.


;;; Default Values for Problem-Generation Parameters
(defparameter *satellite-num* 3)
(defparameter *instrument-total* 15)
(defparameter *targets-num* 20)
(defparameter *mode-num* 5)
(defparameter *goal-num* 5)
(defparameter *possible-modes* '(infrared thermograph spectrograph plain))
(defparameter *target-types* '(phenomenon planet star asteroid nebula))
(defvar *maximum-capacity* 15)
(defvar *maximum-fuel* 10)
(defvar *wave-prob* .4)

(defparameter *calibration-targets-table* (make-hash-table :test #'equal))
(defparameter *supports-table* (make-hash-table :test #'equal))

;;; Random Problem Generator
(defun genSatProb (prob-num  
		       &key (satellites *satellite-num*)
		       (instruments *instrument-total*)
		       (targets *targets-num*)
		       (modes *mode-num*)
		       (wave-prob *wave-prob*)
		       (target-list *target-types*)
		       (relative-dir-name "a")
		       &aux pdir)
     
     (ensure-directories-exist 
         (make-pathname :directory (list :relative "domains" "satellite-mk2-probs" relative-dir-name))
     )
     (let (direction-list mode-list)
	  (loop for pnum from 1 to prob-num do
	       (let ((filename 
	                 (make-pathname 
	                     :directory (list :relative "domains" "satellite-mk2-probs" relative-dir-name)
	                     :name (format nil "satellite-mk2-~a" pnum)
	                     :type "prob"
                     )
                ))
		    (with-open-file (str filename :direction :output :if-does-not-exist :create :if-exists :supersede)

			 (format str "~%(define (problem satellite-mk2-~a)" pnum)
			 (format str "~%;;Version 5.0 Prob = ~a" *wave-prob*)
			 (format str " Satellites = ~a" *satellite-num*)
			 (format str " Modes = ~a" *mode-num*)
			 (format str " Targets = ~a" *targets-num*)
			 (format str " Instruments = ~a" *instrument-total*)
			 (format str "~%(:domain satellite)")
			 (format str "~%(:objects")

			 ;; Satellites
			 (loop for s from 1 to satellites
			      do (format str "~%      satellite~a - satellite" s))

			 ;; Instruments
			 (loop for x from 1 to instruments 
			      do (format str "~%      instrument~a - instrument" x))
				 
			 ;; Photo-taking reqs -- modes
			 (setq mode-list 
			      (loop for m from 1 to modes collect
				   (format nil "~s~a"  (nth (random (length *possible-modes*)) *possible-modes*) m)))
			 (remove-duplicates mode-list :test #'equal)
			 (loop for m in mode-list do
			      (format str "~%      ~a - mode" m))

			 ;; Targets -- directions
			 (setq direction-list 
			      (loop for x from 1 to targets collect 
				   (format nil "~s~a"  (nth (random (length target-list)) target-list)	x)))
			 (remove-duplicates direction-list :test #'equal)
             (format str "~%      GroundCenter0 - direction")
			 (loop for direction in direction-list do
			      (format str "~%      ~a - direction" direction))
			 
			 (format str "~%)")
			 
			 (format str "~% (:init")

			 ;; Satellites
			 (loop for s from 1 to satellites
			      do (progn
				      (format str "~%      (power_avail satellite~a)" s)
				      (format str "~%      (pointing satellite~a GroundCenter0)" s)
				     )
			 )

			 ;; Instruments
			 (loop for instr from 1 to instruments 
			      do (progn
				      (setf (gethash instr *calibration-targets-table*)   
					   (nth (random (length direction-list)) direction-list))
				      (format str "~%      (calibration_target instrument~a ~a)" 
					   instr (gethash instr *calibration-targets-table*))
				      (format str "~%      (on_board instrument~a satellite~a)" instr (+ 1 (random satellites)))
				     )
			 )
		      
             (setq *supports-table* (make-hash-table :test #'equal))
				      
			 ;; Computing the SUPPORTS
			 (let (modes-to-be-supported instr mode)
			     (setq modes-to-be-supported mode-list)
				  ; (remove nil 
				       ; (loop for m in mode-list collect
					    ; (when (= (random 2) 1)
						 ; m))))
			      (loop for i from 1 to instruments 
				   do (progn
					   (setq mode (nth (random (length modes-to-be-supported)) modes-to-be-supported))
					   (setf (gethash i *supports-table*) (cons mode (gethash i *supports-table*)))
					   (format str "~%      (supports instrument~a ~a)" i mode)
					  )
				  )

			      
			      (loop for m in modes-to-be-supported 
				   do (unless (supported-mode m)
					   (setq instr (1+ (random instruments)))
					   (setf (gethash instr *supports-table*) (cons m (gethash instr *supports-table*)))
					   (format str "~%      (supports instrument~a ~a)" instr m)
					  )
				  )
			 )


			 ;; Predicates for modeling fuel level and data capacity change
             (loop for dir in direction-list do
                 (loop for i from 1 to instruments do
                     (when (< (random 1d0) wave-prob)
                         (format str "~%      (intense-wavelength ~a instrument~a)" dir i)
                     )
                 )
             )

			 (format str "~%)")

			 (format str "~%(:values ((time-taken) 0))")
			 (format str "~%(:goal (and (do-quickly (achieve (")
			 (let (goal-direction-base  goal-directions-num  goal-direction-list)
			      (maphash 
				   #'(lambda (key value) 
					  (setq goal-direction-base (cons value goal-direction-base)))
				   *calibration-targets-table*
				  )
			      (setq goal-direction-base (remove-duplicates goal-direction-base :test #'equal))
;			      (setq goal-directions-num (random (length goal-direction-base)))
			      (setq goal-directions-num *goal-num*)
			      (loop
				   (unless (= goal-directions-num 0) (return))
				   (setq goal-directions-num (random (length goal-direction-base)))
				  )
			      
			      ;; Get the random directions that will appear in the goals
			      (setq goal-direction-list nil)
			      (loop while (< (length goal-direction-list) goal-directions-num) do
			          (pushnew (nth (random (length direction-list)) direction-list)
			              goal-direction-list
                      )
                  )
			      
			      ;; for each direction, generate a mode to take the image in from the list of supported modes
			      ;; This latter is for not to generate unsolvable problems
			      (loop for d in goal-direction-list 
				   do (let* ((instr (find-instrument d)))
;						 (mode-list (find-mode instr)))
;					   (when instr
						(format str "~%      (have_image ~a ~a)" d  (nth (random (length mode-list)) mode-list))
					  )
				  );)
			 )		
			 (format str "~%)))))")
				   
			 (format str "~%)"))))))
			 

(defun find-instrument (direction)
     (let (inst-list)
	  (maphash  
	       #'(lambda (key value)
		      (when (eql value direction)
			   (push key inst-list)))
	       *calibration-targets-table*)
	  (unless inst-list
	       (return-from find-instrument nil))
	  (setq inst-list (remove-duplicates inst-list :test #'equal))
	  (nth (random (length inst-list)) inst-list)))

(defun find-mode (instrument)
     (gethash instrument *supports-table*))

(defun supported-mode (m)
     (maphash 
	  #'(lambda (instr mode-list)
		 (format t "~%ML: ~a" mode-list)
		 (when (member m mode-list :test #'equal)
		      (return-from supported-mode t)))
	  *supports-table*)
     nil)
		     