(define (domain :satellite-mk2)
  (:requirements :strips :typing)
  (:types satellite direction instrument mode time ordinal)
  (:supertypes (time int) (ordinal int))

  (:predicates 
	(on_board ?i - instrument ?s - satellite)
	(supports ?i - instrument ?m - mode)
	(pointing ?s - satellite ?d - direction)
	(power_avail ?s - satellite)
	(power_on ?i - instrument)
	(calibrated ?i - instrument)
	(have_image ?d - direction ?m - mode)
	(taking-image ?s - satellite ?m - mode)
	(calibration_target ?i - instrument ?d - direction)
	(intense-wavelength ?d - direction ?i - instrument)
	(lens-flaring ?d - direction ?s - satellite)
	(broken-motor ?s - satellite)
	(broken-lens ?s - satellite)
	(turning ?s - satellite ?d - direction)
	(turn-memory ?seq - ordinal ?s - satellite ?d - direction ?t - time)
  )
  
  (:functions
    (time-taken) - time
  )
  
  (:hidden intense-wavelength)
  
  (:static on_board supports calibration_target)
 
  (:internal taking-image)

  (:action turn_to
   :parameters (?s - satellite ?d_new - direction)
   :precondition (not (broken-motor ?s))
   :effect (and  (turning ?s ?d_new)))
 
  (:event turning
   :parameters (?s - satellite)
   :precondition (and (turning ?s ?d_new) (pointing ?s ?d_prev))
   :effect (and  (pointing ?s ?d_new)
                 (not (pointing ?s ?d_prev))
                 (increase (time-taken) 1)
                 (not (turning ?s ?d_new))))
#|
  (:event remember-turn
   :parameters (?s - satellite)
   :precondition (and (turning ?s ?d_new) (eq (time-taken) ?t))
   :effect (and  (turn-memory 0 ?s ?d_new ?t)))

  (:event remember-old-turn
   :parameters (?s - satellite ?seq - ordinal)
   :precondition (and (turning ?s ?d_new) (turn-memory ?seq ?s ?d ?t)
                      (assign ?nseq (+ ?seq 1))
                 )
   :effect (and (not (turn-memory ?seq ?s ?d ?t))
                (turn-memory ?nseq ?s ?d ?t)
           )
  )
   
  (:event motor-wears-out
   :parameters (?s - satellite)
   :precondition (and (turning ?s ?d_new) (turn-memory 3 ?s ?d ?t)
                      (eq (time-taken) ?t_now) (eval (>= ?t (- ?t_now 15))))
   :effect (and (broken-motor ?s))
  )
|#  
  (:action repair-motor
   :parameters (?s - satellite)
   :precondition (and (broken-motor ?s))
   :effect (and (not (broken-motor ?s)) (increase (time-taken) 10))
  )
                 
  (:action switch_on
   :parameters (?i - instrument)
   :precondition (and (on_board ?i ?s) 
                      (power_avail ?s))
   :effect (and (power_on ?i)
                (not (calibrated ?i))
                (not (power_avail ?s))))

  (:action switch_off
   :parameters (?i - instrument)
   :precondition (and (on_board ?i ?s)
                      (power_on ?i))
   :effect (and (not (power_on ?i))
                (power_avail ?s)
                (not (calibrated ?i))))

  (:action calibrate
   :parameters (?s - satellite ?i - instrument ?d - direction)
   :precondition (and (on_board ?i ?s)
                      (calibration_target ?i ?d)
                      (pointing ?s ?d)
                      (power_on ?i)
                      (not (broken-lens ?s))
                 )
   :effect (and (calibrated ?i)))

  (:action take_image
   :parameters (?s - satellite ?m - mode)
   :precondition (and (calibrated ?i)
                      (on_board ?i ?s)
                      (supports ?i ?m)
                      (power_on ?i)
                      (pointing ?s ?d)
                      (not (broken-lens ?s)))
   :effect (and (taking-image ?s ?m)
                (increase (time-taken) 1)))

  (:event lens-flare 
   :parameters (?i - instrument ?d - direction)
   :precondition (and (power_on ?i)
                      (not (calibrated ?i))
                      (intense-wavelength ?d ?i)
                      (on_board ?i ?s)
                      (not (lens-flaring ?d ?s))
                 )
   :effect (and (lens-flaring ?d ?s)))

  
  (:event lens-flare-ends 
   :parameters (?i - instrument ?d - direction)
   :precondition (and (lens-flaring ?d ?s)
                      (on_board ?i ?s)
                      (calibrated ?i)
                      (power_on ?i)
                 )
   :effect (and (not (lens-flaring ?d ?s)))
  )

  (:event burn-lens
   :parameters (?s - satellite)
   :precondition (and (taking-image ?s ?m)
                      (pointing ?s ?d)
                      (power_on ?i)
                      (intense-wavelength ?d ?i)
                      (on_board ?i ?s))
   :effect (and (broken-lens ?s)))
   
  (:event image-finished
   :parameters (?s - satellite ?m - mode)
   :precondition (and (taking-image ?s ?m)
                      (pointing ?s ?d))
   :effect (and (not (taking-image ?s ?m)) (have_image ?d ?m)))
   
  (:action repair-lens
   :parameters (?s - satellite)
   :precondition (and (broken-lens ?s))
   :effect (and (not (broken-lens ?s)) (increase (time-taken) 10)))
)

