 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: April 8, 2011
 ;;;; Project Name: Explanation for Planning
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file runs a learning experiment in the mud domain.
 
 (load "main/artue-loader")

(in-package :artue)

;(utils:load-file "learning/" "foil-stuff.lisp")
(utils:load-file "learning/" "my-foil.lisp")
(utils:load-file "full-explainer/" "my-foil-interface.lisp")

(setq artue::*max-cost* 30)
(setq *max-actions-expected* 50) ;; change to field of agent

(defun mud-test-withhold (&key (prob-num 1) (category "basic") 
                               (planning-time-limit 60) (learning-allowed t)
                               (withheld-events '(artue::mud-slows-down artue::no-more-mud)))
    (setq artue::*use-achieve-syntax* nil)
    (setq artue::*learning-allowed* learning-allowed) ;; Needs to know that now a field of expgen.
    (setq artue::*start-with-noop* t)
    (test-event-explanation 
        (make-pathname :directory '(:relative "domains") :name "mudworld" :type "htn")
        (make-pathname :directory 
            `(:relative "domains" "mudworld-probs" ,category) 
            :name (format nil "mud~a" prob-num) 
            :type "prob"
        )
        :unknown-events withheld-events
        :planning-time-limit planning-time-limit
    )
)


(defun leave-one-in-experiment (&key (results-file nil) (category "basic")
                                     (planning-time-limit 60) (cutoff-seconds 240)
                                     (max-exp-cost 25)
                                &aux (examples nil) (start-time nil) (learning-time nil))
    (when (null results-file)
        (setq results-file 
            (make-pathname :name "mudworld-results"
                           :type "csv"
            )
        )
    )
    (setq artue::*cutoff-seconds* cutoff-seconds)
    (setq artue::*max-cost* max-exp-cost)
    (with-open-file (str results-file :direction :output 
            :if-does-not-exist :create :if-exists :append)
        (format str "~%Leave-one-in FOIL experiments")
        (format str "~%Category: ~A" category)
        (format str "~%Maximum explanation cost: ~A" max-exp-cost)
        (format str "~%Planning cutoff time: ~A" planning-time-limit)
        (format str "~%Explanation cutoff in seconds: ~A" cutoff-seconds)
        (format str "~%Problem directory: ~A" 
            (make-pathname :directory (:relative "domains" "mudworld-probs"))
        )
        (format str "~%Training example, Test example, Start Time, Time Taken, Learning Seconds, Performance Seconds, Unknown Events, Learned Models, Abandoned Models, Actions")
    )
    (dotimes (i 25)
        (push (1+ i) examples)
    )
    (setq examples (nreverse examples))
    (dolist (i examples)
        (wipe-memory)
        (mud-test-withhold :category category :prob-num i
                           :planning-time-limit planning-time-limit
        )
        (setq start-time (get-internal-run-time))
        (learn-from-explanation (car *plausible-explanations*))
        (setq learning-time 
            (coerce
                (/ (- (get-internal-run-time) start-time) 
                   internal-time-units-per-second
                )
                'double-float
            )
        )
        (dolist (j (remove i examples))
            (do-mud-trial results-file i i j learning-time
                :category category :prob-num i
                :planning-time-limit planning-time-limit        
            )
        )
    )
)

(defun do-baseline (&key (results-file nil) (category "basic")
                         (planning-time-limit 60) (cutoff-seconds 240)
                         (max-exp-cost 30) 
                         (withheld-events '(no-more-mud mud-slows-down))
                    &aux (examples nil))
    (when (null results-file)
        (setq results-file 
            (make-pathname :name "mudworld-baseline"
                           :type "csv"
            )
        )
    )
    (setq artue::*cutoff-seconds* cutoff-seconds)
    (setq artue::*max-cost* max-exp-cost)
    (with-open-file (str results-file :direction :output 
            :if-does-not-exist :create :if-exists :append)
        (format str "~%FOIL baseline")
        (format str "~%Category: ~A" category)
        (format str "~%Maximum explanation cost: ~A" max-exp-cost)
        (format str "~%Planning cutoff time: ~A" planning-time-limit)
        (format str "~%Explanation cutoff in seconds: ~A" cutoff-seconds)
        (format str "~%Withheld-events: ~A" withheld-events)
        (format str "~%Training example, Test example, Test File, Start Time, Time Taken, Learning Seconds, Performance Seconds, Unknown Events, Learned Models, Abandoned Models")
    )
    (dotimes (i 25)
        (do-mud-trial results-file "N/A" 0 (1+ i) -1
            :category category
            :planning-time-limit planning-time-limit        
            :category category
            :withheld-events withheld-events
        )
    )
)

(defun do-mud-trial (results-file train-file-num i j learning-time
                     &key (planning-time-limit 60)
                          (withheld-events '(mud-slows-down no-more-mud))
                          (learning-allowed t)
                          (category category)
                     &aux start-time)
    (setq start-time (get-internal-run-time))
    (mud-test-withhold :category category :prob-num j 
                       :planning-time-limit planning-time-limit
                       :withheld-events withheld-events
                       :learning-allowed learning-allowed
    )
    (with-open-file (str results-file :direction :output 
            :if-does-not-exist :create :if-exists :append)
        (format str "~%~A,~A,~A,~A,~A,~3,1F,~3,2F,~3,2F,~A,~A,~A"
            i j 
            (format nil "~a/mud~a.prob" category j)
            (format nil "training/mud~a.prob" train-file-num)
            (get-time-string)
            (second 
                (car 
                    (grep-trees 
                        (list-state-values *cur-state*) 
                        'time-taken
                    )
                )
            )
            learning-time
            (coerce
                (/ (- (get-internal-run-time) start-time) 
                    internal-time-units-per-second
                )
                'double-float
            )
            (length (get-unknown-events (car *plausible-explanations*)))
            (length *learned-event-models*)
            (length (explanation-abandoned-models (car *plausible-explanations*)))
        )
    )
)

(defun learning-curves-experiment (&key (results-file nil) (category "testing")
                                     (planning-time-limit 60) (cutoff-seconds 240)
                                     (max-exp-cost 30)
                                     (reps 10)
                                     (train-ex-count 5)
                                     (test-ex-count 25)
                                     (start-train-num 1)
                                     (withheld-events '(no-more-mud mud-slows-down))
                                &aux (examples nil) (start-time nil) 
                                     (learning-time nil) (train-num 0))
    (when (null results-file)
        (setq results-file 
            (make-pathname :name "mudworld-curves"
                           :type "csv"
            )
        )
    )
    (setq artue::*cutoff-seconds* cutoff-seconds)
    (setq artue::*max-cost* max-exp-cost)
    (with-open-file (str results-file :direction :output 
            :if-does-not-exist :create :if-exists :append)
        (format str "~%Learning-curves FOIL experiment")
        (format str "~%Category: ~A" category)
        (format str "~%Maximum explanation cost: ~A" max-exp-cost)
        (format str "~%Planning cutoff time: ~A" planning-time-limit)
        (format str "~%Explanation cutoff in seconds: ~A" cutoff-seconds)
        (format str "~%Withheld-events: ~A" withheld-events)
        (format str "~%Problem directory: ~A" 
            (make-pathname :directory '(:relative "domains" "mudworld-probs"))
        )
        (format str "~%Training count, Test example, Test file, Training file, Start Time, Time Taken, Learning Seconds, Performance Seconds, Unknown Events, Learned Models, Abandoned Models")
    )
    (dotimes (i test-ex-count)
        (push (1+ i) examples)
    )
    (setq examples (nreverse examples))
    (setq train-num (1- start-train-num))
    (dotimes (rep reps)
        (wipe-memory)
        (dotimes (train-ex train-ex-count)
            (incf train-num)
            (mud-test-withhold :category "training" :prob-num train-num
                               :planning-time-limit planning-time-limit
                               :withheld-events withheld-events
            )
            (setq start-time (get-internal-run-time))
            (learn-from-explanation (car *plausible-explanations*))
            (setq learning-time 
                (coerce
                    (/ (- (get-internal-run-time) start-time) 
                       internal-time-units-per-second
                    )
                    'double-float
                )
            )
            (dolist (i examples)
                (do-mud-trial results-file train-num (+ train-ex 1) i learning-time
                    :category category
                    :planning-time-limit planning-time-limit
                    :withheld-events withheld-events
                )
            )
        )
    )
)

