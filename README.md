# ARTUE/DiscoverHistory

Matthew Molineaux: myfriendmatt [at] gmail.com

## Learn More

Autonomous Response to Unexpected Events (ARTUE) and DiscoverHistory consist of a set of Lisp-based integrated tools designed for agent-based research in goal reasoning and explanation.

### These tools include:

* SHOP2-PDDL+, a continuous time-planner
* DiscoverHistory, an abductive explanation system for inferring past actions and events in continuous time
* FOIL-PS, a learning component used for inference of unknown events based on exploration
* an unnamed rule-based motivation system for goal formulation and management
* a simulation system that executes SHOP2-PDDL+ planning domains as a single- or multi-agent system
* ARTUE/DHAgent, an intelligent software agent that interfaces with and uses these components together to achieve goals in a set of domains.

All of these are domain-independent general reasoning algorithms, and their primary purpose is to act as part of a cognitive system for agents that interact directly with their environment. The Naval Research Labratory (Code 5514) has published, together with colleagues at Knexus Research Corporation, a series of scientific publications detailing experiments with this software.

---

The accompanying codebase includes the following directories:

* asdf/: source code for Another System Definition Facility, a standard Lisp library for management of software libraries.
* deductive-explainer/: Lisp source code for an alternative explanation system that uses deductive inferences only
* domains/:
  1. Lisp main functions for running reported experiments using the ARTUE/DiscoverHistory code base.
  2. Planning domains, HTN decomposition trees, and problem sets for evaluation of ARTUE/DiscoverHistory in various domains.
* learning/: Lisp source code for FOIL-PS
* main/: Lisp source code for ARTUE/DHAgent, general experimentation; relies on and ties together other modules
* motivations/: Lisp source code for a rule-based system that selects and prioritizes goals based on a current environment state
* full-explainer/: Lisp source code for the DiscoverHistory abductive explanation algorithm
* shop2/: Lisp source code for the (modified) SHOP2 planning system
* shop2ext/: Lisp source code providing extensions to SHOP2 for continuous-time planning
* utils/: Library of common basic functions provided to other modules; includes search, but no novel reasoning algorithms.
