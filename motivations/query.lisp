 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: 2009
 ;;;; Project Name: ARTUE
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE. 
 ;;;;

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

 ;;;; Backchaining rule system as in the Navy Strategy Simulation paper.

(in-package :artue)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; backchainer

(defun do-query (query facts rules)
  "Figures out if formula is true, backchaining if necessary."
  (when (eq (car query) 'not)
      ; (format t "~% Reversing query.")
      (if (do-query (second query) facts rules)
          ;then
          (return-from do-query nil)   ; no compatible bindings
          ;else
          (return-from do-query '(())) ; one empty set of bindings
      )
  )
  ; works, but slow -- MAW
  (when (eq (car query) 'or)
		; (format t "~% Multiple queries.")
		(return-from
			do-query
			(loop
				for clause in (cdr query)
				for v = (do-query clause facts rules)
				when v append v
			))
	)
	(when (eq (car query) 'and)
		; (format t "~% Making new rule.")
		(let* ((queryname (gensym))
				(rules (cons (makerule queryname (cdr query)) rules)))
			; (format t "~% New rule: ~A" (car rules))
			(return-from
				do-query
				(do-query `(,queryname) facts rules)))
	)
  (let ((bindings (ask query facts :bindings)))
    ;; Backchain:
    (dolist (rule (rules-for-query query rules))
      (let ((new-bindings 
	     (query-rule rule query rules facts)))
	(setq bindings 
	      (eliminate-redundant-bc-answers bindings new-bindings))))
    bindings))

(defun makerule (queryname query)
	(append `(<== (,queryname)) query))

(defun rules-for-query (query rules)
  (remove-if (lambda (rule)
	       (eq (simple-unify query (cadr rule)) :FAIL))
	     rules))

(defun query-rule (rule query rules facts)
  (let ((clause-bindings (simple-unify query (consequent rule)))
	(antes (antes rule)))
    (let ((bindings ;;; MEK: This holds the final return bindings 
	   (do-query (sublis clause-bindings (car antes)) facts rules)))
      (when bindings
        (setq bindings 
	      (increment-solution bindings clause-bindings))
        (dolist (this-ante (rest antes)) ; Combine subsequent answers
          (let ((term-bindings nil))
            ;; Search for consistent extensions to solutions so far:
            (dolist (prior-bindings bindings)
              (let ((new-bindings
		     (do-query (sublis prior-bindings this-ante)
		       facts rules)))
		(let ((incremented-bindings
		       (increment-solution new-bindings prior-bindings))) ;; new-antes ante-list)
		  (setq term-bindings (nconc term-bindings incremented-bindings))
		  )))
            (if term-bindings
		(setq bindings term-bindings)
		(return-from query-rule (values nil)))
            ))
	(lift-solutions bindings clause-bindings query)
	;;MEK [8/13/2009]: should clean up the bindings to not include variables
	;;MEK [8/13/2009]: not in the consequent
	))))

(defun consequent (rule)
  (cadr rule))

(defun antes (rule)
  (cddr rule)) 



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; a simplified version of ask

(defun ask (query facts response)
  (cond
    ((outsourced-pred? (car query))
     (make-response-from-bindings
      (handle-outsourced-query query)
      query
      response))
    (t 
     (make-response-from-bindings
      (bind-to-fact-list query facts)
      query
      response))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; An extremely poor mans evaluation subsystem

(defparameter *outsourced-preds*
  '(evaluate
    assign
    distinct))

(defun outsourced-pred? (pred)
  (member pred *outsourced-preds*))

(defun handle-outsourced-query (query)
  (case (car query)
    (evaluate
     (handle-evaluate-query query))
    (assign
     (handle-assign-query query))
    (distinct
     (handle-distinct-query query))
    (t nil)))

(defun handle-evaluate-query (query)
   (if (evaluate (second query))
       ;then 
       (list nil)
       ;else
       nil
   )
)
       

(defun handle-assign-query (query)
  (let ((var (second query))
	(ans (evaluate (third query))))
    (when ans
      (cond ((artue::variable? var)
	     (list (list (cons var ans))))
	    ((equal var ans)
	     '(nil))
	    (t nil)))))

(defun handle-distinct-query (query)
  (when (some #'artue::variable? (cdr query))
      (error "Unbound variable found in distinct query: ~A" query)
  )
  (if (> (length (cdr query)) (length (remove-duplicates (cdr query))))
      ;then
      '()
      ;else
      '(())
  )
)

;;;This should branch more
(defun evaluate (form)
  (cond ((null form) nil)
        ((numberp form) form)
        ((atom form) form) ; `(quote ,form)?
;        ((and (listp form)
;	      (quantity? form))
;	 (value-of-quantity form))
	((listp form)
         (evaluate-expression form))
        (t form)))

(defun evaluate-expression (expression)
  (let ((fn (case (car expression) 
	      (LessThan-UnitValuesFn #'handle-less-than-fn)
	      (DifferenceFn #'handle-difference-fn)
	      (t :eval)))
	(arg-values (evaluate-arguments (cdr expression))))
    (if (eq fn :eval)
	(apply (car expression) arg-values)
	(apply fn arg-values))))

(defun evaluate-arguments (args)
  (mapcar (lambda (arg)
	    (evaluate arg))
	  args))

(defun handle-less-than-fn (arg1 arg2)
  (if (< arg1 arg2)
      'True
      'False))

(defun handle-difference-fn (arg1 arg2)
  (- arg1 arg2))

;; MEK [8/20/09] - This should be driven by the domain file
;; Instead I'm using the state to know what the quantities are
(defun quantity? (quantity)
  (value-of-quantity quantity)
  )

;; This hsould be conciser
(defun value-of-quantity (quantity)
  (error "needs state")
  (cond ((ask `(valueOf ,quantity ?val) *state* '?val)
	 (car (ask `(valueOf ,quantity ?val) *state* '?val)))
	((ask `(valueOf (AtFn ,quantity ?time) ?val) *state* '?val)
	 (car (ask `(valueOf (AtFn ,quantity ?time) ?val) *state* '?val)))
	((ask (cons 'is (list (append quantity '(?val))))
		   *state* '?val)
	 (car (ask (cons 'is (list (append quantity '(?val))))
		   *state* '?val)))
	((eql (car quantity) 'FullEnergyFn) 100)
	(t 
	 nil
	 ;;(error "don't know how to compute value for quantity ~A" quantity)
	 )))
	 
(defun bind-to-fact-list (query facts)
  (generate-bindings query facts))
    

(defun generate-bindings (query facts)
  (remove :FAIL
	  (mapcar (lambda (fact)
		    (let ((ans1 (simple-unify query fact))
			  (ans2 (simple-unify `(is ,query) fact)))
		      (cond ((not (eq ans1 :FAIL)) ans1)
			    ((not (eq ans2 :FAIL)) ans2)
			    (t :FAIL))))
		  facts)))

;;;query '(isa ?x MotivationType)
;;;bindings (((?x . RechargeBattery)))
;;;response if nil then return the facts, if a pattern, then bind the pattern
(defun make-response-from-bindings (bindings query &optional response)
  (cond ((null response)
	 (mapcar (lambda (binding)
		   (sublis binding query))
		 bindings))
	((eq response :bindings) 
	 bindings)
	(t 
	 (mapcar (lambda (binding)
		   (sublis binding response))
		 bindings))))








;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; utils

;;; Extensions is list of binding lists.
;;; Solution is a single binding list.
;;; Since these are shared, we need to copy them
;;; ARGUMENTS:
;;;  bl-extensions   - a list of binding lists consistent with solution
;;;  solution        - a single binding list
;;;; MEK: I'm not concerned with passing the fact list around now
;;;  fact-list       - a flat list of facts corresponding to bl-extension
;;;  antecedent-list - a flat list of term antecedents
;;; RETURNS:
;;;  bindings - a list of binding lists
;;;  antes    - a list of lists of believed facts.
(defun increment-solution (bl-extensions solution );; fact-list antecedent-list)
  (let ((bindings (mapcar #'(lambda (extension)
                              (merge-bindings extension solution)) 
                    bl-extensions)))
;;        (antes (mapcar #'(lambda (fact)
;;                           (append antecedent-list (list fact)))
;;                 fact-list)))
    bindings))
;;    (values bindings antes)))

;;; The bindings are from the same side.  Can't unthread.

(defun merge-bindings (blist1 blist2)
  (let ((result blist1))
    (dolist (binding blist2 result)
      (unless (assoc (first binding) blist1)
        (push binding result)))))

;;; maybe formula-bindings is useless because I am not doing left and right unifies?
(defun lift-solutions ( binding-lists formula-bindings query )
  ;; RETURNS:
  ;;   real-solutions - a list of binding lists of the variables in the query
  (let ((formula-bindings (extend-bindings-if-necessary formula-bindings query ))
	(real-solutions nil)
	(query-vars (vars-in-query query)))
    (dolist (solution-bindings binding-lists)
      ;; Construct the solution in terms of answer variables
      (let ((consequent-bindings 
             (extract-consequent-bindings formula-bindings solution-bindings query-vars)))
        (unless (member consequent-bindings real-solutions :test 'matching-bl)
	  (push consequent-bindings real-solutions) ; fully unthreaded
	  )))
    real-solutions))

;;; Bind output variables to themselves so they can flow up from subqueries
;;; This is used in the subexpressionMatching test in the chainer regression.
(defun extend-bindings-if-necessary ( blist query)
  (dolist (var (vars-in-query query))
    (unless (assoc var blist)
      ;(format t "~%extending binding list with ~s" (cons var var))
      (setq blist (cons (cons var var) blist))))
  blist)


(defun vars-in-query (query)
  (cond ((artue::variable? query) (list query))
	((atom query) nil)
	((consp query)
	 (append (vars-in-query (car query))
		 (vars-in-query (cdr query))))))

;;; formula-vars is not really true as we don't have left and right bindings
(defun extract-consequent-bindings (formula-bindings antecedent-bindings query-vars)
  (let ((bindings (unthread-bindings (copy-tree formula-bindings) antecedent-bindings)))
    (remove-local-vars
     (remove-cyclical-bindings bindings antecedent-bindings)
     query-vars
    )))

;;;removes variables not necessary to the query
(defun remove-local-vars (bindings query-vars)
  (remove-if-not (lambda (binding) 
			  (member (car binding) query-vars))
			bindings))

(defun remove-cyclical-bindings (lbindings rbindings)
  (let ((bindings nil))
    (dolist (binding lbindings)
      (unless (and (artue::variable? (cdr binding))
                   (eq (cdr (assoc (cdr binding) rbindings)) (car binding)))
        (push binding bindings)))
    (nreverse bindings)))




;;; Traverse back and forth between binding lists to find ground bindings,
;;;  if they exist, being careful not to recurse forever.
;;; Beware! unthread-bindings is destructive.  This is not a problem within
;;; unification, because it gets newly consed bindings, but if you call it
;;; externally, call copy-tree on both arguments if you don't want to 
;;; side-effect the lists.
(defun unthread-bindings (bindings other-bindings)
  (dolist (sub bindings)
    (when sub
      (setf (cdr sub) (unthread-binding (cdr sub) other-bindings bindings (list (car sub)) nil))))
  (remove-if #'(lambda (entry) (eq (car entry) (cdr entry))) bindings))

(defun unthread-binding (exp bindings other-bindings stack other-stack)
  (cond ((artue::variable? exp)
         (unthread-variable exp bindings other-bindings stack other-stack))
        ((consp exp)
         (cons (unthread-binding (car exp) bindings other-bindings stack other-stack)
               (unthread-binding (cdr exp) bindings other-bindings stack other-stack)))
        (t exp)))

(defun unthread-variable (var bindings other-bindings stack other-stack)
  (let ((sub (cdr (assoc var bindings))))
    (cond ((null sub) var)
          ((member var other-stack) ; halt infinite-recursion
           sub)
          (t (unthread-binding sub other-bindings bindings (cons var other-stack) stack)))))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; query tests

(defun run-all-query-tests ()
  (do-backchain-test
      "Motivations Test"
    '((<== (unexpectedDecrease ?quantity)
       (expects (valueOf ?quantity ?expected-value))
       (valueOf ?quantity ?actual-value)
       (evaluate True (LessThan-UnitValuesFn ?actual-value ?expected-value))))
    '((expects (valueOf (AtFn (energy Rover-1) (TimeStepFn 3)) 
		  60)) ;;should be a units here 
      (valueOf (AtFn (energy Rover-1) (TimeStepFn 3))
       50))
    '(((unexpectedDecrease (AtFn (energy Rover-1) (TimeStepFn 3)))
       (nil))))
  (do-backchain-test
      "Progressive Binding to Constants"
    '((<== (conse ?X ?Y ?Z)
       (ante1 ?X)
       (ante2 ?X ?Y)
       (ante3 ?X ?Y ?Z)))
    '((ante1 Foo)
      (ante2 Foo Bar)
      (ante3 Foo Bar Baz))
    '(((conse ?what ?where ?why) 
       (((?what . Foo) (?where . Bar) (?why . Baz))))))
  (do-backchain-test
      "Multiple Clauses"
    '((<== (Mortal ?x)
       (Human ?x))
      (<== (Human ?x)
       (Sentient ?x))
      (<== (Sentient ?x)
       (Robot ?x)
       (FIRE-based ?x)))
    '((Human Fred)
      (Robot Robbie)
      (FIRE-based Robbie))
    '(((Mortal Robbie) (nil))
      ((Human Robbie) (nil))
      ((Mortal ?who) (((?who . Robbie))
		      ((?who . Fred))))
      ((Sentient ?who) (((?who . Robbie)))))))
  
(defun do-backchain-test (name rules state query-results)
  (let ((pass? t))
    (dolist (query-result query-results )
      (let ((ans
	     (do-query (car query-result) state rules)))
	(unless (passes? ans (cadr query-result))
	  (setq pass? nil)
	  (warn "~% ~A test fialed expected ~A got ~A" 
		name (cadr query-result) ans))))
    pass?)) 
  



(defun passes? (result desired-result)
  (cond ((null desired-result) (null result))
        ((equal desired-result '(nil))
         (and (consp result)
              (null (car result))))
        ((list-of-binding-lists? desired-result)
         (matching-bls result desired-result))))


(defun list-of-binding-lists? (desired-result)
  (and (consp desired-result)
       (every #'binding-list? desired-result)))

(defun binding-list? (bl)
  (and (consp bl)
       (every #'bl-pair? bl)))

(defun bl-pair? (pair)
  (and (consp pair)
       (artue::variable? (car pair))))

;;; Ugh! Set matching
(defun matching-bls (bl1 bl2)
  (or (equal bl1 bl2)
      (progn (setq bl1 (remove-duplicates bl1 :test #'equal))
        (and (eql (length bl1) (length bl2))
             (every #'(lambda (bl) (member bl bl2 :test #'matching-bl)) bl1)))))

(defun matching-bl (bl1 bl2)
  (or (equal bl1 bl2)
      (and (listp bl1) (listp bl2)
           (eql (length bl1) (length bl2))
           (every #'(lambda (bl) (member bl bl2 :test #'equal))
                  bl1))))

(defun eliminate-redundant-bc-answers (bindings new-bindings )
  (let ((merged-bindings bindings))
    (dolist (new-blist new-bindings)
      (unless (some #'(lambda (prior-blist)
                        (matching-bl prior-blist new-blist))
                    bindings)
        (push new-blist merged-bindings)))
    merged-bindings))

;; Simple unify functions moved here from goal-generation.lsp 7/1/2013 MCM
(defun simple-unify (a b &optional (bindings nil))
   (cond ((equal a b) bindings)
     ;MCM (09/18) Added special case for %any, which should not match to a variable.
     ((eq a '%any) (eq b '%any))
     ((eq b '%any) nil)
	 ((variable? a) (unify-variable a b bindings))
	 ((variable? b) (unify-variable b a bindings))
	 ((or (not (listp a)) (not (listp b))) :FAIL)
	 ((not (eq :FAIL (setq bindings
			       (simple-unify (car a) (car b) bindings))))
	  (simple-unify (cdr a) (cdr b) bindings))
	 (t :FAIL)))

(defun unify-variable (var exp bindings &aux val)
  ;; Must distinguish no value from value of nil
  (setq val (assoc var bindings))
  (cond (val (simple-unify (cdr val) exp bindings))
	;; If safe, bind <var> to <exp>
	((free-in? var exp bindings) (cons (cons var exp) bindings))
	(t :FAIL)))

(defun variable? (x)
  (and (symbolp x)	;A symbol whose first character is "?"
       (char= #\? (elt (symbol-name x) 0))))

(defun free-in? (var exp bindings)
  ;; Returns nil if <var> occurs in <exp>, assuming <bindings>.
  (cond ((null exp) t)
	((equal var exp) nil)
	((variable? exp)
	 (let ((val (assoc exp bindings)))
	   (if val 
	       (free-in? var (cdr val) bindings)
	     t)))
	((not (listp exp)) t)
	((free-in? var (car exp) bindings)
	 (free-in? var (cdr exp) bindings))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; End of Code
