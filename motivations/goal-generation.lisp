 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: 2009
 ;;;; Project Name: ARTUE
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE. 
 
 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file includes lisp functions for goal generation from 
 ;;;; motivation rules, as in the Navy Strategy Simulation paper.

(in-package :artue)
(export 
    '(defmotivations <== believed is was isa expected
        motivationcondition motivationintensity motivationgoal 
        motivationparticipanttype motivationtype
        highmotivationalintensity lowmotivationalintensity
     )
)


;;;; Notes: requires atms-unify.lsp, query.lsp, rover-test.meld, rover-motivation.meld=


;; Goal Generation makes use of motivations after a discrepency has 
;; been detected in the state 
;;
;; Algorithm
;; Inputs: Current State, Discrepencies, Explanations, Motivations
;; For each motivation
;; - Attempt to bind participants to observed state constrianed by conditions
;; - If any of the statements use a Discrepency or Explanation,
;;   Generate the corresponding goal
;; Output: should be a list of goals and the intensity
 
;(defparameter *state* 'RoverMotivationTest-1 "this holds the mt for the current state")
;(defparameter *motivation-dt* 'RoverMotivationsMt 
;  "holds the mt for definition of the motivations")

;;; These are not really used any more
; (defparameter *motivation-path* "c:\\nrl\\prego\\motivations\\")
; (defparameter *state-file* 
  ; (concatenate 'string *motivation-path* "rover-test-state.meld"))



;(defparameter *state* nil
;  "this holds a list of the facts which make up the state")

(defparameter *motivations-dt*
  nil "this holds a list of the motivation facts")

(defparameter *dt-rules* nil)

(defclass motivation-system ()
    (
        (state
            :reader get-st
            :initarg :state
        )
        (rules 
            :reader get-rules
            :initarg :rules
        )
        (motivations
            :reader get-motivations
            :initarg :motivations
        )
    )
)

(defun generate-goals (state mot-kb dt-rules &aux ms)
    (setq state (clean-state state))
    (setq ms
        (make-instance 'motivation-system
            :state state
            :rules dt-rules
            :motivations mot-kb
        )
    )
    (mapcan #'(lambda (mot) (generate-goals-for-motivation ms mot)) 
        (motivations mot-kb)
    )
)

(defmacro defmotivations (motivation-objs motivation-rules)
    `(progn 
        (setq *motivations-dt* ',motivation-objs)
        (setq *dt-rules* ',motivation-rules)
     )
)


(defun generate-goals-for-motivation (ms motivation)
    (instantiate-motivations
        ms
        motivation
        (filter-blists-by-conditions
            ms
            (motivation-conditions motivation (get-motivations ms))
            (possible-bindings-by-type 
                (motivation-participants motivation (get-motivations ms))
                (get-st ms)
            )
        )
    )
)

(defun instantiate-motivations (ms motivation blists)
  (let ((goals (motivation-goal motivation (get-motivations ms)))
	(intensity (car (motivation-intensity motivation (get-motivations ms)))))
    (mapcar (lambda (blist)
	      (list (sublis blist goals)
		    (create-quantitative-intensity-fn
		     (first intensity)
		     (sublis blist (second intensity)))))
	    blists)))

;;; New way returns a function which takes 
;;; 1 argument a list of facts representing the state
(defun create-quantitative-intensity-fn (qualitative-intensity expression)
  #'(lambda (state)
      (let ((*state* state))
	(compute-quantitative-intensity qualitative-intensity expression))))
  
;;; Old way returns a static value for the motivation
(defun compute-quantitative-intensity (qualitative-intensity expression)
  (let ((quant-intensity (evaluate expression)))
    (cond ((eql qualitative-intensity 'HighMotivationalIntensity)
	   (* 100 quant-intensity))
	  (t quant-intensity))))

(defun filter-blists-by-conditions (ms conditions blists)
    (apply #'append
        (mapcar 
            #'(lambda (blist)
                (apply #'append
                    (mapcar 
                        #'(lambda (condition)
                            (mapcar 
                                #'(lambda (query-results)
                                    (sublis query-results blist)
                                  )
                                (do-query condition (get-st ms) (get-rules ms))
                            )
                          )
                        (map-out conditions blist)
                    )
                )
              )
            blists
        )
    )
)

(defun map-out (conditions bindings)
    (let
      ((bound-conditions nil))
        (dolist (cur-cond conditions)
            (setq bound-conditions (append bound-conditions (find-all cur-cond bindings)))
        )
        bound-conditions
    )
)

(defun find-all (cur-cond bindings &aux bound-conditions)
    (cond
        ((null cur-cond) (setq bound-conditions (list nil)))
        ((listp (car cur-cond)) 
            (setq bound-conditions
                (append 
                    (find-all (car cur-cond) bindings)
                    (find-all (cdr cur-cond) bindings)
                )
            )
        )
        ((and (symbolp (car cur-cond)) (keywordp (car cur-cond)))
            (dolist (sub (mapcar #'cdr 
                            (remove (car cur-cond) bindings :test-not #'eq :key #'car)
                         )
                    )
                (dolist (fact (find-all (cdr cur-cond) bindings))
                    (push (cons sub fact) bound-conditions)
                )
            )
            (when (null (find (car cur-cond) bindings :test #'eq :key #'car))
                (dolist (fact (find-all (cdr cur-cond) bindings))
                    (push 
                        (cons 
                            (intern (concatenate 'string "?" (symbol-name (car cur-cond))))
                            fact
                        )
                        bound-conditions
                    )
                )
            )
        )
        ((symbolp (car cur-cond))
            (dolist (fact (find-all (cdr cur-cond) bindings))
                (push (cons (car cur-cond) fact) bound-conditions)
            )
        )
    )
    bound-conditions
)
                
            

;;; MRK [8/13/2009]: use the bindings list code from query.lsp...but not now
;;; MEK: This function is wrong
;;;  Iterate over each participant type
;;;  Find all the possible bindings
;;; Return a list of binding lists
(defun possible-bindings-by-type (participant-types state)
  (cond 
    ((null participant-types) '(nil))
    ((member (third (car participant-types)) '(real int))
        (extend-blists
            (list (list (cons (first (car participant-types)) (gentemp "?V"))))
            (possible-bindings-by-type (cdr participant-types) state)
        )
    )
	((ask
	  `(isa ?obj ,(third (car participant-types)))
	  state (cons (car (car participant-types)) '?obj)
     )
	 (extend-blists ;;not sure why ask is not returning a list of bindings lists
        (mapcar #'list
           (ask
			  `(isa ?obj ,(third (car participant-types)))
			  state (cons (car (car participant-types)) '?obj)
           )
        )
		(possible-bindings-by-type (cdr participant-types) state)
     )
    )
	(t (possible-bindings-by-type (cdr participant-types) state))
  )
)
 
(defun extend-blists (bindings blists)
  (let ((results nil))
    (dolist (prior-bindings blists results)
      (setq results (nconc (increment-solution bindings prior-bindings) results)))))
;  (if blists
;      (mapcan (lambda (blist)
;		(mapcar (lambda (binding) 
;			  (cons binding blist))
;			bindings))
;	      blists)
;      bindings))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; motivation definitions

(defun motivation-participants (motivation mot-kb)
  (ask `(motivationParticipantType ,motivation ?var ?role-relation ?type)
		     mot-kb '(?var ?role-relation ?type)))
;;  (fire::ask-it `(motivationParticipantType ,motivation ?var ?role-relation ?type)
;;		:context *motivation-dt* :response '(?var ?role-relation ?type)))

(defun motivation-goal (motivation mot-kb)
  ;;;assumes only one generated goal
  (car (ask `(motivationGoal ,motivation ?goal)
		     mot-kb '?goal)))
;;  (fire::ask-it `(motivationGoal ,motivation ?goal)
;;		:context *motivation-dt* :response '?goal))

(defun motivation-conditions (motivation mot-kb)
  (ask `(motivationCondition ,motivation ?condition)
		     mot-kb '?condition))
;;  (fire::ask-it `(motivationCondition ,motivation ?condition)
;;		:context *motivation-dt* :response '?condition)

(defun motivation-intensity (motivation mot-kb)
  (ask `(motivationIntensity ,motivation ?qualitative-intensity ?quantitative-intesity)
		     mot-kb '(?qualitative-intensity ?quantitative-intesity)))

(defun motivations (mot-kb)
  (ask `(isa ?x MotivationType)
		     mot-kb '?x))

;;  (fire::ask-it `(isa ?x MotivationType) 
;;		:context dt :response '?x)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; read-file-to-variable

(defun clean-state (state)
  (append
   (mapcar (lambda (fact)
	     (cond ((eql (car fact) 'IS-A)
		    (cons 'isa (cdr fact)))
		   (t fact)))
	  state)
   (add-coordinate-facts state)))

;;; This could be added to the constraint information, but really
;;; what are coordinates doing in motivations.
(defun add-coordinate-facts (state)
  (let ((new-facts nil))
    (dolist (fact state new-facts)
      (let ((bindings (simple-unify fact
				'(believed (is (sub_location ?sub ?x ?y))))))
	(unless (eql bindings :fail)
	  (pushnew `(isa ,(sublis bindings '?x) XCoordinate) new-facts)
	  (pushnew `(isa ,(sublis bindings '?y) YCoordinate) new-facts)
            )))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;debugging funtions

(defun trace-motivations ()
  (trace ASK DO-QUERY POSSIBLE-BINDINGS-BY-TYPE FILTER-BLISTS-BY-CONDITIONS
     INSTANTIATE-MOTIVATIONS GENERATE-GOALS-FOR-MOTIVATION
     GENERATE-GOALS))

(defun untrace-motivations ()
  (untrace ASK DO-QUERY POSSIBLE-BINDINGS-BY-TYPE FILTER-BLISTS-BY-CONDITIONS
     INSTANTIATE-MOTIVATIONS GENERATE-GOALS-FOR-MOTIVATION
     GENERATE-GOALS))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; End of Code

