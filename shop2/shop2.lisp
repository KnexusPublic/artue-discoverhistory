(in-package :shop2)

;; replaced with a constant that is defined in the ASDF system definition. [2007/10/24:rpg]
;;(defparameter *version* "SHOP2 version 2.0 alpha")

(defconstant +shopyright+
"Copyright (C) 2002  University of Maryland.
Modifications by SIFT, LLC personnel Copyright (C) 2004-2007 SIFT, LLC.
This software is distributed on an \"AS IS\" basis, WITHOUT WARRANTY OF ANY
KIND, either express or implied.  This software is distributed under 
the GPLv3 license.  For details, see the software source file.")

;;; The Original Code is SHOP2.
;;; 
;;; The Initial Developer of the Original Code is the University of
;;; Maryland. Portions created by the Initial Developer are Copyright (C)
;;; 2002,2003 the Initial Developer. All Rights Reserved.
;;;
;;; Additions and modifications made by Robert P. Goldman, John
;;; Maraist.  Portions created by Drs. Goldman and Maraist are
;;; Copyright (C) 2004-2007 SIFT, LLC.
;;; 
;;; Contributor(s):
;;;    Dana S. Nau (UMD)
;;;    Yue Cao (???)
;;;    Tsz-Au Chiu (UMD)
;;;    Okhtay Ilghami (UMD)
;;;    Ugur Kuter (UMD)
;;;    Steve Mitchell (???)
;;;    J. William Murdock (IBM Research)
;;;    Robert P. Goldman (SIFT, LLC)
;;;    John Maraist (SIFT, LLC)
;;;    Matthew Molineaux (Knexus Research Corporation)
;;; 

;;; For modifications made to SHOP2 under the Integrated Learning Contract:
;;; ----------------------------------------------------------------------
;;; Smart Information Flow Technologies Copyright 2006-2007 Unpublished work
;;; 
;;; GOVERNMENT PURPOSE RIGHTS
;;; 
;;; Contract No.         FA8650-06-C-7606, 
;;; Contractor Name      Smart Information Flow Technologies, LLC
;;;                      d/b/a SIFT, LLC
;;; Contractor Address   211 N 1st Street, Suite 300
;;;                      Minneapolis, MN 55401
;;; Expiration Date      5/2/2011
;;; 
;;; The Government's rights to use, modify, reproduce, release,
;;; perform, display, or disclose this software are restricted by
;;; paragraph (b)(2) of the Rights in Noncommercial Computer Software
;;; and Noncommercial Computer Software Documentation clause contained
;;; in the above identified contract. No restrictions apply after the
;;; expiration date shown above. Any reproduction of the software or
;;; portions thereof marked with this legend must also reproduce the
;;; markings.
;;; ----------------------------------------------------------------------


 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE. 
 
 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; (at your option) any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; -----------------------------------------------------------------------
;;; REVISION LOG

; ****************** Revisions since SHOP2 1.2 Release

;;;  [2007/04/30:jm] Dividing the main SHOP2 Lisp file to separate the
;;;  theorem prover, and providing the prover as a separate package.

;;;  [2005/05/26:rpg] Substantial changes that can be found in the CVS
;;;  changelog.
;;   [2004/12/30:rpg] An enormous modification for check-in to
;;   sourceforge.  This set of changes contains modifications to
;;   improve the return of plan trees from SHOP2; the introduction of
;;   LIST type DEFSTRUCTS (STATE, OPERATOR, and OPERATOR-NODE) to
;;   provide named accessors to data structures (makes a little easier
;;   to understand for now, could be an intermediate step towards more
;;   efficient implementation using true DEFSTRUCTs later); added
;;   problem-state and problem-tasks "accessor" functions to make
;;   PROBLEM implementation more abstract; added an ASSIGN*
;;   pseudo-logical operator that provides a multiple-binding
;;   equivalent to ASSIGN; modified the SHOP-TRACE mechanism (see
;;   change to SHOP-TRACE argument pattern); added a (rudimentary)
;;   manually-directed planning option; fixed a problem with
;;   retrieving preconditions with already-bound variables; made it
;;   possible to extract a state-trajectory, as well as an operator
;;   sequence and plan tree from FIND-PLANS; also folded in mods from
;;   unreleased UMD 1.3 beta version.

;;   2004.04.05 (rpg) Made functions that encapsulated the choice of
;;   next task to plan for, so that they could be replaced or wrapped
;;   to customize the behavior of SHOP2.  See TASK-SORTER and
;;   CHOOSE-IMMEDIATE-TASK.
;;   Changed the binding of *CURRENT-STATE* to happen earlier, in
;;   SEEK-PLANS, instead of later (in FIND-SATISFIERS).

;;   2004.02.07 (uk) Fixed the variable binding problem in function
;;   MAKE-PROBLEM, that was reported by Robert Goldman as a bug in
;;   DEFPROBLEM macro. The function MAKE-PROBLEM is helpful for backward-
;;   compatability with SHOP -- the earlier version of SHOP2.

;;   2004.02.06 (rpg) Fixed problems in variable-binding interacting
;;   with returning trees.  Added new TREE-STORE object argument to the
;;   SEEK-PLANS-<foo> functions.  Also added code to FIND-PLANS to
;;   recover those trees.  This is only an interim modification: the
;;   original functionaly (although buggy) has not been removed yet.

;;   2003.11.17 (jwm) Provisional modification to the way protections
;;   are handled: protections can now be arbitrary logical expressions
;;   (including references to axioms).  Since this capability creates
;;   the possibility that an addition to a state could violate a
;;   protection, an additional change was required so that protections
;;   are now checked after deletes AND adds (before they were checked
;;   after deletes but before adds).  This has the side-effect that an
;;   operator that deletes and then adds back a protected atom does
;;   not constitute a protection violation (it's not clear that the
;;   former behavior was desirable anyway).

;;   2003.11.04 (rpg) Moved SHOP-TRACE comments into the function as a document
;;   string.

;;   2003.11.03 (rpg) Added an ASSIGN* intended to make it possible to have a
;;   lisp function compute multiple possible bindings for a variable.


; ****************** Revisions since the SHOP2 1.1 Release

;;   2003.9.17 (jwm) Revised the :external keyword (see below); now if
;;   external access fails, an attempt is made to prove the reference
;;   internally.

;;   2003.9.2 (jwm) Modified the call to plan-found-hook (see below)
;;   to send the final plan rather than the internal partial plan.

;;   2003.8.1 (jwm) Added reporting of method preconditions for
;;   shop-trace of methods.

;;   2003.7.17 (jwm) Fixed a minor bug with the shop-trace code
;;   that was causing it to incorrectly report that it was
;;   backtracking from immediate tasks when it actually had
;;   completed planning.

;;   2003.7.9 (jwm) Revised the external-access-hook mechanism (see
;;   the 2003.4.21 revision, below).  There is a new keyword for the
;;   theorem prover: :external.  Any expression that begins with
;;   :external has the following semantics: If there is an
;;   external-access-hook defined, any attempt to prove (:external A B
;;   C ...)  will return the results of running external-access-hook
;;   on the list '(A B C ...).  If there is no external-access-hook
;;   defined, any attempt to prove (:external A B C ...) will return
;;   nil.  In addition, assertions obtained from calling the
;;   external-access-hook are added to the internal state.

;;   2003.6.16 (jwm) There is a new hook routine, trace-query-hook.
;;   If a routine with that name is defined, whenever the shop-trace
;;   module is invoked, that hook routine is invoked on whatever trace
;;   information is available in that point (including the logical atoms
;;   in the current state).
;;   This functionality was originally created for John Shin's SHOP2
;;   graphical user interface.  In general, we expect it would be
;;   generally useful for an external system that needs access to lots
;;   of internal SHOP2 information.

;;   2003.4.21 (jwm) Added code to the theorem proving mechanism that
;;   invokes a routine called external-access-hook iff one is defined.
;;   This hook is intended to allow SHOP2 users to supply their own
;;   mechanisms for accessing external data structures.  If no
;;   external-access hook routine is supplied, this change has no
;;   effect.

;;   2003.4.1 (jwm) Added new keyword argument to SHOP2, :explanation.
;;   Defaults to nil; when true SHOP2 adds extra information at the
;;   end of each operator explaining how the preconditions for that
;;   operator were satisfied.  Currently supports only logical atoms,
;;   and, & or (no forall, not, eval, etc.).

;;   2003.3.4 (jwm) Enforced consistent capitalization of variables
;;   and function names (important for using SHOP2 with case-sensitive
;;   Lisp).  SHOP2 has not been extensively tested using
;;   case-sensitive Lisp, however.

; ****************** Revisions since the SHOP2 1.0 Release

;;   2002.2.8 (jwm) Fixed some minor issues that were requiring that
;;   you load SHOP2 before compiling it.  You don't have to do that
;;   any more.

;;   2002.12.20 (jwm) Tiny bug fix involving debugging code that
;;   didn't work with one of the state encodings (:bit).

;;;  2002.12.15 (jwm) Fixed the previous bug fix (see 2002.12.12)

;;;  2002.12.12 (jwm) Fixed subtle bug that was caused by unexpected
;;;  interaction between the new plan tree code (see 2002.9.27 below)
;;;  and existing (bad) delete-task-top-list code.

;;;  2002.11.1 (jwm) Fixed bug in retract-state-changes that was
;;;  causing errors when an operator attempted to delete and add the
;;;  same atom to the state (the error only arises during
;;;  backtracking).

;;;  2002.9.27 (jwm) Added new keyword parameter :plan-tree to
;;;  find-plans.  It defaults to nil; if true, the first return value
;;;  is a _list_ containing (1) the complete task decomposition tree
;;;  for the plan and (2) the plan itself.

;;;  2002.8.16 (jwm) Added code to seek-plans-null which invokes a
;;;  function called plan-found-hook if such a function is defined.
;;;  Useful for larger systems that invoke SHOP2 and want to perform
;;;  some action whenever a plan is found.

;;;  2002.7.12 (jwm) Added new global variable *all-problems* which stores
;;;  all of the problems that have been defined by defproblem or make-problem.

; ****************** Revisions prior to SHOP2 1.0 Release

;;;  2002.5.24 (jwm) Fixed bug involving operators that try to add
;;;  atoms to the state that are already true or delete operators from
;;;  a state that were already false; those operators caused strange
;;;  problems when backtracking.

;;;  2002.5.10 (jwm) Added new :state keyword to find-plans which
;;;  takes the same arguments that encode-states used to;
;;;  encode-states is no longer user.  I also fixed strip-nops so that
;;;  iNOP's are no longer included in the final plan.  I also fixed
;;;  eval terms (eval in the task list of a method).

;;;  2002.5.10 (dsn)  Removed the old verbosity code (since it is superseded by
;;;  SHOP-TRACE), and replaced it with code for :STATS, :PLANS, :LONG-PLANS,
;;;  etc.  I moved a small amount of the old verbosity code into SHOP-TRACE,
;;;  e.g., we now print a message if a traced operator violates a protection. 
;;;  Also, I fixed the *VERSION* variable and put the right copyright/licensing
;;;  comments at the start of the file, to prepare for our software release.

;;;  2002.5.3 (jwm) Task lists in methods and in problem definitions
;;;  are now processed using the same mechanism.  The :ordered and
;;;  :task keywords are optional in both mechanisms and are implied
;;;  where omitted.  For example, the task list:
;;;;    (((a 1) (b 2))
;;;;     (:unordered (:immediate c 3) (d 4)))
;;;  is interpretted as:
;;;;   (:ordered (:ordered (:task a 1) (:task b 2))
;;;;             (:unordered (:task :immediate c 3) (:task d 4)))
;;;  Note that this approach is backward compatible with SHOP1,
;;;  which leaves out the :ordered and :task keywords, since those
;;;  keywords are implied.  It is _not_ backward compatible with
;;;  MSHOP which has the top-level task list always be unordered.

;;;  2002.5.3 (jwm) Deleted the check-for-loops feature because it was
;;;  never used much and stopped working many revisions ago.

;;;  2002.4.26 (jwm) Fixed bug in sort-by which reversed sort orders.
;;;  Incidentally, the change also makes the :which option return plans
;;;  in the order that they were found (rather than the reverse order).

;;;  2002.4.19 (jwm) Did more fine tuning of state code.  Improves speed,
;;;  especially with :bit and :hash encodings.

;;;  2002.4.7 (jwm) Added new option for encode-state, :bit, which
;;;  uses bit strings to represent states; casual testing suggests
;;;  that :bit is much slower than the other options and adds little
;;;  or no additional coverage.  It may improve with further tweaking,
;;;  however, so its staying in for now.

;;;  2002.4.7 (jwm) Fixed an idiosyncracy in axiom handling code which
;;;  led to strange behavior when an axiom has a single expression for
;;;  a tail rather than a list of expressions.  Axioms now work
;;;  correctly with either form.

;;;  2002.3.30 (jwm) Fixed a bug in the interaction between the new
;;;  state handling code and the existing code for protections.
;;;  Protections should now work like they did before the new state
;;;  code was introduced.

;;;  2002.3.29 (jwm) The :pshort argument now omits operators that
;;;  begin with !! rather than operators that have 0 cost.

;;;  2002.3.29 (jwm) It is now possible to use unbound variables in
;;;  methods when invoking an operator.  If those variables are bound
;;;  in the precondition of the operator then the associated step in
;;;  the final plan will use those bindings.

;;;  2002.3.26 (jwm) Updated print-current-state and
;;;  query-current-state so that they work with the modified state
;;;  representations.

;;;  2002.3.22 (jwm) Modified the :sort-by keyword for method
;;;  preconditions again; the new syntax is:
;;;    (:sort-by <value-expr> [ <comparison-expr> ] <precondition>)
;;;  For example, the following precondition would require that there
;;;  be values for the predicates foo and bar and would sort the
;;;  satisfiers in decreasing order of the product of the values of
;;;  those predicates:
;;;    (:sort-by (* ?x ?y) #'> (and (foo ?x) (bar ?y)))

;;;  2002.3.19 (uk) Added two new keywords that is to be used by the
;;;  keyword :sort-by. These are namely :asc and :dsc, which enable 
;;;  sorting in either ascending or descending order.

;;;  2002.3.18 (jwm) Added new keyword for logical expressions,
;;;  :sort-by.  When seek-satisfiers encounters the expression
;;;  (:sort-by ?foo expr1), it seeks staisfiers for the expression
;;;  expr1 and then sorts the results by the number that they bind
;;;  ?foo too.  If a satisfier does not bind ?foo or binds ?foo to a
;;;  non-number, it is treated as 0 for sorting purposes.

;;;  2002.3.15 (jwm) Added new keyword argument, :time-limit.  When
;;;  :time-limit is a number, SHOP2 terminates when that number of
;;;  seconds of CPU time has elapsed (or when it gets done, whichever
;;;  comes first).  The limit is checked at each call to seek-plans,
;;;  so if there is a long gap between calls, the total CPU time of
;;;  planning may be greater than the time limit.

;;;  2002.3.15 (jwm) Added a new user function encode-state, which
;;;  changes the way that states are encoded.  Currently three values
;;;  are supported:
;;;
;;;    :hash encodes states as hash tables; memory efficient but slow
;;;
;;;    :list encodes states as lists; fastest but memory inefficient
;;;
;;;    :mixed encodes states as lists within hash tables; a compromise
;;;      between speed and memory effiency
;;;
;;;  The default behavior is :mixed.

;;;  2002.3.15 (jwm) Changed find-plans to return both the plans and
;;;  total CPU time used (in seconds) rather than just the plans.

;;;  2002.3.13 (jwm) Fixed bug in delete-task-top-list (list was being
;;;  destructively modified).  

;;;  2002.3.10 (jwm) Isolated the code for manipulating states.
;;;  States are now destructively modified, and need to be explicitly
;;;  undone whenever backtracking occurs.  Hash tables (keyed by
;;;  predicate) are used to store state information.

;;;  2002.2.28 (jwm) Fixed minor bug in apply-method (some values
;;;  returned by force-immediate-reduction were being dropped).

;;;  2002.2.28 (jwm) Added some rounding code to print-stats because
;;;  it printed out very badly with some floating point numbers (this
;;;  has only become significant recently with the addition of various
;;;  cost related features see below).

;;;  2002.2.27 (dsn) get-top-tasks was running in quadratic time; I
;;;  replaced it with a linear-time algorithm.

;;;  2002.2.26 (dsn) in several places, SHOP2 was computing NEWDEPTH,
;;;  passing it as a parameter, and then taking (1- NEWDEPTH).  I
;;;  changed this so that SHOP2 just passes DEPTH as a parameter
;;;  instead.  Oddly enough, this minor change improved memory usage a
;;;  bit in my tests.

;;;  2002.2.26 (dsn) declared the local COST variables.  Removed
;;;  'CONTINUE from SEEK-PLANS and its subroutines, and fixed a case
;;;  where SEEK-PLANS was being called unnecessarily -- this seems to
;;;  improve both speed and memory usage

;;;  2002.2.26 (dsn) fixed problems with unused variables.

;;;  2002.2.26 (dsn) removed got-plan, and fixed some problems with
;;;  variable declarations.

;;;  2002.2.24 (dsn) replaced list-methods, list-operators, and
;;;  list-axioms with print-methods, print-operators, and print-axioms
;;;  -- functions that I had mentioned in an email but forgot to put
;;;  into this file!

;;;  2002.2.24 (dsn) mtrace, otrace, and atrace are gone; I replaced
;;;  them with functions named shop-trace and shop-untrace.

;;;  2002.2.23 (dsn) New debugging functions: mtrace, otrace, and
;;;  atrace, for tracing individual methods, operators, and axioms.
;;;  Also, modifications so that the domain name is irrelevant.

;;;  2002.2.22 (jwm) New keyword for find-plans, :optimize-cost,
;;;  defaults to nil, and if true uses branch-and-bound to find
;;;  optimal (minimum cost) results.  If the argument to
;;;  :optimize-cost is a number, results which are less than or equal
;;;  to that number are found.

;;;  2002.2.22 (dsn) Fixed MCL compatibility bug with seek-satisfiers.

;;;  2002.2.22 (dsn) Added debug printing code to apply-method

;;;  2002.2.15 (jwm) Costs for SHOP2 operators can now be arbitrary
;;;  eval expresssions, rather than just numbers.  Note that this
;;;  feature doesn't work with backward-compatible SHOP1 operators
;;;  (i.e., ones with no preconditions).

;;;  2002.2.8 (chiu) New debugging functions: query-current-state,
;;;  print-current-state, print-current-tasks, and print-current-plan

;;;  2002.2.8 (uk) Fix the the missing part in the if-then-else
;;;  structure in handling the axioms in the function do-conjunct

;;;  2002.1.2 (jwm) Added a check-for-loops keyword parameter to
;;;  find-plans (for symmetry with the other parameters which used to
;;;  be global variables, e.g., pshort.

;;;  2001.12.27 (jwm) Added the force-immediate function to resolve
;;;  some issues with combining ordered and unordered methods, q.v.

;;;  2001.12.27 (jwm) Some miscellaneous compatibility tweaks, e.g.,
;;;  the #+:lispworks code below.

;;;  2001.12.17 (jwm) Changed printing so that if find-plans is
;;;  invoked with :verbose 0, it prints absolutely nothing.  Before,
;;;  it always printed stats no matter what.  Now to get stats you
;;;  must explicitly include :stats in the verbosity list or give
;;;  :verbose a number greater than 0.

;;;  2001.12.17 (jwm) Changed !iNOP operator to automatically be
;;;  treated as if it has a :immediate before it.  This makes !iNOP's
;;;  more efficient in unordered lists and has no functional effects
;;;  because iNOP's never do anything.

;;;  2001.12.17 (jwm) Changed :pshort argument to find-plans such that
;;;  short printing is enabled when the argument is non-nil, rather
;;;  than when it is equal to 0.  This is more consistent with LISP
;;;  and with the other arguments to find-plans.

;;;  2001.12.17 (jwm) Bug fix for the :pshort argument; previously
;;;  when printing short, only the first plan was printed.  Now all
;;;  plans are printed.

;;;  2001.12.13 (ois) Changed the apply-operator function under the
;;;  assumption that at most one operator's precondition will be
;;;  satisfied every time.  Changed find-satisfiers to have an
;;;  optional parameter which is sent as just1 to seek-satisfiers.
;;;  Set this parameter to "t" when find-satisfiers is called to find
;;;  the satisfiers of an operator's preconditions, since we assume
;;;  that there will be at most one such satisfier.

;;;  2001.12.12 (jwm) Divided seek-plans into smaller functions such a
;;;  seek-plans-nonprimitive.  This is not a functional change, but it
;;;  should make it easier to edit seek-plans in the future.

;;;  2001.12.12 (ois) Added the capability to handle ANDs in
;;;  preconditions. Also changed return value of find-satisfiers from
;;;  'fail to nil in case of failure.

;;;  2001.12.05 (jwm) Added calls to standardize for methods and
;;;  operators.  Also rewrote unify1 to no longer standardize, because
;;;  all calls to unify now follow calls to standardize.

;;;  2001.11.28 (jwm) Added call to standardize when using axioms in
;;;  seek-satisfiers (primarily because recursive axioms were broken).
;;;  Also rewrote standardize to be more efficient (since it is called
;;;  more).

;;;  2001.11.22 (chiu) Added the assignment-term "assign".

;;;  2001.11.14 (chiu) Added *current-plan* and *current-tasks*
;;;  similar to *current-state* below.

;;;  2001.11.07 (chiu) Created global variable *current-state*,
;;;  updated in find-satisfiers, which records the current state;
;;;  handy for debugging.

;;;  2001.11.07  (jwm) Fixed the debug-print of the plans to use
;;;  the :plans tag rather than :stats

;;;  20001.11.07 (jwm) Created a new function variable-gensym which
;;;  generates a new variable _and_ sets the variable property for
;;;  that variable.  Replaced calls of the form (gensym "?... with
;;;  this new function

;;;  2001.07.05 (swm) Hacked on call some more to get process-method
;;;  to expand out method tails to something which actually gets
;;;  evaluated at run time.

;;;  2001.07.02 (swm) Removed !do-nothing operator from logistics
;;;  domain and re-ran regession tests on logistics and blocks-world
;;;  domains.  This finishes the previous change, and eliminates
;;;  Jason's hacked-in requirement to avoid nil tails in methods.

;;;  2001.07.01 (swm) Added default !NOP operator to make-domain and
;;;  defdomain.  Fixed handling of nil method tails in process-method.

;;;  2001.06.27 (swm) Additional syntactic changes to synchronize SHOP
;;;  2.0 syntax with the recently released JSHOP 1.0 Added macro call
;;;  to be used in place of eval.  Replaced make-problem with
;;;  defproblem.  Replaced make-problem-set with defproblem-set.
;;;  Replaced make-domain with defdomain.  NOTE BENE that these
;;;  changes were also propogated to the sample domain problem
;;;  definition files.

;;;  2001.06.06 (swm) Added preconditions to operators.  This required
;;;  modifying apply-operator to handle the new SHOP2 operator syntax,
;;;  and to test the preconditions before executing the delete and add
;;;  lists.  The precondition substitutions are interated over when
;;;  actually evaluating the delete and add lists.  Changed
;;;  operator-cost to access the correct element of the new SHOP 2
;;;  operator.  Modified process-operator to handle SHOP 2 operators,
;;;  and to re-write SHOP 1 operators into SHOP 2 syntax.

;;;  2001.06.05 (swm) Re-designed the control of debug output.  You
;;;  can still use a number which gives the same behavior as before,
;;;  or give a list of keywords (listed after the (defvar *verbosity*
;;;  declaration near the bottom of the file, which provides finer
;;;  control over the debug output.  A new macro is provided called
;;;  debug-print which replaces the old use of if statements with
;;;  tests on various numeric and other arguments.

;;;  2001.06.02 (swm) Replaced all occurances of :serial with
;;;  :ordered, and :parallel with :unordered to better represent the
;;;  actual meaning of that syntax.

;;;  2001.05.28 (swm) Modified make-domain to convert SHOP 1.x axioms
;;;  into SHOP 2 axioms with named branches if necessary.  Modified
;;;  seek-satisfiers to accomodate new axiom syntax.

;;;  2001.02.26 (swm) Modified process-method and apply-method to
;;;  accomodate the new branch labels in the method data structure:
;;;  (:method head label_1 precondition_1 task_list_1
;;;           ...  label_n precondition_n task_list_n)
;;;  NOTE: if the method does not have labels for the branches,
;;;  process-method will generate a label for each 
;;;  branch of the form method-name-branch-number.

;;;  2001.01.02 (swm) Added strip-NOPs to remove the !NOP operator
;;;  that Jason hacked in to fix another problem.  This is only used
;;;  when pushing a partial plan onto the list *plans-found*. I'll go
;;;  back and fix the problem right at some later date.  The real
;;;  ugliness here is that to accomodate Jason's !NOP hack all of the
;;;  domains had to be modified so that methods which used to have ()
;;;  as the action now have to have (!NOP) or (!do-nothing).

;;;  2000.12.19 (swm) ACL and MCL expand backquotes in very different
;;;  ways.  The code that Jason wrote for incorporating :serial,
;;;  :parallel, :task, and :immediate into legacy SHOP :method tails
;;;  works for ACL, but cannot handle the MCL backquote expansions.
;;;  To get around that, added special backquote and comma reader
;;;  macros to a special readtable called *blue-light-readtable*, and
;;;  modified all regression SHOP domain files to bind to that
;;;  readtable during input under MCL.

;;;  2000.12.13  (swm) selectively replaced equal with = or eql to 
;;;  speed up code.

;;;  2000.12.8   (swm) redefined the IMPLY case in seek-satisfiers per 
;;;  the embedded comment.

;;;  2000.10.20 (swm) redefined variablep and primitivep as macros and
;;;  added preprocessing code to make-domain with the helper function
;;;  set-variable-property to set variable and primitive as properties
;;;  on the atom's plist to speed up code.

;;; -----------------------------------------------------------------------
;;; Global, dynamically scoped variables.  Most of these could probably have
;;; been passed as parameters instead, but it would have been awkward
;;; -----------------------------------------------------------------------

(defvar *start-run-time*)
(defvar *start-real-time*)

;; (defvar *traced-operators* nil)        ; break when attempting to
                                        ; apply one of these.
(defvar *traced-methods* nil)                ; break when attempting to
                                        ; apply one of these.
(defvar *traced-tasks* nil)                ; break when attempting to
                                        ; expand one of these.

(defconstant SHOP-TRACE-ITEMS
  (list :methods :axioms :operators :tasks :goals :effects :protections
       :states :plans :functions :events)
  "Acceptable arguments for SHOP-TRACE (and SHOP-UNTRACE).")

(defmacro shop-trace (&rest items)
  "(SHOP-TRACE) will return a list of what's currently being traced

 (SHOP-TRACE ITEM) will turn on tracing for ITEM, which may be
   any of the following:
    - the name of a method, axiom, operator, task, or predicate;
    - one of the keywords :METHODS, :AXIOMS, :OPERATORS, :TASKS,
      :GOALS, :EFFECTS, or :PROTECTIONS, in which case SHOP will
      trace all items of that type (:GOALS, :EFFECTS, and :PROTECTIONS
      refer to three different ways predicates can occur: as goals to
      be satisfied, and as effects or protections in operators);
    - a pair of the form (:TASK <taskname>), (:METHOD <methodname>).  SHOP will
      break when attempting to expand the task, or apply the method, respectively.
    - the keyword :STATES, in which case SHOP will include the current
      state whenever it prints out a tracing message
    - the keyword :ALL in which case SHOP will print out all the tracing
      information it knows how to.

 (SHOP-TRACE ITEM1 ITEM2 ...) will do the same for a list of items"
  (let* ((items `,items)
        (new-items
         (if (null items) nil items)
         ))
    (when (member :all new-items)
      (setf new-items (delete :all new-items))
      (setf new-items (union SHOP-TRACE-ITEMS new-items)))
       
    `(shop-trace-1 ',new-items)))

(defun shop-trace-1 (items)
  ;; make sure the argument is coerced to a list
  (unless (null items) 
    (dolist (item items)
      (cond ((member item SHOP-TRACE-ITEMS)
            (pushnew item *shop-trace*))
           ((listp item)
            (case (car item)
              (:task (shop-trace-task (second item)))
              (:method (shop-trace-method (second item)))
              (:axiom (shop-trace-axiom (second item)))
              (otherwise 
               (warn "Ignoring invalid shop-trace argument ~S" item))))
           (t
            (warn "Ignoring invalid shop-trace argument ~S" item)))))
  (shop-trace-info))

(defun shop-trace-method (meth-name)
  (pushnew meth-name *traced-methods*))

(defun shop-trace-axiom (axiom-name)
  (declare (ignore axiom-name))
  (cerror "Just do nothing"
         "Tracing axioms by name is not yet implemented."))

;;;(defun shop-trace-op (opname)
;;;  (pushnew opname *traced-operators*))

(defun shop-trace-task (taskname)
  (pushnew taskname *traced-tasks*))

(defun shop-trace-info ()
  "Information about the traced aspects of shop2."
  (append
   *shop-trace*
   (loop for taskname in *traced-tasks*
       collect `(:task ,taskname))
   (loop for methname in *traced-methods*
       collect `(:method ,methname))))

(defmacro shop-untrace (&rest items)
  (if (null items)
      '(shop-untrace-all)
    `(shop-untrace-1 ',items)))
  
(defun shop-untrace-all ()
  (setf *shop-trace* nil
       *traced-tasks* nil
       ;;*traced-operators* nil
       *traced-methods* nil))

;;; (SHOP-UNTRACE ...) is the inverse of (SHOP-TRACE ...)
(defun shop-untrace-1 (items)
  ;; it's OK to use destructive deletion here
  (dolist (item items)
    (cond ((symbolp item)
          (setq *shop-trace* (delete item *shop-trace*)))
         ((eq (car item) :task)
          (setf *traced-tasks* (delete (second item) *traced-tasks*)))
         ((eq (car item) :method)
          (setf *traced-methods* (delete (second item) *traced-methods*)))
         (t
          (warn "don't know how to delete ~S from shop-trace items: ignoring." 
                item)))))


;;; Many of these should probably be absorbed into the definition of
;;; PLANNER... [2006/07/05:rpg]

(defparameter *internal-time-limit* nil)  ; maximum time (in units) for execution
(defparameter *internal-time-tag* nil)    ; catch tag to throw to when time expires
(defparameter *time-limit* nil)     ; maximum time (in seconds) for execution
(defparameter *expansions* 0)       ; number of task expansions so far
(defparameter *plans-found* nil)    ; list of plans found so far
(defparameter *plan-tree* nil)      ; whether to return the tree
(defparameter *subtask-parents* nil) ; record of the parents in the tree
(defparameter *operator-tasks* nil) ; record of the task atom for operators
(defparameter *optimize-cost* nil)  ; whether to optimize with branch and bound
(defparameter *optimal-plan* 'fail) ; optimal plan found so far
(defparameter *optimal-cost* 0)     ; cost of *optimal-plan*
(defparameter *depth-cutoff* nil)   ; maximum allowable depth for SEEK-PLANS 
(defparameter *verbose* 1)          ; default value for VERBOSE in FIND-PLANS
(defparameter *which* :first)       ; default value for WHICH in FIND-PLANS
(defparameter *max-plans* 10)       ; global value for MAX-PLANS, set in FIND-PLANS
(defparameter *gc* t)        ; whether to call GC each time we call SEEK-PLANS
(defparameter *pp* t)               ; what value to use for *PRINT-PRETTY*
(defparameter *tasklist* nil)       ; initial task list set to nil
(defparameter *protections* nil)    ; initial protection list set to nil
(defparameter *explanation* nil)    ; whether to return explanations

(defvar *print-plans*)              ; whether to print out the plans we find
(defvar *pshort*)  ; whether to skip ops that begin with !! when printing plans
(defvar *print-stats*)              ; whether to print statistics
(defvar *all-problems* nil)         ; all problems that have been defined

(defparameter *current-plan* nil)  ; current plan
(defparameter *current-tasks* nil) ; current task

(defvar *unifiers-found* nil)        ;associated with *PLANS-FOUND*
(defvar *states-found* nil)                ;associated with *PLANS-FOUND* [2004/09/14:rpg]

(defvar *hand-steer* nil
  "This variable will be DYNAMICALLY bound and used to indicate whether the user
wishes to choose tasks for planning by hand.")
(defvar *leashed* nil
  "This variable will be DYNAMICALLY bound and it will further constrain the behavior
of SHOP2 when we hand-steer it (see *hand-steer*).  If *leashed* is NIL, SHOP2 will
simply proceed when choosing tasks, if there is no choice, i.e., when there's only 
one task on the open list, or only one :immediate task.  If *leashed* is non-nil, 
will consult the user even in these cases.")

(defvar *break-on-backtrack* nil
  "If this variable is bound to T, SHOP will enter a break loop upon backtracking.")

#+allegro
(top-level:alias "bb" (&optional (val t))
  "Set *break-on-backtrack* variable"
  (setf *break-on-backtrack* val))


;;; It is relatively common practice in Lispworks to use ! as a macro
;;; for :redo.  This will really mess up interpretation of operators
;;; in SHOP, since all operators start with "!".  Thus, we will turn
;;; off macro interpretation of "!" if we are running Lispworks.  see:
;;; http://www.xanalys.com/software_tools/reference/lwl41/lwuser/LWUG_93.HTM

#+:lispworks (set-syntax-from-char #\! #\@)

;;; Setting the compiler parameters
;;; changed these so I could see, for example, the arglists. [2003/06/20:rpg]
(eval-when (:compile-toplevel)
  ;; the addition of this EVAL-WHEN keeps Allegro 6.2 from "leaking"
  ;; its compiler optimization settings into the surrounding context.
  (declaim (optimize (speed 3)
                     (compilation-speed 0)
                     (safety 1)
                     (debug 2))))

; (defun trace-query-hook (&rest args) (query-java args))

;;; PRIMITIVEP returns T if X is a symbol whose name begins with "!"
(defmacro primitivep (x) `(and (symbolp ,x) (get ,x 'primitive)))

;;;---------------------------------------------------------------------------
;;; Changed defproblem to simply add quotes and call MAKE-PROBLEM.
;;; This can help us avoid the danger of having defproblem and
;;; make-problem diverge. [2004/02/17:rpg]
;;;---------------------------------------------------------------------------
#+allegro (excl::define-simple-parser defproblem second :shop2-problem)
(defmacro defproblem (problem-name &rest args)
  ;; ARGS normally are state tasks
  ;; if extra arg is given, then the args are problem-name, domain-name, state,
  ;; and tasks respectively. in that case, we want to ignore domain-name
  (if (= (length args) 3)
    `(apply 'make-problem ',problem-name ',(cdr args))
    `(apply 'make-problem ',args)))

(defmacro def-problem-set (list-name problem-list)
  `(progn
    (format t "~%Defining problem set ~s ..." ',list-name)
    (setf (get ',list-name :problems) ',problem-list)))

;;;---------------------------------------------------------------------------
;;; DOMAIN manipulation functions --- these should probably be moved
;;; out into a file of their own at some point. [2006/07/24:rpg]
;;;---------------------------------------------------------------------------
(defmacro defdomain (name-and-options items)
  (unless (listp name-and-options)
    (setf name-and-options (list name-and-options)))
  ;; it makes more sense to me that defdomain have a &rest argument,
  ;; instead of a single argument that's a list.... [2006/07/31:rpg]
  (push '(:operator (!!inop) () () () 0) items)
  (push '(:action !replan :parameters () :precondition ()) items)
  (push '(:action !wait-for :parameters () :precondition ()) items)
  (push '(:action !wait :duration ?dur :parameters (?dur) :precondition ()) items)
  (destructuring-bind (name &rest options &key (type 'domain) redefine-ok &allow-other-keys)
         name-and-options
    (unless (subtypep type 'domain)
         (error "Type argument to defdomain must be a subtype of DOMAIN. ~A is not acceptable." type))
    (remf options :type)
    (remf options :redefine-ok)
    `(progn
       (format t "~%Defining domain ...")
       (let ((domain (apply #'make-instance ',type
                       :name ',name
                       ',options
                       )))
         ;; I suspect that this should go away and be handled by
         ;; initialize-instance... [2006/07/31:rpg]
         (apply #'handle-domain-options domain ',options)
         (parse-domain-items domain ',items)
         (install-domain domain ,redefine-ok)
         (setf *domain* domain)))))

(defmethod install-domain ((domain domain) &optional redefine-ok)
  (when (get (domain-name domain) :domain)
    (unless redefine-ok
      (warn "Redefining domain named ~A" (domain-name domain))))
  (setf (get (domain-name domain) :domain) domain))

(defun find-domain (name &optional (if-not-found :error))
  "Find and return a domain object with the name NAME.  Will return
the value in its IF-NOT-FOUND argument if no such domain is loaded.
IF-NOT-FOUND defaults to :error, which will raise an error condition."
  (let ((domain (get name :domain)))
    (cond (domain domain)
          ;; not found
          ((eq if-not-found :error)
           (error "No domain named ~A" name))
          (t if-not-found))))

(defun set-domain (name)
  "NAME argument is a symbol.  Will set the global variable *DOMAIN* 
to the domain with domain-name NAME."
  (setf *domain*
        (find-domain name :error)))

;;; default null method
(defmethod handle-domain-options ((domain domain) &key type)
  (declare (ignore type))
  (values))

;;; I have refactored parse-domain-items to use eql method dispatch
;;; instead of a case statement dispatch... [2006/07/28:rpg]
(defmethod parse-domain-items ((domain domain) items)
  (with-slots (axioms operators methods) domain
    (setf axioms (make-hash-table :test 'eq)
          operators (make-hash-table :test 'eq)
          methods (make-hash-table :test 'eq))
    (set-variable-property domain items)        ; set primitive and variable properties
    (dolist (x (reverse items))
      (parse-domain-item domain (car x) x))
    (values)))

;;;---------------------------------------------------------------------------
;;; Parse-domain-item methods
;;;---------------------------------------------------------------------------

(defmethod parse-domain-item ((domain domain) (item-key (eql ':method)) item)
  (push (process-method domain item)
        (gethash (first (second item))
                 (slot-value domain 'methods))))

(defmethod parse-domain-item ((domain domain) (item-key (eql ':operator)) item)
  (let ((op-name (first (second item))))
    (with-slots (operators) domain
      (when (gethash op-name operators)
        (error "There is more than one operator named ~s" op-name))
      (setf (gethash op-name operators) (process-operator domain item)))))

(defmethod parse-domain-item ((domain domain) (item-key (eql ':-)) item)
  (with-slots (axioms) domain
    ;; convert SHOP 1.x axioms into SHOP 2 axioms if necessary
    (let ((regularized (regularize-axiom domain item)))
      (push regularized (gethash (first (second item)) axioms)))))







;;;---------------------------------------------------------------------------
;;; End of domain functions....
;;;---------------------------------------------------------------------------

    
;;;-----------------------------------------------------------------------
;;; Had to rewrite backquote macro for MCL so it leaves the structure
;;; alone, then write a function to create the structure later.  We're
;;; assuming that "," only appears by itself and not as ",." or ",@".
;;; This is a Allegro Common Lisp compatibility hack.  (swm)

#+:MCL (defparameter *blue-light-readtable* (copy-readtable nil))

#+:MCL (set-macro-character #\`
                           #'(lambda (stream char) 
                               (declare (ignore char))
                               (list 'simple-backquote (read stream t nil t  )))
                           nil *blue-light-readtable*)

#+:MCL (set-macro-character #\,
                           #'(lambda (stream char) 
                               (declare (ignore char))
                               (list 'bq-comma (read stream t nil t  )))
                           nil *blue-light-readtable*)

(defparameter *back-quote-name*
  #+:MCL 'simple-backquote
  ;; need ,this to make sure it doesn't turn into quote
  #-:MCL (car '`(test ,this))) 

(defmacro simple-backquote (x)
  (quotify x))

(defun quotify (x) ; lifted from YAPS; added check for "call" and "eval"
  (cond ((not (consp x)) (list 'quote x))
        ((eql (car x) 'bq-comma) (cadr x))
        ((member (car x) '(call eval)) x)
        (t (let ((type 'list) args)
             (setq args 
                   (cons (quotify (car x))
                         (loop for y on x
                               collect (cond ((atom (cdr y))
                                              ;; non-nil tail
                                              (setq type 'list*)
                                              (cdr y))
                                             (t (quotify (cadr y)))))))
             (cons type args)))))

;;; A simple macro to add an item on to the end of a list.
(defmacro push-last (item list)
  `(setf ,list 
   (if (null ,list)
       (list ,item)
     (progn
       (setf (cdr (last ,list)) (list ,item))
       ,list))))

;;; This is a hack to pull out the !NOP operations Jason stuck in to get around
;;; another problem.
(defun strip-NOPs (plan)
  (cond ((null plan) nil)
  ((and
    (listp (first plan))
    (eq '!!inop (first (first plan))))
   (strip-NOPs (rest (rest plan))))
  (t
   (cons (first plan)
         (cons (second plan)
         (strip-NOPs (rest (rest plan))))))))

(defun print-axioms (&optional name (domain *domain*))
  (if name
    (progn (format t "Axioms for name ~S are:" name)
           (mapcar #'(lambda (x) (format t "~2%  ~s" x))
                   (axioms domain name)))
    (maphash #'(lambda (k defs) 
                 (format t "~2%Axioms for goal ~S are:" k)
                 (dolist (ad defs)
                   (format t "~2%  ~S" ad)))
             (domain-axioms domain))))

(defun print-methods (&optional name (domain *domain*))
  (if name
    (progn (format t "Methods for name ~S are:" name)
           (mapcar #'(lambda (x) (format t "~2%  ~s" x))
                   (methods domain name)))
    (maphash #'(lambda (k defs)
                 (format t "~2%Methods for task ~S are:" k)
                 (dolist (ad  defs)
                   (format t "~2%   ~S" ad)))
             (domain-methods domain))))

;;; this I don't understand: AFAICT from the rest of the code there
;;; can be only ONE operator defined with a particular
;;; name... [2006/07/05:rpg]
(defun print-operators (&optional name (domain *domain*))
  (if name
    (progn (format t "Operator for name ~S is:" name)
           (format t "~2%  ~s" (operator domain name)))
    (maphash #'(lambda (k defs) 
                 (format t "~2%Operators for task ~S are:" k)
                 (dolist (ad defs)
                   (format t "~2%   ~S" ad)))
             (domain-operators domain))))

(defun print-operator (operator &optional (stream t))
  (format stream "~THead: ~S~%~@[~TPreconditions: ~S~%~]~@[~TDelete-list: ~S~%~]
                  ~@[~TAdd-list: ~S~%~]~@[~TCost-fun: ~S~%~]"
          (operator-head operator)
          (operator-preconditions operator)
          (operator-deletions operator)
          (operator-additions operator)
          (operator-cost-fun operator)))

;;; determine-verbosity uses the current value of *verbose* to set
;;; global flags that tell SHOP2 what information to print out
(defun determine-verbosity (verbose)
  (ecase verbose
      ((0 nil)         (setq *print-stats* nil *print-plans* nil))
      ((1 :stats)      (setq *print-stats* t *print-plans* nil))
      ((2 :plans)      (setq *print-stats* t *print-plans* t *pshort* t))
      ((3 :long-plans) (setq *print-stats* t *print-plans* t *pshort* nil))))

;;; get the list of top-tasks in the main task list
;;; i.e.  the tasks with no predecessor
(defun get-top-tasks (L)
  (if (null L)
    nil
    (ecase (car L)
      (:simultaneously
       ;; a simultaneous task list, so manage waits
       (let* ((top-tasks (mapcan #'get-top-tasks (cdr L)))
              (top-non-waits (remove '!wait top-tasks :key #'task-name))
              min-time
            )
           (when (and top-tasks (null top-non-waits))
               (setq min-time (apply #'min (mapcar #'first (mapcar #'task-args top-tasks))))
               (return-from get-top-tasks (list (list :task '!wait min-time)))
           )
           top-non-waits
       )
      )
      (:unordered 
       ;; a parallel task list, so recursively call the function on 
       ;; each element and append the answers
       (mapcan #'get-top-tasks (cdr L)))
      (:ordered
       ;; a serial task list, so recursively call the function on the 
       ;; first element of the list
       (get-top-tasks (cadr L)))    
      (:task
       ;; a simple task, so return a list containing the task
       (list L)))))

;;; look inside the L, replace first t1 with the top-level tasks of t2
(defun replace-task-top-list (L t1 t2)
  (append (remove t1 L :count 1 :test #'equal) (get-top-tasks t2)))

;;; look inside the ML, replace first t1 with the top-level 
;;; tasks of t2, also, this function will replace t1 in the 
;;; main task-list with t2
(defun replace-task-main-list (ML t1 t2)
  (let (answer temp temp-found found)
    (if (or (null t1) (null ML))
      (return-from replace-task-main-list (values ML nil))
      (if (equal ML t1)
        (return-from replace-task-main-list (values t2 t))))
    (ecase (car ML)
      ((:unordered :simultaneously) 
       ;; a parallel task list, recursively calling the function on 
       ;; each element
       (setq answer (list (car ML)))
       (dolist (sub-task (cdr ML))
         (if found
           ;; already found t1
           (setq answer (cons sub-task answer))
           (progn
             ;; recursively calling the function on sub-task
             (multiple-value-setq (temp temp-found) 
               (replace-task-main-list sub-task t1 t2))
             (if temp-found 
               (setq t1 nil
                     t2 nil 
                     found t))
             (setq answer (cons temp answer)))))
       (return-from replace-task-main-list (values (reverse answer) found)))
      
      (:ordered
       ;; a serial task list. recursively calling the function to the
       ;; first element of the list, then append the rest of the list
       (setq answer '(:ordered))
       (multiple-value-setq (temp found)
         (replace-task-main-list (second ML) t1 t2))
       (setq answer (cons temp answer))
       (setq answer (append (reverse answer) (cddr ML)))
       (return-from replace-task-main-list (values answer found)))
      
      (:task
       ;; a simple task, if it equals to t1, it wouldn't have
       ;; reached here.  so simply return ML and nil
       (return-from replace-task-main-list (values ML nil))))))

;;; this function will delete TASK from both the main task-list ML,  
;;; and L, the top-level task list.  Also, it will update
;;; L by appending the children of TASK in the main task-list
(defun delete-task-top-list (L ML TASK &aux (wait-time nil))
  (let ((tempML (copy-task-tree ML)) L1)
    (when (eq (task-name TASK) '!wait)
        (setq TASK (copy-list TASK))
    )
    (setq L1 (append (remove TASK L :count 1 :test #'equal) 
                     (find-next-main-list TASK tempML)))
    (delete-task-main-list tempML TASK nil)

    (unless (car L1)
        (setq L1 (cdr L1)))
    (when (null L1)
        ;;Matt: I had to add this to make simultaneously work. 8/12
        (setq L1 (get-top-tasks tempML))
    )
    (return-from delete-task-top-list (values L1 tempML))))

(defun remove-zero-waits (lst)
    (cond 
        ((eq (car lst) (:ordered :unordered :simultaneously))
            (cons (car lst) (remove-zero-waits (cdr lst)))
        )
        ((and (listp (car lst)) (eq (caar list) :task))
            (if (and (eq (task-name (car lst)) '!wait)
                     (< (first (task-args (car lst))) 1d-6))
                ;then
                (remove-zero-waits (cdr lst))
                ;else
                (cons (car lst) (remove-zero-waits (cdr lst)))
            )
        )
        (t (error "Did not expect ~A in task list." (car lst)))
    )
)

; copy-task-tree is like copy-tree except that anything starting with
; a :task is simply inserted rather than copied.  The motivation here
; is to allow functions that destructively modify the task tree
; structures to not disrupt other copies but to still allow references
; to individual tasks to be compared using eq (as is done in some
; helpers to extract-tree).
(defun copy-task-tree (tt)
  (cond
   ((atom tt) tt)
   ((eq (first tt) :task) tt)
   (t
    (cons (copy-task-tree (first tt))
          (copy-task-tree (rest tt))))))

;;; this function will find the list of children that
;;; decend from task directly
(defun find-next-main-list (task L)
  (if (null L)
    (return-from find-next-main-list (values nil nil t))
    (let (found answer (goNext t))
      (ecase (car L)
        ((:unordered :simultaneously) 
         ;; a parallel task list, recursively calling the function on 
         ;; each element, stop when it has found task
         (dolist (sub-task (cdr L))
           (if (equal sub-task task)
             ;; found the task, since it's in a :unordered list, it has 
             ;; no child directly
             (if (<= (list-length L) 2)
               ;; a :unordered list with at most one task
               (return-from find-next-main-list (values nil t t))
               (return-from find-next-main-list (values nil t nil)))
             ;; else not found task in unordered list.
             (progn
               (multiple-value-setq (answer found goNext) (find-next-main-list task sub-task))
               (when (eq (car L) :simultaneously)
                   ;; Here, waits must not be returned until all other actions are
                   ;; executed.
                   (setq answer (remove '!wait answer :key #'task-name))
               )
               (if found
                 (if (and goNext (<= (list-length L) 2))
                   ;; need to getNext child 
                   (return-from find-next-main-list (values answer found t))
                   (return-from find-next-main-list (values answer found nil)))))))
         (return-from find-next-main-list (values nil nil t)))
        
        (:ordered
         ;; a serial task list, check if the first element is equal to task, 
         ;; if not, recursively calling the function on the 
         ;; first element of the list
         (if (equal (second L) task)
           ;then
           (if (<= (list-length L) 2)
             ;then
             (return-from find-next-main-list (values nil t t))
             ;else
             (return-from find-next-main-list 
               (values (get-top-tasks (third L)) t nil)))
           ;else second L != task
           (progn
             (multiple-value-setq (answer found goNext) (find-next-main-list task (second L)))
             (if found
               (if goNext 
                 (if (<= (list-length L) 2)
                   ;; need to getNext child 
                   (return-from find-next-main-list (values answer found t))
                   (return-from find-next-main-list 
                     (values (get-top-tasks (third L)) found nil)))
                 (return-from find-next-main-list (values answer found nil)))))))
        
        (:task
         ;; a simple task, L can't equal task, if it did, it would have been 
         ;; caught on the previous two cases
         (return-from find-next-main-list (values nil nil t)))))))

;;; This function removes TASK from the main-list.
;;; TODO: this function should not only delete the first occurance of TASK, but
;;; also should flatten the list if possible
;;; i.e. (:ordered ... (:ordered t1 t2) ... ) should become
;;; (:ordered ... t1 t2 ...)
(defun delete-task-main-list (L TASK deleted)
  (let (current-deleted
        (templist (cdr L))
        sub-task
       )
    (when (null L)
        (return-from delete-task-main-list nil)
    )
      
    (ecase (car L)
      (:simultaneously
         ;; a simultaneous task list, recursively calling the function on 
         ;; each element.
         (loop while (and (not (null templist)) (not (null (car templist)))) do
             (cond 
               ((and (eq (task-name TASK) '!wait) (not deleted))
                 (setf sub-task (car templist))
                 (setq current-deleted (delete-task-main-list sub-task TASK nil))
                 (when (not current-deleted)
                     (error "Should always be able to delete a simultaneous wait.")
                 )
                 (perform-deletion templist sub-task current-deleted)
               )
               (t
                 (setf sub-task (car templist))
                 (setq current-deleted (delete-task-main-list sub-task TASK deleted))
                 (setq deleted (if (or deleted current-deleted) t nil))
                 (perform-deletion templist sub-task current-deleted)
               )
             )
             (setq templist (cdr templist))
         )
         (when (eq (task-name TASK) '!wait)
             (setq deleted t)
         )
      )

      (:unordered
         ;; a parallel task list, recursively calling the function on 
         ;; each element.
         (loop while (and (not (null templist)) (not (null (car templist)))) do
            (setf sub-task (car templist))
            (setq current-deleted (delete-task-main-list sub-task TASK deleted))
            (setq deleted (if (or deleted current-deleted) t nil))
            (perform-deletion templist sub-task current-deleted)
            (setq templist (cdr templist))
         )
      )

      (:ordered
         ;; a serial task list, recursively calling the function on the 
         ;; first element of the list
         (setf sub-task (car templist))
         (setq current-deleted (delete-task-main-list sub-task TASK deleted))
         (setq deleted (if (or deleted current-deleted) t nil))
         (perform-deletion templist sub-task current-deleted)
      )
        
      (:task
         ;; a simple task, check if it's equal to t1, if so, set it to (nil)
         ;; and at the up-level it will be deleted
         ;; Don't set -> instead just non-destructively return :task since
         ;;  individual task elements are reused now - jwm 12/12/02
         (when (and (eq (task-name TASK) '!wait) (not deleted)
                    (eq (task-name L) '!wait))
             (setf (first (task-args L)) 
                 (- (first (task-args L)) (first (task-args TASK)))
             )
             (when (> -1d-6 (first (task-args L)))
                 (error "Why wait longer than shortest wait?")
             )
             (if (> 1d-6 (first (task-args L)))
                 ;then
                 (return-from delete-task-main-list :task)
                 ;else
                 (return-from delete-task-main-list t)
             )
         )
         (if (and (not deleted) (equal TASK L))
             ;then
             (return-from delete-task-main-list :task)
             ;else
             (return-from delete-task-main-list nil)
         )
      )
    )
    ;; clean up code to remove the last nil in the list
    (when (and (> (list-length L) 1)
               (null (car (last L))))
        (rplacd (nthcdr (- (list-length L) 2) L) nil)
    )
    (return-from delete-task-main-list deleted)
  )
)

(defun perform-deletion (templist sub-task current-deleted)
    (if (or (<= (list-length sub-task) 1)
            (eq current-deleted :task))
        ;;then remove sub-task from L
        (setf (car templist) (second templist)
              (cdr templist) (cddr templist))
    )
)

(defun process-task-list (tasks)
  (cond
   ((null tasks) (list :ordered (list :task '!!inop)))
   ((atom tasks) (cons :task tasks))
   ((member (first tasks) '(:ordered :unordered :simultaneously))
    (cons (first tasks)
    (mapcar #'process-task-list (rest tasks))))
   ((eq (first tasks) :task)
    tasks)
   ((atom (first tasks))
    (cons :task tasks))
   (t
    (cons :ordered
          (mapcar #'process-task-list tasks)))))


;;;;;; EXTRACT-VARIABLES returns a list of all of the variables in EXPR
;;;(defun extract-variables (expr)
;;;  (cond ((variablep expr) (list expr))
;;;  ((and (consp expr) (not (eql (car expr) 'forall)))
;;;   (shop-union (extract-variables (car expr))
;;;       (extract-variables (cdr expr))))))
;;;  
;;;;;; Lisp has a built-in UNION function, but we need something that returns
;;;;;; the same results regardless of what platform SHOP is running on.
;;;(defun shop-union (s1 s2 &key (test #'eql))
;;;   (append s1
;;;     (remove-if #'(lambda (e2) (member e2 s1 :test test)) s2)))
;;;
;;;;;;;;; APPLY-SUBSTITUTION searches through TARGET, replacing each variable
;;;;;;;;; symbol with the corresponding value (if any) in A-LIST  (Dana Nau)
;;;;;;(defmacro apply-substitution (target a-list)
;;;;;;  `(if (null ,a-list) ,target
;;;;;;    (real-apply-substitution ,target ,a-list)))
;;;;;;(defun real-apply-substitution (target a-list)
;;;;;;  (cond ((atom target)
;;;;;;         (let ((result (assoc target a-list)))
;;;;;;           (if result (cdr result) target)))
;;;;;;        ((null (cdr target)) (list (real-apply-substitution (car target) a-list)))
;;;;;;        (t (cons (real-apply-substitution (car target) a-list)
;;;;;;                 (real-apply-substitution (cdr target) a-list)))))
;;;;;;
;;;;;;;;; COMPOSE-SUBSTITUTIONS applies SUB2 to the right-hand-side of each item
;;;;;;;;; in SUB1, and appends all items in SUB2 whose left-hand-sides aren't in SUB1.
;;;;;;;;; *** Warning:  COMPOSE-SUBSTITUTIONS destroys the old value of SUB1 ***
;;;;;;;;; I normally would avoid destructive operations, but here it speeds up the
;;;;;;;;; planner by about 5%, and none of the calling functions need SUB1 afterwards
;;;;;;;;; (Dana Nau)
;;;;;;(defun compose-substitutions (sub1 sub2)
;;;;;;  (dolist (pair sub1)
;;;;;;    (setf (cdr pair) (apply-substitution (cdr pair) sub2)))
;;;;;;  (dolist (pair sub2 sub1)
;;;;;;    (pushnew pair sub1 :test #'(lambda (x y) (equal (car x) (car y))))))
;;;;;;
;;;;;;;;; UNIFY is based on the procedure in Nilsson's 1980 book, but modified
;;;;;;;;; to produce an mgu that simultaneously unifies and standardizes the two
;;;;;;;;; expressions.  This improves efficiency by avoiding repeated calls to
;;;;;;;;; STANDARDIZER in FIND-SATISFIERS and APPLY-METHOD.  (Dana Nau)
;;;;;;(defun unify (e1 e2)
;;;;;;  (cond ((atom e1) (unify1 e1 e2))
;;;;;;        ((atom e2) (unify1 e2 e1))
;;;;;;        (t (let ((hsub (unify (car e1) (car e2))))
;;;;;;             (if (eql hsub 'fail)
;;;;;;               'fail
;;;;;;               (let* ((tail1 (apply-substitution (cdr e1) hsub))
;;;;;;                      (tail2 (apply-substitution (cdr e2) hsub))
;;;;;;                      (tsub (unify tail1 tail2)))
;;;;;;                 (if (eql tsub 'fail)
;;;;;;                   'fail
;;;;;;                   (compose-substitutions hsub tsub))))))))
;;;;;;
;;;;;;(defun unify1 (e1 e2)
;;;;;;  (cond ((equal e1 e2) nil)
;;;;;;    ((variablep e1)
;;;;;;      (if (and (occurs e1 e2))
;;;;;;        'fail
;;;;;;        (list (cons e1 e2))))
;;;;;;    ((variablep e2)
;;;;;;      (list (cons e2 e1)))
;;;;;;    (t 'fail)))
;;;
;;;;;; OCCURS is the infamous "occurs check" - it returns T if the variable
;;;;;; symbol V occurs anywhere in the expression EXPR (Dana Nau)
;;;;;;(defun occurs (v expr)
;;;;;;  (if (atom expr)
;;;;;;    (eq v expr)
;;;;;;    (or (occurs v (car expr)) (occurs v (cdr expr)))))
;;;;;;
;;;;;;(defun variable-gensym (&optional base-name)
;;;;;;  (let ((sym (if base-name (gensym (string base-name)) (gensym "?"))))
;;;;;;    (setf (get sym 'variable) t)
;;;;;;    sym))
;;;
;;;;;; STANDARDIZER returns a substitution that replaces every varible symbol
;;;;;; in EXPRESSION with a new variable symbol not used elsewhere (Dana Nau)
;;;;;;(defun standardizer (expression)
;;;;;;  (mapcar #'(lambda (x) (cons x (variable-gensym))) (extract-variables expression)))
;;;;;;
;;;;;;(defun standardize (expr &optional subs)
;;;;;;  (cond
;;;;;;   ((null expr) (values nil subs))
;;;;;;   ((consp expr)
;;;;;;    (multiple-value-bind 
;;;;;;     (car-expr car-subs)
;;;;;;     (standardize (car expr) subs)
;;;;;;     (multiple-value-bind
;;;;;;      (cdr-expr cdr-subs)
;;;;;;      (standardize (cdr expr) car-subs)
;;;;;;      (values (cons car-expr cdr-expr) cdr-subs))))
;;;;;;   ((variablep expr)
;;;;;;    (let ((bin (assoc expr subs)))
;;;;;;      (if bin
;;;;;;        (values (cdr bin) subs)
;;;;;;        (let ((new-var (variable-gensym expr)))
;;;;;;          (values new-var (cons (cons expr new-var) subs))))))
;;;;;;   (t
;;;;;;    (values expr subs))))
;;;
;;;;;; ------------------------------------------------------------------------
;;;;;; Theorem prover
;;;;;; ------------------------------------------------------------------------
;;;
;;;;;; This macro is defined before find-satisfiers because it is invoked by
;;;;;;  find-satisfiers and some compilers want all macros to be defined before
;;;;;;  any code which invokes them.
;;;(defmacro seek-satisfiers (goals state bindings level just1)
;;;         `(if (null ,goals)
;;;            (list ,bindings)
;;;            (real-seek-satisfiers ,goals ,state ,bindings ,level ,just1)))
;;;
;;;;;; FIND-SATISFIERS returns a list of all satisfiers of GOALS in AXIOMS
;;;(defun find-satisfiers (goals state &optional just-one (level 0))
;;;    "Find and return a binding list that represents the answer to 
;;;goals \(a list representation of a deductive query\), where 
;;;state provides the database.  level is a non-negative integer indicating the
;;;current depth of inference, used in debugging prints, and 
;;;just-one is a boolean indicating whether you want just one answer (non-nil)
;;;or all answers (nil)."
;;;  (setf *current-state* state)
;;;  (let* ((sought-goals
;;;          (cond
;;;            ((eq (first goals) :first) (rest goals))
;;;            ((eq (first goals) :sort-by)
;;;              (if (= (length goals) 3)
;;;                (third goals)
;;;                (fourth goals)))
;;;            (t goals)))
;;;         (variables (extract-variables sought-goals))
;;;         (answer (seek-satisfiers sought-goals state variables level
;;;                                  (or (eq (first goals) :first) just-one)))
;;;         (satisfiers (mapcar #'(lambda (bindings) (make-binding-list variables bindings))
;;;                             answer)))
;;;    ;(format t "~%sat: ~s~%" satisfiers) ;***
;;;
;;;    (if (eq (first goals) :sort-by)
;;;        (sort satisfiers 
;;;              (if (= (length goals) 3) #'<
;;;                  (eval (third goals)))
;;;              :key #'(lambda (sat)
;;;                       (eval (apply-substitution (second goals) sat))))
;;;        satisfiers)))
;;;
;;;;;; REAL-SEEK-SATISFIERS is the workhorse for FIND-SATISFIERS.  For each
;;;;;; proof of GOALS from AXIOMS, it returns the values of GOAL's variables
;;;;;; that make the proof true.  Here are the other parameters:
;;;;;;  - VARIABLES is the list of variables that need to appear in the answer
;;;;;;  - BINDINGS is the variable bindings at the current node of the proof tree
;;;;;;  - LEVEL is the current search depth, for use in debugging printout
;;;;;;  - JUST1 is a flag saying whether to return after finding the 1st satisfier
;;;(defun real-seek-satisfiers (goals state bindings level just1)
;;;  (setq *inferences* (1+ *inferences*))
;;;  (let ((goal1 goals) (newlevel (1+ level))
;;;        remaining mgu1 answers new-answers)
;;;    (when (listp (car goals))
;;;      (setq goal1 (car goals))
;;;      (setq remaining (cdr goals)))
;;;    (case (car goal1)
;;;      (not
;;;        ;; we just want to see if (CDR GOAL1) is satisfiable, so last arg is T
;;;        (if (seek-satisfiers (cdr goal1) state nil newlevel t)
;;;          (return-from real-seek-satisfiers nil)
;;;          (seek-satisfiers remaining state bindings newlevel just1)))
;;;      
;;;      (eval
;;;        (if (eval (cadr goal1))
;;;          (seek-satisfiers remaining state bindings newlevel just1)
;;;          (return-from real-seek-satisfiers nil)))
;;;
;;;      (call
;;;        (if (eval (cdr goal1))
;;;          (seek-satisfiers remaining state bindings newlevel just1)
;;;          (return-from real-seek-satisfiers nil)))
;;;
;;;      ;; we may want to have a function that can return a LIST of
;;;      ;; possible values for a variable. [2003/11/03:rpg]
;;;      (assign*
;;;       ;; it's possible that the VAR part of (ASSIGN VAR ANS) will already
;;;       ;; be bound by the time the ASSIGN form is examined.  We need to
;;;       ;; check for this important special case. [2003/06/20:rpg]
;;;       (let ((var (cadr goal1))
;;;             (answers (eval (caddr goal1))))
;;;                (loop for ans in answers
;;;                      with resulting-answers = nil
;;;                      with new-answers
;;;                      if (or (variablep var)
;;;                             ;; trivial unification --- probably wrong, should be true unification
;;;                             (equalp var ans))
;;;                        do (setf new-answers (seek-satisfiers
;;;                                              (apply-substitution remaining (list (make-binding var ans)))
;;;                                                  state
;;;                                                  (apply-substitution bindings (list (make-binding var ans)))
;;;                                                  newlevel just1))
;;;                      when new-answers
;;;                        if just1
;;;                          do (return-from real-seek-satisfiers new-answers)
;;;                      else do (setf resulting-answers
;;;                                 (shop-union new-answers resulting-answers :test #'equal))
;;;                      finally (return resulting-answers))))
;;;      (assign
;;;       ;; it's possible that the VAR part of (ASSIGN VAR ANS) will already
;;;       ;; be bound by the time the ASSIGN form is examined.  We need to
;;;       ;; check for this important special case. [2003/06/20:rpg]
;;;       (let ((var (second goal1))
;;;             (ans (eval (third goal1))))
;;;         (cond ((variablep var)
;;;                (seek-satisfiers 
;;;                 (apply-substitution remaining (list (make-binding var ans)))
;;;                 state (apply-substitution bindings (list (make-binding var ans)))
;;;                 newlevel just1))
;;;               ;; trivial unification --- probably wrong --- should be true unification
;;;               ((equalp var ans)
;;;                (seek-satisfiers 
;;;                 (apply-substitution remaining nil)
;;;                 state (apply-substitution bindings nil)
;;;                 newlevel just1))
;;;               ;; otherwise, a constant value for VAR didn't match ANS
;;;               (t (return-from real-seek-satisfiers nil)))))
;;;
;;;      (imply
;;;        (let ((conditionA (second goal1)) (conditionB (third goal1)))
;;;          (if (or (not (find-satisfiers conditionA state))
;;;                  (find-satisfiers conditionB state t))
;;;            (seek-satisfiers remaining state bindings newlevel just1)
;;;            (return-from real-seek-satisfiers nil))))
;;;
;;;      (or
;;;        (dolist (goal1-sub (cdr goal1))
;;;          ;; First, look for ways to satisfy GOAL1-sub from the atoms in STATE
;;;          (when (setq mgu1 (find-satisfiers goal1-sub state nil newlevel))
;;;            (dolist (tempmgu mgu1)
;;;              (setq new-answers (seek-satisfiers
;;;                                 (apply-substitution remaining tempmgu)
;;;                                 state (apply-substitution bindings tempmgu)
;;;                                 newlevel just1))
;;;              (when new-answers
;;;                (if just1
;;;                  (return-from real-seek-satisfiers new-answers))
;;;        (setq answers (shop-union new-answers answers :test #'equal))))))
;;;        (return-from real-seek-satisfiers answers))
;;;      
;;;      (forall
;;;        (let ((bounds (third goal1)) (conditions (fourth goal1)) mgu2)
;;;          (setq mgu2 (find-satisfiers bounds state))
;;;          (dolist (m2 mgu2)
;;;            (unless (seek-satisfiers (apply-substitution conditions m2)
;;;                     state bindings 0 t)
;;;              (return-from real-seek-satisfiers nil))))
;;;        (seek-satisfiers remaining state bindings newlevel just1))
;;;
;;;      (exists
;;;        (let ((bounds (third goal1)) (conditions (fourth goal1)) mgu2)
;;;          (setq mgu2 (find-satisfiers bounds state))
;;;          (dolist (m2 mgu2)
;;;            (when (seek-satisfiers (apply-substitution conditions m2)
;;;                     state bindings 0 t)
;;;              (return-from real-seek-satisfiers nil))))
;;;        (seek-satisfiers remaining state bindings newlevel just1))
;;;
;;;      (t
;;;        ;; Handling and
;;;        (if (eq (car goal1) 'and)
;;;          (setq goal1 (cdr goal1)))
;;;        ;; Handling a list of goals as a conjunction of 
;;;        ;; (not necessarily atomic) goals
;;;        (if (or (listp (car goal1)) (eq (car goal1) :external))
;;;          (progn
;;;            (when (setq mgu1 (if (eq (car goal1) :external)
;;;                                 (or
;;;                                  (external-find-satisfiers (cdr goal1) state)
;;;                                  (find-satisfiers (cdr goal1) state nil newlevel))
;;;                               (find-satisfiers goal1 state nil newlevel)))
;;;              (dolist (tempmgu mgu1)
;;;                      (setq new-answers 
;;;                            (seek-satisfiers
;;;                             (apply-substitution remaining tempmgu)
;;;                             state (apply-substitution bindings tempmgu)
;;;                             newlevel just1))
;;;                (when new-answers
;;;                 (if just1
;;;                  (return-from real-seek-satisfiers new-answers))
;;;                  (setq answers (shop-union new-answers answers 
;;;                                          :test #'equal)))))
;;;            (return-from real-seek-satisfiers answers))
;;;          ;; If goal1 is an atomic predicate, try to satisfy it.
;;;          (do-conjunct goal1 remaining state bindings level just1))))))
;;;
;;;; An alternate version that wasn't used:
;;;;(defun external-find-satisfiers (goal state)
;;;;  (format t "EFS: ~s~%" goal)
;;;;  (or (when *external-access* 
;;;;        (let ((sats (external-query goal state)))
;;;;          (nconc *attribution-list* 
;;;;                 (mapcar #'(lambda (sat)
;;;;                             (list (apply-substitution goal sat)
;;;;                                   ATTRIB))
;;;;                         sats))
;;;;          sats))
;;;;      (find-satisfiers goal state)))
;;;
;;;(defun external-find-satisfiers (goal state)
;;;;  (format t "EFS: ~s~%" goal)
;;;  (if *external-access*
;;;      (external-query goal state)
;;;    nil))
;;;
;;;;;; Goal1 is guaranteed to be an atomic predicate
;;;(defun do-conjunct (goal1 remaining state bindings level just1)
;;;  (let (mgu1 answers new-answers found-match new-just1)
;;;    (trace-print :goals (car goal1) state
;;;                 "~2%Level ~s, trying to satisfy goal ~s"
;;;                 level
;;;                 goal1)
;;;
;;;    ;; Then, look for ways to satisfy GOAL1 from the atoms in STATE
;;;    (dolist (r (state-candidate-atoms-for-goal state goal1))
;;;      (unless (eql (setq mgu1 (unify goal1 r)) 'fail)
;;;        (setq found-match t) ; for debugging printout
;;;        (setq new-answers (seek-satisfiers
;;;                           (apply-substitution remaining mgu1)
;;;                           state (apply-substitution bindings mgu1) 
;;;                           (1+ level) just1))
;;;        (if new-answers
;;;          (progn
;;;            (trace-print :goals (car goal1) state
;;;                         "~2%Level ~s, state satisfies goal ~s~%satisfiers ~s"
;;;                         level
;;;                         goal1
;;;                         new-answers)
;;;            (if just1
;;;              (return-from do-conjunct new-answers))
;;;            (setq answers (shop-union new-answers answers :test #'equal))))))
;;;    ;; Next, look for ways to prove GOAL1 from the *axioms*
;;;    (dolist (r (gethash (car goal1) (domain-axioms *domain*)))
;;;      (let ((standardized-r (standardize r)))
;;;        (unless (eql (setq mgu1 (unify goal1 (second standardized-r))) 'fail)
;;;          (setq found-match t) ; for debugging printout
;;;          ;; found an axiom which unifies, now look at branches of the tail
;;;          (let ((tail (cddr standardized-r)))
;;;            (do ((ax-branch-name (car tail) (car tail))
;;;                 (ax-branch (cadr tail) (cadr tail)))
;;;              ((null tail)  nil)
;;;              (trace-print :goals (car goal1) state
;;;                           "~2%Level ~s, axiom matches goal ~s~%     axiom ~s~%satisfiers ~s"
;;;                           level
;;;                           goal1
;;;                           ax-branch-name
;;;                           mgu1)
;;;              (trace-print :axioms ax-branch-name state
;;;                   "~2%Level ~s, trying axiom ~s~%      goal ~s~%      tail ~s"
;;;                     level
;;;                     ax-branch-name
;;;                     goal1
;;;                     (apply-substitution ax-branch mgu1))
;;;              (if (eq (car ax-branch) :first)
;;;                (setq new-just1 t ax-branch (cdr ax-branch))
;;;                (setq new-just1 just1))
;;;              (setq new-answers (seek-satisfiers
;;;                                  (apply-substitution 
;;;                                    (append (list ax-branch) remaining) mgu1)
;;;                                 state (apply-substitution bindings mgu1)
;;;                                 (1+ level) new-just1))
;;;              (if new-answers
;;;                (progn
;;;                  (trace-print :axioms ax-branch-name state
;;;                               "~2%Level ~s, applying axiom ~s~%      goal ~s~%      tail ~s"
;;;                               level
;;;                               ax-branch-name
;;;                               goal1
;;;                               (apply-substitution ax-branch mgu1))
;;;                  (if new-just1 
;;;                    (return-from do-conjunct new-answers))
;;;                  (setq answers (shop-union new-answers answers :test #'equal))
;;;                  (return nil))
;;;                (progn
;;;                  (trace-print :axioms ax-branch-name state
;;;                               "~2%Level ~s, exiting axiom ~s~%      goal ~s~%      tail ~s"
;;;                               level
;;;                               ax-branch-name
;;;                               goal1
;;;                               (apply-substitution ax-branch mgu1))))
;;;              (setf tail (cddr tail)))))))
;;;    (unless found-match
;;;      (trace-print :goals (car goal1) state
;;;                   "~2%Level ~s, couldn't match goal ~s"
;;;                   level
;;;                   goal1))
;;;    (return-from do-conjunct answers)))
;;;
;;;
;;;
;;;;;; ------------------------------------------------------------------------
;;;;;; Explanation of satisifiers
;;;;;; ------------------------------------------------------------------------
;;;
;;;; The following code is invoked only if the :explanation keyword for
;;;;  the planner has been given a true value.
;;;
;;;; Given a goal unified by a valid satisfier, construct an assertion
;;;;  that explains how the unifier satisfied that goal and an
;;;;  attribution for the explanation (if it exists).  For example, if
;;;;  the original goal were:
;;;;    '(and (or (and (on ?a ?b) (on ?a ?c)) (on ?d ?a)) (on ?b ?c))
;;;;  and a unifier ((?a.x1) (?b.x2) (?c.x3)) then the explanation would be:
;;;;    '(and (and (on x1 x2) (on x1 x3)) (on x2 x3))
;;;;
;;;; If any term or conjunct in the explanation comes from an external
;;;;  query (see the external-query routine), a list containing the term
;;;;  :source followed by attribution information is added to the
;;;;  beginning of the term or conjunct.  In the above example, if the
;;;;  attribution for (and (on x1 x2) (on x1 x3)) were (PoliceReport
;;;;  UID1234), and the attribution for (on x2 x3) were (PoliceReport
;;;;  UID4321) then the explanation would be
;;;;    '(and ((:source PoliceReport UID1234) and (on x1 x2) (on x1 x3))
;;;;          ((:source PoliceReport UID4321) on x2 x3))
;;;(defun explain-satisfier (unified-goal state &optional external)
;;;  (let ((*external-access* nil)) ; otherwise we'd query twice
;;;    (cond
;;;     ((member (first unified-goal)
;;;              '(sort-by not eval call assign imply forall))
;;;     ; The above constructs are not handled by the explanation code yet.
;;;      nil)
;;;     ((eq (first unified-goal) :external)
;;;      (explain-satisfier (rest unified-goal) state t))
;;;     ((eq (first unified-goal) 'or)
;;;      (cond
;;;       ((null (rest unified-goal)) nil)
;;;       ((not (find-satisfiers (second unified-goal) state))
;;;        (explain-satisfier (cons 'or (rest (rest unified-goal)))
;;;                           state external))
;;;       (t
;;;        (explain-satisfier (second unified-goal) state external))))
;;;
;;;     ((eq (first unified-goal) 'and)
;;;      (let* ((explanation-list
;;;              (mapcar #'(lambda (g) (explain-satisfier g state external)) 
;;;                      (rest unified-goal)))
;;;             (simplified-explanation-list (remove nil explanation-list))
;;;             (explanation
;;;              (when (find-satisfiers unified-goal state)
;;;                (cons 'and simplified-explanation-list))))
;;;        (when explanation
;;;          (add-source unified-goal external explanation))))
;;;
;;;     ((listp (first unified-goal)) ; implicit and
;;;      (explain-satisfier (cons 'and unified-goal) state external))
;;;     (t ; logical-atom
;;;      (add-source unified-goal external unified-goal)))))
;;;
;;;(defun add-source (unified-goal external explanation)
;;;  (if external
;;;      (let ((source (get-attribution unified-goal)))
;;;        (if source
;;;            (cons (list :source source) explanation)
;;;          explanation))
;;;    explanation))
;;;  
;;;(defun get-attribution (unified-query)
;;;;(format t "~%Source access: ~a from~%   ~a" unified-query *attribution-list*)
;;;  (second (assoc unified-query *attribution-list* 
;;;                 :test #'(lambda (q att)
;;;                           (or (equal q att)
;;;                               (equal (list 'and q) att))))))
;;;
;;;(defun fully-instantiated-goal (goal)
;;;  (if (atom goal)
;;;      (not (variablep goal))
;;;    (and (fully-instantiated-goal (first goal))
;;;         (fully-instantiated-goal (rest goal)))))
;;;
;;;;;; ------------------------------------------------------------------------
;;;;;; External access to state information
;;;;;; ------------------------------------------------------------------------
;;;
;;;; The following code is invoked only if an external-access-hook routine
;;;;   has been defined.
;;;
;;;; If external-query receives a query of the form (<pred> <val> <val>)
;;;;   or (and (<pred> <val> <val>)+), it sends that query to
;;;;   external-access-hook.  If the query succeeds, the resulting
;;;;   information is added to the state and the attribution information
;;;;   is stored in *attribution-list*.
;;;;  If external-query receives a complex query involving and's and
;;;;   or's, it decomposes that into queries it can send to
;;;;   external-access-hook.  If external-query encounters other logical
;;;;   constructs (e.g., not, imply), external-query returns nil (but
;;;;   any responses it had already received are still kept in the
;;;;   state and *attribution-list*.
;;;(defun  external-query (query state)
;;;;  (format t "~%potential query: ~s" query)
;;;  (cond
;;;   ((null query) nil)
;;;   ((member (first query) '(not eval call assign assign* imply forall sort-by))
;;;    nil)
;;;   ((listp (first query)) ; implicit and
;;;    (external-query (cons 'and query) state))
;;;   ((eq (first query) 'or)
;;;    (or (external-query (second query) state)
;;;        (when (rest (rest query))
;;;          (external-query `(or ,(rest (rest query))) state))))
;;;   ((and (eq (first query) 'and) (rest query))
;;;    (if (find-if-not #'(lambda (subquery) 
;;;                         (and (listp subquery)
;;;                              (= (length subquery) 3)
;;;                              (not (logical-keywordp (first subquery)))
;;;                              (not (find-if-not #'atom subquery))))
;;;                     (rest query))
;;;        ; complex query - try to decompose
;;;        (let ((first-response (external-query (second query) state)))
;;;          (when first-response
;;;            (let ((rest-response (external-query `(and ,@(rest (rest query)))
;;;                                                 state)))
;;;              (when rest-response
;;;                (merge-binding-set-lists first-response rest-response)))))
;;;      ; simple query - invoke external access
;;;      (invoke-external-query query state)))
;;;   (t ; query of a single logical atom
;;;    (invoke-external-query (list 'and query) state))))
;;;
;;;(defun logical-keywordp (sym)
;;;  (member sym '(and or not eval call assign assign* imply forall sort-by)))
;;;
;;;; Takes two lists of sets of bindings and returns a list of sets of
;;;;  bindings consisting of all consistent combinations of one set from
;;;;  each list.  E.g.:
;;;;
;;;; (merge-binding-set-lists '(((?x 1) (?y 2)) ((?x 3) (?z 4)))
;;;;                          '(((?x 1) (?a 5)) ((?b 6) (?c 7))))
;;;; =>
;;;; '(((?x 1) (?y 2) (?a 5)) ((?x 1) (?y 2) (?b 6) (?c 7))
;;;;   ((?x 3) (?z 4) (?b 6) (?c 7)))
;;;;
;;;; Note that the second set in the first list and the first set in the
;;;;  second list are not merged in the result because they have
;;;;  incompatible bindings for ?x.
;;;(defun merge-binding-set-lists (binding-sets1 binding-sets2
;;;                                              &optional original-binding-sets2)
;;;  (cond
;;;   ((null binding-sets1) nil)
;;;   ((null binding-sets2)
;;;    (when original-binding-sets2
;;;      (merge-binding-set-lists (rest binding-sets1) 
;;;                               original-binding-sets2 original-binding-sets2)))
;;;   (t
;;;    (let* ((original-sets2 (or original-binding-sets2 binding-sets2))
;;;           (first-merge 
;;;            (merge-binding-sets (first binding-sets1) (first binding-sets2)))
;;;           (rest-merge 
;;;            (merge-binding-set-lists binding-sets1 (rest binding-sets2) 
;;;                                     original-sets2)))
;;;      (if first-merge
;;;          (cons first-merge rest-merge)
;;;        rest-merge)))))
;;;
;;;(defun merge-binding-sets (binding-set1 binding-set2)
;;;  (append
;;;   binding-set1
;;;   (remove-if
;;;    #'(lambda (binding2)
;;;        (find-if #'(lambda (binding1)
;;;                     (cond
;;;                      ((equal binding1 binding2) t)
;;;                      ((eq (first binding1) (first binding2))
;;;                       (return-from merge-binding-sets nil))
;;;                      (t nil)))
;;;                 binding-set1))
;;;    binding-set2)))
;;;
;;;; Directly invokes external-access-hook on a query of the form
;;;;  (and (<pred> <val> <val>)+) and returns the resulting bindings,
;;;;  stores the attribution information in *attribution-list*, and
;;;;  stores the resulting facts in the state [NOTE: adding an atom to
;;;;  the state here has wierd implications for backtracking, etc.; may
;;;;  want to reconsider.]
;;;(defun invoke-external-query (query state)
;;;  (mapcar
;;;   #'(lambda (attributed-binding-set)
;;;       (let* ((attribution (first attributed-binding-set))
;;;              (binding-set (fix-uninterned-bindings
;;;                            (mapcar #'(lambda (l)
;;;                                        (cons (first l) (second l)))
;;;                                    (second attributed-binding-set))
;;;                            (extract-variables query)))
;;;              (unified-query (apply-substitution query binding-set)))
;;;         (setf *attribution-list* (cons (list unified-query attribution)
;;;                                        *attribution-list*))
;;;         (dolist (fact (rest unified-query))
;;;                 (add-atom-to-state fact state nil nil))
;;;         binding-set))
;;;     (funcall (fdefinition 'external-access-hook) query)))
;;;
;;;;;;; Some bindings returned in a query may involve uninterned symbols.  This
;;;;;;;  routine substitutes those symbols with the original values.
;;;;;;(defun fix-uninterned-bindings (bindings query-vars)
;;;;;;;(format t "~%bindings: ~s - vars: ~s" bindings query-vars)
;;;;;;  (mapcar #'(lambda (binding)
;;;;;;              (let* ((name (symbol-name (car binding)))
;;;;;;                     (matching-var
;;;;;;                      (find-if 
;;;;;;                       #'(lambda (v) (string-equal (symbol-name v) name))
;;;;;;                       query-vars)))
;;;;;;                (if matching-var
;;;;;;                    (cons matching-var (cdr binding))
;;;;;;                  binding)))
;;;;;;          bindings))
;;;
;;;; Sample behavior of an external-access-hook:
;;;;  Takes a goal as input, returns bindings and attribution.
;;;(defun DUMMY-external-access-hook (goal)
;;;  (format t "~%external query: ~s" goal)
;;;  (let ((dummy-goal '(and
;;;                     (employees ?business UID101)
;;;                     (hasMembers UID101 ?business)))
;;;        (dummy-bindings `((,(second (second goal)) UID102)))
;;;        (dummy-attribution '(UID103 BusinessNews)))
;;;    (when (goal-equalp goal dummy-goal)
;;;      (list (list dummy-attribution dummy-bindings)))))
;;;
;;;(defun goal-equalp (g1 g2)
;;;  (cond
;;;   ((eq g1 g2) t)
;;;   ((and (variablep g1) (variablep g2)) t)
;;;   ((and (listp g1) (listp g2) 
;;;         (= (length g1) (length g2)) 
;;;         (goal-equalp (first g1) (first g2))
;;;         (goal-equalp (rest g1) (rest g2)))
;;;    t)
;;;   (t nil)))

(defmacro catch-internal-time (&rest body)
  `(if *internal-time-limit*
       (catch *internal-time-tag*
         ,@body)
       (progn ,@body)))

;;; ------------------------------------------------------------------------
;;; Top-level calls to the planner
;;; ------------------------------------------------------------------------
(defun find-plans (problem
                   &key (domain *domain*) (which *which*) (verbose *verbose*)
                        (gc *gc*) (pp *pp*) (state *state-encoding*) 
                        (max-plans *max-plans*)
                        (plan-tree *plan-tree*) (optimize-cost *optimize-cost*)
                        (time-limit *time-limit*) (explanation *explanation*)
                        (depth-cutoff *depth-cutoff*)
                        hand-steer leashed
                        )
  "FIND-PLANS looks for solutions to the planning problem named PROBLEM.       
   The keyword arguments are as follows:                                       
     :WHICH tells what kind of search to do.  Its possible values are:         
        :FIRST      - depth-first search, returning the first plan found.      
        :ALL        - depth-first search for *all* plans.                      
        :SHALLOWEST - depth-first search for the shallowest plan in the        
                      search space (this usually is also the shortest plan).   
                      If there's more than one such plan, return the first.    
        :ALL-SHALLOWEST - depth-first search for all shallowest plans.         
        :ID-FIRST   - iterative deepening search, returning the first plan.    
        :ID-ALL     - iterative deepening search for all shallowest plans.     
     :VERBOSE says how much information to print about the plans SHOP2         
              finds.  Its values can be any of the following:                  
        0 or NIL    - print nothing                                            
        1 or :STATS - print some statistics on SHOP2's operation               
        2 or :PLANS - print the stats and print all plans found, but omit      
             operator costs and omit all operators whose names start with \"!!\" 
        3 or :LONG-PLANS - print the stats and plans, including all operator   
             costs and all operators (even those whose names start with \"!!\")  
     :GC says whether to do a garbage collection before calling SEEK-PLANS
     :PLAN-TREE indicates whether or not to return a plan tree."
  ;; my compiler doesn't like this --- what is IGNORABLE?
  ;;  (declare (ignorable gc))

  ;; I'm not sure what the MCL function is...
  #+(or :MCL :allegro :sbcl)(when gc #+allegro (excl:gc t)
                            #+sbcl (sb-ext:gc)
                    #+mcl (error "Need the name of the MCL gc function here."))
  #-(or :MCL :allegro :sbcl)
  (when gc (cerror "Just continue, skip GC."
                   "Requested GC before planning, but do not know how to request GC for this lisp implementation (see source code)."))
  (let* ((*start-run-time* (get-internal-run-time))
         (*start-real-time* (get-internal-real-time))
         (*internal-time-limit* 
          (if time-limit (* time-limit
                            internal-time-units-per-second)
            nil))
         (*internal-time-tag* (gensym))
         (*print-pretty* pp)
         (*state-encoding* state)
         (*plan-tree* plan-tree)
;         (*subtask-parents* nil) (*operator-tasks* nil)
         (*optimize-cost* optimize-cost)
         (*expansions* 0) (*inferences* 0)
         (total-expansions 0) (total-inferences 0)
         (old-expansions 0) (old-inferences 0)
         (total-run-time 0) (total-real-time 0)
         ;; make this controllable [2004/08/06:rpg]
         (*depth-cutoff* depth-cutoff)
             (*plans-found* nil)
         (*optimal-plan* 'fail)
             (*explanation* explanation) (*attribution-list* nil)
             (*external-access* (fboundp 'external-access-hook))
             (*trace-query* (fboundp 'trace-query-hook))
         (state (make-state (get-state problem)))
         (tasks (get-tasks problem))
         *optimal-cost* *verbose*
         new-run-time new-real-time top-tasks
         ;; used for tree-building code.
         (*unifiers-found* nil)
         ;; if you want the state trajectory [2004/09/14:rpg]
         (*states-found* nil)
         ;; used dynamically to allow the user to choose tasks for
         ;; expansion, rather than allowing SHOP2 to search
         ;; autonomously. [2004/03/30:rpg]
         (*hand-steer* hand-steer)
         (*max-plans* max-plans)
         (*leashed* leashed)
         (*domain* domain)
         )
          (setq *subtask-parents* nil)
          (setq *operator-tasks* nil)
    ;; we need to be sure that the pieces of the input tasks are
    ;; properly recognized as being/not being variables, etc. This
    ;; used to be done in make-problem, but now that
    ;; set-variable-property has a domain argument... [2007/03/14:rpg]
    (set-variable-property domain tasks)
    
    (setq top-tasks (get-top-tasks tasks))
    ;;    (format t "~%Top-Tasks: ~a" top-tasks)
    (determine-verbosity verbose)

    (when *print-stats*
      (format t "~%---------------------------------------------------------------------------")
      (format t "~%Problem ~s with :WHICH = ~s, :VERBOSE = ~s" problem which verbose)    
      (if optimize-cost
          (format t ", OPTIMIZE-COST = ~s" optimize-cost)))
    ;; if *hand-steer* allows the user to abort planning.
    (catch 'user-done 
      (ecase which 
        ((:id-first :id-all)
         (print-stats-header "Depth")
         (do ((*depth-cutoff* 0 (1+ *depth-cutoff*)))
             ((or (time-expired-p)        ;need to check here for expired time....
                  (and *plans-found*
                       ;; I think the second disjunct here is probably
                       ;; unnecessary now [2005/01/10:rpg]
                       (not (or (optimize-continue-p (if (eq which :id-first) :first :all))
                                (eq which :id-all)))))
              nil)
           (setq new-run-time (get-internal-run-time)
                 new-real-time (get-internal-real-time))
           (catch-internal-time
            (seek-plans domain state tasks top-tasks nil 0 0 
                        (if (eq which :id-first) :first :all) nil
                        ;; unifier
                        nil
                        ))
           (setq new-run-time (- (get-internal-run-time) new-run-time)
                 new-real-time (- (get-internal-real-time) new-real-time)
                 total-run-time (+ total-run-time new-run-time)
                 total-real-time (+ total-real-time new-real-time)
                 total-expansions (+ total-expansions *expansions*)
                 total-inferences (+ total-inferences *inferences*))
           (print-stats *depth-cutoff* *plans-found* *expansions*
                        *inferences* new-run-time new-real-time)
           (and (equal *expansions* old-expansions)
                (equal *inferences* old-inferences)
                (progn (format t "~&Ending at depth ~D: no new expansions or inferences.~%"
                               *depth-cutoff*)
                       (return nil)))                ; abort if nothing new happened on this iteration
           (setq old-expansions *expansions*
                 old-inferences *inferences*)
           (setq *expansions* 0
                 *inferences* 0)))
        
        ((:first :all :shallowest :all-shallowest)
         (catch-internal-time
          (seek-plans domain state tasks top-tasks nil 0 0 which nil
                      ;; unifier
                      nil
                      ))
         (setq total-expansions *expansions*
               total-inferences *inferences*))))

    ;; I'm pretty sure that this needs to be modified to update the
    ;; unifiers and states found as well.  [2005/01/06:rpg]
    (if (numberp *optimize-cost*)
        (setq *plans-found*
              (delete-if
               #'(lambda (plan)
                   (> (plan-cost plan) *optimize-cost*))
               *plans-found*)))


    (setq total-run-time (- (get-internal-run-time) *start-run-time*)
          total-real-time (- (get-internal-real-time) *start-real-time*))

    (print-stats-header "Totals:")
    (print-stats "" *plans-found* total-expansions total-inferences
                 total-run-time total-real-time)
    (let ((plan-trees
           (when *plan-tree*
             (loop for plan in *plans-found*
                   for unifier in *unifiers-found*
                   for tree = (extract-tree plan)
                   collect (apply-substitution tree unifier)))))
      (when *print-plans*
        (cond
         (*plan-tree*
          (format t "~%Plan trees:~%~s~%~%" plan-trees))
         (*pshort*
          (format t "~%Plans:~%~s~%~%" (mapcar #'shorter-plan *plans-found*)))
         (t
          (format t "~%Plans:~%~s~%~%" *plans-found*))))
      (if *plan-tree*
          (values *plans-found*
                  (+ 0.0
                     (/ total-run-time
                        internal-time-units-per-second))
                  plan-trees
                  *states-found*)
        (values *plans-found*
                (+ 0.0
                   (/ total-run-time
                      internal-time-units-per-second)))))))

(format t "~2%SHOP2 version ~a~%~a~%" cl-user::+shop-version+ +shopyright+)

