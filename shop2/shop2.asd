;;; The Original Code is SHOP2.  ASDF system definitions developed by
;;; Robert P. Goldman, John Maraist.  Portions created by Drs. Goldman
;;; and Maraist are Copyright (C) 2004-2007 SIFT, LLC.

;;; Smart Information Flow Technologies Copyright 2006-2007 Unpublished work
;;; 
;;; GOVERNMENT PURPOSE RIGHTS
;;; 
;;; Contract No.         FA8650-06-C-7606, 
;;; Contractor Name      Smart Information Flow Technologies, LLC
;;;                      d/b/a SIFT, LLC
;;; Contractor Address   211 N 1st Street, Suite 300
;;;                      Minneapolis, MN 55401
;;; Expiration Date      5/2/2011
;;; 
;;; The Government's rights to use, modify, reproduce, release,
;;; perform, display, or disclose this software are restricted by
;;; paragraph (b)(2) of the Rights in Noncommercial Computer Software
;;; and Noncommercial Computer Software Documentation clause contained
;;; in the above identified contract. No restrictions apply after the
;;; expiration date shown above. Any reproduction of the software or
;;; portions thereof marked with this legend must also reproduce the
;;; markings.


 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2012
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE. 
 
 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; (at your option) any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defpackage :shop2-asd
    (:use :common-lisp :asdf)
    )
(in-package :shop2-asd)

(defclass shop-tester (system)
     ()
  (:documentation "This class allows special system behaviors
for systems used to test shop."))

(defmethod operation-done-p 
           ((o test-op)
            (c shop-tester))
  "We need to make sure that operation-done-p doesn't return its
normal value, or a test-op will be run only once."
  (values nil))

(defclass cl-file-with-defconstants ( cl-source-file )
     ()
  (:documentation "Allows us to quash SBCL errors about 
complex defconstants."))

(defclass tester-cl-source-file ( cl-file-with-defconstants )
  ()
  (:documentation "Special class that will have to recompile no matter
what..."))

(defmethod operation-done-p 
           ((o compile-op)
            (c tester-cl-source-file))
  "We are crushing the normal operation-done-p for compile-op, so that
the tester files get recompiled, to take into account any changes to
shop2."
  (values nil))

;;;(defmethod perform :around
;;;           ((o compile-op)
;;;            (c tester-cl-source-file))
;;;  "Make the tests and test results, with their pprinted ANSI output
;;;be compatible with ACL's modern mode."
;;;  (let ((*readtable* (copy-readtable)))
;;;    (setf (readtable-case *readtable*) :upcase)
;;;    (call-next-method)))


(defconstant +shop-package+ :shop2-user)

(defconstant cl-user::+shop-version+ "2.0.1")

(defsystem :shop2
    :serial t
    :default-component-class cl-file-with-defconstants
    :depends-on (:shop2-core :shop2-theorem-prover)
    :version #.cl-user::+shop-version+
    :in-order-to ((test-op
		   (test-op
		    ;; bitrotted [2007/10/22:rpg]
		    ;; :shop-semweb
		    :shop-pddl-tests
		    :shop-logistic
		    :shop-blocks :shop-umt
		    :shop-depots
		)))
    :components ((:file "methods-domains")
		 (:file "tree-accessors")
		 (:file "pddl")))

(defsystem :shop-pddl-tests
    :class shop-tester
    :depends-on (:shop2 :rrt)
    :in-order-to ((test-op (load-op :shop-pddl-tests)))
    :components ((:file "pddl-unit-tests")))

(defmethod perform ((op test-op)
		    (system (eql (find-system :shop-pddl-tests))))
  (let ((do-test (intern (symbol-name '#:do-test) :rrt)))
    (eval `(,do-test
	       ',(intern (symbol-name '#:simple-pddl-actions) :shop2)))
    (eval `(,do-test
	       ',(intern (symbol-name '#:defdomain-single-action) :shop2)))
    (eval `(,do-test
	       ',(intern (symbol-name '#:pddl-add-and-delete) :shop2)))))
  
;;;; handle SBCL's strict notion of the way DEFCONSTANT should work. [2006/05/16:rpg]
;;;#+sbcl
;;;(defmethod traverse ((op operation) (c shop-tester))
;;;  (handler-bind ((sb-ext:defconstant-uneql
;;;		     #'(lambda (c)
;;;			 (continue c))))
;;;    (call-next-method)))


#+sbcl
(defmethod perform :around ((op operation)
			    (c cl-file-with-defconstants))
  (handler-bind ((sb-ext:defconstant-uneql
		     #'(lambda (c)
			 (continue c))))
    (call-next-method)))


;;;---------------------------------------------------------------------------
;;; SHOP-UMT domain tester.
;;;---------------------------------------------------------------------------

(defsystem :shop-umt
    :class shop2-asd::shop-tester
    :depends-on (:shop2)
    :default-component-class tester-cl-source-file
    :in-order-to ((test-op (load-op :shop-umt))
		  (load-op (compile-op :shop-umt)))
    :pathname #.(merge-pathnames (make-pathname :directory '(:relative :up "examples" "UMT2")) *load-truename*)
    :components ((:file "UMT2")
		 (:file "pfile1" :depends-on ("UMT2"))
		 (:file "pfile2" :depends-on ("UMT2"))
		 ;; interestingly, pfile3 does not seem solvable.
		 ;; Haven't checked to see why [2006/05/10:rpg]
		 (:file "pfile3" :depends-on ("UMT2"))
		 (:file "plans")
		 ))
    
;;; needs to be changed to check the results we find.
(defmethod perform :after ((op test-op) (component (eql (find-system :shop-umt))))
  (eval `(,(intern (symbol-name '#:test-umt-shop) +shop-package+))))


(defsystem :shop-blocks
    :class shop2-asd::shop-tester
    :depends-on (:shop2)
    :default-component-class tester-cl-source-file
    :pathname #.(merge-pathnames (make-pathname :directory '(:relative :up "examples" "blocks")) *load-truename*)
    :in-order-to ((test-op (load-op :shop-blocks))
		  (load-op (compile-op :shop-blocks)))
    :components ((:file "block2")
		 (:file "problem100" :depends-on ("block2"))
		 (:file "problem200" :depends-on ("block2"))
		 (:file "problem300" :depends-on ("block2"))
		 (:file "plans" :depends-on ("problem100" "problem200" "problem300"))))
    
(defmethod perform :after ((op test-op) (component (eql (find-system :shop-blocks))))
  (multiple-value-bind (success failed-test-names)
      (eval (list (intern "BW-TESTS" +shop-package+)))
    (if success t
      (progn
	(warn "Failed the following blocks world tests: ~S" failed-test-names)
	;; this seems like The Wrong Thing --- there should be a
	;; specific test-failed error... [2006/05/09:rpg]
	(cerror "Continue and return nil from perfoming test-op."
		(make-condition 'operation-error
		  :component component
		  :operation op))
	nil))))



(defsystem :shop-depots
    :class shop2-asd::shop-tester
    :default-component-class tester-cl-source-file
    :depends-on (:shop2)
    :pathname #.(merge-pathnames (make-pathname :directory '(:relative :up "examples" "depots")) *load-truename*)
    :in-order-to ((test-op (load-op :shop-depots))
		  (load-op (compile-op :shop-depots)))
    :components ((:file "depots")
		 (:file "pfile1" :depends-on ("depots"))
		 (:file "pfile2" :depends-on ("depots"))
		 (:file "pfile3" :depends-on ("depots"))
		 (:file "pfile4" :depends-on ("depots"))
		 (:file "pfile5" :depends-on ("depots"))
		 (:file "pfile6" :depends-on ("depots"))
		 (:file "pfile7" :depends-on ("depots"))
		 (:file "pfile8" :depends-on ("depots"))
		 (:file "pfile9" :depends-on ("depots"))
		 (:file "pfile10" :depends-on ("depots"))
		 (:file "pfile11" :depends-on ("depots"))
		 (:file "pfile12" :depends-on ("depots"))
		 (:file "pfile13" :depends-on ("depots"))
		 (:file "pfile14" :depends-on ("depots"))
		 (:file "pfile15" :depends-on ("depots"))
		 (:file "pfile16" :depends-on ("depots"))
		 (:file "pfile17" :depends-on ("depots"))
		 (:file "pfile18" :depends-on ("depots"))
		 (:file "pfile19" :depends-on ("depots"))
		 (:file "pfile20" :depends-on ("depots"))
		 (:file "pfile21" :depends-on ("depots"))
		 (:file "pfile22" :depends-on ("depots"))
		 (:file "plans")))
    

    
;;; needs to be changed to check the results we find.
(defmethod perform :after ((op test-op) (component (eql (find-system :shop-depots))))
  (eval (list (intern "TEST-SHOP-DEPOTS" +shop-package+))))

(defsystem :shop-logistic
    :class shop2-asd::shop-tester
    :default-component-class tester-cl-source-file
    :depends-on (:shop2)
    :pathname #.(merge-pathnames (make-pathname :directory '(:relative :up "examples" "logistic")) *load-truename*)
    :components ((:file "logistic")
		 (:file "Log_ran_problems_15" :depends-on ("logistic"))
		 (:file "Log_ran_problems_20" :depends-on ("logistic"))
		 (:file "Log_ran_problems_25" :depends-on ("logistic"))
		 (:file "Log_ran_problems_30" :depends-on ("logistic"))
		 (:file "Log_ran_problems_35" :depends-on ("logistic"))
		 (:file "Log_ran_problems_40" :depends-on ("logistic"))
		 (:file "Log_ran_problems_45" :depends-on ("logistic"))
		 (:file "Log_ran_problems_50" :depends-on ("logistic"))
		 (:file "Log_ran_problems_55" :depends-on ("logistic"))
		 (:file "Log_ran_problems_60" :depends-on ("logistic"))
		 (:file "plans")
		 )
    :in-order-to ((test-op (load-op :shop-logistic))
		  (load-op (compile-op :shop-logistic))))
    
;;; needs to be changed to check the results we find.
(defmethod perform :after ((op test-op) (component (eql (find-system :shop-logistic))))
  (eval `(,(intern "TEST-LOGISTICS-PLANS" +shop-package+))))

;;; make sure we don't do this only once...

;;;---------------------------------------------------------------------------
;;; SHOP2 Web Services tester.
;;; this seems bitrotted... [2007/10/22:rpg]
;;;---------------------------------------------------------------------------

#|
(defsystem :shop-semweb
    :class shop2-asd::shop-tester
    :depends-on (:shop2)
    :default-component-class tester-cl-source-file
    :in-order-to ((test-op (load-op :shop-semweb))
                  (load-op (compile-op :shop-semweb)))
    :pathname #.(merge-pathnames (make-pathname :directory '(:relative :up "examples" "WebServices")) *load-truename*)
    :components (
                 (:file "poirot")
                 (:file "PoirotSimulation")
                 (:file "poirot-problem" :depends-on ("poirot" "PoirotSimulation"))
                 (:file "plans")
                 ))

;;; needs to be changed to check the results we find.
(defmethod perform :after ((op test-op) (component (eql (find-system :shop-semweb))))
  (eval `(,(intern "TEST-POIROT-SHOP" :shop2))))
|#


