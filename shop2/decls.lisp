;;; The Original Code is SHOP2.  
;;; 
;;; The Initial Developer of the Original Code is the University of
;;; Maryland. Portions created by the Initial Developer are Copyright (C)
;;; 2002,2003 the Initial Developer. All Rights Reserved.
;;;
;;; Additional developments made by Robert P. Goldman, John Maraist.
;;; Portions created by Drs. Goldman and Maraist are Copyright (C)
;;; 2004-2007 SIFT, LLC.
;;;
;;; Smart Information Flow Technologies Copyright 2006-2007 Unpublished work
;;; 
;;; GOVERNMENT PURPOSE RIGHTS
;;; 
;;; Contract No.         FA8650-06-C-7606, 
;;; Contractor Name      Smart Information Flow Technologies, LLC
;;;                      d/b/a SIFT, LLC
;;; Contractor Address   211 N 1st Street, Suite 300
;;;                      Minneapolis, MN 55401
;;; Expiration Date      5/2/2011
;;; 
;;; The Government's rights to use, modify, reproduce, release,
;;; perform, display, or disclose this software are restricted by
;;; paragraph (b)(2) of the Rights in Noncommercial Computer Software
;;; and Noncommercial Computer Software Documentation clause contained
;;; in the above identified contract. No restrictions apply after the
;;; expiration date shown above. Any reproduction of the software or
;;; portions thereof marked with this legend must also reproduce the
;;; markings.

 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE. 
 
 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; (at your option) any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :shop2)

#+sbcl
(defmacro defconstant (name value &optional doc)
       `(common-lisp:defconstant ,name (if (boundp ',name) (symbol-value ',name) ,value)
                           ,@(when doc (list doc))))

(defgeneric set-variable-property (domain x)
  (:documentation
   "Record facts about X being a variable, operator, or
other special symbol.  Done for side-effects."
   )
)

(defgeneric install-domain (domain &optional redefine-ok)
  (:documentation "Record DOMAIN for later retrieval.  
Currently records the domain on the prop-list of the domain's name.
By default will warn if you redefine a domain.  If REDEFINE-OK is
non-nil, redefinition warnings will be quashed (handy for test suites)."))

(defgeneric handle-domain-options (domain &key type &allow-other-keys)
  (:documentation "Handle the options in option-list, 
as presented to defdomain.  These are typically ways of 
tailoring the domain object, and should be keyword arguments.
Returns no value; operates by side effects on DOMAIN."))

(defgeneric parse-domain-items (domain items)
  (:documentation "Process all the items in ITEMS.  These will be 
methods, operators, axioms, and whatever special items domain 
subclasses may require.  
Returns no value; operates by side effects on DOMAIN."))

(defgeneric parse-domain-item (domain item-keyword item)
  (:documentation "The default method for parse-domain-items 
will invoke this method to process a single item s-expression.  
The addition of this generic function makes SHOP2's syntax 
more extensible.")
  )

(defgeneric process-operator (domain operator-def)
  (:documentation "This generic function allows for specialization 
by domain type on how operator definitions are parsed.  
Should return something suitable for installation in the 
operators table of DOMAIN."))

(defgeneric process-method (domain method-def)
  (:documentation "This generic function allows for 
specialization by domain type on how method definitions 
are parsed.  Should return something suitable for 
installation in the methods table of DOMAIN."))

(defgeneric regularize-axiom (domain axiom-def)
  (:documentation "This generic function allows for 
specialization by domain type on how axiom definitions 
are parsed.  Should return something suitable for 
installation in the axioms table of DOMAIN."))

(defgeneric task-sorter (domain tasks unifier)
  (:documentation
   "This function allows customization of choice of pending task
to expand.  SHOP2 search behavior can be changed by specializing this 
generic function (most likely using the domain argument).  Returns a 
sorted list of tasks in the order in which they should be expanded.  
A failure can be triggered by returning NIL."))

(defmethod methods ((domain domain) (name symbol))
  (gethash name (domain-methods domain)))

(defmethod operator ((domain domain) (name symbol))
  (gethash name (domain-operators domain)))

(defmethod axioms ((domain domain) (name symbol))
  "Return a list of axioms for for NAME as defined in DOMAIN."
  (gethash name (domain-axioms domain))
  )



;;;;;; I believe that two changes should be made to this function (at least!):
;;;;;; 1.  It should be renamed to apply-primitive and
;;;;;; 2.  The CLOSage should be modified to make the operators be
;;;;;; objects so that we can dispatch on them.  Currently this isn't
;;;;;; really workable because the operators are lists (so that we can do
;;;;;; things like standardize them).
(defgeneric apply-operator (domain state task-body operator protections depth in-unifier)
  (:documentation
   "If OPERATOR is applicable to TASK in STATE, then APPLY-OPERATOR returns
five values:
1.  the operator as applied, with all bindings;
2.  a state tag, for backtracking purposes;
3.  a new set of protections;
4.  the cost of the new operator;
5.  unifier.
This function also DESTRUCTIVELY MODIFIES its STATE argument.
Otherwise it returns FAIL."))

(defgeneric seek-plans-primitive (domain task1 task-name task-body state tasks top-tasks
                                  partial-plan partial-plan-cost depth which-plans protections
                                  unifier)
  (:documentation "If there is an OPERATOR that is applicable to the primitive TASK in STATE,
then SEEK-PLANS-PRIMITIVE applies that OPERATOR, generates the successor state, updates the current
partial plan with the OPERATOR, and continues with planning by recursively calling SEEK-PLANS. 
Otherwise, it returns FAIL."))

(defgeneric seek-plans-null (domain state which-plans partial-plan partial-plan-cost depth unifier)
  (:documentation " This SEEK-PLANS-NULL is used with WebServices to strip the add and del lists from
the actions in the partial plan before doing the actual SEEK-PLANS-NULL..."))

;;; Below, SEEK-PLANS-XXX method definitions take as input a DOMAIN object and a LTML-PLANNER-STATE object.
;;; Here, I would like to implement the ideas we discussed about CLOSifying SHOP2's internal planner state
;;; and passing planner-state objects around in the SEEK-PLANS-XXX functions, instead of the current crazy
;;; list of arguments. I am going to assume that we define a top-level PLANNER-STATE object that would
;;; probably include the current state, the partial plan, and perhaps the current task. Then, SHOP2 would
;;; use its inherited version SHOP2-PLANNER-STATE and here we would use our inherited version LTML-PLANNER-STATE.
;;; This would also help plugging ND-SHOP2, and perhaps Yoyo, in the system, since they use their slightly different
;;; planner-state versions. [2006/12/28:uk]

(defgeneric seek-plans (domain state tasks top-tasks partial-plan partial-plan-cost depth which-plans 
			  protections unifier)
  (:documentation "Top-level task-decomposition function."))

(defgeneric seek-plans-task (domain task1 state tasks top-tasks partial-plan
                             partial-plan-cost depth which-plans protections unifier)
  (:documentation "This is a wrapper function that checks whether the current task to be decomposed is primitive or not,
		   and depending on the task's type, it invokes either SEEK-PLANS-PRIMITIVE or SEEK-PLANS-NONPRIMITIVE,
		   which are the real task-decomposition workhorses."))

(defgeneric seek-plans-nonprimitive (domain task1 task-name task-body state tasks top-tasks
                                     partial-plan partial-plan-cost depth which-plans protections
                                     unifier)
  (:documentation "Applies the HTN method to the current TASK1 and generates the subtasks of TASK. 
		   Recursively calls the task-decomposition routine to decompose the subtasks. In the LTML context,
		   What should it do with the OUTPUTS and/or the RESULTS of that method? (TBD) "))

(defgeneric apply-method (domain state task-body method protections depth in-unifier)
  (:documentation "Applies the LTML method and returns a decomposition of the TASK. In LTML context,
		   what should it do with the OUTPUTS and/or the RESULTS of that method? (TBD) "))


;;; DOMAINS are in place --- PROBLEM objects not yet in use.
#|
(defclass problem ()
     ((domain-name
       :initarg :domain-name
       :reader domain-name
       :initform nil
       :documentation
       "The programmer MAY (but is not obligated to) specify that a problem
is intended for use with a particular domain definition."
       ))
  (:documentation "An object representing a SHOP2 problem.")
  )
|#

