;;; -*- Mode: common-lisp; package: shop2.theorem-prover; -*-
;;; The Original Code is SHOP2.  
;;; 
;;; The Initial Developer of the Original Code is the University of
;;; Maryland. Portions created by the Initial Developer are Copyright (C)
;;; 2002,2003 the Initial Developer. All Rights Reserved.
;;;
;;; Additional developments made by Robert P. Goldman, John Maraist.
;;; Portions created by Drs. Goldman and Maraist are Copyright (C)
;;; 2004-2007 SIFT, LLC.

;;; Smart Information Flow Technologies Copyright 2006-2007 Unpublished work
;;; 
;;; GOVERNMENT PURPOSE RIGHTS
;;; 
;;; Contract No.         FA8650-06-C-7606, 
;;; Contractor Name      Smart Information Flow Technologies, LLC
;;;                      d/b/a SIFT, LLC
;;; Contractor Address   211 N 1st Street, Suite 300
;;;                      Minneapolis, MN 55401
;;; Expiration Date      5/2/2011
;;; 
;;; The Government's rights to use, modify, reproduce, release,
;;; perform, display, or disclose this software are restricted by
;;; paragraph (b)(2) of the Rights in Noncommercial Computer Software
;;; and Noncommercial Computer Software Documentation clause contained
;;; in the above identified contract. No restrictions apply after the
;;; expiration date shown above. Any reproduction of the software or
;;; portions thereof marked with this legend must also reproduce the
;;; markings.


 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE. 
 
 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; (at your option) any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :shop2.theorem-prover)

;;;;;; APPLY-SUBSTITUTION searches through TARGET, replacing each variable
;;;;;; symbol with the corresponding value (if any) in A-LIST  (Dana Nau)
;;;(defmacro apply-substitution (target a-list)
;;;  `(if (null ,a-list) ,target
;;;    (real-apply-substitution ,target ,a-list)))
;;;(defun real-apply-substitution (target a-list)
;;;  (cond ((atom target)
;;;         (let ((result (assoc target a-list)))
;;;           (if result (cdr result) target)))
;;;        ((null (cdr target)) (list (real-apply-substitution (car target) a-list)))
;;;        (t (cons (real-apply-substitution (car target) a-list)
;;;                 (real-apply-substitution (cdr target) a-list)))))
;;;
;;;;;; COMPOSE-SUBSTITUTIONS applies SUB2 to the right-hand-side of each item
;;;;;; in SUB1, and appends all items in SUB2 whose left-hand-sides aren't in SUB1.
;;;;;; *** Warning:  COMPOSE-SUBSTITUTIONS destroys the old value of SUB1 ***
;;;;;; I normally would avoid destructive operations, but here it speeds up the
;;;;;; planner by about 5%, and none of the calling functions need SUB1 afterwards
;;;;;; (Dana Nau)
;;;(defun compose-substitutions (sub1 sub2)
;;;  (dolist (pair sub1)
;;;    (setf (cdr pair) (apply-substitution (cdr pair) sub2)))
;;;  (dolist (pair sub2 sub1)
;;;    (pushnew pair sub1 :test #'(lambda (x y) (equal (car x) (car y))))))
;;;
;;;;;; UNIFY is based on the procedure in Nilsson's 1980 book, but modified
;;;;;; to produce an mgu that simultaneously unifies and standardizes the two
;;;;;; expressions.  This improves efficiency by avoiding repeated calls to
;;;;;; STANDARDIZER in FIND-SATISFIERS and APPLY-METHOD.  (Dana Nau)
;;;(defun unify (e1 e2)
;;;  (cond ((atom e1) (unify1 e1 e2))
;;;        ((atom e2) (unify1 e2 e1))
;;;        (t (let ((hsub (unify (car e1) (car e2))))
;;;             (if (eql hsub 'fail)
;;;               'fail
;;;               (let* ((tail1 (apply-substitution (cdr e1) hsub))
;;;                      (tail2 (apply-substitution (cdr e2) hsub))
;;;                      (tsub (unify tail1 tail2)))
;;;                 (if (eql tsub 'fail)
;;;                   'fail
;;;                   (compose-substitutions hsub tsub))))))))
;;;
;;;(defun unify1 (e1 e2)
;;;  (cond ((equal e1 e2) nil)
;;;    ((variablep e1)
;;;      (if (and (occurs e1 e2))
;;;        'fail
;;;        (list (cons e1 e2))))
;;;    ((variablep e2)
;;;      (list (cons e2 e1)))
;;;    (t 'fail)))

;;; OCCURS is the infamous "occurs check" - it returns T if the variable
;;; symbol V occurs anywhere in the expression EXPR (Dana Nau)
;;;(defun occurs (v expr)
;;;  (if (atom expr)
;;;    (eq v expr)
;;;    (or (occurs v (car expr)) (occurs v (cdr expr)))))
;;;
;;;(defun variable-gensym (&optional base-name)
;;;  (let ((sym (if base-name (gensym (string base-name)) (gensym "?"))))
;;;    (setf (get sym 'variable) t)
;;;    sym))

;;; STANDARDIZER returns a substitution that replaces every varible symbol
;;; in EXPRESSION with a new variable symbol not used elsewhere (Dana Nau)
;;;(defun standardizer (expression)
;;;  (mapcar #'(lambda (x) (cons x (variable-gensym))) (extract-variables expression)))
;;;
;;;(defun standardize (expr &optional subs)
;;;  (cond
;;;   ((null expr) (values nil subs))
;;;   ((consp expr)
;;;    (multiple-value-bind 
;;;     (car-expr car-subs)
;;;     (standardize (car expr) subs)
;;;     (multiple-value-bind
;;;      (cdr-expr cdr-subs)
;;;      (standardize (cdr expr) car-subs)
;;;      (values (cons car-expr cdr-expr) cdr-subs))))
;;;   ((variablep expr)
;;;    (let ((bin (assoc expr subs)))
;;;      (if bin
;;;        (values (cdr bin) subs)
;;;        (let ((new-var (variable-gensym expr)))
;;;          (values new-var (cons (cons expr new-var) subs))))))
;;;   (t
;;;    (values expr subs))))

;;; ------------------------------------------------------------------------
;;; Theorem prover
;;; ------------------------------------------------------------------------


;;; This macro is defined before find-satisfiers because it is invoked by
;;;  find-satisfiers and some compilers want all macros to be defined before
;;;  any code which invokes them.
(defmacro seek-satisfiers (goals state bindings level just1
                           &key (domain nil domain-supp-p))
  (let ((d (gensym)))
    `(let ((,d (if ,domain-supp-p ,domain *domain*)))
       (if (null ,goals)
           (list ,bindings)
         (real-seek-satisfiers ,d ,goals ,state
                               ,bindings ,level ,just1)))))

;;; FIND-SATISFIERS returns a list of all satisfiers of GOALS in AXIOMS
(defun find-satisfiers (goals state &optional just-one (level 0)
                        &key (domain *domain*))
  "Find and return a binding list that represents the answer to 
goals \(a list representation of a deductive query\), where 
state provides the database.  level is a non-negative integer indicating the
current depth of inference, used in debugging prints, and 
just-one is a boolean indicating whether you want just one answer (non-nil)
or all answers (nil)."
  (setf *current-state* state)
  (let* ((sought-goals
          (cond
           ((eq (first goals) :first) (rest goals))
           ((eq (first goals) :sort-by)
            (if (= (length goals) 3)
                ;then
                (third goals)
                ;else
                (fourth goals)))
           (t goals)))
         (variables (extract-variables sought-goals))
         (answer (seek-satisfiers sought-goals state variables level
                                  (or (eq (first goals) :first) just-one)
                                  :domain domain))
         (satisfiers (mapcar #'(lambda (bindings) (make-binding-list variables bindings))
                             answer)))
                                        ;(format t "~%sat: ~s~%" satisfiers) ;***

    (if (eq (first goals) :sort-by)
        (sort satisfiers 
              (if (= (length goals) 3) #'<
                (eval (third goals)))
              :key #'(lambda (sat)
                       (eval (apply-substitution (second goals) sat))))
        satisfiers)))


;;; the following comment seems to be out of date --- it describes a
;;; variables list that simply doesn't appear...

;;; REAL-SEEK-SATISFIERS is the workhorse for FIND-SATISFIERS.  For each
;;; proof of GOALS from AXIOMS, it returns the values of GOAL's variables
;;; that make the proof true.  Here are the other parameters:
;;;  - VARIABLES is the list of variables that need to appear in the answer
;;;  - BINDINGS is the variable bindings at the current node of the proof tree
;;;  - LEVEL is the current search depth, for use in debugging printout
;;;  - JUST1 is a flag saying whether to return after finding the 1st satisfier
(defgeneric real-seek-satisfiers (domain goals state bindings
                                         level just1)
  (:method (domain goals state bindings level just1)
     (setq *inferences* (1+ *inferences*))
     (let ((goal1 goals)
               (newlevel (1+ level))
                remaining 
            mgu1 
            answers 
            new-answers
          )
       (when (listp (car goals))
           (setq goal1 (car goals))
           (setq remaining (cdr goals))
       )
       (case (car goal1)
           (not
               ;; we just want to see if (CDR GOAL1) is satisfiable, so last arg is T
               (if (seek-satisfiers (cadr goal1) state nil newlevel t :domain domain)
                   ;then
                   (return-from real-seek-satisfiers nil)
                   ;else
                   (if (seek-satisfiers (list 'maybe (cadr goal1)) state nil newlevel t :domain domain)
                       ;then
                       (return-from real-seek-satisfiers nil)
                       ;else
                       (seek-satisfiers remaining state bindings newlevel just1 :domain domain)
                   )
               )
           )

           (eval
               (if (state-eval state (cadr goal1))
                   ;then
                   (seek-satisfiers remaining state bindings newlevel just1 :domain domain)
                   ;else
                   (return-from real-seek-satisfiers nil)
               )
           )
           
           (neq
               (when (or (not (groundp (second goal1))) (not (groundp (third goal1))))
                   (error "NEQ predicate not fully ground: ~A" goal1)
               )
               (if (not (equalp (state-eval state (second goal1)) (third goal1)))
                   ;then
                   (seek-satisfiers remaining state bindings newlevel just1 :domain domain)
                   ;else
                   (return-from real-seek-satisfiers nil)
               )
           )
           
           (eq
               (if (or (not (groundp (second goal1))) (not (groundp (third goal1))))
                   ;then
                   (let ((resulting-answers nil))
                       (dolist (r (shop2.common::state-candidate-atoms-for-eq state goal1))
                           (unless (eql (setq mgu1 (unify goal1 r)) (shop-fail))
                               (setq new-answers
                                   (seek-satisfiers 
                                       (apply-substitution remaining mgu1)
                                       state
                                       (apply-substitution bindings mgu1) 
                                       (1+ level) just1 :domain domain
                                   )
                               )
                               (setq resulting-answers
                                   (shop-union new-answers resulting-answers 
                                       :test #'equal
                                   )
                               )
                           )
                       )
                       ;(break "Test results.")
                       resulting-answers
                   )
                   ;else
                   (real-seek-satisfiers 
                       domain 
                       (cons (list 'assign (third goal1) (second goal1)) remaining)
                       state bindings level just1
                   )
               )
           )
           (<=
               (if (inequality-compare state goal1 #'< t)
                   ;then
                   (seek-satisfiers remaining state bindings newlevel just1 :domain domain)
                   ;else
                   (return-from real-seek-satisfiers nil)
               )
           )
           (>=
               (if (inequality-compare state goal1 #'> t)
                   ;then
                   (seek-satisfiers remaining state bindings newlevel just1 :domain domain)
                   ;else
                   (return-from real-seek-satisfiers nil)
               )
           )
           (<
               (if (inequality-compare state goal1 #'< nil)
                   ;then
                   (seek-satisfiers remaining state bindings newlevel just1 :domain domain)
                   ;else
                   (return-from real-seek-satisfiers nil)
               )
           )
           (>
               (if (inequality-compare state goal1 #'> nil)
                   ;then
                   (seek-satisfiers remaining state bindings newlevel just1 :domain domain)
                   ;else
                   (return-from real-seek-satisfiers nil)
               )
           )
           
           (=
               (if (state-eval state goal1)
                   ;then
                   (seek-satisfiers remaining state bindings newlevel just1 :domain domain)
                   ;else
                   (return-from real-seek-satisfiers nil)
               )
           )
           
           (call
               (if (state-eval state (cdr goal1))
                   ;then
                   (seek-satisfiers remaining state bindings newlevel just1 :domain domain)
                   ;else
                   (return-from real-seek-satisfiers nil)
               )
           )

           ;; we may want to have a function that can return a LIST of
           ;; possible values for a variable. [2003/11/03:rpg]
           (assign*
              ;; it's possible that the VAR part of (ASSIGN VAR ANS) will already
              ;; be bound by the time the ASSIGN form is examined.  We need to
              ;; check for this important special case. [2003/06/20:rpg]
              (let ((var (cadr goal1))
                    (answers (state-eval state (caddr goal1))))
                  (loop for ans in answers
                      with resulting-answers = nil
                      with new-answers
                      if (or (variablep var)
                          ;; trivial unification --- probably wrong, should be true unification
                          (equalp var ans))
                      do 
                        (setf new-answers
                            (seek-satisfiers
                                (apply-substitution 
                                    remaining
                                    (list (make-binding var ans))
                                )
                                state
                                (apply-substitution 
                                    bindings
                                    (list (make-binding var ans))
                                )
                                newlevel just1 :domain domain
                            )
                        )
                     when new-answers
                     if just1
                     do (return-from real-seek-satisfiers new-answers)
                     else do 
                        (setf resulting-answers
                            (shop-union new-answers resulting-answers :test #'equal)
                        )
                     finally (return resulting-answers)
                  )
              )
           )
           
           (assign
              ;; it's possible that the VAR part of (ASSIGN VAR ANS) will already
              ;; be bound by the time the ASSIGN form is examined.  We need to
              ;; check for this important special case. [2003/06/20:rpg]
              (let ((var (second goal1))
                    (ans (state-eval state (third goal1))))
                  (cond 
                      ((variablep var)
                          (seek-satisfiers 
                              (apply-substitution remaining (list (make-binding var ans)))
                              state 
                              (apply-substitution bindings (list (make-binding var ans)))
                              newlevel just1 :domain domain
                          )
                      )
                      ;; trivial unification --- probably wrong --- should be true unification
                      ((val-close-enough var ans)
                          (seek-satisfiers 
                              (apply-substitution remaining nil)
                              state 
                              (apply-substitution bindings nil)
                              newlevel just1 :domain domain
                          )
                      )
                      ;; otherwise, a constant value for VAR didn't match ANS
                      (t (return-from real-seek-satisfiers nil))
                  )
              )
           )

           (create
              (let ((var (third goal1))
                    (ans (state-eval state (fourth goal1))))
                  (cond 
                      ((variablep var)
                          (add-object-to-state state ans (second goal1))
                          (seek-satisfiers 
                              (apply-substitution remaining (list (make-binding var ans)))
                              state 
                              (apply-substitution bindings (list (make-binding var ans)))
                              newlevel just1 :domain domain
                          )
                      )
                      (t (error "Value for variable ~A in statement ~A already exists!" var goal1))
                  )
              )
           )

           (is
              (let ((var-type (second goal1)) 
                    (var (third goal1))
                   )
                  (if (is-assignable-from state var-type (get-object-type state var))
                      ;then
                      (seek-satisfiers remaining state bindings newlevel just1 :domain domain)
                      ;else
                      (return-from real-seek-satisfiers nil)
                  )
                  
              )
           )

           (imply
               (let ((conditionA (second goal1)) 
                     (conditionB (third goal1))
                    )
                   (if 
                       (or 
                           (not (find-satisfiers conditionA state nil 0 :domain domain))
                           (find-satisfiers conditionB state t 0 :domain domain)
                       )
                       ;then
                       (seek-satisfiers remaining state bindings newlevel just1 :domain domain)
                       ;else
                       (return-from real-seek-satisfiers nil)
                   )
               )
           )

           (or
               (dolist (goal1-sub (cdr goal1))
                 ;; First, look for ways to satisfy GOAL1-sub from the atoms in STATE
                 (when (setq mgu1 (find-satisfiers goal1-sub state nil newlevel :domain domain))
                     (dolist (tempmgu mgu1)
                         (setq new-answers 
                             (seek-satisfiers
                                 (apply-substitution remaining tempmgu)
                                 state (apply-substitution bindings tempmgu)
                                 newlevel just1 :domain domain
                             )
                         )
                         (when new-answers
                             (if 
                                 just1
                                 ;then
                                 (return-from real-seek-satisfiers new-answers)
                             )
                             (setq answers (shop-union new-answers answers :test #'equal))
                         )
                     )
                 )
               )
               (return-from real-seek-satisfiers answers)
           )
      
           (forall
               (let ((bounds (third goal1)) 
                     (conditions (fourth goal1)) 
                     mgu2
                    )
                 (setq mgu2 (find-satisfiers bounds state nil 0 :domain domain))
                 (dolist (m2 mgu2)
                     (unless 
                         (seek-satisfiers 
                             (apply-substitution conditions m2)
                             state bindings 0 t :domain domain
                         )
                         (return-from real-seek-satisfiers nil)
                     )
                 )
               )
               (seek-satisfiers remaining state bindings newlevel just1 :domain domain)
           )

           (exists
               (let ((bounds (third goal1)) 
                     (conditions (fourth goal1)) 
                      mgu2
                    )
                 (setq mgu2 (find-satisfiers bounds state nil 0 :domain domain))
                 (loop for m2 in mgu2
                     when 
                        (seek-satisfiers 
                            (apply-substitution conditions m2)
                            state bindings 0 t :domain domain
                        )
                     return t                
                     ;short-circuit, and move on to remaining goals... [2007/07/15:rpg]
                     finally ;; didn't find any value that worked
                        (return-from real-seek-satisfiers nil)
                 )
               )
               (seek-satisfiers remaining state bindings newlevel just1 :domain domain)
           )

           (setof ;; (setof ?var expr ?outvar)
               (destructuring-bind 
                   (keyword var bounds outvar)
                   goal1
                   (declare (ignore keyword))
                   (let ((raw-results 
                             (find-satisfiers 
                                 (list bounds) state
                                 ;; no bindings should be necessary because of 
                                 ;; the substitution into the goal...
                                 nil 0 :domain domain
                             )
                         )
                        )
                       (unless raw-results (return-from real-seek-satisfiers nil))
                       (let ((new-binding
                                 (make-binding outvar
                                     (remove-duplicates 
                                         (loop for binding-list in raw-results
                                             collect (binding-list-value var binding-list)
                                         )
                                         :test 'equal
                                     )
                                 )
                             )
                            )
                           (seek-satisfiers 
                               (apply-substitution remaining (list new-binding))
                               state 
                               (apply-substitution bindings (list new-binding))
                               newlevel just1 :domain domain
                           )
                       )
                   )
               )
           )
           
           (t
             ;; Handling and
             (if 
                 (eq (car goal1) 'and)
                 ;then
                 (setq goal1 (cdr goal1))
             )
             ;; Handling a list of goals as a conjunction of 
             ;; (not necessarily atomic) goals
             (if 
                 (or (listp (car goal1)) (eq (car goal1) :external))
                 (progn
                     (when 
                         (setq mgu1 
                             (if (eq (car goal1) :external)
                                 (or
                                     (external-find-satisfiers (cdr goal1) state)
                                     (find-satisfiers (cdr goal1) state nil newlevel :domain domain)
                                 )
                                 (find-satisfiers goal1 state nil newlevel :domain domain)
                             )
                         )
                         (dolist (tempmgu mgu1)
                             (setq new-answers 
                                 (seek-satisfiers
                                     (apply-substitution remaining tempmgu)
                                     state (apply-substitution bindings tempmgu)
                                     newlevel just1 :domain domain
                                 )
                             )
                             (when new-answers
                                 (if just1
                                     (return-from real-seek-satisfiers new-answers)
                                 )
                                 (setq answers 
                                     (shop-union 
                                         new-answers answers 
                                         :test #'equal
                                     )
                                 )
                             )
                         )
                     )
                     (return-from real-seek-satisfiers answers)
                 )
                 ;; If goal1 is an atomic predicate, try to satisfy it.
                 (do-conjunct domain goal1 remaining state bindings level just1)
             )
           )
       )
     )
  )
)

(defun inequality-compare (state stmt fn equals-allowed &aux val1 val2)
   (setq val1 (state-eval state (second stmt)))
   (setq val2 (state-eval state (third stmt)))
   (when (or (not (numberp val1)) (not (numberp val2)))
       (return-from inequality-compare nil)
   )
   (or (funcall fn val1 val2) (and equals-allowed (val-close-enough val1 val2)))
)

(defun val-close-enough (val1 val2)
    (cond 
        ((and (numberp val1) (numberp val2))
                (within val1 val2 *rounding-precision-constant*)
        )
        ((and (listp val1) (listp val2))
            (every #'val-close-enough val1 val2)
        )
        (t (return-from val-close-enough (eq val1 val2)))
    )
)

(defun within (real1 real2 percent)
    (or (<= (abs (- real1 real2)) (* percent (abs real1)))
        (< (- percent) real1 0 real2 percent)
        (< (- percent) real2 0 real1 percent)
    )
)

; An alternate version that wasn't used:
;(defun external-find-satisfiers (goal state)
;  (format t "EFS: ~s~%" goal)
;  (or (when *external-access* 
;	(let ((sats (external-query goal state)))
;	  (nconc *attribution-list* 
;		 (mapcar #'(lambda (sat)
;			     (list (apply-substitution goal sat)
;				   ATTRIB))
;			 sats))
;	  sats))
;      (find-satisfiers goal state nil 0 :domain domain)))

(defun external-find-satisfiers (goal state)
;  (format t "EFS: ~s~%" goal)
  (if *external-access*
      (external-query goal state)
    nil))

;;; Goal1 is guaranteed to be an atomic predicate
(defun do-conjunct (domain goal1 remaining state bindings level just1)
  (let (mgu1 answers new-answers found-match new-just1)
    (trace-print :goals (car goal1) state
                 "~2%Level ~s, trying to satisfy goal ~s"
                 level
                 goal1)

    ;; Then, look for ways to satisfy GOAL1 from the atoms in state
    (dolist (r (state-candidate-atoms-for-goal state goal1))
      (unless (eql (setq mgu1 (unify goal1 r)) (shop-fail))
        (setq found-match t) ; for debugging printout
        (setq new-answers (seek-satisfiers
                           (apply-substitution remaining mgu1)
                           state
                           (apply-substitution bindings mgu1) 
                           (1+ level) just1 :domain domain))
        (if new-answers 
          (progn
            (trace-print :goals (car goal1) state
                         "~2%Level ~s, state satisfies goal ~s~%satisfiers ~s"
                         level
                         goal1
                         new-answers)
            (if just1
              (return-from do-conjunct new-answers))
            (setq answers (shop-union new-answers answers :test #'equal))))))
    ;; Next, look for ways to prove GOAL1 from the *axioms*
    (dolist (r (gethash (car goal1) (domain-axioms domain)))
      (let ((standardized-r (standardize r)))
        (unless (eql (setq mgu1 (unify goal1 (second standardized-r)))
                     (shop-fail))
          (setq found-match t) ; for debugging printout
          ;; found an axiom which unifies, now look at branches of the tail
          (let ((tail (cddr standardized-r)))
            (do ((ax-branch-name (car tail) (car tail))
                 (ax-branch (cadr tail) (cadr tail)))
              ((null tail)  nil)
              (trace-print :goals (car goal1) state
                           "~2%Level ~s, axiom matches goal ~s~%     axiom ~s~%satisfiers ~s"
                           level
                           goal1
                           ax-branch-name
                           mgu1)
              (trace-print :axioms ax-branch-name state
                   "~2%Level ~s, trying axiom ~s~%      goal ~s~%      tail ~s"
                     level
                     ax-branch-name
                     goal1
                     (apply-substitution ax-branch mgu1))
              (if (eq (car ax-branch) :first)
                (setq new-just1 t ax-branch (cdr ax-branch))
                (setq new-just1 just1))
              (setq new-answers (seek-satisfiers
                                  (apply-substitution 
                                    (append (list ax-branch) remaining) mgu1)
                                  state
                                  (apply-substitution bindings mgu1)
                                 (1+ level) new-just1 :domain domain))
              (if new-answers
                (progn
                  (trace-print :axioms ax-branch-name state
                               "~2%Level ~s, applying axiom ~s~%      goal ~s~%      tail ~s"
                               level
                               ax-branch-name
                               goal1
                               (apply-substitution ax-branch mgu1))
                  (if new-just1 
                    (return-from do-conjunct new-answers))
                  (setq answers (shop-union new-answers answers :test #'equal))
                  (return nil))
                (progn
                  (trace-print :axioms ax-branch-name state
                               "~2%Level ~s, exiting axiom ~s~%      goal ~s~%      tail ~s"
                               level
                               ax-branch-name
                               goal1
                               (apply-substitution ax-branch mgu1))))
              (setf tail (cddr tail)))))))
    (unless found-match
      (trace-print :goals (car goal1) state
                   "~2%Level ~s, couldn't match goal ~s"
                   level
                   goal1))
    (return-from do-conjunct answers)))



;;; ------------------------------------------------------------------------
;;; Explanation of satisifiers
;;; ------------------------------------------------------------------------

; The following code is invoked only if the :explanation keyword for
;  the planner has been given a true value.

; Given a goal unified by a valid satisfier, construct an assertion
;  that explains how the unifier satisfied that goal and an
;  attribution for the explanation (if it exists).  For example, if
;  the original goal were:
;    '(and (or (and (on ?a ?b) (on ?a ?c)) (on ?d ?a)) (on ?b ?c))
;  and a unifier ((?a.x1) (?b.x2) (?c.x3)) then the explanation would be:
;    '(and (and (on x1 x2) (on x1 x3)) (on x2 x3))
;
; If any term or conjunct in the explanation comes from an external
;  query (see the external-query routine), a list containing the term
;  :source followed by attribution information is added to the
;  beginning of the term or conjunct.  In the above example, if the
;  attribution for (and (on x1 x2) (on x1 x3)) were (PoliceReport
;  UID1234), and the attribution for (on x2 x3) were (PoliceReport
;  UID4321) then the explanation would be
;    '(and ((:source PoliceReport UID1234) and (on x1 x2) (on x1 x3))
;          ((:source PoliceReport UID4321) on x2 x3))
(defun explain-satisfier (unified-goal state &optional external
                                       &key (domain nil domain-supp-p))
  (unless domain-supp-p (setf domain *domain*))
  (let ((*external-access* nil)) ; otherwise we'd query twice
    (cond
     ((member (first unified-goal)
              '(sort-by not eval call assign imply forall))
     ; The above constructs are not handled by the explanation code yet.
      nil)
     ((eq (first unified-goal) :external)
      (explain-satisfier (rest unified-goal) state t :domain domain))
     ((eq (first unified-goal) 'or)
      (cond
       ((null (rest unified-goal)) nil)
       ((not (find-satisfiers (second unified-goal) state nil 0 :domain domain))
        (explain-satisfier (cons 'or (rest (rest unified-goal)))
                           state external :domain domain))
       (t
        (explain-satisfier (second unified-goal) state external :domain domain))))

     ((eq (first unified-goal) 'and)
      (let* ((explanation-list
              (mapcar #'(lambda (g) (explain-satisfier g state external :domain domain)) 
                      (rest unified-goal)))
             (simplified-explanation-list (remove nil explanation-list))
             (explanation
              (when (find-satisfiers unified-goal state nil 0 :domain domain)
                (cons 'and simplified-explanation-list))))
        (when explanation
          (add-source unified-goal external explanation))))

     ((listp (first unified-goal)) ; implicit and
      (explain-satisfier (cons 'and unified-goal) state external :domain domain))
     (t ; logical-atom
      (add-source unified-goal external unified-goal)))))

(defun add-source (unified-goal external explanation)
  (if external
      (let ((source (get-attribution unified-goal)))
        (if source
            (cons (list :source source) explanation)
          explanation))
    explanation))
  
(defun get-attribution (unified-query)
;(format t "~%Source access: ~a from~%   ~a" unified-query *attribution-list*)
  (second (assoc unified-query *attribution-list* 
                 :test #'(lambda (q att)
                           (or (equal q att)
                               (equal (list 'and q) att))))))

(defun fully-instantiated-goal (goal)
  (if (atom goal)
      (not (variablep goal))
    (and (fully-instantiated-goal (first goal))
         (fully-instantiated-goal (rest goal)))))

;;; ------------------------------------------------------------------------
;;; External access to state information
;;; ------------------------------------------------------------------------

; The following code is invoked only if an external-access-hook routine
;   has been defined.

; If external-query receives a query of the form (<pred> <val> <val>)
;   or (and (<pred> <val> <val>)+), it sends that query to
;   external-access-hook.  If the query succeeds, the resulting
;   information is added to the state and the attribution information
;   is stored in *attribution-list*.
;  If external-query receives a complex query involving and's and
;   or's, it decomposes that into queries it can send to
;   external-access-hook.  If external-query encounters other logical
;   constructs (e.g., not, imply), external-query returns nil (but
;   any responses it had already received are still kept in the
;   state and *attribution-list*.
(defun  external-query (query state)
;  (format t "~%potential query: ~s" query)
  (cond
   ((null query) nil)
   ((member (first query) '(not eval call assign assign* imply forall sort-by))
    nil)
   ((listp (first query)) ; implicit and
    (external-query (cons 'and query) state))
   ((eq (first query) 'or)
    (or (external-query (second query) state)
        (when (rest (rest query))
          (external-query `(or ,(rest (rest query))) state))))
   ((and (eq (first query) 'and) (rest query))
    (if (find-if-not #'(lambda (subquery) 
                         (and (listp subquery)
                              (= (length subquery) 3)
                              (not (logical-keywordp (first subquery)))
                              (not (find-if-not #'atom subquery))))
                     (rest query))
        ; complex query - try to decompose
        (let ((first-response (external-query (second query) state)))
          (when first-response
            (let ((rest-response (external-query `(and ,@(rest (rest query)))
                                                 state)))
              (when rest-response
                (merge-binding-set-lists first-response rest-response)))))
      ; simple query - invoke external access
      (invoke-external-query query state)))
   (t ; query of a single logical atom
    (invoke-external-query (list 'and query) state))))

(defun logical-keywordp (sym)
  (member sym '(and or not eval call neq assign assign* imply forall sort-by maybe)))

; Takes two lists of sets of bindings and returns a list of sets of
;  bindings consisting of all consistent combinations of one set from
;  each list.  E.g.:
;
; (merge-binding-set-lists '(((?x 1) (?y 2)) ((?x 3) (?z 4)))
;                          '(((?x 1) (?a 5)) ((?b 6) (?c 7))))
; =>
; '(((?x 1) (?y 2) (?a 5)) ((?x 1) (?y 2) (?b 6) (?c 7))
;   ((?x 3) (?z 4) (?b 6) (?c 7)))
;
; Note that the second set in the first list and the first set in the
;  second list are not merged in the result because they have
;  incompatible bindings for ?x.
(defun merge-binding-set-lists (binding-sets1 binding-sets2
                                              &optional original-binding-sets2)
  (cond
   ((null binding-sets1) nil)
   ((null binding-sets2)
    (when original-binding-sets2
      (merge-binding-set-lists (rest binding-sets1) 
                               original-binding-sets2 original-binding-sets2)))
   (t
    (let* ((original-sets2 (or original-binding-sets2 binding-sets2))
           (first-merge 
            (merge-binding-sets (first binding-sets1) (first binding-sets2)))
           (rest-merge 
            (merge-binding-set-lists binding-sets1 (rest binding-sets2) 
                                     original-sets2)))
      (if first-merge
          (cons first-merge rest-merge)
        rest-merge)))))

(defun merge-binding-sets (binding-set1 binding-set2)
  (append
   binding-set1
   (remove-if
    #'(lambda (binding2)
        (find-if #'(lambda (binding1)
                     (cond
                      ((equal binding1 binding2) t)
                      ((eq (first binding1) (first binding2))
                       (return-from merge-binding-sets nil))
                      (t nil)))
                 binding-set1))
    binding-set2)))

; Directly invokes external-access-hook on a query of the form
;  (and (<pred> <val> <val>)+) and returns the resulting bindings,
;  stores the attribution information in *attribution-list*, and
;  stores the resulting facts in the state [NOTE: adding an atom to
;  the state here has wierd implications for backtracking, etc.; may
;  want to reconsider.]
(defun invoke-external-query (query state)
  (mapcar
   #'(lambda (attributed-binding-set)
       (let* ((attribution (first attributed-binding-set))
              (binding-set (fix-uninterned-bindings
                            (mapcar #'(lambda (l)
                                        (cons (first l) (second l)))
                                    (second attributed-binding-set))
                            (extract-variables query)))
              (unified-query (apply-substitution query binding-set)))
         (setf *attribution-list* (cons (list unified-query attribution)
                                        *attribution-list*))
         (dolist (fact (rest unified-query))
           (shop2.common::add-atom-to-state fact state nil nil))
         binding-set))
     (funcall (fdefinition 'external-access-hook) query)))

; Sample behavior of an external-access-hook:
;  Takes a goal as input, returns bindings and attribution.
(defun DUMMY-external-access-hook (goal)
  (format t "~%external query: ~s" goal)
  (let ((dummy-goal '(and
                     (employees ?business UID101)
                     (hasMembers UID101 ?business)))
        (dummy-bindings `((,(second (second goal)) UID102)))
        (dummy-attribution '(UID103 BusinessNews)))
    (when (goal-equalp goal dummy-goal)
      (list (list dummy-attribution dummy-bindings)))))

(defun goal-equalp (g1 g2)
  (cond
   ((eq g1 g2) t)
   ((and (variablep g1) (variablep g2)) t)
   ((and (listp g1) (listp g2) 
         (= (length g1) (length g2)) 
         (goal-equalp (first g1) (first g2))
         (goal-equalp (rest g1) (rest g2)))
    t)
   (t nil)))

