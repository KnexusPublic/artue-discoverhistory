;;; The Original Code is SHOP2.  
;;; 
;;; The Initial Developer of the Original Code is the University of
;;; Maryland. Portions created by the Initial Developer are Copyright (C)
;;; 2002,2003 the Initial Developer. All Rights Reserved.
;;;
;;; Additional developments made by Robert P. Goldman, John Maraist.
;;; Portions created by Drs. Goldman and Maraist are Copyright (C)
;;; 2004-2007 SIFT, LLC.

;;; Smart Information Flow Technologies Copyright 2006-2007 Unpublished work
;;; 
;;; GOVERNMENT PURPOSE RIGHTS
;;; 
;;; Contract No.         FA8650-06-C-7606, 
;;; Contractor Name      Smart Information Flow Technologies, LLC
;;;                      d/b/a SIFT, LLC
;;; Contractor Address   211 N 1st Street, Suite 300
;;;                      Minneapolis, MN 55401
;;; Expiration Date      5/2/2011
;;; 
;;; The Government's rights to use, modify, reproduce, release,
;;; perform, display, or disclose this software are restricted by
;;; paragraph (b)(2) of the Rights in Noncommercial Computer Software
;;; and Noncommercial Computer Software Documentation clause contained
;;; in the above identified contract. No restrictions apply after the
;;; expiration date shown above. Any reproduction of the software or
;;; portions thereof marked with this legend must also reproduce the
;;; markings.


 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE. 
 
 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; (at your option) any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :shop2.common)



; Functions (other than constructors) that are called from shop2.lisp:

; state-atoms (from trace-print and print-current-state)
; state-all-atoms-for-predicate (from query-current-state)
; state-candidate-atoms-for-goal (from do-conjuct)
; copy-state (from store-plan!)
; tag-state (from apply-operator)
; retract-state-changes (from apply-operator and seek-plans-primitive)
; add-atom-to-state (from invoke-external-query and apply-operator)
; delete-atom-from-state (from apply-operator)

; I hope the preceding list is useful for clarifying the interface that states
; should present to the outside world.

;;; higher level operations on the state objects:
;;; find-satisfiers (in SHOP2) uses the above to answer queries on states

;;;

; The "state" class

(defstruct (state (:constructor nil) (:copier nil))
  body)

(defun make-state (atoms &optional (state-encoding *state-encoding*))
  (ecase state-encoding
    (:list (make-list-state atoms))
    (:mixed (make-mixed-state atoms))
    (:hash (make-hash-state atoms))
    (:bit (make-bit-state atoms))))

; The following generic functions should be implemented by every state

; Insert the atom into the state. The return value is undefined.
; This function destructively modifies its state argument.
(defgeneric insert-atom (atom state)
  (:documentation
   "Insert the atom into the state.  The return value is
   undefined. This function destructively modifies its state
   argument.
   Note that an atom is NOT a lisp atom --- it is actually a list of
   pred . args representing a first order logic positive literal."
   ))

; Remove the atom from the state. The return value is undefined.
; This function destructively modifies its state argument.
(defgeneric remove-atom (atom state)
  (:documentation
   "Delete the atom from the statebody.  The return value is
   undefined.  This function destructively modifies its state
   argument.
      Note that an atom is NOT a lisp atom --- it is actually a list of
   pred . args representing a first order logic positive literal."))

; Returns the atoms of the state as a list.
(defgeneric state-atoms (state)
  (:documentation "Return the atoms of the state in a plain list"))

; Returns nil iff the atom is not in the state.
(defgeneric atom-in-state-p (atom state)
  (:documentation "Is the atom in the state?"))

(defgeneric state-all-atoms-for-predicate (state pred))

(defgeneric state-candidate-atoms-for-goal (state goal))

; Returns a copy of the state.
(defgeneric copy-state (state)
  (:documentation "Return a copy of the state"))

(defgeneric state-eval (state form)
  (:documentation "Evaluate form with state environment"))

(defgeneric set-state-value (st fun-name fun-params new-val &key &allow-other-keys)
  (:documentation "Change value of function in state"))

(defgeneric get-state-value (call st)
  (:documentation "Find value of function in state"))

(defgeneric add-object-to-state (st obj-name obj-type &key &allow-other-keys)
  (:documentation "Adding a new object"))

(defgeneric remove-object-from-state (st obj-name &key &allow-other-keys)
  (:documentation "Removing an object (backtracking only)"))

(defgeneric tag-state (state)
  (:documentation "Add a tag to a state; used to make tagged-states, which 
provide information about how to backtrack over state updates.")
  )

(defgeneric include-in-tag (action atom tag)
  (:documentation "Add to the TAG a state update \(characterized by ACTION\)
performed with ATOM \(a literal\) as operand."))

(defgeneric retract-state-changes (state tag)
  )

(defgeneric add-atom-to-state (atom state depth operator)
  )

(defgeneric delete-atom-from-state (atom state depth operator)
  )

(defgeneric state-trajectory (state)
  (:documentation "Any state in SHOP implicitly defines a trajectory ---
the sequence of states, starting from the initial state, and terminating at
this state.  This function returns a trajectory leading to STATE.")
  )


; Unlike for MIXED, HASH, and BIT encodings, LIST-insert-atom-into-statebody and
; LIST-remove-atom-from-statebody are recursive, requiring their arguments to be
; statebodies and not states. So until we redo the way these functions work,
; they have to stay.
(defun LIST-insert-atom-into-statebody (atom statebody)
  (cond 
   ((null statebody)
    (list (list (car atom) atom)))
   ((string< (car atom) (caar statebody))
    (cons (list (car atom) atom) statebody))
   ((eq (car atom) (caar statebody))
    (cons
     (cons (caar statebody)
           (if (member atom (cdar statebody) :test #'equal)
               (cdar statebody)
               (cons atom (cdar statebody))))
     (cdr statebody)))
   (t (cons (car statebody)
    (LIST-insert-atom-into-statebody atom (cdr statebody))))))

(defun LIST-remove-atom-from-statebody (atom statebody)
  (cond ((null statebody) nil)
        ((string< (car atom) (caar statebody)) statebody)
        ((eq (car atom) (caar statebody))
         (let ((newval (remove atom (cdar statebody) :test #'equal)))
           (if newval
               (cons (cons (car atom) newval) (cdr statebody))
               ;; if there are no remaining propositions for this
               ;; predicate, we just drop the entry
               ;; altogether. [2006/08/02:rpg]
               (cdr statebody))))
        (t (cons (car statebody)
                 (LIST-remove-atom-from-statebody atom (cdr statebody))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; The "tagged-state" class

;;; Tags-info is a list of tag-info entries.  Each tag-info is a list whose
;;; first element is a tag (represented by an integer) and whose remaining
;;; elements are a list of changes made to the state while that tag was active.
;;; The command tag-state activates a new tag and returns it.  The command
;;; retract-state-changes retracts all changes which were made while the given
;;; tag was active.  It is expected that retractions will typically involve the
;;; most recently added tag, but the system does allow older tags to be
;;; retracted instead.

(defstruct (tagged-state (:include state) (:constructor nil) (:copier nil))
  (tags-info (list (list 0))))

(deftype action-type () '(member add delete))

(defstruct state-update
  (action 'add :type action-type)
  (literal nil :type list)
  (provenance nil)
)

(defmethod state-eval ((st tagged-state) form)
    (eval form)
)

(defmethod tag-state ((st tagged-state))
  (let ((new-tag (1+ (first (first (tagged-state-tags-info st))))))
    (push (list new-tag) (tagged-state-tags-info st))
    new-tag))

(defmethod include-in-tag (action atom (st tagged-state))
  (unless (typep action 'action-type)
    (error "Unacceptable action ~S" action))
  (push (make-state-update :action action :literal atom :provenance *cur-head*)
        (rest (first (tagged-state-tags-info st)))))

(defmethod retract-state-changes ((st tagged-state) tag)
  (multiple-value-bind (new-tags-info changes)
      (pull-tag-info (tagged-state-tags-info st) tag)
    (setf (tagged-state-tags-info st) new-tags-info)
    (dolist (change changes)
      (ecase (state-update-action change)
        (add
         (remove-atom (state-update-literal change) st))
        (delete
         (insert-atom (state-update-literal change) st))
        (set-value
         (error "set-value action not supported by tagged-state."))))))

(defmethod add-atom-to-state (atom (st tagged-state) depth operator)
;;;  (let ((shop2::state st))
;;;    ;; the above binding makes the trace-print work properly --- it references state [2006/12/06:rpg]
    (trace-print :effects (car atom) st
                 "~2%Depth ~s, adding atom to current state~%      atom ~s~%  operator ~s"
                 depth atom operator)
;;;  )
  (unless (atom-in-state-p atom st)
;    (unless (member (cons 'delete atom) (first (first st)) :test #'equal)
    (include-in-tag 'add atom st)
    (insert-atom atom st)))

(defmethod delete-atom-from-state (atom (st tagged-state) depth operator)
;;;  (let ((shop2::state st))
;;;    ;; the above binding makes the trace-print work properly --- it references state [2006/12/06:rpg]
    (trace-print :effects (car atom) st
                 "~2%Depth ~s, deleting atom from current state~%      atom ~s~%  operator ~s"
                 depth atom operator)
;;;    )
  (when (atom-in-state-p atom st)
    (include-in-tag 'delete atom st)
    (remove-atom atom st)))

(defun pull-tag-info (tags-info tag)
  (if (null tags-info)
      (error "Attempt to retract to nonexistent state")
    (let ((first-info (first tags-info)))
      (if (= tag (first first-info))
        ;then
        (values (rest tags-info) (rest first-info))
        ;else
        (multiple-value-bind
            ;variables
            (rest-info rest-changes)
            ;value call
            (pull-tag-info (rest tags-info) tag)
            ;forms
            (values rest-info (append (rest first-info) rest-changes))
        )
      )
    )
  )
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; The "list-state" class

(defstruct (list-state (:include tagged-state)
                       (:constructor makeliststate)
                       (:copier nil)))

(defun make-list-state (atoms)
  (let ((st (makeliststate)))
    (setf (state-body st) nil)
    (dolist (atom atoms) (insert-atom atom st))
    st))

(defmethod insert-atom (atom (st list-state))
  (setf (state-body st) (LIST-insert-atom-into-statebody atom (state-body st))))

(defmethod remove-atom (atom (st list-state))
  (setf (state-body st) (LIST-remove-atom-from-statebody atom (state-body st))))

(defmethod state-atoms ((st list-state))
  (mapcan #'(lambda (entry) (copy-list (cdr entry))) (state-body st)))

(defmethod atom-in-state-p (atom (st list-state))
  (member atom (rest (assoc (first atom) (state-body st))) :test #'equal))

(defmethod state-all-atoms-for-predicate ((st list-state) pred)
  (rest (assoc pred (state-body st))))

(defmethod state-candidate-atoms-for-goal ((st list-state) goal)
  (state-all-atoms-for-predicate st (first goal)))

(defmethod copy-state ((st list-state))
  (let ((the-copy (make-list-state nil)))
    (setf (state-body the-copy) (copy-tree (state-body st)))
    (setf (tagged-state-tags-info the-copy)
          (copy-tree (tagged-state-tags-info st)))
    the-copy))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; The "hash-state" class

(defstruct (hash-state (:include tagged-state)
                       (:constructor makehashstate)
                       (:copier nil)))

(defun make-hash-state (atoms)
  (let ((st (makehashstate)))
    (setf (state-body st) (make-hash-table :test #'equal))
    (dolist (atom atoms) (insert-atom atom st))
    st))

(defmethod insert-atom (atom (st hash-state))
  (setf (gethash atom (state-body st)) t))

(defmethod remove-atom (atom (st hash-state))
  (remhash atom (state-body st)))

(defmethod state-atoms ((st hash-state))
  (let ((statebody (state-body st))
        (acc nil))
    (maphash #'(lambda (key val)
                 (declare (ignore val)) (setf acc (cons key acc)))
             statebody)
    acc))

(defmethod atom-in-state-p (atom (st hash-state))
  (gethash atom (state-body st)))

(defmethod state-all-atoms-for-predicate ((st hash-state) pred)
  (remove-if-not
   #'(lambda (atom)
       (eq (first atom) pred))
   (state-atoms st)))

(defmethod state-candidate-atoms-for-goal ((st hash-state) goal)
  (cond
   ((find-if-not #'(lambda (term) (and (atom term) (not (variablep term))))
                 (rest goal))
    (state-all-atoms-for-predicate st (first goal)))
   ((atom-in-state-p goal st)
    (list goal))
   (t nil)))

(defmethod copy-state ((st hash-state))
  (let ((the-copy (make-hash-state nil)))
    (setf (state-body the-copy) (copy-hash-table (state-body st)))
    (setf (tagged-state-tags-info the-copy)
          (copy-tree (tagged-state-tags-info st)))
    the-copy))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; The "mixed-state" class

(defstruct (mixed-state (:include tagged-state)
                        (:constructor makemixedstate)
                        (:copier nil)))

(defun make-mixed-state (atoms)
  (let ((st (makemixedstate)))
    (setf (state-body st) (make-hash-table :test #'eq))
    (dolist (atom atoms) (insert-atom atom st))
    st))

(defmethod insert-atom (atom (st mixed-state))
  (push (rest atom) (gethash (first atom) (state-body st))))

(defmethod remove-atom (atom (st mixed-state))
  (let ((statebody (state-body st)))
    (setf
     (gethash (first atom) statebody)
     (delete
      (rest atom)
      (gethash (first atom) statebody)
      :test #'equal))))

(defmethod state-atoms ((st mixed-state))
  (let ((statebody (state-body st)))
    (let ((acc nil)) 
      (maphash #'(lambda (pred lis)
                   (setf acc
                         (append (mapcar #'(lambda (entry) (cons pred entry)) lis)
                                 acc)))
               statebody)
      acc)))

(defmethod atom-in-state-p (atom (st mixed-state))
  (member (rest atom) (gethash (first atom) (state-body st)) :test #'equalp))

(defmethod state-all-atoms-for-predicate ((st mixed-state) pred)
  (let ((lis (gethash pred (state-body st))))
    (mapcar #'(lambda (entry) (cons pred entry)) lis)))

(defmethod state-candidate-atoms-for-goal ((st mixed-state) goal)
  (cond
   ((find-if-not #'(lambda (term)
                     (and (atom term) (not (variablep term))))
                 (rest goal))
    (state-all-atoms-for-predicate st (first goal)))
   ((atom-in-state-p goal st) (list goal))
   (t nil)))

(defmethod copy-state ((st mixed-state))
  (let ((the-copy (make-mixed-state nil)))
    (setf (state-body the-copy) (copy-hash-table (state-body st)))
    (setf (tagged-state-tags-info the-copy)
          (copy-tree (tagged-state-tags-info st)))
    the-copy))

; If we don't trust that copy-hash-table copies a mixed-state correctly, we can
; replace the preceding function with:
; (defmethod copy-state ((st mixed-state))
;   (let ((the-copy (make-mixed-state (state-atoms st))))
;     (setf (tagged-state-tags-info the-copy)
;           (copy-tree (tagged-state-tags-info st)))
;     the-copy))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; The "bit-state" class

(defstruct (bit-state (:include tagged-state)
                      (:constructor makebitstate)
                      (:copier nil)))

(defun make-bit-state (atoms)
  (let ((st (makebitstate)))
    ;; The previous version of shop2.lisp did some strange initialization work
    ;; when making a new :bit statebody which I didn't understand.
    ;; This doesn't do that. It seems to me like this just makes bit-states into
    ;; list-states that carry around some useless empty hash tables. That is, I
    ;; don't think the hash tables in the statebody do anything in this
    ;; implementation.
    (setf (state-body st)
          (list (make-hash-table :test #'eq)
                (make-hash-table :test #'equal)
                (make-hash-table :test #'eq)
                nil))
    (dolist (atom atoms) (insert-atom atom st))
    st))

(defmethod insert-atom (atom (st bit-state))
  (let* ((statebody (state-body st))
         (pred-table (first statebody))
         (entity-table (second statebody))
         (extras (fourth statebody))
         (entities (rest atom))
         (types (mapcar #'(lambda (entity)
                            (first (gethash entity entity-table)))
                        entities))
         (entity-numbers (mapcar #'(lambda (entity)
                                     (second (gethash entity entity-table)))
                                 entities))
         (pred-entry (gethash (first atom) pred-table))
         (pred-types (first pred-entry))
         (pred-array (third pred-entry)))

    (if (and entities (equal types pred-types))
        (setf (apply #'aref pred-array entity-numbers) 1)
      (setf (fourth statebody)
            (LIST-insert-atom-into-statebody atom extras)))))

(defmethod remove-atom (atom (st bit-state))
  (let* ((statebody (state-body st))
         (pred-table (first statebody))
         (entity-table (second statebody))
         (extras (fourth statebody))
         (entities (rest atom))
         (types (mapcar #'(lambda (entity)
                            (first (gethash entity entity-table)))
                        entities))
         (entity-numbers (mapcar #'(lambda (entity)
                                     (second (gethash entity entity-table)))
                                 entities))
         (pred-entry (gethash (first atom) pred-table))
         (pred-types (first pred-entry))
         (pred-array (third pred-entry)))

    (if (and entities (equal types pred-types))
        (setf (apply #'aref pred-array entity-numbers) 0)
      (setf (fourth statebody)
            (LIST-remove-atom-from-statebody atom extras)))))

(defmethod state-atoms ((st bit-state))
  (let ((acc nil))
    (maphash #'(lambda (pred lis)
                 (declare (ignore lis))
                 (setf acc
                       (append
                        (state-all-atoms-for-predicate st pred)
                       acc)))
             (first (state-body st)))
    (remove-duplicates (append
                        acc 
                        (mapcan #'(lambda (entry) (copy-list (cdr entry)))
                                (fourth (state-body st)))))))

(defmethod atom-in-state-p (atom (st bit-state))
  (let* ((statebody (state-body st))
         (pred-table (first statebody))
         (entity-table (second statebody))
         (extras (fourth statebody))
         (entities (rest atom))
         (types (mapcar #'(lambda (entity)
                            (first (gethash entity entity-table)))
                        entities))
         (entity-numbers (mapcar #'(lambda (entity)
                                     (second (gethash entity entity-table)))
                                 entities))
         (pred-entry (gethash (first atom) pred-table))
         (pred-types (first pred-entry))
         (pred-array (third pred-entry)))

    (if (and entities (equal types pred-types))
        (= (apply #'aref pred-array entity-numbers) 1)
      (member atom (rest (assoc (first atom) extras)) :test #'equal))))

(defmethod state-all-atoms-for-predicate ((st bit-state) pred)
  (let* ((statebody (state-body st))
         (pred-table (first statebody))
         (type-table (third statebody))
         (extras (fourth statebody))
         (pred-entry (gethash pred pred-table))
         (pred-types (first pred-entry))
         (pred-type-counts (second pred-entry))
         (pred-array (third pred-entry)))

    (append
     (when pred-entry
       (mapcar #'(lambda (entities)
                   (cons pred entities))
               (BIT-statebody-search-array
                pred-array pred-type-counts
                (mapcar #'(lambda (type-name)
                            (second (gethash type-name type-table)))
                        pred-types)
                (mapcar #'(lambda (x) (declare (ignore x)) (list :variable 0))
                        pred-types))))
     (rest (assoc pred extras)))))

(defmethod state-candidate-atoms-for-goal ((st bit-state) goal)
  (let* ((statebody (state-body st))
         (pred-table (first statebody))
         (entity-table (second statebody))
         (type-table (third statebody))
         (extras (fourth statebody))
         (pred (first goal))
         (goal-terms (rest goal))
         (pred-entry (gethash pred pred-table))
         (pred-types (first pred-entry))
         (pred-type-counts (second pred-entry))
         (pred-array (third pred-entry)))

    (append
     (when (and pred-entry
                (= (length goal-terms) (length pred-types)))
       (let ((initial-counter
              (mapcar #'(lambda (entity pred-type)
                          (if (variablep entity)
                              (list :variable 0)
                            (let ((entry (gethash entity entity-table)))
                              (if (eq (first entry) pred-type)
                                  (list :fixed (second entry))
                                nil))))
                      goal-terms pred-types)))

         (unless (member nil initial-counter)
           (mapcar #'(lambda (entities)
                       (cons pred entities))
                   (BIT-statebody-search-array
                    pred-array pred-type-counts
                    (mapcar #'(lambda (type-name)
                                (second (gethash type-name type-table)))
                            pred-types)
                    initial-counter)))))
     (rest (assoc pred extras)))))

; This is very different from what was in state-utils before, but I'm pretty
; sure this does the job.
(defmethod copy-state ((st bit-state))
  (let ((the-copy (make-bit-state (state-atoms st))))
    (setf (tagged-state-tags-info the-copy)
          (copy-tree (tagged-state-tags-info st)))
    the-copy))

; I don't know what these next two functions do, so I left them as defuns
; rather than trying to define them as methods for the bit-state class.
(defun BIT-statebody-search-array
  (pred-array pred-type-counts entity-number-tables complex-position)
  (let ((position (mapcar #'second complex-position)))
    (cond
     ((null position)
      nil)
     ((= (apply #'aref pred-array position) 1)
      (cons
       (mapcar #'(lambda (num entity-number-table)
                   (gethash num entity-number-table))
               position entity-number-tables)
       (BIT-statebody-search-array
        pred-array pred-type-counts entity-number-tables
        (BIT-statebody-increment-position
         complex-position pred-type-counts))))
     (t
      (BIT-statebody-search-array
       pred-array pred-type-counts entity-number-tables
       (BIT-statebody-increment-position
        complex-position pred-type-counts))))))

(defun BIT-statebody-increment-position
  (position pred-type-counts)
  (cond
   ((null position) nil)
   ((eq :fixed (first (first position)))
    (if (BIT-statebody-increment-position
         (rest position) (rest pred-type-counts))
        position
      nil))
   (t
    (incf (second (first position)))
    (cond 
     ((< (second (first position)) (first pred-type-counts))
      position)
     ((null (rest position))
      nil)
     ((BIT-statebody-increment-position
       (rest position) (rest pred-type-counts))
      (setf (second (first position)) 0)
      position)
     (t nil)))))





(defun copy-hash-table (H1 &optional (copy-fn #'identity))
  ;; modified this to use the hash-table-test function, instead of always building
  ;; an "EQUAL" hash-table.  Also initialized to be the same size, avoiding
  ;; resizes in building, I hope. [2002/10/08:rpg]
  (let ((H2 (make-hash-table :size (hash-table-size H1) :test (hash-table-test H1))))
    (maphash #'(lambda (key val) (setf (gethash key H2) (funcall copy-fn val)))
             H1)
    H2))

(defmethod state-trajectory ((st tagged-state))
  (let ((state (copy-state st)))
    (loop for state-info in (tagged-state-tags-info state)
        for state-list = (state-atoms state)
        with trajectory
        do (push state-list trajectory)
           (retract-state-changes state (first state-info))
        finally (return trajectory))))
