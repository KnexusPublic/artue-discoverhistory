;;; The Original Code is SHOP2.  
;;; 
;;; The Initial Developer of the Original Code is the University of
;;; Maryland. Portions created by the Initial Developer are Copyright (C)
;;; 2002,2003 the Initial Developer. All Rights Reserved.
;;;
;;; Additional developments made by Robert P. Goldman, John Maraist.
;;; Portions created by Drs. Goldman and Maraist are Copyright (C)
;;; 2004-2007 SIFT, LLC. 

;;; Smart Information Flow Technologies Copyright 2006-2007 Unpublished work
;;; 
;;; GOVERNMENT PURPOSE RIGHTS
;;; 
;;; Contract No.         FA8650-06-C-7606, 
;;; Contractor Name      Smart Information Flow Technologies, LLC
;;;                      d/b/a SIFT, LLC
;;; Contractor Address   211 N 1st Street, Suite 300
;;;                      Minneapolis, MN 55401
;;; Expiration Date      5/2/2011
;;; 
;;; The Government's rights to use, modify, reproduce, release,
;;; perform, display, or disclose this software are restricted by
;;; paragraph (b)(2) of the Rights in Noncommercial Computer Software
;;; and Noncommercial Computer Software Documentation clause contained
;;; in the above identified contract. No restrictions apply after the
;;; expiration date shown above. Any reproduction of the software or
;;; portions thereof marked with this legend must also reproduce the
;;; markings.


 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE. 
 
 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; (at your option) any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :shop2)

(defclass pddl-action-test ( rrt:entry )
     ()
  )

(defclass pddl-action-test-group ( rrt:test-group )
  ((action-def
    :accessor action-def
    :initarg :action-def)
   (domain
    :accessor domain
    )
   (action
    :accessor action
    ))
  (:default-initargs :default-test-class 'pddl-action-test))
      

(defmethod rrt:setup :after ((group pddl-action-test-group))
  (setf (domain group) (make-instance 'simple-pddl-domain
                         :name (gentemp (symbol-name '#:domain))))
  (setf (action group)
    (process-action (domain group) (action-def group))))

(rrt:deftest-group (simple-pddl-actions :type pddl-action-test-group
                                        :action-def
                                        (:action drive
                                                 :parameters (?v - vehicle
                                                                 ?from ?to - location
                                                                 ?fbefore ?fafter - fuel-level)
                                                 :precondition (and (at ?v ?from)
                                                                    (accessible ?v ?from ?to) 
                                                                    (fuel ?v ?fbefore)
                                                                    (next ?fbefore ?fafter))
                                                 :effect (and (not (at ?v ?from))
                                                              (at ?v ?to)
                                                              (not (fuel ?v ?fbefore))
                                                              (fuel ?v ?fafter)))))

(defvar *pddl-test-test-action*)
                   

(defmethod rrt:do-entry :around
  ((test pddl-action-test) &optional (s *standard-output*))
  "Bind the special variable *pddl-test-test-action* to the parsed
representation of the action-def of the parent test-group, so that
the tests can read this value."
  (let ((*pddl-test-test-action* (action (rrt:parent test))))
    (declare (special *pddl-test-test-action*))
    (call-next-method test s)))

(rrt:deftest (check-head :parent simple-pddl-actions)
    (pddl-action-head *pddl-test-test-action*)
  (drive ?v ?from ?to ?fbefore ?fafter))

(rrt:deftest (check-precondition :parent simple-pddl-actions)
    (pddl-action-precondition *pddl-test-test-action*)
  (and (at ?v ?from) (accessible ?v ?from ?to) (fuel ?v ?fbefore)
       (next ?fbefore ?fafter)))

(rrt:deftest (check-effect :parent simple-pddl-actions)
    (pddl-action-effect *pddl-test-test-action*)
  (and (not (at ?v ?from)) (at ?v ?to) (not (fuel ?v ?fbefore)) (fuel ?v ?fafter)))

(rrt:deftest (check-cost-fun :parent simple-pddl-actions)
      (pddl-action-cost-fun  *pddl-test-test-action*)
  1.0)

;;;---------------------------------------------------------------------------
;;; End of tests concerning the initial parsing of simple pddl actions.
;;;---------------------------------------------------------------------------

;;;---------------------------------------------------------------------------
;;; Quick test to see if we can define actions...
;;;---------------------------------------------------------------------------
(rrt:deftest defdomain-single-action
             (progn
               (defdomain (#.(gentemp (symbol-name '#:domain))
                             :type simple-pddl-domain
                             :redefine-ok t)
                 ((:action walk
                           :parameters (?from ?to - loc)
                           :precondition (at robot ?from)
                           :effect (and (not (at robot ?from))
                                        (at robot ?to)))))
               (first (multiple-value-list (operator *domain* '!walk))))
             (PDDL-ACTION (!WALK ?FROM ?TO) (AT ROBOT ?FROM)
                          (AND (NOT (AT ROBOT ?FROM)) (AT ROBOT ?TO)) 1.0))

      
;;;---------------------------------------------------------------------------
;;; Test to see if we can add and delete fluents...
;;;---------------------------------------------------------------------------
(defclass action-test ( rrt:entry )
     ()
  )

(defclass action-test-group ( rrt:test-group )
     ((old-domain
       :accessor old-domain
       )
      (domain-def
       :initarg :domain-def
       :reader domain-def
       )
      )
  (:default-initargs :default-test-class 'action-test)
  )

(defmethod rrt:setup ((group action-test-group))
  ;; keep old *domain* binding, just in case...
  (if (boundp '*domain*)
      (setf (old-domain group) *domain*)
      (setf (old-domain group) nil))
  (eval (domain-def group)))

(defmethod rrt:teardown ((group action-test-group))
  ;; restore the previous *domain* binding...
  (if (old-domain group)
      (setf *domain* (old-domain group))
    (setf *domain* nil)))


(rrt:deftest-group (pddl-add-and-delete :type action-test-group
                                        :domain-def
                                        (defdomain (#.(gentemp (symbol-name '#:domain))
                                                    :type simple-pddl-domain
                                                    :redefine-ok t)
                                            ((:action walk
                                                      :parameters (?from ?to - loc)
                                                      :precondition (at robot ?from)
                                                      :effect (and (not (at robot ?from))
                                                                   (at robot ?to))))))
    (rrt:deftest pddl-action-test
        (let ((*state-encoding* :list))
          (declare (special *state-encoding*))
          (let ((state (make-state '((at robot new-jersey)))))
            (apply-action state
                          '(!walk new-jersey new-york)
                          (operator *domain* '!walk)
                          nil 0 nil)
            (state-atoms state)))
      ((AT ROBOT NEW-YORK)))

  (rrt:deftest pddl-action-failed-precond-test
      (let ((*state-encoding* :list))
        (declare (special *state-encoding*))
        (let ((state (make-state nil)))
          (first
           (multiple-value-list
            (apply-action state
                          '(!walk new-jersey new-york)
                          (operator *domain* '!walk)
                          nil 0 nil)))))
    fail))

(rrt:deftest-group quantified-preconditions

    (rrt:deftest simplest
        (let* ((domain
                (defdomain (#.(gentemp (symbol-name '#:domain))
                            :type pddl-domain
                            :redefine-ok t)
                    nil))
               (act (process-action domain
                                    '(:action move
                                      :parameters
                                      (?a - airplane ?t - airplanetype ?d1 - direction ?s1 ?s2  - segment ?d2 - direction)
                                      :precondition
                                      (forall (?s - segment)
                                       (imply
                                        (and
                                         (is-blocked ?s ?t ?s2 ?d2)
                                         (not (= ?s ?s1)))
                                        (not (occupied ?s))))))))
          (pddl-action-precondition act))
      (forall (?s)
              (:of-type segment ?s)
              (imply
               (and
                (is-blocked ?s ?t ?s2 ?d2)
                (not (= ?s ?s1)))
               (not (occupied ?s)))))


      (rrt:deftest with-others
          (let* ((domain
                  (defdomain (#.(gentemp (symbol-name '#:domain))
                              :type pddl-domain
                              :redefine-ok t)
                      nil))
                 (act (process-action domain
                                      '(:action move
                                        :parameters
                                        (?a - airplane ?t - airplanetype ?d1 - direction ?s1 ?s2  - segment ?d2 - direction)
                                        :precondition
                                        (and
                                         (has-type ?a ?t)
                                         (is-moving ?a)
                                         (not (= ?s1 ?s2))
                                         (facing ?a ?d1)
                                         (can-move ?s1 ?s2 ?d1)
                                         (move-dir ?s1 ?s2 ?d2)
                                         (at-segment ?a ?s1)
                                         (forall (?s - segment)
                                          (imply
                                           (and
                                            (is-blocked ?s ?t ?s2 ?d2)
                                            (not (= ?s ?s1)))
                                           (not (occupied ?s)))))))))
            (pddl-action-precondition act))
        (and
         (has-type ?a ?t)
         (is-moving ?a)
         (not (= ?s1 ?s2))
         (facing ?a ?d1)
         (can-move ?s1 ?s2 ?d1)
         (move-dir ?s1 ?s2 ?d2)
         (at-segment ?a ?s1)
         (forall (?s)
                 (:of-type segment ?s)
                 (imply
                  (and
                   (is-blocked ?s ?t ?s2 ?d2)
                   (not (= ?s ?s1)))
                  (not (occupied ?s))))))

      (rrt:deftest multiple-variables
          (let* ((domain
                  (defdomain (#.(gentemp (symbol-name '#:domain))
                              :type pddl-domain
                              :redefine-ok t)
                      nil))
                 (act (process-action domain
                                      '(:action move
                                        :parameters
                                        (?a - airplane ?t - airplanetype ?d1 - direction ?s1 ?s2  - segment ?d2 - direction)
                                        :precondition
                                        (forall (?s - segment ?a2 - airplane)
                                         (imply
                                          (and
                                           (is-blocked ?s ?t ?s2 ?d2)
                                           (not (= ?s ?s1)))
                                          (not (at ?a2 ?s))))))))
            (pddl-action-precondition act))
        (forall (?s ?a2)
                (and 
                 (:of-type segment ?s)
                 (:of-type airplane ?a2))
                (imply
                 (and
                  (is-blocked ?s ?t ?s2 ?d2)
                  (not (= ?s ?s1)))
                 (not (at ?a2 ?s)))))
      (rrt:deftest existential
          (let* ((domain
                  (defdomain (#.(gentemp (symbol-name '#:domain))
                              :type pddl-domain
                              :redefine-ok t)
                      nil))
                 (act (process-action domain
                                      '(:action move
                                        :parameters
                                        (?a - airplane ?t - airplanetype ?d1 - direction ?s1 ?s2  - segment ?d2 - direction)
                                        :precondition
                                        (and
                                         (has-type ?a ?t)
                                         (is-moving ?a)
                                         (not (= ?s1 ?s2))
                                         (facing ?a ?d1)
                                         (can-move ?s1 ?s2 ?d1)
                                         (move-dir ?s1 ?s2 ?d2)
                                         (at-segment ?a ?s1)
                                         (not         (exists        (?a1 - airplane)        (and         (not (= ?a1 ?a))
                                                                                        (blocked ?s2 ?a1))))
                                         (forall (?s - segment)        (imply         (and         (is-blocked ?s ?t ?s2 ?d2)
                                                                                (not (= ?s ?s1)))
                                                                        (not (occupied ?s))
                                                                        ))
                                         )
                                        :effect
                                        (and
                                         (occupied ?s2)
                                         (blocked ?s2 ?a)
                                         (not (occupied ?s1))
                                         (when         (not (is-blocked ?s1 ?t ?s2 ?d2))
                                           (not (blocked ?s1 ?a)))
                                         (when         (not (= ?d1 ?d2))
                                           (not (facing ?a ?d1)))
                                         (not (at-segment ?a ?s1))
                                         (forall (?s - segment)        (when         (is-blocked ?s ?t ?s2 ?d2)
                                                                  (blocked ?s ?a)
                                                                  ))
                                         (forall (?s - segment)        (when         (and        (is-blocked ?s ?t ?s1 ?d1)
                                                                                (not (= ?s ?s2)) 
                                                                                (not (is-blocked ?s ?t ?s2 ?d2))
                                                                                )
                                                                  (not (blocked ?s ?a))
                                                                  ))
                                         (at-segment ?a ?s2)
                                         (when         (not (= ?d1 ?d2))
                                           (facing ?a ?d2))
                                         )
                                        ))))
            (pddl-action-precondition act))
        (and
         (has-type ?a ?t)
         (is-moving ?a)
         (not (= ?s1 ?s2))
         (facing ?a ?d1)
         (can-move ?s1 ?s2 ?d1)
         (move-dir ?s1 ?s2 ?d2)
         (at-segment ?a ?s1)
         (not
          (exists (?a1)
                  (:of-type airplane ?a1)
                  (and         (not (= ?a1 ?a))
                        (blocked ?s2 ?a1))))
         (forall (?s)
                 (:of-type segment ?s)
                 (imply (and (is-blocked ?s ?t ?s2 ?d2)
                             (not (= ?s ?s1)))
                        (not (occupied ?s))))
         )))


(defun prop-sorter (p1 p2)
  (cond ((and p1 p2)
         (or (string-lessp (first p1) (first p2))
             (and (not (string-lessp (first p2) (first p1)))
                  (prop-sorter (cdr p1) (cdr p2)))))
        ;; if p1 is longer, it's greater --- this should never happen!
        (p1 nil)))                  

;;; need to test WHENs...
(rrt:deftest-group (simple-when
                    :type action-test-group
                    :domain-def
                    (defdomain (#.(gentemp (symbol-name '#:domain))
                                :type pddl-domain
                                :redefine-ok t)
                        ((:action walk
                                  :parameters (?from ?to - loc)
                                  :precondition (at robot ?from)
                                  :effect (and (not (at robot ?from))
                                               (at robot ?to)
                                               (when (carrying cargo)
                                                 (and (not (at cargo ?from))
                                                      (at cargo ?to))))))))
    (rrt:deftest simple-when-no
        (let ((*state-encoding* :list))
          (declare (special *state-encoding*))
          (let ((state (make-state '((at robot new-jersey)))))
            (apply-action state
                          '(!walk new-jersey new-york)
                          (operator *domain* '!walk)
                          nil 0 nil)
            (state-atoms state)))
      ((AT ROBOT NEW-YORK)))
  (rrt:deftest simple-when-yes
      (let ((*state-encoding* :list))
        (declare (special *state-encoding*))
        (let ((state (make-state '((at robot new-jersey)
                                   (carrying cargo)))))
                                   
          (apply-action state
                        '(!walk new-jersey new-york)
                        (operator *domain* '!walk)
                        nil 0 nil)
          (sort (state-atoms state)
                'prop-sorter)))
    ((AT CARGO NEW-YORK) (AT ROBOT NEW-YORK) (CARRYING CARGO))))
                                                          

(rrt:deftest-group (quantified-when
                    :type action-test-group
                    :domain-def
                    (defdomain (#.(gentemp (symbol-name '#:domain))
                                :type pddl-domain
                                :redefine-ok t)
                        ((:action walk
                                  :parameters (?from ?to - loc)
                                  :precondition (at robot ?from)
                                  :effect (and (not (at robot ?from))
                                               (at robot ?to)
                                               (forall (?x - luggage)
                                                 (when (carrying robot ?x)
                                                   (and (not (at ?x ?from))
                                                        (at ?x ?to)))))))))
    (rrt:deftest check-state-making
        (let ((*state-encoding* :list))
          (let ((state (make-state '((at robot new-jersey)
                                     (at bag1 new-jersey)
                                     (at bag2 new-jersey)
                                     (at bag3 new-jersey)
                                     (:of-type luggage bag1)
                                     (:of-type luggage bag2)
                                     (:of-type luggage bag3)))))
            (sort (state-atoms state) 'prop-sorter)))
      ((at bag1 new-jersey)
       (at bag2 new-jersey)
       (at bag3 new-jersey)
       (at robot new-jersey)
       (:of-type luggage bag1)
       (:of-type luggage bag2)
       (:of-type luggage bag3)))

  (rrt:deftest check-antecedent-unsatisfied
        (let ((*state-encoding* :list))
          (let ((state (make-state '((at robot new-jersey)
                                     (at bag1 new-jersey)
                                     (at bag2 new-jersey)
                                     (at bag3 new-jersey)
                                     (:of-type luggage bag1)
                                     (:of-type luggage bag2)
                                     (:of-type luggage bag3)))))
            (apply-action state
                        '(!walk new-jersey new-york)
                        (operator *domain* '!walk)
                        nil 0 nil)
            (sort (state-atoms state) 'prop-sorter)))
      ((at bag1 new-jersey)
       (at bag2 new-jersey)
       (at bag3 new-jersey)
       (at robot new-york)
       (:of-type luggage bag1)
       (:of-type luggage bag2)
       (:of-type luggage bag3)))

  (rrt:deftest check-antecedent-satisfied
        (let ((*state-encoding* :list))
          (let ((state (make-state '((at robot new-jersey)
                                     (at bag1 new-jersey)
                                     (at bag2 new-jersey)
                                     (at bag3 new-jersey)
                                     (carrying robot bag1)
                                     (:of-type luggage bag1)
                                     (:of-type luggage bag2)
                                     (:of-type luggage bag3)))))
            (apply-action state
                        '(!walk new-jersey new-york)
                        (operator *domain* '!walk)
                        nil 0 nil)
            (sort (state-atoms state) 'prop-sorter)))
      ((at bag1 new-york)
       (at bag2 new-jersey)
       (at bag3 new-jersey)
       (at robot new-york)
       (carrying robot bag1)
       (:of-type luggage bag1)
       (:of-type luggage bag2)
       (:of-type luggage bag3)))

  (rrt:deftest check-antecedent-multiply-satisfied
        (let ((*state-encoding* :list))
          (let ((state (make-state '((at robot new-jersey)
                                     (at bag1 new-jersey)
                                     (at bag2 new-jersey)
                                     (at bag3 new-jersey)
                                     (carrying robot bag1)
                                     (carrying robot bag3)
                                     (:of-type luggage bag1)
                                     (:of-type luggage bag2)
                                     (:of-type luggage bag3)))))
            (apply-action state
                        '(!walk new-jersey new-york)
                        (operator *domain* '!walk)
                        nil 0 nil)
            (sort (state-atoms state) 'prop-sorter)))
      ((at bag1 new-york)
       (at bag2 new-jersey)
       (at bag3 new-york)
       (at robot new-york)
       (carrying robot bag1)
       (carrying robot bag3)
       (:of-type luggage bag1)
       (:of-type luggage bag2)
       (:of-type luggage bag3)))
  )
    

