;;; The Original Code is SHOP2.  
;;; 
;;; The Initial Developer of the Original Code is the University of
;;; Maryland. Portions created by the Initial Developer are Copyright (C)
;;; 2002,2003 the Initial Developer. All Rights Reserved.
;;;
;;; Additional developments made by Robert P. Goldman, John Maraist.
;;; Portions created by Drs. Goldman and Maraist are Copyright (C)
;;; 2004-2007 SIFT, LLC.

;;; Smart Information Flow Technologies Copyright 2006-2007 Unpublished work
;;; 
;;; GOVERNMENT PURPOSE RIGHTS
;;; 
;;; Contract No.         FA8650-06-C-7606, 
;;; Contractor Name      Smart Information Flow Technologies, LLC
;;;                      d/b/a SIFT, LLC
;;; Contractor Address   211 N 1st Street, Suite 300
;;;                      Minneapolis, MN 55401
;;; Expiration Date      5/2/2011
;;; 
;;; The Government's rights to use, modify, reproduce, release,
;;; perform, display, or disclose this software are restricted by
;;; paragraph (b)(2) of the Rights in Noncommercial Computer Software
;;; and Noncommercial Computer Software Documentation clause contained
;;; in the above identified contract. No restrictions apply after the
;;; expiration date shown above. Any reproduction of the software or
;;; portions thereof marked with this legend must also reproduce the
;;; markings.


 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE. 
 
 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; (at your option) any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :shop2)

(defvar *return-wait-times* nil)


(defgeneric validator-export (domain plan stream)
  (:documentation "Print a plan in a way that it can be consumed by
the VAL validator."))

(defun write-pddl-plan (plan &key (domain *domain*) (stream t stream-supplied-p) (filename nil))
  "Write the plan argument \(in a way sensitive to the domain type\)
to the FILENAME or STREAM in a format that it can be checked by the validator.
This is an easier to use interface to the validator-export function, qv."
  (when (and filename stream-supplied-p)
    (error "Cannot supply both a filename and stream destination for write-pddl-plan."))
  (when filename
    (setf stream (open filename :direction :output)))
  (unwind-protect
       (validator-export domain plan stream)
    (when filename (close stream))))


;;;---------------------------------------------------------------------------
;;; Class definitions for domains, used to control processing of PDDL
;;; syntax.[2006/08/01:rpg]
;;;---------------------------------------------------------------------------


(defclass simple-pddl-domain ( domain )
  (
    (types
       :initarg :types
       :initform nil
       :reader domain-types
    )
    (predicates
       :initarg :predicates
       :initform nil
       :accessor domain-predicates
    )
)
  (:documentation "A new class of SHOP2 domain that permits inclusion of
PDDL operator definitions.")
  )

(defclass universal-precondition-mixin ()
     ()
  )

(defclass existential-precondition-mixin ()
     ()
  )


(defclass conditional-effects-mixin ()
     ()
  )

(defclass equality-mixin ()
     ()
  )

(defclass pddl-domain ( conditional-effects-mixin existential-precondition-mixin universal-precondition-mixin equality-mixin simple-pddl-domain )
  ()
  (:documentation "A new class of SHOP2 domain that permits inclusion of
PDDL operator definitions.  Right now, this approximates what happens if you have
the :adl flag in a PDDL domain file.")
)

(defgeneric process-action (domain action-def)
  (:documentation "PDDL has actions \(vanilla SHOP2 has operators\), so we 
add code for processing the actions that is parallel to process-operator."))

;;;---------------------------------------------------------------------------
;;; Structure for PDDL actions -- plays a role akin to the role played
;;; by the OPERATOR defstruct in vanilla shop2.
;;;---------------------------------------------------------------------------
(defstruct (pddl-action :named (:type list))
  (head nil :type list)
  performer
  precondition
  (duration 0)
  effect
  (cost-fun nil))

(defmethod parse-domain-items :around ((domain equality-mixin) items)
  "Add the axiom that treats equality as a built-in predicate.  This should 
later be compiled into find-satisfiers or something."
  (let ((equality-axiom '(:- (= ?x ?x) nil)))
    (set-variable-property domain equality-axiom)
    (call-next-method domain (cons equality-axiom items))))

;;; parsing PDDL action items --- for now we treat them just as
;;; operators, but this may get us in trouble! [2006/07/28:rpg]
(defmethod parse-domain-item ((domain simple-pddl-domain) (item-key (eql ':action)) item)
  (let ((op-name (second item)))
    ;; do some nasty voodoo to give PDDL actions names like SHOP2
    ;; operators [2006/07/31:rpg]
    (unless (eql (elt (symbol-name op-name) 0) #\!)
      (setf op-name (intern (concatenate 'string
                                         "!" (symbol-name op-name))
                            (symbol-package op-name)
                            ))
;;;      (format t "~&Making new SHOP2 operator name ~S from old name ~S.~%"
;;;              op-name (second item))
      (setf (second item) op-name))
    (with-slots (operators) domain
      (when (gethash op-name operators)
        (error "There is more than one operator named ~s" op-name))
      (setf (gethash op-name operators) (process-action domain item)))))

(defmethod parse-domain-item ((domain simple-pddl-domain) (item-key (eql ':predicates)) item)
  "MCM: I want to save these."
  (with-slots (predicates) domain
     (setf predicates (append predicates (cdr item)))
  )
  (values))

(defmethod parse-domain-item ((domain simple-pddl-domain) (item-key (eql ':types)) item)
  "MCM: I want to save these."
  (with-slots (types) domain
     (setf types (append types (cdr item)))
  )
  (values))

(defmethod process-action ((domain simple-pddl-domain) action-def)
  "Processes a PDDL action.  Handles only the simplest class of actions, 
with unconditional actions."
  (destructuring-bind (keyword action-symbol &key performer parameters precondition 
                                                  effect (duration 0) (cost 1.0))
      action-def
    (unless (eq keyword :action)
      (error "Unexpected action expression: ~A" action-def))
    ;; this takes care of processing variables and marking primitive
    ;; actions [2006/08/01:rpg]
    (set-variable-property domain action-def)
    (multiple-value-bind (param-vars param-types)
        (typed-list-vars parameters)
      (declare (ignore param-types))
      (let ((precond
             (translate-precondition domain precondition))
            (eff
             (translate-effect domain effect))
            (head (cons action-symbol param-vars)))
        (make-pddl-action :head head :performer performer :precondition precond
                          :effect eff :duration duration :cost-fun cost)))))

;;;---------------------------------------------------------------------------
;;; Additional generic functions, used to tailor the translation of
;;; PDDL actions into SHOP2 syntax.  The different PDDL constructs are
;;; gated by PDDL requirements flags, which are represented as mixins
;;; in PDDL domains.
;;;---------------------------------------------------------------------------
(defgeneric translate-precondition (domain expression)
  (:documentation
   "This generic function is used to translate PDDL-style preconditions
into SHOP2-style syntax.  The rewriting is done based on the domain so 
that different syntax features can be turned on and off."))

(defgeneric translate-effect (domain expression)
  (:documentation
   "This generic function is used to translate PDDL-style effects
into SHOP2-style syntax.  The rewriting is done based on the domain so 
that different syntax features can be turned on and off."))

;;;---------------------------------------------------------------------------
;;; Methods for translate-effect
;;;---------------------------------------------------------------------------


(defmethod translate-effect ((domain simple-pddl-domain) effect)
  "Basis method for translating a PDDL effect into SHOP2 syntax is
to just leave it alone."
  effect)

(defmethod translate-effect ((domain conditional-effects-mixin) effect)
  "This method translates any forall expressions in a PDDL precondition into the
slightly-different SHOP2 FORALL syntax.
It then invokes the next method, to insure that all PDDL - SHOP2 constructs are
translated."
  (let ((new-effect (translate-pddl-quantifier effect 'forall)))
    (call-next-method domain new-effect)))

;;;---------------------------------------------------------------------------
;;; Methods for translate-precondition
;;;---------------------------------------------------------------------------

(defmethod translate-precondition ((domain simple-pddl-domain) expression)
  "Basis method for translating a PDDL precondition into SHOP2 syntax is
to just leave it alone."
  expression)

(defmethod translate-precondition ((domain universal-precondition-mixin) expression)
  "This method translates any forall expressions in a PDDL precondition into the
slightly-different SHOP2 FORALL syntax.
It then invokes the next method, to insure that all PDDL - SHOP2 constructs are
translated."
  (let ((new-expr (translate-pddl-quantifier expression 'forall)))
    (call-next-method domain new-expr)))

(defmethod translate-precondition ((domain existential-precondition-mixin) expression)
  "This method translates any exists expressions in a PDDL precondition into the
slightly-different SHOP2 EXISTS syntax.
It then invokes the next method, to insure that all PDDL - SHOP2 constructs are
translated."
  (let ((new-expr (translate-pddl-quantifier expression 'exists)))
    (call-next-method domain new-expr)))


;;;
;;;---------------------------------------------------------------------------
;;; Helper functions
;;;---------------------------------------------------------------------------

(defun translate-pddl-quantifier (expression quantifier)
  "Translate EXPRESSION from PDDL quantifier \(forall and exists\) notation 
into SHOP2 quantifier notation \(and adds some
\(:of-type <type> <var>\) conditions\)."
  (labels ((iter (expr)
             (cond ((and (listp expr) (eq (first expr) quantifier))
                    (rewrite-quant expr))
                   ((listp expr)
                    (mapcar #'iter expr))
                   ((atom expr) expr)))
           (rewrite-quant (quant-expr)
             (destructuring-bind (quant typed-list sub-expr)
                 quant-expr
               (declare (ignore quant))
               (multiple-value-bind (vars types)
                   (typed-list-vars typed-list)
                 `(,quantifier ,vars
                          ,(let ((exprs (of-type-exprs vars types)))
                             (if (= (length exprs) 1)
                                 (first exprs)
                                 (cons 'and exprs)))
                          ,(iter sub-expr))))))
    (iter expression)))

(defun of-type-exprs (vars types)
  "Because SHOP2 doesn't have the same typing, we take variables
and their types and generate a list of distinguished propositions 
we can use in preconditions."
  (loop for v in vars
        as type in types
        collect `(:of-type ,type ,v)))
  

;;; this should be fixed to check to make sure everything in the
;;; return value list is really a variable.... [2006/07/28:rpg]
(defun typed-list-vars (typed-list)
  "Takes a typed-list (this is a PDDL construct) as argument and 
pulls out the variables and types and returns two parallel
lists of variable names and type names."
  (let ((srav nil)
        (sepyt nil))
    (labels ((iter (lst &optional (counter 0))
               ;; counter is how many variables of the current type
               ;; have we seen so far. [2006/07/31:rpg]
               (cond ((null lst) nil)
                     ((eq (first lst) '-)
                      (setf sepyt
                        (nconc (make-list counter :initial-element (second lst))
                               sepyt))
                      (iter (cddr lst) 0))
                     (t (push (first lst) srav)
                        (iter (rest lst) (1+ counter))))))
      (iter typed-list))
    (values 
     (reverse srav)
     (reverse sepyt))))

;;;---------------------------------------------------------------------------
;;; Apply-action, which plays a role akin to apply-operator in vanilla
;;; SHOP2.
;;;---------------------------------------------------------------------------
(defun apply-action (state task-body action protections depth
                           in-unifier)
  "If ACTION, a PDDL ACTION, is applicable to TASK in STATE, then 
APPLY-ACTION returns five values:
1.  the operator as applied, with all bindings;
2.  a state tag, for backtracking purposes;
3.  a new set of protections;
4.  the cost of the new operator;
5.  unifier.
This function also DESTRUCTIVELY MODIFIES its STATE argument.
Otherwise it returns FAIL."
  (let* ((standardized-action (standardize action))
         (head (pddl-action-head standardized-action))
         (preconditions (pddl-action-precondition standardized-action))
             (effect (pddl-action-effect standardized-action))
         unifier
         results
         ret-list
        )
    (when (eq (car task-body) '!replan)
        (return-from apply-action
            (values 
                (apply-substitution task-body unifier) 
                (tag-state state)
                protections 0.0 nil
            )
        )
    )
    (when (eq (car task-body) '!wait-for)
        (trace-print 
            :operators 
            (first head) 
            state
            "~2%Depth ~s, applying PDDL action ~s~%      task ~s~%       effect ~s"
            depth
            (first head)
            (apply-substitution task-body unifier)
            nil
        )
        (setq ret-list
            (append
                (multiple-value-list
                  (apply-pddl-effects 
                      state task-body nil
                      protections depth
                  )
                )
                (list 1.0 nil)
            )
        )
        (let ((duration))
            (setq duration
                (apply #'pass-time-until state *domain* (if (cdr task-body) (cdr task-body) (list nil)))
            )
            (return-from apply-action 
                (if *return-wait-times*
                    ;then
                    (values-list (cons (list '!wait duration) (cdr ret-list)))
                    ;else
                    (values-list ret-list)
                )
            )
        )
    )

    ;; added rudimentary arity-checking...
    (unless (= (length task-body) (length head))
      (cerror "Continue (action application will fail)"
              "Arity of action in the plan library, ~D~%~T~S~%does not match task, ~D~%~T~S"
              (length head)
              head
              (length task-body)
              task-body)
    )

    ;; new tracing facility for debugging
    (when *traced-tasks*
       (when (member (first head) *traced-tasks*)
           (break "Applying action for ~A~%~S" (first head) task-body)
       )
    )

    (let ((action-unifier (unify head (apply-substitution task-body in-unifier))))
      (when (eql action-unifier 'fail)
          (return-from apply-action (values 'fail protections 0))
      )

      ;; everything below is "if action-unifier != fail"
      (setf action-unifier
            (compose-substitutions action-unifier in-unifier)
      )
      (setq unifier action-unifier)
      
      ;; first check the preconditions, if any
      (when preconditions
          (let* ((pre (apply-substitution preconditions unifier))
          
                  ;; need to specially handle the preconditions, since the
                  ;; syntax of PDDL preconditions are different from
                  ;; SHOP2. [2006/07/31:rpg]
                     (pu (shopthpr:find-satisfiers pre state t))
               )
              (unless pu
                  (trace-print :operators (first head) state
                      "~2%Depth ~s, inapplicable action ~s~%     task ~s.~%     Precondition failed: ~s.~%"
                      depth
                      (first head)
                      (apply-substitution task-body unifier)
                      pre
                  )
                  (trace-print :operators (first head) state
                      "Current state ~A ~%Current fluents ~A" 
                      (state-atoms state) 
                      (shop2.common::list-state-values-as-atoms state)
                  )
                  (return-from apply-action (values 'fail pre 0))
              )
              (setq unifier (compose-substitutions unifier (first pu)))
          )      
      )
      ;; at this point UNIFIER is bound to the results of unifying
      ;; TASK-BODY with the operator's head and then retrieving
      ;; the first set of bindings that satisfy the preconditions.
      ;; we will return this unifier later on, assuming that
      ;; bindings from add and delete lists should not be plugged
      ;; in. [2004/01/20:rpg]
      (let* 
          ((effect-subbed (apply-substitution effect unifier))
           (head-subbed (apply-substitution head unifier))
           (cost-value
               (eval (apply-substitution 
                       (pddl-action-cost-fun standardized-action) 
                       unifier
                      )
               )
           )
           (duration 
               (eval 
                   (apply-substitution 
                       (pddl-action-duration standardized-action) 
                       unifier
                   )
               )
           )
           (cost-number (if (numberp cost-value) cost-value 1.0))
          )
          (when *explanation*
              (setq head-subbed 
                  `(,@(cons 
                          (first head)
                          (mapcar #'list
                              (rest (second action))
                              (rest head-subbed)
                              )
                          )
                      :explanation
                      ,(shopthpr:explain-satisfier 
                          (apply-substitution preconditions unifier)
                          state
                       )
                   )
               )
          )
          (trace-print 
              :operators 
              (first head) 
              state
              "~2%Depth ~s, applying PDDL action ~s~%      task ~s~%       effect ~s"
              depth
              (first head)
              (apply-substitution task-body unifier)
              effect-subbed
          )

          (setq results
              (multiple-value-list
                  (apply-pddl-effects 
                      state head-subbed effect-subbed
                      protections depth
                  )
              )
          )
          
          (when (eq (pass-time duration state) 'fail)
              (trace-print :operators (first head) state
                  "~2%Depth ~s, action failed due to event cycle ~s~%                  task ~s.~%~%"
                  depth
                  (first head)
                  (apply-substitution task-body unifier)
              )
              (retract-state-changes state (second results))
              ;(break "Action failed.")
              (return-from apply-action (values 'fail protections 0))
          )
          
          (trace-print :operators (first head) state "~&Operator successfully applied.")
          (values-list 
              (append
                  results
                  (list cost-number unifier)
              )
          )
      )
    );; end of scope for action-unifier...
  )
)

(defun is-set (x)
    (eq (car x) 'set)
)

(defun is-decrease (x)
    (eq (car x) 'decrease)
)

(defun is-increase (x)
    (eq (car x) 'increase)
)

(defun is-assign (x)
    (eq (car x) 'assign)
)

(defun is-create (x)
    (eq (car x) 'create)
)

(defun transform-create-to-assign (create)
    (list 'assign (third create) (fourth create))
)
    

(defun apply-pddl-effects (state head-subbed effect-subbed protections depth)
    ;; all this stuff below here must be revised since we have an EFFECT,
    ;; instead of add and delete lists... [2006/07/30:rpg]
      (setq *cur-head* head-subbed)
      (multiple-value-bind (final-adds final-dels)
          (extract-adds-and-deletes effect-subbed state)
          (let ((assigns (remove-if-not #'is-assign final-adds))
                (creates (remove-if-not #'is-create final-adds))
                (new-bindings nil))
              (setq assigns (append assigns (mapcar #'transform-create-to-assign creates)))
              (when (not (null assigns))
;                  (format t "~%assigns: ~A ~%final-adds: ~A ~%final-dels: ~A" assigns final-adds final-dels)
                  (dolist (a assigns)
;                      (format t "~%Evaluating assignment: ~A" a)
                      (setq a (apply-substitution a new-bindings))
                      (unless (groundp (caddr a))
                          (error 
                              "~%Attempting to bind variable to non-ground expression ~A"
                              (caddr a)
                          )
                      )
                      (push (make-binding (cadr a) (state-eval state (caddr a))) new-bindings)
                  )
                  (setq final-adds (apply-substitution final-adds new-bindings))
                  (setq final-dels (apply-substitution final-dels new-bindings))
;                  (format t "~%assigns: ~A ~%new-bindings: ~A ~%final-adds: ~A ~%final-dels: ~A" assigns new-bindings final-adds final-dels)
;                  (break)
              )
          )
          (let ((protections1 protections)               
                (statetag (tag-state state))
                (decreases (remove-if-not #'is-decrease final-adds))
                (increases (remove-if-not #'is-increase final-adds))
                (sets (remove-if-not #'is-set final-adds))
                (final-adds (remove-if #'is-set 
                                (remove-if #'is-assign 
                                    (remove-if #'is-increase 
                                        (remove-if #'is-decrease 
                                            (remove-if #'is-create 
                                                final-adds)))))
                )
               )
              (when (equal '(()) final-adds) (setq final-adds '()))
              ;; process PROTECTIONS generated by this operator
              

              (dolist (d final-dels)
                  (if (eq (car d) :protection)
                      (setq protections1 
                          (delete-protection 
                              protections1 (second d) depth (first head-subbed) state
                          )
                      )
                      (delete-atom-from-state d state depth (first head-subbed))
                  )
              )

                  (dolist (a final-adds)
                  (unless (eq (car a) :protection)
                      ;; added this error-checking.  I can't think of a case where
                      ;; it's ok to add a non-ground literal to the
                      ;; state. [2004/02/17:rpg]
                      (unless (groundp a)
                          (error 
                              "Attempting to add non-ground literal ~S to state."
                              a
                          )
                      )
                      (add-atom-to-state a state depth (first head-subbed))
                  )
              )

                  (dolist (a decreases)
                  ;; added this error-checking.  I can't think of a case where
                  ;; it's ok to add a non-ground literal to the
                  ;; state. [2004/02/17:rpg]
                  (unless (groundp a)
                      (error 
                          "Attempting to add non-ground literal ~S to state."
                          a
                      )
                  )
                  (set-state-value 
                      state (car (second a)) (cdr (second a))
                      (- (get-state-value (second a) state)
                          (state-eval state (third a))
                      )
                  )
                  ;(eval `(setf ,(cadr a) (- ,(cadr a) ,(caddr a))))
              )
              
                  (dolist (a increases)
                  ;; added this error-checking.  I can't think of a case where
                  ;; it's ok to add a non-ground literal to the
                  ;; state. [2004/02/17:rpg]
                  (unless (groundp a)
                      (error 
                          "Attempting to add non-ground literal ~S to state."
                          a
                      )
                  )
                  (set-state-value 
                      state (car (second a)) (cdr (second a))
                      (+ (get-state-value (second a) state)
                          (state-eval state (third a))
                      )
                  )
                  ;(eval `(setf ,(cadr a) (+ ,(cadr a) ,(caddr a))))
              )

                  (dolist (a sets)
                  ;; added this error-checking.  I can't think of a case where
                  ;; it's ok to add a non-ground literal to the
                  ;; state. [2004/02/17:rpg]
                  (unless (groundp a)
                      (error 
                          "Attempting to add non-ground literal ~S to state."
                          a
                      )
                  )
                  (set-state-value 
                      state (car (second a)) (cdr (second a))
                      (if (listp (third a)) (state-eval state (third a)) (third a))
                  )
                  ;(eval `(setf ,(cadr a) ,(caddr a)))
              )

              (cond
                  ((protection-ok state protections1 head-subbed) 
                      (setq protections protections1)
                  )
                  (t
                      (retract-state-changes state statetag)
                      ;; I don't understand why we need to return more than one
                      ;; value here. [2006/07/31:rpg]
                      (setq *cur-head* nil)
                      (return-from apply-pddl-effects (values 'fail 'fail protections 0))
                  )
              )

              ;; protections just added are not checked immediately...
              (dolist (a final-adds)
                  (when (eql (car a) :protection)
                      (setq protections 
                          (add-protection 
                              protections (second a) 
                              depth (first head-subbed) state
                          )
                      )
                  )
              )

              (trace-print :operators (first head-subbed) state "~&PDDL effects for ~A successfully applied." head-subbed)

              (setq *cur-head* nil)
              (values 
                  head-subbed statetag 
                  protections
              )
          )
      )
)    

;;;---------------------------------------------------------------------------
;;; Helpers for apply-action
;;;---------------------------------------------------------------------------
(defun extract-adds-and-deletes (effect-expr state)
  "This function is intended for use on a PDDL :effect expression.
It partitions the effects into adds and deletes and returns these
two values."
  (case (first effect-expr)
    (and
     (loop for effect in (rest effect-expr)
           with add and delete
           do (multiple-value-setq (add delete)
                  (extract-adds-and-deletes effect state))
           when add
             append add into recursive-adds
           when delete
             append delete into recursive-deletes
           finally (return (values recursive-adds recursive-deletes))))
    (not
     ;; add something to deletes
     (values nil (list (second effect-expr))))
    (forall
     (destructuring-bind (forall vars restr consequent)
         effect-expr
       (declare (ignore forall vars))
       (let ((unifiers
              (shopthpr:find-satisfiers restr state)))
         (when unifiers
           (loop for unifier in unifiers
                 with new-adds and new-deletes
                 do (multiple-value-setq (new-adds new-deletes)
                        (extract-adds-and-deletes
                         (apply-substitution consequent unifier)
                         state))
                 append new-adds into adds
                 append new-deletes into dels
                 finally (return (values adds dels)))))))
    (when
        (destructuring-bind (when antecedent consequent)
            effect-expr
          (declare (ignore when))
          (let ((result (shopthpr:find-satisfiers antecedent state t)))
            (when result
              (let ((unifier (first result)))
                (multiple-value-bind (new-adds new-deletes)
                    (extract-adds-and-deletes
                     (apply-substitution consequent unifier)
                     state)
                  (values new-adds new-deletes)))))))
    (otherwise                                ;includes :protection
     ;; normal expression
     (values (list effect-expr) nil))))
        

(defmethod validator-export ((domain simple-pddl-domain) (plan list) stream)
  ;; first check to see if there are costs in the plan...
  (when (numberp (second plan))
    (setf plan (remove-costs plan)))
  (flet ((de-shopify (list)
           (cons
            ;; now the first element will be a list, which isn't
            ;; entirely desirable, but helps us avoid package
            ;; issues. [2007/07/17:rpg]
            (subseq (symbol-name (first list)) 1)
            (rest list))))
    (let ((massaged-plan
           (mapcar #'de-shopify
            (remove-if #'internal-operator-p plan :key 'first))))
      (loop for x in massaged-plan
            as i from 0
            do (format stream "~d: ~a~%" i x)))))
