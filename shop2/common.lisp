;;; The Original Code is SHOP2.  
;;; 
;;; The Initial Developer of the Original Code is the University of
;;; Maryland. Portions created by the Initial Developer are Copyright (C)
;;; 2002,2003 the Initial Developer. All Rights Reserved.
;;;
;;; Additional developments made by Robert P. Goldman, John Maraist.
;;; Portions created by Drs. Goldman and Maraist are Copyright (C)
;;; 2004-2007 SIFT, LLC.
;;;
;;; Smart Information Flow Technologies Copyright 2006-2007 Unpublished work
;;; 
;;; GOVERNMENT PURPOSE RIGHTS
;;; 
;;; Contract No.         FA8650-06-C-7606, 
;;; Contractor Name      Smart Information Flow Technologies, LLC
;;;                      d/b/a SIFT, LLC
;;; Contractor Address   211 N 1st Street, Suite 300
;;;                      Minneapolis, MN 55401
;;; Expiration Date      5/2/2011
;;; 
;;; The Government's rights to use, modify, reproduce, release,
;;; perform, display, or disclose this software are restricted by
;;; paragraph (b)(2) of the Rights in Noncommercial Computer Software
;;; and Noncommercial Computer Software Documentation clause contained
;;; in the above identified contract. No restrictions apply after the
;;; expiration date shown above. Any reproduction of the software or
;;; portions thereof marked with this legend must also reproduce the
;;; markings.


 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE. 
 
 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; (at your option) any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :shop2.common)

(defmacro shop-fail () `'fail)

(defclass domain ()
     ((domain-name
       :initarg :domain-name
       :reader domain-name
       :initarg :name
       )
      (methods
       :initarg :methods
       :reader domain-methods
       )
      (operators
       :initarg :operators
       :initform nil
       :reader domain-operators
       )
      (axioms
       :initarg :axioms
       :reader domain-axioms
       ))
  (:documentation "An object representing a SHOP2 domain.")
  )

(defmethod print-object ((x domain) stream)
  (unless *print-readably*
    (format stream "<DOMAIN: ~A>" (domain-name x))))

(defgeneric methods (domain task-name)
  (:documentation "Return a list of all the SHOP2
methods for TASK-NAME in DOMAIN."))

(defgeneric operator (domain task-name)
  (:documentation "Return the SHOP2 operator (if any) 
defined for TASK-NAME in DOMAIN."))

(defgeneric axioms (domain predicate)
  (:documentation "Return a list of all the SHOP2
axioms for PREDICATE in DOMAIN."))

(defvar *domain*)		; domain object, containing domain attributes, and
                                ; useful for subclassing behaviors

;;; the following have been absorbed into being slots of the DOMAIN
;;; objects.  See definition of DOMAIN object. [2006/07/05:rpg]
;;;(defvar *methods*)                  ; methods in the current planning domain
;;;(defvar *operators*)                ; operators in the current planning domain
;;;(defvar *axioms*)                   ; axioms in the current planning domain

(defparameter *current-state* nil) ; current state (for find-satisfiers)

(defparameter *cur-head* nil) ; for provenance reporting

(defparameter *state-encoding* :mixed) ; current encoding of states

(defparameter *inferences* 0)       ; number of logical inferences so far

(defparameter *external-access* nil)  ; whether to access external data

(defparameter *attribution-list* nil) ; sources of facts from external access

;;; For numeric inequalities, this constant provides bounds for equality to 
;;; allow for rounding errors; therefore, the following equations apply:
;;; x <= y iff ~x <= ~y + *rounding-precision-constant*
;;; x >= y iff ~x >= ~y - *rounding-precision-constant*
;;; x <  y iff ~x <  ~y - *rounding-precision-constant*
;;; x >  y iff ~x >  ~y + *rounding-precision-constant*
(defvar *rounding-precision-constant* .0001)

(defmacro call (fn &rest params)
  `(funcall (function ,fn) ,.params))
