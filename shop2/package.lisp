;;; The Original Code is SHOP2.  
;;; 
;;; The Initial Developer of the Original Code is the University of
;;; Maryland. Portions created by the Initial Developer are Copyright (C)
;;; 2002,2003 the Initial Developer. All Rights Reserved.
;;;
;;; Additional developments made by Robert P. Goldman, John Maraist.
;;; Portions created by Drs. Goldman and Maraist are Copyright (C)
;;; 2004-2007 SIFT, LLC.
;;; 

;;; Smart Information Flow Technologies Copyright 2006-2007 Unpublished work
;;; 
;;; GOVERNMENT PURPOSE RIGHTS
;;; 
;;; Contract No.         FA8650-06-C-7606, 
;;; Contractor Name      Smart Information Flow Technologies, LLC
;;;                      d/b/a SIFT, LLC
;;; Contractor Address   211 N 1st Street, Suite 300
;;;                      Minneapolis, MN 55401
;;; Expiration Date      5/2/2011
;;; 
;;; The Government's rights to use, modify, reproduce, release,
;;; perform, display, or disclose this software are restricted by
;;; paragraph (b)(2) of the Rights in Noncommercial Computer Software
;;; and Noncommercial Computer Software Documentation clause contained
;;; in the above identified contract. No restrictions apply after the
;;; expiration date shown above. Any reproduction of the software or
;;; portions thereof marked with this legend must also reproduce the
;;; markings.


 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE. 
 
 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; (at your option) any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :common-lisp-user)

(defpackage :shop2
    (:nicknames :shop)
    (:use :common-lisp :shop2.unifier :shop2.common :shop2.theorem-prover)
    #+sbcl
    (:shadow #:defconstant)
    (:export #:shop-fail #:call

             ;; The following set of symbols is defined in
             ;; shop-common, but are replayed for one export by shop2.
             ;; Packages :use'ing shop2 should *not* also :use
             ;; shop-common.

             #:domain #:domain-axioms #:domain-name #:domain-types
             #:domain-operators #:domain-methods
             #:axioms #:operators #:methods #:name
             
             #:*domain* #:*current-state* #:*inferences* #:*external-access*
             #:*attribution-list* #:*state-encoding*
             
             #:state-candidate-atoms-for-goal #:state-atoms
             #:list-state-values
             #:list-state-values-as-atoms
             #:state-all-atoms-for-predicate
             #:copy-state
             #:retract-state-changes
             #:add-atom-to-state
             #:delete-atom-from-state
             #:tag-state
             #:state-eval
             #:make-state
             #:set-state-value
             #:get-state-value
             #:add-object-to-state
             #:remove-object-from-state
             #:get-object-type
             #:is-assignable-from

             ;; The following set of symbols is defined in
             ;; shop-unifier, but are replayed for one export by
             ;; shop2.  Packages :use'ing shop2 should *not* also :use
             ;; shop-unifier.
             #:trace-print
             #:*shop-trace* #:*shop-trace-stream* #:*trace-query*
             
             #:unify #:standardize #:fail
             #:apply-substitution #:compose-substitutions
             #:fix-uninterned-bindings #:binding-list-value
             #:make-binding-list #:make-binding
             #:variablep #:groundp
             #:extract-variables #:shop-union
             #:get-alist
             
             ;; The following set of symbols is defined in
             ;; shop-theorem-prover, but are replayed for one export
             ;; by shop2.  Packages :use'ing shop2 should *not* also
             ;; :use shop-theorem-prover
             #:explain-satisfier #:find-satisfiers
             #:extract-variables
             
             #:call
             #:imply
             #:forall
             #:exists
             #:neq
             #:maybe
             #:is
             #:create
             #:assign
             #:assign*                        ;possibly non-canonical addition
             ;; end of shop-unifier stuff
             
             #:defproblem
             #:make-problem
             #:delete-problem
             #:def-problem-set
             #:make-problem-set
             #:find-plans
             #:do-problems
             #:shop-trace
             #:shop-untrace

             ;; domain objects and their functions...
             #:defdomain
             #:set-domain
             #:find-domain

             ;; default planner search setting --- should be replaced
             ;; by subclassing a planner object later.
             #:*which*

             ;; hooks
             #:plan-found-hook
             #:trace-query-hook
             #:external-access-hook

             ;; This function prints a list of the axioms for the
             ;; domain whose name is name defaults to the most
             ;; recently defined domain.
             #:print-axioms 
             ;; This function prints a list of the operators for the domain whose
             ;; name is name; defaults to the most recently defined domain.
             #:print-operators

             ;; This function prints a list of the methods for the
             ;; domain whose name is name; defaults to the most
             ;; recently defined domain.
             #:print-methods

             ;; accessors to problems and domains...
             #:get-state
             #:get-tasks
             #:get-problems

             ;; get rid of cost information out of a plan tree...
             #:remove-costs

             ;; classes and methods to be specialized
             #:domain
             #:task-sorter
             #:process-operator
             #:process-axiom
             #:process-method
             #:handle-domain-options
             #:parse-domain-items
             #:parse-domain-item

             #:pddl-domain

             ;; ENQ-related methods
             #:apply-operator
             #:extrapolate-state
             #:seek-plans-primitive
             #:seek-plans-null

             ;; tree-accessors
             #:complex-node-p
             #:complex-node-task
             #:complex-node-children
             #:primitive-node-p
             #:remove-internal-operators
             #:tree-node-task
             #:tree-node-task-name
             #:task-name
             #:find-complex-node-if
             #:find-all-complex-node-if
             #:find-complex-node-for-task
             #:find-all-complex-node-for-task

             ;; conditions
             #:no-method-for-task

             ;; things you might want to use in your domain definitions
             #:variablep
             
             ;; other utilities
             #:shop-union

             ;; exporting for val
             #:validator-export
             #:write-pddl-plan
             
             #:replan
             #:!replan
             #:wait-for
             #:!wait-for
             #:wait
             #:!wait
             #:!!inop
             ))

(defpackage :shop2-user
    (:nicknames :shop-user)
    (:use :shop2 :common-lisp)
    (:export #:explain-satisfier
             #:find-satisfiers))


