;;; The Original Code is SHOP2.  
;;; 
;;; The Initial Developer of the Original Code is the University of
;;; Maryland. Portions created by the Initial Developer are Copyright (C)
;;; 2002,2003 the Initial Developer. All Rights Reserved.
;;;
;;; Additional developments made by Robert P. Goldman, John Maraist.
;;; Portions created by Drs. Goldman and Maraist are Copyright (C)
;;; 2004-2007 SIFT, LLC. 

;;; Smart Information Flow Technologies Copyright 2006-2007 Unpublished work
;;; 
;;; GOVERNMENT PURPOSE RIGHTS
;;; 
;;; Contract No.         FA8650-06-C-7606, 
;;; Contractor Name      Smart Information Flow Technologies, LLC
;;;                      d/b/a SIFT, LLC
;;; Contractor Address   211 N 1st Street, Suite 300
;;;                      Minneapolis, MN 55401
;;; Expiration Date      5/2/2011
;;; 
;;; The Government's rights to use, modify, reproduce, release,
;;; perform, display, or disclose this software are restricted by
;;; paragraph (b)(2) of the Rights in Noncommercial Computer Software
;;; and Noncommercial Computer Software Documentation clause contained
;;; in the above identified contract. No restrictions apply after the
;;; expiration date shown above. Any reproduction of the software or
;;; portions thereof marked with this legend must also reproduce the
;;; markings.


 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE. 
 
 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; (at your option) any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

 (in-package :shop2)

;;; ------------------------------------------------------------------------
;;; Method and operator application
;;; modified to use the new SHOP2 operator syntax, which includes preconditions (swm)
;;; ------------------------------------------------------------------------

(defstruct (operator :named (:type list))
  "This structure definition was added in order to make the 
access to operator attributes self-documenting.  Later, the list
structure could be removed, and a true struct could be used instead."
  (head nil :type list)                ;this is the operator expression itself...
  preconditions
  deletions
  additions
  (cost-fun nil))

(defun extrapolate-state (domain st instantiated-operator &optional (hidden-facts nil) (fail-outcome 'fail))
  (let 
    ((op (operator domain (car instantiated-operator)))
     (*domain* domain)
     (curr-state *current-state*)
     (result nil)
    )
        
    (setq *current-state* (copy-state st))
    (dolist (fact hidden-facts)
        (add-atom-to-state fact st 0 'hidden-fact)
    )
    (when (null op)
        (error "No operator named ~A was found!" (car instantiated-operator))
    )
    
    (setq result
        (etypecase op
           (operator 
              (apply-operator domain st instantiated-operator op nil 0 nil)
           )
          ;; pddl action
           (pddl-action 
              (apply-action st instantiated-operator op nil 0 nil)
           )
        )
    )
    (setq st *current-state* *current-state* curr-state)
    (dolist (fact hidden-facts)
        (delete-atom-from-state fact st 0 'hidden-fact)
    )
    (if (eq result 'fail)
        ;then
        (case fail-outcome
            (error  
                (error "Action ~A could not be executed, probably because its preconditions were not met."
                    instantiated-operator
                )
            ) 
            (break  
                (break "Action ~A could not be executed, probably because its preconditions were not met."
                    instantiated-operator
                )
                'fail
            ) 
            (otherwise fail-outcome)
        )
        ;else
        st
    )
  )
)

(defmethod apply-operator ((domain domain) state task-body operator protections depth
                             in-unifier)
  "If OPERATOR is applicable to TASK in STATE, then APPLY-OPERATOR returns
five values:
1.  the operator as applied, with all bindings;
2.  a state tag, for backtracking purposes;
3.  a new set of protections;
4.  the cost of the new operator;
5.  unifier.
This function also DESTRUCTIVELY MODIFIES its STATE argument.
Otherwise it returns FAIL."
  (let* ((standardized-operator (standardize operator))
         (head (operator-head standardized-operator))
         (preconditions (operator-preconditions standardized-operator))
         (deletions (operator-deletions standardized-operator))
         (additions (operator-additions standardized-operator))
         operator-unifier pre protections1 tempdel tempadd statetag
         head-subbed dels-subbed adds-subbed pu unifier
         cost-value cost-number)

    ;; added rudimentary arity-checking...
    (unless (= (length task-body) (length head))
      (cerror "Continue (operator application will fail)"
              "Arity of operator in the plan library, ~D~%~T~S~%does not match task, ~D~%~T~S"
              (length head)
              head
              (length task-body)
              task-body))

    ;; new tracing facility for debugging
    (when *traced-tasks*
      (when (member (first head) *traced-tasks*)
        (break "Applying operator for ~A~%~S" (first head) task-body)))

    (setq operator-unifier (unify head (apply-substitution task-body in-unifier)))
    
    (cond
     ((eql operator-unifier 'fail) (values 'fail protections 0))
     (t
      (setf operator-unifier
            (compose-substitutions operator-unifier in-unifier))
      ;; first check the preconditions, if any
      (when preconditions
        (setq pre (apply-substitution preconditions operator-unifier))
        (setq pu (shopthpr:find-satisfiers pre state t))
        (unless pu
          (trace-print :operators (first head) state
                       "~2%Depth ~s, inapplicable operator ~s~%     task ~s.~%     Precondition failed: ~s.~%"
                       depth
                       (first head)
                       (apply-substitution task-body unifier)
                       pre
                       )
          (return-from apply-operator (values 'fail preconditions 0))))
      (setq unifier (compose-substitutions operator-unifier (car pu)))
      (setq dels-subbed (apply-substitution deletions unifier))
      (setq adds-subbed (apply-substitution additions unifier))
      (setq head-subbed (apply-substitution head unifier))
      ;; added this to update the tree [2003/06/25:rpg]
      ;(setq *current-tree* (apply-substitution *current-tree* unifier))
                                        ;(format t "~%Sat to explain: ~s" (apply-substitution preconditions unifier))

      ;; at this point UNIFIER is bound to the results of unifying
      ;; TASK-BODY with the operator's head and then retrieving
      ;; the first set of bindings that satisfy the preconditions.
      ;; we will return this unifier later on, assuming that
      ;; bindings from add and delete lists should not be plugged
      ;; in. [2004/01/20:rpg]
      (when *explanation*
          (setq head-subbed `(,@(cons (first head-subbed)
                                      (mapcar #'list
                                              (rest (second operator))
                                              (rest head-subbed)))
                                :explanation
                                ,(shopthpr:explain-satisfier
                                  (apply-substitution preconditions unifier)
                                  state))))
      (trace-print :operators (first head) state
                   "~2%Depth ~s, applying operator ~s~%      task ~s~%       del ~s~%       add ~s"
                   depth
                   (first head)
                   (apply-substitution task-body unifier)
                   dels-subbed
                   adds-subbed)
      (setq cost-value
        (eval (apply-substitution
               (operator-cost-fun standardized-operator) unifier)))
      (setq cost-number (if (numberp cost-value) cost-value 1))
      ;; process DELETE list
      (dolist (d dels-subbed)
        (unless (eql 'fail d)
          (if (eql (car d) 'forall)
              (let ((bounds (third d)) (dels (fourth d)) mgu2 tempd)
                (setq mgu2 (shopthpr:find-satisfiers
                            bounds state))
                (dolist (m2 mgu2)
                  (setq tempd (apply-substitution dels m2))
                  (dolist (d1 tempd)
                    (setq tempdel
                      (shop-union (list d1) tempdel :test #'equal)))))
            (setq tempdel (shop-union (list d) tempdel :test #'equal)))))
      ;; process ADD list
      (dolist (a adds-subbed)
        (unless (eql 'fail a)
          (if (eql (car a) 'forall)
              (let ((bounds (third a)) (adds (fourth a)) mgu2 tempa)
                (setq mgu2 (shopthpr:find-satisfiers
                            bounds state))
                (dolist (m2 mgu2)
                  (setq tempa (apply-substitution adds m2))
                  (dolist (a1 tempa)
                    (setq tempadd
                      (shop-union (list a1) tempadd :test #'equal)))))
            (setq tempadd (shop-union (list a) tempadd :test #'equal)))))

      (setq protections1 protections)               
      (setq statetag (tag-state state))
      ;; process PROTECTIONS generated by this operator
      (dolist (d tempdel)
        (if (eql (car d) :protection)
            (setq protections1 
              (delete-protection 
               protections1 (second d) depth (first head) state))
          (delete-atom-from-state d state depth (first head))))

      (dolist (a tempadd)
        (unless (eql (car a) :protection)
          ;; added this error-checking.  I can't think of a case where
          ;; it's ok to add a non-ground literal to the
          ;; state. [2004/02/17:rpg]
          (unless (groundp a)
                 (error "Attempting to add non-ground literal ~S to state."
                        a))
          (add-atom-to-state a state depth (first head))))

      (if (protection-ok state protections1 head) 
          ;then
          (setq protections protections1)
          ;else
        (progn
          (retract-state-changes state statetag)
          (return-from apply-operator (values 'fail 'fail protections 0))))

      (dolist (a tempadd)
        (when (eql (car a) :protection)
          (setq protections 
            (add-protection protections (second a) 
                            depth (first head) state))))

      (trace-print :operators operator state "~&Operator successfully applied.")

      (values head-subbed statetag 
              protections cost-number
              unifier)))))

;;; if the state has everything that the protections list has, then 
;;; return true, else return nil
(defun protection-ok (state protections head)
  (dolist (p protections)
    (unless (shopthpr:find-satisfiers
             (car p) state t)
      (trace-print :operators (first head) state
                   "~%Backtracking because operator ~s~%  violated the protected condition ~s"
                   (first head) (car p))
      (backtrack "Protection violation ~S" (car p))
      (return-from protection-ok nil)))
  t)

;;; increase the count for PROTECT in the protection list
(defun add-protection (protections protect depth operator state)
  (let (p)
    (dolist (d protections)
      (if (equal protect (car d))
        (return (setq p d))))
    (cond ((null p)
           (trace-print :protections (car protect) state
                        "~2%Depth ~s, incrementing protection count to ~s~%      atom ~s~%  operator ~s"
                        depth 1 protect operator)
           (setq protections (cons (list protect 1) protections)))
          (t 
           (setq protections (remove p protections))
           (setq p (cons (car p) (list (+ 1 (car (cdr p))))))
           (trace-print :protections (car protect) state
                        "~2%Depth ~s, incrementing protection count to ~s~%      atom ~s~%  operator ~s"
                        depth (cadr p) protect operator)
           (setq protections (cons p protections))))))

;;; decrease the count for PROTECT in the protection list
(defun delete-protection (protections protect depth operator state)
  (let (p)
    (dolist (d protections)
      (if (equal protect (car d))
        (return (setq p d))))
    (if (null p)
      (trace-print :protections (car protect) state
                   "~2%Depth ~s, protection count not decremented since it is already 0~%      atom ~s~%  operator ~s"
                   depth protect operator)
    (progn 
      (setq protections (remove p protections))
      (if (eql 1 (car (cdr p)))
        protections
        (progn 
          (setq p (cons (car p) (list (- (car (cdr p)) 1))))
          (trace-print :protections (car protect) state
                        "~2%Depth ~s, decrementing protection count to ~s~%      atom ~s~%  operator ~s"
                        depth (cadr p) protect operator)
          (setq protections (cons p protections))))))))

;;; If METHOD is applicable to TASK in STATE, then APPLY-METHOD returns the
;;; resulting list of reductions.  Otherwise it returns NIL.
(defmethod apply-method ((domain domain) state task-body method protections depth
                           in-unifier)
  (declare (ignore protections))  ; do we really want to ignore protections?
  (let ((standardized-method (standardize method))  
        task-unifier state-unifiers pre tail)

    (unless (= (length (second standardized-method))
               (length task-body))
      (error "Arity mismatch between task to plan and method:~%Task: ~S~%Method header: ~S"
             task-body (second standardized-method)))

    ;; see if the standardized-method's head unifies with TASK-BODY
    (setq task-unifier (unify (second standardized-method)
                              (apply-substitution task-body in-unifier)))
    (when *traced-tasks*
      (when (member (first task-body) *traced-tasks*)
        (break "Attempting to apply a method for ~A:~%~S"
               (first task-body) task-body)))

    (unless (eql task-unifier 'fail)
      (setq task-unifier (compose-substitutions in-unifier task-unifier))
      ;; STANDARDIZED-METHOD's CDDR is a list 
      ;; (label_1 pre_1 d_1 label_2 pre_2 d_2 ...) which acts like an
      ;; if-then-else: we look for the first true pre_i, and then evaluate d_i
      (do* ((body (cddr standardized-method) (cdddr body)))
           ((null body) nil)

        ;; apply v to PRE and TAIL
        (setq pre (apply-substitution (second body) task-unifier))
        (setq tail (apply-substitution (third body) task-unifier))

        ;; check for tracing
        (when *traced-methods*
          (when (member (first body) *traced-methods*)
            (break "Attempting to apply method ~A" (first body))))

        (trace-print :methods (first body) state
                           "~2%Depth ~s, trying method ~s~%      for task ~s~%"
                           depth
                           (first body)
                           task-body)
        
        ;; find all matches to the current state
        (setq state-unifiers (shopthpr:find-satisfiers
                              pre state))

        (if state-unifiers
            (let* ((answers-with-duplicates
                    (mapcan
                     #'(lambda (unifier)
                         (let ((unifier (compose-substitutions (copy-tree unifier) task-unifier)))
                           (mapcar
                            #'(lambda (reduction)
                                (cons 
                                 (cons (first body) reduction)
                                 ;;keep the unifier around a bit longer...
                                 ;; [2003/06/25:rpg]
                                 unifier))
                            (force-immediate-reduction 
                             (eval (apply-substitution tail unifier))))))
                     state-unifiers))
                    (answers-and-unifiers
                        (lexico-sort
                (remove-duplicates answers-with-duplicates
                           ;; added this to ignore the unifiers....
                           :key #'car
                           :test #'equal :from-end t)))
                    (answers (mapcar #'car answers-and-unifiers))
                    (unifiers (mapcar #'cdr answers-and-unifiers)))
              (trace-print :methods (first body) state
                           "~2%Depth ~s, applicable method ~s~%      task ~s~%reductions ~s"
                           depth
                           (first body)
                           task-body
                           answers)
              (return-from apply-method (values answers unifiers)))
          (trace-print :methods (first body) state
                     "~2%Depth ~s, inapplicable method ~s~%      task ~s"
                     depth
                     (first body)
                     task-body))))))

(defun lexico-sort (any-lst)
    (when (null (cdr any-lst))
        (return-from lexico-sort any-lst)
    )
    (let ((pre-sort-list (pairlis (mapcar #'write-to-string any-lst) any-lst)))
        (mapcar #'cdr
            (sort 
                (copy-list pre-sort-list)
                #'string-lessp 
                :key #'car
            )
        )
    )
)

;;; This function forces there to be at least one immediate task in any
;;;  reduction so that when a method is reduced, it is immediately
;;;  reduced all the way to the bottom level.  For ordered reductions,
;;;  the function makes the first task immediate.  For unordered
;;;  reductions which have no immediate element, it splits the reduction
;;;  into seperate reductions each of which have exactly one immediate
;;;  element UNLESS the reduction already has an immediate element.  For
;;;  nested reductions, it applies the appropriate fix at each level.
;;;
;;; Examples:
;;;  (force-immediate ; non-nested
;;;    '((:ordered (:task a) (:task b))
;;;      (:unordered (:task x) (:task y))))
;;;  =>
;;;   ((:ordered (:task :immediate a) (:task b))
;;;    (:unordered (:task :immediate x) (:task y))
;;;    (:unordered (:task x) (:task :immediate y)))
;;;----
;;;  (force-immediate ; nested
;;;    '((:unordered (:ordered (:task a) (:task b))
;;;                  (:ordered (:task x) (:task y))))
;;;  =>
;;;   ((:unordered (:ordered (:task :immediate a) (:task b))
;;;                (:ordered (:task x) (:task y)))
;;;    (:unordered (:ordered (:task a) (:task b))
;;;                (:ordered (:task :immediate x) (:task y))))
;;;----
;;;  (force-immediate ; nested, but already immediate
;;;    '((:unordered (:ordered (:task a) (:task b))
;;;                  (:ordered (:task :immediate x) (:task y))))
;;;  =>
;;;   ((:unordered (:ordered (:task a) (:task b))
;;;                (:ordered (:task :immediate x) (:task y))))
(defun force-immediate (reductions)
  (mapcan #'force-immediate-reduction reductions))

(defun force-immediate-reduction (reduction)
  (cond
   ((null reduction) nil)
   ((already-immediate-p reduction)
    (list reduction))
   ((eq (first reduction) :task)
    (list `(:task :immediate ,@(get-task-body reduction))))
   ((eq (first reduction) :ordered)
    (mapcar #'(lambda (ordered-term)
    (append (list :ordered ordered-term)
      (rest (rest reduction))))
      (force-immediate-reduction (second reduction))))
   ((eq (first reduction) :unordered)
    (mapcar #'(lambda (unordered-result)
    (cons :unordered unordered-result))
      (force-immediate-unordered (rest reduction))))
   ((eq (first reduction) :simultaneously)
    (mapcar #'(lambda (unordered-result)
    (cons :simultaneously unordered-result))
      (force-immediate-unordered (rest reduction))))
  )
)
  
(defun already-immediate-p (reduction)
  (cond
   ((null reduction) nil)
   ((eq (first reduction) :task)
    (eq (second reduction) :immediate))
   ((eq (first reduction) :ordered)
    (already-immediate-p (second reduction)))
   ((eq (first reduction) :unordered)
    (find-if #'already-immediate-p (rest reduction)))
   ((eq (first reduction) :simultaneously)
    (find-if #'already-immediate-p (rest reduction)))
  )
)

(defun force-immediate-unordered (unordered-list &optional previous)
  (if (null unordered-list)
      nil
    (append
     (mapcar #'(lambda (unordered-term)
     `(,@previous
       ,unordered-term
       ,@(rest unordered-list)))
       (force-immediate-reduction (first unordered-list)))
     (force-immediate-unordered
      (rest unordered-list)
      (append previous (list (first unordered-list)))))))

;;; ------------------------------------------------------------------------
;;; Functions for creating and manipulating planning domains and problems
;;; ------------------------------------------------------------------------

;;; MAKE-DOMAIN gives the name NAME to the planning domain whose axioms,
;;; operators, and methods are those in in ITEMS.  More specifically, it
;;; puts the axioms, operators, and methods onto NAME's property list under
;;; the indicators :AXIOMS, :OPERATORS, and :METHODS, respectively

;;; It scares me that we have defdomain and make-domain, they do the
;;; same thing, and there's duplicated code.  This seems like a recipe
;;; for pain in the future.  I believe that defdomain should be
;;; modified to expand to a call to make-domain... [2006/07/05:rpg]
(defun make-domain (name &optional items)
  (warn "MAKE-DOMAIN is an obsolete, deprecated interface.  Please use DEFDOMAIN instead.")
  (when (null items)
    (psetf items name
           name (gentemp (symbol-name '#:domain))))
  ;; name is ignored -- it's there only for compatibility with SHOP 1
  (format t "~%Defining domain ...")
  (let ((domain (make-instance 'domain
                  :name name)))
    (setq items (append '((:operator (!!inop) () () 0)) items))
    (setq items (append '((:operator (!replan ?task) () () 0)) items))
;    (setq items (append '((:action replan :parameters (?task - fact) :precondition () :effect ())) items))
    (parse-domain-items domain items)
    (install-domain domain)
    (setf *domain* domain)))

;;; If the axiom has SHOP 1.x or mixed syntax, regularize-axiom will
;;; return it in SHOP 2 syntax.
(defmethod regularize-axiom ((domain domain) axiom)
  (if (shop2-axiom-p axiom) axiom       ; SHOP2 syntax
    (let ((ax-name (caadr axiom))       ; SHOP1 or mixed syntax
          (branch-counter 0)
          (labelified-axiom (list (car axiom) (cadr axiom)))
          (atail (cddr axiom)))
          (append labelified-axiom 
              (loop until (null atail) do
                    (incf branch-counter)
                    if (listp (car atail))       ; no label on this branch
                    append (list (gensym (format nil "~A~D--"
                                                 ax-name branch-counter))
                                 (pop atail))
                    else                ; this branch has a label
                    append (list (pop atail) (pop atail)))))))

(defun shop2-axiom-p (ax)
  (if (not (listp ax)) nil
      (let ((lax (length ax)))
        (if (<= lax 3) nil (rest-shop2-axiom-p (cddr ax))))))

(defun rest-shop2-axiom-p (ax)
  (let ((lax (length ax)))
    (cond ((< lax 2) nil)
          ((= lax 2) (if (and (atom (car ax)) (listp (cadr ax))) t nil))
          ((= lax 3) nil)
          (t (and (atom (car ax)) (listp (cadr ax)) (rest-shop2-axiom-p (cddr ax)))))))

;;; rewrote the funky boolean under the (atom x) branch to use WHEN
;;; and COND to make it clear it's a conditional, executed for
;;; effect. [2006/07/31:rpg]
(defmethod set-variable-property ((domain domain) x)
  (cond ((atom x)
         (when (symbolp x)
           (cond ((eql (elt (symbol-name x) 0) #\?)
                  (setf (get x 'variable) t))
                 ((eql (elt (symbol-name x) 0) #\!)
                  (setf (get x 'primitive) t)))))
        ((consp x) (set-variable-property domain (car x))
         (set-variable-property domain (cdr x)))
        (t (error "the domain is malformed"))))

;;; MAKE-PROBLEM creates a planning problem named PROBLEM-NAME
;;; by putting STATE and TASK onto PROBLEM-NAME's
;;; property list under the indicators :STATE and :TASKS.
#+allegro (excl::define-simple-parser make-problem second :shop2-problem)

;;; this must be a variable, rather than an optional argument, because
;;; of the unpleasant way make-problem has extra arguments for
;;; backward compatibility. [2005/01/07:rpg]
(defvar *make-problem-silently* nil
  "If this variable is bound to t, make-problem will NOT print a message.")

(defun make-problem (problem-name state tasks &optional extra)
  #+allegro
  (excl:record-source-file problem-name :type :shop2-problem)
  ;; if extra is given, then the args are problem-name, domain-name, state, tasks
  ;; in that case, we want to ignore domain-name
  (when extra
    (setq state tasks)
    (setq tasks extra))
  (unless *make-problem-silently*
    (format t "~%Defining problem ~s ..." problem-name))
  (setf *all-problems* (cons problem-name *all-problems*))
  (setf (get problem-name :state) state)
  ;; I don't know why this wasn't in here --- AFAICT it still should be [2007/03/14:rpg]
  (setf (get problem-name :tasks) (process-task-list tasks))
  ;; uck --- this won't work any more because we aren't guaranteed to
  ;; have a domain here.  Will move to find-plans... [2007/03/14:rpg]
  ;; (set-variable-property *domain* tasks)
  problem-name)

(defun delete-problem (problem-name)
  (setf (get problem-name :state) nil
        (get problem-name :tasks) nil)
  (setf *all-problems* (delete problem-name *all-problems*))
  problem-name)

;;;---------------------------------------------------------------------------
;;; I have added these two accessors to make it easier to modify the
;;; implementation of SHOP2 problems, should we like to do it.  I
;;; suggest that we use these instead of (get <problem-name> :state)
;;; and (get <problem-name> :tasks) [2004/10/27:rpg]
;;;---------------------------------------------------------------------------

(defun problem-state (problem)
  (get problem :state))

(defun problem-tasks (problem)
  (get problem :tasks))

;;; MAKE-PROBLEM-SET gives the name SET-NAME to the problems in PROBLEM-SET.
;;; More specifically, it puts PROBLEM-SET onto PROBLEM-NAME's
;;; property list under the indicators :STATE, :TASKS, and :DOMAIN
(defun make-problem-set (list-name problem-list)
  (format t "~%Defining problem set ~s ..." list-name)
  (setf (get list-name :problems) problem-list))

;;; Get the initial state for the problem named NAME
(defun get-state (name)
  (let ((answer (get name :state 'fail)))
    (if (eq answer 'fail) (error "No initial state for the name ~s" name))
    answer))

;;; Get the task list for the problem named NAME
(defun get-tasks (name)
  (let ((answer (get name :tasks 'fail)))
    (if (eq answer 'fail) (error "No task list for the name ~s" name))
    answer))

;;; Get the list of problems for the problem set named NAME
(defun get-problems (name &key print)
  (let ((answer (get name :problems 'fail)))
    (cond ((eq answer 'fail) (error "No problem list for the name ~s" name))
          (print (format t "~%~s" answer)))
    answer))

;;; DO-PROBLEMS runs FIND-PLANS on each problem in PROBLEMS, which may be
;;; either a problem-set name or a list of problems
(defun do-problems (problems &rest keywords)
  (if (not (listp problems))    ; treat NIL as an empty list, not a problem name
    (setq problems (get-problems problems)))
  (dolist (problem problems)
    (apply #'find-plans (cons problem keywords))))

;;; get-immediate-list returns the first member in tasklist tl that has the
;;; first task with the keyword :immediate
;;; Furthermore, all iNOP's are considered immediate, because they don't do
;;; anything so you might as well run them right away (this is just a minor
;;; efficiency tweak if you have a domain with lots of unordered tasks which
;;; reduce to iNOP's).  - Murdock 12/01
(defun get-immediate-list (tl)
  (dolist (obj tl)
    (if (or (eq :immediate (second obj)) (eq '!!inop (second obj))) 
      (return-from get-immediate-list (list obj))))
  (return-from get-immediate-list nil))

;;; SEEK-PLANS is the basic planning engine.  Here are its arguments:
;;;  - STATE is the current state;
;;;  - TASKS is the list of remaining tasks to accomplish;
;;;  - TOP-TASKS is the agenda of tasks to be worked first, 
;;;    based on the :immediate keyword
;;;  - PARTIAL-PLAN is the partial plan accumulated so far, with the
;;;    steps listed in reverse order;
;;;  - PARTIAL-PLAN-COST is the total cost of the partial plan
;;;  - DEPTH is the current level of recursive calls to SEEK-PLANS;
;;;  - WHICH-PLANS has the same meaning as the WHICH argument to FIND-PLANS
;;; Rather than returning the plans, SEEK-PLANS puts them into the dynamically
;;; scoped variable *PLANS-FOUND*.  Ordinarily I'd consider this to be poor
;;; coding style, but in this case it makes the coding significantly cleaner.
;;;
;;; Seek-plans has been broken down into the following lower level
;;; functions, below: seek-plans-task, seek-plans-primitive,
;;;                   seek-plans-nonprimitive, seek-plans-null

(defmethod seek-plans ((domain domain) state tasks top-tasks partial-plan partial-plan-cost
                         depth which-plans protections
                         unifier)
;  (format t "~%Current task list: ~A~%" tasks)
  (when (time-expired-p)
    (if *print-stats* (format t "~%Terminating because the time limit has expired."))
    (throw *internal-time-tag* nil))

  (setq *current-plan* partial-plan)
  (setq *current-tasks* tasks)
  ;; moved this setf here, to try to get access to the state earlier.
  (setf *current-state* state)
  (setq *expansions* (1+ *expansions*))

  (cond
    ;; if no top-tasks to accomplish, then we have an answer
    ((or (null top-tasks) (equal top-tasks '(NIL)))
     (seek-plans-null domain state which-plans partial-plan partial-plan-cost depth
                      unifier))
    
    ;; else if we've hit the depth bound, then fail
    ((and *depth-cutoff* (>= depth *depth-cutoff*)))

    ;; else look at the agenda
    (t
     (let ((immediate-tasks (get-immediate-list top-tasks)))
       (if immediate-tasks
           (let ((task1 (choose-immediate-task immediate-tasks unifier)))
             ;; in temporal planning we may be unable to choose a
             ;; schedulable immediate task, in which case we must fail
             ;; and backtrack. [2004/04/05:rpg]
             (when task1
               (seek-plans-task domain
                task1 state tasks top-tasks partial-plan partial-plan-cost
                depth which-plans protections
                                        ;tree
                unifier))
             ;; rewrote this as if-then-else, to make it clearer...
             (when (and *plans-found* 
                        (or (eq which-plans :first)
                            (>= (length *plans-found*) *max-plans*)
                        )
                        (not (optimize-continue-p which-plans)))
               (return-from seek-plans nil))
             (cond (task1
                    
                    (trace-print :tasks (get-task-name task1) state
                                 "~2%Depth ~s, backtracking from task~%      task ~s"
                                 depth
                                 task1)
                    (backtrack "Task ~S failed" task1))
                   (t
                    (trace-print :tasks (get-task-name task1) state
                                 "~2%Depth ~s, unable to choose task from immediate task-list, backtracking~%"
                                 depth)
                    (backtrack "Couldn't choose from immediate task-list"))
                   ))
           ;; no :IMMEDIATE tasks, so do the rest of the tasks on the agenda
           (if *hand-steer*
               (let ((task1 (user-choose-task top-tasks unifier)))
                 (seek-plans-task domain task1 state tasks top-tasks partial-plan
                                  partial-plan-cost depth which-plans
                                  protections 
                                  unifier)
                 (when (and *plans-found* 
                            (or (eq which-plans :first)
                                (>= (length *plans-found*) *max-plans*)
                            )
                            (not (optimize-continue-p which-plans)))
                   (return-from seek-plans nil))
                 (trace-print :tasks (get-task-name task1) state
                              "~2%Depth ~s, backtracking from task ~s"
                              depth
                              task1)
                 (backtrack "Task ~S failed" task1))
               (loop for task1 in (task-sorter domain top-tasks unifier)
                     while task1
                     do (seek-plans-task domain task1 state tasks top-tasks partial-plan
                             partial-plan-cost depth which-plans
                             protections 
                             unifier)
                     when (and *plans-found* 
                               (or (eq which-plans :first)
                                   (>= (length *plans-found*) *max-plans*)
                               )
                               (not (optimize-continue-p which-plans)))
                       do (return-from seek-plans nil)
                     do (trace-print :tasks (get-task-name task1) state
                              "~2%Depth ~s, backtracking from task ~s"
                              depth
                              task1)
                        (backtrack "Task ~S failed" task1))))))))

(defun choose-immediate-task (immediate-tasks unifier)
  "Which of the set of IMMEDIATE-TASKS should SHOP2 work on
first?  Defaults to the first element, unless the user intervenes."
  (if *hand-steer*
      (user-choose-task immediate-tasks unifier t)
      (car immediate-tasks)))

(defmethod task-sorter ((domain domain) task-list unifier)
  "The default task-sorter method simply loops through the 
elements of task-list."
  (declare (ignore unifier))
  task-list)

(defun user-choose-task (task-list unifier &optional (immediate nil))
  "Function called to allow a user to choose the next task for expansion, instead 
of SHOP2."
  (let ((num-tasks (length task-list)))
    (format t "~&Choose a~:[ ~;n immediate ~] task for expansion:~%"
            immediate)
    (if (= num-tasks 1)
        (let ((input (y-or-n-p "~&Only one candidate ~:[~;immediate ~] task for expansion:~%~T~S~%Proceed?~%"
                               immediate (apply-substitution (first task-list) unifier))))
          (if input
              (first task-list)
              (if (y-or-n-p "Abort planning?")
                  (throw 'user-done nil)
                  (user-choose-task task-list immediate))))
        (progn
          (loop for i from 0
                for task in task-list
                for bound-task = (apply-substitution task unifier)
                do (format t "~D - ~S~%" i bound-task))
          (let ((input (read)))
            (if (and (typep input 'fixnum)
                     (>= input 0)
                     (< input num-tasks))
                (nth input task-list)
                (user-choose-task task-list immediate)))))))

(defmethod seek-plans-task (domain task1 state tasks top-tasks partial-plan
                              partial-plan-cost depth which-plans
                              protections
                              unifier
                              )
  (let ((task-name (get-task-name task1))
        (task-body (get-task-body task1)))
    (trace-print :tasks task-name state
                 "~2%Depth ~s, trying task ~s"
                 depth
                 (apply-substitution task1 unifier))
;;    (y-or-n-p "continue?")
    (if (primitivep task-name)
        (seek-plans-primitive domain task1 task-name task-body state tasks top-tasks
                              partial-plan partial-plan-cost depth which-plans
                              protections
                              unifier 
                              )
        (seek-plans-nonprimitive domain task1 task-name task-body state tasks top-tasks
                                 partial-plan partial-plan-cost depth
                                 which-plans protections
                                 unifier
                                 ))))

;;; so we can use etypecase in seek-plans-primitive
(deftype operator ()
  '(satisfies operator-p))
(deftype pddl-action ()
  '(satisfies pddl-action-p))

(defmethod seek-plans-primitive ((domain domain) task1 task-name task-body state tasks top-tasks
                                      partial-plan partial-plan-cost depth which-plans protections unifier)
;;; I commented out the following two lines since we are getting the domain object from the first parameter right now
;;; [2006/08/09:ugur]
;;  (declare (special *domain*))
;;  (let ((m (operator *domain* task-name)))
  (let ((m (operator domain task-name)))
    (unless m
      (error "No operator for task ~s" task1))
    (multiple-value-bind
        (result1 tag protections cost operator-unifier)
        ;; I can't yet turn this into method dispatch, because the
        ;; operators and pddl-actions, so far, are only :list type
        ;; structs --- this because they used to be simply lists.
        ;; Changing this will require tracking places where
        ;; unification happens and fixing them. [2006/07/30:rpg]
        (etypecase m
            (operator 
             (apply-operator domain state task-body m protections depth
                             unifier))
            ;; pddl action
            (pddl-action 
             (apply-action state task-body m protections depth
                             unifier)))

      ;;; There is no planning operator for this primitive task. Return FAILURE.
      (when (eql result1 'fail)
        (return-from seek-plans-primitive nil))

      (when *plan-tree*
        (record-operator task1 result1 operator-unifier))

      (multiple-value-bind (top-tasks1 tasks1)
          (delete-task-top-list top-tasks tasks task1)

        ;; MCM (11/4/2013) Added to allow replans to cutoff rest of planning.
    (when (eq task-name '!replan)
        (when *optimize-cost*
            (error "Cannot optimize costs when replans occur!")
        )
            (trace-print :operators '!replan state
                         "~2%Depth ~s, discarding remaining tasks due to replan added."
                         depth
        )
        ;(break "Attempting replan")
        (seek-plans domain state nil nil
            (cons cost (cons result1 partial-plan))
            (+ cost partial-plan-cost) (1+ depth) which-plans protections
            operator-unifier
        )
        (return-from seek-plans-primitive nil) 
    )
        
        (let ((new-cost (+ cost partial-plan-cost)))
          (when (and *optimize-cost*
                     (not (acceptable-cost-p new-cost)))
            (trace-print :operators task-name state
                         "~2%Depth ~s, backtracking from operator ~s because the plan costs too much~%     task ~s~%     cost ~s"
                         depth task-name (cdr task1) new-cost)
            (backtrack "Exceeded plan cost with operator ~S" task-name)
            (retract-state-changes state tag)
            (return-from seek-plans-primitive nil))
          (seek-plans domain state tasks1 top-tasks1 
                      (cons cost (cons result1 partial-plan))
                      new-cost (1+ depth) which-plans protections
                      operator-unifier))
        (retract-state-changes state tag)
        nil))))

(define-condition no-method-for-task (error)
  ((task-name
    :initarg :task-name
    ))
  (:report report-no-method))

(defun report-no-method (x str)
  (format str "No method definition for task ~A" (slot-value x 'task-name)))

(defmethod seek-plans-nonprimitive 
                     ((domain domain) task1 task-name task-body state tasks 
                                      top-tasks partial-plan partial-plan-cost
                                      depth which-plans protections
                                      in-unifier
                                      )
  (let ((methods (methods domain task-name)))
    (unless methods
      (error (make-condition 'no-method-for-task :task-name task-name)))
    (dolist (m methods)
        (multiple-value-bind (result1 unifier1)
            (apply-method domain state task-body m protections depth in-unifier)
            (when result1
                (loop for lr in result1
                      as u1 in unifier1
                      for label = (car lr)
                      for r = (cdr lr)
                      with tasks1 and top-tasks1
                 when *plan-tree*
                 do (record-reduction task1 r u1 label)
                 do (trace-print :methods label state
                        "~2%Depth ~s, applying method ~s~%      task ~s~%   precond ~s~% reduction ~s"
                        depth label task1 (second (member label m)) r
                    ) ;MCM (09/15): This was printing preconditions for the first method, not the selected method.
                    (when (not (member :method *shop-trace*)) ; MCM (09/15): This message seems to be redundant with the prior one when it's printed.
                       (trace-print :tasks task-name state
                         "~2%Depth ~s, reduced task ~s~% reduction ~s"
                         depth task1 r
                       )
                    )
                    (setq top-tasks1 (replace-task-top-list top-tasks task1 r))
                    (let ((new-task-net (replace-task-main-list tasks task1 r)))
                        (setq tasks1 new-task-net)
                    )
                    (seek-plans 
                        domain state tasks1 top-tasks1 partial-plan
                        partial-plan-cost (1+ depth) which-plans
                        protections u1
                    )
                 when (and *plans-found* 
                           (or (eq which-plans :first)
                               (>= (length *plans-found*) *max-plans*)
                           )
                           (not (optimize-continue-p which-plans))
                      )
                 do (return-from seek-plans-nonprimitive nil)
                )
            )
        )
    )
  )
)

;;; Called when there are no top level tasks to run
(defmethod seek-plans-null ((domain domain) state which-plans partial-plan partial-plan-cost depth unifier)
  ;; note that ACCEPTABLE-COST is actually a misnomer.  If you aren't
  ;; thinking about cost at all (e.g., you are just trying to find the
  ;; first plan, this will end up being T). [2004/09/14:rpg]
  (let ((acceptable-cost (acceptable-cost-p partial-plan-cost))
        (final-plan (strip-NOPs (reverse partial-plan)))) 

    (trace-print :plans nil state
                 "~2%Depth ~D, have found a plan with cost ~D~%*optimize-cost* = ~S, *optimal-cost* = ~A~%"
                 depth partial-plan-cost
                 *optimize-cost* *optimal-cost*)
    ;; This hook is useful for external routines that invoke SHOP2 
    ;;  and want to do something whenever a plan is found.  For example,
    ;;  the Java / SHOP2 interface uses this to store the final state
    ;;  of the plan.
    (when (fboundp 'plan-found-hook)
      (funcall (fdefinition 'plan-found-hook)
               state which-plans final-plan partial-plan-cost depth))

    ;; I believe the condition here boils down to "this is a redundant
    ;; plan --- you only want the first optimal plan, and this one
    ;; you've found has exactly the same cost as a previously found
    ;; one." [2005/01/06:rpg]
    (if (and *optimize-cost* acceptable-cost
             (eq which-plans :first)
             (equal *optimal-cost* partial-plan-cost))
        (trace-print :plans nil state
                     "~4TAlready have a plan with identical cost.~%")
        ;; old body of the UNLESS
        (progn 
          (if (and *optimize-cost* acceptable-cost)
              (progn
                (trace-print :plans nil state
                             "~4TCost of new plan is acceptable.~%")
                (when (or (null *optimal-cost*)
                          (< partial-plan-cost *optimal-cost*))
                  (trace-print :plans nil state
                               "~4TStoring new plan as optimal plan.~%")
                  (setq *optimal-plan* final-plan)
                  (setq *optimal-cost* partial-plan-cost))
                (cond ((member which-plans '(:first :shallowest))
                       (trace-print :plans nil state "~4TThrowing away old plans.~%")
                       (dump-previous-plans!))
                      (t
                      (trace-print :plans nil state "~4TFiltering out other plans of higher cost.~%")
                      (dump-higher-cost-plans!))))
              (when *optimize-cost*
                (trace-print :plans nil state
                           "~4TCost of new plan is UNacceptable.~%")))
          (cond
            ((or (eq which-plans :all-shallowest)
                 (and (eq which-plans :shallowest) *optimize-cost*))
             (when (not (equal *depth-cutoff* depth))
               (dump-previous-plans!)
               (setq *depth-cutoff* depth)))
            ((eq which-plans :shallowest)
             (setq *depth-cutoff* (1- depth))
             (dump-previous-plans!)))
          (when acceptable-cost
            (trace-print :plans nil state "~4TStoring new plan in *plans-found*~%")
            (store-plan! final-plan state unifier))
            )))
    ;; should this better be (values)? [2004/02/11:rpg]
    nil)

(defun store-plan! (plan state unifier)
  (push-last plan *plans-found*)
  (push-last (copy-state state) *states-found*)
  (push-last unifier *unifiers-found*))

;;; helpers for SEEK-PLANS-NULL [2005/01/07:rpg]
(defun dump-previous-plans! ()
  "Clear out the data structures (special variables) 
we use to return plans."
  (setf *plans-found* nil
        *unifiers-found* nil
        *states-found* nil))

(defun dump-higher-cost-plans! ()
  (loop for plan in *plans-found*
        for unifier in *unifiers-found*
        for state in *states-found*
        when (acceptable-cost-p (plan-cost plan))
          collect plan into plans
        and collect unifier into unifiers
        and collect state into states
        finally (setf *plans-found* plans
                      *unifiers-found* unifiers
                      *states-found* states)))

;;; end helpers for SEEK-PLANS-NULL


(defun get-task-name (task1)
  (if (eq (second task1) :immediate)
    (third task1)
    (second task1)))

(defun get-task-body (task1)
  (if (eq (second task1) :immediate)
    (cddr task1)
    (cdr task1)))

;;; This function returns true iff additional optimization needs to be done.
(defun optimize-continue-p (which)
  (assert (or (eq which :first) (eq which :all)))
  (cond
   ((not *optimize-cost*) nil)
   ;; added this so that we can terminate immediately if we're
   ;; optimizing and only need one plan. [2005/01/10:rpg]
   ((and (eq which :first) (zerop *optimal-cost*)) nil)
   ((and (numberp *optimize-cost*)
         (numberp *optimal-cost*))
    (>= *optimal-cost* *optimize-cost*))
   (t t)))

;;; This function returns true iff a given cost is acceptible under the
;;;  current optimization requirements.
(defun acceptable-cost-p (cost)
  (cond
   ((numberp *optimize-cost*)
    (<= cost *optimize-cost*))
   ((and *optimize-cost*
         (not (eq *optimal-plan* 'fail)))
    (<= cost *optimal-cost*))
   (t t)))

;;; This function returns true iff there is a time limit and it has expired.
(defun time-expired-p ()
  (if *internal-time-limit*
      (>= (- (get-internal-run-time) *start-run-time*)
          *internal-time-limit*)
    nil))
  
;;; ------------------------------------------------------------------------
;;; :plan-tree option functions
;;; ------------------------------------------------------------------------

; This function records the parents of each subtask in a reduction.
(defun record-reduction (task1 reduction unifier label)
  (declare (ignore unifier))
  (let ((all-subtasks (extract-subtasks reduction)))
    (setf *subtask-parents*
           (nconc
            *subtask-parents*
            (mapcar #'(lambda (subtask) (list subtask (list task1 label unifier)))
                    all-subtasks))))
  nil
)

(defun extract-subtasks (reduction)
  (cond
   ((atom reduction) nil)
   ((eq (first reduction) :task) (list reduction))
   (t (append (extract-subtasks (first reduction))
              (extract-subtasks (rest reduction))))))

; This function records the task atom that produced a given operator
; instance.
(defun record-operator (task1 operator unifier)
  (declare (ignore unifier))
  (setf *operator-tasks*
    (cons
     (list operator task1)
     *operator-tasks*)))
        

; This function is executed at the end of the planning process to produce
;  the final tree.
(defun extract-tree (plan)
  (strip-tree-tags
   (let* ((operator-nodes (plan-operator-nodes plan))
          (all-nodes (plan-tree-nodes operator-nodes))
          (root-nodes (node-children-old nil all-nodes)))
     (mapcar #'(lambda (root-node) (extract-subtree root-node all-nodes))
             root-nodes))))

(defun strip-tree-tags (tree)
  (cond
   ((atom tree) tree)
   ((and (eq (first tree) :task)
         (eq (second tree) :immediate))
    (rest (rest tree)))
   ((eq (first tree) :task)
    (rest tree))
   (t
    (cons
     (strip-tree-tags (first tree))
     (strip-tree-tags (rest tree))))))

(defun extract-subtree (root-node nodes)
  (let ((children (node-children root-node nodes)))
    (if children
        (cons root-node 
            (append (rest (first children))
              (mapcar #'(lambda (child) (extract-subtree (first child) nodes))
                  children
              )
            )
        )
        root-node
    )
  )
)

(defun node-children (node nodes)
  (when (null node)
      (error "nil is not a node")
  )
  (remove nil
      (mapcar
        #'(lambda (other-node) 
           (let* 
             ((onode (or (operator-task other-node) other-node))
              (parent-relation (assoc onode *subtask-parents*)))
                (if (eq (first (second parent-relation)) node)
                    ;then
                    (cons onode (rest (second parent-relation)))
                    ;else
                    nil
                )
             
           )
          )
        nodes
       )
  )
)

(defun node-children-old (node nodes)
  (remove-if-not
   #'(lambda (other-node) 
       (eq (first (second (assoc (or (operator-task other-node) other-node)
                          *subtask-parents*)))
           node))
   nodes)
)

(defun plan-tree-nodes (base-nodes)
  (let* ((extended-base-nodes
          (remove-duplicates
           (extend-plan-tree-nodes base-nodes)
           :from-end t))
         (new-base-nodes
          (set-difference extended-base-nodes base-nodes)))
    (if new-base-nodes
        (plan-tree-nodes extended-base-nodes)
      base-nodes)))

(defun extend-plan-tree-nodes (base-nodes)
  (if (null base-nodes) nil
    (let* ((operator-task (operator-task (first base-nodes)))
           (task (or operator-task (first base-nodes)))
           (parent (first (second (assoc task *subtask-parents*))))
           (rest-nodes (cons (first base-nodes)
                             (extend-plan-tree-nodes (rest base-nodes)))))
      (if parent
          (cons parent rest-nodes)
        rest-nodes))))

;;; Introduced an OPERATOR-NODE structure as a way of better
;;; understanding the TREE extraction code. [2004/02/05:rpg]
(defstruct (operator-node (:type list))
  cost 
  operator
  position)

;;; I think OPERATOR-TASK here actually applies to an operator NODE...
(defun operator-task (operator-node)
  (second (assoc (operator-node-operator operator-node) *operator-tasks*)))

(defun plan-operator-nodes (plan &optional (n 0))
  "This function translates operator expressions into operator nodes,
assembling together the operator, its the position in the plan and cost."
  (if (null plan) nil
    (cons
     (make-operator-node
      :cost (second plan)
      :operator (first plan)
      :position n)
     (plan-operator-nodes (rest (rest plan)) (1+ n)))))

;;; ------------------------------------------------------------------------
;;; Functions to create some of the output
;;; ------------------------------------------------------------------------

;;; debugging output, indented by INDENTATION number of spaces 
(defun indented-format (indentation &rest body)
  (format nil "~%~A" (make-string indentation :initial-element #\space))
  (apply #'format (cons nil body)))

(defun print-stats-header (label)
  (if *print-stats*
    (format t
            "~%~7@a Plans Mincost Maxcost Expansions Inferences  CPU time  Real time"
            label)))

(defun print-stats (depth plans tasks inferences runtime realtime)
  (if *print-stats*
    (format t "~%~6@a~6@a ~7@a ~7@a~11@s~11@s~10,3f~11,3f"
            depth (length plans)
            (if plans (to-string (apply #'min (mapcar #'plan-cost plans)) 2) "-")
            (if plans (to-string (apply #'max (mapcar #'plan-cost plans)) 2) "-")
            tasks inferences
            (/ runtime internal-time-units-per-second)
            (/ realtime internal-time-units-per-second))))

; Converts a number to a string.
; Examples:
;   (to-string 3 2) => "3"
;   (to-string 3.0 2) => "3.0"
;   (to-string 3.8 2) => "3.8"
;   (to-string 3.775 2) => "3.78"
; This is sort of like the ~F format string, but it doesn't add extra 0's
;  at the end.
(defun to-string (num &optional (max-decimal 1))
  (if (integerp num) (format nil "~a" num)
    (let* ((base-string (format nil (format nil "~~,~aF" max-decimal) num))
           (trimmed-string (string-right-trim "0" base-string)))
      (if (eq #\. (char trimmed-string (1- (length trimmed-string))))
          (concatenate 'string trimmed-string "0")
        trimmed-string))))

(defun plan-cost (plan)
  (if (null plan)
    0
    (+ (cadr plan) (plan-cost (cddr plan)))))

;;;---------------------------------------------------------------------------
;;; This should only be called on plans with COSTS IN THEM!!!!!
;;;---------------------------------------------------------------------------
(defun shorter-plan (plan)
  "Removes the internal operators from a plan sequence, and
returns the resulting new sequence.  AFAICT, this also removes the costs.
Non-destructive."
  (cond 
   ((null plan) nil)
   ((internal-operator-p (first (first plan)))
    (shorter-plan (rest (rest plan))))
   (t
    (cons (first plan) (shorter-plan (rest (rest plan)))))))

(defun remove-costs (plan-and-costs)
  "The SHOP2 plans come with the operators interspersed with their costs.
This function just throws away the costs."
  (loop with planlist = plan-and-costs
        while planlist
        for (operator cost . rest) = planlist
        do (assert (numberp cost))
        collect operator
        do (setf planlist rest)))

(defun internal-operator-p (operator-name)
  (if (symbolp operator-name) 
      (let ((name (symbol-name operator-name)))
        (and 
         (>= (length name) 2)
         (equal (elt name 0) #\!)
         (equal (elt name 1) #\!)))
    nil))

(defun print-output-file (plan)
  (dolist (obj plan)
    (format t ", ")
    (print1 obj)))

(defun print1 (obj)
  (if (atom obj) (format t " ~A" obj)
      (progn
        (format t "(~S" (car obj))
        (dolist (obj1 (cdr obj))
          (print1 obj1))
        (format t ")"))))

;;;(defun get-alist (variables)
;;;  (let (alist vlist)
;;;    (setq vlist (mapcar #'(lambda (x) (declare (ignore x))
;;;                            (variable-gensym))
;;;                            variables))
;;;    (setq alist (pairlis variables vlist))
;;;    (values alist vlist)))

;;; Plan accessible debugging information.

;;; The following 4 routines, which were written by Chiu, are not
;;; invoked by any other SHOP routines.  Instead, they are intended to
;;; be invoked in call or eval forms within a SHOP domain; they are
;;; intended to assist with debugging a domain.

(defun query-current-state (first-symbol)
  (state-all-atoms-for-predicate *current-state* first-symbol))

(defun print-current-state ()
  (format t "~%~A~%" (state-atoms *current-state*))
  t)

(defun print-current-tasks ()
  (format t "~%~A~%" *current-tasks*)
  t)

(defun print-current-plan ()
  (let ((tplan *current-plan*)
        (fplan nil)
        (i 1))
    (do* ((cc (pop tplan) (pop tplan))
          (tt (pop tplan) (pop tplan)))
         ((not cc))
      (when (not (internal-operator-p (car tt)))
        (push tt fplan)))
    (dolist (s fplan)
      (format t "~A: ~A~%" i s)
      (setf i (1+ i)))
    t))

;;;---------------------------------------------------------------------------
;;; Debug function
;;;---------------------------------------------------------------------------
(defun backtrack (format-string &rest args)
  (when *break-on-backtrack*
    (apply #'break format-string args)))


;;; Note: I believe the following comment is wrong.  Instead, I
;;; believe that this function is actually called on add and delete
;;; lists as well as on the preconditions. [2004/02/03:rpg]

;;; this is the main function that does the pre-processing, it
;;; looks through the preconditions finding the forall 
;;; conditions and replace the variables in that condition
(defun process-pre (pre)
  (let (answer pre1 alist vlist)
    (if (atom pre)
        (return-from process-pre pre)
        (progn
          (setq pre1 (car pre))
          (cond  
            ((listp pre1)
             (setq answer (cons (process-pre pre1) (process-pre (cdr pre)))))
            ((eql pre1 'or)
             (setq answer (cons 'or (process-pre (cdr pre)))))
            ((eql pre1 'imply)
             (setq answer (cons 'imply (process-pre (cdr pre)))))
            ((eql pre1 ':first)
             (setq answer (cons ':first (process-pre (cdr pre)))))
            ((eql pre1 'forall)
             (progn
               (unless (and (listp (second pre)) (every #'(lambda (x) (variablep x)) (second pre)))
                 (error "The first argument to a FORALL expression should be a LIST of variables in ~S"
                        pre))
               (multiple-value-setq (alist vlist) (get-alist (second pre)))
               (setq answer (list 'forall vlist 
                                  (process-pre (apply-substitution (third pre) alist))
                                  (process-pre (apply-substitution (fourth pre) alist))))))
            (t
             (setq answer pre)))
          (return-from process-pre answer)))))

;;; this function pre-processes the methods, replace every 
;;; variable defined by the forall condition to a previously
;;; unused variable. It also regularizes the methdods in old SHOP format.
(defmethod process-method ((domain domain) method)
  (let ((method-head (cadr method))
        (answer nil)
        (tail nil)
        (branch-counter 0)
        (method-name nil)
        clause first-of-clause)
    (setq method-name (car method-head))
    (setq answer (list (car method) method-head))
    (setq tail (cddr method))
    (append answer 
            (loop until (null tail)
                  do (incf branch-counter)
                  when (and (not (null (car tail))) (symbolp (car tail)))
                    append (list (pop tail))       ; skip over a label
                  else append (list (gensym (format nil "~A~D--" 
                                                    method-name branch-counter)))
                  append (list  (process-pre (pop tail))
                                ;; check to see if there is a quote or 
                                ;; backquote in the front of this list (SHOP1
                                ;; or SHOP2 syntax) and process accordingly
                                (progn 
                                  (setq clause (pop tail)
                                        first-of-clause (car clause))
                                  (cond ((or (eq first-of-clause 'quote)
                                             (eq first-of-clause *back-quote-name*))
                                         ;; this next bit of strangeness is to take the quote 
                                         ;; off the front of the task list,
                                         ;; decompose the task list, then slap the quote back on
                                         (list first-of-clause (process-task-list (cadr clause))))
                                        ((search-tree 'call clause)
                                         (list 'simple-backquote (process-task-list clause)))
                                        (t (list 'quote (process-task-list clause))))))))))

;;; returns t if item is found anywhere in the tree; doubly recursive,
;;; but only runs once per method definition.
(defun search-tree (item tree)
  (cond ((eq item tree) t)
        ((not (consp tree)) nil)
        (t (or (search-tree item (car tree)) (search-tree item (cdr tree))))))

;;; The above puts QUOTE on the front, but inside the clause there could be a "call" which is
;;; ignored.  Previously, instead of "call" there would have been a backquote around the form and
;;; commas in front of the "call".  After substituting the variables, the form is eval'd and the
;;; backquote is done or the quote is just taken off.  Now, something like backquote needs to be done.
;;; perhaps the form should be searched for an instance of "call"; if found, put "backquote" on the
;;; front and modify the backquote function to handle "call" like comma.

;;; this function pre-process the operator, replace every 
;;; variable defined by the forall condition to a previously
;;; unused variable. It also addresses the issue of different
;;; syntaxes of operators in different versions of SHOP.
(defmethod process-operator ((domain domain) operator)
  (let ((lopt (length operator)))
    (cond ((= lopt 4)             ; a SHOP 1 operator, no cost specified
           (make-operator :head (second operator)
                          :preconditions nil
                          :deletions (process-pre (third operator))
                          :additions (process-pre (fourth operator))
                          :cost-fun 1.0))
          ;; a SHOP 1 operator, with cost specified
          ((and (= lopt 5) (numberp (fifth operator)))
           (make-operator :head (second operator)
                          :preconditions nil
                          :deletions (process-pre (third operator))
                          :additions (process-pre (fourth operator))
                          :cost-fun (process-pre (fifth operator))))
          ((= lopt 5)             ; a SHOP 2 operator, no cost specified
           (make-operator :head (second operator)
                          :preconditions (process-pre (third operator))
                          :deletions (process-pre (fourth operator))
                          :additions (process-pre (fifth operator))
                          :cost-fun 1.0))
          ((= lopt 6)             ; a SHOP 2 operator, with cost specified
           (make-operator :head (second operator)
                          :preconditions (process-pre (third operator))
                          :deletions (process-pre (fourth operator))
                          :additions (process-pre (fifth operator))
                          :cost-fun (process-pre (sixth operator))))
          (t (error (format nil "mal-formed operator ~A in process-operator" operator))))))
