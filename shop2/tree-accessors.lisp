;;; The Original Code is SHOP2.  
;;; 
;;; This code developed by Robert P. Goldman.
;;; Portions created by Drs. Goldman and Maraist are Copyright (C)
;;; 2004-2007 SIFT, LLC.

;;; Smart Information Flow Technologies Copyright 2006-2007 Unpublished work
;;; 
;;; GOVERNMENT PURPOSE RIGHTS
;;; 
;;; Contract No.         FA8650-06-C-7606, 
;;; Contractor Name      Smart Information Flow Technologies, LLC
;;;                      d/b/a SIFT, LLC
;;; Contractor Address   211 N 1st Street, Suite 300
;;;                      Minneapolis, MN 55401
;;; Expiration Date      5/2/2011
;;; 
;;; The Government's rights to use, modify, reproduce, release,
;;; perform, display, or disclose this software are restricted by
;;; paragraph (b)(2) of the Rights in Noncommercial Computer Software
;;; and Noncommercial Computer Software Documentation clause contained
;;; in the above identified contract. No restrictions apply after the
;;; expiration date shown above. Any reproduction of the software or
;;; portions thereof marked with this legend must also reproduce the
;;; markings.


 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE. 
 
 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; (at your option) any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :shop)

(defun complex-node-p (tree-node)
  "Is TREE-NODE a representation of a complex node (i.e., not an operator) in 
the SHOP2 tree format as described in SHOP2?"
  (listp (first tree-node)))

(defun complex-node-task (tree-node)
  "TREE-NODE must be a COMPLEX-NODE (cf. COMPLEX-NODE-P).
Returns the corresponding TASK s-expression."
  (first tree-node))

(defun complex-node-children (tree-node)
  "TREE-NODE must be a COMPLEX-NODE (cf. COMPLEX-NODE-P).
Returns its children."
  (rest tree-node))

(defun (setf complex-node-children) (value tree-node)
  (assert (complex-node-p tree-node))
  (setf (cdr tree-node) value))

(defun make-complex-node (task children)
  (cons task children))

(defun remove-internal-operators (complex-node)
  "Returns a new complex-node like the original, but with any 
children that are internal operators (primitive nodes) removed."
  (assert (complex-node-p complex-node))
  (make-complex-node (tree-node-task complex-node)
                     (loop for child in (complex-node-children complex-node)
                         if (complex-node-p child)
                         collect child
                         else unless (internal-operator-p
                                      (tree-node-task-name child))
                         collect child)))

(defun primitive-node-p (tree-node)
    "Is TREE-NODE a representation of a primitive node (i.e., an operator) in 
the SHOP2 tree format as described in SHOP2?"
  (and (= (length tree-node) 3)
       (numberp (first tree-node))))

(defun primitive-node-task (tree-node)
  "TREE-NODE must be a PRIMITIVE-NODE (cf. PRIMITIVE-NODE-P).
Returns the corresponding TASK s-expression."
  (second tree-node))

(defun tree-node-task (tree-node)
  (cond ((primitive-node-p tree-node) (primitive-node-task tree-node))
        ((complex-node-p tree-node)   (complex-node-task tree-node))
        (t (error "Not a valid SHOP2 tree node."))))

(defun tree-node-task-name (tree-node)
  (task-name (tree-node-task tree-node)))

(defun task-name (task)
  (case (first task)
    (:task (task-name (rest task)))
    (:immediate (second task))
    (otherwise (first task))))

(defun task-args (task)
  (let ((task-pos (position (task-name task) task)))
    (rest (nthcdr task-pos task))))

(defun find-complex-node-if (fun tree)
  "Return a complex node whose TASK (first element)
satisfies FUN."
  (labels ((list-iter (lst)
             (unless (null lst)
               (or (node-iter (first lst))
                   (list-iter (cdr lst)))))
           (node-iter (node)
             (when (complex-node-p node)
;;               (format t "Complex node head: ~S~%" (first node))
               (if (funcall fun (first node)) node
                   (list-iter (cdr node))))))
    ;; top level i
    (list-iter tree)))

(defun find-all-complex-node-if (fun tree)
  "Return a complex node whose TASK (first element)
satisfies FUN."
  (labels ((list-iter (lst acc)
             (if (null lst)
                 acc
                 (let ((new (node-iter (first lst))))
                   (reverse (list-iter (cdr lst) (append new acc))))))
           (node-iter (node)
             (when (complex-node-p node)
;;               (format t "Complex node head: ~S~%" (first node))
               (let ((new (when (funcall fun (first node)) (list node))))
                 (list-iter (cdr node) new)))))
    ;; top level i
    (list-iter tree nil)))

(defun find-complex-node-for-task (task-name tree)
  "Find a complex-node in the TREE whose task name is TASK."
  (find-complex-node-if
   #'(lambda (task)
       (eq (first task) task-name))
   tree))

(defun find-all-complex-node-for-task (task-name tree)
  "Find all complex-nodes in the TREE whose task name is TASK."
  (find-all-complex-node-if
   #'(lambda (task)
       (eq (first task) task-name))
   tree))



