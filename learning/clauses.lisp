 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: 2014
 ;;;; Project Name: Explanation
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file provides the FOIL-PS clause data structure and functions
 ;;;; for manipulating it.

(in-package :artue)

(defstruct clause
    (lits nil)
    (var-types nil)
)

(defun clause-empty (clause)
    (null (clause-lits clause))
)

(defun clause-conjoin (envmodel new-lit clause)
    (let ((pred (predicate-named (cnd-type new-lit) 
                                 (get-predicates envmodel))
          )
          new-clause
         )
        (setq new-clause
            (make-clause 
                :lits (cons new-lit (clause-lits clause))
                :var-types 
                    (if pred
                        ;then
                        (union (pairlis (cnd-args new-lit) 
                                        (mapcar #'second (predicate-arg-types pred))
                               )
                               (clause-var-types clause)
                               :test #'equalp
                        )
                        ;else
                        (clause-var-types clause)
                    )
            )
        )
        (when (and pred (not (member (cnd-value new-lit) '(t nil))))
            (setf (clause-var-types new-clause)
                (adjoin (cons (cnd-value new-lit) (predicate-value-type pred))
                    (clause-var-types new-clause)
                    :test #'equalp
                )
            )
        )
        new-clause
    )
)
 
(defun clause-size (clause)
    (length (clause-lits clause))
)
                    
(defun generate-possible-literals (clause pred 
                                   &aux (candidate-literals nil) new-cnd 
                                        new-var-types old-vars cnd
                                  )
    (setq cnd (make-cnd :type (predicate-name pred)))
    (when (predicate-value-type pred)
        (setq new-cnd (copy-cnd cnd))
        (setf (cnd-value new-cnd) (generate-variable (predicate-value-type pred)))
        (push new-cnd candidate-literals)
        (setq new-cnd (copy-cnd cnd))
        (setf (cnd-inequality new-cnd) #'neq)
        (setf (cnd-value new-cnd) (generate-variable (predicate-value-type pred)))
        (push new-cnd candidate-literals)
        (dolist (var-type-pair (clause-var-types clause))
            (when (eq (cdr var-type-pair) (predicate-value-type pred))
                (setq new-cnd (copy-cnd cnd))
                (setf (cnd-value new-cnd) (car var-type-pair))
                (push new-cnd candidate-literals)
                (setq new-cnd (copy-cnd cnd))
                (setf (cnd-inequality new-cnd) #'neq)
                (setf (cnd-value new-cnd) (car var-type-pair))
                (push new-cnd candidate-literals)
            )
        )
    )
    (when (not (predicate-value-type pred))
        (setq new-cnd (copy-cnd cnd))
        (setf (cnd-value cnd) t)
        (push new-cnd candidate-literals)
        (setq new-cnd (copy-cnd cnd))
        (setf (cnd-value cnd) nil)
        (push new-cnd candidate-literals)
    )
    
    (dolist (arg-type (predicate-arg-types pred))
        (push (cons (generate-variable (second arg-type)) (second arg-type))
            new-var-types
        )
    )
                    
    (setq candidate-literals
        (generate-args candidate-literals 
            (reverse (mapcar #'second (predicate-arg-types pred)))
            (clause-var-types clause)
            new-var-types
        )
    )

    ;;Restricts candidate literals to only those that use at least one old 
    ;;variable.
    (setq old-vars (mapcar #'first (clause-var-types clause)))

    (setq candidate-literals
        (remove-if 
            #'(lambda (lit) 
                (null (intersection 
                        (cons (cnd-value lit) (cnd-args lit))
                        old-vars
                      )
                )
              )
            candidate-literals
        )
    )
    (setq candidate-literals (set-difference candidate-literals (clause-lits clause) :test #'equal-cnd))
    (setq candidate-literals (set-difference candidate-literals (clause-lits clause) :test #'contradicts-cnd))
)

(defun generate-args (candidate-literals arg-types existing-var-types
                      new-var-types
                      &aux (candidate-args nil) (new-candidates nil) new-cnd 
                           (found-new-var nil)
                     )
    (when (null arg-types)
        (return-from generate-args candidate-literals)
    )
    
    (dolist (var-type-pair existing-var-types)
        (when (eq (cdr var-type-pair) (car arg-types))
            (push (car var-type-pair) candidate-args)
        )
    )
    

    (dolist (lit candidate-literals)
        (setq found-new-var nil)
        (when (and (null (cnd-inequality lit)) (cnd-value lit))
            (dolist (var-type-pair new-var-types)
                (when (and (eq (cdr var-type-pair) (car arg-types))
                           (not (member (car var-type-pair) (cnd-args lit)))
                           (not found-new-var)
                      )
                    (setq new-cnd (copy-cnd lit))
                    (push (car var-type-pair) (cnd-args new-cnd))
                    (push new-cnd new-candidates)
                    (setq found-new-var t)
                )
            )
            (when (not found-new-var)
                (error "No new var found.")
            )
        )

        (dolist (arg-val candidate-args)
            (setq new-cnd (copy-cnd lit))
            (push arg-val (cnd-args new-cnd))
            (push new-cnd new-candidates)
        )
    )
    
    (generate-args new-candidates
        (rest arg-types)
        existing-var-types
        new-var-types           
    )
)

