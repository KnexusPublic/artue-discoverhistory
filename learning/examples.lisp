 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: 2014
 ;;;; Project Name: Explanation
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file provides the FOIL-PS example data structure and functions
 ;;;; for manipulating it.
 
(in-package :artue)

(defstruct example-bag
    (examples nil)
)

(defstruct example
    (fact-set nil)
    (bindings nil)
)

(defun new-example (fset)
    (when (not (fact-set-p fset))
        (error "Can't construct example from ~A" fset)
    )
    (make-example :fact-set fset :bindings (list nil))
)

(defun new-example-bag (ex-set)
    (when (notevery #'fact-set-p ex-set)
        (error "Can't construct example bag from ~A" ex-set)
    )
    (make-example-bag :examples (mapcar #'new-example ex-set))
)



;; Produces a new set of bags containing only those bags which match 
;; the clause produced by extending the current clause with a new literal. 
;; In addition, the retained bags contain only those examples that match the new
;; literal, and those examples are modified to contain new binding-lists 
;; consistent with the new literal.
(defun refine-example-bags (envmodel example-bags new-literal &aux (ret-bags nil) new-exes)
    (dolist (bag example-bags)
        (setq new-exes (refine-examples envmodel (example-bag-examples bag) new-literal))
        (when new-exes
            (push (make-example-bag :examples new-exes) ret-bags)
        )
    )
    ret-bags
)

 
;; Produces a new set of examples containing only those examples which match 
;; the clause produced by extending the current clause with a new literal.
;; All examples are modified to contain new binding-lists consistent with the 
;; new literal.
(defun refine-examples (envmodel example-set new-literal &aux (ret-exes nil) cur-ex)
    (dolist (ex example-set)
        (setq cur-ex (make-example :fact-set (example-fact-set ex) :bindings nil))
        (dolist (binding-list (example-bindings ex))
            (setf (example-bindings cur-ex) 
                (append (example-bindings cur-ex)
                    (unify-example envmodel new-literal cur-ex binding-list)
                )
            )
        )
        (when (example-bindings cur-ex)
            (push cur-ex ret-exes)
        )
    )
    ret-exes
)

(defun unify-example (envmodel lit ex blist &aux lits pred)
    (setq lits nil)
    (setq pred (predicate-named (cnd-type lit) (get-predicates envmodel)))
    (when pred
        (dolist (arg-type-pair (pairlis (cnd-args lit) (mapcar #'cdr (predicate-arg-types pred))))
            (when (and (is-variable (car arg-type-pair)) 
                       (not (member (car arg-type-pair) blist :key #'car)))
                (push (var-decl-to-cnd arg-type-pair envmodel) lits)
            )
        )
    )
    (push lit lits)
    (unify-set envmodel lits (example-fact-set ex) blist :assume-fn #'default-closed-world-assume-fn)
)


