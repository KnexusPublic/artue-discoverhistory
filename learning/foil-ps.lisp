 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: 2013
 ;;;; Project Name: Explanation
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file provides the FOIL-PS learner for environment models.

(in-package :artue)

(defparameter *maximize-coverage* nil)


(defun foil-ps (envmodel orig-positive-example-bags negative-examples
             initial-clause
             &aux (cur-clause nil) (max-depth 0) (clauses nil) 
                  (positive-example-bags nil))
    (trace-print *trace-foil* "~%SETUP COMPLETE -- STARTING INDUCTION")
    ;;; The top-level loop continues until all positive examples have
    ;;; been covered by some clause in the hypothesis.
    (setq max-depth 0)
    (setq positive-example-bags orig-positive-example-bags)
    (loop while (and positive-example-bags (< max-depth 10)) do
        (setq positive-example-bags orig-positive-example-bags)
        (setq clauses nil)
        (setq cur-clause t)
        (loop while (and positive-example-bags cur-clause) do
            (trace-print *trace-foil* "~%~%FINAL CLAUSES:~%~A~%~%" clauses)
            (setq cur-clause
                (extend-clause 
                    envmodel positive-example-bags negative-examples 
                    max-depth (copy-clause initial-clause) 0 0
                )
            )
    
            (when cur-clause
                (when (clause-empty cur-clause)
                    (break "Why?")
                )

                ;;; Add the new clause to the hypothesis, and calculate the positive
                ;;; examples yet to be covered.
                (push cur-clause clauses)
                (trace-print *trace-foil* "~%~%>>>>>FOUND CLAUSE: ~a" (car clauses))
                ;(break "Clause found.")
        
                (setq positive-example-bags
                    (remove-if 
                        #'(lambda (ex-bag)
                            (some 
                                #'(lambda (ex)
                                    (unify-set envmodel 
                                        (cdr (reverse (clause-lits cur-clause)))
                                        (example-fact-set ex) 
                                        (car (example-bindings ex)) 
                                        :assume-fn 
                                            #'default-closed-world-assume-fn
                                    )
                                  )
                                (example-bag-examples ex-bag)
                            )
                          )
                        positive-example-bags
                    )
                )
            )
        )
        (incf max-depth)
    )
    (mapcar #'reverse (mapcar #'clause-lits clauses))
)

(defun extend-clause (envmodel positive-example-bags negative-examples max-zeros
                      cur-clause best-clause-matches next-var-id
                      &aux info-value best-literal 
                           (best-clause nil) (best-matches nil) gain-lit-pairs
                     )
    (when (null positive-example-bags)
        (trace-print 
            *trace-foil* 
            "~%ABORTING CLAUSE: ~A. Matches no positive examples.~%"
            cur-clause
        )
        (return-from extend-clause nil)
    )
    (when (null negative-examples)
        (trace-print 
            *trace-foil* 
            "~%SUCCESSFUL CLAUSE: ~A. Matches no negative examples, ~A positive examples.~%"
            cur-clause
            (length positive-example-bags)
        )
        (return-from extend-clause (values cur-clause positive-example-bags))
    )
    (when (and *maximize-coverage* (>= best-clause-matches (length positive-example-bags)))
        (trace-print 
            *trace-foil* 
            "~%ABORTING CLAUSE: ~A. Best clause matches ~A examples, New clause matches ~A.~%"
            cur-clause
            best-clause-matches 
            (length positive-example-bags)
        )
        
        (return-from extend-clause nil)
    )
    (trace-print 
        *trace-foil* 
        "~%CURRENT CLAUSE: ~A~%STILL COVERS NEGATIVES~%"
        cur-clause
    )
    ;(break "Deepening clause.")
    
    (setq info-value 
        (compute-info-value 
            ;(apply #'+ (mapcar #'length (mapcar #'example-bag-examples positive-example-bags)))
            (length positive-example-bags)
            (length negative-examples)
        )
    )
    
    ;; Need to maintain this variable because lisp will retain all symbols 
    ;; created.
    (setq *next-var-id* next-var-id)
    
    (setq gain-lit-pairs 
        (find-best-literals 
            envmodel positive-example-bags negative-examples 
            info-value cur-clause :zeros-allowed (> max-zeros 0)
        )
    )
    
    (setq next-var-id *next-var-id*)

    (when (null gain-lit-pairs)
        (trace-print *trace-foil* "~%~%NO LITERAL HAS ANY GAIN. BACKTRACKING.")
        (return-from extend-clause nil)
    )
    
    (dolist (gain-lit-pair gain-lit-pairs)
        (setq best-literal (cdr gain-lit-pair))
        (trace-print *trace-foil* "~%~%BEST ANTECEDENT FOUND: ~a" best-literal)
        (let ((new-clause (clause-conjoin envmodel best-literal cur-clause)) cur-pos-ex cur-neg-ex)
            ;;; Modify the positive and negative tuples to reflect the
            ;;; newly extended current clause.
            (setq cur-pos-ex (refine-example-bags envmodel positive-example-bags best-literal))
            (setq cur-neg-ex (refine-examples envmodel negative-examples best-literal))
            
            (multiple-value-setq (new-clause cur-pos-ex)
                (extend-clause 
                    envmodel cur-pos-ex cur-neg-ex 
                    (if (zerop (car gain-lit-pair)) (1- max-zeros) max-zeros) 
                    new-clause best-clause-matches next-var-id
                )
            )

            (when *maximize-coverage*
                (when (= (length cur-pos-ex) (length positive-example-bags))
                    (return-from extend-clause (values new-clause cur-pos-ex))
                )
                
                (when (> (length cur-pos-ex) best-clause-matches)
                    (setq best-matches cur-pos-ex)
                    (setq best-clause new-clause)
                    (setq best-clause-matches (length cur-pos-ex))
                )
            )
            
            (when (and (not *maximize-coverage*) new-clause)
                (return-from extend-clause (values new-clause cur-pos-ex))
            )
        )
    )
    (values best-clause best-matches)
)



;; find-best-literals returns all literals ordered by information gain from the 
;; prior clause. Literals with gain of 0 are returned only if all literals have 
;; 0 gain.
(defun find-best-literals (envmodel positive-example-bags negative-examples 
                           old-info-value cur-clause
                           &key (zeros-allowed t)
                           &aux (all-lits nil) (max-gain 0) info-gain (lit-gains nil)
                                new-vars new-type)
    (dolist (pred (get-predicates envmodel))
        (push-all (generate-possible-literals cur-clause pred) all-lits)
    )
    (setq new-vars 
        (remove-duplicates
            (set-difference 
                (get-variable-terms (clause-lits cur-clause)) 
                (get-variable-terms (rest (clause-lits cur-clause)))
            )
        )
    )
    (dolist (new-var new-vars)
        (setq new-type (cdr (assoc new-var (clause-var-types cur-clause))))
        (dolist (poss-value (get-constants-of-type envmodel new-type))
            (push 
                (make-cnd :type 'eval :args nil :value `(eval (eq ',new-var ',poss-value)))
                all-lits
            )
        )
    )
        
        
    (dolist (lit all-lits)
        (setq info-gain
            (compute-info-gain envmodel
                old-info-value positive-example-bags negative-examples lit
            )
        ) 
        
        (when (> info-gain max-gain)
            ; (when (zerop max-gain)
                ; (setq lit-gains nil)
            ; )
            (setq max-gain info-gain)
        )
        
        ;(when (or (> info-gain 0) (and (zerop info-gain) (zerop max-gain)))
        (when (>= info-gain 0)
            (push (cons info-gain lit) lit-gains)
        )
    )
    
    (setq lit-gains
        (sort lit-gains #'> :key #'car)
    )
    
    (dolist (lit-gain lit-gains)
        (trace-print *trace-foil* "~%     Found gain: ~a  ~a" (cdr lit-gain) (car lit-gain))
    )
    
    (if zeros-allowed
        ;then
        lit-gains
        ;else
        (remove-if #'zerop lit-gains :key #'car)
    )
    ;(mapcar #'cdr lit-gains)
)

    
;;; COMPUTE-INFO-GAIN
;;; returns two values, the information gained by using this binding of
;;; the relation defined by ext-definition, and the maximum gain
;;; potentially achievable by specializations of this binding.
(defun compute-info-gain (envmodel old-info-value positive-example-bags negative-examples lit
                          &aux new-pos-count new-neg-count pos-retained)
    (multiple-value-setq (new-pos-count pos-retained)
        (count-extension-bags envmodel positive-example-bags lit)
    )
    (when (zerop pos-retained)
        (return-from compute-info-gain (values 0 0))
    )
    (setq new-neg-count (count-extensions envmodel negative-examples lit))
    (values 
        (* pos-retained 
            (- old-info-value (compute-info-value pos-retained new-neg-count))
        )
        (* pos-retained old-info-value)
    )
)
    

;;; This is the original FOIL definition which counts each possible way a 
;;; clause can be bound to the facts separately.
;;; COUNT-EXTENSIONS returns two values, the number of tuples
;;; in the set which results from expanding each tuple in TUPLES
;;; with all compatible tuples from EXT-DEFINITION under the restriction
;;; imposed by binding, and the number of tuples which have any extension.
; (defun count-extensions (examples lit &aux ext-count (total-exts 0) (retained-bindings 0))
    ; (dolist (ex examples)
        ; (dolist (binding-list (example-bindings ex))
            ; (setq ext-count (length (unify-example lit ex binding-list)))
            ; (when (not (zerop ext-count))
                ; (incf total-exts ext-count)
                ; (incf retained-bindings)
            ; )
        ; )
    ; )
    ; (values total-exts retained-bindings)
; )


;;; This is my new definition of count-extensions which counts only how many 
;;; examples are retained, and not how many ways each example can be bound.
;;; COUNT-EXTENSIONS returns two values, the number of tuples
;;; in the set which results from expanding each tuple in TUPLES
;;; with all compatible tuples from EXT-DEFINITION under the restriction
;;; imposed by binding, and the number of tuples which have any extension.
(defun count-extensions (envmodel examples lit &aux (retained-examples 0))
    (dolist (ex examples)
        (when 
            (some
                #'(lambda (blist)
                    (unify-example envmodel lit ex blist)
                  )
                (example-bindings ex)
            )
            (incf retained-examples)
        )
    )
    (values retained-examples retained-examples)
)

;;; COUNT-EXTENSION-BAGS returns two values, both of which correspond to the 
;;; number of example bags which still satisfy the clause with the new literal 
;;; added. 
(defun count-extension-bags (envmodel example-bags lit 
                             &aux (retained-examples 0) (examples-left-in-bag 0)
                                  (retained-bags 0))
    (dolist (bag example-bags)
        (when 
            (setq examples-left-in-bag
                (count-if
                    #'(lambda (ex)
                        (some
                            #'(lambda (blist)
                                (unify-example envmodel lit ex blist)
                              )
                            (example-bindings ex)
                        )
                      )
                    (example-bag-examples bag)
                )
            )
            (when (> examples-left-in-bag 0)
                (incf retained-bags)
                (incf retained-examples examples-left-in-bag)
            )
        )
    )
    (values retained-examples retained-bags)
)




