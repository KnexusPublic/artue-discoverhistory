 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: 2013
 ;;;; Project Name: Explanation
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file provides an external interface to FOIL-PS learning that 
 ;;;; can be used with the explanation generator.

(in-package :artue)

(defclass foil-learner ()
  (
    (scenario-memory :initform nil :accessor get-scenario-memory)
    (all-states :initform nil :accessor get-all-states)
    (mystery-events :initform nil :accessor get-mystery-events)
    (prior-ids :initform nil :accessor get-prior-ids)
    (learned-events :initform nil :accessor get-learned-events)
    (learned-triggers :initform nil :accessor get-learned-triggers)
    (prior-event-names :initform nil :accessor get-prior-event-names)
    (current-event-names :initform nil :accessor get-current-event-names)
    (scenario-counter :initform 0 :accessor get-scenario-counter)
    (event-counter :initform 0 :accessor get-event-counter)
  )
)

(defun wipe-memory (learner)
    (setf (get-scenario-memory learner) nil)
    (setf (get-all-states learner) nil)
    (setf (get-mystery-events learner) nil)
    (setf (get-learned-events learner) nil)
    (setf (get-learned-triggers learner) nil)
    (setf (get-prior-event-names learner) nil)
    (setf (get-current-event-names learner) nil)
    (setf (get-learned-events learner) nil)
    (setf (get-scenario-counter learner) 0)
    (setf (get-event-counter learner) 0)
)

(defun learn-from-explanation (learner expgen ex)
    (setf (get-ev-models expgen) 
        (remove-if 
            #'(lambda (em) (member (model-type em) (get-prior-event-names learner))) 
            (get-ev-models expgen)
        )
    )
    ;(setq new-ex (construct-explanation expgen (get-assumptions ex)))
    (store-scenario learner expgen (remove-learned-models expgen ex))
    (check-events learner expgen ex)
    (setf (domain-events (get-domain expgen))
        (remove-if 
            #'(lambda (pev) 
                (member (shop2::pddl-event-name pev) (get-prior-event-names learner))
              )
            (domain-events (get-domain expgen))
        )
    )
    (use-updated-knowledge learner expgen)
    nil
)

(defun use-updated-knowledge (learner expgen)
    (setf (get-learned-event-models expgen) nil)
    (mapcar+ #'add-rule-to-knowledge (get-current-event-names learner) learner expgen)
    nil
)    


(defun store-scenario (learner expgen ex &aux scen-sym)
    (setq scen-sym (combine-strings-symbols "SCENARIO" (incf (get-scenario-counter learner)))) 
    (find-inconsistencies expgen ex)
    (push (list scen-sym (explanation-cached-facts ex)) (get-scenario-memory learner))
    (push-all (mapcar #'cdr (explanation-cached-facts ex)) (get-all-states learner))
    
    (dolist (ev (get-unknown-events ex)) 
        (let ((inc (second (occurrence-signature ev))))
            (push 
                (list
                    inc
                    (get-intervening-event-points expgen inc)
                    scen-sym
                )
                (get-mystery-events learner)
            )
        )
    )
)

(defun check-events (learner expgen ex &aux (ret-evs nil))
    (dolist (cnd (get-unknown-conditions expgen ex)) 
        (push-all 
            (acquire-event-models learner expgen cnd)
            ret-evs
        )
    )
    ret-evs
)

(defun get-unknown-conditions (expgen ex)
    (remove-duplicates 
        (mapcar+ #'generalize-args 
            (mapcar #'get-unknown-event-condition (get-unknown-events ex))
            expgen
        )
        :test #'equal-cnd
    )
)

(defun generalize-args (cnd expgen &aux pred)
    (setq pred 
        (predicate-named 
            (cnd-type cnd) 
            (get-predicates (get-last-envmodel expgen))
        )
    )
    (make-cnd 
        :type (cnd-type cnd)
        :args (mapcar #'first (predicate-arg-types pred))
        :value (cnd-value cnd)
    )
)

(defun get-unknown-event-condition (ev)
    (inconsistency-cnd (second (occurrence-signature ev)))
)

(defun acquire-event-models (learner expgen mystery-cnd 
                             &aux mystery-tuples pos-states neg-states 
                                  mystery-states triggers ret-triggers 
                                  mystery-pred cur-blist is-cnds neg-examples)
    (format t "Learning to predict predicate ~A." mystery-cnd)
    (setq mystery-tuples
        (remove mystery-cnd (get-mystery-events learner) 
            :key #'(lambda (tuple) (inconsistency-cnd (car tuple)))
            :test-not #'same-outcome
        )
    )
    
    (setq pos-states nil)
    (setq neg-states (get-all-states learner))
    (setq neg-examples nil)
    (setq mystery-pred 
        (predicate-named 
            (cnd-type mystery-cnd) 
            (get-predicates (get-last-envmodel expgen))
        )
    )
    (setq is-cnds nil)
  
    (dolist (arg-type (predicate-arg-types mystery-pred))
        (push (var-decl-to-cnd arg-type (get-last-envmodel expgen)) is-cnds)
    )

    (dolist (mystery-tuple mystery-tuples)
        (setq cur-blist 
            (pairlis 
                (cnd-args mystery-cnd)
                (cnd-args (inconsistency-cnd (car mystery-tuple)))
            )
        )
        (setq mystery-states nil)
        (dolist (pt (second mystery-tuple))
            (let ((cur-fact-set                         
                        (cdr
                            (assoc (point-prior expgen pt) 
                                (second (assoc (third mystery-tuple) (get-scenario-memory learner)))
                                :test #'eql
                            )
                        )
                 ))

                (push 
                    (make-example
                        :fact-set cur-fact-set
                        :bindings (list cur-blist)
                    )
                    mystery-states
                )
        
                (push
                    (make-example 
                        :fact-set cur-fact-set
                        :bindings 
                            (remove cur-blist 
                                (unify-set expgen is-cnds cur-fact-set nil
                                    :assume-fn #'default-closed-world-assume-fn
                                    :ground-all-vars t       
                                )
                                :test #'equalp
                            )
                    )
                    neg-examples
                )
            )
        )
        (push (make-example-bag :examples (remove nil mystery-states :key #'example-fact-set)) pos-states)
        (setq neg-states (set-difference neg-states (mapcar #'example-fact-set mystery-states)))
    )
    
    (setq triggers
        (foil-ps (get-last-envmodel expgen) pos-states 
            (refine-examples (get-last-envmodel expgen)
                (append neg-examples
                    (mapcar 
                        #'(lambda (st) 
                            (make-example :fact-set st 
                                :bindings 
                                    (unify-set expgen is-cnds st nil
                                        :assume-fn #'default-closed-world-assume-fn
                                        :ground-all-vars t       
                                    )
                            )
                          )
                        neg-states 
                    )
                )
                (negate-condition mystery-cnd)
            )
            (clause-conjoin
                (get-last-envmodel expgen)      
                (negate-condition mystery-cnd)
                (make-clause)
            )
        )
    )
    (setq ret-triggers nil)
    (setf (get-current-event-names learner)
        (set-difference (get-current-event-names learner)
            (mapcar #'second
                (remove mystery-cnd (get-learned-events learner) 
                    :key #'first :test-not #'equal-cnd
                )
            )
        )
    )
    (dolist (trigger triggers)
        (let ((event-name (combine-strings-symbols "NEW-EVENT" (incf (get-event-counter learner)))))
            (push 
                (list mystery-cnd event-name)
                (get-learned-events learner)
            )
            (push (list event-name trigger mystery-cnd) (get-learned-triggers learner))
            (push event-name (get-prior-event-names learner))
            (push event-name (get-current-event-names learner))
            (push trigger ret-triggers)
        )
    )
    ret-triggers
)

(defun add-rule-to-knowledge (name learner expgen &aux evt-rep)
    (setq evt-rep 
        (apply #'build-event-representation expgen
            (assoc name (get-learned-triggers learner))
        )
    )
    (let (new-em)
        (setq new-em
            (build-event-model 
                evt-rep 
                (get-predicates (get-last-envmodel expgen)) 
                (get-last-envmodel expgen)
            )
        )
        (push-all new-em (get-ev-models expgen))
        (push-all new-em (get-learned-event-models expgen))
    )
    (push 
        (apply #'shop2::make-pddl-event 
            :domain (get-domain expgen)
            (cons :name evt-rep)
        )
        (domain-events (get-domain expgen))
    )
)
    
(defun build-event-representation (expgen name trigger cnd &aux pred 
                                                      (model-params nil) 
                                                      eff evt-rep)
    (setq pred
        (find (cnd-type cnd) (get-predicates (get-last-envmodel expgen)) 
            :key #'predicate-name
        )
    )
    ;(setq pos 0)   
    (setq eff (make-effect-from-condition cnd))
    
    (dolist (arg-type (predicate-arg-types pred))
        (let ((keep-going t) (rem-cnds trigger))
            (loop while (and rem-cnds keep-going) do
                (when (member (car arg-type) (cnd-vars (car rem-cnds)))
                    (setq keep-going nil)
                    (when (or (cnd-inequality (car rem-cnds))
                              (null (cnd-value (car rem-cnds)))
                          )
                        (push (var-decl-to-cnd arg-type (get-last-envmodel expgen)) trigger)
                    )
                )
                (pop rem-cnds)
            )
        )
    )
    
    (setq evt-rep
        (list name
            :parameters model-params
            :precondition 
                (cons 'and 
                    ;(cons (convert-dh-condition (negate-condition cnd))
                        (mapcar #'convert-dh-condition trigger)
                    ;)
                )
            :effect eff
        )
    )
    (dolist (var (vars-in evt-rep))
        (setf (get var 'shop2::variable) t)
    )

    evt-rep
)

(defun make-effect-from-condition (cnd)
    (cond
        ((cnd-inequality cnd) (error "Can't make effect from condition."))
        ((member (cnd-value cnd) '(t nil))
            (convert-dh-condition cnd)
        )
        (t (list 'set (cons (cnd-type cnd) (cnd-args cnd)) (cnd-value cnd)))
    )
)


(defun same-outcome (cnd1 cnd2)
    (and (eq (cnd-type cnd1) (cnd-type cnd2))
         (equalp (cnd-value cnd1) (cnd-value cnd2))
    )
)


(defun cnd-vars (cnd)
    (cons (cnd-value cnd) (cnd-args cnd))
)

;--------------------------------------------------------------

(defun abandon-learned-event-knowledge (expgen ex)
    ;;Prevents current event models from being reused.
    ;(setq (get-current-event-names learner) nil)
    ;;Removes new events from explanation knowledge
    (combine-event-models expgen)
    ;;Removes new events from planning knowledge
    (setf (domain-events (get-domain expgen))
        (remove-if 
            #'(lambda (pev) 
                (member (shop2::pddl-event-name pev) (mapcar #'envmodel-name (get-learned-event-models expgen)))
              )
            (domain-events (get-domain expgen))
        )
    )
    ;;Removes new events from current explanation
    (when ex
        (remove-learned-models expgen ex)
    )
    (setf (get-learned-event-models expgen) nil)
)

(defun remove-learned-models (expgen ex)
    (setq ex (copy-explanation ex))
    (dolist (model (get-learned-event-models expgen))
        (setq ex (car (abandon-event expgen ex (model-type model))))
    )
    (when (find-inconsistencies expgen ex)
        (setq expgen (copy-generator expgen))
        (setf (get-max-cost expgen) 100)
        (setq ex (car (find-plausible-explanations expgen (list (reset-explanation expgen ex :reuse t)))))
    )
    ex
)


