 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: 2013
 ;;;; Project Name: Explanation
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2018 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file provides some small utility functions specific to FOIL-PS.

(in-package :artue)

(defvar *next-var-id* 1)
(defparameter *trace-foil* nil)

(defun generate-variable (var-type)
    (combine-strings-symbols "?" var-type (incf *next-var-id*))
)


;;; COMPUTE-INFO-VALUE returns the information content of a positive
;;; signal for a set containing POS-COUNT positives and NEG-COUNT 
;;; negatives.  The goofy constant is the binary log conversion.
(defun compute-info-value (pos-count neg-count)
  (- (* (log (/ pos-count (+ pos-count neg-count))) 1.4426950408889634))
)


(defmacro trace-print (test-var &rest format-form)
  ;; Print using the format string only if test-var (usually a trace-* variable) is nonNIL
  `(if ,test-var
     (format t ,@format-form)
   )
)

(defun equal-pair (pair1 pair2)
    (and (equalp (car pair1) (car pair2))
         (equalp (cdr pair1) (cdr pair2))
    )
)

 
