 ;;;; 
 ;;;;----------------------------------------------------------------
 ;;;; @author: Matthew Molineaux
 ;;;; Date created: January 2015
 ;;;; Project Name: Autonomous Squad Member
 ;;;;----------------------------------------------------------------
 ;;;; Copyright Notice: Knexus Research Corporation, 2009 
 ;;;; All rights Reserved 
 ;;;;---------------------------------------------------------------- 
 ;;;; Distribution Notice: 
 ;;;; Pursuant to DFARS Section 252.227-7014, the government has 
 ;;;; unlimited rights in this computer software and has a royalty 
 ;;;; free, nonexclusive license to use, modify, reproduce, release, 
 ;;;; perform, display, or disclose this computer software in whole 
 ;;;; or in part, in any manner and for any purpose whatsoever, 
 ;;;; and to have or authorize others to do so. Knexus Research Corp. 
 ;;;; retains all rights not granted to the government in the computer 
 ;;;; software, including the right to seek copyright in the computer 
 ;;;; software. 
 ;;;;---------------------------------------------------------------- 
 ;;;; Disclaimer: 
 ;;;; THIS SOFTWARE IS PROVIDED BY KNEXUS RESEARCH CORP., "AS IS" 
 ;;;; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 ;;;; LIMITED TO, WARRANTIES OF INFRINGEMENT AND THE IMPLIED 
 ;;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 ;;;; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KNEXUS RESEARCH CORP. 
 ;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 ;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 ;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 ;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 ;;;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 ;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 ;;;; OUT OF THE DISTRIBUTION OR USE OF THIS SOFTWARE, EVEN IF ADVISED 
 ;;;; OF THE POSSIBILITY OF SUCH DAMAGE.

 ;;;; This program is free software: you can redistribute it and/or modify
 ;;;; it under the terms of the GNU General Public License as published by
 ;;;; the Free Software Foundation, either version 3 of the License, or
 ;;;; at your option any later version.
 ;;;; 
 ;;;; This program is distributed in the hope that it will be useful,
 ;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;;; GNU General Public License for more details.
 ;;;; 
 ;;;; You should have received a copy of the GNU General Public License
 ;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ;;;;
 ;;;; This file provides a variant explainer that uses only a deductive 
 ;;;; method to add to an explanation, and does not revise retrospectively.
 
 (in-package :artue)

(defvar *max-result-length* 30)

(defclass deductive-explanation-generator (discoverhistory-explanation-generator) 
  (
  )
)

(defun make-start-exes (expgen &aux ex pred-cnds fun-cnds temp-fun-cnds brothers)
    (setf (get-plausible-explanations expgen) nil)
    (setq pred-cnds (generate-assumable-conditions expgen))
    (setq fun-cnds (remove t pred-cnds :key #'cnd-value))
    (setq pred-cnds (remove t pred-cnds :key #'cnd-value :test-not #'eq))
    (dotimes (i *max-result-length*)
        (setq ex (get-null-explanation expgen))
        (dolist (cnd pred-cnds)
            (when (< (random 1.0) 0.5)
                (setq ex
                    (car
                        (refine-by-hypothesizing-initial-value expgen ex 
                            (make-inconsistency :cnd cnd :prior-occ nil :next-occ nil)
                        )
                    )
                )
            )
        )
        (setq temp-fun-cnds fun-cnds)
        (loop while temp-fun-cnds do
            (setq brothers (list (pop temp-fun-cnds)))
            (loop while (and temp-fun-cnds (same-map-cnd (car brothers) (car temp-fun-cnds))) do
                (push (pop temp-fun-cnds) brothers)
            )
            (setq ex 
                (car
                    (refine-by-hypothesizing-initial-value expgen ex 
                        (make-inconsistency 
                            :cnd (nth (random (length brothers)) brothers) 
                            :prior-occ nil :next-occ nil
                        )
                    )
                )
            )
        )
        (push ex (get-plausible-explanations expgen))
    )
    (get-plausible-explanations expgen)
)

(defun same-map-cnd (cnd1 cnd2)
    (and (eq (cnd-type cnd1) (cnd-type cnd2))
         (equalp (cnd-args cnd1) (cnd-args cnd2))
    )
)

(defmethod find-plausible-explanations ((expgen deductive-explanation-generator) exes &key &allow-other-keys &aux sampler results)
    (when (get-pass-through expgen)
        (return-from find-plausible-explanations exes)
    )
    (when (equalp (occurrence-point (get-prior-observation expgen)) (get-first-point expgen))
        (setq exes (make-start-exes expgen))
    )
    (setq *expgen* expgen)
    (setq *exes* exes)
    (setf (get-cost-limit expgen) 0)
    (setq sampler (new-reservoir-sampler *max-result-length*)) 
    (mapcar+ #'try-all-actions exes expgen sampler)
    (setq results (get-results sampler))
    (when (null results)
        (setf (get-pass-through expgen) t)
        (return-from find-plausible-explanations exes)
    )
    results
)

(defun try-all-actions (ex expgen sampler &aux facts pt poss-occs new-ex start-time actions-considered)
    (setq pt (point-next expgen (occurrence-point (get-prior-observation expgen))))
    (setq facts (facts-at expgen ex pt))
    (setq facts (remove-from-fact-set (get-self-fact expgen) (copy-fact-set facts)))
    (setq actions-considered 0)
    (setq start-time (get-internal-run-time))
    (dolist (act-model (get-external-action-models expgen))
        (setq poss-occs 
            (enumerate-occs expgen act-model facts pt
                 :assume-fn #'default-and-self-assume-fn :ground-all-vars t
                 :bind-var-symbols t
            )
        )
        (dolist (occ poss-occs)
            (setf (occurrence-is-action occ) t)
            (setf (occurrence-is-event occ) nil)
            (setf (occurrence-is-assumption occ) t)
            (when (not (member (get-self-fact expgen) (occurrence-pre occ) :test #'equal-fact))
                (setf (occurrence-pre occ)
                    (remove 'self (occurrence-pre occ) :key #'fact-type)
                )
                (incf actions-considered)
                (setq new-ex
                    (find-extra-events expgen 
                        (add-event-to-explanation expgen occ ex)
                    ) 
                )
                (sample-ambiguity sampler expgen (list new-ex))
            )
        )
    )
    (format t "~%Actions considered: ~A" actions-considered) 
    (format t "~%Seconds: ~A" (coerce (/ (- (get-internal-run-time) start-time) internal-time-units-per-second) 'float)) 
    
    #+ALLEGRO (excl::gc t)
    #+SBCL (sb-ext::gc)
    
    sampler
)

